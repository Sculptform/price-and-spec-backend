set :branch, proc { `git rev-parse --abbrev-ref origin/demo`.chomp }

server "10.1.1.57", user: "jenkins", roles: %w{app web db}, primary: true, no_release: false

set :deploy_root,       "/var/www/html/woodform/demo" #project root path on server
set :deploy_to,         "#{fetch(:deploy_root)}/#{fetch(:deploy_dir)}"

after "deploy:finished",        "doctrine:schema:drop"
after "doctrine:schema:drop",   "doctrine:schema:create"
after "doctrine:schema:create", "doctrine:fixtures:load"
after "doctrine:fixtures:load", "sonata:media:fix_media_context"
after "deploy:symlink:release", "deployment:extra:create_symlink"