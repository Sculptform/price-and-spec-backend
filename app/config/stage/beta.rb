set :branch, proc { `git rev-parse --abbrev-ref origin/beta`.chomp }

server "10.1.1.57", user: "jenkins", roles: %w{app web db}, primary: true, no_release: false
server "staging.sculptform.com.au", user: "ec2-user", roles: %w{staging}, primary: false, no_release: true

set :deploy_root,         "/var/www/html/woodform/beta" #project root path on server
set :deploy_to,           "#{fetch(:deploy_root)}/#{fetch(:deploy_dir)}"
set :staging_deploy_to,   "/var/www/html/#{fetch(:deploy_dir)}"
set :files_path,          "#{fetch(:staging_deploy_to)}/shared/web/"
set :staging_shared_path, "#{fetch(:staging_deploy_to)}/shared"

namespace :database do
    task :restore do
        on roles(:staging) do |host|
            info "#{host}"
            info "--> Restoring DB from staging server..."

            stage_db_dump_file_path = "#{fetch(:staging_shared_path)}/backup/#{fetch(:backup_name)}.sql"
            stage_db_dump_file_path_compressed = "#{stage_db_dump_file_path}.gz"
            local_db_dump_file_path = "#{fetch(:deploy_to)}/shared/backup/#{fetch(:backup_name)}.sql.gz"

            execute "mkdir -p #{fetch(:staging_shared_path)}/backup"
            execute "cd #{fetch(:staging_deploy_to)}/current && ./#{fetch(:symfony_console_path)} app:database:dump #{stage_db_dump_file_path}"

            on roles(:app) do |host|
                info "#{host}"
                execute "mkdir -p #{shared_path}/backup"
                execute "scp -C ec2-user@52.63.22.5:#{stage_db_dump_file_path_compressed} #{local_db_dump_file_path}"
            end
            #download! prod_db_dump_file_path, local_db_dump_file_path

            execute "rm #{stage_db_dump_file_path_compressed}"

            on roles(:app) do
                #upload! local_db_dump_file_path, fetch(:db_dump_file_path)
                invoke "doctrine:schema:drop"
                invoke "symfony:console", "app:database:restore", "#{local_db_dump_file_path}"
                execute "rm #{local_db_dump_file_path}*"
            end
        end
    end
end

namespace :files do
    task :restore do
        on roles(:app) do
            info "--> Restoring files from staging server..."
            info "--> Downloading images..."
            execute "rsync -r --exclude=quotation --exclude=projects --exclude=cache ec2-user@52.63.22.5:#{fetch(:files_path)} #{shared_path}/web"
        end
    end
end

after "deploy:finished",             "database:restore"
after "database:restore",            "files:restore"
after "files:restore",               "doctrine:migrations:migrate"
after "doctrine:migrations:migrate", "sonata:media:fix_media_context"
after "deploy:symlink:release",      "deployment:extra:create_symlink"
