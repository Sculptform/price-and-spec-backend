server "api.sculptform.com.au", user: "ec2-user", roles: %w{app web db}, :primary => true, :no_release => false

set :deploy_to, "#{fetch(:prod_deploy_path)}"

after  "deploy:finished",             "doctrine:migrations:migrate"
after  "doctrine:migrations:migrate", "sonata:media:fix_media_context"
after "sonata:media:fix_media_context", "deployment:oauth:config"