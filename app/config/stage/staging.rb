set :branch, proc { `git rev-parse --abbrev-ref origin/staging`.chomp }

server "staging.sculptform.com.au", user: "ec2-user", roles: %w{app web db}, primary: true, no_release: false

set :deploy_root, "/var/www/html" #project root path on server
set :deploy_to,   "#{fetch(:deploy_root)}/#{fetch(:deploy_dir)}"

after "deploy:finished",                "deployment:aws:restore"
after "deployment:aws:restore",     "files:restore_from_production"
after "files:restore_from_production",  "doctrine:migrations:migrate"
after "doctrine:migrations:migrate",    "sonata:media:fix_media_context"
after "deploy:symlink:release",         "deployment:extra:create_symlink"
after "doctrine:migrations:migrate",    "deployment:oauth:config"
