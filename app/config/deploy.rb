set :application,   "Woodform_API"
set :repo_url,      "git@git.sibers.com:sibers/woodform-api.git"
set :keep_releases, 3
set :deploy_dir,    "deploy"
set :branch,        "origin/master"

# Production server config
set :prod_deploy_path,      "/var/www/html"
server "api.sculptform.com.au", user: "ec2-user", roles: %w{prod}, :primary => false, :no_release => true

# Backup / Restore
set :timestamp,             Time.now.to_i # timestamp in seconds
set :backup_name,           "woodform-#{fetch(:timestamp)}"
set(:db_dump_file_path)     {"#{shared_path}/backup/#{fetch(:backup_name)}.sql"}
set(:files_backup_path)     {"#{shared_path}/backup/#{fetch(:backup_name)}.7z"}

# Symfony console commands will use this environment for execution
set :symfony_env,  "prod"

set :symfony_directory_structure, 3
set :sensio_distribution_version, 5

# symfony-standard edition directories
set :app_path,          "app"
set :web_path,          "web"
set :var_path,          "var"
set :bin_path,          "bin"
set :app_config_path,   "app/config"
set :log_path,          "var/logs"
set :cache_path,        "var/cache"

set :uploads_path,      "web/uploads"
set :media_temp_path,   "web/temp"
set :media_cache_path,  "web/cache"

set :symfony_console_path,  fetch(:bin_path) + "/console"
set :symfony_console_flags, "--no-debug --no-interaction --quiet"

# Remove app_dev.php during deployment, other files in web/ can be specified here
set :controllers_to_clear, ["app_*.php"]

# asset management
set :assets_install_path,   fetch(:web_path)
set :assets_install_flags,  "--symlink"

# Share files/directories between releases
append :linked_files, "app/config/parameters.yml"
append :linked_dirs, fetch(:log_path), fetch(:uploads_path)

set :permission_method,      :acl
set :use_set_permissions,    true
set :webserver_user,         "apache"
set :file_permissions_roles, :app
set :file_permissions_users, ["apache"]
set :file_permissions_paths, [fetch(:log_path), fetch(:cache_path), fetch(:uploads_path), fetch(:media_cache_path), fetch(:media_temp_path)]

set :format_options, log_file: "var/logs/capistrano.log"

set :composer_install_flags, '--prefer-dist --no-interaction --optimize-autoloader'

set :ssh_options, {
    forward_agent: false,
    auth_methods: %w(publickey password),
    compression: true
}

set :rsh, "ssh -o PasswordAuthentication=no -o StrictHostKeyChecking=no"

set :rsync_options, {
    source: '.',
    cache: 'cached-copy',
    args: {
        local_to_remote: %W(--rsh #{fetch(:rsh)} --compress --recursive --delete --exclude=.git* --delete-excluded),
        cache_to_release: %w(--archive)
    }
}

namespace :deployment do
    namespace :setup do
        task :upload_parameters do
            on roles(:app) do
                info "--> Uploading stage parameters.yml..."
                origin_file = "#{fetch(:app_config_path)}/parameters.yml.#{fetch(:stage)}"
                destination_file = "#{shared_path}/#{fetch(:app_config_path)}/parameters.yml"
                execute "mkdir -p #{shared_path}/#{fetch(:app_config_path)}"
                upload! origin_file, destination_file
            end
        end
    end

    namespace :memcached do
        task :flush do
            on roles(:app) do
                info "--> Flushing memcached local instance..."
                execute 'echo flush_all > /dev/tcp/localhost/11211'
            end
        end
    end

    namespace :oauth do
        task :config do
            on roles(:app) do
                info "--> Setting default oauth config..."
                invoke 'symfony:console', 'app:oauth:config'
            end
        end
    end

    namespace :extra do
        task :create_symlink do
            on roles(:app) do
                info "--> Creating symlink for web folder..."
                deploy_root = fetch(:deploy_root)
                release_path = fetch(:release_path)
                execute "rm -rf #{deploy_root}/web/api"
                execute "ln -s #{release_path}/web #{deploy_root}/web/api"
            end
        end
    end

    namespace :aws do
        task :restore do
            on roles(:app) do
                info "--> Restoring RDS DB..."
                invoke 'symfony:console', 'app:aws:clone_db'
            end
        end
    end
end

namespace :sonata do
    namespace :media do
        task :fix_media_context do
            on roles(:app) do
                info "--> Sonata - Media - Fixing media context..."
                invoke 'symfony:console', 'sonata:media:fix-media-context'
            end
        end
    end
end

namespace :doctrine do
    namespace :fixtures do
        task :load do
            on roles(:app) do
                info "--> Loading fixtures..."
                invoke 'symfony:console', 'doctrine:fixtures:load'
            end
        end
    end

    namespace :migrations do
        task :migrate do
            on roles(:app) do
                info "--> Migrating DB..."
                invoke 'symfony:console', 'doctrine:migrations:migrate'
            end
        end
    end

    namespace :schema do
        task :drop do
            on roles(:app) do
                info "--> Force drop full schema..."
                invoke 'symfony:console', 'doctrine:schema:drop', '--full-database --force'
            end
        end

        task :create do
            on roles(:app) do
                info "--> Creating schema..."
                invoke 'symfony:console', 'doctrine:schema:create'
            end
        end
    end
end

namespace :db do
    task :restore_from_production do
        on roles(:prod) do
            info "--> Restoring DB from production server..."

            on roles(:app) do
                execute "mkdir -p #{shared_path}/backup"
            end

            prod_db_dump_file_path = "#{fetch(:prod_deploy_path)}/shared/backup/#{fetch(:backup_name)}.sql"
            local_db_dump_file_path = "#{fetch(:backup_name)}.sql"

            execute "cd #{fetch(:prod_deploy_path)}/current && ./#{fetch(:symfony_console_path)} app:database:dump #{prod_db_dump_file_path}"
            execute "scp -C #{prod_db_dump_file_path} staging.sculptform.com.au:#{fetch(:db_dump_file_path)}"
            #download! prod_db_dump_file_path, local_db_dump_file_path
            execute "rm #{prod_db_dump_file_path}"

            on roles(:app) do
                #upload! local_db_dump_file_path, fetch(:db_dump_file_path)
                invoke "doctrine:schema:drop"
                invoke "symfony:console", "app:database:restore", fetch(:db_dump_file_path)
                execute "rm #{fetch(:db_dump_file_path)}*"
            end
        end
    end
end

namespace :files do
    task :restore_from_production do
        on roles(:app) do
            info "--> Restoring files from production server..."

            on roles(:prod) do
                execute "rsync -r #{fetch(:prod_deploy_path)}/shared/web ec2-user@staging.sculptform.com.au:#{shared_path}"
            end

            execute "cd #{shared_path}/web && sudo chown -R #{fetch(:user)}:#{fetch(:webserver_user)} *"
        end
    end
end

before "deploy:check",   "deployment:setup:upload_parameters"
before "composer:run",   "deployment:memcached:flush"
after  "deploy:updated", "deployment:memcached:flush"
