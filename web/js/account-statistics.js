$(function () {
    $(document).ready(function () {
        var getRandomColor = function () {
            var letters = '0123456789ABCDEF';
            var color = '#';
            for (var i = 0; i < 6; i++) {
                color += letters[Math.floor(Math.random() * 16)];
            }
            return color;
        };
        var ctx = document.getElementById("designChart").getContext("2d");
        var config = {
            "type": "line",
            "data": {
                "labels": [
                    'January',
                    'February',
                    'March',
                    'April',
                    'May',
                    'June',
                    'July',
                    'August',
                    'September',
                    'October',
                    'November',
                    'December'
                ],
                "datasets": []
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                title: {
                    display: true,
                    text: 'Sculptform Chart'
                },
                tooltips: {
                    mode: 'index',
                    intersect: false,
                },
                hover: {
                    mode: 'nearest',
                    intersect: true
                },
                scales: {
                    xAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Month'
                        }
                    }],
                    yAxes: [{
                        display: true,
                        scaleLabel: {
                            display: true,
                            labelString: 'Value',
                        }
                    }]
                }
            }
        };


        for (var property in charts) {
            if (charts.hasOwnProperty(property)) {
                if ($.grep(charts[property]['data']['counter'], function (v) {
                        return v > 0;
                    }).length) {
                    var color = getRandomColor();
                    config['data']['datasets'].push(
                        {
                            "label": charts[property]['title'],
                            "data": charts[property]['data']['counter'],
                            "fill": false,
                            "borderColor": color,
                            "backgroundColor": color,
                            "lineTension": 0.5
                        }
                    )
                }
            }
        }

        var designChart = new Chart(ctx, config);
        $('input').on('ifChecked ifUnchecked', function(event){
            var checkbox = $(event.target);
            var url = checkbox.attr('data-url');
            if(checkbox.prop('checked')){
                url+='?'+checkbox.attr('name')+'='+checkbox.prop('checked');
            }

            location.href = url;
        });
    });
});