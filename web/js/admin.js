jQuery(function () {
    jQuery(document).ready(function () {
        jQuery('.sonata-filter-form').on('submit', function () {
            jQuery(this).find('[sonata-filter="true"]:hidden :input').val('');
            jQuery(this).find('[sonata-filter="true"]:hidden :input:checkbox').removeAttr('checked');
        });
    });
});