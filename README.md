Symfony Standard Edition
========================

Installing project
------------------

1. go to work dir

    ```bash
    cd /path/to/workdir
    ```
2. clone project

    ```bash
    git clone git@git.sibers.com:sibers/woodform-api.git
    ```
3. go to project dir

    ```bash
    cd /path/to/project
    ```
4. [install composer globally or locally in project folder](http://symfony.com/doc/current/setup/composer.html)

5. [fix file permissions](http://symfony.com/doc/current/setup/file_permissions.html#using-acl-on-a-system-that-supports-setfacl-linux-bsd)

6. update composer

    ```bash
    php path/to/composer.phar update
    ```
7. create project database

    ```bash
    bin/console d:d:c
    ```
8. create project schema

    ```bash
    bin/console d:s:c
    ```
9. update project fixtures

    ```bash
    sudo -u www-data bin/console d:f:l -v -n
    ```
10. update symfony production cache

    ```bash
    bin/console c:c --env=prod
    ```

Updating project
----------------

1. go to project dir

    ```bash
    cd /path/to/project
    ```
2. update project

    ```bash
    git pull
    ```
3. clear memcache

    ```bash
    printf "flush_all" | nc localhost 11211
    ```
4. update composer

    ```bash
    php path/to/composer.phar update
    ```
5. update project schema

    ```bash
    bin/console d:s:u --force
    ```
6. update project fixtures

    ```bash
    sudo -u www-data bin/console d:f:l -v -n
    ```
7. update symfony production cache

    ```bash
    bin/console c:c --env=prod
    ```

Enjoy!