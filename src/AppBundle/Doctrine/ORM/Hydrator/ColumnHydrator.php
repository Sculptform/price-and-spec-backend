<?php

namespace AppBundle\Doctrine\ORM\Hydrator;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;

/**
 * Specifies that the fetch method shall return only a single requested column
 * from the next row in the result set.
 * 
 * @author Dmitry Bykov <dmitry.bykov@sibers.com>
 */
class ColumnHydrator extends AbstractHydrator
{
    const NAME = 'ColumnHydrator';

    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_COLUMN);
    }
}
