<?php

namespace AppBundle\Doctrine\ORM\Hydrator;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;

/**
 * Create an associative list style array of database results
 * 
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ListHydrator extends AbstractHydrator
{
    const NAME = 'ListHydrator';

    protected function hydrateAllData()
    {
        $result = [];

        foreach($this->_stmt->fetchAll(\PDO::FETCH_ASSOC) as $row) {
            $this->hydrateRowData($row, $result);
        }

        return $result;
    }

    protected function hydrateRowData(array $row, array &$result)
    {
        if(count($row) !== 0) {
            $data = [];

            foreach ($row as $key => $value) {
                $scalarRowData = $this->gatherScalarRowData($row);

                $columnInfo = (array)$this->hydrateColumnInfo($key);

                $fieldName = &$columnInfo['fieldName'];

                $isIdentifier = &$columnInfo['isIdentifier'];

                $alias = &$columnInfo['dqlAlias'];

                $scalarValue = $scalarRowData[$alias ? $alias . '_' . $fieldName : $fieldName];

                if ($fieldName === 'id' || $isIdentifier === true) {
                    $id = $scalarValue;
                }

                $data[$fieldName] = $scalarValue;
            }

            if(isset($id)){
                $result[$id] = $data;
            } else {
                $result[] = $data;
            }
        }
    }
}
