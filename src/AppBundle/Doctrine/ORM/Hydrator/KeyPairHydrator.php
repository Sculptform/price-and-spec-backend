<?php

namespace AppBundle\Doctrine\ORM\Hydrator;

use Doctrine\ORM\Internal\Hydration\AbstractHydrator;

/**
 * Fetch a two-column result into an array
 * where the first column is a key
 * and the second column is the value
 *
 * @author Dmitry Bykov <dmitry.bykov@sibers.com>
 */
class KeyPairHydrator extends AbstractHydrator
{
    const NAME = 'KeyPairHydrator';

    /**
     * {@inheritdoc}
     */
    protected function hydrateAllData()
    {
        return $this->_stmt->fetchAll(\PDO::FETCH_KEY_PAIR);
    }
}
