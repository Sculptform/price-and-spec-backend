<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 09.02.17
 * Time: 18:02
 */

namespace AppBundle\EntityListener;

use AppBundle\Entity\Comment\DiscussionComment;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Mapping\PreFlush;

class DiscussionCommentListener
{
    /**
     * @PreFlush
     * @param DiscussionComment $comment
     * @param PreFlushEventArgs $event
     */
    public function preFlushHandler(DiscussionComment $comment, PreFlushEventArgs $event)
    {
        $em = $event->getEntityManager();

        $maxDiscussionNumbers = $em->getRepository(DiscussionComment::class)->getMaxDiscussionNumbers();

        $entities = $em->getUnitOfWork()->getScheduledEntityInsertions();

        /** @var DiscussionComment[] $discussionComments */
        $discussionComments = [];

        foreach ($entities as $entity) {
            if ($entity instanceof $comment) {
                $discussionComments[$entity->getCreatedAt()->getTimestamp()] = $entity;
            }
        }

        ksort($discussionComments);

        foreach ($discussionComments as $discussionComment) {
            $maxDiscussionNumber = &$maxDiscussionNumbers[$discussionComment->getDiscussion()->getId()];
            $maxDiscussionNumber++;
            $discussionComment->setNumber($maxDiscussionNumber);
        }
    }
}