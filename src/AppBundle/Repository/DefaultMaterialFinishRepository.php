<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * Default MaterialFinishRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class DefaultMaterialFinishRepository extends EntityRepository
{
    /**
     * @param array $params
     * @param bool $returnQuery
     * @return array|\Doctrine\ORM\Query
     */
    public function findAllByParams(array $params, $returnQuery = false){
        $params = new ParameterBag($params);

        $materialType = $params->getInt('materialType');
        $productType = $params->getInt('productType');

        $qb = $this->createQueryBuilder('materialFinish');

        if($materialType){
            $qb
                ->andWhere('materialFinish.materialType = :materialType')
                ->setParameter('materialType', $materialType);
        }

        if($productType){
            $qb
                ->andWhere('materialFinish.productType = :productType')
                ->setParameter('productType', $productType);
        }

        return $returnQuery ? $qb->getQuery() : $qb->getQuery()->getResult();
    }
}
