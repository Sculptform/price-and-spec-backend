<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * Object3DRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class Object3DRepository extends EntityRepository
{
}
