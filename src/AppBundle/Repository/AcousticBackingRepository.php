<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * AcousticBackingRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class AcousticBackingRepository extends EntityRepository
{
    /**
     * @param array $params
     *
     * @return mixed
     */
    public function findAllByParams(array $params){
        $params = new ParameterBag($params);

        $productType     = $params->getInt('product_type');

        $qb = $this->createQueryBuilder('ab');
        if ($productType) {
            $qb->andWhere('ab.productType = :type')->setParameter('type', $productType);
        }
        $qb->andWhere('ab.title != :title')->setParameter('title', 'Group 3 Rated - Standard');

        $qb->addOrderBy('ab.price', 'asc');

        return $qb->getQuery()->getResult();
    }
}
