<?php

namespace AppBundle\Repository;

use AppBundle\Entity\User;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

/**
 * QuotationRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class QuotationRepository extends EntityRepository
{
    /**
     * @param array $params
     * @param bool $returnQuery
     * @return array|\Doctrine\ORM\Query
     */
    public function findAllByParams(array $params, $returnQuery = false)
    {
        $params = new ParameterBag($params);

        $user = $params->getInt('user');
        $project = $params->getInt('project');

        $qb = $this->createQueryBuilder('quotation');

        if ($user || $project) {

            if ($user && !$project) {
                $qb->where('quotation.manager = :user')->setParameter('user', $user);
            }

            if ($user && $project) {
                $qb->where('quotation.project = :project')
                    ->andWhere('quotation.manager = :user')
                    ->setParameter('project', $project)
                    ->setParameter('user', $user);
            } elseif ($project && !$user) {
                $qb->where('quotation.project = :project')->setParameter('project', $project);
            }
        }

        return $returnQuery ? $qb->getQuery() : $qb->getQuery()->getResult();
    }

    public function countAll(User $user = null)
    {
        $qb = $this->createQueryBuilder('quotation');

        $qb->select('COUNT(quotation)');

        if (null !== $user) {
            $qb->andWhere('quotation.user = :user')->setParameter('user', $user);
        }

        return (int)$qb->getQuery()->getSingleScalarResult();
    }
}
