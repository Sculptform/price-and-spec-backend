<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

class RailingFinishRepository extends EntityRepository
{
    /**
     * @param array $params
     * @param bool $returnQuery
     * @return array|\Doctrine\ORM\Query
     */
    public function findAllByParams(array $params, $returnQuery = false){
        $params = new ParameterBag($params);

        $materialType = $params->getInt('materialType');
        $applicationType = $params->getInt('applicationType');

        $qb = $this->createQueryBuilder('materialFinish');

        if($materialType){
            $qb
                ->andWhere('materialFinish.materialType = :materialType')
                ->setParameter('materialType', $materialType);
        }

        if($applicationType){
            $qb
                ->andWhere('materialFinish.applicationTypes like :applicationType')
                ->setParameter('applicationType', '%'.$applicationType.'%');
        }

        return $returnQuery ? $qb->getQuery() : $qb->getQuery()->getResult();
    }

}