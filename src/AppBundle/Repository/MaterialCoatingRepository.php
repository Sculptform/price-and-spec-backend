<?php
/**
 * Class MaterialCoatingRepository
 * @package AppBundle\Repository
 * @author Andrey Zaytsev <dreyup96@gmail.com>
 */


namespace AppBundle\Repository;

use AppBundle\Enum\ApplicationType;
use Doctrine\ORM\EntityRepository;
use Symfony\Component\HttpFoundation\ParameterBag;

class MaterialCoatingRepository extends EntityRepository
{
    /**
     * @param array $params
     * @return array
     */
    public function findAllByParams(array $params){
        $params = new ParameterBag($params);

        $applicationType = $params->getInt('application_type');

        $qb = $this->createQueryBuilder('materialCoating');

        if (in_array($applicationType, ApplicationType::toArray())) {
            $qb
                ->andWhere('materialCoating.applicationTypes like :applicationType')
                ->setParameter('applicationType', '%'.$applicationType.'%');
        }

        return $qb->getQuery()->getResult();
    }
}