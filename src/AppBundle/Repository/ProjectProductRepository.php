<?php

namespace AppBundle\Repository;

use Doctrine\ORM\EntityRepository;

/**
 * ProjectMaterialRepository
 *
 * This class was generated by the Doctrine ORM. Add your own custom
 * repository methods below.
 */
class ProjectProductRepository extends EntityRepository
{
    public function getMostUsedShape(){
        return $this->createQueryBuilder('projectProduct')
            ->join('projectProduct.product', 'product')
            ->join('product.material', 'material')
            ->join('material.materialShapeSize', 'materialShapeSize')
            ->join('materialShapeSize.materialShape', 'materialShape')
            ->select('materialShape.id, COUNT(materialShape.id) as uses')
            ->groupBy('materialShape.id')
            ->orderBy('uses', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getMostUsedFinish(){
        return $this->createQueryBuilder('projectProduct')
            ->join('projectProduct.product', 'product')
            ->join('product.materialFinish', 'materialFinish')
            ->select('materialFinish.id, COUNT(materialFinish.id) as uses')
            ->where('materialFinish.texturePath is not null')
            ->groupBy('materialFinish.id')
            ->orderBy('uses', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }

    public function getMostUsedColor(){
        return $this->createQueryBuilder('projectProduct')
            ->join('projectProduct.product', 'product')
            ->join('product.materialFinish', 'materialFinish')
            ->select('materialFinish.id, COUNT(materialFinish.id) as uses')
            ->where('materialFinish.color is not null')
            ->groupBy('materialFinish.id')
            ->orderBy('uses', 'DESC')
            ->setMaxResults(1)
            ->getQuery()
            ->getOneOrNullResult();
    }
}
