<?php

namespace AppBundle\Exception;

use Symfony\Component\HttpFoundation\Response;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ConflictException extends \RuntimeException implements ApiErrorException
{
    /**
     * @var array
     */
    protected $errors = [];

    /**
     * ConflictException constructor.
     * @param string|array $errors
     * @param \Throwable $previous
     */
    public function __construct($errors, \Throwable $previous = null)
    {
        parent::__construct('Conflict', Response::HTTP_CONFLICT, $previous);

        $this->errors = array_map(function ($error){ return ['detail' => $error];}, (array) $errors);
    }

    /**
     * @return array
     */
    public final function getErrors()
    {
        return $this->errors;
    }
}
