<?php

namespace AppBundle\Exception;

/**
 * Base bundle exception.
 *
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
interface ApiErrorException
{
    public function getErrors();
}
