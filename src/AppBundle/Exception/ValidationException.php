<?php

namespace AppBundle\Exception;

use Doctrine\Common\Inflector\Inflector;
use Symfony\Component\Form\Form;
use Symfony\Component\Form\FormErrorIterator;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Validator\ConstraintViolation;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\ConstraintViolationListInterface;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ValidationException extends \RuntimeException implements ApiErrorException
{
    /**
     * @var array
     */
    private $errors;

    /**
     * ValidationException constructor.
     * @param ConstraintViolationListInterface|FormErrorIterator $errors
     * @param \Throwable $previous
     */
    public function __construct($errors, \Throwable $previous = null)
    {
        parent::__construct('Validation failed', Response::HTTP_BAD_REQUEST, $previous);

        $violations = new ConstraintViolationList();

        if ($errors instanceof FormErrorIterator) {
            foreach ($errors as $error) {
                $cause = $error->getCause();

                if ($cause instanceof ConstraintViolation) {
                    $violations->add($cause);
                }
            }
        }

        if($errors instanceof ConstraintViolationListInterface) {
            $violations = $errors;
        }

        $errorMessages = [];

        foreach ($violations as $violation) {
            $root = $violation->getRoot();

            if ($root instanceof Form) {
                $propertyPath = preg_replace (
                    '/^(data|children)/',
                    $violation->getRoot()->getName(),
                    $violation->getPropertyPath(),
                    1
                );

                $propertyPath = preg_replace (
                    '/\[/',
                    '.',
                    $propertyPath
                );

                $propertyPath = preg_replace (
                    '/\]/',
                    '',
                    $propertyPath
                );

                $propertyPath = Inflector::tableize($propertyPath);
            } else {
                $ref = new \ReflectionClass($root);
                $propertyPath = Inflector::tableize($ref->getShortName()) . '_' . Inflector::tableize($violation->getPropertyPath());
            }

            $message = $violation->getMessage();

            if (!isset($errorMessages[$propertyPath])) {
                $errorMessages[$propertyPath] = [];
            }

            $errorMessages[$propertyPath][] = $message;
        }

        $this->errors = $errorMessages;
    }

    /**
     * @return array
     */
    public function getErrors()
    {
        return $this->errors;
    }
}
