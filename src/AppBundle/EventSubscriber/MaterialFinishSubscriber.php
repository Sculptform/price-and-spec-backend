<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Scene;
use Doctrine\Common\EventSubscriber;
use Doctrine\Common\Persistence\Event\LifecycleEventArgs;
use Doctrine\ORM\EntityManager;
use Ramsey\Uuid\Uuid;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class MaterialFinishSubscriber implements EventSubscriber, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function getSubscribedEvents()
    {
        return array(
            'preRemove',
            'postUpdate',
        );
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!($object instanceof MaterialFinish)) {
            return;
        }

        gc_enable();
        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if (!$request) {
            return;
        }

        $hostUrl = $request->getScheme() . '://' . $request->getHttpHost();

        /** @var EntityManager $em */
        $em = $args->getObjectManager();
        $uow = $em->getUnitOfWork();
        $changeSet = $uow->getEntityChangeSet($object);
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('s')
            ->getQuery();

        $oldTexturePath = $newTexturePath = null;

        if (array_key_exists('texturePath', $changeSet) || array_key_exists('color', $changeSet)) {
            if (array_key_exists('texturePath', $changeSet)) {
                list($oldTexturePath, $newTexturePath) = $changeSet['texturePath'];
            }

            foreach ($scenesQuery->iterate() as $sceneEntity) {
                /** @var Scene $sceneEntity */
                $sceneEntity = $sceneEntity[0];
                $sceneData = $sceneEntity->getScene();

                $scene = &$this->getSubArray($sceneData, 'scene');
                $sceneObject = &$this->getSubArray($scene, 'object');
                $sceneChildren = &$this->getSubArray($sceneObject, 'children');
                $sceneMaterials = &$this->getSubArray($scene, 'materials');
                $sceneTextures = &$this->getSubArray($scene, 'textures');
                $sceneImages = &$this->getSubArray($scene, 'images');

                $sceneChildren = is_array($sceneChildren) ? $sceneChildren : [];

                foreach ($sceneChildren as &$sceneChild) {
                    $product = null;

                    if (array_key_exists('productModel', $sceneChild)) {
                        $product = $em->find(Product::class, $sceneChild['productModel']);
                    }

                    if (
                        (
                            array_key_exists('materialFinishModel', $sceneChild) &&
                            $sceneChild['materialFinishModel'] == $object->getId()
                        ) ||
                        (
                            $product && $product->getMaterialFinishId() == $object->getId()
                        )
                    ) {
                        if (array_key_exists('material', $sceneChild)) {
                            $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                            if (false !== $materialKey) {
                                $material = &$sceneMaterials[$materialKey];

                                if ($oldTexturePath) {
                                    if (array_key_exists('map', $material)) {
                                        $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                        if (false !== $sceneTextureKey) {
                                            $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                            if (array_key_exists('image', $sceneTexture)) {
                                                $sceneImageKey = $this->findKey(
                                                    $sceneImages,
                                                    'uuid',
                                                    $sceneTexture['image']
                                                );

                                                if (false !== $sceneImageKey) {
                                                    if ($newTexturePath) {
                                                        $sceneImages[$sceneImageKey]['url'] = $hostUrl . '/' . $object->getTextureWebPath(
                                                            );
                                                    } else {
                                                        unset(
                                                            $sceneImages[$sceneImageKey],
                                                            $sceneTextures[$sceneTextureKey],
                                                            $material['map']
                                                        );
                                                    }
                                                }
                                            }

                                            unset($sceneTexture);
                                        }
                                    }
                                } elseif ($newTexturePath) {
                                    $sceneImageUuid = Uuid::uuid4()->toString();

                                    $sceneImage = [
                                        'uuid' => $sceneImageUuid,
                                        'url' => $hostUrl . '/' . $object->getTextureWebPath()
                                    ];

                                    $sceneImages[] = $sceneImage;

                                    $textureUuid = Uuid::uuid4()->toString();

                                    $texture = [
                                        'uuid' => $textureUuid,
                                        'name' => '',
                                        'mapping' => 300,
                                        'repeat' => [1, 1],
                                        'offset' => [0, 0],
                                        'wrap' => [1001, 1001],
                                        'minFilter' => 1008,
                                        'magFilter' => 1006,
                                        'anisotropy' => 1,
                                        'image' => $sceneImageUuid
                                    ];

                                    $sceneTextures[] = $texture;

                                    $material['map'] = $textureUuid;
                                }

                                $material['color'] = $object->getDecimalColor();

                                unset($material);
                            }
                        }
                    }
                    gc_collect_cycles();
                }

                $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);
                $sceneEntity->setScene($sceneData);
                $em->flush();
                $em->clear();

                unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);

                gc_collect_cycles();
            }
        }
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function preRemove(LifecycleEventArgs $args)
    {
        $request = $this->container->get('request_stack')->getCurrentRequest();

        if (!$request) {
            return;
        }

        $object = $args->getObject();

        if ($object instanceof MaterialFinish) {

            gc_enable();
            ini_set('memory_limit', '-1');
            ini_set('max_execution_time', '-1');

            $projectProductIds = $productIds = [];

            /** @var EntityManager $em */
            $em = $args->getObjectManager();
            $em->getConnection()->getConfiguration()->setSQLLogger(null);

            $scenesQuery = $em->getRepository(Scene::class)
                ->createQueryBuilder('s')
                ->getQuery();

            foreach ($scenesQuery->iterate() as $sceneEntity) {
                /** @var Scene $sceneEntity */
                $sceneEntity = $sceneEntity[0];
                $sceneData = $sceneEntity->getScene();

                $scene = &$this->getSubArray($sceneData, 'scene');
                $sceneObject = &$this->getSubArray($scene, 'object');
                $sceneChildren = &$this->getSubArray($sceneObject, 'children');
                $sceneMaterials = &$this->getSubArray($scene, 'materials');
                $sceneTextures = &$this->getSubArray($scene, 'textures');
                $sceneImages = &$this->getSubArray($scene, 'images');

                $sceneChildren = is_array($sceneChildren) ? $sceneChildren : [];

                foreach ($sceneChildren as &$sceneChild) {
                    $product = null;

                    if (array_key_exists('productModel', $sceneChild)) {
                        $product = $em->find(Product::class, $sceneChild['productModel']);
                    }

                    if (
                        (
                            array_key_exists('materialFinishModel', $sceneChild) &&
                            $sceneChild['materialFinishModel'] == $object->getId()
                        ) ||
                        (
                            $product && $product->getMaterialFinishId() == $object->getId()
                        )
                    ) {
                        unset($sceneChild['materialFinishModel']);

                        if (array_key_exists('productModel', $sceneChild)) {
                            unset($sceneChild['productModel']);
                        }

                        if (array_key_exists('material', $sceneChild)) {
                            $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                            if (false !== $materialKey) {
                                $material = &$sceneMaterials[$materialKey];

                                    if (array_key_exists('map', $material)) {
                                        $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                        if (false !== $sceneTextureKey) {
                                            $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                            if (array_key_exists('image', $sceneTexture)) {
                                                $sceneImageKey = $this->findKey(
                                                    $sceneImages,
                                                    'uuid',
                                                    $sceneTexture['image']
                                                );

                                                if (false !== $sceneImageKey) {
                                                    $material['color'] = $object->getDecimalColor(true);
                                                    unset($sceneImages[$sceneImageKey], $sceneTextures[$sceneTextureKey], $material['map']);
                                                }
                                            }
                                            unset($sceneTexture);
                                        }
                                    }
                                unset($material);
                            }
                        }
                    }
                    gc_collect_cycles();
                }

                $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);

                $sceneEntity->setScene($sceneData);
                $em->flush();
                //$em->clear(Scene::class);
                unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);
                gc_collect_cycles();

                /** @var ProjectProduct $projectProduct */
                foreach ($sceneEntity->getProject()->getProjectProducts() as $projectProduct) {
                    if ($projectProduct->getFinishId() === $object->getId()) {
                        $projectProductIds[] = $object->getId();
                        $productIds[] = $projectProduct->getProductId();
                    }
                }
            }

            $em->getRepository(ProjectProduct::class)->createQueryBuilder('pp')
                ->delete()->where('pp.id IN (:ids)')->setParameter('ids', $projectProductIds)
                ->getQuery()
                ->execute()
            ;

            $em->getRepository(Product::class)->createQueryBuilder('p')
                ->delete()->where('p.id IN (:ids)')->setParameter('ids', $productIds)
                ->getQuery()
                ->execute()
            ;

            gc_collect_cycles();
        }
    }

    private function &getSubArray(&$array, $key)
    {
        if (!array_key_exists($key, $array)) {
            $array[$key] = [];
        }

        return $array[$key];
    }

    private function findKey($array, $searchKey, $searchValue)
    {
        $key = array_search($searchValue, array_column($array, $searchKey));

        return $key === false ? false : array_keys($array)[$key];
    }
}