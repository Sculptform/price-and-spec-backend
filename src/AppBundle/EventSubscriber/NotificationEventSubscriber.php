<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Notification\DiscussionNotification;
use AppBundle\Entity\Notification\FeedNotification;
use AppBundle\Entity\Notification\FollowerDiscussionNotification;
use AppBundle\Entity\Notification\GalleryProjectNotification;
use AppBundle\Entity\Notification\NewsNotification;
use AppBundle\Entity\Notification\ProductSandpitNotification;
use AppBundle\Entity\Notification\ProjectNotification;
use AppBundle\Entity\Notification\ShareNotification;
use AppBundle\Entity\Notification\TeamMembersNotification;
use AppBundle\Entity\Notification\TeamUserNotification;
use AppBundle\Entity\Notification\UserFollowerNotification;
use AppBundle\Entity\User;
use AppBundle\Event\DiscussionEvent;
use AppBundle\Event\Events;
use AppBundle\Event\FeedEvent;
use AppBundle\Event\FollowerDiscussionEvent;
use AppBundle\Event\GalleryProjectEvent;
use AppBundle\Event\NewsEvent;
use AppBundle\Event\ProductSandpitEvent;
use AppBundle\Event\ProjectEvent;
use AppBundle\Event\SampleRequestEvent;
use AppBundle\Event\TeamMembersEvent;
use AppBundle\Event\TeamUserEvent;
use AppBundle\Event\UserFollowerEvent;
use AppBundle\Event\UserProjectEvent;
use AppBundle\Event\QuotationEvent;
use AppBundle\Repository\UserRepository;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

class NotificationEventSubscriber implements EventSubscriberInterface
{
    /** @var ContainerInterface */
    private $container;

    /** @var EntityManagerInterface */
    private $em;

    /**
     * NotificationEventSubscriber constructor.
     *
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
        $this->em        = $container->get('doctrine.orm.default_entity_manager');
    }

    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::NEWS_CREATED                         => 'onNewsCreatedEventReceived',
            Events::PROJECT_SHARED                       => 'onProjectSharedEventReceived',
            Events::QUOTATION_RECEIVED                   => 'onQuotationReceivedEventReceived',
            Events::SAMPLE_REQUEST_RECEIVED              => 'onSampleRequestEventReceived',
            Events::USER_FOLLOWED                        => 'onUserFollowedEventReceived',
            Events::TEAM_INVITED                         => 'onTeamUserEventReceived',
            Events::TEAM_REQUEST_TO_JOIN                 => 'onTeamUserEventReceived',
            Events::FEED_LIKED                           => 'onFeedEventReceived',
            Events::FEED_COMMENTED                       => 'onFeedEventReceived',
            Events::FEED_SHARED                          => 'onFeedEventReceived',
            Events::PROJECT_RATED                        => 'onProjectEventReceived',
            Events::PROJECT_COMMENTED                    => 'onProjectEventReceived',
            Events::PROJECT_SHARED                       => 'onProjectEventReceived',
            Events::GALLERY_PROJECT_COMMENTED            => 'onGalleryProjectEventReceived',
            Events::GALLERY_PROJECT_SHARED               => 'onGalleryProjectEventReceived',
            Events::TEAM_MEMBERS_PROJECT_CREATED         => 'onTeamMembersEventReceived',
            Events::TEAM_MEMBERS_GALLERY_PROJECT_CREATED => 'onTeamMembersEventReceived',
            Events::TEAM_MEMBERS_DISCUSSION_CREATED      => 'onTeamMembersEventReceived',
            Events::DISCUSSION_SET_SPAMMED               => 'onDiscussionEventReceived',
            Events::DISCUSSION_SHARED                    => 'onDiscussionEventReceived',
            Events::DISCUSSION_COMMENTED                 => 'onDiscussionEventReceived',
            Events::PRODUCT_SANDPIT_CREATED              => 'onProductSandpitEventReceived',
            Events::FOLLOWER_DISCUSSION_CREATED          => 'onFollowerDiscussionCreatedEventReceived',

        ];
    }

    /**
     * @param NewsEvent $event
     */
    public function onNewsCreatedEventReceived(NewsEvent $event)
    {
        $news = $event->getNews();

        $this->em->getRepository(NewsNotification::class)->generateNotifications($news);

        /** @var UserRepository $userRepository */
        $userRepository = $this->em->getRepository('AppBundle:User');

        $emailSubscribedUsers = $userRepository->findBy(['email_news_notifications' => true]);

        /** @var User $emailSubscribedUser */
        foreach ($emailSubscribedUsers as $emailSubscribedUser) {
            /** @var \Swift_Message $message */
            $message = $this->get('mailer')->createMessage();

            $message
                ->setSubject('News')
                ->setFrom(
                    $this->getParameter('email.from.address'),
                    $this->getParameter('email.from.sender_name')
                )
                ->setTo($emailSubscribedUser->getEmail(), $emailSubscribedUser->getFullName())
                ->setBody(
                    $this->get('templating')->render(
                        'AppBundle:Email/Notification:news.html.twig',
                        [
                            'news' => $news,
                            'user' => $emailSubscribedUser,
                        ]
                    ),
                    'text/html'
                );

            $this->get('mailer')->send($message);
        }
    }

    /**
     * @param UserProjectEvent $event
     */
    public function onProjectSharedEventReceived(UserProjectEvent $event)
    {
        $userProject = $event->getUserProject();

        $this->em->getRepository(ShareNotification::class)->generateNotification($userProject);

        $user = $userProject->getUser();

        if ($user->getEmailShareNotifications()) {
            /** @var \Swift_Message $message */
            $message = $this->get('mailer')->createMessage();

            $body = $this->get('templating')->render(
                'AppBundle:Email/Notification:share.html.twig',
                [
                    'project' => $userProject->getProject(),
                    'user'    => $user,
                ]
            );

            $message
                ->setSubject('Project shared')
                ->setFrom(
                    $this->getParameter('email.from.address'),
                    $this->getParameter('email.from.sender_name')
                )
                ->setTo($user->getEmail(), $user->getFullName())
                ->setBody($body, 'text/html');

            $this->get('mailer')->send($message);
        }
    }

    /**
     * @param QuotationEvent $event
     */
    public function onQuotationReceivedEventReceived(QuotationEvent $event)
    {
        $quotation = $event->getQuotation();

        $user = $quotation->getUser();

        $body = $this->get('templating')->render(
            'AppBundle:Email/Notification:quotation.html.twig',
            [
                'quotation' => $quotation,
                'user'      => $user,
            ]
        );

        $subject = sprintf(
            'Order request "%s" was received [%s]',
            $quotation->getTitle(),
            $quotation->getId()
        );

        $to = (array)$this->getParameter('email.notification.quotation.to');
        $cc = (array)$this->getParameter('email.notification.quotation.cc');

        $userEmail      = $user->getEmail();
        $quotationEmail = $quotation->getEmail();

        $cc[] = $userEmail;

        if ($userEmail !== $quotationEmail) {
            $cc[] = $quotationEmail;
        }

        /** @var \Swift_Message $message */
        $message = $this->get('mailer')->createMessage();

        $message
            ->setSubject($subject)
            ->setFrom(
                $this->getParameter('email.noreply'),
                $this->getParameter('email.from.sender_name')
            )
            ->setTo($to)
            ->setCc($cc)
            ->setBody($body, 'text/html');

        if ($quotation->getAttachmentAbsolutePath()) {
            $message->attach(\Swift_Attachment::fromPath($quotation->getAttachmentAbsolutePath()));
        }

        $project = $quotation->getProject();

        if ($project && $project->getImagePath()) {
            $message->attach(\Swift_Attachment::fromPath($project->getImageAbsolutePath()));
        }

        try {
            $this->get('mailer')->send($message);
        } catch (\Exception $e) {
        }
    }

    /**
     * @param SampleRequestEvent $event
     */
    public function onSampleRequestEventReceived(SampleRequestEvent $event)
    {
        $sampleRequest = $event->getSampleRequest();

        $user = $sampleRequest->getUser();

        $body = $this->get('templating')->render(
            'AppBundle:Email/Notification:sample_request.html.twig',
            [
                'sampleRequest' => $sampleRequest,
                'user'          => $user,
            ]
        );

        $subject = sprintf(
            'Sample request "%s" was received [%s]',
            $sampleRequest->getProjectName(),
            $sampleRequest->getId()
        );

        $to = (array)$this->getParameter('email.notification.sample_request.to');
        $cc = (array)$this->getParameter('email.notification.sample_request.cc');

        $userEmail          = $user->getEmail();
        // $sampleRequestEmail = $sampleRequest->getEmail();

        $cc[] = $userEmail;

        // if ($userEmail !== $sampleRequestEmail) {
        //     $cc[] = $sampleRequestEmail;
        // }

        /** @var \Swift_Message $message */
        $message = $this->get('mailer')->createMessage();

        $message
            ->setSubject($subject)
            ->setFrom(
                $this->getParameter('email.noreply'),
                $this->getParameter('email.from.sender_name')
            )
            ->setTo($to)
            ->setCc($cc)
            ->setBody($body, 'text/html');

        $project = $sampleRequest->getProject();

        if ($project && $project->getImagePath()) {
            $message->attach(\Swift_Attachment::fromPath($project->getImageAbsolutePath()));
        }

        try {
            $this->get('mailer')->send($message);
        } catch (\Exception $e) { }
    }

    /**
     * @param UserFollowerEvent $event
     */
    public function onUserFollowedEventReceived(UserFollowerEvent $event)
    {
        $userFollower = $event->getUserFollower();

        $this->em->getRepository(UserFollowerNotification::class)
                 ->generateNotification($userFollower);
    }

    /**
     * @param TeamUserEvent $event
     */
    public function onTeamUserEventReceived(TeamUserEvent $event)
    {
        $teamUser  = $event->getTeamUser();
        $eventType = $event->getEventType();
        $eventUser = $event->getEventUser();

        $this->em->getRepository(TeamUserNotification::class)
                 ->generateNotification($teamUser, $eventUser, $eventType);
    }

    /**
     * @param FeedEvent $event
     */
    public function onFeedEventReceived(FeedEvent $event)
    {
        $feed      = $event->getFeed();
        $eventUser = $event->getEventUser();
        $eventType = $event->getEventType();

        $this->em->getRepository(FeedNotification::class)
                 ->generateNotification($feed, $eventUser, $eventType);
    }

    /**
     * @param ProjectEvent $event
     */
    public function onProjectEventReceived(ProjectEvent $event)
    {
        $project   = $event->getProject();
        $owner     = $event->getProjectOwner();
        $user      = $event->getEventUser();
        $eventType = $event->getEventType();

        $this->em->getRepository(ProjectNotification::class)
                 ->generateNotification($project, $user, $owner, $eventType);
    }

    /**
     * @param GalleryProjectEvent $event
     */
    public function onGalleryProjectEventReceived(GalleryProjectEvent $event)
    {
        $galleryProject = $event->getGalleryProject();
        $owner          = $event->getGalleryProjectOwner();
        $user           = $event->getEventUser();
        $eventType      = $event->getEventType();

        $this->em->getRepository(GalleryProjectNotification::class)
                 ->generateNotification($galleryProject, $user, $owner, $eventType);
    }

    /**
     * @param TeamMembersEvent $event
     */
    public function onTeamMembersEventReceived(TeamMembersEvent $event)
    {
        $team      = $event->getTeam();
        $user      = $event->getUser();
        $eventUser = $event->getEventUser();
        $eventType = $event->getEventType();

        $this->em->getRepository(TeamMembersNotification::class)
                 ->generateNotification($team, $user, $eventUser, $eventType);
    }


    /**
     * @param DiscussionEvent $event
     */
    public function onDiscussionEventReceived(DiscussionEvent $event)
    {
        $discussion = $event->getDiscussion();
        $eventUser  = $event->getEventUser();
        $eventType  = $event->getEventType();
        $user       = $event->getDiscussionUser();

        $this->em->getRepository(DiscussionNotification::class)
                 ->generateNotification($discussion, $eventUser, $eventType, $user);
    }

    /**
     * @param FollowerDiscussionEvent $event
     */
    public function onFollowerDiscussionCreatedEventReceived(FollowerDiscussionEvent $event)
    {
        $discussion = $event->getDiscussion();
        $eventUser  = $event->getEventUser();
        $user       = $event->getDiscussionUser();

        $this->em->getRepository(FollowerDiscussionNotification::class)
                 ->generateNotification($discussion, $eventUser, $user);
    }

    /**
     * @param ProductSandpitEvent $event
     */
    public function onProductSandpitEventReceived(ProductSandpitEvent $event)
    {
        $productSandpit = $event->getProductSandpit();
        $user           = $event->getUser();

        $this->em->getRepository(ProductSandpitNotification::class)
                 ->generateNotification($productSandpit, $user);
    }

    /**
     * @param string $id
     *
     * @return object
     */
    protected function get($id)
    {
        return $this->container->get($id);
    }

    /**
     * @param string $parameter
     *
     * @return mixed
     */
    protected function getParameter($parameter)
    {
        return $this->container->getParameter($parameter);
    }
}
