<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 23.07.17
 * Time: 17:53
 */

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\User;
use Doctrine\Common\EventSubscriber;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Gregwar\ImageBundle\Services\ImageHandling;
use Symfony\Component\DependencyInjection\Container;

class UserEventSubscriber implements EventSubscriber
{
    /**
     * @var Container
     */
    private $container;

    /**
     * UserEventSubscriber constructor.
     * @param Container $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    /**
     * @return array
     */
    public function getSubscribedEvents()
    {
        return [
            'postPersist',
            'postUpdate',
        ];
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $this->generateUserAvatarMultipleSizes($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postPersist(LifecycleEventArgs $args)
    {
        $this->generateUserAvatarMultipleSizes($args);
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function generateUserAvatarMultipleSizes(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if ($object instanceof User) {
            if ($object->getImagePath()) {
                /** @var ImageHandling $imageService */
                $imageService = $this->container->get('image.handling');

                $sizes = [20, 50, 100, 150]; //TODO: Put this into configuration file

                foreach ($sizes as $size) {
                    $imageService->open($object->getImageWebPath())
                        ->zoomCrop($size, $size, 'transparent', 'center', 'top')
                        ->save($object->getImageWebPath($size));
                }
            }
        }
    }
}