<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Exception\ApiErrorException;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpKernel\Event\GetResponseForExceptionEvent;
use Symfony\Component\HttpKernel\KernelEvents;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ApiErrorExceptionSubscriber implements EventSubscriberInterface
{
    /**
     * {@inheritdoc}
     */
    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::EXCEPTION => 'onException',
        ];
    }
    
    /**
     * @param GetResponseForExceptionEvent $event
     */
    public function onException(GetResponseForExceptionEvent $event)
    {
        $e = $event->getException();
        
        if (!$e instanceof ApiErrorException) {
            return ;
        }

        $exception = [
            'message' => $e->getMessage(),
            'code' => $e->getCode(),
            'errors' => $e->getErrors()
        ];

        $response = new JsonResponse($exception, $e->getCode());

        $event->setResponse($response);
    }
}
