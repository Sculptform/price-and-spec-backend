<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Enum\SampleType;
use AppBundle\Service\SalesForce\LeadCommand;
use AppBundle\Service\SalesForce\PsDownloadCommand;
use Exception;
use AppBundle\Event\Events;
use Psr\Log\LoggerInterface;
use AppBundle\Event\UserCreatedEvent;
use AppBundle\Event\PsDownloadedEvent;
use AppBundle\Event\SampleRequestEvent;
use AppBundle\Service\SalesForce\Tracking;
use AppBundle\Service\SalesForce\Endpoint;
use AppBundle\Service\SalesForce\BlackList;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class SalesForceTrackingSubscriber
 *
 * @package AppBundle\EventSubscriber
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class SalesForceTrackingSubscriber implements EventSubscriberInterface
{
    /** @var LoggerInterface */
    private $logger;

    /** @var Tracking */
    private $tracking;

    /** @var */
    private $baseURL;

    /**
     * @param LoggerInterface $logger
     * @param Tracking        $tracking
     * @param string          $baseURL
     */
    public function __construct(LoggerInterface $logger, Tracking $tracking, $baseURL)
    {
        $this->logger = $logger;
        $this->tracking = $tracking;
        $this->baseURL = $baseURL;
    }

    /**
     * @return string[]
     */
    public static function getSubscribedEvents()
    {
        return [
            Events::USER_CREATED  => 'onUserCreated',
            Events::PS_DOWNLOADED => 'onPsDownloaded',
            Events::SAMPLE_REQUEST_RECEIVED => 'onSampleRequestReceived',
        ];
    }

    /**
     * @param UserCreatedEvent $event
     */
    public function onUserCreated(UserCreatedEvent $event)
    {
        $user = $event->getUser();
        $type = $event->getType();

        if (false === BlackList::passes($user->getEmail())) {
            return;
        }

        $company  = (!$user->getCompanyName())    ? "No company"  : $user->getCompanyName();
        $address  = (!$user->getCompanyAddress()) ? "No address"  : $user->getCompanyAddress();
        $phone    = (!$user->getCompanyPhone())   ? "No phone"    : $user->getCompanyPhone();
        $position = (!$user->getPosition())       ? "No position" : $user->getPosition();

        $endpoint = new Endpoint();
        $endpoint
            ->setUri('/services/apexrest/Leads')
            ->setOptions([
                'leadName'       => $user->getFullName(),
                'email'          => $user->getEmail(),
                'companyName'    => $company,
                'companyAddress' => $address,
                'phone'          => $phone,
                'position'       => $position,
                'action'         => $type.' P&S Account',
            ]);
        $this->logger->critical(
            sprintf('SalesForce url: %s; Data: %s', $endpoint->getUri(), json_encode($endpoint->getOptions()))
        );
        try {
            $this->tracking->track($endpoint);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param PsDownloadedEvent $event
     */
    public function onPsDownloaded(PsDownloadedEvent $event)
    {
        $user = $event->getUser();

        if (false === BlackList::passes($user->getEmail())) {
            return;
        }

        $project = $event->getProject();
        $designURL = "{$this->baseURL}/user/designer/{$project->getId()}";

        $company  = (!$user->getCompanyName()) ? "No company" : $user->getCompanyName();

        $endpoint = new Endpoint();
        $endpoint
            ->setUri('/services/apexrest/PsDownload')
            ->setOptions([
                'leadEmail'    => $user->getEmail(),
                'url'          => $designURL,
                'type'         => $event->getType(),
                'projectName'  => $project->getTitle(),
                'projectId'    => $project->getId(),
                'company_name' => $company,
                'name'         => $user->getFullName(),
                'phone'        => $user->getPhone(),
                'title'        => $user->getPosition(),
                'action'       => 'Download Specification '.strtoupper($event->getType())
            ]);
        $this->logger->critical(
            sprintf('SalesForce url: %s; Data: %s', $endpoint->getUri(), json_encode($endpoint->getOptions()))
        );
        try {
            $this->tracking->track($endpoint);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }

    /**
     * @param SampleRequestEvent $event
     */
    public function onSampleRequestReceived(SampleRequestEvent $event)
    {
        $sampleRequest = $event->getSampleRequest();

        if (false === BlackList::passes($sampleRequest->getUser()->getEmail())) {
            return;
        }
        $project   = $event->getSampleRequest()->getProject();
        $designURL = "{$this->baseURL}/user/designer/{$project->getId()}";

        $endpoint = new Endpoint();
        $endpoint
            ->setUri('/services/apexrest/sampleRequest')
            ->setOptions([
                'leadEmail'      => $sampleRequest->getUser()->getEmail(),
                'project_id'     => $sampleRequest->getProject()->getId(),
                'project_name'   => $sampleRequest->getProjectName(),
                'company_name'   => $sampleRequest->getCompanyName(),
                'name'           => $sampleRequest->getName(),
                'conent'         => $sampleRequest->getContent(),
                'street_address' => $sampleRequest->getStreetAddress(),
                'phone'          => $sampleRequest->getPhone(),
                'sample_type'    => ucwords(SampleType::getTitles()[$sampleRequest->getSampleType()]),
                'title'          => $sampleRequest->getUser()->getPosition(),
                'url'            => $designURL,
                'action'         => 'Request Sample',
            ]);

        $this->logger->critical(
            sprintf('SalesForce url: %s; Data: %s', $endpoint->getUri(), json_encode($endpoint->getOptions()))
        );
        try {
            $this->tracking->track($endpoint);
        } catch (Exception $e) {
            $this->logger->error($e->getMessage());
        }
    }
}
