<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\EventSubscriber\AWS;


use AppBundle\Event\AWS\RDS\DBCluster\DBClusterCreatedEvent;
use AppBundle\Event\AWS\RDS\DBCluster\DBClusterCreationInitializedEvent;
use AppBundle\Event\AWS\RDS\DBCluster\DBClusterDeletedEvent;
use AppBundle\Event\AWS\RDS\DBCluster\DBClusterDeletionInitializedEvent;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceCreatedEvent;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceCreationInitialized;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceDeletedEvent;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceDeletionInitializedEvent;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;

/**
 * Class RDSEventSubscriber
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\EventSubscriber\AWS
 */
class RDSEventSubscriber implements EventSubscriberInterface
{
    public static function getSubscribedEvents()
    {
        return [
            DBClusterCreationInitializedEvent::NAME  => 'onClusterCreation',
            DBClusterDeletionInitializedEvent::NAME  => 'onClusterDeletion',
            DBClusterCreatedEvent::NAME              => 'onClusterCreated',
            DBClusterDeletedEvent::NAME              => 'onClusterDeleted',
            DBInstanceCreationInitialized::NAME      => 'onInstanceCreation',
            DBInstanceDeletionInitializedEvent::NAME => 'onInstanceDeletion',
            DBInstanceCreatedEvent::NAME             => 'onInstanceCreated',
            DBInstanceDeletedEvent::NAME             => 'onInstanceDeleted',
        ];
    }

    /**
     * @param DBClusterCreationInitializedEvent $event
     */
    public function onClusterCreation(DBClusterCreationInitializedEvent $event)
    {
        $dispatcher = $event->getDispatcher();
        $manager    = $event->getManager();
        $logger     = $manager->getLogger();

        while ( ! $manager->clusterAvailable()) {
            sleep(10);
            $logger->info('Waiting while cluster will be created...');
        }

        $subEvent = new DBClusterCreatedEvent($manager);
        $dispatcher->dispatch($subEvent::NAME, $subEvent);
    }

    /**
     * @param DBClusterDeletionInitializedEvent $event
     */
    public function onClusterDeletion(DBClusterDeletionInitializedEvent $event)
    {
        $dispatcher = $event->getDispatcher();
        $manager    = $event->getManager();
        $logger     = $manager->getLogger();

        while ($manager->clusterExists()) {
            sleep(10);
            $logger->info('Waiting while cluster will be deleted...');
        }

        $subEvent = new DBClusterDeletedEvent($manager);
        $dispatcher->dispatch($subEvent::NAME, $subEvent);
    }

    /**
     * @param DBClusterCreatedEvent $event
     */
    public function onClusterCreated(DBClusterCreatedEvent $event)
    {
        $manager = $event->getManager();
        $logger  = $manager->getLogger();

        $logger->info('Cluster was created.');

        $manager->createDbInstance();
    }

    /**
     * @param DBClusterDeletedEvent $event
     */
    public function onClusterDeleted(DBClusterDeletedEvent $event)
    {
        $manager = $event->getManager();
        $logger  = $manager->getLogger();

        $logger->info('Cluster was deleted.');
    }

    /**
     * @param DBInstanceCreationInitialized $event
     */
    public function onInstanceCreation(DBInstanceCreationInitialized $event)
    {
        $dispatcher = $event->getDispatcher();
        $manager    = $event->getManager();
        $logger     = $manager->getLogger();

        while ( ! $manager->instanceAvailable()) {
            sleep(10);
            $logger->info('Waiting while instance will be created...');
        }

        $subEvent = new DBInstanceCreatedEvent($manager);
        $dispatcher->dispatch($subEvent::NAME, $subEvent);
    }

    /**
     * @param DBInstanceDeletionInitializedEvent $event
     */
    public function onInstanceDeletion(DBInstanceDeletionInitializedEvent $event
    ) {
        $dispatcher = $event->getDispatcher();
        $manager    = $event->getManager();
        $logger     = $manager->getLogger();

        while ($manager->instanceExists()) {
            sleep(10);
            $logger->info('Waiting while instance will be deleted...');
        }

        $subEvent = new DBInstanceDeletedEvent($manager);
        $dispatcher->dispatch($subEvent::NAME, $subEvent);
    }

    /**
     * @param DBInstanceCreatedEvent $event
     */
    public function onInstanceCreated(DBInstanceCreatedEvent $event)
    {
        $manager = $event->getManager();
        $logger  = $manager->getLogger();

        $logger->info('Instance was created.');
    }

    /**
     * @param DBInstanceDeletedEvent $event
     */
    public function onInstanceDeleted(DBInstanceDeletedEvent $event)
    {
        $manager = $event->getManager();
        $logger  = $manager->getLogger();

        $logger->info('Instance was deleted.');
        $manager->removeDbCluster();
    }
}
