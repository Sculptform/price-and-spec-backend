<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Image\AbstractImage;
use AppBundle\Entity\Image\GalleryProjectImage;
use Gregwar\ImageBundle\Services\ImageHandling;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\JsonSerializationVisitor;
use Symfony\Component\DependencyInjection\Container;

class ImageSerializerSubscriber implements EventSubscriberInterface
{
    /**
     * @var Container
     */
    private $container;

    /**
     * ImageSerializerSubscriber constructor.
     * @param $container
     */
    public function __construct($container)
    {
        $this->container = $container;
    }

    public static function getSubscribedEvents()
    {
        return [
            ['event' => 'serializer.post_serialize', 'method' => 'onPostSerialize', 'class' => GalleryProjectImage::class]
        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        /** @var ImageHandling $imageService */
        $imageService = $this->container->get('image.handling');

        /** @var AbstractImage $image */
        $image = $event->getObject();

        $handledImage = $imageService->open($image->getImageWebPath())->cropResize(200);

        $visitor->setData('content', 'data:image/' . $handledImage->guessType() . ';base64,'.base64_encode($handledImage->get()));
    }
}