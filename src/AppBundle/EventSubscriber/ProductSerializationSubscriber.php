<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Entity\Product;
use AppBundle\Entity\Project;
use AppBundle\Entity\Railing;
use AppBundle\Entity\Currency;
use AppBundle\Entity\AcousticBacking;
use Doctrine\ORM\EntityManagerInterface;
use JMS\Serializer\JsonSerializationVisitor;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;

/**
 * Class ProductSerializationSubscriber
 *
 * @package AppBundle\EventSubscriber
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class ProductSerializationSubscriber implements EventSubscriberInterface
{
    /** @var array */
    private static $watchedModels = [
        Product::class,
        Railing::class,
        Project::class,
        AcousticBacking::class,
    ];

    /** @var EntityManagerInterface */
    private $em;

    /** @param EntityManagerInterface $em */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /** {@inheritdoc} */
    public static function getSubscribedEvents()
    {
        return [
            [
                'event'  => 'serializer.post_serialize',
                'method' => 'onPostSerialize',
                // 'class'  => Product::class,
            ]
        ];
    }

    public function onPostSerialize(ObjectEvent $event)
    {
        /** @var Product|AcousticBacking $model */
        $model = $event->getObject();

        if (!in_array(get_class($model), static::$watchedModels)) {
            return;
        }

        $price = $model->getPrice();
        $rates = $this->getRates();

        /** @var JsonSerializationVisitor $visitor */
        $visitor = $event->getVisitor();

        foreach ($rates as $rate) {
            $converted = (float) ($price * $rate->getRate());

            if ($model instanceof Project and 'USD' === $rate->getCode()) {
                $converted = $converted / Project::SQUARE_METRE;
            }

            $key = "price_".strtolower($rate->getCode());

            // $visitor->setData($key, (float) number_format($converted, 2));
            $visitor->setData($key, $converted);
        }
    }

    /**
     * @return Currency[]|array|object[]
     */
    private function getRates()
    {
        return $this->em->getRepository(Currency::class)->findBy(['isActive' => 1]);
    }
}
