<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class TeamMembersEvent extends Event
{
    /**
     * @var Team
     */
    private $team;

    /**
     * @var User
     */
    private $user;

    /**
     * @var User
     */
    private $eventUser;

    private $eventType;

    /**
     * TeamMembersEvent constructor.
     * @param Team $team
     * @param User $user
     * @param $eventType
     * @param User|null $eventUser
     */
    public function __construct(Team $team, User $user, $eventType, User $eventUser = null)
    {
        $this->team = $team;
        $this->user = $user;
        $this->eventUser = $eventUser;
        $this->eventType = $eventType;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}
