<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Event\AWS\RDS\DBInstance;


use AppBundle\Interfaces\AWS\AwsClientAwareEventInterface;
use AppBundle\Traits\AWS\AwsClientAwareEventTrait;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class DBInstanceCreatedEvent
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Event\AWS\RDS\DBInstance
 */
class DBInstanceCreatedEvent extends Event implements AwsClientAwareEventInterface
{
    use AwsClientAwareEventTrait;

    const NAME = 'aws.rds.dbinstance.created';
}
