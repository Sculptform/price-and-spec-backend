<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Event\AWS\RDS\DBCluster;


use AppBundle\Interfaces\AWS\AwsClientAwareEventInterface;
use AppBundle\Traits\AWS\AwsClientAwareEventTrait;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class DBClusterDeletedEvent
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Event\AWS\RDS\DBCluster
 */
class DBClusterDeletedEvent extends Event implements AwsClientAwareEventInterface
{
    use AwsClientAwareEventTrait;

    const NAME = 'aws.rds.dbcluster.deleted';
}
