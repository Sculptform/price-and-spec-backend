<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\UserFollower;
use Symfony\Component\EventDispatcher\Event;

class UserFollowerEvent extends Event
{
    /**
     * @var UserFollower
     */
    private $userFollower;

    /**
     * UserFollowerEvent constructor.
     * @param UserFollower $userFollower
     */
    public function __construct(UserFollower $userFollower)
    {
        $this->userFollower = $userFollower;
    }

    /**
     * @return UserFollower
     */
    public function getUserFollower()
    {
        return $this->userFollower;
    }
}
