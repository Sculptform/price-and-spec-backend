<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\UserProject;
use Symfony\Component\EventDispatcher\Event;

class UserProjectEvent extends Event
{
    /**
     * @var UserProject
     */
    private $userProject;

    /**
     * UserProjectEvent constructor.
     * @param UserProject $userProject
     */
    public function __construct(UserProject $userProject)
    {
        $this->userProject = $userProject;
    }

    /**
     * @return UserProject
     */
    public function getUserProject()
    {
        return $this->userProject;
    }
}
