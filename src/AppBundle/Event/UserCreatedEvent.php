<?php

namespace AppBundle\Event;

use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class UserCreatedEvent
 *
 * @package AppBundle\Event
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class UserCreatedEvent extends Event
{
    /**
     * @var User
     */
    private $user;

    /**
     * @var string
     */
    private $type;

    /**
     * UserCreatedEvent constructor.
     *
     * @param User $user
     * @param $type
     */
    public function __construct(User $user, $type)
    {
        $this->user = $user;
        $this->type = $type;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    public function getType()
    {
        return $this->type;
    }
}
