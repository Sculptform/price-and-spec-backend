<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Event;


use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class PardotTrackActionEvent
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Event
 */
class PardotTrackActionEvent extends Event
{
    const NAME = 'pardot.action.tracked';

    /**
     * @var User
     */
    private $user;

    /**
     * @var Project
     */
    private $project;

    /**
     * @var string
     */
    private $actionType;

    /**
     * PardotTrackActionEvent constructor.
     *
     * @param User    $user
     * @param Project $project
     * @param string  $actionType
     */
    public function __construct(User $user, Project $project, string $actionType)
    {
        $this->user       = $user;
        $this->project    = $project;
        $this->actionType = $actionType;

    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

}
