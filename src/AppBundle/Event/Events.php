<?php

namespace AppBundle\Event;

/**
 * Class Events
 */
final class Events
{
    /**
     * @Event("AppBundle\Event\NewsEvent")
     */
    const NEWS_CREATED = 'app.news_created';

    /**
     * @Event("AppBundle\Event\QuotationEvent")
     */
    const QUOTATION_RECEIVED = 'app.quotation_received';

    /**
     * @Event("AppBundle\Event\SampleRequestEvent")
     */
    const SAMPLE_REQUEST_RECEIVED = 'app.sample_request_received';

    /**
     * @Event("AppBundle\Event\UserProjectEvent")
     */
    const PROJECT_SHARED = 'app.project_shared';

    /**
     * @Event("AppBundle\Event\UserFollowerEvent")
     */
    const USER_FOLLOWED = 'app.user_followed';

    /**
     * @Event("AppBundle\Event\TeamUserEvent")
     */
    const TEAM_INVITED = 'app.team_invited';

    /**
     * @Event("AppBundle\Event\TeamUserEvent")
     */
    const TEAM_REQUEST_TO_JOIN = 'app.team_request_to_join';

    /**
     * @Event("AppBundle\Event\FeedEvent")
     */
    const FEED_LIKED = 'app.feed_liked';

    /**
     * @Event("AppBundle\Event\FeedEvent")
     */
    const FEED_COMMENTED = 'app.feed_commented';

    /**
     * @Event("AppBundle\Event\FeedEvent")
     */
    const FEED_SHARED = 'app.feed_shared';

    /**
     * @Event("AppBundle\Event\ProjectEvent")
     */
    const PROJECT_RATED = 'app.project_rated';

    /**
     * @Event("AppBundle\Event\ProjectEvent")
     */
    const PROJECT_COMMENTED = 'app.project_commented';

    /**
     * @Event("AppBundle\Event\GalleryProjectEvent")
     */
    const GALLERY_PROJECT_COMMENTED = 'app.gallery_project_commented';

    /**
     * @Event("AppBundle\Event\GalleryProjectEvent")
     */
    const GALLERY_PROJECT_SHARED = 'app.gallery_project_shared';

    /**
     * @Event("AppBundle\Event\TeamMembersEvent")
     */
    const TEAM_MEMBERS_GALLERY_PROJECT_CREATED = 'app.team_members_gallery_project_created';

    /**
     * @Event("AppBundle\Event\TeamMembersEvent")
     */
    const TEAM_MEMBERS_PROJECT_CREATED = 'app.team_members_project_created';

    /**
     * @Event("AppBundle\Event\TeamMembersEvent")
     */
    const TEAM_MEMBERS_DISCUSSION_CREATED = 'app.team_members_discussion_created';

    /**
     * @Event("AppBundle\Event\DiscussionEvent")
     */
    const DISCUSSION_SET_SPAMMED = 'app.discussion_set_spammed';

    /**
     * @Event("AppBundle\Event\DiscussionEvent")
     */
    const DISCUSSION_COMMENTED = 'app.discussion_commented';

    /**
     * @Event("AppBundle\Event\DiscussionEvent")
     */
    const DISCUSSION_SHARED = 'app.forum_post_shared';

    /**
     * @Event("AppBundle\Event\ProductSandpitEvent")
     */
    const PRODUCT_SANDPIT_CREATED = 'app.product_sandpit_created';

    /**
     * @Event("AppBundle\Event\FollowerDiscussionCreatedEvent")
     */
    const FOLLOWER_DISCUSSION_CREATED = 'app.follower_discussion_created';

    /**
     * @Event("AppBundle\Event\UserCreatedEvent")
     */
    const USER_CREATED = 'app.user_created';

    /**
     * @Event("AppBundle\Event\PsDownloadedEvent")
     */
    const PS_DOWNLOADED  = 'app.ps_downloaded';
}
