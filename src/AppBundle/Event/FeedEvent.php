<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\Feed;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class FeedEvent extends Event
{
    /**
     * @var Feed
     */
    private $feed;

    /**
     * @var User
     */
    private $eventUser;

    private $eventType;

    /**
     * FeedEvent constructor.
     * @param Feed $feed
     * @param User $eventUser
     * @param $eventType
     * @param User|null $feedUser
     */
    public function __construct(Feed $feed, User $eventUser, $eventType, User $feedUser = null)
    {
        $this->feed = $feed;
        $this->eventUser = $eventUser;
        $this->eventType = $eventType;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

}
