<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\ProductSandpit;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class ProductSandpitEvent extends Event
{
    /**
     * @var ProductSandpit
     */
    private $productSandpit;

    /**
     * @var User
     */
    private $user;

    /**
     * ProductSandpitEvent constructor.
     * @param ProductSandpit $productSandpit
     * @param User $user
     */
    public function __construct(ProductSandpit $productSandpit, User $user)
    {
        $this->productSandpit = $productSandpit;
        $this->user = $user;
    }

    /**
     * @return ProductSandpit
     */
    public function getProductSandpit()
    {
        return $this->productSandpit;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

}
