<?php
/**
 * Created by PhpStorm.
 * User: dreyup
 * Date: 19.04.17
 * Time: 10:45
 */

namespace AppBundle\Event;


use AppBundle\Entity\Discussion;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class FollowerDiscussionEvent extends Event
{
    /**
     * @var Discussion
     */
    private $discussion;

    /**
     * @var User
     */
    private $eventUser;

    /**
     * @var User|null
     */
    private $discussionUser;

    /**
     * DiscussionEvent constructor.
     * @param Discussion $discussion
     * @param User $eventUser
     * @param User|null $discussionUser
     */
    public function __construct(Discussion $discussion, User $eventUser, User $discussionUser = null)
    {
        $this->discussion = $discussion;
        $this->eventUser = $eventUser;
        $this->discussionUser = $discussionUser;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @return User
     */
    public function getDiscussionUser()
    {
        return $this->discussionUser;
    }
}