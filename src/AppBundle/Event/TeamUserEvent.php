<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\TeamUser;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class TeamUserEvent extends Event
{
    /**
     * @var TeamUser
     */
    private $teamUser;

    /**
     * @var User
     */
    private $eventUser;

    private $eventType;

    /**
     * TeamUserEvent constructor.
     * @param TeamUser $teamUser
     * @param User $eventUser
     * @param $eventType
     */
    public function __construct(TeamUser $teamUser, User $eventUser, $eventType)
    {
        $this->teamUser = $teamUser;
        $this->eventType = $eventType;
        $this->eventUser = $eventUser;
    }

    /**
     * @return TeamUser
     */
    public function getTeamUser()
    {
        return $this->teamUser;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }
}
