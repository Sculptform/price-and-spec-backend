<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\News;
use Symfony\Component\EventDispatcher\Event;

class NewsEvent extends Event
{
    /**
     * @var News
     */
    private $news;

    /**
     * NewsEvent constructor.
     * @param News $news
     */
    public function __construct(News $news)
    {
        $this->news = $news;
    }

    /**
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }
}
