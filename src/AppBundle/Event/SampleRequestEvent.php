<?php

namespace AppBundle\Event;

use AppBundle\Entity\SampleRequest;
use Symfony\Component\EventDispatcher\Event;

class SampleRequestEvent extends Event
{
    /** @var SampleRequest */
    private $sampleRequest;

    /**
     * QuotationEvent constructor.
     * @param SampleRequest $sampleRequest
     */
    public function __construct(SampleRequest $sampleRequest)
    {
        $this->sampleRequest = $sampleRequest;
    }

    /**
     * @return SampleRequest
     */
    public function getSampleRequest()
    {
        return $this->sampleRequest;
    }
}
