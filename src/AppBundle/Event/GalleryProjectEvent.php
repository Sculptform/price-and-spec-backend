<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class GalleryProjectEvent extends Event
{
    /**
     * @var GalleryProject
     */
    private $galleryProject;

    /**
     * @var User
     */
    private $galleryProjectOwner;

    /**
     * @var User
     */
    private $eventUser;

    private $eventType;

    public function __construct(GalleryProject $galleryProject, User $eventUser, $eventType, User $galleryProjectOwner = null)
    {
        $this->galleryProject = $galleryProject;
        $this->galleryProjectOwner = $galleryProjectOwner;
        $this->eventUser = $eventUser;
        $this->eventType = $eventType;
    }

    /**
     * @return GalleryProject
     */
    public function getGalleryProject()
    {
        return $this->galleryProject;
    }

    /**
     * @return User
     */
    public function getGalleryProjectOwner()
    {
        return $this->galleryProjectOwner;
    }


    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

}
