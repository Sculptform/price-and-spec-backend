<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\Discussion;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class DiscussionEvent extends Event
{
    /**
     * @var Discussion
     */
    private $discussion;

    /**
     * @var User
     */
    private $eventUser;

    /**
     * @var User|null
     */
    private $discussionUser;

    private $eventType;

    /**
     * DiscussionEvent constructor.
     * @param Discussion $discussion
     * @param User $eventUser
     * @param $eventType
     * @param User|null $discussionUser
     */
    public function __construct(Discussion $discussion, User $eventUser,  $eventType, User $discussionUser = null)
    {
        $this->discussion = $discussion;
        $this->eventUser = $eventUser;
        $this->eventType = $eventType;
        $this->discussionUser = $discussionUser;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return User
     */
    public function getDiscussionUser()
    {
        return $this->discussionUser;
    }

}
