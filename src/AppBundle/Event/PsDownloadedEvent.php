<?php

namespace AppBundle\Event;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class PsDownloadedEvent
 *
 * @package AppBundle\Event
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class PsDownloadedEvent extends Event
{
    private $user;
    private $project;
    private $type;

    public function __construct(User $user, Project $project, $type)
    {
        $this->type    = $type;
        $this->user    = $user;
        $this->project = $project;
    }

    public function getUser()
    {
        return $this->user;
    }

    public function getProject()
    {
        return $this->project;
    }

    public function getType()
    {
        return $this->type;
    }
}
