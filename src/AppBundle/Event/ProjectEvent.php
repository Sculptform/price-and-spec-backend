<?php

/*
 * This file is part of the FOSUserBundle package.
 *
 * (c) FriendsOfSymfony <http://friendsofsymfony.github.com/>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Event;

use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use Symfony\Component\EventDispatcher\Event;

class ProjectEvent extends Event
{
    /**
     * @var Project
     */
    private $project;

    /**
     * @var User
     */
    private $projectOwner;

    /**
     * @var User
     */
    private $eventUser;

    private $eventType;

    /**
     * ProjectEvent constructor.
     * @param Project $project
     * @param User $eventUser
     * @param $eventType
     * @param User|null $projectOwner
     */
    public function __construct(Project $project, User $eventUser, $eventType, User $projectOwner = null)
    {
        $this->project = $project;
        $this->projectOwner = $projectOwner;
        $this->eventUser = $eventUser;
        $this->eventType = $eventType;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * @return User
     */
    public function getProjectOwner()
    {
        return $this->projectOwner;
    }

    /**
     * @return mixed
     */
    public function getEventType()
    {
        return $this->eventType;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }
}
