<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 23.01.17
 * Time: 11:09
 */

namespace AppBundle\EventListener;

use Doctrine\ORM\EntityManager;
use Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorageInterface;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;
use Symfony\Component\HttpKernel\HttpKernel;
use FOS\UserBundle\Model\UserManagerInterface;
use FOS\UserBundle\Model\UserInterface;


/**
 * Listener that updates the last activity of the authenticated user
 */
class ActivityUserListener
{
    /**
     * @var TokenStorageInterface
     */
    protected $tokenStorage;
    /**
     * @var UserManagerInterface
     */
    protected $userManager;

    /**
     * ActivityUserListener constructor.
     * @param TokenStorageInterface $tokenStorage
     * @param UserManagerInterface $userManager
     */
    public function __construct(TokenStorageInterface $tokenStorage, UserManagerInterface $userManager)
    {
        $this->tokenStorage = $tokenStorage;
        $this->userManager  = $userManager;
    }

    /**
     * Update the user "lastActivity" on each request
     * @param FilterControllerEvent $event
     */
    public function onCoreController(FilterControllerEvent $event)
    {
        // Check that the current request is a "MASTER_REQUEST"
        // Ignore any sub-request
        if ($event->getRequestType() !== HttpKernel::MASTER_REQUEST) {
            return;
        }
        $clientHeader = $event->getRequest()->headers->get('x-api-client');

        // Check token authentication availability
        if ($this->tokenStorage->getToken()) {
            $user = $this->tokenStorage->getToken()->getUser();

            if (($user instanceof UserInterface) && !($user->isOnline())) {
                $user->setLastLogin(new \DateTime());
                $this->userManager->updateUser($user);
            }elseif($user instanceof UserInterface && ($clientHeader && $clientHeader=='sculptform')) {//TODO: Move header name to the string constant
                    $user->setLastLoginSFM(new \DateTime());
                    $this->userManager->updateUser($user);
            }
        }
    }
}