<?php
/**
 * Class ProductSandpitListener
 * @package AppBundle\EventListener
 * @author Andrey Zaytsev <dreyup96@gmail.com>
 */

namespace AppBundle\EventListener;

use AppBundle\Entity\ProductSandpit;
use AppBundle\Entity\User;
use AppBundle\Event\Events;
use AppBundle\Event\ProductSandpitEvent;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\PreFlushEventArgs;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\DependencyInjection\ContainerInterface;

class ProductSandpitListener extends ORM\DefaultEntityListenerResolver
{
    /** @var ContainerInterface */
    private $container;

    /**
     * ProductSandpitListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @ORM\PostPersist
     * @param ProductSandpit $productSandpit
     * @param LifecycleEventArgs $event
     */
    public function postPersistHandler(ProductSandpit $productSandpit, LifecycleEventArgs $event)
    {
        if (!$productSandpit instanceof ProductSandpit) {
            return;
        }

        $entityManager = $event->getEntityManager();

        $users = $entityManager->getRepository(User::class)->findAll();

        foreach ($users as $user) {
            $this->container->get('event_dispatcher')->dispatch(Events::PRODUCT_SANDPIT_CREATED, new ProductSandpitEvent($productSandpit, $user));
        }
    }
}