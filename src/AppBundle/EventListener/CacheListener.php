<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 08.12.17
 * Time: 13:01
 */

namespace AppBundle\EventListener;


use Symfony\Component\HttpKernel\Event\FilterResponseEvent;

class CacheListener
{
    public function onKernelResponse(FilterResponseEvent $event)
    {
        $response = $event->getResponse();

        $response->headers->addCacheControlDirective('no-cache', true);
        $response->headers->addCacheControlDirective('max-age', 0);
        $response->headers->addCacheControlDirective('must-revalidate', true);
        $response->headers->addCacheControlDirective('no-store', true);
        $response->headers->add(['Expires' => 'Wed, 11 Jan 1984 05:00:00 GMT']);
    }
}