<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\EventListener;


use AppBundle\Entity\PardotStatistics;
use AppBundle\Event\PardotTrackActionEvent;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\Event;

/**
 * Class PardotTrackActionListener
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\EventListener
 */
class PardotTrackActionListener
{
    /** @var EntityManagerInterface */
    private $em;

    /**
     * PardotTrackActionListener constructor.
     *
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param PardotTrackActionEvent $event
     */
    public function onPardotActionTracked(PardotTrackActionEvent $event)
    {
        $stats = new PardotStatistics(
            $event->getUser(), $event->getProject(), $event->getActionType()
        );
        $this->em->persist($stats);
        $this->em->flush();
    }
}
