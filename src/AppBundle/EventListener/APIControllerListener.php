<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 15.08.2016
 * Time: 12:43
 */

namespace AppBundle\EventListener;

use AppBundle\Controller\API\IAPIController;
use FOS\RestBundle\Controller\FOSRestController;
use Symfony\Component\HttpKernel\Event\FilterControllerEvent;

class APIControllerListener
{
    /**
     * @param FilterControllerEvent $event
     */
    public function onKernelController(FilterControllerEvent $event)
    {
        /** @var FOSRestController $controller */
        $controller = $event->getController();

        /*
         * $controller passed can be either a class or a Closure.
         * This is not usual in Symfony but it may happen.
         * If it is a class, it comes in array format
         */
        if (!is_array($controller)) {
            return;
        }

        if ($controller[0] instanceof IAPIController) {
            $event->getRequest()->getSession()->save();
        }
    }
}