<?php

namespace AppBundle\EventListener;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Scene;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Ramsey\Uuid\Uuid;

class MaterialFinishListener
{
    /** @var ContainerInterface */
    private $container;

    /**
     * ProductSandpitListener constructor.
     * @param ContainerInterface $container
     */
    public function __construct(ContainerInterface $container)
    {
        $this->container = $container;
    }

    /**
     * @param LifecycleEventArgs $args
     */
    public function postUpdate(LifecycleEventArgs $args)
    {
        $object = $args->getObject();

        if (!($object instanceof MaterialFinish)) {
            return;
        }

        gc_enable();
        ini_set('memory_limit','-1');
        ini_set('max_execution_time','-1');

        $request = $this->container->get('request_stack')->getCurrentRequest();

        if (!$request) { return; }

        $hostUrl = $request->getScheme().'://'.$request->getHttpHost();

        $em = $args->getEntityManager();
        $uow = $em->getUnitOfWork();
        $changeset = $uow->getEntityChangeSet($object);
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('s')
            ->getQuery()
        ;

        $oldTexturePath = $newTexturePath = null;

        if(array_key_exists('texturePath', $changeset) || array_key_exists('color', $changeset)) {
            if (array_key_exists('texturePath', $changeset)) {
                list($oldTexturePath, $newTexturePath) = $changeset['texturePath'];
            }

            foreach ($scenesQuery->iterate() as $sceneEntity) {
                /** @var Scene $sceneEntity */
                $sceneEntity = $sceneEntity[0];
                $sceneData = $sceneEntity->getScene();

                $scene = &$this->getSubArray($sceneData, 'scene');

                $sceneObject = &$this->getSubArray($scene, 'object');
                $sceneChildren   = &$this->getSubArray($sceneObject, 'children');
                $sceneMaterials = &$this->getSubArray($scene, 'materials');
                $sceneTextures  = &$this->getSubArray($scene, 'textures');
                $sceneImages = &$this->getSubArray($scene, 'images');

                foreach ($sceneChildren as &$sceneChild) {
                    if(array_key_exists('materialFinishModel', $sceneChild) && $sceneChild['materialFinishModel'] == $object->getId()) {
                        if (array_key_exists('material', $sceneChild)) {
                            $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                            if (false !== $materialKey) {
                                $material = &$sceneMaterials[$materialKey];

                                if ($oldTexturePath) {
                                    if (array_key_exists('map', $material)) {
                                        $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                        if (false !== $sceneTextureKey) {
                                            $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                            if (array_key_exists('image', $sceneTexture)) {
                                                $sceneImageKey = $this->findKey($sceneImages, 'uuid', $sceneTexture['image']);

                                                if (false !== $sceneImageKey) {
                                                    if ($newTexturePath) {
                                                        $sceneImages[$sceneImageKey]['url'] = $hostUrl . '/' . $object->getTextureWebPath();
                                                    } else {
                                                        unset(
                                                            $sceneImages[$sceneImageKey],
                                                            $sceneTextures[$sceneTextureKey],
                                                            $material['map']
                                                        );
                                                    }
                                                }
                                            }

                                            unset($sceneTexture);
                                        }
                                    }
                                }
                                elseif ($newTexturePath) {
                                    $sceneImageUuid = Uuid::uuid4()->toString();

                                    $sceneImage = [
                                        'uuid' => $sceneImageUuid,
                                        'url' => $hostUrl . '/' . $object->getTextureWebPath()
                                    ];

                                    $sceneImages[] = $sceneImage;

                                    $textureUuid = Uuid::uuid4()->toString();

                                    $texture = [
                                        'uuid' => $textureUuid,
                                        'name' => '',
                                        'mapping' => 300,
                                        'repeat' => [1,1],
                                        'offset' => [0,0],
                                        'wrap' => [1001, 1001],
                                        'minFilter' => 1008,
                                        'magFilter' => 1006,
                                        'anisotropy' => 1,
                                        'image' => $sceneImageUuid
                                    ];

                                    $sceneTextures[] = $texture;

                                    $material['map'] = $textureUuid;
                                }

                                $material['color'] = $object->getDecimalColor();

                                unset($material);
                            }
                        }
                    }
                    gc_collect_cycles();
                }

                unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);

                $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);

                $sceneEntity->setScene($sceneData);
                $em->flush();
                $em->clear();

                gc_collect_cycles();
            }
        }
    }

    private function &getSubArray(&$array, $key)
    {
        if (!array_key_exists($key, $array)) {
            $array[$key] = [];
        }

        return $array[$key];
    }

    private function findKey($array, $searchKey, $searchValue)
    {
        $key = array_search($searchValue, array_column($array, $searchKey));

        return $key === false ? false : array_keys($array)[$key];
    }
}