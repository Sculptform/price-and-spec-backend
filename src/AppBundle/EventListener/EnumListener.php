<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\EventListener;


use AppBundle\Type\EnumType;
use Doctrine\ORM\Tools\Event\GenerateSchemaEventArgs;

/**
 * Class EnumListener
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\EventListener
 */
class EnumListener
{
    /**
     * @param GenerateSchemaEventArgs $eventArgs
     */
    public function postGenerateSchema(GenerateSchemaEventArgs $eventArgs
    ) {
        $columns = [];

        foreach ($eventArgs->getSchema()->getTables() as $table) {
            foreach ($table->getColumns() as $column) {
                if ($column->getType() instanceof EnumType) {
                    $columns[] = $column;
                }
            }
        }

        /** @var \Doctrine\DBAL\Schema\Column $column */
        foreach ($columns as $column) {
            $column->setComment(
                trim(
                    sprintf(
                        '%s (%s)', $column->getComment(),
                        implode(',', $column->getType()->getValues())
                    )
                )
            );
        }
    }
}
