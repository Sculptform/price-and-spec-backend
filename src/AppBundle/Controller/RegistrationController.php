<?php

namespace AppBundle\Controller;

use FOS\UserBundle\FOSUserEvents;
use AppBundle\Entity\OAuth\Client;
use FOS\UserBundle\Event\FormEvent;
use JMS\Serializer\SerializationContext;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use FOS\UserBundle\Event\GetResponseUserEvent;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RegistrationController
 */
class RegistrationController extends Controller
{
    /**
     * @param Request $request
     *
     * @return null|Response
     * @throws \OAuth2\OAuth2ServerException
     */
    public function indexAction(Request $request)
    {
        /** @var \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        /** @var \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        /** @var \Symfony\Component\EventDispatcher\EventDispatcherInterface */
        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $event = new GetResponseUserEvent($user, $request);
        $dispatcher->dispatch(FOSUserEvents::REGISTRATION_INITIALIZE, $event);

        if (null !== $event->getResponse()) {
            return $event->getResponse();
        }

        $form = $formFactory->createForm(array('csrf_protection' => false));
        $form->setData($user);
        $this->processForm($request, $form);

        $response = null;

        if ($form->isSubmitted()) {
            if ($form->isValid()) {
                $event = new FormEvent($form, $request);
                $dispatcher->dispatch(FOSUserEvents::REGISTRATION_SUCCESS, $event);

                $userManager->updateUser($user);

                $response = $this->createOAuthRequest([
                    'username' => $request->request->get('email'),
                    'password' => $request->request->get('password')
                ]);

                // $token = $tokenResponse->getContent();

            } else {
                $errors = $this->getErrorsFromForm($form);

                $data = [
                    'form_errors' => $errors
                ];

                $response = new JsonResponse($data);
            }
        }

        return $this->setBaseHeaders($response);
    }

    /**
     * @param FormInterface $form
     *
     * @return array
     */
    private function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }
        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    if ($childForm->getName() === 'username') {
                        continue;
                    }

                    $errors[$childForm->getName()] = $childErrors;
                }
            }
        }

        return $errors;
    }


    /**
     * @param array $data
     *
     * @return Response
     *
     * @throws \OAuth2\OAuth2ServerException
     */
    private function createOAuthRequest(array $data)
    {
        $em = $this->get('doctrine.orm.entity_manager');

        /** @var Client $client */
        $client = $em->getRepository(Client::class)->findOneBy([]);

        $grantRequest = new Request([
            'client_id'     => $client->getPublicId(),
            'client_secret' => $client->getSecret(),
            'grant_type'    => 'password',
            'username'      => $data['username'],
            'password'      => $data['password']
        ]);

        $tokenResponse = $this
            ->get('fos_oauth_server.server')
            ->grantAccessToken($grantRequest)
        ;

        return $tokenResponse;
    }

    /**
     * @param  Request $request
     * @param  FormInterface $form
     */
    private function processForm(Request $request, FormInterface $form)
    {
        // $data = json_decode($request->getContent(), true);

        if ($request->request->count() === 0) {
            throw new BadRequestHttpException();
        }

        $username = '';
        $email    = $request->request->has('email') ? $request->request->get('email') : '';
        $password = $request->request->has('password') ? $request->request->get('password') : '';

        if ($email) {
            $username = strstr($email, '@', true);
        }

        $submittedData = [
            'email' => $email,
            'username' => $username,
            'plainPassword' => [
                'first' => $password,
                'second' => $password
            ]
        ];


        $form->submit($submittedData);
    }

    /**
     * Data serializing via JMS serializer.
     *
     * @param mixed $data
     *
     * @return string JSON string
     */
    private function serialize($data)
    {
        $context = new SerializationContext();
        $context->setSerializeNull(true);

        return $this->get('jms_serializer')
            ->serialize($data, 'json', $context);
    }

    /**
     * Set base HTTP headers.
     *
     * @param Response $response
     *
     * @return Response
     */
    private function setBaseHeaders(Response $response)
    {
        $response->headers->set('Content-Type', 'application/json');
        $response->headers->set('Access-Control-Allow-Origin', '*');

        return $response;
    }
}
