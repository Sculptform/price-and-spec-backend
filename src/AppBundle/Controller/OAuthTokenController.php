<?php

namespace AppBundle\Controller;

use AppBundle\Entity\User;
use AppBundle\Enum\PardotUserLastAction;
use AppBundle\Service\PardotService;
use Doctrine\ORM\EntityManager;
use FOS\OAuthServerBundle\Controller\TokenController as BaseTokenController;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class OAuthTokenController extends BaseTokenController
{
    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @var PardotService
     */
    protected $pardotService;

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param OAuth2 $server
     * @param EntityManager $entityManager
     * @param PardotService $pardotService
     * @param LoggerInterface $logger
     */
    public function __construct(OAuth2 $server, EntityManager $entityManager, PardotService $pardotService, LoggerInterface $logger)
    {
        parent::__construct($server);
        $this->em            = $entityManager;
        $this->pardotService = $pardotService;
        $this->logger        = $logger;
    }

    /**
     * @param Request $request
     *
     * @return Response
     */
    public function tokenAction(Request $request)
    {
        try {
            $token = $this->server->grantAccessToken($request);

            /** Send Pardot Statistic */
            $params = [];
            parse_str($request->getContent(), $params);
            if ($params['username']) {
                $user = $this->em->getRepository(User::class)->findOneBy(['username' => $params['username']]);
                if ($user) {
                    $this->sendToPardot($user);
                }
            }
        } catch (OAuth2ServerException $e) {
            return $e->getHttpResponse();
        }

        return $token;
    }

    /**
     * {@inheritDoc}
     */
    private function sendToPardot($user)
    {
        try {
            $pardotResponse = $this->pardotService
                ->sendUserLastAction(
                    $user,
                    PardotUserLastAction::SESSION_LOGIN,
                    [
                        'P&S - Action' => 'Session Login',
                    ]
                );
            $this->logger->addInfo($pardotResponse);
        } catch (\Exception $e) {
            $this->logger->addError($e->getMessage());
        }
    }
}