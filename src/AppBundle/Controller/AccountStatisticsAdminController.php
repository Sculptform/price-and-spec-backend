<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 15.06.17
 * Time: 16:00
 */

namespace AppBundle\Controller;

use AppBundle\Service\AccountStatsService;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class AccountStatisticsAdminController extends Controller
{
    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }

        $showTestData = $this->admin->getRequest()->query->get('show-test-data', false);
        $showTestData = $showTestData == 'true' ? 1 : 0;

        /**
         * @var AccountStatsService
         */
        $statsService = $this->get('app.service.account_stats');
        $statsService->getChartData('Project');
        $data = array(
            'total' => array(
                'design'               => $statsService->getTotal('Project', $showTestData),
                'account'              => $statsService->getTotal('User', $showTestData),
                'quotation'            => $statsService->getTotal('Quotation', $showTestData),
                'sample_request'       => $statsService->getTotal('SampleRequest', $showTestData),
                'concept_click'        => $statsService->getTotal('Project.Concept Click', $showTestData),
                'expression_cladding'  => $statsService->getTotal('Project.Expression Cladding', $showTestData),
                'fintrax'              => $statsService->getTotal('Project.Fintrax', $showTestData),
        ), 'charts' => array(
                'design'              => array('title' => 'Designs', 'data' => $statsService->getChartData('Project', $showTestData)),
                'account'             => array('title' => 'Accounts', 'data' => $statsService->getChartData('User', $showTestData)),
                'quotation'           => array('title' => 'Estimate Requests', 'data' => $statsService->getChartData('Quotation', $showTestData)),
                'sample_request'      => array('title' => 'Sample Requests', 'data' => $statsService->getChartData('SampleRequest', $showTestData)),
                'concept_click'       => array('title' => 'Concept Click', 'data' => $statsService->getChartData('Project.Concept Click', $showTestData)),
                'expression_cladding' => array('title' => 'Expression Cladding', 'data' => $statsService->getChartData('Project.Expression Cladding', $showTestData)),
                'fintrax'             => array('title' => 'Fintrax', 'data' => $statsService->getChartData('Project.Fintrax', $showTestData))
            ));

       return $this->renderWithExtraParams('AppBundle:Statistics:account_statistics.html.twig', array(
           'data'  => $data,
           'showTestData' => $showTestData
       ));
    }
}