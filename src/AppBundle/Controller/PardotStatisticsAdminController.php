<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Controller;


use AppBundle\Service\AccountStatsService;
use Sonata\AdminBundle\Controller\CRUDController as Controller;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PardotStatisticsAdminController extends Controller
{
    public function listAction()
    {
        if (false === $this->admin->isGranted('LIST')) {
            throw new AccessDeniedException();
        }


        /**
         * @var AccountStatsService
         */
        $statsService = $this->get('app.pardot.client');
        dump($statsService); die;


        return $this->renderWithExtraParams(
            'AppBundle:Statistics:pardot_statistics.html.twig', [
        ]
        );
    }
}
