<?php

namespace AppBundle\Controller\API;

use AppBundle\Entity\OAuth\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Form\FormInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;

/**
 * Class RegisterRestTrait
 */
trait RegisterRestTrait
{
    /**
     * @param  Request $request
     * @param  FormInterface $form
     */
    public function processForm(Request $request, FormInterface $form)
    {
        if ($request->request->count() === 0) {
            throw new BadRequestHttpException();
        }

        $username = $request->request->has('email') ? $request->request->get('email') : '';
        $email    = $request->request->has('email') ? $request->request->get('email') : '';
        $password = $request->request->has('password') ? $request->request->get('password') : '';

        $submittedData = [
            'email' => $email,
            'username' => $username,
            'plainPassword' => [
                'first' => $password,
                'second' => $password
            ]
        ];


        $form->submit($submittedData);
    }

    /**
     * @param array $data
     *
     * @return Response
     */
    private function createOAuthRequest(array $data)
    {
        /** @var EntityManagerInterface $em */
        $em = $this->get('doctrine.orm.entity_manager');

        /** @var Client $client */
        $client = $em->getRepository(Client::class)->findOneBy([]);

        $grantRequest = new Request([
            'client_id'     => $client->getPublicId(),
            'client_secret' => $client->getSecret(),
            'grant_type'    => 'password',
            'username'      => $data['username'],
            'password'      => $data['password']
        ]);

        $tokenResponse = $this
            ->get('fos_oauth_server.server')
            ->grantAccessToken($grantRequest)
        ;

        return $tokenResponse;
    }

    public function explodeFirstName($name)
    {
        $parts = explode(' ', $name);

        return array_map('trim', $parts);
    }
}
