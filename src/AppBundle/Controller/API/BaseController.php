<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 03.02.17
 * Time: 21:26
 */

namespace AppBundle\Controller\API;

use FOS\RestBundle\Controller\FOSRestController;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Knp\Component\Pager\Pagination\SlidingPagination;
use Symfony\Component\HttpFoundation\Response;

class BaseController extends FOSRestController implements IAPIController
{
    /**
     * @param string $name
     * @param SlidingPagination|PaginationInterface $pagination
     * @return Response
     */
    public function paginationResponse($name, $pagination, $error = null)
    {
        $view = $this->view(
            [
                $name => $pagination->getItems(),
                'meta' => [
                    'error' => $error,
                    'total' => $pagination->getTotalItemCount(),
                    'count' => $pagination->count(),
                    'page'  => $pagination->getCurrentPageNumber(),
                    'limit' => $pagination->getItemNumberPerPage(),
                    'total_pages' => $pagination->getPaginationData()['pageCount']
                ]
            ],
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }

    /**
     * @return \Doctrine\Common\Persistence\ObjectManager
     */
    public function getEm()
    {
        return $this->getDoctrine()->getManager();
    }
}
