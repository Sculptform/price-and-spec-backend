<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Project;
use AppBundle\Entity\Team;
use AppBundle\Entity\TeamUser;
use AppBundle\Entity\User;
use AppBundle\Entity\Comment\TeamComment;
use AppBundle\Entity\Image\TeamImage;
use AppBundle\Entity\UserProject;
use AppBundle\Enum\TeamAccessType;
use AppBundle\Enum\TeamUserStatus;
use AppBundle\Enum\UserProjectUserType;
use AppBundle\Event\Events;
use AppBundle\Event\TeamUserEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\ImageFormType;
use AppBundle\Form\Type\TeamFormType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class TeamController extends BaseController
{
    /**
     * Use this method to get whole list of teams. All Teams objects are wrapped in "teams" field.
     * Team object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/teams",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Teams",
     *     section="Teams",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Teams List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="updatedAt", requirements="createdAt|updatedAt|users|title|projects", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="desc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="access_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Team Type")
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By User Id")
     * @Rest\QueryParam(name="title", allowBlank=true, nullable=true, description="Filter By Team Title")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllTeamsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(Team::class)->findAllByParams([
                'access_type' => $paramFetcher->get('access_type'),
                'user_id' => $paramFetcher->get('user_id'),
                'title' => $paramFetcher->get('title'),
                'sort' => $paramFetcher->get('sort'),
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('teams', $pagination);
    }

    /**
     * Use this method to get Team by Id. Team object documented here was wrapped in "team" field.
     *
     * @Rest\Get(
     *     path="/teams/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Teams by Id",
     *     section="Teams",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     statusCodes={
     *         404 = "Team not found. All other errors looks like this",
     *         200 = "Return Team"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Team"
     *     }
     * )
     *
     * @param Team $team
     * @return Response
     */
    public function getTeamAction(Team $team)
    {
        $confirmedTeamUsers = new ArrayCollection();
        $em = $this->getDoctrine()->getManager();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        /** @var TeamUser $teamUser */
        foreach ($team->getTeamUsers() as $teamUser) {
            if ($teamUser->getStatus() == TeamUserStatus::CONFIRMED) {
                $confirmedTeamUsers[] = $teamUser;

                $userProjects = $teamUser->getUser()->getUserProjects();

                /** @var UserProject $userProject */
                foreach ($userProjects as $userProject) {
                    if ($userProject->getUserType() == UserProjectUserType::OWNER) {
                        $project = $userProject->getProject();
                        if (!in_array($project->getId(), $team->getProjectsIds())) {
                            $team->addProject($project);
                            $em->persist($team);
                            $em->flush();
                        }
                    }
                }
            }
        }

        if (
            $team->getAccessType() == TeamAccessType::PRIVATE_ACCESS &&
            !$currentUser->hasRole('ROLE_ADMIN') &&
            !$confirmedTeamUsers->contains($currentUser) &&
            $team->getAdmin() !== $currentUser
        ) {
            throw new AccessDeniedException();
        }

        return $this->handleView($this->view(
            ['team' => $team],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Team by Id. Team object documented here was wrapped in "team" field
     *
     * @Rest\Route(
     *     path="/teams/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Team",
     *     input="AppBundle\Form\Type\TeamFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     section="Teams",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Team"
     *     },
     *     statusCodes={
     *         404 = "Team not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Team $team
     * @param Request $request
     * @return Response
     */
    public function updateTeamAction(Team $team, Request $request)
    {
        return $this->processTeamForm($team, $request);
    }

    /**
     * Use this method to create Team. Team object documented here was wrapped in "team" field
     *
     * @Rest\Post(
     *     path="/teams",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Team",
     *     input="AppBundle\Form\Type\TeamFormType",
     *     section="Teams",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Team"
     *     },
     *     statusCodes={
     *         404 = "Team not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createTeamAction(Request $request)
    {
        $team = new Team();
        $team->setAdmin($this->getUser());

        return $this->processTeamForm($team, $request);
    }

    /**
     * Delete Team .
     *
     * @Rest\Delete(
     *     path="/teams/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Team by id",
     *     section="Teams",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param Team $team
     * @return Response
     */
    public function deleteTeamAction(Team $team)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {
            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $team->getAdmin()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->remove($team);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Team - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param Team $team
     * @param Request $request
     * @return Response
     */
    private function processTeamForm(Team $team, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(TeamFormType::class, $team, ['method' => $method]);

        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();
            $teamUsers = $this->getTeamUsers($team);
            $teamProjects = $team->getProjectsIds();
            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $team->getAdmin()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            if ($form->has('projects')) {
                /** @var Collection $newTeamProjects */
                $newTeamProjects = $form->get('projects')->getData();

                if ($newTeamProjects->count() > 0) {
                    foreach ($newTeamProjects as $newTeamProject) {
                        /** @var Project $newTeamProject */
                        if (!in_array($newTeamProject->getId(), $teamProjects)) {
                            $team->addProject($newTeamProject);
                        }
                    }
                }
            }

            /** @var Collection $newUsers */
            $newUsers = $form->get('users_ids')->getData();

            if ($newUsers->count() > 0) {
                foreach ($newUsers as $newUser) {
                    /** @var User $newUser */
                    if (!$teamUsers->get($newUser->getId())) {

                        $teamUser = new TeamUser();
                        $teamUser->setUser($newUser);
                        $teamUser->setTeam($team);
                        $team->addTeamUser($teamUser);
                        $em->persist($teamUser);

                        $this->get('event_dispatcher')->dispatch(Events::TEAM_INVITED,
                                new TeamUserEvent($teamUser, $currentUser, Events::TEAM_INVITED));
                    }
                }
            }

            $em->persist($team);
            $em->flush();

            $view = $this->view(
                ['team' => $team],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to update Team Comment by Id. Team Comment object documented here was wrapped in "team" field
     *
     * @Rest\Route(
     *     path="/teams/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\TeamComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Update Team Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     section="Teams",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\TeamComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param TeamComment $comment
     * @param Request $request
     * @return Response
     */
    public function updateTeamCommentAction(TeamComment $comment, Request $request)
    {
        return $this->processTeamCommentForm($comment, $request);
    }

    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="teams/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     section="Teams",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\TeamComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Team $team
     * @param Request $request
     * @return Response
     */
    public function createTeamCommentAction(Team $team, Request $request)
    {
        $comment = new TeamComment();
        $comment->setTeam($team);
        $comment->setUser($this->getUser());

        return $this->processTeamCommentForm($comment, $request);
    }

    /**
     * Use this method to request to join Team. All Teams objects are wrapped in "teams" field.
     *
     * @Rest\Route(
     *     path="/teams/{id}/join",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Join to the Team",
     *     section="Teams",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Team"
     *     },
     *     resource=true
     * )
     *
     * @param Team $team
     * @return Response
     */
    public function requestToJoinAction(Team $team)
    {
        $em = $this->getEm();
        /** @var User $user */
        $user = $this->getUser();

        /** @var TeamUser $teamUser */
        if (!in_array($user->getId(), $team->getUsersIds())) {
            $teamUser = new TeamUser();

            $teamUser->setTeam($team);
            $teamUser->setUser($user);

            $teamUser->setStatus(TeamUserStatus::INVITED);

            $em->persist($teamUser);
            $team->addTeamUser($teamUser);
            $em->persist($team);
            $em->flush();

            $this->get('event_dispatcher')
                ->dispatch(Events::TEAM_REQUEST_TO_JOIN,
                    new TeamUserEvent($teamUser, $user, Events::TEAM_REQUEST_TO_JOIN));

        } else {
            $teamUser = $em->getRepository(TeamUser::class)->findOneBy(['team'=>$team, 'user'=>$user]);
            $teamUser->setStatus(TeamUserStatus::INVITED);

            $em->flush();

            $this->get('event_dispatcher')->dispatch(Events::TEAM_REQUEST_TO_JOIN, new TeamUserEvent($teamUser, $user, Events::TEAM_REQUEST_TO_JOIN));
        }

        return $this->handleView($this->view(
            ['team' => $team],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to accept to join the Team. All Teams objects are wrapped in "teams" field.
     *
     * @Rest\Route(
     *     path="/teams/{id}/accept/{user_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("user", class="AppBundle\Entity\User", options={"id" = "user_id"})
     *
     * @ApiDoc(
     *     description="Accept to join the Team",
     *     section="Teams",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Team"
     *     },
     *     resource=true
     * )
     *
     * @param Team $team
     * @param User $user
     * @return Response
     */
    public function acceptToJoinAction(Team $team, User $user)
    {
        $em = $this->getEm();

        /** @var TeamUser $teamUser */
        if ($teamUser = $em->getRepository(TeamUser::class)->findOneBy(['team' => $team, 'user' => $user])) {
            $teamUser->setStatus(TeamUserStatus::CONFIRMED);

            $em->persist($teamUser);
            $em->flush();
        }

        return $this->handleView($this->view(
            ['team' => $team],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to leave the Team. All Teams objects are wrapped in "teams" field.
     *
     * @Rest\Route(
     *     path="/teams/{id}/leave/{user_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"}
     * )
     *
     * @ParamConverter("user", class="AppBundle\Entity\User", options={"id" = "user_id"})
     *
     * @ApiDoc(
     *     description="Leave the Team",
     *     section="Teams",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Team"
     *     },
     *     resource=true
     * )
     *
     * @param Team $team
     * @param User $user
     * @return Response
     */
    public function leaveTeamAction(Team $team, User $user)
    {
        /** @var User $user */
        $currentUser = $this->getUser();

        if (!$currentUser->getId()
            || (!$currentUser->hasRole('ROLE_ADMIN')
                && $team->getAdmin()->getId() !== $currentUser->getId()
                && $currentUser != $user
            )
        ) {
            throw new AccessDeniedException();
        }

        $em = $this->getEm();

        /** @var TeamUser $teamUser */
        if ($teamUser = $em->getRepository(TeamUser::class)->findOneBy(['team' => $team->getId(), 'user' => $user->getId()])) {
            $team->removeTeamUser($teamUser);
            $em->persist($team);
            $em->remove($teamUser);
            $em->flush();
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param TeamComment $comment
     * @param Request $request
     * @return Response
     */
    private function processTeamCommentForm(TeamComment $comment, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(CommentFormType::class, $comment, ['method' => $method]);

        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($comment);

            $em->flush();

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to create Team Image. Team Image object documented here was wrapped in "team" field
     *
     * @Rest\Post(
     *     path="teams/{id}/images",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Add Image",
     *     input="AppBundle\Form\Type\ImageFormType",
     *     section="Teams",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\TeamImage"
     *     },
     *     statusCodes={
     *         404 = "Team not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     },
     *     resource=true
     * )
     *
     * @param Team $team
     * @param Request $request
     * @return Response
     */
    public function createTeamImageAction(Team $team, Request $request)
    {
        $image = new TeamImage();
        $image->setTeam($team);

        return $this->processTeamImageForm($image, $request);
    }

    /**
     * Delete Comment .
     *
     * @Rest\Delete(
     *     path="/teams/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\TeamComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Teams",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Team Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param TeamComment $comment
     * @return Response
     */
    public function deleteTeamCommentAction(TeamComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {
            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Comment - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param TeamImage $image
     * @param Request $request
     * @return Response
     */
    private function processTeamImageForm(TeamImage $image, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ImageFormType::class, $image, ['method' => $method]);

        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $image->getTeam()->getAdmin()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($image);
            $em->flush();

            $view = $this->view(['image' => $image], Response::HTTP_OK);
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * @param Team $team
     * @return ArrayCollection
     */
    private function getTeamUsers(Team $team)
    {
        $teamUsers = new ArrayCollection();
        $em = $this->getDoctrine()->getEntityManager();

        // SET ADMIN //
        $admin = new TeamUser();
        $admin->setTeam($team);
        $admin->setUser($team->getAdmin());
        $admin->setStatus(TeamUserStatus::CONFIRMED);
        $em->persist($admin);
        $em->flush();

        $teamUsers[$team->getAdmin()->getId()] = $team->getAdmin();

        /** @var TeamUser $teamUser */
        foreach ($team->getTeamUsers() as $teamUser) {
            $teamUsers[$teamUser->getUser()->getId()] = $teamUser->getUser();
        }

        return $teamUsers;
    }
}