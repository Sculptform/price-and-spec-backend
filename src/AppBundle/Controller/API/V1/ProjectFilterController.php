<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectFilter;
use AppBundle\Entity\User;
use Doctrine\ORM\EntityManager;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\API\BaseController;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpKernel\Exception\NotFoundHttpException;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProjectFilterController extends BaseController
{
    /**
     * Get all filters.
     *
     * @Rest\Get(name="api_v1_get_filters", path="/projects/filters", defaults={"_format"="json"})
     *
     * @return Response
     */
    public function indexAction()
    {
        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();

        $filters = $em->getRepository(ProjectFilter::class)->findAll();

        $view = $this->view($filters, Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * @param Project|null       $project
     * @param ProjectFilter|null $filter
     *
     * @Rest\Delete(path="/projects/{id}/filters/{filter_id}", defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}", "filter_id"="\d+"}
     * )
     *
     * @ParamConverter("project", class="AppBundle\Entity\Project",       options={"id"="id"})
     * @ParamConverter("filter",  class="AppBundle\Entity\ProjectFilter", options={"id"="filter_id"})
     *
     * @return Response
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function deleteProjectFilterAction(Project $project = null, ProjectFilter $filter = null)
    {
        if (null === $project or null === $filter) {
            throw new NotFoundHttpException('Project or Filter not found.');
        }

        /** @var User $user */
        $user = $this->getUser();

        if (false === $user->hasRole('ROLE_ADMIN') and $project->getOwnerId() !== $user->getId()) {
            throw new AccessDeniedException();
        }

        if ($project->getFilters()->contains($filter)) {
            $project->getFilters()->removeElement($filter);
        }

        /** @var EntityManager $em */
        $em = $this->getDoctrine()->getManager();
        $em->flush($project);

        return $this->handleView($this->view(['project' => $project], Response::HTTP_OK));
    }
}
