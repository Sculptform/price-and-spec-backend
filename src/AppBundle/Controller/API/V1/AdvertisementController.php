<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Advertisement;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\AdvertisementFormType;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class AdvertisementController extends BaseController
{
    /**
     * Use this method to get whole list of ads. All Advertisement objects are wrapped in "advertisements" field.
     * Advertisement object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/advertisements",
     *     defaults={"_format"="json"},
     * )
     * 
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", default="25", allowBlank=true, nullable=true, requirements="\d+", description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Advertisement List",
     *     section="Advertisement",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Advertisement List"
     *     },
     *     resource=true
     * )
     * 
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAdvertisementAllAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Advertisement')->createQueryBuilder('advertisement')->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('advertisements', $pagination);
    }

    /**
     * Use this method to get Advertisement by Id. Advertisement object documented here was wrapped in "advertisement" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_advertisement",
     *     path="/advertisements/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Advertisement by Id",
     *     section="Advertisement",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Advertisement Id"}
     *     },
     *     statusCodes={
     *         404 = "Advertisement not found. All other errors looks like this",
     *         200 = "Return Advertisement"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Advertisement"
     *     }
     * )
     *
     * @param Advertisement $advertisement
     * @return Response
     */
    public function getAdvertisementAction(Advertisement $advertisement)
    {
        return $this->handleView($this->view(
            [
                'advertisement' => $advertisement,
                'code' => Response::HTTP_OK
            ],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Advertisement by Id. Advertisement object documented here was wrapped in "advertisement" field
     *
     * @Rest\Route(
     *     path="/advertisements/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Advertisement",
     *     input="AppBundle\Form\Type\AdvertisementFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Advertisement Id"}
     *     },
     *     section="Advertisement",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Advertisement"
     *     },
     *     statusCodes={
     *         404 = "Advertisement not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Advertisement $advertisement
     * @param Request $request
     * @return Response
     */
    public function updateProjectAction(Advertisement $advertisement, Request $request)
    {
        return $this->processAdvertisementForm($advertisement, $request);
    }

    /**
     * Use this method to create Advertisement. Advertisement object documented here was wrapped in "advertisement" field
     * 
     * @Rest\Post(
     *     path="/advertisements",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Advertisement",
     *     input="AppBundle\Form\Type\AdvertisementFormType",
     *     section="Advertisement",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Advertisement"
     *     },
     *     statusCodes={
     *         404 = "Advertisement not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createAdvertisementAction(Request $request)
    {
        return $this->processAdvertisementForm(new Advertisement(), $request);
    }

    /**
     * @param Advertisement $advertisement
     * @param Request $request
     * @return Response
     */
    private function processAdvertisementForm(Advertisement $advertisement, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(AdvertisementFormType::class, $advertisement, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();

            $em->persist($advertisement);
            $em->flush();

            $view = $this->view(
                [
                    'advertisement' => $advertisement,
                    'code' => Response::HTTP_OK
                ],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}