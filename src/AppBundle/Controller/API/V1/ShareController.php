<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Enum\SharedContentType;
use FOS\RestBundle\Context\Context;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class ShareController extends BaseController
{
    /**
     * Use this method to return  a part of the object that is required for sharing page. Object is wrapped in "object" field.
     *
     * @Rest\Get(
     *     path="/share",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"Valera"})
     *
     * @ApiDoc(
     *     description="Returns part of the object for sharing",
     *     section="Share object",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Object not found. All other errors looks like this",
     *         200 = "Return Part of the object"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="id",  allowBlank=false, nullable=false, requirements="\d+", description="Content Id")
     * @Rest\QueryParam(name="type", allowBlank=false, nullable=false, requirements="\d+", description="Content type Id")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function shareAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $classes = SharedContentType::getClasses();

        $object = $em->getRepository($classes[$paramFetcher->get('type')])->findOneBy(array('id' => $paramFetcher->get('id')));

        if($object){
            $view = $this->view(['object' => $object], Response::HTTP_OK);
            $view->getContext()->setGroups(array('Share'));
        }else{
            $view = $this->view([], Response::HTTP_NOT_FOUND);
        }

        return $this->handleView($view);
    }
}