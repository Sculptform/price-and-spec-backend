<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Comment\AbstractComment;
use AppBundle\Entity\Comment\DiscussionComment;
use AppBundle\Entity\Image;
use AppBundle\Entity\User;
use AppBundle\Event\DiscussionEvent;
use AppBundle\Event\Events;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\ImageFormType;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class CommentController
 * @package AppBundle\Controller\API\V1
 * @Route("/comments")
 */
class CommentController extends BaseController
{
    /**
     * Use this method to get whole list of feeds. All Comments objects are wrapped in "comments" field.
     * Comment object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Parent Comment Id")
     * @Rest\QueryParam(name="team_id", allowBlank=true, nullable=true, requirements="\d+", description="Team Id of comments")
     * @Rest\QueryParam(name="project_id", allowBlank=true, nullable=true, requirements="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}", description="Project Id of comments")
     * @Rest\QueryParam(name="discussion_id", allowBlank=true, nullable=true, requirements="\d+", description="Discussion Id of comments")
     * @Rest\QueryParam(name="product_sandpit_id", allowBlank=true, nullable=true, requirements="\d+", description="Product Sandpit Id of comments")
     * @Rest\QueryParam(name="gallery_project_id", allowBlank=true, nullable=true, requirements="\d+", description="Gallery Project Id of comments")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(AbstractComment::class)->getListQB(
                $paramFetcher->get('parent_id'),
                $paramFetcher->get('discussion_id'),
                $paramFetcher->get('team_id'),
                $paramFetcher->get('project_id'),
                $paramFetcher->get('product_sandpit_id'),
                $paramFetcher->get('gallery_project_id')
            ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('comments', $pagination);
    }

    /**
     * Use this method to get page in ordered/sorted/filtered list for required comment.
     *
     * @Rest\Get(
     *     path="/{id}/page",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comment List Page",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comment List Page"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Parent Comment Id")
     * @Rest\QueryParam(name="team_id", allowBlank=true, nullable=true, requirements="\d+", description="Team Id of comments")
     * @Rest\QueryParam(name="project_id", allowBlank=true, nullable=true, requirements="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}", description="Project Id of comments")
     * @Rest\QueryParam(name="discussion_id", allowBlank=true, nullable=true, requirements="\d+", description="Discussion Id of comments")
     * @Rest\QueryParam(name="product_sandpit_id", allowBlank=true, nullable=true, requirements="\d+", description="Product Sandpit Id of comments")
     * @Rest\QueryParam(name="gallery_project_id", allowBlank=true, nullable=true, requirements="\d+", description="Gallery Project Id of comments")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     *
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListPage(Request $request, ParamFetcher $paramFetcher)
    {
        $position = $this->getEm()->getRepository(AbstractComment::class)
            ->getPositionInSortedList(
                $request->get('id'),
                $paramFetcher->get('sort'),
                $paramFetcher->get('direction'),
                $paramFetcher->get('parent_id'),
                $paramFetcher->get('discussion_id'),
                $paramFetcher->get('team_id'),
                $paramFetcher->get('project_id'),
                $paramFetcher->get('product_sandpit_id'),
                $paramFetcher->get('gallery_project_id')
            );

        $page = ceil($position / $paramFetcher->get('limit'));

        $view = $this->view([
            'page' => $page,
            'position' => $position
        ], Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Use this method to get Comment by Id. Comment object documented here was wrapped in "comment" field.
     *
     * @Rest\Get(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comment by Id",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     statusCodes={
     *         404 = "Feed not found. All other errors looks like this",
     *         200 = "Return Comment"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\AbstractComment"
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function getAction(AbstractComment $comment)
    {
        return $this->handleView($this->view(
            ['comment' => $comment],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to get Comment Images. Images objects documented here was wrapped in "images" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_comment_images",
     *     path="/{id}/images",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comment Images",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     statusCodes={
     *         404 = "Image not found. All other errors looks like this",
     *         200 = "Return Images"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\CommentImage"
     *     }
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\AbstractComment", options={"id" = "id"})
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function getCommentImagesAction(AbstractComment $comment)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $commentUser = $comment->getUser();

        if(!$currentUser->hasRole('ROLE_ADMIN') && $commentUser->getId() !== $currentUser->getId()){
            throw new AccessDeniedException();
        }

        return $this->handleView($this->view(
            ['images' => $comment->getImages()],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to get Comment Image by Id. Image object documented here was wrapped in "image" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_comment_image",
     *     path="/{id}/images/{image_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+", "image_id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comment Image by Id",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"},
     *      {"name"="image_id", "dataType"="integer", "requirement"="\d+", "description"="Image Id"}
     *     },
     *     statusCodes={
     *         404 = "Image not found. All other errors looks like this",
     *         200 = "Return Images"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\CommentImage"
     *     }
     * )
     *
     * @ParamConverter("image", class="AppBundle\Entity\Image\CommentImage", options={"id" = "image_id"})
     *
     * @param Image\CommentImage $image
     * @return Response
     */
    public function getCommentImageAction(Image\CommentImage $image)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $imageUser = $image->getComment()->getUser();

        if(!$currentUser->hasRole('ROLE_ADMIN') && $imageUser->getId() !== $currentUser->getId()){
            throw new AccessDeniedException();
        }

        return $this->handleView($this->view(
            ['image' => $image],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to create Comment Image. Image object documented here was wrapped in "image" field
     *
     * @Rest\Post(
     *     path="/{id}/images",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment Image",
     *     input="AppBundle\Form\Type\ImageFormType",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\CommentImage"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @param Request $request
     * @return Response
     */
    public function createCommentImageAction(AbstractComment $comment, Request $request)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $commentUser = $comment->getUser();

        if(!$currentUser->hasRole('ROLE_ADMIN') && $commentUser->getId() !== $currentUser->getId()){
            throw new AccessDeniedException();
        }

        $image = new Image\CommentImage();
        $image->setComment($comment);

        return $this->processCommentImageForm($image, $request);
    }

    /**
     * Delete Comment Image.
     *
     * @Rest\Delete(
     *     path="/{id}/images/{image_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+", "image_id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Delete Comment Image by id",
     *     section="Comments",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"},
     *      {"name"="image_id", "dataType"="integer", "requirement"="\d+", "description"="Comment Image Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @ParamConverter("image", class="AppBundle\Entity\Image\CommentImage", options={"id" = "image_id"})
     *
     * @param Image\CommentImage $image
     * @return Response
     */
    public function deleteCommentImageAction(Image\CommentImage $image)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $imageUser = $image->getComment()->getUser();

        if(!$currentUser->hasRole('ROLE_ADMIN') && $imageUser->getId() !== $currentUser->getId()){
            throw new AccessDeniedException();
        }

        $em = $this->getEm();

        $em->remove($image);
        $em->flush();

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param Image\CommentImage $image
     * @param Request $request
     * @return Response
     */
    private function processCommentImageForm(Image\CommentImage $image, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ImageFormType::class, $image, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $image->getComment()->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($image);
            $em->flush();

            $view = $this->view(
                ['image' => $image],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to add current user to liked users
     *
     * @Rest\Route(
     *     path="/{id}/like",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT"}
     * )
     *
     * @ApiDoc(
     *     description="Like comment",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function likeAction(AbstractComment $comment)
    {
        $em = $this->getEm();

        $comment->addLikedUser($this->getUser());

        $em->persist($comment);

        $em->flush();

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to remove current user from liked users
     *
     * @Rest\Route(
     *     path="/{id}/dislike",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"}
     * )
     *
     * @ApiDoc(
     *     description="Like comment",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function dislikeAction(AbstractComment $comment)
    {
        $em = $this->getEm();

        $comment->removeLikedUser($this->getUser());

        $em->persist($comment);

        $em->flush();

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to remove current user from liked users
     *
     * @Rest\Route(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"}
     * )
     *
     * @ApiDoc(
     *     description="Delete comment",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function deleteAction(AbstractComment $comment)
    {
        $em = $this->getEm();

        $em->remove($comment);

        $em->flush();

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to add current comment in spam
     *
     * @Rest\Put(
     *     path="/{id}/in-spam",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Add comment in spam",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function inSpamAction(AbstractComment $comment)
    {
        if(!$comment->getSpammedUser()) {
            $em = $this->getEm();

            $comment->setSpammedUser($this->getUser());

            $em->persist($comment);
            $em->flush();

            // GENERATE DISCUSSION SPAMMED NOTIFICATION //
            if ($comment instanceof DiscussionComment) {
                $this->get('event_dispatcher')->dispatch(
                    Events::DISCUSSION_SET_SPAMMED,
                    new DiscussionEvent(
                        $comment->getDiscussion(),
                        $this->getUser(),
                        Events::DISCUSSION_SET_SPAMMED,
                        $comment->getUser()
                    )
                );
            }
        }

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to remove current comment from spam
     *
     * @Rest\Put(
     *     path="/{id}/un-spam",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Remove comment from spam",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Comment Id"}
     *     },
     *     section="Comments",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractComment $comment
     * @return Response
     */
    public function unSpamAction(AbstractComment $comment)
    {
        if(!$comment->getSpammedUser()) {
            $em = $this->getEm();

            $comment->setSpammedUser(null);

            $em->persist($comment);
            $em->flush();
        }

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }
}