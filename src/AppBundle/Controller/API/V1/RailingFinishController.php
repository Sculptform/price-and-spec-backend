<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class RailingFinishController extends BaseController
{
    /**
     * Use this method to get whole list of finishing materials. All Material Finish objects are wrapped in "railing_finishes" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/railing_finishes",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Application Type")
     * @Rest\QueryParam(name="material_type_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Type")
     * @Rest\QueryParam(name="group_id", allowBlank=true, nullable=true, description="Filter By Finish Group Id")
     *
     * @ApiDoc(
     *     description="Get Railing Finishes list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Railing Finishes List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getRailingFinishesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:RailingFinish')->findAllByParams([
                'materialType' => $paramFetcher->get('material_type_id'),
                'applicationType' => $paramFetcher->get('application_type'),
                'group' => $paramFetcher->get('group_id')
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('railing_finishes', $pagination);
    }

}