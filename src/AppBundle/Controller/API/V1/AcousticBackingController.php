<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\AcousticBacking;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class AcousticBackingController extends BaseController
{
    /**
     * Use this method to get whole list of acoustic backings. All AcousticBackings objects are wrapped in "acoustic_backing" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/acoustic_backings",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, default=null, requirements="\d+", description="Product type id")
     *
     * @ApiDoc(
     *     description="Get AcousticBackings list",
     *     section="AcousticBackings",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return AcousticBackings List"
     *     },
     *     resource=true
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher){
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(AcousticBacking::class)->findAllByParams([
                'product_type'     => $paramFetcher->get('product_type')
            ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('acoustic_backings', $pagination);
    }
}
