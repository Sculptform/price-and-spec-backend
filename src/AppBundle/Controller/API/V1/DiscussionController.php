<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Comment\DiscussionComment;
use AppBundle\Entity\Discussion;
use AppBundle\Entity\DiscussionView;
use AppBundle\Entity\FollowedContent\FollowedDiscussion;
use AppBundle\Entity\Tag;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use AppBundle\Entity\UserFollower;
use AppBundle\Event\DiscussionEvent;
use AppBundle\Event\Events;
use AppBundle\Event\FollowerDiscussionEvent;
use AppBundle\Event\TeamMembersEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\DiscussionFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

class DiscussionController extends BaseController
{
    /**
     * Use this method to get whole list of discussions. All Discussions objects are wrapped in "discussions" field.
     * Discussion object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/discussions",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Discussions",
     *     section="Discussions",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Discussions List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Discussion Type")
     * 
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllDiscussionsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Discussion')->findAllByParams([
                'type' => $paramFetcher->get('type')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('discussions', $pagination);
    }

    /**
     * Use this method to get whole list of tags. All tags objects are wrapped in "tags" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/discussions/tags",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Tags",
     *     section="Discussions",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Tags List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="content", allowBlank=true, nullable=true, requirements="\w+", description="Search By tag content")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllTagsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Tag')->findAllByParams([
                'content' => $paramFetcher->get('content')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('tags', $pagination);
    }

    /**
     * Use this method to get Discussion by Id. Discussion object documented here was wrapped in "discussion" field.
     *
     * @Rest\Get(
     *     path="/discussions/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Discussions by Id",
     *     section="Discussions",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     statusCodes={
     *         404 = "Discussion not found. All other errors looks like this",
     *         200 = "Return Discussion"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Discussion"
     *     }
     * )
     *
     * @param Discussion $discussion
     * @return Response
     */
    public function getDiscussionAction(Discussion $discussion)
    {
        $this->getEm()->getRepository(DiscussionView::class)->create($discussion, $this->getUser());

        return $this->handleView($this->view(
            ['discussion' => $discussion],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Discussion by Id. Discussion object documented here was wrapped in "discussion" field
     *
     * @Rest\Route(
     *     path="/discussions/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Discussion",
     *     input="AppBundle\Form\Type\DiscussionFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     section="Discussions",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Discussion"
     *     },
     *     statusCodes={
     *         404 = "Discussion not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Discussion $discussion
     * @param Request $request
     * @return Response
     */
    public function updateDiscussionAction(Discussion $discussion, Request $request)
    {
        return $this->processDiscussionForm($discussion, $request);
    }

    /**
     * Use this method to create Discussion. Discussion object documented here was wrapped in "discussion" field
     *
     * @Rest\Post(
     *     path="/discussions",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Discussion",
     *     input="AppBundle\Form\Type\DiscussionFormType",
     *     section="Discussions",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Discussion"
     *     },
     *     statusCodes={
     *         404 = "Discussion not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createDiscussionAction(Request $request)
    {
        $discussion = new Discussion();
        $discussion->setUser($this->getUser());

        return $this->processDiscussionForm($discussion, $request);
    }

    /**
     * Delete Discussion .
     *
     * @Rest\Delete(
     *     path="/discussions/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Discussion by id",
     *     section="Discussions",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param Discussion $discussion
     * @return Response
     */
    public function deleteDiscussionAction(Discussion $discussion)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try{

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $discussion->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            $em->remove($discussion);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            throw new ConflictException("Can't delete Discussion - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }


    /**
     * @param Discussion $discussion
     * @param Request $request
     * @return Response
     */
    private function processDiscussionForm(Discussion $discussion, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(DiscussionFormType::class, $discussion, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();
            $extraFields = $form->getExtraData();
            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $discussion->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            // SET TAGS //
            if (isset($extraFields['tags']) and count($extraFields['tags']) > 0) {
                $tagsData = $extraFields['tags'];

                foreach ($tagsData as $tagData) {

                    /** @var Tag $tag */
                    if (!$tag = $em->getRepository(Tag::class)->findOneBy(['content' => $tagData['content']])) {
                        $tag = new Tag();
                    }

                    $tag->setContent($tagData['content']);
                    $tag->addDiscussion($discussion);
                    $em->persist($tag);
                }
            }

            // GENERATE NOTIFICATIONS FOR TEAMS USERS //
            $eventUsersIds = [];
            $teams = $em->getRepository(Team::class)->findAllByParams(['user_id' => $currentUser->getId()]);

            /** @var Team $team */
            foreach ($teams as $team) {
                $users = $team->getConfirmUsers();

                /** @var User $user */
                foreach ($users as $user) {
                    if (array_key_exists($user->getId(), $eventUsersIds))
                        continue;

                    $eventUsersIds[$user->getId()] = $user->getId();
                    $this->get('event_dispatcher')
                        ->dispatch(Events::TEAM_MEMBERS_DISCUSSION_CREATED, new TeamMembersEvent(
                            $team,
                            $user,
                            Events::TEAM_MEMBERS_DISCUSSION_CREATED,
                            $currentUser)
                        );
                }
            }

            // GENERATE NOTIFICATIONS FOR USER FOLLOWERS //
            $eventUsersIds = [];
            $userFollowers = $currentUser->getFollowers();

            /** @var UserFollower $userFollower */
            foreach ($userFollowers as $userFollower) {
                $user = $userFollower->getFollower();

                if (array_key_exists($user->getId(), $eventUsersIds))
                    continue;

                $eventUsersIds[$user->getId()] = $user->getId();
                $this->get('event_dispatcher')
                    ->dispatch(Events::FOLLOWER_DISCUSSION_CREATED, new FollowerDiscussionEvent(
                        $discussion,
                        $currentUser,
                        $user)
                    );
            }

            $em->persist($discussion);
            $em->flush();

            $view = $this->view(
                ['discussion' => $discussion],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of comments. All Comments objects are wrapped in "comments" field.
     * Comment object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="discussions/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Discussions",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Parent Comment Id")
     *
     * @param Discussion $discussion
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getDiscussionCommentsAction(Discussion $discussion, ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(DiscussionComment::class)->findAllByParams([
                'discussion_id' => $discussion->getId(),
                'parent_id'     => $paramFetcher->get('parent_id')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('comments', $pagination);
    }


    /**
     * Use this method to update Discussion Comment by Id. Discussion Comment object documented here was wrapped in "discussion" field
     *
     * @Rest\Route(
     *     path="/discussions/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\DiscussionComment", options={"id" = "comment_id"})
     * 
     * @ApiDoc(
     *     description="Update Discussion Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     section="Discussions",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\DiscussionComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param DiscussionComment $comment
     * @param Request $request
     * @return Response
     */
    public function updateDiscussionCommentAction(DiscussionComment $comment, Request $request)
    {
        return $this->processDiscussionCommentForm($comment, $request);
    }
    
    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="discussions/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\Comment\DiscussionCommentFormType",
     *     section="Discussions",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\DiscussionComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Discussion $discussion
     * @param Request $request
     * @return Response
     */
    public function createDiscussionCommentAction(Discussion $discussion, Request $request)
    {
        $comment = new DiscussionComment();
        $comment->setDiscussion($discussion);
        $comment->setUser($this->getUser());

        return $this->processDiscussionCommentForm($comment, $request);
    }

    /**
     * Delete Comment .
     *
     * @Rest\Delete(
     *     path="/discussions/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\DiscussionComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Discussions",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param DiscussionComment $comment
     * @return Response
     */
    public function deleteDiscussionCommentAction(DiscussionComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try{

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            throw new ConflictException("Can't delete Comment - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param DiscussionComment $comment
     * @param Request $request
     * @return Response
     */
    private function processDiscussionCommentForm(DiscussionComment $comment, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(CommentFormType::class, $comment, ['method' => $method]);

        if ($method == $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            // GENERATE DISCUSSION COMMENTED NOTIFICATIONS //
            $this->get('event_dispatcher')->dispatch(
                Events::DISCUSSION_COMMENTED,
                new DiscussionEvent(
                    $comment->getDiscussion(),
                    $this->getUser(),
                    Events::DISCUSSION_COMMENTED,
                    $comment->getDiscussion()->getUser()
                )
            );

            if ($comment->getParent() and $comment->getParent()->getUser()->getId() !== $comment->getDiscussion()->getUser()->getId()) {
                $this->get('event_dispatcher')->dispatch(
                    Events::DISCUSSION_COMMENTED,
                    new DiscussionEvent(
                        $comment->getDiscussion(),
                        $this->getUser(),
                        Events::DISCUSSION_COMMENTED,
                        $comment->getParent()->getUser()
                    )
                );
            }

            $em->persist($comment);
            
            $em->flush();

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to follow Discussion. Discussion object documented here was wrapped in "discussion" field
     *
     * @Rest\Route(
     *     path="/discussions/{id}/follow",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Follow Discussion",
     *     input="AppBundle\Form\Type\DiscussionFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     section="Discussions",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Discussion"
     *     },
     *     statusCodes={
     *         404 = "Discussion not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Discussion $discussion
     * @param Request $request
     * @return Response
     */
    public function followDiscussionAction(Discussion $discussion, Request $request)
    {
        $em = $this->getEm();
        /**
         * @var User $user
         */
        $user            = $this->getUser();
        $followedContent = $user->getFollowedContent();

        $followedDiscussion = $em->getRepository('AppBundle\\Entity\\FollowedContent\\FollowedDiscussion')->findOneBy(array('discussion' => $discussion));

        if(!$followedDiscussion || !$followedContent->contains($followedDiscussion)){
            $followed = new FollowedDiscussion();

            $followed->setDiscussion($discussion);
            $user    ->addFollowedContent($followed);
            $em      ->persist($followed);
            $em      ->persist($user);
            $em      ->flush();
        };


        $view = $this->view(
            ['discussion' => $discussion],
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to unfollow Discussion. Discussion object documented here was wrapped in "discussion" field
     *
     * @Rest\Route(
     *     path="/discussions/{id}/unfollow",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Follow Discussion",
     *     input="AppBundle\Form\Type\DiscussionFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Discussion Id"}
     *     },
     *     section="Discussions",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Discussion"
     *     },
     *     statusCodes={
     *         404 = "Discussion not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Discussion $discussion
     * @param Request $request
     * @return Response
     */
    public function unfollowDiscussionAction(Discussion $discussion, Request $request)
    {
        $em              = $this->getEm();
        $user            = $this->getUser();
        $followedContent = $user->getFollowedContent();

        $followedDiscussion = $em->getRepository('AppBundle\\Entity\\FollowedContent\\FollowedDiscussion')->findOneBy(array('discussion' => $discussion));

        if($followedContent && $followedContent->contains($followedDiscussion)){
            $em->remove($followedDiscussion);
            $em->flush();
        };

        $view = $this->view(
            ['discussion' => $discussion],
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }
}