<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 25.10.17
 * Time: 21:29
 */

namespace AppBundle\Controller\API\V1;


use AppBundle\Controller\API\BaseController;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialFinishFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class DefaultMaterialFinishController extends BaseController
{
    /**
     * Use this method to get whole list of finishing materials. All Material Finish objects are wrapped in "material_finishes" field.
     * Material Finish object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/default_material_finishes",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Product Type")
     * @Rest\QueryParam(name="material_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Type")
     *
     * @ApiDoc(
     *     description="Get Default Material Finishes list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Default Material Finishes List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getDefaultMaterialFinishesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:DefaultMaterialFinish')->findAllByParams([
                'materialType' => $paramFetcher->get('material_type'),
                'productType' => $paramFetcher->get('product_type')
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('default_material_finishes', $pagination);
    }
}