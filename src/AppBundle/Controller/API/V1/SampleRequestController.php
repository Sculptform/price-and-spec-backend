<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\SampleRequest;
use AppBundle\Entity\User;
use AppBundle\Enum\PardotTrackActionsEnum;
use AppBundle\Enum\PardotUserLastAction;
use AppBundle\Enum\SampleType;
use AppBundle\Event\Events;
use AppBundle\Event\PardotTrackActionEvent;
use AppBundle\Event\SampleRequestEvent;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\SampleRequestFormType;
use AppBundle\Service\PardotService;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class SampleRequestController
 */
class SampleRequestController extends BaseController
{
    /**
     * Use this method to create sample request. SampleRequest object
     * documented here was wrapped in "sample_request" field
     *
     * @Rest\Post(
     *     path="/sample_requests",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create SampleRequest",
     *     input="AppBundle\Form\Type\SampleRequestFormType",
     *     section="SampleRequests",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\SampleRequest"
     *     },
     *     statusCodes={
     *         404 = "SampleRequest not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createSampleRequestAction(Request $request)
    {
        return $this->processSampleRequestForm(new SampleRequest(), $request);
    }

    /**
     * @param SampleRequest $sampleRequest
     * @param Request       $request
     *
     * @return Response
     */
    private function processSampleRequestForm(
        SampleRequest $sampleRequest, Request $request
    ) {
        $method = $request->getMethod();
        $dispatcher = $this->container->get('event_dispatcher');

        $form = $this->createForm(
            SampleRequestFormType::class, $sampleRequest, ['method' => $method]
        );

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if (null === $sampleRequest->getUser()) {
            $sampleRequest->setUser($currentUser);
        }

        if ($method === $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if (null === $sampleRequest->getUser()) {
                $sampleRequest->setUser($currentUser);
            }

            if ( ! $currentUser->hasRole('ROLE_ADMIN')
                && $sampleRequest->getUser()->getId() !== $currentUser->getId()
            ) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();
            $em->persist($sampleRequest);
            $em->flush();

            try {
                $baseURL   = $this->container->getParameter('sculptform_url');
                $designURL = "{$baseURL}/user/designer/{$sampleRequest->getProject()->getId()}";

                /** @var PardotService $pardotService */
                $pardotService = $this->get('app.service.pardot');
                $pardotResponse = $pardotService->sendUserLastAction(
                    $currentUser, PardotUserLastAction::REQUEST_SAMPLES, [
                        'phone'            => $sampleRequest->getPhone(),
                        'company'          => $sampleRequest->getCompanyName(),
                        'notes'            => $sampleRequest->getContent(),
                        'P&S - Action'     => 'Request Sample',
                        'Project Name'     => $sampleRequest->getProjectName(),
                        'Postal Address'   => $sampleRequest->getStreetAddress(),
                        'Project URL'      => $designURL,
                        'Sample Type'      => ucwords(SampleType::getTitles()[$sampleRequest->getSampleType()]),
                    ]
                );

                $this->get('logger')->addInfo($pardotResponse);
            } catch (\Exception $e) {
                $this->get('logger')->addError($e->getMessage());
            }

            $dispatcher->dispatch(
                Events::SAMPLE_REQUEST_RECEIVED,
                new SampleRequestEvent($sampleRequest)
            );

            $view = $this->view(
                ['sample_request' => $sampleRequest],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}
