<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 15.12.16
 * Time: 12:48
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\User;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\Pardot\UserLastActionFormType;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PardotController extends BaseController
{
    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="pardot/user_last_action",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\Pardot\UserLastActionFormType",
     *     section="Pardot",
     *     views={"v1"},
     *     statusCodes={
     *         400 = "Bad request. All other errors looks like this",
     *         204 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function sendUserLastActionAction(Request $request)
    {
        $form = $this->createForm(UserLastActionFormType::class);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if(!$currentUser->getId()){
                throw new AccessDeniedException();
            }

            //Temporary disabled pardot
            $result = true;

            if(false === $result) {
                throw new ConflictException('Pardot Error');
            } else {
                $view = $this->view(null, Response::HTTP_NO_CONTENT);
            }
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}