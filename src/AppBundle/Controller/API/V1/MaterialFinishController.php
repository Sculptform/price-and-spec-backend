<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialFinishFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class MaterialFinishController extends BaseController
{
    /**
     * Use this method to get whole list of finishing materials. All Material Finish objects are wrapped in "material_finishes" field.
     * Material Finish object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_finishes",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Application Type")
     * @Rest\QueryParam(name="material_type_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Type")
     * @Rest\QueryParam(name="group_id", allowBlank=true, nullable=true, description="Filter By Finish Group Id")
     *
     * @ApiDoc(
     *     description="Get Material Finishes list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Finishes List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getMaterialFinishesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:MaterialFinish')->findAllByParams([
                'materialType' => $paramFetcher->get('material_type_id'),
                'applicationType' => $paramFetcher->get('application_type'),
                'group' => $paramFetcher->get('group_id')
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('material_finishes', $pagination);
    }

    /**
     * Use this method to get Material Finish by Id. Material Finish object documented here was wrapped in "material_finish" field.
     * 
     * @Rest\Get(
     *     name="api_v1_get_material_finish",
     *     path="/material_finishes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Finish by Id",
     *     section="Materials",
     *     views={"v1"},
     *      requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Finish not found. All other errors looks like this",
     *         200 = "Return Material Finish"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinish"
     *     }
     * )
     *
     * @param MaterialFinish $materialFinish
     * @return Response
     */
    public function getMaterialFinishAction(MaterialFinish $materialFinish)
    {
        return $this->handleView($this->view(
            ['material_finish' => $materialFinish],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material Finish by Id. Material Finish object documented here was wrapped in "material_finish" field
     * 
     * @Rest\Route(
     *     path="/material_finishes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Finish",
     *     input="AppBundle\Form\Type\MaterialFinishFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinish"
     *     },
     *     statusCodes={
     *         404 = "Material Finish not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param MaterialFinish $materialFinish
     * @param Request $request
     * @return Response
     */
    public function updateMaterialFinishAction(MaterialFinish $materialFinish, Request $request)
    {
        return $this->processMaterialFinishForm($materialFinish, $request);
    }

    /**
     * Use this method to create Material Finish. Material Finish object documented here was wrapped in "material_finish" field
     * 
     * @Rest\Post(
     *     path="/material_finishes",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material Finish",
     *     input="AppBundle\Form\Type\MaterialFinishFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinish"
     *     },
     *     statusCodes={
     *         404 = "Material Finish not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialFinishAction(Request $request)
    {
        return $this->processMaterialFinishForm(new MaterialFinish(), $request);
    }

    /**
     * @param Request $request
     * @param MaterialFinish $materialFinish
     * @return Response
     */
    private function processMaterialFinishForm(MaterialFinish $materialFinish, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(MaterialFinishFormType::class, $materialFinish, ['method' => $request->getMethod()]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($materialFinish);
            $em->flush();

            $view = $this->view(
                ['material_finish' => $materialFinish],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material Finish.
     *
     * @Rest\Delete(
     *     path="/material_finishes/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material Finish by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialFinish $materialFinish
     * @return Response
     */
    public function deleteAction(MaterialFinish $materialFinish)
    {
        $em = $this->getEm();

        try{
            $em->remove($materialFinish);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Finish - there are some relations.";

            if($materialFinish->getProducts()->count()){
                $errorMsg .= " Related Products: ";

                $items = [];

                foreach($materialFinish->getProducts() as $product){
                    /**@var Product $product*/
                    $items[] = $product->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}