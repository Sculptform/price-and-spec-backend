<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\User;
use AppBundle\Enum\PardotUserLastAction;
use AppBundle\Event\Events;
use AppBundle\Enum\UserPosition;
use AppBundle\Event\UserCreatedEvent;
use AppBundle\Service\PardotService;
use FOS\UserBundle\FOSUserEvents;
use AppBundle\Entity\UserProject;
use AppBundle\Entity\UserFollower;
use FOS\UserBundle\Event\FormEvent;
use AppBundle\Event\UserProjectEvent;
use AppBundle\Form\Type\UserFormType;
use AppBundle\Event\UserFollowerEvent;
use FOS\RestBundle\Request\ParamFetcher;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\API\BaseController;
use AppBundle\Form\Type\UserProjectFormType;
use AppBundle\Exception\ValidationException;
use AppBundle\Controller\API\FormErrorsTrait;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use AppBundle\Controller\API\RegisterRestTrait;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations as Rest;

class UserController extends BaseController
{
    use FormErrorsTrait, RegisterRestTrait;

    /**
     * Use this method to retrieve an authorization token. Token objects are wrapped in "token" field.
     *
     * @Rest\Post(
     *     path="/users/login_check",
     *     defaults={"_format"="json"}
     * )
     * @ApiDoc(
     *     description="User login check",
     *     parameters={
     *          {"name"="_username", "dataType"="string", "required"=true, "description"="Username"},
     *          {"name"="_password", "dataType"="string", "required"=true, "description"="Password"},
     *     },
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Authorization Token"
     *     },
     *     resource=true
     * )
     */
    public function loginUserAction()
    {
    }

    /**
     * @param Request $request
     *
     * @Rest\Post(path="/users/register", defaults={"_format"="json"})
     *
     * @return Response
     *
     * @throws \OAuth2\OAuth2ServerException
     */
    public function registerUserAction(Request $request)
    {
        /** @var \FOS\UserBundle\Form\Factory\FactoryInterface */
        $formFactory = $this->get('fos_user.registration.form.factory');

        /** @var \FOS\UserBundle\Model\UserManagerInterface */
        $userManager = $this->get('fos_user.user_manager');

        $dispatcher = $this->get('event_dispatcher');

        $user = $userManager->createUser();
        $user->setEnabled(true);

        $form = $formFactory->createForm(['csrf_protection' => false]);
        $form->setData($user);
        $this->processForm($request, $form);

        $name  = $request->get('name');
        $parts = $this->explodeFirstName($name);

        $firstName = array_shift($parts);
        $lastName  = null;

        if (count($parts) > 0) {
            $lastName = implode(' ', $parts);
        }

        $user->setFirstName($firstName);
        $user->setLastName($lastName);

        $response = null;

        if ($form->isSubmitted() and $form->isValid()) {
            $userManager->updateUser($user);

            $response = $this->createOAuthRequest(
                [
                    'username' => $request->request->get('email'),
                    'password' => $request->request->get('password'),
                ]
            )->getContent();

            $dispatcher->dispatch(Events::USER_CREATED, new UserCreatedEvent($user, 'Create'));
            $this->sendToPardot($user);

            $view = $this->view(json_decode($response, true), Response::HTTP_OK);
        } else {
            $errors = $this->getErrorsFromForm($form);
            $data   = ['form_errors' => $errors];
            $view   = $this->view($data, Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     *
     * @Rest\Post(path="/users/forgot", defaults={"_format"="json"})
     *
     * @return Response
     *
     * @throws \Exception
     */
    public function forgotAction(Request $request)
    {
        $email    = $request->request->get('email', null);
        $response = null;

        if (! $email) {
            $view = $this->view(['form_errors' => ['email' => "Invalid parameters."]], Response::HTTP_BAD_REQUEST);

            return $this->handleView($view);
        }

        $tokenGenerator = $this->get('fos_user.util.token_generator');
        $mailer         = $this->get('app.mailer');
        $userManager    = $this->get('fos_user.user_manager');

        if (null !== $user = $userManager->findUserByUsernameOrEmail($email)) {
            if (null === $user->getConfirmationToken()) {
                $user->setConfirmationToken($tokenGenerator->generateToken());
            }

            $mailer->sendResettingEmailMessage($user);
            $user->setPasswordRequestedAt(new \DateTime());
            $userManager->updateUser($user);

            $view = $this->view([], Response::HTTP_OK);
        } else {
            $message  = sprintf('User with %s does not exist', $email);
            $response = [
                'form_errors' => ['email' => $message],
            ];

            $view = $this->view($response, Response::HTTP_BAD_REQUEST);
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     *
     * @Rest\Post(path="/users/token-check", name="users.reset.token_check", defaults={"_format"="json"})
     *
     * @return Response
     */
    public function tokenAction(Request $request)
    {
        $view  = $this->view([], Response::HTTP_OK);
        $token = $request->request->get('token', null);

        if (! $token) {
            $view
                ->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setData(['form_errors' => ['token' => 'Token is empty.']]);

            return $this->handleView($view);
        }

        $userManager = $this->get('fos_user.user_manager');
        $user        = $userManager->findUserByConfirmationToken($token);

        if (! $user) {
            $view
                ->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setData(
                    [
                        'form_errors' => [
                            'token' => 'This link is invalid or has already been used, please try sending a new Forgot password request.',
                        ],
                    ]
                );
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     *
     * @Rest\Post(path="/users/new-password", name="users.reset", defaults={"_format"="json"})
     *
     * @return Response
     */
    public function resetAction(Request $request)
    {
        $password = $request->request->get('password');
        $password = trim($password);
        $token    = $request->request->get('token');

        $view = $this->view([], Response::HTTP_OK);

        if (strlen($password) < 8) {
            $view
                ->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setData(
                    [
                        'form_errors' => [
                            'password' => 'The password is too short. The password must be at least 8 characters long.',
                        ],
                    ]
                );

            return $this->handleView($view);
        }

        $userManager = $this->get('fos_user.user_manager');
        $user        = $userManager->findUserByConfirmationToken($token);

        if (null !== $user) {
            $user->setPlainPassword($password);
            $user->setConfirmationToken(null);
            $user->setPasswordRequestedAt(null);
            $user->setEnabled(true);
            $userManager->updateUser($user);

            $view->setData(['email' => $user->getEmail(), 'password' => $password]);
        } else {
            $view
                ->setStatusCode(Response::HTTP_BAD_REQUEST)
                ->setData(['form_errors' => ['email' => 'User Not Found.']]);
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of users. All Users objects are wrapped in "users" field.
     * Users object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/users",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Users",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Users List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page
     *                               of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     * @Rest\QueryParam(name="email", allowBlank=true, nullable=true, description="Filter By User Email")
     * @Rest\QueryParam(name="following", allowBlank=true, nullable=true, requirements="\d+", description="Following By
     *                                    User Id")
     * @Rest\QueryParam(name="fans", allowBlank=true, nullable=true, requirements="\d+", description="Fans By User Id")
     * @Rest\QueryParam(name="name", allowBlank=true, nullable=true,  description="Filter By User name")
     * @Rest\QueryParam(name="online", allowBlank=true, nullable=true, description="Filter By Online Users")
     * @Rest\QueryParam(name="offline", allowBlank=true, nullable=true, description="Filter By Offline Users")
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getUsersAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(User::class)->findAllByParams(
                [
                    'email'     => $paramFetcher->get('email'),
                    'online'    => $paramFetcher->get('online'),
                    'offline'   => $paramFetcher->get('offline'),
                    'following' => $paramFetcher->get('following'),
                    'fans'      => $paramFetcher->get('fans'),
                    'name'      => $paramFetcher->get('name'),
                ]
            ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('users', $pagination);
    }

    /**
     * Use this method to get User by Id. User object documented here was wrapped in "users" field.
     *
     * @Rest\Get(
     *     path="/users/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @View(serializerGroups={"Profile"})
     *
     * @ApiDoc(
     *     description="Get User by Id",
     *     section="Users",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="User Id"}
     *     },
     *     statusCodes={
     *         404 = "User not found. All other errors looks like this",
     *         200 = "Return User"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "groups"={"Profile"},
     *         "class"="AppBundle\Entity\User"
     *     }
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function getUserAction(User $user)
    {
        $view = $this->view(['user' => $user], Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole current user. All Users objects are wrapped in "users" field.
     * Users object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     name="api_v1_get_current_user",
     *     path="/users/current",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"Profile"})
     *
     * @ApiDoc(
     *     description="Get current User",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return current User"
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "groups"={"Profile"},
     *         "class"="AppBundle\Entity\User"
     *     },
     *     resource=true
     * )
     * @return Response
     */
    public function getCurrentUserAction()
    {
        $user = $this->getUser();

        $view = $this->view(['user' => $user], Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole current user notifications settings.
     *
     * @Rest\Get(
     *     path="/notifications/settings",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"notifications"})
     *
     * @ApiDoc(
     *     description="Get Current User Notifications Settings",
     *     section="Notifications",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Current User Notifications Settings"
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "groups"={"notifications"},
     *         "class"="AppBundle\Entity\User"
     *     },
     *     resource=false
     * )
     * @return Response
     */
    public function getCurrentUserNotificationsSettingsAction()
    {
        $view = $this->view($this->getUser(), Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Use this method to update current User. User object documented here was wrapped in "users" field
     *
     * @Rest\Patch(
     *     path="/users/current",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"Profile"})
     *
     * @ApiDoc(
     *     description="Update Current User Field By Field",
     *     input="AppBundle\Form\Type\UserFormType",
     *     section="Users",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "groups"={"Profile"},
     *         "class"="AppBundle\Entity\User"
     *     },
     *     statusCodes={
     *         404 = "User not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function patchCurrentUserAction(Request $request)
    {
        /** @var User $user */
        $user = $this->getUser();

        $form = $this->createForm(
            UserFormType::class,
            $user,
            ['method' => $request->getMethod(), 'validation_groups' => ['settings']]
        );

        $form->submit($request->request->get($form->getName()), false);

        if ($form->isValid()) {
            // No need to update Woodform user, since Drupal doesn't work anymore
            //$this->get('app.service.woodform')->updateWoodformUserByUser($user);

            $data = json_decode($request->getContent(), true);

            $name  = $data['user']['first_name'];
            $parts = $this->explodeFirstName($name);

            $firstName = array_shift($parts);
            $lastName  = null;

            if (count($parts) > 0) {
                $lastName = implode(' ', $parts);
            }

            $user->setFirstName($firstName);
            $user->setLastName($lastName);

            $this->get('fos_user.user_manager')->updateUser($user, true);

            $this->get('event_dispatcher')->dispatch(Events::USER_CREATED, new UserCreatedEvent($user, 'Update'));

            $view = $this->view(['user' => $this->getUser()], Response::HTTP_OK);
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to create User Projects. User Projects object documented here was wrapped in "user_projects"
     * field
     *
     * @Rest\Post(
     *     path="/users/{id}/projects",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Add Project to User",
     *     section="Users",
     *     views={"v1"},
     *     input="AppBundle\Form\Type\UserProjectFormType",
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\UserProject"
     *     },
     *     statusCodes={
     *         404 = "UserProject not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param User    $user
     * @param Request $request
     *
     * @return Response
     */
    public function addUserProjectsAction(User $user, Request $request)
    {
        $userProject = new UserProject();

        $userProject->setUser($user);

        $form = $this->createForm(UserProjectFormType::class, $userProject);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEm();

            $em->persist($userProject);
            $em->flush();

            $this->get('event_dispatcher')
                ->dispatch(Events::PROJECT_SHARED, new UserProjectEvent($userProject));

            $view = $this->view(
                null,
                Response::HTTP_NO_CONTENT
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to set current user as follower of requested user.
     *
     * @Rest\Route(
     *     path="/users/current/followings/{id}",
     *     defaults={"_format"="json"},
     *     methods={"PUT"}
     * )
     * @ApiDoc(
     *     description="Follow user",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "User not found. All other errors looks like this",
     *         204 = "Success"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function followAction(User $user)
    {
        $userFollower = new UserFollower();

        $userFollower
            ->setUser($user)
            ->setFollower($this->getUser());

        $em = $this->getEm();
        $em->persist($userFollower);
        $em->flush();

        $this->get('event_dispatcher')->dispatch(Events::USER_FOLLOWED, new UserFollowerEvent($userFollower));

        return Response::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Use this method to remove current user from followers of requested user.
     *
     * @Rest\Route(
     *     path="/users/current/followings/{id}",
     *     defaults={"_format"="json"},
     *     methods={"DELETE"}
     * )
     * @ApiDoc(
     *     description="Unfollow user",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "User not found. All other errors looks like this",
     *         204 = "Success"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function unfollowAction(User $user)
    {
        $em = $this->getEm();

        /** @var UserFollower $userFollower */
        $userFollower = $em->getRepository(UserFollower::class)->findOneBy(
            [
                'user'     => $user,
                'follower' => $this->getUser(),
            ]
        );

        $em->remove($userFollower);
        $em->flush();

        return Response::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Use this method to remove user from followers of current user.
     *
     * @Rest\Delete(
     *     path="/users/current/followers/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     * @ApiDoc(
     *     description="Remove UserFollower",
     *     input="AppBundle\Form\Type\UserFollowerFormType",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "User not found. All other errors looks like this",
     *         204 = "Success"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param User $user
     *
     * @return Response
     */
    public function removeFollowerAction(User $user)
    {
        $em = $this->getEm();

        /** @var UserFollower $userFollower */
        $userFollower = $em->getRepository(UserFollower::class)->findOneBy(
            [
                'user'     => $this->getUser(),
                'follower' => $user,
            ]
        );

        $em->remove($userFollower);
        $em->flush();

        return Response::create(null, Response::HTTP_NO_CONTENT);
    }

    /**
     * Use this method to get whole list of user positions. All Users objects are wrapped in "user_positions" field.
     *
     * @Rest\Get(
     *     path="/users/positions",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page
     *                               of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get User positions",
     *     section="Users",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Users positions List"
     *     }
     * )
     *
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getUserPositionsAction(ParamFetcher $paramFetcher)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            UserPosition::getChoices(true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('user_positions', $pagination);
    }

    /**
     * @param User $user
     */
    private function sendToPardot(User $user)
    {
        try {
            /** @var PardotService $pardotService */
            $pardotService  = $this->get('app.service.pardot');
            $pardotResponse = $pardotService
                ->sendUserLastAction(
                    $user,
                    PardotUserLastAction::CREATE_ACCOUNT,
                    [
                        'P&S - Action' => 'Create Account',
                    ]
                );
            $this->get('logger')->addInfo($pardotResponse);
        } catch (\Exception $e) {
            $this->get('logger')->addError($e->getMessage());
        }
    }
}
