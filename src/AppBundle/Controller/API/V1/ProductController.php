<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\Product;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\ProductFormType;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class ProductController extends BaseController
{
    /**
     *
     * Use this method to get whole list of products. All Products objects are
     * wrapped in "products" field. Product object description can be found in
     * "Get by Id" method. Also output contains "meta" field with pagination
     * info
     *
     * @Rest\Get(
     *     name="api_v1_get_products",
     *     path="/products",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     * @Rest\QueryParam(name="material_id", allowBlank=true, nullable=true,
     *                                      requirements="\d+",
     *                                      description="Filter By Material
     *                                      Id")
     * @Rest\QueryParam(name="material_finish_id", allowBlank=true,
     *                                             nullable=true,
     *                                             requirements="\d+",
     *                                             description="Filter By
     *                                             Material Finish Id")
     * @Rest\QueryParam(name="material_shape_id", allowBlank=true,
     *                                            nullable=true,
     *                                            requirements="\d+",
     *                                            description="Filter By
     *                                            Material Shape Id")
     * @Rest\QueryParam(name="material_type_id", allowBlank=true,
     *                                           nullable=true,
     *                                           requirements="\d+",
     *                                           description="Filter By
     *                                           Material Type Id")
     *
     * @ApiDoc(
     *     description="Get Products list",
     *     section="Products",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Products List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getProductsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $finish = $em->getRepository(MaterialFinish::class)->find(
            $paramFetcher->get('material_finish_id')
        );
        $shape  = $em->getRepository(MaterialShape::class)->find(
            $paramFetcher->get('material_shape_id')
        );

        $error = '';

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Product')->findAllByParams(
                [
                    'material' => $paramFetcher->get('material_id'),
                    'materialFinish' => $paramFetcher->get(
                        'material_finish_id'
                    ),
                    'materialShape' => $paramFetcher->get('material_shape_id'),
                    'materialType' => $paramFetcher->get('material_type_id'),
                ], true
            ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        if ($finish && $shape && $pagination->count() == 0) {
            $error = sprintf(
                '%s profiles not available in %s', $shape->getTitle(),
                $finish->getTitle()
            );
        }

        return $this->paginationResponse('products', $pagination, $error);
    }

    /**
     * Use this method to get Product by Id. Product object documented here was
     * wrapped in "product" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_product",
     *     path="/products/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Product by Id",
     *     section="Products",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+",
     *      "description"="Product Id"}
     *     },
     *     statusCodes={
     *         404 = "Product not found. All other errors looks like this",
     *         200 = "Return Product"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Product"
     *     }
     * )
     *
     * @param Product $product
     *
     * @return Response
     */
    public function getProductAction(Product $product)
    {
        return $this->handleView(
            $this->view(
                ['product' => $product],
                Response::HTTP_OK
            )
        );
    }

    /**
     * Use this method to update Product by Id. Product object documented here
     * was wrapped in "product" field
     *
     * @Rest\Put(
     *     path="/products/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update Product",
     *     input="AppBundle\Form\Type\ProductFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+",
     *      "description"="Product Id"}
     *     },
     *     section="Products",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Product"
     *     },
     *     statusCodes={
     *         404 = "Product not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Product $product
     * @param Request $request
     *
     * @return Response
     */
    public function updateProductAction(Product $product, Request $request)
    {
        return $this->processProductForm($product, $request);
    }

    /**
     * Use this method to create Product. Product object documented here was
     * wrapped in "product" field
     *
     * @Rest\Post(
     *     path="/products",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Product",
     *     input="AppBundle\Form\Type\ProductFormType",
     *     section="Products",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Product"
     *     },
     *     statusCodes={
     *         404 = "Product not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createProductAction(Request $request)
    {
        return $this->processProductForm(new Product(), $request);
    }

    /**
     * @param Request $request
     * @param Product $product
     *
     * @return Response
     */
    private function processProductForm(Product $product, Request $request)
    {
        $form = $this->createForm(
            ProductFormType::class, $product,
            ['method' => $request->getMethod()]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($product);
            $em->flush();

            $view = $this->view(
                ['product' => $product],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Product.
     *
     * @Rest\Delete(
     *     path="/products/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Product by id",
     *     section="Products",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+",
     *      "description"="Product Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *     },
     * )
     *
     * @param Product $product
     *
     * @return Response
     */
    public function deleteAction(Product $product)
    {
        $em = $this->getEm();

        $em->remove($product);
        $em->flush();


        return $this->handleView(
            $this->view(
                null,
                Response::HTTP_NO_CONTENT
            )
        );
    }
}
