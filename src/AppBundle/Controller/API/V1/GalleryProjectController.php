<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Comment\GalleryProjectComment;
use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use AppBundle\Event\Events;
use AppBundle\Event\GalleryProjectEvent;
use AppBundle\Event\TeamMembersEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\GalleryProjectFormType;
use AppBundle\Form\Type\ImageFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use AppBundle\Entity\Image\GalleryProjectImage;

/**
 * Class GalleryProjectController
 * @package AppBundle\Controller\API\V1
 * @Route("/gallery_projects")
 */
class GalleryProjectController extends BaseController
{
    /**
     * Use this method to get whole list of gallery projects. All Gallery Project objects are wrapped in "gallery_projects" field.
     * GalleryProject object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"}, serializerEnableMaxDepthChecks=true)
     *
     * @ApiDoc(
     *     description="Get Gallery Projects",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Gallery Projects List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="User Id")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, requirements="\d+", description="Product type Id")
     * @Rest\QueryParam(name="material_type", allowBlank=true, nullable=true, requirements="\d+", description="Material type Id")
     * @Rest\QueryParam(name="application", allowBlank=true, nullable=true, requirements="\d+", description="Application Id")
     * @Rest\QueryParam(name="sector", allowBlank=true, nullable=true, requirements="\d+", description="Sector Id")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="id", requirements="id|createdAt|", description="Sort field")
     * @Rest\QueryParam(name="title", allowBlank=true, nullable=true, description="Filter By Gallery Title")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="desc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $userId = intval($paramFetcher->get('user_id'));

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(GalleryProject::class)->findAllByParams(
                array(
                    'user' => $userId,
                    'title' => $paramFetcher->get('title'),
                    'product' => $paramFetcher->get('product_type'),
                    'material' => $paramFetcher->get('material_type'),
                    'application' => $paramFetcher->get('application'),
                    'sector' => $paramFetcher->get('sector')

                )
            ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit'),
            [
                'defaultSortFieldName' => 'id',
                'defaultSortDirection' => 'desc'
            ]
        );

        return $this->paginationResponse('gallery_projects', $pagination);
    }

    /**
     * Use this method to create gallery project. Gallery project object documented here was wrapped in "gallery_project" field
     *
     * @Rest\Post(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Gallery Project",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\GalleryProject"
     *     },
     *     statusCodes={
     *         404 = "Gallery project not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createGalleryProjectAction(Request $request)
    {
        $galleryProject = new GalleryProject();

        $galleryProject->setUser($this->getUser());

        return $this->processGalleryProjectForm($galleryProject, $request);
    }

    /**
     * Use this method to update GalleryProject by Id. Gallery project object documented here was wrapped in "gallery_project" field
     *
     * @Rest\Route(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Gallery Project",
     *     input="AppBundle\Form\Type\GalleryProjectFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Gallery Project Id"}
     *     },
     *     section="Gallery Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\GalleryProject"
     *     },
     *     statusCodes={
     *         404 = "Gallery project not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param GalleryProject $galleryProject
     * @param Request $request
     * @return Response
     */
    public function updateGalleryProjectAction(GalleryProject $galleryProject, Request $request)
    {
        return $this->processGalleryProjectForm($galleryProject, $request);
    }

    /**
     * Use this method to get Gallery Project by Id. Gallery project object documented here was wrapped in "gallery_project" field.
     *
     * @Rest\Get(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Gallery Project by Id",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Gallery Project Id"}
     *     },
     *     statusCodes={
     *         404 = "Gallery project not found. All other errors looks like this",
     *         200 = "Return GalleryProject"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\GalleryProject"
     *     }
     * )
     *
     * @param GalleryProject $galleryProject
     * @return Response
     */
    public function getGalleryProjectAction(GalleryProject $galleryProject)
    {
        $view = $this->view(
            ['gallery_project' => $galleryProject],
            Response::HTTP_OK
        );

        return $this->handleView($view);
    }

    /**
     * Delete Gallery Project.
     *
     * @Rest\Delete(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Gallery Project by id",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Gallery Project Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param GalleryProject $galleryProject
     * @return Response
     */
    public function deleteGalleryProjectAction(GalleryProject $galleryProject)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {
            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $galleryProject->getUser()->getId(
                    ) !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->remove($galleryProject);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Gallery Project - there are some relations.");
        }

        return $this->handleView(
            $this->view(
                null,
                Response::HTTP_NO_CONTENT
            )
        );
    }

    /**
     * @param GalleryProject $galleryProject
     * @param Request $request
     * @return Response
     */
    private function processGalleryProjectForm(GalleryProject $galleryProject, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(GalleryProjectFormType::class, $galleryProject, ['method' => $method]);

        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $currentUser = $this->getUser();
            $em = $this->getEm();

            $teams = $em->getRepository(Team::class)->getTeamsByUser($currentUser);

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $galleryProject->getUser()->getId(
                    ) !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->persist($galleryProject);
            $em->flush();

            // GENERATE NOTIFICATIONS FOR TEAMS USERS //
            $eventUsersIds = [];
            /** @var Team $team */
            foreach ($teams as $team) {
                $users = $team->getConfirmUsers();

                /** @var User $user */
                foreach ($users as $user) {
                    if (array_key_exists($user->getId(), $eventUsersIds))
                        continue;

                    $eventUsersIds[$user->getId()] = $user->getId();

                    $this->get('event_dispatcher')
                        ->dispatch(Events::TEAM_MEMBERS_GALLERY_PROJECT_CREATED,
                            new TeamMembersEvent(
                                $team,
                                $user,
                                Events::TEAM_MEMBERS_GALLERY_PROJECT_CREATED,
                                $currentUser
                            )
                        );
                }
            }

            $view = $this->view(
                ['gallery_project' => $galleryProject],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to create Gallery Project Image. Gallery Project Image object documented here was wrapped in "image" field
     *
     * @Rest\Post(
     *     path="/{id}/images",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Add Image",
     *     input="AppBundle\Form\Type\ImageFormType",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Gallery Project Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\GalleryProjectImage"
     *     },
     *     statusCodes={
     *         404 = "Team not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     },
     *     resource=true
     * )
     *
     * @param GalleryProject $galleryProject
     * @param Request $request
     * @return Response
     */
    public function createGalleryProjectImageAction(GalleryProject $galleryProject, Request $request)
    {
        $image = new GalleryProjectImage();
        $image->setGalleryProject($galleryProject);

        return $this->processGalleryProjectImageForm($image, $request);
    }

    /**
     * @param GalleryProjectImage $image
     * @param Request $request
     * @return Response
     */
    private function processGalleryProjectImageForm(GalleryProjectImage $image, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ImageFormType::class, $image, ['method' => $method]);

        $image->setImageFile($request->request->get('content'));
        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $image->getGalleryProject()->getUser(
                    )->getId() !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->persist($image);
            $em->flush();

            $view = $this->view(
                ['image' => $image],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of comments. All Comments objects are wrapped in "comments" field.
     * Comment object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Parent Comment Id")
     *
     * @param GalleryProject $galleryProject
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getCommentsAction(GalleryProject $galleryProject, ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(GalleryProjectComment::class)->findAllByParams([
                'gallery_project_id' => $galleryProject->getId(),
                'parent_id' => $paramFetcher->get('parent_id')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('comments', $pagination);
    }

    /**
     * Use this method to update GalleryProject Comment by Id. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Route(
     *     path="/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\GalleryProjectComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Update GalleryProject Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"},
     *     },
     *     section="Gallery Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\GalleryProjectComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param GalleryProjectComment $comment
     * @param Request $request
     * @return Response
     */
    public function updateCommentAction(GalleryProjectComment $comment, Request $request)
    {
        return $this->processCommentForm($comment, $request);
    }

    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\GalleryProjectComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param GalleryProject $galleryProject
     * @param Request $request
     * @return Response
     */
    public function createCommentAction(GalleryProject $galleryProject, Request $request)
    {
        $comment = new GalleryProjectComment();
        $comment->setGalleryProject($galleryProject);
        $comment->setUser($this->getUser());

        return $this->processCommentForm($comment, $request);
    }

    /**
     * Delete Comment
     *
     * @Rest\Delete(
     *     path="/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\GalleryProjectComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Gallery Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param GalleryProjectComment $comment
     * @return Response
     */
    public function deleteCommentAction(GalleryProjectComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Comment - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param GalleryProjectComment $comment
     * @param Request $request
     * @return Response
     */
    private function processCommentForm(GalleryProjectComment $comment, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(CommentFormType::class, $comment, ['method' => $method]);

        if ($method == $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($comment);
            $em->flush();

            $this->get('event_dispatcher')
                ->dispatch(Events::GALLERY_PROJECT_COMMENT,
                    new GalleryProjectEvent(
                        $comment->getGalleryProject(),
                        $this->getUser(),
                        Events::GALLERY_PROJECT_COMMENT,
                        $comment->getGalleryProject()->getUser()
                    )
                );

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}