<?php
/**
 * Created by PhpStorm.
 * User: dreyup
 * Date: 24.04.17
 * Time: 10:41
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\News;
use AppBundle\Event\Events;
use AppBundle\Event\NewsEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\NewsFormType;
use Doctrine\Common\Collections\Criteria;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class NewsFeedController extends BaseController
{
    /**
     * Use this method to get whole list of news feed. All NewsFeed objects are wrapped in "news_feeds" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/news_feed",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get NewsFeed list",
     *     section="NewsFeed",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return NewsFeed List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllNewsFeedAction(ParamFetcher $paramFetcher)
    {
        $service = $this->get('app.service.woodform');

        $newsFeedList = $service->getNewsListData();

        $pagination = $this->get('knp_paginator')->paginate(
            $newsFeedList,
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('news_feeds', $pagination);
    }
}