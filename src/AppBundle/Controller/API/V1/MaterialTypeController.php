<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialType;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialTypeFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class MaterialTypeController extends BaseController
{
    /**
     * Use this method to get whole list of material types. All Material Types objects are wrapped in "material_types" field.
     * Material Type object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_types",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Types",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Types List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="has_coatings", allowBlank=true, nullable=true, description="Filter by existing of Material Coatings")
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getMaterialTypesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(MaterialType::class)->findAllByParams([
                'has_coatings' => $paramFetcher->get('has_coatings')
            ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('material_types', $pagination);
    }

    /**
     * Use this method to get Material Type by Id. Material Type object documented here was wrapped in "material_type" field.
     * 
     * @Rest\Get(
     *     name="api_v1_get_material_type",
     *     path="/material_types/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Type by Id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Type Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Type not found. All other errors looks like this",
     *         200 = "Return Material Type"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialType"
     *     }
     * )
     *
     * @param MaterialType $materialType
     * @return Response
     */
    public function getMaterialTypeAction(MaterialType $materialType)
    {
        return $this->handleView($this->view(
            ['material_type' => $materialType],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material Type by Id. Material Type object documented here was wrapped in "material_type" field
     * 
     * @Rest\Put(
     *     path="/material_types/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Type",
     *     input="AppBundle\Form\Type\MaterialTypeFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Type Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialType"
     *     },
     *     statusCodes={
     *         404 = "Material Type not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param MaterialType $materialType
     * @param Request $request
     * @return Response
     */
    public function updateMaterialTypeAction(MaterialType $materialType, Request $request)
    {
        return $this->processMaterialTypeForm($materialType, $request);
    }

    /**
     * Use this method to create Material Type. Material Type object documented here was wrapped in "material_type" field
     * 
     * @Rest\Post(
     *     path="/material_types",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Type",
     *     input="AppBundle\Form\Type\MaterialTypeFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialType"
     *     },
     *     statusCodes={
     *         404 = "Material Type not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialTypeAction(Request $request)
    {
        return $this->processMaterialTypeForm(new MaterialType(), $request);
    }

    /**
     * @param MaterialType $materialType
     * @param Request $request
     * @return Response
     */
    private function processMaterialTypeForm(MaterialType $materialType, Request $request)
    {
        $form = $this->createForm(MaterialTypeFormType::class, $materialType, ['method' => $request->getMethod()]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($materialType);
            $em->flush();

            $view = $this->view(
                ['material_type' => $materialType],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material Type.
     *
     * @Rest\Delete(
     *     path="/material_types/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material Type by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Type Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialType $materialType
     * @return Response
     */
    public function deleteAction(MaterialType $materialType)
    {
        $em = $this->getEm();

        try{
            $em->remove($materialType);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Type - there are some relations.";

            if($materialType->getMaterialFinishes()->count()){
                $errorMsg .= " Related Material Finishes: ";

                $items = [];

                foreach($materialType->getMaterialFinishes() as $materialFinish){
                    /**@var MaterialFinish $materialFinish*/
                    $items[] = $materialFinish->getGroup().' '. $materialFinish->getTitle();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            if($materialType->getMaterials()->count()){
                $errorMsg .= " Related Materials Ids: ";

                $items = [];

                foreach($materialType->getMaterials() as $material){
                    /**@var Material $material*/
                    $items[] = $material->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}