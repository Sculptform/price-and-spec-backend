<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialShapeSizeFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class MaterialShapeSizeController extends BaseController
{
    /**
     * Use this method to get whole list of material shapes. All Material Shape objects are wrapped in "material_shapes" field.
     * Material Shape object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_shape_sizes",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Material Shapes list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Shapes List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getMaterialShapeSizesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:MaterialShapeSize')
                ->createQueryBuilder('MaterialShapeSize')
                ->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('material_shape_sizes', $pagination);
    }

    /**
     * Use this method to get Material Shape by Id. Material Shape object documented here was wrapped in "material_shape" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_material_shape_size",
     *     path="/material_shape_sizes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Shape by Id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found. All other errors looks like this",
     *         200 = "Return Material Shape"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     }
     * )
     *
     * @param MaterialShapeSize $MaterialShapeSize
     * @return Response
     */
    public function getMaterialShapeSizeAction(MaterialShapeSize $MaterialShapeSize)
    {
        return $this->handleView($this->view(
            ['material_shape_size' => $MaterialShapeSize],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material Shape by Id. Material Shape object documented here was wrapped in "material_shape" field
     *
     * @Rest\Route(
     *     path="/material_shape_sizes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Shape",
     *     input="AppBundle\Form\Type\MaterialShapeSizeFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     *
     * )
     *
     * @param MaterialShapeSize $MaterialShapeSize
     * @param Request $request
     * @return Response
     */
    public function updateMaterialShapeSizeAction(MaterialShapeSize $MaterialShapeSize, Request $request)
    {
        return $this->processMaterialShapeSizeForm($MaterialShapeSize, $request);
    }

    /**
     * Use this method to create Material Shape. Material Shape object documented here was wrapped in "material_shape" field
     *
     * @Rest\Post(
     *     path="/material_shape_sizes",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material Shape",
     *     input="AppBundle\Form\Type\MaterialShapeSizeFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialShapeSizeAction(Request $request)
    {
        return $this->processMaterialShapeSizeForm(new MaterialShapeSize(), $request);
    }

    /**
     * @param Request $request
     * @param MaterialShapeSize $MaterialShapeSize
     * @return Response
     */
    private function processMaterialShapeSizeForm(MaterialShapeSize $MaterialShapeSize, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(MaterialShapeSizeFormType::class, $MaterialShapeSize, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($MaterialShapeSize);
            $em->flush();

            $view = $this->view(
                ['material_shape' => $MaterialShapeSize],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material Shape.
     *
     * @Rest\Delete(
     *     path="/material_shape_sizes/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material ShapeSize by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material ShapeSize Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialShapeSize $materialShapeSize
     * @return Response
     */
    public function deleteMaterialShapeSizeAction(MaterialShapeSize $materialShapeSize)
    {
        $em = $this->getEm();

        try{
            $em->remove($materialShapeSize);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Shape Size - there are some relations.";

            if($materials = $materialShapeSize->getMaterials()){
                $errorMsg .= " Related Materials : ";

                $items = [];

                foreach ($materials as $material){
                    /**@var Material $material*/
                    if (!in_array($material->getId(), $items)){
                        $items[] = $material->getId();
                    }
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

}