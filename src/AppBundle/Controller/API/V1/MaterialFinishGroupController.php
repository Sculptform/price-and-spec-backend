<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialFinishGroupFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class MaterialFinishGroupController extends BaseController
{
    /**
     * Use this method to get whole list of finishing material groups.
     * All Material Finish Group objects are wrapped in "material_finish_groups" field.
     * Material Finish Group object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_finish_groups",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="material_type_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Type")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter included materials By Application Type")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Product type Id")
     *
     * @ApiDoc(
     *     description="Get Material Finish Groups list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Finishes Groups List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getMaterialFinishGroupsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(MaterialFinishGroup::class)->findAllByParams([
                'materialType' => $paramFetcher->get('material_type_id'),
                'applicationType' => $paramFetcher->get('application_type'),
                'productType' => $paramFetcher->get('product_type'),
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('material_finish_groups', $pagination);
    }

    /**
     * Use this method to get Material Finish Group by Id.
     * Material Finish Group object documented here was wrapped in "material_finish_group" field.
     * 
     * @Rest\Get(
     *     name="api_v1_get_material_finish_group",
     *     path="/material_finish_groups/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Finish Group by Id",
     *     section="Materials",
     *     views={"v1"},
     *      requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Group Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Finish Group not found. All other errors looks like this",
     *         200 = "Return Material Finish Group"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinishGroup"
     *     }
     * )
     *
     * @param MaterialFinishGroup $materialFinishGroup
     * @return Response
     */
    public function getMaterialFinishGroupAction(MaterialFinishGroup $materialFinishGroup)
    {
        return $this->handleView($this->view(
            ['material_finish_group' => $materialFinishGroup],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material Finish Group by Id.
     * Material Finish Group object documented here was wrapped in "material_finish_group" field
     * 
     * @Rest\Route(
     *     path="/material_finish_groups/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Finish Group",
     *     input="AppBundle\Form\Type\MaterialFinishGroupFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Group Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinishGroup"
     *     },
     *     statusCodes={
     *         404 = "Material Finish Group not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param MaterialFinishGroup $materialFinishGroup
     * @param Request $request
     * @return Response
     */
    public function updateMaterialFinishGroupAction(MaterialFinishGroup $materialFinishGroup, Request $request)
    {
        return $this->processMaterialFinishGroupForm($materialFinishGroup, $request);
    }

    /**
     * Use this method to create Material Finish Group.
     * Material Finish Group object documented here was wrapped in "material_finish_group" field
     * 
     * @Rest\Post(
     *     path="/material_finish_groups",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material Finish Group",
     *     input="AppBundle\Form\Type\MaterialFinishGroupFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialFinishGroup"
     *     },
     *     statusCodes={
     *         404 = "Material Finish Group not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialFinishGroupAction(Request $request)
    {
        return $this->processMaterialFinishGroupForm(new MaterialFinishGroup(), $request);
    }

    /**
     * @param Request $request
     * @param MaterialFinishGroup $materialFinishGroup
     * @return Response
     */
    private function processMaterialFinishGroupForm(MaterialFinishGroup $materialFinishGroup, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(MaterialFinishGroupFormType::class, $materialFinishGroup, ['method' => $request->getMethod()]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($materialFinishGroup);
            $em->flush();

            $view = $this->view(
                ['material_finish_group' => $materialFinishGroup],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material Finish Group.
     *
     * @Rest\Delete(
     *     path="/material_finish_groups/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material Finish Group by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Finish Group Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialFinishGroup $materialFinishGroup
     * @return Response
     */
    public function deleteAction(MaterialFinishGroup $materialFinishGroup)
    {
        $em = $this->getEm();

        try{
            $em->remove($materialFinishGroup);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Finish Group - there are some relations.";

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}