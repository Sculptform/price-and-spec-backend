<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\VideoTutorial;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class VideoTutorialsController extends BaseController
{
    /**
     * Use this method to get whole list of video tutorials.
     *
     * @Rest\Get(
     *     path="/video_tutorials",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Video Tutorials",
     *     section="Video Tutorials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Video Tutorials List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllVideoTutorialsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:VideoTutorial')->createQueryBuilder('video_tutorial')->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('video_tutorials', $pagination);
    }
}