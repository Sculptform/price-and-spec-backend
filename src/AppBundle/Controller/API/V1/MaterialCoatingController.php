<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\MaterialCoating;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class MaterialCoatingController extends BaseController
{
    /**
     * Use this method to get whole list of material coatings. All Material Coatings objects are wrapped in "material_coatings" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_coatings",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Coatings",
     *     section="MaterialCoatings",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return MaterialCoatings List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Application Type")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(MaterialCoating::class)->findAllByParams([
                'application_type' => $paramFetcher->get('application_type')
            ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('material_coatings', $pagination);
    }
}