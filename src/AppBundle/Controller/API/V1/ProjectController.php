<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Service\PardotService;
use AppBundle\Util\SequenceFormatter;
use Mpdf\Mpdf;
use AppBundle\Enum\ {
    ProjectStatus,
    TeamAccessType,
    UserProjectUserType,
    PardotUserLastAction,
    PardotTrackActionsEnum,
};

use AppBundle\Exception\ {
    ConflictException,
    ValidationException,
};

use AppBundle\Entity\{
    User,
    Team,
    Scene,
    Quotation,
    Project,
    Product,
    UserProject,
    FintraxInfo,
    Notification,
    ProjectFilter,
    FintraxProduct,
    FintraxStacker,
    ProjectProduct,
    ProjectRatingUser,
    Comment\ProjectComment,
    ExpressionCladdingProduct};

use AppBundle\Event\{Events,
    ProjectEvent,
    PsDownloadedEvent,
    TeamMembersEvent,
    UserProjectEvent,
    PardotTrackActionEvent};


use AppBundle\Form\Type\ {
    SceneFormType,
    CommentFormType,
    ProjectFormType,
    FintraxProductFormType,
    ProjectProductFormType,
    ProjectRatingUserFormType,
    ExpressionCladdingProductFormType,
};

use Sensio\Bundle\FrameworkExtraBundle\Configuration\ {
    Template,
    ParamConverter,
};

use FOS\RestBundle\ {
    Request\ParamFetcher,
    Controller\Annotations\View,
    Controller\Annotations as Rest,
};

use Symfony\Component\{HttpFoundation\JsonResponse,
    Process\Process,
    Filesystem\Filesystem,
    HttpFoundation\Request,
    HttpFoundation\Response,
    HttpFoundation\RedirectResponse,
    Routing\Generator\UrlGeneratorInterface,
    Security\Core\Exception\AccessDeniedException};

use AppBundle\Util\PlatformUtil;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Doctrine\Common\Collections\Collection;
use AppBundle\Controller\API\BaseController;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;

/**
 * Class ProjectController
 *
 * @package AppBundle\Controller\API\V1
 * @author  Vladimir Yeliseev <vladimir.eliseev@sibers.com>
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class ProjectController extends BaseController
{
    /**
     * Use this method to get whole list of projects. All Project objects are
     * wrapped in "projects" field. Project object description can be found in
     * "Get by Id" method. Also output contains "meta" field with pagination
     * info
     *
     * @Rest\Get(
     *     name="api_v1_get_projects",
     *     path="/projects",
     *     defaults={"_format"="json"},
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true,
     *                                  requirements="\d+", description="Filter
     *                                  By User Id")
     * @Rest\QueryParam(name="user_type", allowBlank=true, nullable=true,
     *                                    requirements="\d+",
     *                                    description="Filter By User Type")
     * @Rest\QueryParam(name="access_type", allowBlank=true, nullable=true,
     *                                      default="2", requirements="\d+",
     *                                      description="Filter By Access
     *                                      Type")
     * @Rest\QueryParam(name="title", allowBlank=true, nullable=true,
     *                                description="Filter By Project Title")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true,
     *                               default="createdAt",
     *                               requirements="createdAt|author|rating|title|id",
     *                               description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true,
     *                                    default="desc",
     *                                    requirements="asc|desc",
     *                                    description="Sort direction")
     * @Rest\QueryParam(name="min", allowBlank=true, nullable=true,
     *                              requirements="\d+", description="Min
     *                              price")
     * @Rest\QueryParam(name="status", allowBlank=true, nullable=true,
     *                                 requirements="\d+", description="Project
     *                                 status")
     * @Rest\QueryParam(name="max", allowBlank=true, nullable=true,
     *                              requirements="\d+", description="Max
     *                              price")
     * @Rest\QueryParam(name="is_template", allowBlank=true, nullable=true,
     *                                      default="0", requirements="1|0",
     *                                      description="Template projects.")
     * @Rest\QueryParam(name="application_type", allowBlank=true,
     *                                           nullable=true,
     *                                           requirements="\d+",
     *                                           description="Filter projects
     *                                           By Application Type")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true,
     *                                       requirements="\d+",
     *                                       description="Product type Id")
     *
     * @Rest\QueryParam(name="filters", allowBlank=true, nullable=true, description="Project filters")
     *
     * @ApiDoc(
     *     description="Get Projects",
     *     section="Projects",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Projects List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getProjectsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        $userId     = $paramFetcher->get('user_id');
        $accessType = $paramFetcher->get('access_type');
        $isTemplate = (bool) $paramFetcher->get('is_template');

        if (! $userId && $currentUser) {
            if ($currentUser->hasRole('ROLE_ADMIN')) {
                $userId = null;
            } else {
                $userId = $currentUser->getId();
            }
        }

        // allow guest access for template projects only
        if (! $userId && ! $isTemplate
            && (! $currentUser
                || $currentUser
                && ! $currentUser->hasRole('ROLE_ADMIN'))
        ) {
            throw new AccessDeniedException();
        }

        // TODO: Add proper security handler (with public and private projects)
        /*if(!$currentUser->hasRole('ROLE_ADMIN') && $userId !== $currentUser->getId()){
            throw new AccessDeniedException();
        }*/

        $repository = $em->getRepository(Project::class);

        if ($accessType == TeamAccessType::PRIVATE_ACCESS) {
            $items = $repository->findAllByParams(
                [
                    'user'             => $userId,
                    'user_type'        => $paramFetcher->get('user_type'),
                    'title'            => $paramFetcher->get('title'),
                    'sort'             => $paramFetcher->get('sort'),
                    'min'              => $paramFetcher->get('min'),
                    'max'              => $paramFetcher->get('max'),
                    'status'           => $paramFetcher->get('status'),
                    'filters'          => $paramFetcher->get('filters'),
                    'is_template'      => $isTemplate,
                    'product_type'     => $paramFetcher->get('product_type'),
                    'application_type' => $paramFetcher->get(
                        'application_type'
                    ),
                ],
                true
            );
        } else {
            $items = $repository->findPublicByParams(
                [
                    'user'    => $userId,
                    'title'   => $paramFetcher->get('title'),
                    'sort'    => $paramFetcher->get('sort'),
                    'min'     => $paramFetcher->get('min'),
                    'max'     => $paramFetcher->get('max'),
                    'status'  => $paramFetcher->get('status'),
                    'filters' => $paramFetcher->get('filters'),
                ],
                true
            );
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $items,
            $paramFetcher->get('page'),
            $paramFetcher->get('limit'),
            [
                'defaultSortFieldName' => 'createdAt',
                'defaultSortDirection' => 'desc',
                'wrap-queries'         => true,
            ]
        );

        return $this->paginationResponse('projects', $pagination);
    }

    /**
     * Use this method to get Project by Id. Project object documented here was
     * wrapped in "project" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_project",
     *     path="/projects/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ApiDoc(
     *     description="Get Project by Id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     statusCodes={
     *         404 = "Project not found. All other errors looks like this",
     *         200 = "Return Project"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     }
     * )
     *
     * @param Project $project
     *
     * @return Response
     */
    public function getProjectAction(Project $project)
    {
        $em = $this->getEm();

        $currentUser = $this->getUser();

        if ($currentUser) {
            $publicProject = $em->getRepository(Project::class)
                ->findPublicByParams(
                    [
                        'user' => $this->getUser()->getId(),
                        'id'   => $project->getId(),
                    ]
                );

            $publicProject
                ? $project->setPublic(true)
                : $project->setPublic(
                false
            );
        }

        $project->setViews($project->getViews() + 1);
        $em->persist($project);
        $em->flush();

        return $this->handleView(
            $this->view(
                ['project' => $project],
                Response::HTTP_OK
            )
        );
    }

    /**
     * Use this method to Export Project by Id.
     *
     * @Rest\Route(
     *     name="api_v1_export_project", path="/projects/{id}/export",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"GET", "POST"}
     * )
     *
     * @View(serializerGroups={"Export"})
     *
     * @Rest\QueryParam(name="type",   allowBlank=true, default="pdf",
     *                                 nullable=true, description="Export
     *                                 type")
     * @Rest\QueryParam(name="email",  allowBlank=true, default=true,
     *                                 nullable=true, description="Send email
     *                                 with exported file instead of
     *                                 downloading")
     * @Rest\QueryParam(name="octet",  allowBlank=true, default=false,
     *                                 nullable=true, description="Works if
     *                                 email parameter is false, replaces
     *                                 response type from pdf|msword to
     *                                 octet-stream, special for ember")
     * @Rest\RequestParam(name="data", allowBlank=true, nullable=true)
     *
     * @ApiDoc(
     *     description="Export Project by Id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *          {
     *              "name"="id", "dataType"="string", "description"="Project
     *              Id",
     *              "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
     *          }
     *     },
     *     statusCodes={
     *         404 = "Project not found. All other errors looks like this",
     *         200 = "Return Project"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     }
     * )
     *
     * @param Project      $project
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     *
     * @throws \Doctrine\ORM\NonUniqueResultException
     * @throws \Mpdf\MpdfException
     * @throws \Twig\Error\Error
     */
    public function exportProjectAction(
        Project $project,
        ParamFetcher $paramFetcher
    ) {
        /** @var User $user */
        $user = $this->getUser();

        $type       = $paramFetcher->get('type');
        $sendEmail  = (bool) $paramFetcher->get('email');
        $octet      = (bool) $paramFetcher->get('octet', false);
        $dispatcher = $this->container->get('event_dispatcher');

        $messageCreator = $this->get('project.message.creator');
        $pardotAction   = null;

        if ('pdf' === $type) {
            $mpdf = new Mpdf(['tempDir' => __DIR__.'/../../../../../web/temp']);

            /** @var array $fintraxes */
            $fintraxes = [];

            if ($project->getProductType()->getId() === 3) {
                $fintraxInfoRepo = $this->getEm()->getRepository(
                    FintraxInfo::class
                );

                foreach ($project->getFintraxProducts() as $fintraxProduct) {
                    $base         = $fintraxProduct->getBase();
                    $nosing       = $fintraxProduct->getNosing();
                    $finishGroups = [];
                    $width        = $base->getMaterial()->getMaterialShapeSize()->getWidth();

                    $depth = 0;
                    foreach ($fintraxProduct->getFintraxStackers() as $stacker) {
                        $depth += $stacker->getProduct()->getMaterial()->getMaterialShapeSize()->getDepth();
                    }

                    $finish = $fintraxProduct->getFinish();

                    if ($finish) {
                        $finishGroup = $finish->getGroup();
                        if ($finishGroup) {
                            $finishGroups = explode('_', $finishGroup->getPath());
                        }
                    }

                    $fintraxInfo = $fintraxInfoRepo->findAllByParams(
                        [
                            'width'         => $width,
                            'depth'         => $depth,
                            'finish_groups' => $finishGroups,
                            'nosing_id'     => $nosing ? $nosing->getMaterialShapeId() : null,
                        ],
                        true
                    )->setMaxResults(1)->getOneOrNullResult();

                    if ($fintraxInfo) {
                        $fintraxes[] = [
                            'fintraxInfo' => $fintraxInfo,
                            'finish'      => $finish,
                        ];
                    }
                }
            }

            $html = $this->renderView(
                'AppBundle:Export/Project:pdf.html.twig',
                ['project' => $project, 'pricingCategory' => Project::$sqFtPricingCategory]
            );
            $mpdf->WriteHTML($html);
            $content = $mpdf->Output('', 'S');
            $this->sendToPardot(
                $user,
                $project,
                PardotUserLastAction::DOWNLOAD_SPECIFICATION_PDF,
                $type
            );
            $this->trackDownload($user, $project, $type);

            if ($sendEmail) {
                if (! $user) {
                    throw $this->createAccessDeniedException();
                }

                try {
                    $message = $messageCreator->create($user, $project, $content, 'PDF');
                    $this->get('mailer')->send($message);
                } catch (\Exception $e) {
                    $this->get('logger')->addError($e->getMessage());
                }

                return Response::create('', Response::HTTP_NO_CONTENT);
            }

            return Response::create(
                $content,
                200,
                [
                    'access-control-expose-headers' => 'content-disposition',
                    'content-type'                  => sprintf(
                        'application/%s',
                        $octet ? 'octet-stream' : 'pdf'
                    ),
                    'content-disposition'           => sprintf(
                        'attachment; filename="%s"',
                        $project->getTitle().'.pdf'
                    ),
                ]
            );
        }

        if ('xlsx' === $type) {
            $platform = PlatformUtil::getName();
            if (! in_array($platform, ['win', 'mac', 'nix'])) {
                return Response::create('Unsupported platform.', Response::HTTP_NO_CONTENT);
            }

            $this->sendToPardot(
                $user,
                $project,
                PardotUserLastAction::DOWNLOAD_SPECIFICATION_EXCEL,
                $type
            );

            $content = $this->get('specification.excel')->generate($project, $platform);

            $this->trackDownload($user, $project, $type);

            if ($sendEmail) {
                if (! $user) {
                    throw $this->createAccessDeniedException();
                }

                try {
                    $message = $messageCreator->create($user, $project, $content, 'XLSX');
                    $this->get('mailer')->send($message);
                } catch (\Exception $e) {
                    $this->get('logger')->addError($e->getMessage());
                }

                return Response::create('', Response::HTTP_NO_CONTENT);
            }

            return Response::create(
                $content,
                200,
                [
                    'access-control-expose-headers' => 'content-disposition',
                    'content-type'                  => 'application/vnd.ms-excel',
                    'content-disposition'           => sprintf(
                        'attachment; filename="%s"',
                        "{$project->getTitle()}-{$platform}.xlsx"
                    ),
                ]
            );
        }

        if ('docx' === $type) {
            $content = $this->get('specification.word')->generate($project);
            $this->sendToPardot(
                $user,
                $project,
                PardotUserLastAction::DOWNLOAD_SPECIFICATION_WORD,
                $type
            );
            $this->trackDownload($user, $project, $type);

            if ($sendEmail) {
                if (! $user) {
                    throw $this->createAccessDeniedException();
                }

                try {
                    $message = $messageCreator->create($user, $project, $content, 'DOCX');
                    $this->get('mailer')->send($message);
                } catch (\Exception $e) {
                    $this->get('logger')->addError($e->getMessage());
                }

                return Response::create('', Response::HTTP_NO_CONTENT);
            }

            return Response::create(
                $content,
                200,
                [
                    'access-control-expose-headers' => 'content-disposition',
                    'content-type'                  => 'application/msword',
                    'content-disposition'           => sprintf(
                        'attachment; filename="%s"',
                        $project->getTitle().'.docx'
                    ),
                ]
            );
        }

        if ('dwg' === $type) {
//            $event = new PardotTrackActionEvent(
//                $user, $project, PardotTrackActionsEnum::DOWNLOAD_DWG
//            );
//            $dispatcher->dispatch($event::NAME, $event);

            $fs = new Filesystem();

            $iFilename = $this->secureTmpName('.xml', 'dwg-input-', '/tmp');
            $oFilename = $this->secureTmpName('.dwg', 'dwg-output-', '/tmp');

            $fs->chmod([$iFilename, $oFilename], 0666);

            $data = (array) $paramFetcher->get('data');

            /** @var \AppBundle\DTO\DWGExportedProduct[] $data */
            $data = $this->get('serializer')->fromArray($data, 'array<AppBundle\DTO\DWGExportedProduct>');
            $content = $this->renderView('AppBundle:Export/Project:dwg.xml.twig', ['data' => $data]);
            $fs->appendToFile($iFilename, $content);
            $command = sprintf(
                '%s/merge -f -platform offscreen -t 2013 -o %s %s',
                $this->getParameter('qcad_path'),
                $oFilename,
                $iFilename
            );

            $process = new Process($command);
            $process->run();

            $content = file_get_contents($oFilename);
            $size    = filesize($oFilename);
            $fs->remove([$iFilename, $oFilename]);

            $this->sendToPardot(
                $user,
                $project,
                PardotUserLastAction::DOWNLOAD_DRAWINGS,
                $type
            );

            $this->trackDownload($user, $project, $type);

            if ($sendEmail) {
                if (! $user) {
                    throw $this->createAccessDeniedException();
                }

                try {
                    $message = $messageCreator->create($user, $project, $content, 'DWG');
                    $this->get('mailer')->send($message);
                } catch (\Exception $e) {
                    $this->get('logger')->addError($e->getMessage());
                }

                return Response::create('', Response::HTTP_NO_CONTENT);
            }

            $disposition = sprintf('attachment; filename="%s"', $project->getTitle().'.dwg');

            return Response::create(
                $content,
                200,
                [
                    'Access-Control-Expose-Headers' => 'Content-Disposition',
                    'Content-Type'                  => 'image/vnd.dwg',
                    'Content-Disposition'           => $disposition,
                    'Content-Length'                => $size,
                ]
            );
        }

        $designURL = "{$this->getParameter('sculptform_url')}/user/designer/{$project->getId()}";
        $dispatcher->dispatch(
            Events::PS_DOWNLOADED,
            new PsDownloadedEvent($user->getEmail(), $designURL)
        );

        return $this->handleView(
            $this->view(['project' => $project], Response::HTTP_OK)
        );
    }

    /**
     * @Rest\Route(
     *     name="api_v1_export_pdf_template", path="/projects/export/template-pdf",
     *     defaults={"_format"="json"},
     *     methods={"GET"}
     * )
     */
    public function exportPDFTemplate()
    {
        $mpdf = new Mpdf(['tempDir' => __DIR__.'/../../../../../web/temp']);

        $html = $this->renderView('AppBundle:Export/Project:pdf-template.html.twig');
        $mpdf->WriteHTML($html);

        $content = $mpdf->Output('', 'S');

        return Response::create(
            $content,
            200,
            [
                'access-control-expose-headers' => 'content-disposition',
                'content-type'                  => sprintf(
                    'application/%s',
                    'pdf'
                ),
                'content-disposition'           => sprintf(
                    'attachment; filename="%s"',
                    'pdf-template.pdf'
                ),
            ]
        );
    }

    /**
     * @param Project $project
     *
     * @Rest\Route(
     *     name="api_v1_export_project_sequence", path="/projects/{id}/sequence",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"GET", "POST"}
     * )
     *
     * @return Response
     */
    public function exportProjectSequence(Project $project)
    {
        $sequence = SequenceFormatter::create($project);

        return new Response($sequence);
    }

    /**
     * Use this method to send project's spec table to the email.
     *
     * @Rest\Route(
     *     name="api_v1_email_project",
     *     path="/projects/{id}/email",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"GET", "POST"}
     * )
     *
     * @View(serializerGroups={"Export"})
     *
     * @ApiDoc(
     *     description="Export Project by Id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *          {
     *              "name"="id", "dataType"="string", "description"="Project
     *              Id",
     *              "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"
     *          }
     *     },
     *     statusCodes={
     *         404 = "Project not found. All other errors looks like this.",
     *         204 = "Return Empty Response"
     *     },
     *     responseMap={
     *         404 = {"class"="AppBundle\Model\APIException"}
     *     },
     *     output={
     *         "parsers"={"Nelmio\ApiDocBundle\Parser\JmsMetadataParser",},
     *         "class"="AppBundle\Entity\Project"
     *     }
     * )
     *
     * @param Project $project
     *
     * @return Response
     */
    public function emailProjectAction(Project $project)
    {
        $projectImagePath = "{$this->getParameter('host')}/{$project->getImageWebPath()}";

        $body = $this->renderView(
            '@App/Export/Project/txt.html.twig',
            [
                'project'          => $project,
                'projectImagePath' => $projectImagePath,
            ]
        );

        /** @var User $user */
        $user = $this->getUser();

        $message = (new \Swift_Message("Project {$project->getTitle()}"))
            ->setFrom(
                $this->getParameter('email.from.address'),
                $this->getParameter('email.from.sender_name')
            )
            ->setTo($user->getEmail(), $user->getFullName())
            ->setBody($body, 'text/html');

        $this->get('mailer')->send($message);

        return Response::create('', Response::HTTP_NO_CONTENT);
    }

    /**
     * Use this method to Share Project by Id.
     *
     * @Rest\Route(
     *     name="api_v1_project_share",
     *     path="/projects/{id}/share",
     *     defaults={"_format"="html"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"GET"}
     * )
     *
     * @ApiDoc(
     *     description="Export Project by Id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *          {"name"="id", "dataType"="string",
     *          "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *          "description"="Project Id"}
     *     },
     *     statusCodes={
     *         404 = "Project not found. All other errors looks like this",
     *         200 = "Return Project"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     }
     * )
     *
     * @Template(template="@App/Project/share_project.html.twig")
     *
     * @param Project $project
     * @param Request $request
     *
     * @return array|Response
     */
    public function shareProjectAction(Project $project, Request $request)
    {
        $this->getEm()->refresh($project);

        $agent = strtolower($request->headers->get('User-Agent'));

        if (strpos($agent, 'facebook') === false
            && strpos($agent, 'linkedin') === false
        ) {
            return new RedirectResponse(
                sprintf(
                    '%s/guest/designer/%s',
                    $this->getParameter('sculptform_url'),
                    $project->getId()
                )
            );
        }

        $titles = [
            sprintf('Application: %s', $project->getApplicationLabel()),
            sprintf('Product Type: %s', $project->getProductType()->getTitle()),
            sprintf('Guaranteed Supply Cost: %s /m2', $project->getPrice()),
        ];

        $content = implode('; ', $titles);

        $url = $this->generateUrl(
            'api_v1_project_share',
            ['id' => $project->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL // This guy right here
        );

        $imageWidth   = $imageHeight = '';
        $imageWebPath = $project->getImageWebPath();

        if ($imageWebPath) {
            [$imageWidth, $imageHeight] = getimagesize(
                $project->getImageAbsolutePath()
            );
        }


        return [
            'title'        => $project->getTitle(),
            'content'      => $content,
            'imageWebPath' => $imageWebPath,
            'imageWidth'   => $imageWidth,
            'imageHeight'  => $imageHeight,
            'url'          => $url,
        ];
    }

    private function trackDownload(User $user, Project $project, $type)
    {
        $dispatcher = $this->container->get('event_dispatcher');

        $dispatcher->dispatch(
            Events::PS_DOWNLOADED,
            new PsDownloadedEvent($user, $project, $type)
        );
    }

    /**
     * @param User $user
     * @param Project $project
     * @param $action
     * @param string $type
     */
    private function sendToPardot(User $user, Project $project, $action, string $type = '')
    {
        try {
            /** @var PardotService $pardotService */
            $pardotService  = $this->get('app.service.pardot');
            $projectURL     = $this->getParameter('sculptform_url')
                ."/user/designer/{$project->getId()}";
            $pardotResponse = $pardotService
                ->sendUserLastAction(
                    $user,
                    $action,
                    [
                        'project_url'  => $projectURL,
                        'P&S - Action' => $type == 'dwg' ? 'Download Drawings' : 'Download Specification '.strtoupper($type),
                        'Project Name' => $project->getTitle(),
                    ]
                );
            $this->get('logger')->addInfo($pardotResponse);
        } catch (\Exception $e) {
            $this->get('logger')->addError($e->getMessage());
        }
    }

    /**
     * @param string $postfix
     * @param string $prefix
     * @param string $dir
     *
     * @return bool|string
     */
    private function secureTmpName(
        $postfix = '.tmp',
        $prefix = 'tmp',
        $dir = null
    ) {
        // validate arguments
        if (! (isset($postfix) && is_string($postfix))) {
            return false;
        }
        if (! (isset($prefix) && is_string($prefix))) {
            return false;
        }
        if (! isset($dir)) {
            $dir = getcwd();
        }

        // find a temporary name
        $tries = 1;
        do {
            // get a known, unique temporary file name
            $sysFileName = tempnam($dir, $prefix);
            if ($sysFileName === false) {
                return false;
            }

            // tack on the extension
            $newFileName = $sysFileName.$postfix;
            if ($sysFileName == $newFileName) {
                return $sysFileName;
            }

            // move or point the created temporary file to the new filename
            // NOTE: these fail if the new file name exist
            $newFileCreated = @rename($sysFileName, $newFileName);
            if ($newFileCreated) {
                return $newFileName;
            }

            unlink($sysFileName);
            $tries++;
        } while ($tries <= 5);

        return false;
    }

    /**
     * Use this method to update Project by Id. Project object documented here
     * was wrapped in "project" field
     *
     * @Rest\Route(
     *     path="/projects/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Project",
     *     input="AppBundle\Form\Type\ProjectFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     },
     *     statusCodes={
     *         404 = "Project not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Project $project
     * @param Request $request
     *
     * @return Response
     */
    public function updateProjectAction(Project $project, Request $request)
    {
        return $this->processProjectForm($project, $request);
    }

    /**
     * Use this method to create Project. Project object documented here was
     * wrapped in "project" field
     *
     * @Rest\Post(
     *     path="/projects",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Project",
     *     input="AppBundle\Form\Type\ProjectFormType",
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     },
     *     statusCodes={
     *         404 = "Project not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createProjectAction(Request $request)
    {
        $currentUser = $this->getUser();

        if (! $currentUser) {
            throw new AccessDeniedException();
        }

        /*if (!$currentUser->hasRole('ROLE_SUBSCRIBER') && !$currentUser->hasRole('ROLE_ADMIN')) {
            $ownedProjectsCount = $currentUser->getUserProjects()->filter(function (UserProject $userProject) {
                return $userProject->getUserType() === UserProjectUserType::OWNER;
            })->count();

            $freeProjectsLimit = $this->getParameter('free_subscription_projects_limit');

            if ($ownedProjectsCount >= $freeProjectsLimit) {
                throw new PaymentRequiredException(sprintf(
                    'You have exceeded %s projects limit for your free subscription!',
                    $freeProjectsLimit
                ));
            }
        }*/

        $project = new Project();

        $userProject = new UserProject();
        $userProject
            ->setProject($project)
            ->setUser($currentUser)
            ->setUserType(UserProjectUserType::OWNER);

        $project->addUserProject($userProject);

        return $this->processProjectForm($project, $request);
    }

    /**
     * Use this method to add Project Rating by current user. Project Rating
     * User object documented here was wrapped in "project" field
     *
     * @Rest\Post(
     *     path="projects/{id}/rating",
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Add Project Rating",
     *     input="AppBundle\Form\Type\ProjectRatingUserFormType",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Project"
     *     },
     *     statusCodes={
     *         404 = "Project not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     },
     *     resource=true
     * )
     *
     * @param Project $project
     * @param Request $request
     *
     * @return Response
     */
    public function setProjectRatingAction(Project $project, Request $request)
    {
        $em     = $this->getEm();
        $method = $request->getMethod();

        /** @var ProjectRatingUser $projectRatingUsers */
        if (! $projectRatingUser = $em->getRepository(ProjectRatingUser::class)
            ->findOneBy(['project' => $project, 'user' => $this->getUser()])
        ) {
            $projectRatingUser = new ProjectRatingUser();
            $projectRatingUser->setUser($this->getUser());
            $projectRatingUser->setProject($project);
        }

        $form = $this->createForm(
            ProjectRatingUserFormType::class,
            $projectRatingUser,
            ['method' => $method]
        );
        $form->handleRequest($request);

        if ($form->isValid()) {

            $em->persist($projectRatingUser);
            $em->flush();

            // GENERATE PROJECT RATED NOTIFICATION //
            $this->get('event_dispatcher')
                ->dispatch(
                    Events::PROJECT_RATED,
                    new ProjectEvent(
                        $project,
                        $this->getUser(),
                        Events::PROJECT_RATED,
                        $project->getOwner()
                    )
                );

            $view = $this->view(
                ['project' => $project],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * @param Project $project
     * @param Request $request
     *
     * @return Response
     */
    private function processProjectForm(Project $project, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(
            ProjectFormType::class,
            $project,
            ['method' => $method]
        );

        if ($method === $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();
            $em          = $this->getEm();

            $teams = $em->getRepository(Team::class)->getTeamsByUser(
                $currentUser
            );

            /**
             * Just delete shared user if he wish to delete shared project
             */
            if ($currentUser->getId()
                && $project->getStatus() === ProjectStatus::DELETED
                && $project->getOwnerId() !== $currentUser->getId()
            ) {
                $userProject = $em->getRepository(UserProject::class)
                    ->findOneBy(
                        [
                            'user'     => $currentUser,
                            'project'  => $project,
                            'userType' => UserProjectUserType::SHARED,
                        ]
                    );

                if ($userProject) {
                    $em->remove($userProject);
                    $em->flush();

                    return new Response(null, Response::HTTP_NO_CONTENT);
                }
            }

            if (! $currentUser->getId()
                || (! $currentUser->hasRole(
                        'ROLE_ADMIN'
                    )
                    && $project->getOwnerId() !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            if (! $project->getId()) {
                $em->persist($project);
            }

            if (in_array(
                $project->getStatus(),
                [ProjectStatus::ARCHIVED, ProjectStatus::DELETED]
            )
            ) {
                /** @var Notification\ShareNotification $notification */
                foreach ($project->getShareNotifications() as $notification) {
                    if (! $notification->getReadAt()) {
                        $em->remove($notification);
                    }
                }

                /** @var Quotation $quotation */
                foreach ($project->getQuotations() as $quotation) {
                    /** @var Notification\QuotationNotification $notification */
                    foreach ($quotation->getNotifications() as $notification) {
                        if (! $notification->getReadAt()) {
                            $em->remove($notification);
                        }
                    }
                }
            } else {
                $sharedUsersIds = $project->getUsersIds();

                /** @var Collection $newSharedUsers */
                $newSharedUsers = $form->get('users_ids')->getData();

                if ($newSharedUsers) {
                    foreach ($newSharedUsers as $newSharedUser) {
                        /** @var User $newSharedUser */
                        if (! in_array(
                                $newSharedUser->getId(),
                                $sharedUsersIds
                            )
                            && $newSharedUser->getId() !== $project->getOwnerId()
                        ) {
                            $userProject = new UserProject();

                            $userProject
                                ->setProject($project)
                                ->setUser($newSharedUser)
                                ->setUserType(UserProjectUserType::SHARED);

                            $this->get('event_dispatcher')->dispatch(
                                Events::PROJECT_SHARED,
                                new UserProjectEvent($userProject)
                            );
                        }
                    }

                    $deletedUsersIds = array_diff(
                        $sharedUsersIds,
                        $newSharedUsers->map(
                            function (User $user) {
                                return $user->getId();
                            }
                        )->toArray()
                    );

                    if ($deletedUsersIds) {
                        $deletedUserProjects = $em->getRepository(
                            UserProject::class
                        )->findBy(
                            [
                                'user'     => $deletedUsersIds,
                                'project'  => $project,
                                'userType' => UserProjectUserType::SHARED,
                            ]
                        );

                        /** @var UserProject $deletedUserProject */
                        foreach ($deletedUserProjects as $deletedUserProject) {
                            /** @var Notification\ShareNotification $notification */
                            foreach (
                                $deletedUserProject->getProject()
                                    ->getShareNotifications() as $notification
                            ) {
                                if ($deletedUserProject->getUser()->getId()
                                    === $notification->getUser()->getId()
                                ) {
                                    $em->remove($notification);
                                }
                            }

                            $em->remove($deletedUserProject);
                        }
                    }
                }
            }

            $em->flush();

            // GENERATE NOTIFICATIONS FOR TEAMS USERS //
            $eventUsersIds = [];
            /** @var Team $team */
            foreach ($teams as $team) {
                $users = $team->getConfirmUsers();

                /** @var User $user */
                foreach ($users as $user) {
                    if (array_key_exists($user->getId(), $eventUsersIds)) {
                        continue;
                    }

                    $eventUsersIds[$user->getId()] = $user->getId();

                    $this->get('event_dispatcher')->dispatch(
                        Events::TEAM_MEMBERS_PROJECT_CREATED,
                        new TeamMembersEvent(
                            $team,
                            $user,
                            Events::TEAM_MEMBERS_PROJECT_CREATED,
                            $currentUser
                        )
                    );
                }
            }

            $view = $this->view(
                ['project' => $project],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        $url = $this->generateUrl(
            'api_v1_project_share',
            ['id' => $project->getId()],
            UrlGeneratorInterface::ABSOLUTE_URL // This guy right here
        );

        $this->clearOpenGraphCache($url);

        return $this->handleView($view);
    }

    /**
     * @Rest\Get(
     *     path="/projects/{id}/scenes",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Project Scene List",
     *     section="Projects",
     *     views={"v1"},
     *     resource=true
     * )
     *
     * @param int     $page
     * @param int     $limit
     * @param Project $project
     *
     * @return Response
     */
    public function getProjectScenes(Project $project, $page, $limit)
    {
        $pagination = $this->get('knp_paginator')->paginate(
            $this->getEm()->getRepository(Scene::class)->findBy(
                ['project' => $project],
                ['createdAt' => 'desc']
            ),
            $page,
            $limit
        );

        return $this->paginationResponse('scenes', $pagination);
    }

    /**
     * @Rest\Get(
     *     name="api_v1_get_project_scene",
     *     path="/projects/{id}/scenes/{scene_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "scene_id"="\d+"}
     * )
     *
     * @ParamConverter("scene", class="AppBundle\Entity\Scene", options={"id" =
     *                          "scene_id"})
     *
     * @ApiDoc(
     *     description="Get Project Scene by Id",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Scene $scene
     *
     * @return Response
     */
    public function getProjectSceneAction(Scene $scene)
    {
        return $this->handleView(
            $this->view(['scene' => $scene], Response::HTTP_OK)
        );
    }

    /**
     * @Rest\Post(
     *     path="/projects/{id}/scenes",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ApiDoc(
     *     description="Create Project Scene",
     *     input="AppBundle\Form\Type\SceneFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request $request
     * @param Project $project
     *
     * @return Response
     */
    public function createProjectSceneAction(Project $project, Request $request)
    {
        $scene = new Scene();

        $scene->setProject($project);

        return $this->processProjectSceneForm($scene, $request);
    }

    /**
     * @param Request $request
     * @param Scene   $scene
     *
     * @return Response
     */
    private function processProjectSceneForm(Scene $scene, Request $request)
    {
        $form = $this->createForm(
            SceneFormType::class,
            $scene,
            ['method' => $request->getMethod()]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if (! $currentUser->getId()
                || (! $currentUser->hasRole(
                        'ROLE_ADMIN'
                    )
                    && $scene->getProject()->getOwnerId()
                    !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();

            foreach ($scene->getProject()->getScenes() as $oldScene) {
                $em->remove($oldScene);
            }

            $em->persist($scene);
            $em->flush();

            $view = $this->view(
                ['scene' => $scene],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * @Rest\Get(
     *     path="/projects/{id}/project_products",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Project Products List",
     *     section="Projects",
     *     views={"v1"},
     *     resource=true
     * )
     *
     * @param int     $page
     * @param int     $limit
     * @param Project $project
     *
     * @return Response
     */
    public function getProjectProducts(Project $project, $page, $limit)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(ProjectProduct::class)
                ->createQueryBuilder('projectProduct')
                ->where('projectProduct.project = :project')
                ->setParameter('project', $project),
            $page,
            $limit
        );

        return $this->paginationResponse('project_products', $pagination);
    }

    /**
     * @Rest\Get(
     *     name="api_v1_get_project_product",
     *     path="/projects/{id}/project_products/{project_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "project_product_id"="\d+"}
     * )
     *
     * @ParamConverter("projectProduct", class="AppBundle\Entity\ProjectProduct",
     *                                   options={"id" = "project_product_id"})
     *
     * @ApiDoc(
     *     description="Get ProjectProduct by Id",
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProjectProduct"
     *     },
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="project_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Product Id"},
     *     }
     * )
     *
     * @param ProjectProduct $projectProduct
     *
     * @return Response
     */
    public function getProjectProductAction(ProjectProduct $projectProduct)
    {
        return $this->handleView(
            $this->view(
                ['project_product' => $projectProduct],
                Response::HTTP_OK
            )
        );
    }

    /**
     * @Rest\Post(
     *     path="/projects/{id}/project_products",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ApiDoc(
     *     description="Create Project Product",
     *     input="AppBundle\Form\Type\ProjectProductFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request $request
     * @param Project $project
     *
     * @return Response
     */
    public function createProjectProductAction(
        Project $project,
        Request $request
    ) {
        $projectProduct = new ProjectProduct();

        $projectProduct->setProject($project);

        return $this->processProjectProductForm($projectProduct, $request);
    }

    /**
     * Delete Project Product
     *
     * @Rest\Delete(
     *     path="/projects/{id}/project_products/{project_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "project_product_id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Delete Project Product by id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="project_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Product Id"},
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted"
     *     }
     * )
     *
     * @ParamConverter("projectProduct", class="AppBundle\Entity\ProjectProduct",
     *                                   options={"id" = "project_product_id"})
     * @ParamConverter("project", class="AppBundle\Entity\Project",
     *                            options={"id" = "id"})
     *
     * @param ProjectProduct $projectProduct
     *
     * @return Response
     */
    public function deleteProjectProductAction(ProjectProduct $projectProduct)
    {
        $em = $this->getEm();

        $currentUser = $this->getUser();

        if (! $currentUser->hasRole('ROLE_ADMIN')
            && $projectProduct->getProject()->getOwnerId()
            !== $currentUser->getId()
        ) {
            throw new AccessDeniedException();
        }

        $em->remove($projectProduct);
        $em->flush();

        return $this->handleView(
            $this->view(
                null,
                Response::HTTP_NO_CONTENT
            )
        );
    }

    /**
     * @param Request        $request
     * @param ProjectProduct $projectProduct
     *
     * @return Response
     */
    private function processProjectProductForm(
        ProjectProduct $projectProduct,
        Request $request
    ) {
        $form = $this->createForm(
            ProjectProductFormType::class,
            $projectProduct,
            ['method' => $request->getMethod()]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $currentUser = $this->getUser();

            if (! $currentUser->hasRole('ROLE_ADMIN')
                && $projectProduct->getProject()->getOwnerId()
                !== $currentUser->getId()
            ) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();
            $em->persist($projectProduct);
            $em->flush();

            $view = $this->view(
                ['project_product' => $projectProduct],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of project fintrax products. All
     * fintrax products objects are wrapped in "fintrax_products" field.
     * Fintrax object description can be found in "Get by Id" method. Also
     * output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/projects/{id}/fintrax_products",
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get fintrax products",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Fintrax Products list"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     *
     * @param Project      $project
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getProjectFintraxProductsAction(
        Project $project,
        ParamFetcher $paramFetcher
    ) {
        $pagination = $this->get('knp_paginator')->paginate(
            $project->getFintraxProducts()->toArray(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('fintrax_products', $pagination);
    }

    /**
     * @Rest\Get(
     *     name="api_v1_get_project_fintrax_products",
     *     path="/projects/{id}/fintrax_products/{fintrax_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "fintrax_product_id"="\d+"}
     * )
     *
     * @ParamConverter("fintraxProduct", class="AppBundle\Entity\FintraxProduct",
     *                                   options={"id" = "fintrax_product_id"})
     *
     * @ApiDoc(
     *     description="Get Project Fintrax Product by Id",
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\FintraxProduct"
     *     },
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="fintrax_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Fintrax Product Id"},
     *     }
     * )
     *
     * @param FintraxProduct $fintraxProduct
     *
     * @return Response
     */
    public function getProjectFintraxProductAction(
        FintraxProduct $fintraxProduct
    ) {
        return $this->handleView(
            $this->view(
                ['fintrax_product' => $fintraxProduct],
                Response::HTTP_OK
            )
        );
    }

    /**
     * @Rest\Post(
     *     path="/projects/{id}/fintrax_products",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ApiDoc(
     *     description="Create Project Fintrax Product",
     *     input="AppBundle\Form\Type\FintraxProductFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request $request
     * @param Project $project
     *
     * @return Response
     */
    public function createProjectFintraxProductAction(
        Project $project,
        Request $request
    ) {
        $fintraxProduct = new FintraxProduct();

        $fintraxProduct->setProject($project);

        return $this->processProjectFintraxProductForm(
            $fintraxProduct,
            $request
        );
    }

    /**
     * @Rest\Route(
     *     path="/projects/{id}/fintrax_products/{fintrax_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "fintrax_product_id"="\d+"}, methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("fintraxProduct", class="AppBundle\Entity\FintraxProduct",
     *                                   options={"id" = "fintrax_product_id"})
     *
     * @ApiDoc(
     *     description="Update Project Fintrax Product",
     *     input="AppBundle\Form\Type\FintraxProductFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request        $request
     * @param FintraxProduct $fintraxProduct
     *
     * @return Response
     */
    public function updateProjectFintraxProductAction(
        FintraxProduct $fintraxProduct,
        Request $request
    ) {
        return $this->processProjectFintraxProductForm(
            $fintraxProduct,
            $request
        );
    }

    /**
     * Delete Project Fintrax Product
     *
     * @Rest\Delete(
     *     path="/projects/{id}/fintrax_products/{fintrax_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "fintrax_product_id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Delete Project Fintrax Product by id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="fintrax_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Fintrax Product Id"},
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted"
     *     }
     * )
     *
     * @ParamConverter("fintraxProduct", class="AppBundle\Entity\FintraxProduct",
     *                                   options={"id" = "fintrax_product_id"})
     * @ParamConverter("project", class="AppBundle\Entity\Project",
     *                            options={"id" = "id"})
     *
     * @param FintraxProduct $fintraxProduct
     *
     * @return Response
     */
    public function deleteProjectFintraxAction(FintraxProduct $fintraxProduct)
    {
        $em = $this->getEm();

        $currentUser = $this->getUser();

        if (! $currentUser->hasRole('ROLE_ADMIN')
            && $fintraxProduct->getProject()->getOwnerId()
            !== $currentUser->getId()
        ) {
            throw new AccessDeniedException();
        }

        $em->remove($fintraxProduct);
        $em->flush();

        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }

    /**
     * @param Request        $request
     * @param FintraxProduct $fintraxProduct
     *
     * @return Response
     */
    private function processProjectFintraxProductForm(
        FintraxProduct $fintraxProduct,
        Request $request
    ) {
        $form = $this->createForm(
            FintraxProductFormType::class,
            $fintraxProduct,
            ['method' => $request->getMethod()]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $currentUser = $this->getUser();

            if (! $currentUser->hasRole('ROLE_ADMIN')
                && $fintraxProduct->getProject()->getOwnerId()
                !== $currentUser->getId()
            ) {
                throw new AccessDeniedException();
            }

            if ($form->has('stackers')) {
                foreach ($fintraxProduct->getFintraxStackers() as $stacker) {
                    $this->getEm()->remove($stacker);
                }

                $fintraxProduct->getFintraxStackers()->clear();

                /** @var Product $stackerProduct */
                foreach ($form->get('stackers')->getData() as $stackerProduct) {
                    $stacker = new FintraxStacker();
                    $stacker->setProduct($stackerProduct)->setFintraxProduct(
                        $fintraxProduct
                    );
                    $fintraxProduct->addFintraxStacker($stacker);
                }
            }

            $em = $this->getEm();
            $em->persist($fintraxProduct);
            $em->flush();

            $view = $this->view(
                ['fintrax_product' => $fintraxProduct],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of project expression cladding
     * products. All expression cladding products objects are wrapped in
     * "expression_cladding_products" field. Expression Cladding object
     * description can be found in "Get by Id" method. Also output contains
     * "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/projects/{id}/expression_cladding_products",
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Expression Cladding Products",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Expression Cladding Products list"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     *
     * @param Project      $project
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getProjectExpressionCladdingProductsAction(
        Project $project,
        ParamFetcher $paramFetcher
    ) {
        $pagination = $this->get('knp_paginator')->paginate(
            $project->getExpressionCladdingProducts()->toArray(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse(
            'expression_cladding_products',
            $pagination
        );
    }

    /**
     * @Rest\Get(
     *     name="api_v1_get_project_expression_cladding_product",
     *     path="/projects/{id}/expression_cladding_products/{expression_cladding_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "expression_cladding_product_id"="\d+"}
     * )
     *
     * @ParamConverter("ecp", class="AppBundle\Entity\ExpressionCladdingProduct",
     *                        options={"id" =
     *                        "expression_cladding_product_id"})
     *
     * @ApiDoc(
     *     description="Get Project Expression Cladding Product by Id",
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ExpressionCladdingProduct"
     *     },
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="expression_cladding_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Expression Cladding
     *        Product Id"},
     *     }
     * )
     *
     * @param ExpressionCladdingProduct $ecp
     *
     * @return Response
     */
    public function getProjectExpressionCladdingProductAction(
        ExpressionCladdingProduct $ecp
    ) {
        return $this->handleView(
            $this->view(
                ['expression_cladding_product' => $ecp],
                Response::HTTP_OK
            )
        );
    }

    /**
     * @Rest\Post(
     *     path="/projects/{id}/expression_cladding_products",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ApiDoc(
     *     description="Create Project Expression Cladding Product",
     *     input="AppBundle\Form\Type\ExpressionCladdingProductFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request $request
     * @param Project $project
     *
     * @return Response
     */
    public function createProjectExpressionCladdingProductAction(
        Project $project,
        Request $request
    ) {
        $ecp = new ExpressionCladdingProduct();

        $ecp->setProject($project);

        return $this->processProjectExpressionCladdingProductForm(
            $ecp,
            $request
        );
    }

    /**
     * @Rest\Route(
     *     path="/projects/{id}/expression_cladding_products/{expression_cladding_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "expression_cladding_product_id"="\d+"}, methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("ecp", class="AppBundle\Entity\ExpressionCladdingProduct",
     *                        options={"id" =
     *                        "expression_cladding_product_id"})
     *
     * @ApiDoc(
     *     description="Update Project Expression Cladding Product",
     *     input="AppBundle\Form\Type\ExpressionCladdingProductFormType",
     *     section="Projects",
     *     views={"v1"}
     * )
     *
     * @param Request                   $request
     * @param ExpressionCladdingProduct $ecp
     *
     * @return Response
     */
    public function updateProjectExpressionCladdingAction(
        ExpressionCladdingProduct $ecp,
        Request $request
    ) {
        return $this->processProjectExpressionCladdingProductForm(
            $ecp,
            $request
        );
    }

    /**
     * Delete Project Expression Cladding Product
     *
     * @Rest\Delete(
     *     path="/projects/{id}/expression_cladding_products/{expression_cladding_product_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *     "expression_cladding_product_id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Delete Project Expression Cladding Product by id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *        {"name"="id", "dataType"="integer",
     *        "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *        "description"="Project Id"},
     *        {"name"="expression_cladding_product_id", "dataType"="integer",
     *        "requirement"="\d+", "description"="Project Expression Cladding
     *        Id"},
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted"
     *     }
     * )
     *
     * @ParamConverter("ecp", class="AppBundle\Entity\ExpressionCladdingProduct",
     *                        options={"id" =
     *                        "expression_cladding_product_id"})
     * @ParamConverter("project", class="AppBundle\Entity\Project",
     *                            options={"id" = "id"})
     *
     * @param ExpressionCladdingProduct $ecp
     *
     * @return Response
     */
    public function deleteProjectExpressionCladdingProductAction(
        ExpressionCladdingProduct $ecp
    ) {
        $em = $this->getEm();

        $currentUser = $this->getUser();

        if (! $currentUser->hasRole('ROLE_ADMIN')
            && $ecp->getProject()->getOwnerId() !== $currentUser->getId()
        ) {
            throw new AccessDeniedException();
        }

        $em->remove($ecp);
        $em->flush();

        return $this->handleView($this->view(null, Response::HTTP_NO_CONTENT));
    }

    /**
     * @param Request                   $request
     * @param ExpressionCladdingProduct $ecp
     *
     * @return Response
     */
    private function processProjectExpressionCladdingProductForm(
        ExpressionCladdingProduct $ecp,
        Request $request
    ) {
        $form = $this->createForm(
            ExpressionCladdingProductFormType::class,
            $ecp,
            ['method' => $request->getMethod()]
        );

        $form->handleRequest($request);

        if ($form->isValid()) {
            $currentUser = $this->getUser();

            if (! $currentUser->hasRole('ROLE_ADMIN')
                && $ecp->getProject()->getOwnerId() !== $currentUser->getId()
            ) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();
            $em->persist($ecp);
            $em->flush();

            $view = $this->view(
                ['expression_cladding_product' => $ecp],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of comments. All Comments objects are
     * wrapped in "comments" field. Comment object description can be found in
     * "Get by Id" method. Also output contains "meta" field with pagination
     * info
     *
     * @Rest\Get(
     *     path="/projects/{id}/comments",
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true,
     *                               default="1", requirements="\d+",
     *                               description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true,
     *                                default="25", requirements="\d+",
     *                                description="Item count limit on page.")
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true,
     *                                    requirements="\d+",
     *                                    description="Filter By Parent Comment
     *                                    Id")
     *
     * @param Project      $project
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getProjectCommentsAction(
        Project $project,
        ParamFetcher $paramFetcher
    ) {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(ProjectComment::class)->findAllByParams(
                [
                    'project_id' => $project->getId(),
                    'parent_id'  => $paramFetcher->get('parent_id'),
                ],
                false
            ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('comments', $pagination);
    }

    /**
     * Use this method to update Project Comment by Id. Project Comment object
     * documented here was wrapped in "comment" field
     *
     * @Rest\Route(
     *     path="/projects/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\ProjectComment",
     *                            options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Update Project Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"},
     *      {"name"="comment_id", "dataType"="integer", "requirement"="\d+",
     *      "description"="Comment Id"}
     *     },
     *     section="Projects",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\ProjectComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param ProjectComment $comment
     * @param Request        $request
     *
     * @return Response
     */
    public function updateProjectCommentAction(
        ProjectComment $comment,
        Request $request
    ) {
        return $this->processProjectCommentForm($comment, $request);
    }

    /**
     * Use this method to create Comment. Comment object documented here was
     * wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="/projects/{id}/comments",
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\ProjectComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Project $project
     * @param Request $request
     *
     * @return Response
     */
    public function createProjectCommentAction(
        Project $project,
        Request $request
    ) {
        $comment = new ProjectComment();
        $comment->setProject($project);
        $comment->setUser($this->getUser());

        return $this->processProjectCommentForm($comment, $request);
    }

    /**
     * Delete Comment .
     *
     * @Rest\Delete(
     *     path="/projects/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\ProjectComment",
     *                            options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Projects",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer",
     *      "requirement"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}",
     *      "description"="Project Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some
     *         relations)"
     *     },
     * )
     *
     * @param ProjectComment $comment
     *
     * @return Response
     */
    public function deleteProjectCommentAction(ProjectComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {

            if (! $currentUser->getId()
                || (! $currentUser->hasRole(
                        'ROLE_ADMIN'
                    )
                    && $comment->getUser()->getId() !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException(
                "Can't delete Comment - there are some relations."
            );
        }

        return $this->handleView(
            $this->view(
                null,
                Response::HTTP_NO_CONTENT
            )
        );
    }

    /**
     * @param ProjectComment $comment
     * @param Request        $request
     *
     * @return Response
     */
    private function processProjectCommentForm(
        ProjectComment $comment,
        Request $request
    ) {
        $method = $request->getMethod();

        $form = $this->createForm(
            CommentFormType::class,
            $comment,
            ['method' => $method]
        );

        if ($method == 'PATCH') {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (! $currentUser->getId()
                || (! $currentUser->hasRole(
                        'ROLE_ADMIN'
                    )
                    && $comment->getUser()->getId() !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->persist($comment);
            $em->flush();

            // GENERATE PROJECT COMMENTED NOTIFICATION //
            $this->get('event_dispatcher')
                ->dispatch(
                    Events::PROJECT_COMMENTED,
                    new ProjectEvent(
                        $comment->getProject(),
                        $this->getUser(),
                        Events::PROJECT_COMMENTED,
                        $comment->getProject()->getOwner()
                    )
                );

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * @param Request $request
     * @param Project $project
     *
     * @Rest\Post(path="/projects/{id}/filters", defaults={"_format"="json"},
     *     requirements={"id"="[0-9a-f]{8}-[0-9a-f]{4}-[1-5][0-9a-f]{3}-[89ab][0-9a-f]{3}-[0-9a-f]{12}"}
     * )
     *
     * @ParamConverter("project", class="AppBundle\Entity\Project", options={"id"="id"})
     *
     * @return Response
     */
    public function attachFiltersAction(Request $request, Project $project)
    {
        /** @var User $user */
        $user = $this->getUser();

        if (false === $user->hasRole('ROLE_ADMIN') and $project->getOwnerId() !== $user->getId()) {
            throw new AccessDeniedException();
        }

        $content     = trim($request->getContent());
        $filters     = json_decode($content, true)['filters'];
        $filtersDiff = array_diff($project->getProjectFilters(), $filters);

        $em = $this->getEm();

        $filters = ['add' => $filters, 'remove' => $filtersDiff];


        foreach ($filters as $key => $value) {
            foreach ($value as $id) {
                $filter = $em->getRepository(ProjectFilter::class)->find($id);
                if ($key == 'add' && ! $project->getFilters()->contains($filter)) {
                    $project->getFilters()->add($filter);
                } elseif ($key == 'remove' && $project->getFilters()->contains($filter)) {
                    $project->getFilters()->removeElement($filter);
                }
            }
        }

        $em->flush();

        $view = $this->view(['project' => $project], Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Provide a URL in $url to empty the OG cache
     *
     * @param $url
     */
    private function clearOpenGraphCache($url)
    {
        $vars = ['id' => $url, 'scrape' => 'true'];
        $body = http_build_query($vars);

        $fp = fsockopen('ssl://graph.facebook.com', 443);
        fwrite($fp, "POST / HTTP/1.1\r\n");
        fwrite($fp, "Host: graph.facebook.com\r\n");
        fwrite($fp, "Content-Type: application/x-www-form-urlencoded\r\n");
        fwrite($fp, "Content-Length: ".strlen($body)."\r\n");
        fwrite($fp, "Connection: close\r\n");
        fwrite($fp, "\r\n");
        fwrite($fp, $body);
        fclose($fp);
    }
}
