<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Image\AbstractImage;
use AppBundle\Entity\User;
use AppBundle\Exception\ConflictException;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ImageController extends BaseController
{
    /**
     * Delete Image
     *
     * @Rest\Delete(
     *     path="/images/{image_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("image", class="AppBundle\Entity\Image\AbstractImage", options={"id" = "image_id"})
     *
     * @ApiDoc(
     *     description="Delete Image by id",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Image Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param AbstractImage $image
     * @return Response
     */
    public function deleteImageAction(AbstractImage $image)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        // todo: fix access rights
        try {
            if (
                !$currentUser->getId()
                //|| (!$currentUser->hasRole('ROLE_ADMIN') && $image->getFeed()->getUser()->getId() !== $currentUser->getId())
            ) {
                throw new AccessDeniedException();
            }

            $em->remove($image);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete image - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}
