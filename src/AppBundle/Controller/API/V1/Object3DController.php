<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\Object3D;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\Object3DFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class Object3DController extends BaseController
{
    /**
     * Use this method to get whole list of Object3Ds. All Object3Ds objects are wrapped in "object3ds" field.
     * Object3D object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     name="api_v1_get_object3ds",
     *     path="/object3ds",
     *     defaults={"_format"="json"},
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Object3Ds List",
     *     section="Object3Ds",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Object3Ds List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllObject3DAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Object3D')->findAll(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('object3ds', $pagination);
    }

    /**
     * Use this method to get Object3D by Id. Object3D object documented here was wrapped in "object3d" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_object3d",
     *     path="/object3ds/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Get Object3D by Id",
     *     section="Object3Ds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Object3D Id"}
     *     },
     *     statusCodes={
     *         404 = "Object3D not found. All other errors looks like this",
     *         200 = "Return Object3D"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Object3D"
     *     }
     * )
     *
     * @param Object3D $object3d
     * @return Response
     */
    public function getObject3DAction(Object3D $object3d)
    {
        return $this->handleView($this->view(
            ['object3d' => $object3d],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Object3D by Id. Object3D object documented here was wrapped in "object3d" field
     *
     * @Rest\Route(
     *     path="/object3ds/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Object3D",
     *     input="AppBundle\Form\Type\Object3DFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Object3D Id"}
     *     },
     *     section="Object3Ds",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Object3D"
     *     },
     *     statusCodes={
     *         404 = "Object3D not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Object3D $object3d
     * @param Request $request
     * @return Response
     */
    public function updateObject3dAction(Object3D $object3d, Request $request)
    {
        return $this->processObject3DForm($object3d, $request);
    }

    /**
     * Use this method to create Object3D. Object3D object documented here was wrapped in "object3d" field
     * 
     * @Rest\Post(
     *     path="/object3ds",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Object3D",
     *     input="AppBundle\Form\Type\Object3DFormType",
     *     section="Object3Ds",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Object3D"
     *     },
     *     statusCodes={
     *         404 = "Object3D not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createObject3DAction(Request $request)
    {
        return $this->processObject3DForm(new Object3D(), $request);
    }

    /**
     * @param Object3D $object3d
     * @param Request $request
     * @return Response
     */
    private function processObject3DForm(Object3D $object3d, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(Object3DFormType::class, $object3d, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($object3d);
            $em->flush();

            $view = $this->view(
                ['object3d' => $object3d],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Object3D.
     *
     * @Rest\Delete(
     *     path="/object3ds/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Object3D by id",
     *     section="Object3Ds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Object3D Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param Object3D $object3d
     * @return Response
     */
    public function deleteAction(Object3D $object3d)
    {
        $em = $this->getEm();

        try{
            $em->remove($object3d);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Object3D - there are some relations.";

            if($object3d->getMaterials()){
                $errorMsg .= " Related Materials: ";

                $items = [];

                foreach($object3d->getMaterials() as $material){
                    /**@var Material $material*/
                    $items[] = $material->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }


            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}