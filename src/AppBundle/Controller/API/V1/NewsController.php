<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\News;
use AppBundle\Event\Events;
use AppBundle\Event\NewsEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\NewsFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class NewsController extends BaseController
{
    /**
     * Use this method to get whole list of news. All News objects are wrapped in "news" field.
     * News object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/news",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get News",
     *     section="News",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return News List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllNewsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:News')->createQueryBuilder('news')->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('news', $pagination);
    }

    /**
     * Use this method to get News by Id. News object documented here was wrapped in "news" field.
     * 
     * @Rest\Get(
     *     path="/news/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get News by Id",
     *     section="News",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="News Id"}
     *     },
     *     statusCodes={
     *         404 = "Not found. All other errors looks like this",
     *         200 = "Return News"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\News"
     *     }
     * )
     *
     * @param News $news
     * @return Response
     */
    public function getNewsAction(News $news)
    {
        return $this->handleView($this->view(
            ['news' => $news],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update News by Id. News object documented here was wrapped in "news" field
     * 
     * @Rest\Put(
     *     path="/news/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update News",
     *     input="AppBundle\Form\Type\NewsFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="News Id"}
     *     },
     *     section="News",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\News"
     *     },
     *     statusCodes={
     *         404 = "Not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param News $news
     * @param Request $request
     * @return Response
     */
    public function updateNewsAction(News $news, Request $request)
    {
        return $this->processNewsForm($news, $request);
    }

    /**
     * Use this method to create News. News object documented here was wrapped in "news" field
     * 
     * @Rest\Post(
     *     path="/news",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create News",
     *     input="AppBundle\Form\Type\NewsFormType",
     *     section="News",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\News"
     *     },
     *     statusCodes={
     *         404 = "Not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createNewsAction(Request $request)
    {
        return $this->processNewsForm(new News(), $request);
    }

    /**
     * @param News $news
     * @param Request $request
     * @return Response
     */
    private function processNewsForm(News $news, Request $request)
    {
        $form = $this->createForm(NewsFormType::class, $news, ['method' => $request->getMethod()]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $isNewEntity = !$news->getId();

            $em = $this->getEm();
            $em->persist($news);
            $em->flush();

            if($isNewEntity){
                $this->get('event_dispatcher')->dispatch(Events::NEWS_CREATED, new NewsEvent($news));
            }

            $view = $this->view(
                ['news' => $news],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete News.
     *
     * @Rest\Delete(
     *     path="/news/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete News by id",
     *     section="News",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="News Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     }
     * )
     *
     * @param News $news
     * @return Response
     */
    public function deleteAction(News $news)
    {
        $em = $this->getEm();

        try{
            $em->remove($news);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            throw new ConflictException("Can't delete News - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}