<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\Product;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class MaterialController
 */
class MaterialController extends BaseController
{
    /**
     *
     * Use this method to get whole list of materials. All Material objects are wrapped in "materials" field.
     * Material object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     name="api_v1_get_materials",
     *     path="/materials",
     *     defaults={"_format"="json"},
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="material_type_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Type Id")
     * @Rest\QueryParam(name="material_shape_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Material Shape Type")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Application Type")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Product type Id")
     *
     * @ApiDoc(
     *     description="Get Materials list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Materials List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     *
     * @return Response
     */
    public function getMaterialsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(Material::class)->findAllByParams([
                'materialTypeId' => $paramFetcher->get('material_type_id'),
                'materialShapeId' => $paramFetcher->get('material_shape_id'),
                'productType' => $paramFetcher->get('product_type'),
                'applicationType' => $paramFetcher->get('application_type'),
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('materials', $pagination);
    }

    /**
     * @Rest\Patch(
     *     path="/materials/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Patch Materials list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Materials List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\RequestParam(name="application_types", allowBlank=false, nullable=false, requirements="[\d,]+", description="Application types")
     *
     * @param ParamFetcher $paramFetcher A ParamFetcher instance.
     * @param Material     $material     A Material entity instance.
     *
     * @return Response
     */
    public function patchMaterialsAction(ParamFetcher $paramFetcher, Material $material)
    {

        $applicationTypes = $paramFetcher->get('application_types');
        $applicationTypes = explode(',', $applicationTypes);

        $material->setApplicationTypes($applicationTypes);

        $em = $this->getEm();

        $em->persist($material);
        $em->flush();

        return $this->handleView($this->view($material, Response::HTTP_OK));
    }
    
    /**
     * Use this method to get Material by Id. Material object documented here was wrapped in "material" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_material",
     *     path="/materials/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material by Id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Id"}
     *     },
     *     statusCodes={
     *         404 = "Material not found. All other errors looks like this",
     *         200 = "Return Material"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Material"
     *     }
     * )
     *
     * @param Material $material
     * @return Response
     */
    public function getMaterialAction(Material $material)
    {
        return $this->handleView($this->view(
            ['material' => $material],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material by Id. Material object documented here was wrapped in "material" field
     *
     * @Rest\Put(
     *     path="/materials/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material",
     *     input="AppBundle\Form\Type\MaterialFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Material"
     *     },
     *     statusCodes={
     *         404 = "Material not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Material $material
     * @param Request $request
     * @return Response
     */
    public function updateMaterialAction(Material $material, Request $request)
    {
        return $this->processMaterialForm($material, $request);
    }

    /**
     * Use this method to create Material. Material object documented here was wrapped in "material" field
     * 
     * @Rest\Post(
     *     path="/materials",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material",
     *     input="AppBundle\Form\Type\MaterialFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Material"
     *     },
     *     statusCodes={
     *         404 = "Material not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialAction(Request $request)
    {
        return $this->processMaterialForm(new Material(), $request);
    }

    /**
     * @param Request $request
     * @param Material $material
     * @return Response
     */
    private function processMaterialForm(Material $material, Request $request)
    {
        $form = $this->createForm(MaterialFormType::class, $material, ['method' => $request->getMethod()]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($material);
            $em->flush();

            $view = $this->view(
                ['material' => $material],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material.
     *
     * @Rest\Delete(
     *     path="/materials/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param Material $material
     * @return Response
     */
    public function deleteAction(Material $material)
    {
        $em = $this->getEm();

        try{
            $em->remove($material);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material - there are some relations.";

            if($material->getProducts()->count()){
                $errorMsg .= " Related Products: ";

                $items = [];

                foreach($material->getProducts() as $product){
                    /**@var Product $product*/
                    $items[] = $product->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}