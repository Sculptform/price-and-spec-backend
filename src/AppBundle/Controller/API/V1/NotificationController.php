<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Notification\AbstractNotification;
use AppBundle\Entity\User;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\NotificationFormType;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class NotificationController extends BaseController
{
    /**
     * Use this method to get whole list of users notifications. All User Notifications objects are wrapped in "notifications" field.
     * User object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/notifications",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By User Id")
     * @Rest\QueryParam(name="created_at", allowBlank=true, nullable=true, requirements="today|yesterday|week|month", description="Filter created date")
     *
     * @ApiDoc(
     *     description="Get User Notifications",
     *     section="Notifications",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return User Notifications List"
     *     },
     *     resource=true
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getNotificationsAction(ParamFetcher $paramFetcher){
        $em = $this->getEm();

        $currentUserId = $this->getUser()->getId();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(AbstractNotification::class)
                ->findAllByParams(
                    [
                        'user' => $currentUserId,
                        'createdAt' => $paramFetcher->get('created_at')
                    ],
                    ['createdAt' => 'DESC'],
                    true
                ),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('notifications', $pagination);
    }

    /**
     * Use this method to get Notification by Id. Notification object documented here was wrapped in "notification" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_notification",
     *     path="/notifications/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Notification by Id",
     *     section="Notifications",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Notification Id"}
     *     },
     *     statusCodes={
     *         404 = "Entity not found. All other errors looks like this",
     *         200 = "Return Notification"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Notification\AbstractNotification"
     *     }
     * )
     *
     * @param AbstractNotification $notification
     * @return Response
     */
    public function getNotificationAction(AbstractNotification $notification)
    {
        /** @var User $currentUser */
        $currentUser = $this->getUser();

        if(!$currentUser->hasRole('ROLE_ADMIN') && $notification->getUser()->getId() !== $currentUser->getId()){
            throw new AccessDeniedException();
        }

        if(!!$notification->getReadAt()){
            throw new AccessDeniedException();
        }

        return $this->handleView($this->view(
            ['newsfeed' => $notification],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Notification by Id. Notification object documented here was wrapped in "notification" field
     *
     * @Rest\Put(
     *     path="/notifications/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update Notification",
     *     input="AppBundle\Form\Type\NotificationFormType",
     *     requirements={
     *          {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Notification Id"}
     *     },
     *     section="Notifications",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Notification\AbstractNotification"
     *     },
     *     statusCodes={
     *         404 = "Material Type not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param AbstractNotification $notification
     * @param Request $request
     * @return Response
     */
    public function updateNotificationAction(AbstractNotification $notification, Request $request)
    {
        return $this->processNotificationForm($notification, $request);
    }

    /**
     * @param AbstractNotification $notification
     * @param Request $request
     * @return Response
     */
    private function processNotificationForm(AbstractNotification $notification, Request $request)
    {
        $form = $this->createForm(NotificationFormType::class, $notification, ['method' => $request->getMethod()]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if(!$currentUser->hasRole('ROLE_ADMIN') && $notification->getUser()->getId() !== $currentUser->getId()) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();
            $em->persist($notification);
            $em->flush();

            $view = $this->view(
                ['newsfeed' => $notification],
                Response::HTTP_OK
            );
        }
        else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}