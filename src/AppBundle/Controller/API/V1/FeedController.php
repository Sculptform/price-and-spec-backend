<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Feed;
use AppBundle\Entity\User;
use AppBundle\Entity\Image\FeedImage;
use AppBundle\Entity\Comment\FeedComment;
use AppBundle\Enum\SharedContentType;
use AppBundle\Event\Events;
use AppBundle\Event\FeedEvent;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\FeedFormType;
use AppBundle\Form\Type\ImageFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class FeedController
 * @package AppBundle\Controller\API\V1
 * @Route("/feeds")
 */
class FeedController extends BaseController
{
    /**
     * Use this method to get whole list of feeds. All Feeds objects are wrapped in "feeds" field.
     * Feed object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Feeds",
     *     section="Feeds",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Feeds List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="User Id")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt|interactions", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="desc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="followers", nullable=true, allowBlank=true, default="0", requirements="1|0", description="Filter by user followers")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $currentUser = $this->getUser();

        $em = $this->getEm();

        $repository = $em->getRepository(Feed::class);

        $results = $repository->getListQb(
            $currentUser,
            $paramFetcher->get('user_id'),
            $paramFetcher->get('sort'),
            $paramFetcher->get('direction'),
            $paramFetcher->get('followers')
        )->getQuery()->getResult();

        $pagination = $this->get('knp_paginator')->paginate(
            $results,
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('feeds', $pagination);
    }

    /**
     * Use this method to get page in ordered/sorted/filtered list for required feed.
     *
     * @Rest\Get(
     *     path="/{id}/page",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Feeds List Page",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Feeds List Page"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="User Id")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt|interactions", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param Request $request
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListPage(Request $request, ParamFetcher $paramFetcher)
    {
        $feedId = intval($request->get('id'));
        $userId = intval($paramFetcher->get('user_id'));
        $limit = intval($paramFetcher->get('limit'));

        $feedPosition = $this->getEm()->getRepository(Feed::class)->getFeedPositionInSortedList(
            $feedId,
            $userId,
            $paramFetcher->get('sort', 'createdAt'),
            $paramFetcher->get('direction', 'desc')
        );

        $page = ceil($feedPosition / $limit);

        $view = $this->view([
            'page' => $page,
            'feedPosition' => $feedPosition
        ], Response::HTTP_OK);

        return $this->handleView($view);
    }

    /**
     * Use this method to get Feed by Id. Feed object documented here was wrapped in "feed" field.
     *
     * @Rest\Get(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Feed by Id",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         404 = "Feed not found. All other errors looks like this",
     *         200 = "Return Feed"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Feed"
     *     }
     * )
     *
     * @param Feed $feed
     * @return Response
     */
    public function getFeedAction(Feed $feed)
    {
        return $this->handleView($this->view(
            ['feed' => $feed],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Feed by Id. Feed object documented here was wrapped in "feed" field
     *
     * @Rest\Route(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Feed",
     *     input="AppBundle\Form\Type\FeedFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     section="Feeds",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Feed"
     *     },
     *     statusCodes={
     *         404 = "Feed not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Feed $feed
     * @param Request $request
     * @return Response
     */
    public function updateFeedAction(Feed $feed, Request $request)
    {
        return $this->processFeedForm($feed, $request);
    }

    /**
     * Use this method to create Feed. Feed object documented here was wrapped in "feed" field
     *
     * @Rest\Post(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Feed",
     *     input="AppBundle\Form\Type\FeedFormType",
     *     section="Feeds",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Feed"
     *     },
     *     statusCodes={
     *         404 = "Feed not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createFeedAction(Request $request)
    {
        $feed = new Feed();
        $feed->setUser($this->getUser());

        return $this->processFeedForm($feed, $request);
    }

    /**
     * @param Feed $feed
     * @param Request $request
     * @return Response
     */
    private function processFeedForm(Feed $feed, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(FeedFormType::class, $feed, ['method' => $method]);

        if ($method == $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $feed->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            if(array_key_exists('shared_content', $data = $request->get($form->getName()))){
                $types = SharedContentType::getTitles();
                $events = SharedContentType::getEvents();

                $content = $types[$data['shared_content']['type']];
                $eventClass = 'AppBundle\Event\\'.$content.'Event';
                $event = $events[$data['shared_content']['type']];

                $sharedContent = $em->getRepository('AppBundle:'.$content)->find(current($data['shared_content'])['id']);
                $feed->setSharedContent($sharedContent);

                $this->get('event_dispatcher')
                    ->dispatch($event, new $eventClass($sharedContent, $this->getUser(), $event));
            }

            $em->persist($feed);

            $em->flush();

            $view = $this->view(
                ['feed' => $feed],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Feed .
     *
     * @Rest\Delete(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Feed by id",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param Feed $feed
     * @return Response
     */
    public function deleteFeedAction(Feed $feed)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {
            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $feed->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->remove($feed);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Feed - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * Use this method to add current user to liked users
     *
     * @Rest\Route(
     *     path="/{id}/like",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT"}
     * )
     *
     * @ApiDoc(
     *     description="Like feed",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     section="Feeds",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Feed not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Feed $feed
     * @return Response
     */
    public function likeFeedAction(Feed $feed)
    {
        $em = $this->getEm();

        $feed->addLikedUser($this->getUser());

        $em->persist($feed);

        $em->flush();

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        $this->get('event_dispatcher')
            ->dispatch(Events::FEED_LIKED, new FeedEvent($feed, $this->getUser(), Events::FEED_LIKED));

        return $this->handleView($view);
    }

    /**
     * Use this method to remove current user from liked users
     *
     * @Rest\Route(
     *     path="/{id}/dislike",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"DELETE"}
     * )
     *
     * @ApiDoc(
     *     description="Like feed",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     section="Feeds",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Feed not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         201 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Feed $feed
     * @return Response
     */
    public function dislikeFeedAction(Feed $feed)
    {
        $em = $this->getEm();

        $feed->removeLikedUser($this->getUser());

        $em->persist($feed);

        $em->flush();

        $view = $this->view(
            null,
            Response::HTTP_NO_CONTENT
        );

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of comments. All Comments objects are wrapped in "comments" field.
     * Comment object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Parent Comment Id")
     *
     * @param Feed $feed
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getFeedCommentsAction(Feed $feed, ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(FeedComment::class)->findAllByParams([
                'feed_id' => $feed->getId(),
                'parent_id' => $paramFetcher->get('parent_id')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('comments', $pagination);
    }

    /**
     * Use this method to update Feed Comment by Id. Feed Comment object documented here was wrapped in "feed" field
     *
     * @Rest\Route(
     *     path="/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\FeedComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Update Feed Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"},
     *     },
     *     section="Feeds",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\FeedComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param FeedComment $comment
     * @param Request $request
     * @return Response
     */
    public function updateFeedCommentAction(FeedComment $comment, Request $request)
    {
        return $this->processFeedCommentForm($comment, $request);
    }

    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\FeedComment"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Feed $feed
     * @param Request $request
     * @return Response
     */
    public function createFeedCommentAction(Feed $feed, Request $request)
    {
        $comment = new FeedComment();
        $comment->setFeed($feed);
        $comment->setUser($this->getUser());

        return $this->processFeedCommentForm($comment, $request);
    }

    /**
     * Delete Comment .
     *
     * @Rest\Delete(
     *     path="/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\FeedComment", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param FeedComment $comment
     * @return Response
     */
    public function deleteFeedCommentAction(FeedComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try {

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        } catch (ForeignKeyConstraintViolationException $e) {
            throw new ConflictException("Can't delete Comment - there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param FeedComment $comment
     * @param Request $request
     * @return Response
     */
    private function processFeedCommentForm(FeedComment $comment, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(CommentFormType::class, $comment, ['method' => $method]);

        if ($method == $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($comment);
            $em->flush();

            // GENERATE FEED COMMENTED NOTIFICATION //
            $this->get('event_dispatcher')
                ->dispatch(Events::FEED_COMMENTED, new FeedEvent(
                    $comment->getFeed(),
                    $this->getUser(),
                    Events::FEED_COMMENTED)
                );

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to create Feed Image. Feed Image object documented here was wrapped in "image" field
     *
     * @Rest\Post(
     *     path="/{id}/images",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Add Image",
     *     input="AppBundle\Form\Type\ImageFormType",
     *     section="Feeds",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Feed Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Image\FeedImage"
     *     },
     *     statusCodes={
     *         404 = "Feed not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     },
     *     resource=true
     * )
     *
     * @param Feed $feed
     * @param Request $request
     * @return Response
     */
    public function createFeedImageAction(Feed $feed, Request $request)
    {
        $image = new FeedImage();
        $image->setFeed($feed);

        return $this->processFeedImageForm($image, $request);
    }

    /**
     * @param FeedImage $image
     * @param Request $request
     * @return Response
     */
    private function processFeedImageForm(FeedImage $image, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ImageFormType::class, $image, ['method' => $method]);

        if ($method == $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if (!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $image->getFeed()->getUser()->getId() !== $currentUser->getId())) {
                throw new AccessDeniedException();
            }

            $em->persist($image);
            $em->flush();

            $view = $this->view(['image' => $image], Response::HTTP_OK);
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}