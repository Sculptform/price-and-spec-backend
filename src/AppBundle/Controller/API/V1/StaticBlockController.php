<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\StaticBlock;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;

/**
 * Class StaticBlockController
 * @package AppBundle\Controller\API\V1
 * @Route("/static_blocks")
 */
class StaticBlockController extends BaseController
{
    /**
     * Use this method to get Block by Name. Block object documented here was wrapped in "static_block" field.
     *
     * @Rest\Get(
     *     path="/{block_name}",
     *     defaults={"_format"="json"},
     *     requirements={"block_name"="\w+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Block by Id",
     *     section="Static Blocks",
     *     views={"v1"},
     *     requirements={
     *      {"name"="block_name", "dataType"="string", "requirement"="\w+", "description"="Block Name"}
     *     },
     *     statusCodes={
     *         404 = "Block not found. All other errors looks like this",
     *         200 = "Return Block"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\StaticBlock"
     *     }
     * )
     *
     * @ParamConverter("block", class="AppBundle:StaticBlock", options={
     *    "mapping": {"block_name": "name"},
     *    "map_method_signature" = true
     * })
     *
     * @param StaticBlock $block
     * @return Response
     */
    public function getStaticBlockAction(StaticBlock $block)
    {
        return $this->handleView($this->view(
            ['static_block' => $block],
            Response::HTTP_OK
        ));
    }
}