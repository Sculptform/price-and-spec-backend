<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\MaterialShapeFormType;
use AppBundle\Form\Type\MaterialShapeSizeFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class MaterialShapeController extends BaseController
{
    /**
     * Use this method to get whole list of material shapes. All Material Shape objects are wrapped in "material_shapes" field.
     * Material Shape object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_shapes",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Application Type")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Product type Id")
     *
     * @ApiDoc(
     *     description="Get Material Shapes list",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Shapes List"
     *     },
     *     resource=true
     * )
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getMaterialShapesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em
                ->getRepository('AppBundle:MaterialShape')
                ->findAllByParams([
                    'productType' => $paramFetcher->get('product_type'),
                    'applicationType' => $paramFetcher->get('application_type')
                ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('material_shapes', $pagination);
    }

    /**
     * Use this method to get Material Shape by Id. Material Shape object documented here was wrapped in "material_shape" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_material_shape",
     *     path="/material_shapes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Material Shape by Id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found. All other errors looks like this",
     *         200 = "Return Material Shape"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShape"
     *     }
     * )
     *
     * @param MaterialShape $materialShape
     * @return Response
     */
    public function getMaterialShapeAction(MaterialShape $materialShape)
    {
        return $this->handleView($this->view(
            ['material_shape' => $materialShape],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Material Shape by Id. Material Shape object documented here was wrapped in "material_shape" field
     *
     * @Rest\Put(
     *     path="/material_shapes/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Update Material Shape",
     *     input="AppBundle\Form\Type\MaterialShapeFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShape"
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     *
     * )
     *
     * @param MaterialShape $materialShape
     * @param Request $request
     * @return Response
     */
    public function updateMaterialShapeAction(MaterialShape $materialShape, Request $request)
    {
        return $this->processMaterialShapeForm($materialShape, $request);
    }

    /**
     * Use this method to create Material Shape. Material Shape object documented here was wrapped in "material_shape" field
     *
     * @Rest\Post(
     *     path="/material_shapes",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material Shape",
     *     input="AppBundle\Form\Type\MaterialShapeFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShape"
     *     },
     *     statusCodes={
     *         404 = "Material Shape not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createMaterialShapeAction(Request $request)
    {
        return $this->processMaterialShapeForm(new MaterialShape(), $request);
    }

    /**
     * @param Request $request
     * @param MaterialShape $materialShape
     * @return Response
     */
    private function processMaterialShapeForm(MaterialShape $materialShape, Request $request)
    {
        $form = $this->createForm(MaterialShapeFormType::class, $materialShape, ['method' => $request->getMethod()]);

        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($materialShape);
            $em->flush();

            $view = $this->view(
                ['material_shape' => $materialShape],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to get whole list of material shape sizes by material shape ID. All Material Shape Sizes objects are wrapped in "material_shape_sizes" field.
     * Material Shape Sizes object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/material_shapes/{id}/material_shape_sizes",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Material Shape Sizes List",
     *     section="Materials",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Material Shape Sizes List"
     *     },
     *     resource=true
     * )
     *
     * @param int $page
     * @param int $limit
     * @param MaterialShape $materialShape
     * @return Response
     */
    public function getMaterialShapeSizes(MaterialShape $materialShape, $page, $limit){
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:MaterialShapeSize')->findBy(['materialShape' => $materialShape], ['width' => 'asc']),
            $page,
            $limit
        );

        return $this->paginationResponse('material_shape_sizes', $pagination);
    }

    /**
     * Use this method to get Material Shape Size by Id. Material Shape Size object documented here was wrapped in "material_shape_size" field.
     *
     * @Rest\Get(
     *     name="api_v1_get_material_shape_size",
     *     path="/material_shapes/{id}/material_shape_sizes/{material_shape_size_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+", "material_shape_size_id"="\d+"}
     * )
     *
     * @ParamConverter("materialShapeSize", class="AppBundle:MaterialShapeSize", options={"id" = "material_shape_size_id"})
     *
     * @ApiDoc(
     *     description="Get Material Shape Size by Id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     statusCodes={
     *         404 = "Material Shape Size not found. All other errors looks like this",
     *         200 = "Return Material Shape Size"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     }
     * )
     *
     * @param MaterialShapeSize $materialShapeSize
     * @return Response
     */
    public function getMaterialShapeSizeAction(MaterialShapeSize $materialShapeSize)
    {
        return $this->handleView($this->view(
            ['material_shape_size' => $materialShapeSize],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to create Material Shape Size. Material Shape Size object documented here was wrapped in "material_shape_size" field
     *
     * @Rest\Post(
     *     path="/material_shapes/{id}/material_shape_sizes",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Create Material Shape Size",
     *     input="AppBundle\Form\Type\MaterialShapeSizeFormType",
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     },
     *     statusCodes={
     *         404 = "Material Shape Size not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param MaterialShape $materialShape
     * @param Request $request
     * @return Response
     */
    public function createMaterialShapeSizeAction(MaterialShape $materialShape, Request $request)
    {
        $materialShapeSize = new MaterialShapeSize();

        $materialShapeSize->setMaterialShape($materialShape);

        return $this->processMaterialShapeSizeForm($materialShapeSize, $request);
    }

    /**
     * Use this method to update Material Shape Size by Id. Material Shape Size object documented here was wrapped in "material_shape_size" field
     *
     * @Rest\Route(
     *     path="/material_shapes/{id}/material_shape_sizes/{material_shape_size_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("materialShapeSize", class="AppBundle:MaterialShapeSize", options={"id" = "material_shape_size_id"})
     *
     * @ApiDoc(
     *     description="Update Material Shape Size",
     *     input="AppBundle\Form\Type\MaterialShapeSizeFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     section="Materials",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\MaterialShapeSize"
     *     },
     *     statusCodes={
     *         404 = "Material Shape Size not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param MaterialShapeSize $materialShapeSize
     * @param Request $request
     * @return Response
     */
    public function updateMaterialShapeSizeAction(MaterialShapeSize $materialShapeSize, Request $request)
    {
        return $this->processMaterialShapeSizeForm($materialShapeSize, $request);
    }

    /**
     * @param Request $request
     * @param MaterialShapeSize $materialShapeSize
     * @return Response
     */
    private function processMaterialShapeSizeForm(MaterialShapeSize $materialShapeSize, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(MaterialShapeSizeFormType::class, $materialShapeSize, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($materialShapeSize);
            $em->flush();

            $view = $this->view(
                ['material_shape_size' => $materialShapeSize],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Material Shape.
     *
     * @Rest\Delete(
     *     path="/material_shapes/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Material Shape by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialShape $MaterialShape
     * @return Response
     */
    public function deleteMaterialShapeAction(MaterialShape $MaterialShape)
    {
        $em = $this->getEm();

        try{
            $em->remove($MaterialShape);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Shape - there are some relations.";

            if($materialShapeSizes = $MaterialShape->getMaterialShapeSizes()){
                $errorMsg .= " Related Materials : ";

                $items = [];

                foreach($materialShapeSizes as $materialShapeSize){
                    /**@var MaterialShapeSize $materialShapeSize*/
                    $materials = $materialShapeSize->getMaterials();

                    foreach ($materials as $material){
                        /**@var Material $material*/
                        if (!in_array($material->getId(), $items)){
                            $items[] = $material->getId();
                        }
                    }
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * Delete Material Shape Size.
     *
     * @Rest\Delete(
     *     path="/material_shapes/{id}/material_shape_sizes/{material_shape_size_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+", "material_shape_size_id"="\d+"}
     * )
     *
     * @ParamConverter("materialShapeSize", class="AppBundle:MaterialShapeSize", options={"id" = "material_shape_size_id"})
     *
     * @ApiDoc(
     *     description="Delete Material Size by id",
     *     section="Materials",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Material Shape Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param MaterialShapeSize $materialShapeSize
     * @return Response
     */
    public function deleteMaterialShapeSizeAction(MaterialShapeSize $materialShapeSize)
    {
        $em = $this->getEm();

        try{
            $em->remove($materialShapeSize);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Shape Size - there are some relations.";

            if($materialShapeSize->getMaterials()->count()){
                $errorMsg .= " Related Materials: ";

                $items = [];

                foreach($materialShapeSize->getMaterials() as $material){
                    /**@var Material $material*/
                    $items[] = $material->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}