<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Sector;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class SectorController extends BaseController
{
    /**
     * Use this method to get whole list of sectors. All sectors objects are wrapped in "sectors" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/sectors",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Sectors",
     *     section="Sectors",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Sectors List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="user_id", allowBlank=true, nullable=true, requirements="\d+", description="User Id")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt|likes", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(Sector::class)->findAll(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit'),
            [
                'defaultSortFieldName' => 'id',
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->paginationResponse('sectors', $pagination);
    }
}