<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\DTO\StatsDTO;
use AppBundle\DTO\UserStatsDTO;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Quotation;
use AppBundle\Entity\User;
use AppBundle\Entity\UserFollower;
use AppBundle\Entity\UserProject;
use AppBundle\Enum\UserProjectUserType;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class StatsController extends BaseController
{
    /**
     * Use this method to get site stats object
     *
     * @Rest\Get(
     *     path="/stats",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Get Current User Stats",
     *     section="Statistics",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Stats"
     *     },
     *     resource=true,
     *     output="AppBundle\DTO\StatsDTO"
     * )
     * @return Response
     */
    public function getStatsAction()
    {
        $stats = new StatsDTO();

        $em = $this->getEm();

        $projectRepository = $em->getRepository(Project::class);
        $quotationRepository = $em->getRepository(Quotation::class);
        $userRepository = $em->getRepository(User::class);
        $projectProductRepository = $em->getRepository(ProjectProduct::class);

        $mostUsedProductType = $projectRepository->getMostUsedProductType();

        if ($mostUsedProductType) {
            $stats
                ->setMostUsedProductType($em->getRepository(ProductType::class)->find($mostUsedProductType['id']))
                ->setMostUsedProductTypeUses($mostUsedProductType['uses']);
        }

        $mostUsedShape = $projectProductRepository->getMostUsedShape();

        if ($mostUsedShape) {
            $stats
                ->setMostUsedShape($em->getRepository(MaterialShape::class)->find($mostUsedShape['id']))
                ->setMostUsedShapePreviewUses($mostUsedShape['uses']);
        }

        $mostUsedFinish = $projectProductRepository->getMostUsedFinish();

        if ($mostUsedFinish) {
            $stats
                ->setMostUsedFinish($em->getRepository(MaterialFinish::class)->find($mostUsedFinish['id']))
                ->setMostUsedFinishUses($mostUsedFinish['uses']);
        }

        $mostUsedColor = $projectProductRepository->getMostUsedColor();

        if ($mostUsedColor) {
            $stats
                ->setMostUsedColor($em->getRepository(MaterialFinish::class)->find($mostUsedColor['id']))
                ->setMostUsedColorUses($mostUsedColor['uses']);
        }

        $stats
            ->setTotalDesigns($projectRepository->countAll())
            ->setTotalUsers($userRepository->countAll())
            ->setTotalQuotations($quotationRepository->countAll());

        return $this->handleView($this->view(
            ['stats' => [$stats]],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to get stats object for required user
     *
     * @Rest\Get(
     *     path="/stats/user/{id}",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get User Stats",
     *     section="Statistics",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="User Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Stats"
     *     },
     *     resource=false,
     *     output="AppBundle\DTO\UserStatsDTO"
     * )
     *
     * @param User $user
     * @return Response
     */
    public function getUserStatsAction(User $user){
        $stats = new UserStatsDTO();

        $em = $this->getEm();

        $projectRepository = $em->getRepository(Project::class);
        $quotationRepository = $em->getRepository(Quotation::class);
        $userFollowerRepository = $em->getRepository(UserFollower::class);
        $userProjectRepository = $em->getRepository(UserProject::class);

        $daysSinceLastDesign = 0;
        $averageProjectPerDay = 0;

        $countProjects = $projectRepository->countAll($user, UserProjectUserType::OWNER);

        if ($countProjects) {
            $lastUserProject = $userProjectRepository->getLastProject($user);
            $firstUserProject = $userProjectRepository->getFirstProject($user);

            $currentDate = new \DateTime();
            $lastProjectCreatedAt = $lastUserProject->getProject()->getCreatedAt();
            $firstProjectCreatedAt = $firstUserProject->getProject()->getCreatedAt();

            $daysSinceLastDesign = $currentDate->diff($lastProjectCreatedAt)->days;
            $daysSinceFirstDesign = $currentDate->diff($firstProjectCreatedAt)->days;
            $averageProjectPerDay = round($countProjects / ($daysSinceFirstDesign ?: 1), 2);
        }

        $stats
            ->setId($user->getId())
            ->setCreatedDesigns($countProjects)
            ->setSharedDesigns($projectRepository->countAll($user, UserProjectUserType::SHARED))
            ->setTotalQuotations($quotationRepository->countAll($user))
        ;

        $stats->setAwards($user->getAwards());
        $stats->setFollowers($user->getFollowers()->count());
        $stats->setFollowing(count($userFollowerRepository->findBy(['follower'=>$user->getId()])));
        $stats->setDaysSinceLastDesign($daysSinceLastDesign);
        $stats->setAverageProjectPerDay($averageProjectPerDay);


        return $this->handleView($this->view(
            ['user_stat' => $stats],
            Response::HTTP_OK
        ));
    }
}
