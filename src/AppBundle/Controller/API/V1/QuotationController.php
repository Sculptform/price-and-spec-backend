<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Quotation;
use AppBundle\Entity\User;
use AppBundle\Enum\PardotTrackActionsEnum;
use AppBundle\Enum\PardotUserLastAction;
use AppBundle\Event\Events;
use AppBundle\Event\PardotTrackActionEvent;
use AppBundle\Event\QuotationEvent;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\QuotationFormType;
use Symfony\Component\EventDispatcher\EventDispatcher;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpFoundation\Request;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

/**
 * Class QuotationController
 */
class QuotationController extends BaseController
{
    /**
     * Use this method to create Quotation. Quotation object documented here
     * was wrapped in "quotation" field
     *
     * @Rest\Post(
     *     path="/quotations",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Quotation",
     *     input="AppBundle\Form\Type\QuotationFormType",
     *     section="Quotations",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Quotation"
     *     },
     *     statusCodes={
     *         404 = "Quotation not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     *
     * @return Response
     */
    public function createQuotationAction(Request $request)
    {
        return $this->processQuotationForm(new Quotation(), $request);
    }

    /**
     * @param Quotation $quotation
     * @param Request   $request
     *
     * @return Response
     */
    private function processQuotationForm(Quotation $quotation, Request $request
    ) {
        $method = $request->getMethod();
        $dispatcher = $this->container->get('event_dispatcher');

        $form = $this->createForm(
            QuotationFormType::class, $quotation, ['method' => $method]
        );

        if ($method === $request::METHOD_PATCH) {
            $form->submit($request->request->get($form->getName()), false);
        } else {
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            if (null === $quotation->getUser()) {
                $quotation->setUser($currentUser);
            }

            if ( ! $currentUser->hasRole('ROLE_ADMIN')
                && $quotation->getUser()->getId() !== $currentUser->getId()
            ) {
                throw new AccessDeniedException();
            }

            $em = $this->getEm();
            $em->persist($quotation);
            $em->flush();

            try {
                $pardotService  = $this->get('app.service.pardot');
                $pardotResponse = $pardotService->sendUserLastAction(
                    $currentUser, PardotUserLastAction::REQUEST_QUOTE, [
                        'phone' => $quotation->getPhone(),
                        'company' => $quotation->getCompanyName(),
                        'notes'   => $quotation->getContent(),
                    ]
                );
                $this->get('logger')->addInfo($pardotResponse);
            } catch (\Exception $e) {
                $this->get('logger')->addError($e->getMessage());
            }

            $dispatcher->dispatch(
                PardotTrackActionEvent::NAME, new PardotTrackActionEvent(
                    $currentUser, $quotation->getProject(),
                    PardotTrackActionsEnum::START_ORDER
                )
            );

            $dispatcher->dispatch(
                Events::QUOTATION_RECEIVED, new QuotationEvent($quotation)
            );

            $view = $this->view(['quotation' => $quotation], Response::HTTP_OK);
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}
