<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Railing;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class RailingController
 * @package AppBundle\Controller\API\V1
 * @Route("/railings")
 */
class RailingController extends BaseController
{
    /**
     * Use this method to get whole list of Railing. All Railing objects are wrapped in "railing" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="application_type", allowBlank=true, nullable=true, default=null, requirements="\d+", description="Application type id")
     * @Rest\QueryParam(name="product_type", allowBlank=true, nullable=true, default=null, requirements="\d+", description="Product type id")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @ApiDoc(
     *     description="Get Railings list",
     *     section="Railings",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Railings List"
     *     },
     *     resource=true
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher){
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(Railing::class)->findAllByParams([
                'product_type'     => $paramFetcher->get('product_type'),
                'application_type' => $paramFetcher->get('application_type')
            ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('railings', $pagination);
    }

    /**
     * Use this method to get Railing by Id. Railing object documented here was wrapped in "railing" field.
     *
     * @Rest\Get(
     *     path="/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Railing by Id",
     *     section="Railings",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Railing Id"}
     *     },
     *     statusCodes={
     *         404 = "Feed not found. All other errors looks like this",
     *         200 = "Return Railing"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Railing"
     *     }
     * )
     *
     * @param Railing $railing
     * @return Response
     */
    public function getAction(Railing $railing)
    {
        return $this->handleView($this->view(
            ['railing' => $railing],
            Response::HTTP_OK
        ));
    }
}