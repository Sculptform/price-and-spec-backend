<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Tag;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Routing\Annotation\Route;
use FOS\RestBundle\Controller\Annotations\View;

/**
 * Class TagController
 * @package AppBundle\Controller\API\V1
 * @Route("/tags")
 */
class TagController extends BaseController
{
    /**
     * Use this method to get whole list of tags. All Tags objects are wrapped in "tags" field.
     * Tag object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="",
     *     defaults={"_format"="json"}
     * )
     *
     * @View(serializerGroups={"List"})
     *
     * @ApiDoc(
     *     description="Get Tags",
     *     section="Tags",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Tags List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="createdAt", requirements="createdAt", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="content", allowBlank=true, nullable=true, description="Filter by Tags content")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(Tag::class)->findAllByParams([
                'content' => $paramFetcher->get('content')
            ]),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('tags', $pagination);
    }
}