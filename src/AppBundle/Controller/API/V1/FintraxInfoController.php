<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 19.07.16
 * Time: 16:28
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\FintraxInfo;
use AppBundle\Entity\MaterialFinish;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;

class FintraxInfoController extends BaseController
{
    /**
     * Use this method to get whole list of Fintrax infos. All FintraxInfo objects are wrapped in "fintrax_infos" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/fintrax_infos",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="wide", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax wide.")
     * @Rest\QueryParam(name="deep", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax deep.")
     * @Rest\QueryParam(name="finish_id", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax finish Id.")
     * @Rest\QueryParam(name="nosing_id", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax nosing shape Id.")
     *
     * @ApiDoc(
     *     description="Get FintraxInfo list",
     *     section="Fintrax Info",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return FintraxInfo List"
     *     },
     *     resource=true
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getListAction(ParamFetcher $paramFetcher){
        $em = $this->getEm();

        $finishGroups = [];

        $finish = $em->find(MaterialFinish::class, intval($paramFetcher->get('finish_id')));

        if ($finish) {
            $finishGroup = $finish->getGroup();

            if ($finishGroup) {
                $finishGroups = explode('_', $finishGroup->getPath());
            }
        }

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(FintraxInfo::class)->findAllByParams([
                'width' => $paramFetcher->get('wide'),
                'depth' => $paramFetcher->get('deep'),
                'finish_groups' => $finishGroups,
                'nosing_id' => $paramFetcher->get('nosing_id'),
            ], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')
        );

        return $this->paginationResponse('fintrax_infos', $pagination);
    }

    /**
     * Use this method to get find one of Fintrax info. FintraxInfo object are wrapped in "fintrax_info" field.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/fintrax_info",
     *     defaults={"_format"="json"}
     * )
     *
     * @Rest\QueryParam(name="wide", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax wide.")
     * @Rest\QueryParam(name="deep", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax deep.")
     * @Rest\QueryParam(name="finish_id", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax finish Id.")
     * @Rest\QueryParam(name="nosing_id", allowBlank=true, nullable=true, requirements="\d+", description="Fintrax nosing shape Id.")
     *
     * @ApiDoc(
     *     description="Get FintraxInfo",
     *     section="Fintrax Info",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return FintraxInfo"
     *     }
     * )
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAction(ParamFetcher $paramFetcher){
        $em = $this->getEm();

        $finishGroups = [];

        $finish = $em->find(MaterialFinish::class, intval($paramFetcher->get('finish_id')));

        if ($finish) {
            $finishGroup = $finish->getGroup();

            if ($finishGroup) {
                $finishGroups = explode('_', $finishGroup->getPath());
            }
        }

        $fintraxInfo = $em->getRepository(FintraxInfo::class)->findAllByParams([
            'width' => $paramFetcher->get('wide'),
            'depth' => $paramFetcher->get('deep'),
            'finish_groups' => $finishGroups,
            'nosing_id' => $paramFetcher->get('nosing_id'),
        ], true)->setMaxResults(1)->getOneOrNullResult();

        if (!$fintraxInfo) {
            throw $this->createNotFoundException('Fintrax info not found!');
        }

        return $this->handleView($this->view(
            ['fintrax_info' => $fintraxInfo],
            Response::HTTP_OK
        ));
    }
}