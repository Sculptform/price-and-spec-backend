<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Comment\ProductSandpitComment;
use AppBundle\Entity\ProductSandpit;
use AppBundle\Entity\User;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\CommentFormType;
use AppBundle\Form\Type\ProductSandpitFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class ProductSandpitController extends BaseController
{
    /**
     * Use this method to get whole list of product sandpit. All Product Sandpit objects are wrapped in "product_sandpit" field.
     * Product Sandpit object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/product_sandpit",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Product Sandpit List",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Product Sandpit List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="title", requirements="title|createdAt", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getProductSandpitListAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(ProductSandpit::class)->findAllByParams([], true),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit'),
            [
                'defaultSortFieldName' => 'title',
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->paginationResponse('product_sandpit', $pagination);
    }

    /**
     * Use this method to get Product Sandpit by Id. Product Sandpit object documented here was wrapped in "product_sandpit" field.
     * 
     * @Rest\Get(
     *     name="api_v1_get_product_sandpit",
     *     path="/product_sandpit/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Product Sandpit by Id",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     statusCodes={
     *         404 = "Product Sandpit not found. All other errors looks like this",
     *         200 = "Return Product Sandpit"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductSandpit"
     *     }
     * )
     *
     * @param ProductSandpit $productSandpit
     * @return Response
     */
    public function getProductSandpitAction(ProductSandpit $productSandpit)
    {
        return $this->handleView($this->view(
            ['product_sandpit' => $productSandpit],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Product Sandpit by Id. Product Sandpit object documented here was wrapped in "product_sandpit" field
     * 
     * @Rest\Route(
     *     path="/product_sandpit/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Product Sandpit",
     *     input="AppBundle\Form\Type\ProductSandpitFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     section="Product Sandpit",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductSandpit"
     *     },
     *     statusCodes={
     *         404 = "Product Sandpit not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param ProductSandpit $productSandpit
     * @param Request $request
     * @return Response
     */
    public function updateProductSandpitAction(ProductSandpit $productSandpit, Request $request)
    {
        return $this->processProductSandpitForm($productSandpit, $request);
    }

    /**
     * Use this method to create Product Sandpit. Product Sandpit object documented here was wrapped in "product_sandpit" field
     * 
     * @Rest\Post(
     *     path="/product_sandpit",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Product Sandpit",
     *     input="AppBundle\Form\Type\ProductSandpitFormType",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductSandpit"
     *     },
     *     statusCodes={
     *         404 = "Product Sandpit not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createProductSandpitAction(Request $request)
    {
        return $this->processProductSandpitForm(new ProductSandpit(), $request);
    }

    /**
     * @param ProductSandpit $productSandpit
     * @param Request $request
     * @return Response
     */
    private function processProductSandpitForm(ProductSandpit $productSandpit, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ProductSandpitFormType::class, $productSandpit, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($productSandpit);
            $em->flush();

            $view = $this->view(
                ['product_sandpit' => $productSandpit],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Product Sandpit.
     *
     * @Rest\Delete(
     *     path="/product_sandpit/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Product Sandpit by id",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param ProductSandpit $productSandpit
     * @return Response
     */
    public function deleteAction(ProductSandpit $productSandpit)
    {
        $em = $this->getEm();

        try{
            $em->remove($productSandpit);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Product Sandpit - there are some relations.";

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * Use this method to get whole list of comments. All Comments objects are wrapped in "comments" field.
     * Comment object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/product_sandpit/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Comments",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     statusCodes={
     *         200 = "Return Comments List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     * @Rest\QueryParam(name="parent_id", allowBlank=true, nullable=true, requirements="\d+", description="Filter By Parent Comment Id")
     *
     * @param ProductSandpit $productSandpit
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getProductSandpitCommentsAction(ProductSandpit $productSandpit, ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(ProductSandpitComment::class)->findAllByParams([
                'product_sandpit_id' => $productSandpit->getId(),
                'parent_id'       => $paramFetcher->get('parent_id')
            ], false),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('comments', $pagination);
    }

    /**
     * Use this method to update Product Sandpit Comment by Id. Product Sandpit Comment object documented here was wrapped in "product_sandpit" field
     *
     * @Rest\Route(
     *     path="/product_sandpit/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\ProductSandpit", options={"id" = "comment_id"})
     * 
     * @ApiDoc(
     *     description="Update Product Sandpit Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     section="Product Sandpit",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\ProductSandpit"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @ParamConverter("productSandpit", class="AppBundle:ProductSandpit", options={"id" = "id"})
     * @ParamConverter("comment", class="AppBundle:Comment\ProductSandpit", options={"id" = "comment_id"})
     *
     * @param ProductSandpitComment $comment
     * @param Request $request
     * @return Response
     */
    public function updateProductSandpitCommentAction(ProductSandpitComment $comment, Request $request)
    {
        return $this->processProductSandpitCommentForm($comment, $request);
    }
    
    /**
     * Use this method to create Comment. Comment object documented here was wrapped in "comment" field
     *
     * @Rest\Post(
     *     path="/product_sandpit/{id}/comments",
     *     requirements={"id"="\d+"},
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Comment",
     *     input="AppBundle\Form\Type\CommentFormType",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Comment\ProductSandpit"
     *     },
     *     statusCodes={
     *         404 = "Comment not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param ProductSandpit $productSandpit
     * @param Request $request
     * @return Response
     */
    public function createProductSandpitCommentAction(ProductSandpit $productSandpit, Request $request)
    {
        $comment = new ProductSandpitComment();
        $comment->setProductSandpit($productSandpit);
        $comment->setUser($this->getUser());

        return $this->processProductSandpitCommentForm($comment, $request);
    }

    /**
     * Delete Comment .
     *
     * @Rest\Delete(
     *     path="/product_sandpit/{id}/comments/{comment_id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ParamConverter("comment", class="AppBundle\Entity\Comment\ProductSandpit", options={"id" = "comment_id"})
     *
     * @ApiDoc(
     *     description="Delete Comment by id",
     *     section="Product Sandpit",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Sandpit Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param ProductSandpitComment $comment
     * @return Response
     */
    public function deleteProductSandpitCommentAction(ProductSandpitComment $comment)
    {
        $em = $this->getEm();

        /** @var User $currentUser */
        $currentUser = $this->getUser();

        try{

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            $em->remove($comment);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            throw new ConflictException("Can't delete Comment- there are some relations.");
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }

    /**
     * @param ProductSandpitComment $comment
     * @param Request $request
     * @return Response
     */
    private function processProductSandpitCommentForm(ProductSandpitComment $comment, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(CommentFormType::class, $comment, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN') && $comment->getUser()->getId() !== $currentUser->getId())){
                throw new AccessDeniedException();
            }

            $em->persist($comment);
            
            $em->flush();

            $view = $this->view(
                ['comment' => $comment],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}