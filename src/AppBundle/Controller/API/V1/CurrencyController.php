<?php

namespace AppBundle\Controller\API\V1;

use AppBundle\Entity\Currency;
use Symfony\Component\Routing\Annotation;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use AppBundle\Controller\API\BaseController;
use FOS\RestBundle\Controller\Annotations as Rest;

/**
 * Class CurrencyController
 *
 * @package AppBundle\Controller\API\V1
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 *
 * @Annotation\Route("currencies")
 */
class CurrencyController extends BaseController
{
    /**
     * @Rest\Get(path="", defaults={"_format"="json"})
     *
     * @ApiDoc(
     *     description="Get currency list",
     *     section="Currencies",
     *     views={"v1"},
     *     statusCodes={200 = "Return Currency List"},
     *     resource=true
     * )
     *
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function getListAction()
    {
        $em = $this->getEm();

        $currencies = $em->getRepository(Currency::class)->findBy(['isActive' => true]);

        $view = $this->view([
            'currency_types' => $currencies
        ]);

        return $this->handleView($view);
    }
}
