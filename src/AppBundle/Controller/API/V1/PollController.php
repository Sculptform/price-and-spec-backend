<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 17.11.16
 * Time: 9:50
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Poll;
use AppBundle\Entity\PollAnswer;
use AppBundle\Entity\User;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\PollAnswerFormType;
use AppBundle\Form\Type\PollFormType;
use Doctrine\ORM\NoResultException;
use FOS\RestBundle\Request\ParamFetcher;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use FOS\RestBundle\Controller\Annotations as Rest;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use Symfony\Component\Security\Core\Exception\AccessDeniedException;

class PollController extends BaseController
{
    /**
     * Use this method to get whole list of polls. All Polls objects are wrapped in "polls" field.
     * Polls object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/polls",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Polls",
     *     section="Polls",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Polls List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getAllPollsAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository('AppBundle:Poll')->createQueryBuilder('poll')->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit')

        );

        return $this->paginationResponse('polls', $pagination);
    }

    /**
     * Use this method to get Poll by Id. Poll object documented here was wrapped in "poll" field.
     *
     * @Rest\Get(
     *     path="/polls/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Polls by Id",
     *     section="Polls",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Poll Id"}
     *     },
     *     statusCodes={
     *         404 = "Poll not found. All other errors looks like this",
     *         200 = "Return Poll"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Poll"
     *     }
     * )
     *
     * @param Poll $poll
     * @return Response
     */
    public function getPollAction(Poll $poll)
    {
        return $this->handleView($this->view(
            ['poll' => $poll],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to get current weekly poll. Poll object documented here was wrapped in "poll" field.
     *
     * @Rest\Get(
     *     path="/polls/weekly",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Get Current Weekly Poll",
     *     section="Polls",
     *     views={"v1"},
     *     statusCodes={
     *         404 = "Poll not found. All other errors looks like this",
     *         200 = "Return Poll"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Poll"
     *     }
     * )
     *
     * @return Response
     */
    public function getWeeklyPollAction()
    {
        $em = $this->getEm();

        $poll = $em->getRepository('AppBundle:Poll')->getWeeklyPoll();

        if ($poll) {
            $view = $this->view(['poll' => $poll], Response::HTTP_OK);
        } else {
            $view = $this->view(null, Response::HTTP_NOT_FOUND);
        }

        return $this->handleView($view);
    }

    /**
     * Use this method to update Poll by Id. Poll object documented here was wrapped in "poll" field
     *
     * @Rest\Route(
     *     path="/polls/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Poll",
     *     input="AppBundle\Form\Type\PollFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Poll Id"}
     *     },
     *     section="Polls",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Poll"
     *     },
     *     statusCodes={
     *         404 = "Poll not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Poll $poll
     * @param Request $request
     * @return Response
     */
    public function updatePollAction(Poll $poll, Request $request)
    {
        return $this->processPollForm($poll, $request);
    }

    /**
     * Use this method to create Poll. Poll object documented here was wrapped in "poll" field
     *
     * @Rest\Post(
     *     path="/polls",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Poll",
     *     input="AppBundle\Form\Type\PollFormType",
     *     section="Polls",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\Poll"
     *     },
     *     statusCodes={
     *         404 = "Poll not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createPollAction(Request $request)
    {
        return $this->processPollForm(new Poll(), $request);
    }

    /**
     * Use this method to adding PollAnswer by Poll Id. Poll object documented here was wrapped in "poll" field
     *
     * @Rest\Route(
     *     path="/polls/{id}/answers",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"POST"}
     * )
     *
     * @ApiDoc(
     *     description="Add PollAnswer",
     *     input="AppBundle\Form\Type\PollAnswerFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Poll Id"}
     *     },
     *     section="Polls",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\PollAnswer"
     *     },
     *     statusCodes={
     *         404 = "Poll not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param Poll $poll
     * @param Request $request
     * @return Response
     */
    public function addPollAnswer(Poll $poll, Request $request)
    {
        return $this->processPollAnswerForm($poll, $request);
    }

    /**
     * Use this method to add current user answer to poll.
     *
     * @Rest\Put(
     *     path="/polls/current_user_answer/{answerId}",
     *     defaults={"_format"="json"},
     *     requirements={"answerId"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Add PollAnswerUser",
     *     section="Polls",
     *     views={"v1"},
     *     requirements={
     *      {"name"="answerId", "dataType"="integer", "requirement"="\d+", "description"="Answer Id"}
     *     },
     *     statusCodes={
     *         404 = "Entity not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @ParamConverter("answer", class="AppBundle:PollAnswer", options={"id" = "answerId"})
     *
     * @param PollAnswer $answer
     * @return Response
     */
    public function addCurrentUserAnswer(PollAnswer $answer)
    {
        $answer->addUser($this->getUser());

        $errors = $this->get('validator')->validate($answer);

        if ($errors->count()) {
            throw new ValidationException($errors);
        } else {

            $em = $this->getEm();

            $em->persist($answer);
            $em->flush();

            $view = $this->view(null, Response::HTTP_OK);
        }

        return $this->handleView($view);
    }
    /**
     * @param Poll $poll
     * @param Request $request
     * @return Response
     */
    private function processPollForm(Poll $poll, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(PollFormType::class, $poll, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN'))){
                throw new AccessDeniedException();
            }

            $em->persist($poll);
            $em->flush();

            $view = $this->view(
                ['poll' => $poll],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * @param Poll $poll
     * @param Request $request
     * @return Response
     */
    private function processPollAnswerForm(Poll $poll, Request $request)
    {
        $method = $request->getMethod();

        $pollAnswer = new PollAnswer();
        $pollAnswer->setPoll($poll);

        $form = $this->createForm(PollAnswerFormType::class, $pollAnswer, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            /** @var User $currentUser */
            $currentUser = $this->getUser();

            $em = $this->getEm();

            if(!$currentUser->getId() || (!$currentUser->hasRole('ROLE_ADMIN'))){
                throw new AccessDeniedException();
            }

            $em->persist($pollAnswer);
            $em->flush();

            $view = $this->view(
                ['poll' => $poll],
                Response::HTTP_OK
            );
        } else {
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }
}