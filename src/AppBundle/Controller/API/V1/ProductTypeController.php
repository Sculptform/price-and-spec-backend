<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 23.06.2016
 * Time: 17:21
 */

namespace AppBundle\Controller\API\V1;

use AppBundle\Controller\API\BaseController;
use AppBundle\Entity\Material;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use AppBundle\Exception\ConflictException;
use AppBundle\Exception\ValidationException;
use AppBundle\Form\Type\ProductTypeFormType;
use Doctrine\DBAL\Exception\ForeignKeyConstraintViolationException;
use FOS\RestBundle\Request\ParamFetcher;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Nelmio\ApiDocBundle\Annotation\ApiDoc;
use FOS\RestBundle\Controller\Annotations as Rest;

class ProductTypeController extends BaseController
{
    /**
     * Use this method to get whole list of product types. All Product Types objects are wrapped in "product_types" field.
     * Product Type object description can be found in "Get by Id" method.
     * Also output contains "meta" field with pagination info
     *
     * @Rest\Get(
     *     path="/product_types",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Get Product Types",
     *     section="Product Types",
     *     views={"v1"},
     *     statusCodes={
     *         200 = "Return Product Types List"
     *     },
     *     resource=true
     * )
     *
     * @Rest\QueryParam(name="sort", allowBlank=true, nullable=true, default="title", requirements="title|createdAt", description="Sort field")
     * @Rest\QueryParam(name="direction", allowBlank=true, nullable=true, default="asc", requirements="asc|desc", description="Sort direction")
     * @Rest\QueryParam(name="page", allowBlank=true, nullable=true, default="1", requirements="\d+", description="Page of the overview.")
     * @Rest\QueryParam(name="limit", allowBlank=true, nullable=true, default="25", requirements="\d+", description="Item count limit on page.")
     *
     * @param ParamFetcher $paramFetcher
     * @return Response
     */
    public function getProductTypesAction(ParamFetcher $paramFetcher)
    {
        $em = $this->getEm();

        $pagination = $this->get('knp_paginator')->paginate(
            $em->getRepository(ProductType::class)
                ->createQueryBuilder('pt')
                ->addSelect([
                    'pt.id as HIDDEN id',
                    'pt.title as HIDDEN title',
                    'pt.createdAt as HIDDEN createdAt'
                ])
                ->where("pt.title <> 'Fintrax'")
                ->getQuery(),
            $paramFetcher->get('page'),
            $paramFetcher->get('limit'),
            [
                'defaultSortFieldName' => 'id',
                'defaultSortDirection' => 'asc'
            ]
        );

        return $this->paginationResponse('product_types', $pagination);
    }

    /**
     * Use this method to get Product Type by Id. Product Type object documented here was wrapped in "product_type" field.
     * 
     * @Rest\Get(
     *     name="api_v1_get_product_type",
     *     path="/product_types/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"}
     * )
     *
     * @ApiDoc(
     *     description="Get Product Type by Id",
     *     section="Product Types",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Type Id"}
     *     },
     *     statusCodes={
     *         404 = "Product Type not found. All other errors looks like this",
     *         200 = "Return Product Type"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     },
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductType"
     *     }
     * )
     *
     * @param ProductType $productType
     * @return Response
     */
    public function getProductTypeAction(ProductType $productType)
    {
        return $this->handleView($this->view(
            ['product_type' => $productType],
            Response::HTTP_OK
        ));
    }

    /**
     * Use this method to update Product Type by Id. Product Type object documented here was wrapped in "product_type" field
     * 
     * @Rest\Route(
     *     path="/product_types/{id}",
     *     defaults={"_format"="json"},
     *     requirements={"id"="\d+"},
     *     methods={"PUT", "PATCH"}
     * )
     *
     * @ApiDoc(
     *     description="Update Product Type",
     *     input="AppBundle\Form\Type\ProductTypeFormType",
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Type Id"}
     *     },
     *     section="Product Types",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductType"
     *     },
     *     statusCodes={
     *         404 = "Product Type not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful update"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIException"
     *         }
     *     }
     * )
     *
     * @param ProductType $productType
     * @param Request $request
     * @return Response
     */
    public function updateProductTypeAction(ProductType $productType, Request $request)
    {
        return $this->processProductTypeForm($productType, $request);
    }

    /**
     * Use this method to create Product Type. Product Type object documented here was wrapped in "product_type" field
     * 
     * @Rest\Post(
     *     path="/product_types",
     *     defaults={"_format"="json"}
     * )
     *
     * @ApiDoc(
     *     description="Create Product Type",
     *     input="AppBundle\Form\Type\ProductTypeFormType",
     *     section="Product Types",
     *     views={"v1"},
     *     output={
     *         "parsers"={
     *              "Nelmio\ApiDocBundle\Parser\JmsMetadataParser",
     *          },
     *         "class"="AppBundle\Entity\ProductType"
     *     },
     *     statusCodes={
     *         404 = "Product Type not found.",
     *         400 = "Bad request. All other errors looks like this",
     *         200 = "Successful create"
     *     },
     *     responseMap={
     *         404 = {
     *             "class"="AppBundle\Model\APIException"
     *         },
     *         400 = {
     *             "class"="AppBundle\Model\APIBadRequestException"
     *         }
     *     }
     * )
     *
     * @param Request $request
     * @return Response
     */
    public function createProductTypeAction(Request $request)
    {
        return $this->processProductTypeForm(new ProductType(), $request);
    }

    /**
     * @param ProductType $productType
     * @param Request $request
     * @return Response
     */
    private function processProductTypeForm(ProductType $productType, Request $request)
    {
        $method = $request->getMethod();

        $form = $this->createForm(ProductTypeFormType::class, $productType, ['method' => $method]);

        if($method == 'PATCH'){
            $form->submit($request->request->get($form->getName()), false);
        }
        else{
            $form->handleRequest($request);
        }

        if ($form->isValid()) {
            $em = $this->getEm();
            $em->persist($productType);
            $em->flush();

            $view = $this->view(
                ['product_type' => $productType],
                Response::HTTP_OK
            );
        }
        else{
            throw new ValidationException($form->getErrors(true));
        }

        return $this->handleView($view);
    }

    /**
     * Delete Product Type.
     *
     * @Rest\Delete(
     *     path="/product_types/{id}",
     *     defaults={"_format"="json"},
     * )
     *
     * @ApiDoc(
     *     description="Delete Product Type by id",
     *     section="Product Types",
     *     views={"v1"},
     *     requirements={
     *      {"name"="id", "dataType"="integer", "requirement"="\d+", "description"="Product Type Id"}
     *     },
     *     statusCodes={
     *         Response::HTTP_NO_CONTENT = "Returned when successful",
     *         404 = "Returned when entity does not exists",
     *         400 = "Returned when form can not be submitted",
     *         409 = "Returned when can't delete entity (there are some relations)"
     *     },
     * )
     *
     * @param ProductType $productType
     * @return Response
     */
    public function deleteAction(ProductType $productType)
    {
        $em = $this->getEm();

        try{
            $em->remove($productType);
            $em->flush();
        }
        catch(ForeignKeyConstraintViolationException $e){
            $errorMsg = "Can't delete Material Type - there are some relations.";

            if($productType->getMaterials()->count()){
                $errorMsg .= " Related Materials: ";

                $items = [];

                foreach($productType->getMaterials() as $material){
                    /**@var Material $material*/
                    $items[] = $material->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            if($productType->getProjects()->count()){
                $errorMsg .= " Related Projects: ";

                $items = [];

                foreach($productType->getProjects() as $project){
                    /**@var Project $project*/
                    $items[] = $project->getId();
                }

                $errorMsg .= implode(', ', $items).';';
            }

            throw new ConflictException($errorMsg);
        }

        return $this->handleView($this->view(
            null,
            Response::HTTP_NO_CONTENT
        ));
    }
}