<?php

namespace AppBundle\Controller\API;

use Symfony\Component\Form\FormInterface;

/**
 * Trait FormErrorsTrait
 */
trait FormErrorsTrait
{
    /**
     * @param FormInterface $form
     *
     * @return array
     */
    public function getErrorsFromForm(FormInterface $form)
    {
        $errors = [];
        foreach ($form->getErrors() as $error) {
            $errors[] = $error->getMessage();
        }

        foreach ($form->all() as $childForm) {
            if ($childForm instanceof FormInterface) {
                if ($childErrors = $this->getErrorsFromForm($childForm)) {
                    $childFormName = $childForm->getName();

                    if ($childFormName === 'username') {
                        continue;
                    }

                    if ($childFormName === 'plainPassword') {
                        $childFormName = 'password';
                        if (array_key_exists('first', $childErrors)) {
                            $childErrors = $childErrors['first'];
                        }
                        if (array_key_exists('second', $childErrors)) {
                            $childErrors = $childErrors['second'];
                        }
                    }

                    $errors[$childFormName] = $childErrors;
                }
            }
        }

        return $errors;
    }
}
