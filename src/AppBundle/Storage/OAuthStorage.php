<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Storage;


use FOS\OAuthServerBundle\Model\ClientInterface;
use OAuth2\Model\IOAuth2Client;
use OAuth2\OAuth2;
use OAuth2\OAuth2ServerException;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use FOS\OAuthServerBundle\Storage\OAuthStorage as BaseOAuthStorage;

/**
 * Class OAuthStorage
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Storage
 */
class OAuthStorage extends BaseOAuthStorage
{
    public function checkUserCredentials(IOAuth2Client $client, $username, $password)
    {
        if (! $client instanceof ClientInterface) {
            throw new \InvalidArgumentException('Client has to implement the ClientInterface');
        }

        try {
            $user = $this->userProvider->loadUserByUsername($username);
        } catch (AuthenticationException $e) {
            throw new OAuth2ServerException(
                Response::HTTP_BAD_REQUEST,
                OAuth2::ERROR_INVALID_GRANT,
                "The email address you entered doesn’t match any account, please check your email or create a new account."
            );

        }

        if (null !== $user) {
            $encoder = $this->encoderFactory->getEncoder($user);

            if ($encoder->isPasswordValid($user->getPassword(), $password, $user->getSalt())) {
                return [
                    'data' => $user,
                ];
            } else {
                throw new OAuth2ServerException(
                    Response::HTTP_BAD_REQUEST,
                    OAuth2::ERROR_INVALID_GRANT,
                    "Invalid password."
                );
            }
        }

        return false;
    }
}
