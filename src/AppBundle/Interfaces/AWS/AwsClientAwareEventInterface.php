<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Interfaces\AWS;


use AppBundle\Service\AWS\RDSManagerService;
use Aws\AwsClientInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Interface AwsClientAwareEventInterface
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */
interface AwsClientAwareEventInterface
{
    /**
     * AwsClientAwareEventInterface constructor.
     *
     * @param RDSManagerService        $manager
     */
    public function __construct(RDSManagerService $manager);

    /**
     * @return AwsClientInterface
     */
    public function getManager();
}
