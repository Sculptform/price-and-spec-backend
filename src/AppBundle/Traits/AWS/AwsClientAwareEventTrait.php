<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Traits\AWS;

use AppBundle\Service\AWS\RDSManagerService;
use Aws\AwsClientInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;

/**
 * Trait AwsClientAwareEventTrait
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Traits\AWS
 */
trait AwsClientAwareEventTrait
{

    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     * @var RDSManagerService $manager
     */
    protected $manager;

    /**
     * AwsClientAwareEventTrait constructor.
     *
     * @param RDSManagerService $manager
     */
    public function __construct(RDSManagerService $manager)
    {
        $this->manager    = $manager;
        $this->dispatcher = $manager->getDispatcher();
    }

    /**
     * @return RDSManagerService
     */
    public function getManager()
    {
        return $this->manager;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }
}
