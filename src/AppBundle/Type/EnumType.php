<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Type;

use Doctrine\DBAL\Types\Type;
use Doctrine\DBAL\Platforms\AbstractPlatform;

/**
 * Class EnumType
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Type
 */
abstract class EnumType extends Type
{
    protected $name;
    protected $values = [];

    public function getSQLDeclaration(
        array $fieldDeclaration, AbstractPlatform $platform
    ) {
        $values = array_map(
            function ($val) {
                return "'".$val."'";
            }, $this->values
        );

        return "ENUM(".implode(", ", $values).")";
    }

    public function convertToPHPValue($value, AbstractPlatform $platform)
    {
        return $value;
    }

    public function convertToDatabaseValue($value, AbstractPlatform $platform)
    {
        if ( ! in_array($value, $this->values)) {
            throw new \InvalidArgumentException(
                "Invalid '".$this->name."' value."
            );
        }

        return $value;
    }

    public function getName()
    {
        return $this->name;
    }

    public function requiresSQLCommentHint(AbstractPlatform $platform)
    {
        return true;
    }

    public function getValues()
    {
        return $this->values;
    }
}
