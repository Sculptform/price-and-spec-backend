<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Type;

use AppBundle\Enum\PardotTrackActionsEnum;
use AppBundle\Enum\PardotUserLastAction;

/**
 * Class PardotLastActionType
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\DBAL
 */
class PardotLastActionType extends EnumType
{
    protected $name = 'pardot_last_action';
    protected $values
        = [
            PardotTrackActionsEnum::DOWNLOAD_PDF,
            PardotTrackActionsEnum::REQUEST_SAMPLE,
            PardotTrackActionsEnum::DOWNLOAD_DWG,
            PardotTrackActionsEnum::START_ORDER,
            PardotTrackActionsEnum::DOWNLOAD_XLS,
        ];
}
