<?php

namespace AppBundle\Service;

use GuzzleHttp\Client;

/**
 * Class ExchangeRates
 *
 * @package AppBundle\Service
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class ExchangeRates
{
    const BASE_URI       = 'https://api.exchangeratesapi.io';
    const BASE_CURRENCY  = 'AUD';
    const DEFAULT_SYMBOL = 'NZD';

    /** @var Client */
    private $client;

    public function __construct()
    {
        $this->client = new Client(['base_uri' => self::BASE_URI]);
    }

    /**
     * @param string $baseCurrency
     * @param string $symbol
     *
     * @return array
     *
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function latest($baseCurrency = self::BASE_CURRENCY, $symbol = self::DEFAULT_SYMBOL)
    {
        $response = $this->client->request('GET', '/latest', [
            'query' => [
                'base'    => $baseCurrency,
                'symbols' => $symbol
            ]
        ]);

        if (200 !== $response->getStatusCode()) {
            throw new \Exception($response->getBody()->getContents());
        }

        return json_decode($response->getBody()->getContents(), true);
    }
}
