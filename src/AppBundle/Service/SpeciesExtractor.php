<?php

namespace AppBundle\Service;

use AppBundle\Entity\Scene;
use AppBundle\Entity\Project;
use Doctrine\ORM\EntityManagerInterface;

/**
 * Extract species from project
 *
 * @package AppBundle\Service
 */
class SpeciesExtractor
{
    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    /**
     * @param Project $project
     *
     * @return string
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function extract(Project $project)
    {
        $response = "N/A";
        if ($project->getScenes()->count() === 0) {
            return $response;
        }

        /** @var Scene $scene */
        $scene = $project->getScenes()->first();

        $sceneArray = $scene->getScene();

        $finishId = [];
        if (isset($sceneArray['scene']['object']['children'])) {
            foreach ($sceneArray['scene']['object']['children'] as $item) {
                if (isset($item['materialFinishModel'])) {
                    $finishId[] = $item['materialFinishModel'];
                }
            }
        }

        if (count($finishId) > 0) {
            $where = implode(', ', $finishId);

            $titles = $this
                ->em
                ->getConnection()
                ->query(/** @lang text */ "SELECT title FROM material_finish WHERE id IN({$where})")
                ->fetchAll();

            if ($titles) {
                $titles = array_column($titles, 'title');
                $response = implode(", ", $titles);
            }
        }

        return $response;
    }
}
