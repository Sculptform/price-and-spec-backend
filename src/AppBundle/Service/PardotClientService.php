<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Service;


use GuzzleHttp\Client;

/**
 * Class PardotClientService
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Service
 */
class PardotClientService
{
    /**
     * @var string
     */
    protected $baseUri = 'https://pi.pardot.com';

    /**
     * @var string
     */
    protected $apiVersion = '4';

    /**
     * @var string
     */
    protected $email;

    /**
     * @var string
     */
    protected $password;

    /**
     * @var string
     */
    protected $userKey;

    /**
     * @var string
     */
    protected $apiKey;

    /**
     * @var Client
     */
    protected $client;

    /**
     * PardotClientService constructor.
     *
     * @param string $email
     * @param string $password
     * @param string $userKey
     */
    public function __construct(string $email, string $password, string $userKey
    ) {
        $this->client = new Client(
            [
                'base_uri' => $this->baseUri,
            ]
        );

        $this->email    = $email;
        $this->password = $password;
        $this->userKey  = $userKey;

        $this->auth();
    }

    /**
     * @return void
     */
    private function auth()
    {
        $options = [
            'form_params' => [
                "email"    => $this->email,
                "password" => $this->password,
                "user_key" => $this->userKey,
            ],
            'headers'     => [
                'Content-Type', 'application/x-www-form-urlencoded',
            ],
        ];

        try {
            $response = $this->client->post($this->buildUrl('login'), $options);

            $xmlResponse = new \SimpleXMLElement(
                $response->getBody()->getContents()
            );

            $this->apiKey = $xmlResponse->api_key->__toString();
        } catch (\Throwable $ex) {
        }
    }

    /**
     * @param $path
     *
     * @return string
     */
    private function buildUrl($path)
    {
        return sprintf('/api/%s/version/%s', $path, $this->apiVersion);
    }
}
