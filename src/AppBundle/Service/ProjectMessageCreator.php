<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Entity\Project;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;

/**
 * Class ProjectMessageCreator
 *
 * @package AppBundle\Service
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class ProjectMessageCreator
{
    private static $contentTypes = [
        'pdf'  => 'application/pdf',
        'docx' => 'application/msword',
        'dwg'  => 'image/vnd.dwg',
        'xlsx' => 'application/vnd.ms-excel',
    ];

    /** @var EngineInterface */
    private $engine;

    /** @var string */
    private $address;

    /** @var string */
    private $sender;

    /**
     * @param EngineInterface $engine
     * @param string          $address
     * @param string          $sender
     */
    public function __construct(EngineInterface $engine, string $address, string $sender)
    {
        $this->engine = $engine;
        $this->address = $address;
        $this->sender = $sender;
    }

    /**
     * @param User    $user
     * @param Project $project
     * @param string  $content
     * @param string  $type
     *
     * @return \Swift_Message
     */
    public function create(User $user, Project $project, $content, $type)
    {
        $subject = $this->getSubject($project, $type);
        $body    = $this->getBody($user, $project, $type);
        $attach  = $this->getAttachment($project, $content, $type);

        $message = (new \Swift_Message($subject))
            ->setFrom($this->address, $this->sender)
            ->setTo($user->getEmail(), $user->getFullName())
            ->setBody($body, 'text/html')
            ->attach($attach)
        ;

        return $message;
    }

    /**
     * @param Project $project
     * @param string  $content
     * @param string  $type
     *
     * @return \Swift_Mime_Attachment
     */
    protected function getAttachment(Project $project, $content, $type)
    {
        $type = strtolower($type);
        $filename = "{$project->getTitle()}.{$type}";
        $contentType = static::$contentTypes[$type];

        return \Swift_Attachment::newInstance($content, $filename, $contentType);
    }
    
    /**
     * @param Project $project
     * @param string  $type
     *
     * @return string
     */
    protected function getSubject(Project $project, $type)
    {
        return sprintf('Project "%s" was exported as '.$type, $project->getTitle());
    }

    /**
     * @param User    $user
     * @param Project $project
     * @param string  $type
     *
     * @return string
     */
    protected function getBody(User $user, Project $project, $type)
    {
        $type = strtolower($type);

        return $this->engine->render(
            "AppBundle:Email/Project:export_{$type}.html.twig", [
                'project' => $project,
                'user'    => $user,
                'type'    => $type,
            ]
        );
    }
}
