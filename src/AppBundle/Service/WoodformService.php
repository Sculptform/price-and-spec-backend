<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 30.09.2016
 * Time: 13:01
 */

namespace AppBundle\Service;

use AppBundle\DTO\WoodformUserDTO;
use AppBundle\Entity\NewsFeed\DrupalNewsFeed;
use AppBundle\Entity\User;
use AppBundle\Exception\ValidationException;
use AppBundle\Security\WoodformAuthenticator;
use GuzzleHttp\Client;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Response;
use GuzzleHttp\RequestOptions;
use JMS\Serializer\Serializer;
use Psr\Http\Message\RequestInterface;
use Psr\Http\Message\ResponseInterface;
use AppBundle\Model\JsonStream;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class WoodformService implements ContainerAwareInterface
{
    const AUTH_HEADER = 'Authorization';

    use ContainerAwareTrait;

    /** @var Client */
    protected $http;

    /** @var string */
    protected $baseUri;

    /**
     * WoodformService constructor.
     *
     * @param string $baseUri
     */
    public function __construct($baseUri)
    {
        $this->baseUri = $baseUri;

        $stack = HandlerStack::create();

        $stack->push($this->jsonStreamHandlerMiddleware());
        $stack->push($this->authorizationHandlerMiddleware());

        $httpClientConfig = [
            'handler' => $stack,
            'headers' => ['User-Agent' => 'WoodformAPI/1.0'],
            'base_uri' => rtrim($baseUri, '/') . '/api/'
        ];

        $this->http = new Client($httpClientConfig);
    }

    /**
     * @param string $key
     * @return User
     * @throws ValidationException
     */
    public function getUserByOAuthKey($key){
        $woodformUser = $this->getWoodformUser($key);

        /** @var User $user */
        $user = $this->getUserManager()->findUserBy(['uid' => $woodformUser->getUid()]);

        if(!$user) {
            $user = $this->getUserManager()->createUser();
            $user
                ->setEnabled(true)
                ->setPlainPassword(base64_encode(random_bytes(10)));
        } else {
            $this->getUserManager()->reloadUser($user);
        }

        $this->updateUserByWoodformUser($user, $woodformUser);

        $this->validateObject($user, ['Profile']);

        $this->getUserManager()->updateUser($user);

        return $user;
    }

    /**
     * @param User $user
     * @throws \Exception
     */
    public function updateWoodformUserByUser(User $user){
        if(!$this->getOAuthKey()){
            return;
        }

        $response = $this->http->get("user/{$user->getUuid()}");
        $userData = $this->serializeResponse($response);

        if (count($userData)) {
            $picture = &$userData['picture'];

            if ($picture && $picture['uuid'] !== $user->getPictureUuid()) {
                $this->http->delete("file/{$picture['uuid']}");
            }

            if($user->getImageFile()) {
                $imageFile = $user->getImageFile();
                $imagePath = $imageFile->getRealPath();

                $filename = sprintf(
                    'picture-%s-%s.%s',
                    $user->getUid(),
                    $imageFile->getCTime(),
                    $imageFile->guessExtension()
                );

                $picture = [
                    "filename" => $filename,
                    "file"     => base64_encode(file_get_contents($imagePath)),
                    'filemime' => $imageFile->getMimeType(),
                    'filesize' => $imageFile->getSize()
                ];

                $response = $this->http->post("entity_file", [RequestOptions::JSON => $picture]);
                $picture = $this->serializeResponse($response);

                if ($picture['fid']) {
                    $response = $this->http->get("file?parameters[fid]={$picture['fid']}");
                    $pictures = $this->serializeResponse($response);

                    $picture = array_pop($pictures);

                    if ($picture) {
                        $user->setPictureUuid($picture['uuid']);

                        $userData['picture'] = $picture;
                    }
                }

                $this->http->put("user/{$user->getUuid()}", [RequestOptions::JSON => $userData]);
            }
        } else {
            throw new \Exception('Unknown error');
        }

        $response = $this->http->get("profile/{$user->getPid()}");
        $profile = $this->serializeResponse($response);

        $profile = array_merge($profile, [
            'first_name' => $user->getFirstName(),
            'surname' => $user->getLastName(),
            'company_name' => $user->getCompanyName(),
            'i_am_a' => $user->getPosition(),
            'phone_number' => $user->getPhone()
        ]);

        $this->http->put("profile/{$user->getPid()}", [RequestOptions::JSON => $profile]);
    }

    /**
     * @return array
     */
    public function getNewsListData()
    {
        $newsFeeds = [];

        $response = $this->http->get('entity_node?parameters[type]=article&sort=created&direction=DESC');

        foreach ($this->serializeResponse($response) as $newsFeed) {
            $entity = new DrupalNewsFeed();
            $date = new \DateTime();

            if ($newsFeed['image']) {
                $response = $this->http->get("file/{$newsFeed['image']['file']['uuid']}");
                $image = $this->serializeResponse($response);

                $imageUrl = join([
                    $this->baseUri,
                    '/sites/default/files/',
                    str_replace('public://','', $image['uri'])
                ]);

                $entity->setImageUrl($imageUrl);
            }

            $entity->setCreatedAt($date->setTimestamp($newsFeed['created']));
            $entity->setTitle($newsFeed['title']);
            $entity->setUrl($newsFeed['url']);

            $newsFeeds[] = $entity;
        }

        return $newsFeeds;
    }

    /**
     * @return callable
     */
    protected function jsonStreamHandlerMiddleware()
    {
        return Middleware::mapResponse(function (ResponseInterface $response) {
            return $response->withBody(new JsonStream($response->getBody()));
        });
    }

    /**
     * @return callable
     */
    protected function authorizationHandlerMiddleware()
    {
        return Middleware::mapRequest(function (RequestInterface $request) {
            $key = $this->getOAuthKey();

            if ($key && !$request->hasHeader(self::AUTH_HEADER)) {
                return $request->withHeader(self::AUTH_HEADER, "Bearer $key");
            }

            return $request;
        });
    }

    /**
     * @return string|null
     */
    protected function getOAuthKey(){
        /** @var TokenInterface $token */
        $token = $this->getTokenStorage()->getToken();

        return $token && $token->hasAttribute(WoodformAuthenticator::WOODFORM_API_HEADER) ?
            $token->getAttribute(WoodformAuthenticator::WOODFORM_API_HEADER) :
            null;
    }

    /**
     * @param $object
     * @param array|null $groups
     * @throws ValidationException
     */
    protected function validateObject($object, array $groups = null){
        $errors = $this->getValidator()->validate($object, null, $groups);

        if($errors->count()) {
            throw new ValidationException($errors);
        };
    }

    /**
     * @param User $user
     * @param WoodformUserDTO $woodformUser
     */
    protected function updateUserByWoodformUser(User $user, WoodformUserDTO $woodformUser) {
        $user
            ->setPid($woodformUser->getPid())
            ->setUuid($woodformUser->getUuid())
            ->setUid($woodformUser->getUid())
            ->setPhone($woodformUser->getPhoneNumber())
            ->setLastName($woodformUser->getSurname())
            ->setFirstName($woodformUser->getFirstName())
            ->setPosition($woodformUser->getIAmA())
            ->setEmail($woodformUser->getMail())
            ->setUsername($woodformUser->getName())
            ->setCompanyName($woodformUser->getCompanyName())
            ->setState($woodformUser->getState());

        if ($woodformUser->getCreated()) { $user->setCreatedAt($woodformUser->getCreated()); }
        if ($woodformUser->getChanged()) { $user->setUpdatedAt($woodformUser->getChanged()); }

        if ($woodformUser->getPictureUuid()) {
            if ($woodformUser->getPictureUuid() != $user->getPictureUuid()) {
                try {
                    $pictureFilename = $this->getTempDir() . '/' . uniqid('picture-');

                    $pictureContent = file_get_contents($woodformUser->getPictureUrl());
                    file_put_contents($pictureFilename, $pictureContent);

                    $picture = new UploadedFile($pictureFilename, 'picture', null, filesize($pictureFilename), UPLOAD_ERR_OK, true);

                    $user
                        ->setImageFile($picture)
                        ->setPictureUuid($woodformUser->getPictureUuid())
                    ;
                } catch (\Exception $exception) {
                    $this->getLogger()->addError($exception->getMessage(), $exception->getTrace());
                }
            }
        } else {
            $user->setImageFile(null);
        }

        $woodformUserRoles = $woodformUser->getRoles();
        $user->setRoles([]);

        if(in_array('authenticated user', $woodformUserRoles)){
            $user->addRole(User::ROLE_DEFAULT);
        }

        if(in_array('administrator', $woodformUserRoles)){
            $user->addRole(User::ROLE_ADMIN);
        }

        if(in_array('Developer', $woodformUserRoles)){
            $user->addRole(User::ROLE_ADMIN);
        }

        if(in_array('Manager', $woodformUserRoles)){
            $user->addRole(User::ROLE_MANAGER);
        }

        if(in_array('individual', $woodformUserRoles)){
            $user->addRole(User::ROLE_SUBSCRIBER_INDIVIDUAL);
        }

        if(in_array('enterprise', $woodformUserRoles)){
            $user->addRole(User::ROLE_SUBSCRIBER_ENTERPRISE);
        }

        if(in_array('tester', $woodformUserRoles)){
            $user->addRole(User::ROLE_TESTER);
        }
    }

    /**
     * @param string $key
     * @return WoodformUserDTO
     * @throws \Exception
     */
    protected function getWoodformUser($key = null)
    {
        $key = $key ?: $this->getOAuthKey();

        $headers = ['Content-Type' => 'application/json'];

        if ($key) {
            $headers[self::AUTH_HEADER] = "Bearer $key";
        }

        $requestOptions = [RequestOptions::HEADERS => $headers];

        $response = $this->http->post('system/connect', $requestOptions);

        $userResponse = $this->serializeResponse($response);

        if (isset($userResponse['error_description'])) {
            throw new \Exception($userResponse['error_description']);
        }

        if (isset($userResponse['user'])) {
            $user = (array) $userResponse['user'];

            try {
                /** @var Response $response */
                $response = $this->http->get("profile?parameters[uid]={$user['uid']}", $requestOptions);

                $userProfiles = $this->serializeResponse($response);
                $userProfileFields = (array) array_pop($userProfiles);

                $user = array_merge($user, $userProfileFields);
            } catch (\Exception $e) {
            }

            if (isset($user['picture'])) {
                $user['picture_url'] = $user['picture']['url'];
                $user['picture_uuid'] = $user['picture']['uuid'];
            }

            $woodformUser = $this->getSerializer()->fromArray($user, WoodformUserDTO::class);

            $this->validateObject($woodformUser);

            return $woodformUser;
        } else {
            throw new \Exception('Unknown error');
        }
    }

    /**
     * @param ResponseInterface $response
     * @return mixed|null
     */
    protected function serializeResponse(ResponseInterface $response) {
        $stream = $response->getBody();

        if ($stream instanceof JsonStream) {
            return $stream->jsonSerialize();
        }

        return null;
    }

    /**
     * @return string
     */
    protected function getTempDir()
    {
        return $this->container->get('kernel')->getRootDir() . '/../web/temp';
    }

    /**
     * @return Serializer
     */
    protected function getSerializer()
    {
        return $this->container->get('jms_serializer');
    }

    /**
     * @return \Symfony\Bridge\Monolog\Logger
     */
    protected function getLogger()
    {
        return $this->container->get('logger');
    }

    /**
     * @return ValidatorInterface
     */
    protected function getValidator()
    {
        return $this->container->get('validator');
    }

    /**
     * @return \FOS\UserBundle\Doctrine\UserManager
     */
    protected function getUserManager(){
        return $this->container->get('fos_user.user_manager');
    }

    /**
     * @return \Symfony\Component\Security\Core\Authentication\Token\Storage\TokenStorage
     */
    protected function getTokenStorage(){
        return $this->container->get('security.token_storage');
    }
}
