<?php

namespace AppBundle\Service;

use Doctrine\ORM\EntityManagerInterface;

/**
 * Class CurrencyUpdater
 *
 * @package AppBundle\Service
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class CurrencyUpdater
{
    /** @var EntityManagerInterface */
    private $em;

    private static $active = [
        'AUD',
        'NZD',
        'USD',
    ];

    /**
     * @param EntityManagerInterface $em
     */
    public function __construct(EntityManagerInterface $em)
    {
        $this->em = $em;
    }

    public function update()
    {
        $rates = AustraliaReserveBank::latestRates();

        $queries = [];
        $queries[] = $this->buildQuery('AUD', 1);
        foreach ($rates as $rate) {
            $queries[] = $this->buildQuery($rate['code'], $rate['rate']);
        }

        $this->executeQueries($queries);
    }

    protected function executeQueries($queries)
    {
        $this
            ->em
            ->getConnection()
            ->executeQuery(implode(';', $queries));
    }

    protected function buildQuery($code, $rate)
    {
        $date     = date('Y-m-d H:i:s');
        $isActive = (int) in_array($code, static::$active);

        if ('NZD' === $code) {
            $rate = (($rate / 100) * 10) + $rate;
        }

        return "INSERT INTO currencies(code, rate, is_active, updated_at) VALUE('{$code}', '{$rate}', '{$isActive}', '{$date}') ON DUPLICATE KEY UPDATE rate='{$rate}', updated_at='{$date}'";
    }
}
