<?php

namespace AppBundle\Service;

use Swift_Mailer;
use AppBundle\Util\Str;
use FOS\UserBundle\Model\UserInterface;
use Symfony\Bundle\FrameworkBundle\Templating\EngineInterface;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;


/**
 * Class Mailer
 */
class Mailer
{
    /**
     * @var Swift_Mailer
     */
    protected $mailer;

    /**
     * @var UrlGeneratorInterface
     */
    protected $router;

    /**
     * @var EngineInterface
     */
    protected $templating;

    /**
     * @var string
     */
    private $sculptFormURL;

    /**
     * @param Swift_Mailer    $mailer
     * @param EngineInterface $templating
     * @param string          $sculptFormURL
     */
    public function __construct($mailer, EngineInterface $templating, $sculptFormURL)
    {
        $this->mailer        = $mailer;
        $this->templating    = $templating;
        $this->sculptFormURL = $sculptFormURL;
    }

    /**
     * @param UserInterface $user
     */
    public function sendResettingEmailMessage(UserInterface $user)
    {
        $url = "{$this->sculptFormURL}/new-password?token={$user->getConfirmationToken()}";

        $rendered = $this->templating->render('@App/Email/reset.txt.twig', [
            'user'            => $user,
            'confirmationUrl' => $url,
        ]);
        
        $this->sendEmailMessage($rendered, (string) $user->getEmail());
    }

    /**
     * @param string       $renderedTemplate
     * @param array|string $toEmail
     */
    protected function sendEmailMessage($renderedTemplate, $toEmail)
    {
        // Render the email, use the first line as the subject, and the rest as the body
        $renderedLines = explode("\n", trim($renderedTemplate));
        $subject = array_shift($renderedLines);
        $body = implode("\n", $renderedLines);

        $message = (new \Swift_Message())
            ->setSubject($subject)
            ->setFrom('consult@sculptform.com.au')
            ->setTo($toEmail)
            ->setBody($body);

        $this->mailer->send($message);
    }
}
