<?php

namespace AppBundle\Service;

use AppBundle\Entity\User;
use AppBundle\Enum\PardotUserLastAction;
use Psr\Log\LoggerInterface;

/**
 * Class PardotService
 */
class PardotService
{
    const BASE_URL = 'https://go.pardot.com';

    private static $blackListEmailDomains = [
        'sibers.com',
        'sculptform.com.au'
    ];

    private static $blackListEmails = [
        'pavel.ryabov',
        'checkinsib2017'
    ];

    /**
     * @var LoggerInterface
     */
    protected $logger;

    /**
     * @param LoggerInterface $logger
     */
    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @param User   $user
     * @param string $actionPardotTrackActionEvent
     * @param array  $data
     *
     * @see PardotUserLastAction
     *
     * @return bool|string|null
     */
    public function sendUserLastAction(User $user, $action, array $data = null)
    {
        $email = $user->getEmail();

        if (!$this->isValidEmail($email)) {
            return null;
        }

        $URL    = self::BASE_URL.PardotUserLastAction::getForm($action);
        $params = $user->serializeForPardotService();

        if (PHP_SAPI === 'cli') {
            dump($params);
            dump($data);
            dump(array_merge($params, $data));
            dump("-------------------------");
        }
        $params = array_merge($params, $data);

        $this->logger->addInfo(json_encode([
            'url'    => $URL,
            'action' => $action,
            'data'   => $params,
        ]));


        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, $URL);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, http_build_query($params));
        curl_setopt($ch, CURLOPT_HEADER, false);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);

        $response = curl_exec($ch);
        curl_close($ch);

        return $response;
    }

    private function isValidEmail($email){
        $result = true;

        $domain = substr($email, strpos($email, '@') + 1);

        foreach (self::$blackListEmails as $pattern){
            if(strpos($email, $pattern) !== false){
                $result = false;
            }
        }

        if(in_array($domain, static::$blackListEmailDomains)){
            $result = false;
        }

        return $result;
    }
}
