<?php

namespace AppBundle\Service;


use AppBundle\Entity\User;
use AppBundle\Enum\UserProjectUserType;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\QueryBuilder;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class AccountStatsService implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManager
     */
    protected $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getTotal($repoName, $showTestData = false  ){
        $q  = $this->em;

        if(strpos($repoName, '.') !== false){ //This was written specifically for product type queries
            $repoName = explode('.', $repoName);
            $name = $repoName[0];
            $type = $repoName[1];
            $q = $q->getRepository('AppBundle:'.$name)
                   ->createQueryBuilder('p');
            $q = $q->select('count(p.id)')
                ->leftJoin('p.productType', 't')
                ->where('t.title = :type')
                ->setParameter('type', $type)
                ->join('p.userProjects', 'o1')
                ->join('o1.user', 'o');
            if(!$showTestData){
                $q->andWhere($q->expr()->in('o.id', $this->addIgnoreSpecificUsersCondition()));
            }
        }else{
            $q = $q->getRepository('AppBundle:'.$repoName)
              ->createQueryBuilder('p')
              ->select('count(p.id)');
            if(!$showTestData){
                switch ($repoName){
                    case 'Project' : {
                        $q = $q->join('p.userProjects', 'o1')
                               ->join('o1.user', 'o')
                               ->where($q->expr()->eq('o1.userType', UserProjectUserType::OWNER))
                               ->andWhere($q->expr()->in(
                                   'o.id',
                                   $this->addIgnoreSpecificUsersCondition()
                               )
                               );
                    };break;
                    case 'User' : {
                        $q = $q->where($q->expr()->in(
                                   'p.id',
                                   $this->addIgnoreSpecificUsersCondition()
                               )
                               );
                    };break;
                    case 'Quotation' : {
                        $q = $q->join('p.user', 'o')
                               ->where($q->expr()->in(
                                   'o.id',
                                   $this->addIgnoreSpecificUsersCondition()
                               )
                               );
                    };break;
                    case 'SampleRequest' : {
                        $q = $q->join('p.user', 'o')
                               ->where($q->expr()->in(
                                   'o.id',
                                $this->addIgnoreSpecificUsersCondition()
                               )
                               );
                    };break;
                    default: break;
                }
            }
        }
            $q = $q->getQuery();

        return $q->getSingleScalarResult();
    }


    public function getChartData($repoName, $showTestData = false){
        $q = $this->em;

        if(strpos($repoName, '.') !== false){ //This was written specifically for product type queries
            $repoName = explode('.', $repoName);
            $name = $repoName[0];
            $type = $repoName[1];
            $q = $q->getRepository('AppBundle:'.$name)
                ->createQueryBuilder('p');
            $q = $q->select('month(p.createdAt) as g_month, count(p.id) as counter')
                ->leftJoin('p.productType', 't')
                ->where('t.title = :type')
                ->setParameter('type', $type)
                ->join('p.userProjects', 'o1')
                ->join('o1.user', 'o');
                if(!$showTestData){
                    $q->andWhere($q->expr()->in('o.id', $this->addIgnoreSpecificUsersCondition()));
                }
        }else{
            $q = $q->getRepository('AppBundle:'.$repoName)
                ->createQueryBuilder('p')
                ->select('month(p.createdAt) as g_month, count(p.id) as counter');
            if(!$showTestData){
                switch ($repoName){
                    case 'Project' : {
                        $q = $q->join('p.userProjects', 'o1')
                            ->join('o1.user', 'o')
                            ->where($q->expr()->in(
                                'o.id',
                                $this->addIgnoreSpecificUsersCondition()
                            )
                            );
                    };break;
                    case 'User' : {
                        $q = $q->where($q->expr()->in(
                            'p.id',
                            $this->addIgnoreSpecificUsersCondition()
                        )
                        );
                    };break;
                    case 'Quotation' : {
                        $q = $q->join('p.user', 'o')
                            ->where($q->expr()->in(
                                'o.id',
                                $this->addIgnoreSpecificUsersCondition()
                            )
                            );
                    };break;
                    case 'SampleRequest' : {
                        $q = $q->join('p.user', 'o')
                            ->where($q->expr()->in(
                                'o.id',
                                $this->addIgnoreSpecificUsersCondition()
                            )
                            );
                    };break;
                    default: break;
                }
            }
        }
        $q = $q->groupBy('g_month')
               ->orderBy('g_month', 'asc')
               ->getQuery()
               ->getResult();

        $months  = array(
            'January',
            'February',
            'March',
            'April',
            'May',
            'June',
            'July',
            'August',
            'September',
            'October',
            'November',
            'December');
        $counter = array(0,0,0,0,0,0,0,0,0,0,0,0);

        foreach ($q as $value){
            $dateObj   = \DateTime::createFromFormat('!m', $value['g_month']);
            $key = array_search($dateObj->format('F'), $months);
            if($key !== false){
                $counter[$key] = $value['counter'];
            }
        }

        return array('months' => $months, 'counter' => $counter);
    }


    private function addIgnoreSpecificUsersCondition(){
        $qb = $this->em->createQueryBuilder();

        return $qb->select('u.id')
            ->from(User::class, 'u')
            ->where("u.roles not like '%admin%'")
            ->andWhere("u.roles not like '%tester%'")
            ->andWhere("u.email not like '%sibers%'")
            ->andWhere("u.email not like '%woodformarch%'")
            ->andWhere("u.email not like '%sculptform.com%'")
            ->getDQL();
    }
}