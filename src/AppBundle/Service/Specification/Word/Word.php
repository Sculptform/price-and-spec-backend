<?php

namespace AppBundle\Service\Specification\Word;

use AppBundle\Entity\Project;
use AppBundle\Util\SequenceFormatter;
use PhpOffice\PhpWord\PhpWord;
use PhpOffice\PhpWord\IOFactory;
use PhpOffice\PhpWord\Shared\Html;

class Word
{
    protected $headerPath;
    protected $footerPath;

    public function __construct($projectDir)
    {
        $this->headerPath = "{$projectDir}/web/images/doc_header.png";
        $this->footerPath = "{$projectDir}/web/images/doc_footer.png";
    }

    public function generate(Project $project)
    {
        /** @var PhpWord */
        $phpWord = new PhpWord();

        $section = $phpWord->addSection([
            'marginLeft' => 0,
            'marginRight' => 0,
            'marginTop' => 0,
            'marginBottom' => 0,
        ]);

        $section->addImage($this->headerPath, ['height' => 95]);
        $section->addTextBreak();
        $section->addText(
            'Sculptform Specification Details',
            ['bold' => true, 'name' => 'Roboto Condensed', 'color' => '525453', 'size' => 16],
            ['indent' => 2]
        );

        $section->addTextBreak();
        $mainTable = $section->addTable([
            'borderSize' => 1, 'borderColor' => '757575', 'width' => 3800,
            'unit' => 'pct', 'align' => 'center', 'cellMargin' => 80,
        ]);

        $mainTable
            ->addRow()
            ->addCell(null, ['bgColor' => '525453'])
            ->addText(
                'SPECIFICATION TABLE',
                ['name' => 'Roboto Condensed', 'color' => 'ffffff', 'size' => 10],
                ['align' => 'center']
            );

        $map = $this->buildMap($project);
        foreach ($map as $key => $value) {
            $row = $mainTable->addRow();
            $row
                ->addCell(1200, ['bgColor' => 'f8f9f8', 'valign' => 'center'])
                ->addText(htmlspecialchars("{$key}"), ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 11]);

            $row
                ->addCell(2000)
                ->addText(htmlspecialchars("{$value}"), ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 11]);
        }
        $section->addTextBreak();

        $innerTable = $section
            ->addTable([
                'borderSize'  => 0, 'width' => 3800,
                'borderColor' => 'ffffff', 'unit' => 'pct',
                'align'       => 'center',
                'cellMargin'  => 0,
            ])
            ->addRow()
            ->addCell()
            ->addTable([
                'width'       => 3800, 'borderSize' => 1,
                'borderColor' => '757575', 'unit' => 'pct',
                'align'       => 'left', 'cellMargin' => 80,
            ]);

        $innerTable->addRow();
        $firstCell = $innerTable->addCell(1400, ['bgColor' => '525453', 'valign' => 'center']);

        $textRun = $firstCell->addTextRun();
        $textRun->addText('Supply Cost per m',
            ['name' => 'Roboto Condensed', 'color' => 'ffffff', 'size' => 10],
            ['align' => 'center']
        );
        $textRun->addText('2', [
            'superScript' => true, 'name' => 'Roboto Condensed',
            'color'       => 'ffffff',
            'size'        => 10,
        ]);
        $innerTable
            ->addCell(1200, ['bgColor' => '525453', 'valign' => 'center'])
            ->addText('Acoustic Rating*',
                ['name' => 'Roboto Condensed', 'color' => 'ffffff', 'size' => 10],
                ['align' => 'center']);

        $innerTable
            ->addCell(1200, ['bgColor' => '525453', 'valign' => 'center'])
            ->addText('Total Weight',
                ['name' => 'Roboto Condensed', 'color' => 'ffffff', 'size' => 10],
                ['align' => 'center']);

        $innerTable->addRow();
        $secondCell = $innerTable->addCell(1200, ['bgColor' => 'ffffff', 'valign' => 'center']);

        $textRun = $secondCell->addTextRun();
        // $projectPrice = $project->getPrice();
        $projectPrice = (float) number_format(($project->getPrice() * $project->getCurrency()->getRate()), 2);
        $currencyCode = $project->getCurrency()->getCode();

        $textRun->addText(
            htmlspecialchars("$"."{$projectPrice}"),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 14],
            ['align' => 'center']);

        $textRun->addText(
            " {$currencyCode}",
            ['name'  => 'Roboto Condensed', 'color' => '757575', 'size'  => 10],
            ['align' => 'center']);

        $section->addTextBreak(1);
        $textRun = $secondCell->addTextRun();

        $pricingCategory = "{$project->getPricingCategory()} sqm";

        $textRun->addText(
            "based on [{$pricingCategory}]",
            ['name' => 'Roboto Condensed', 'color' => '757575', 'size'  => 9],
            ['align' => 'center']);

        $acousticRating = $project->getAcousticRating()
            ? $project->getAcousticRating()->getNrc() : 'N/A';

        $innerTable
            ->addCell(900, ['bgColor' => 'ffffff', 'valign' => 'center'])
            ->addText(
                htmlspecialchars("{$acousticRating}"),
                ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 14],
                ['align' => 'center']);

        $totalWeight = round($project->getTotalWeight(), 2);

        $innerTable
            ->addCell(900, ['bgColor' => 'ffffff', 'valign' => 'center'])
            ->addText(
                htmlspecialchars("{$totalWeight}kg"),
                ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 14],
                ['align' => 'center']);

        $section->addTextBreak(1);

        $section->addText(
            'IMPORTANT PRICING NOTE',
            ['bold' => true, 'name' => 'Roboto Condensed', 'color' => '525453', 'size' => 9],
            ['indent' => 2]);

        $disclaimer   = strip_tags($project->getProductType()->getDisclaimer());
        $priceInclude = strip_tags($project->getProductType()->getPriceIncludeNote());
        $priceExclude = strip_tags($project->getProductType()->getPriceExcludeNote());
        $priceNote    = strip_tags($project->getProductType()->getPriceNote());

        $section->addTextBreak(1);
        $section->addText(
            htmlspecialchars("{$disclaimer}"),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 9],
            ['indent' => 2]
        );
        $section->addTextBreak(1);
        $section->addText(
            htmlspecialchars("Price includes: {$priceInclude}"),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 9],
            ['indent' => 2]
        );
        $section->addTextBreak(1);
        $section->addText(
            htmlspecialchars("Price does not include: {$priceExclude}"),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 9],
            ['indent' => 2]
        );

        $section->addTextBreak(1);
        $section->addText(
            htmlspecialchars($priceNote),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 8],
            ['indent' => 2]
        );
        $section->addTextBreak(1);
        $section->addText(
            htmlspecialchars('*Based on professional opinion only. See disclaimer in Price & Spec for full details.'),
            ['name' => 'Roboto Condensed', 'color' => '525453', 'size' => 7.5],
            ['indent' => 2]
        );

        $section
            ->addFooter()
            ->addImage($this->footerPath, ['height' => 63, 'marginTop' => 10]);

        $objWriter = IOFactory::createWriter($phpWord, 'Word2007');
        $tempFile  = tempnam(sys_get_temp_dir(), 'PHPWord');

        $objWriter->save($tempFile);
        $content = file_get_contents($tempFile);
        @unlink($tempFile);

        return $content;
    }

    private function buildMap(Project $project)
    {
        $sequence = SequenceFormatter::create($project);
        $species = ($project->getProjectProducts()->count() > 0)
            ? $project->getProjectProducts()->first()->getFinishTitle()
            : 'N/A';

        $map = [
            'PROJECT NAME'     => $project->getTitle(),
            'PRODUCT'          => "Sculptform {$project->getProductType()->getFullTitle()}",
            'APPLICATION TYPE' => $project->getApplicationLabel(),
            'SEQUENCE'         => $sequence,
            'SPACING'          => $project->getSnap().'mm',
            'SPECIES'          => $species,
        ];

        if ($project->getProductType()->getTitle() != 'Facade Blades') {
            $coating = ($project->getMaterialCoatings()->count() > 0)
                ? $project->getMaterialCoatings()->first()->getTitle()
                : '';

            $map['COATING'] = $coating;
        }

        if ($project->getProductType()->getTitle() != 'Tongue & Groove Cladding') {
            $trackType = ($project->getRailing())
                ? $project->getRailing()->getTitle()
                : '';

            $map['MOUNTING TRACK TYPE'] = $trackType;
        }

        $map['MOUNTING TRACK COLOUR'] = ($project->getProductType()->getTitle() == 'Tongue & Groove Cladding')
            ? "Banjo Pine"
            : "Matt Black";

        return $map;
    }
}
