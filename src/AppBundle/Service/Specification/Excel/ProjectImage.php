<?php

namespace AppBundle\Service\Specification\Excel;

use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class ProjectImage
 * @package AppBundle\Service\Specification\Excel
 */
class ProjectImage
{
    /**
     * @var array
     */
    private static $sizes = [
        'win' => [576, 258],
        'mac' => [636, 258],
        'nix' => [719, 245],
    ];

    /**
     * @param Worksheet $sheet
     * @param string    $platform
     * @param string    $imageWebPath
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function create(Worksheet $sheet, $platform, $imageWebPath)
    {
        if (!$imageWebPath) {
            return;
        }
        $projectImage = new Drawing();
        $projectImage->setPath($imageWebPath);
        $projectImage->setResizeProportional(false);

        list($width, $height) = static::$sizes[$platform];

        $projectImage->setWidthAndHeight($width, $height);
        $projectImage->setCoordinates('A13');
        $projectImage->setOffsetY(1);
        $projectImage->setWorksheet($sheet);

        $sheet->getStyle('A13:I25')
            ->applyFromArray([
                'borders' => [
                    'outline' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => '757575']
                    ]
                ]
            ]);
    }
}
