<?php

namespace AppBundle\Service\Specification\Excel;

use PhpOffice\PhpSpreadsheet\Cell\Hyperlink;
use PhpOffice\PhpSpreadsheet\Worksheet\Drawing;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class Header
 * @package AppBundle\Service\Specification\Excel
 */
final class Header
{
    /**
     * @var array
     */
    private static $sizes = [
        'win' => 1280,
        'mac' => 1413,
        'nix' => 1598,
    ];

    /**
     * @param Worksheet $sheet
     * @param string    $platform
     * @param string    $headerPath
     *
     * @throws \PhpOffice\PhpSpreadsheet\Exception
     */
    public static function create(Worksheet $sheet, $platform, $headerPath)
    {
        $headerLink = new Hyperlink('http://sculptform.com.au', 'Go to the SculptForm');

        $header = new Drawing();
        $header->setName('Logo');
        $header->setDescription('Logo');
        $header->setPath($headerPath);
        $header->setResizeProportional(false);

        $header->setWidth(static::$sizes[$platform]);
        $header->setHeight(210);
        $header->setHyperlink($headerLink);
        $header->setWorksheet($sheet);
    }
}
