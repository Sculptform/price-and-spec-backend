<?php

namespace AppBundle\Service\Specification\Excel;

use AppBundle\Entity\Product;
use AppBundle\Entity\Scene;
use AppBundle\Entity\Project;
use AppBundle\Service\SpeciesExtractor;
use AppBundle\Util\SequenceFormatter;
use PhpOffice\PhpSpreadsheet\Style\Fill;
use PhpOffice\PhpSpreadsheet\Style\Color;
use PhpOffice\PhpSpreadsheet\Style\Border;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

/**
 * Class SpecTable
 * @package AppBundle\Service\Specification\Excel
 */
class SpecTable
{
    /**
     * @var array
     */
    protected static $cells = [
        'name'        => null,
        'product'     => null,
        'app_type'    => null,
        'sequence'    => null,
        'profile'     => null,
        'spacing'     => null,
        'species'     => null,
        'coating'     => null,
        'track_type'  => null,
        'track_color' => null,
        'backing'     => null,
    ];

    /**
     * @var int
     */
    protected static $sequenceContentLines = 0;

    public static function create(Worksheet $sheet, Project $project, SpeciesExtractor $extractor, $platform)
    {
        $pos = static::setCells($project, $extractor, $platform);
        static::clearCells();

        static::createHeader($sheet);
        static::styleContent($sheet);
        static::fillContent($sheet);

        return $pos;
    }

    private static function fillContent(Worksheet $sheet)
    {
        static::contentNames($sheet);
        foreach (static::$cells as $cell) {
            $sheet->getCell($cell['value'])->setValue($cell['content']);
        }
    }

    private static function contentNames(Worksheet $sheet)
    {
        foreach (static::$cells as $cell) {
            $sheet->getCell($cell['key'])->setValue(strtoupper($cell['title']));
        }
    }

    private static function styleContent(Worksheet $sheet)
    {
        foreach (static::$cells as $cell) {
            if (!$cell['keyMerge'] or !$cell['valueMerge']) {
                continue;
            }
            $sheet->mergeCells($cell['keyMerge']);
            $sheet->mergeCells($cell['valueMerge']);
        }

        $K = explode(":", reset(static::$cells)['keyMerge'])[0];
        $O = explode(":", end(static::$cells)['keyMerge'])[1];
        $T = explode(":", end(static::$cells)['valueMerge'])[1];

        $sheet
            ->getStyle("{$K}:{$T}")
            ->applyFromArray([
                'font' => [
                    'size'  => 14,
                    'color' => ['argb' => '696969']
                ],
                'alignment' => ['vertical' => 'top', 'wrapText' => true],
                'borders' => [
                    'allBorders' => [
                        'borderStyle' => Border::BORDER_THIN,
                        'color' => ['argb' => 'd4d4d4']
                    ]
                ],
            ]);

        $sheet
            ->getStyle("{$K}:{$O}")
            ->applyFromArray([
                'alignment' => ['vertical' => 'center'],
                'fill' => [
                    'fillType'   => Fill::FILL_SOLID,
                    'startColor' => ['argb' => 'f8f9f8']
                ]
            ]);

        $sheet
            ->getStyle(static::$cells['sequence']['valueMerge'])
            ->applyFromArray([
                'font' => [
                    'size' => 13,
                    'alignment' => ['vertical' => 'center', 'wrapText' => true],
                ],
            ]);

        //$sheet->getStyle('K14')->getBorders()->getTop()->setBorderStyle(Border::BORDER_NONE);
        //$sheet->getStyle('O14')->getBorders()->getTop()->setBorderStyle(Border::BORDER_NONE);
    }

    private static function createHeader(Worksheet $sheet)
    {
        $sheet->mergeCells('K13:T13');
        $sheet->getCell('K13')->setValue('Specification Table');
        $sheet->getStyle('K13')->applyFromArray(static::specTableHeaderStyles());
    }

    private static function specTableHeaderStyles()
    {
        return [
            'font' => [
                'bold'  => false,
                'name'  => 'Lucida Sans',
                'size'  => 16,
                'color' => ['argb' => Color::COLOR_WHITE]
            ],
            'alignment' => ['horizontal' => 'center'],
            'fill' => [
                'fillType'   => Fill::FILL_SOLID,
                'startColor' => ['argb' => '525453']
            ],
        ];
    }

    private static function adjustSequenceContent($text)
    {
        $text = preg_replace('/\s+/', ' ', $text);
        $words = explode(',', $text);

        $tmp = '';
        $sentences = [];

        foreach ($words as $item) {
            if (strlen("{$tmp}{$item},") >= 45) {
                $sentences[] = $tmp;
                $tmp = "";
            }

            $tmp = "{$tmp}{$item},";
        }
        $sentences[] = rtrim($tmp, ",");

        return implode(PHP_EOL, $sentences);
    }
    
    private static function setCells(Project $project, SpeciesExtractor $extractor, $platform)
    {
        static::$cells['name'] = [
            'key'        => 'K14',
            'value'      => 'O14',
            'title'      => 'project name',
            'content'    => $project->getTitle(),
            'keyMerge'   => 'K14:N14',
            'valueMerge' => 'O14:T14',
        ];
        //**********************************************
        static::$cells['product'] = [
            'key'        => 'K15',
            'value'      => 'O15',
            'title'      => 'product',
            'content'    => $project->getProductType()->getFullTitle(),
            'keyMerge'   => 'K15:N15',
            'valueMerge' => 'O15:T15',
        ];
        //**********************************************
        $pos = 15;
        $pos++;
        static::$cells['app_type'] = [
            'key'        => "K{$pos}",
            'value'      => "O{$pos}",
            'title'      => 'application type',
            'content'    => $project->getApplicationLabel(),
            'keyMerge'   => "K{$pos}:N{$pos}",
            'valueMerge' => "O{$pos}:T{$pos}",
        ];
        //**********************************************
        $sequence = SequenceFormatter::create($project);
        $sequence = static::adjustSequenceContent($sequence);
        $sequenceContentLines = count(explode(PHP_EOL, $sequence));

        $pos++;
        $offset = ('nix' !== $platform)
            ? ceil($pos + $sequenceContentLines * 1.15)
            : ($pos + $sequenceContentLines + 1)
        ;

        static::$cells['sequence'] = [
            'key'        => "K{$pos}",
            'value'      => "O{$pos}",
            'title'      => 'sequence',
            'content'    => $sequence,
            'keyMerge'   => "K{$pos}:N{$offset}",
            'valueMerge' => "O{$pos}:T{$offset}",
        ];
        $pos = $offset;
        //**********************************************
        if ($project->getProductType()->getId() === 2) {
            $ecp = $project->getExpressionCladdingProducts()->first();
            if ($ecp) {
                /** @var Product $product */
                $product = ($ecp->getBase())
                    ? $ecp->getBase()
                    : $ecp->getStacker()
                ;

                $stackerParentShape = $product->getParentShape(0);

                $pos++;
                static::$cells['profile'] = [
                    'key'        => "K{$pos}",
                    'value'      => "O{$pos}",
                    'title'      => 'profile',
                    'content'    => $stackerParentShape->getTitle(),
                    'keyMerge'   => "K{$pos}:N{$pos}",
                    'valueMerge' => "O{$pos}:T{$pos}",
                ];
            }
        }
        //**********************************************
        if ($project->getProductType()->getTitle() != 'Tongue & Groove Cladding') {
            $pos++;
            static::$cells['spacing'] = [
                'key'        => "K{$pos}",
                'value'      => "O{$pos}",
                'title'      => 'spacing',
                'content'    => "{$project->getSnap()}mm",
                'keyMerge'   => "K{$pos}:N{$pos}",
                'valueMerge' => "O{$pos}:T{$pos}",
            ];
        }
        //**********************************************
        $pos++;
        static::$cells['species'] = [
            'key'        => "K{$pos}",
            'value'      => "O{$pos}",
            'title'      => 'species',
            'content'    => $extractor->extract($project),
            'keyMerge'   => "K{$pos}:N{$pos}",
            'valueMerge' => "O{$pos}:T{$pos}",
        ];
        //**********************************************
        if ($project->getProductType()->getTitle() != 'Facade Blades') {
            $pos++;

            $coating = $project->getMaterialCoatings()->first()
                ? $project->getMaterialCoatings()->first()->getTitle()
                : '';

            static::$cells['coating'] = [
                'key'        => "K{$pos}",
                'value'      => "O{$pos}",
                'title'      => 'coating',
                'content'    => $coating,
                'keyMerge'   => "K{$pos}:N{$pos}",
                'valueMerge' => "O{$pos}:T{$pos}"
            ];
        }
        //**********************************************
        if ($project->getProductType()->getTitle() != 'Tongue & Groove Cladding') {
            $pos++;
            static::$cells['track_type'] = [
                'key'        => "K{$pos}",
                'value'      => "O{$pos}",
                'title'      => 'mounting track type',
                'content'    => $project->getRailing()->getTitle(),
                'keyMerge'   => "K{$pos}:N{$pos}",
                'valueMerge' => "O{$pos}:T{$pos}",
            ];
        }
        //**********************************************
        $pos++;
        $color = $project->getProductType()->getTitle() == 'Tongue & Groove Cladding'
            ? 'Banjo Pine'
            : 'Matt Black';

        static::$cells['track_color'] = [
            'key'        => "K{$pos}",
            'value'      => "O{$pos}",
            'title'      => 'mounting track color',
            'content'    => $color,
            'keyMerge'   => "K{$pos}:N{$pos}",
            'valueMerge' => "O{$pos}:T{$pos}"
        ];
        //**********************************************
        if ($project->getProductType()->getId() == 1 and $project->getApplicationType() == 1) {
            $pos++;

            $acoustic = $project->getAcousticBacking() !== null
                ? 'Yes'
                : 'No';

            static::$cells['backing'] = [
                'key'        => "K{$pos}",
                'value'      => "O{$pos}",
                'title'      => 'acoustic backing',
                'content'    => $acoustic,
                'keyMerge'   => "K{$pos}:N{$pos}",
                'valueMerge' => "O{$pos}:T{$pos}"
            ];
        }

        return $pos;
    }

    private static function clearCells()
    {
        foreach (static::$cells as $key => $cell) {
            if (!$cell) {
                unset(static::$cells[$key]);
            }
        }
    }
}
