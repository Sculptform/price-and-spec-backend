<?php

namespace AppBundle\Service\Specification\Excel;

use AppBundle\Entity\Project;
use AppBundle\Service\SpeciesExtractor;
use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

class Excel
{
    /** @var SpeciesExtractor */
    private $speciesExtractor;

    protected $headerPath;

    /**
     * @param SpeciesExtractor $speciesExtractor
     * @param string           $projectDir
     */
    public function __construct(SpeciesExtractor $speciesExtractor, $projectDir)
    {
        $this->speciesExtractor = $speciesExtractor;
        $this->headerPath = "{$projectDir}/web/images/doc_header.png";
    }

    /**
     * @param Project $project
     * @param string  $platform
     *
     * @return string
     *
     * @throws \Exception
     */
    public function generate(Project $project, string $platform)
    {
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setTitle("Specification Table");

        Header::create($sheet, $platform, $this->headerPath);
        ProjectImage::create($sheet, $platform, $project->getImageWebPath());

        $pos = SpecTable::create($sheet, $project, $this->speciesExtractor, $platform);
        $pos = TotalTable::create($sheet, $project, $pos);
        $pos = Note::create($sheet, $project, $pos);
        $pos = Steps::create($sheet, $pos);
        Footer::create($sheet, $pos);

        $sheet->getProtection()->setSheet(true);

        $writer = new Xlsx($spreadsheet);
        $writer->setOffice2003Compatibility(true);

        $tempFile  = tempnam(sys_get_temp_dir(), 'PHPExcel');

        $writer->save($tempFile);
        $content = file_get_contents($tempFile);
        @unlink($tempFile);

        return $content;
    }
}
