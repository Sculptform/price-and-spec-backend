<?php

namespace AppBundle\Service\Specification\Excel;

use AppBundle\Entity\Project;
use PhpOffice\PhpSpreadsheet\RichText\RichText;
use PhpOffice\PhpSpreadsheet\Worksheet\Worksheet;

class Note
{
    private static $pos;

    public static function create(Worksheet $sheet, Project $project, $pos)
    {
        static::$pos = ($pos + 2);
        static::title($sheet);

        static::$pos += 3;
        static::disclaimer($sheet, strip_tags($project->getProductType()->getDisclaimer()));

        static::$pos += 3;
        static::includes($sheet, strip_tags($project->getProductType()->getPriceIncludeNote()));

        static::$pos += 4;
        static::doesNotInclude($sheet, strip_tags($project->getProductType()->getPriceExcludeNote()));

        static::$pos += 3;
        static::notes($sheet, strip_tags($project->getProductType()->getPriceNote()));

        static::$pos += 3;
        static::based($sheet);

        return static::$pos;
    }

    private static function based(Worksheet $sheet)
    {
        $pos = static::$pos;
        $text = "*Based on professional opinion only. See disclaimer in Price & Spec for full details.";

        $sheet->mergeCells("K{$pos}:T{$pos}");
        $sheet->getCell("K{$pos}")->setValue($text);

        $styles = static::getStyles();
        $styles['font']['italic'] = true;

        $sheet->getStyle("K{$pos}")->applyFromArray($styles);
    }

    private static function notes(Worksheet $sheet, $text)
    {
        $pos = static::$pos;
        $merge = $pos + 3;

        $sheet->mergeCells("K{$pos}:T{$merge}");

        $bolded = explode('(', $text)[0];
        $text   = explode('(', $text)[1];

        $richText = static::createRichText($bolded, $text);

        $sheet->getCell("K{$pos}")->setValue($richText);

        $styles = static::getStyles();
//        $styles['font']['size'] = 10;
//
        $sheet->getStyle("K{$pos}")->applyFromArray($styles);
    }

    private static function doesNotInclude(Worksheet $sheet, $text)
    {
        $pos = static::$pos;
        $merge = $pos + 1;

        $sheet->mergeCells("K{$pos}:T{$merge}");

        $richText = static::createRichText("Price does not include: ", $text);

        $sheet->getCell("K{$pos}")->setValue($richText);
        $sheet->getStyle("K{$pos}")->applyFromArray(static::getStyles());
    }

    private static function includes(Worksheet $sheet, $text)
    {
        $pos = static::$pos;
        $merge = $pos + 2;

        $sheet->mergeCells("K{$pos}:T{$merge}");

        $richText = static::createRichText("Price includes: ", $text);

        $sheet->getCell("K{$pos}")->setValue($richText);
        $sheet->getStyle("K{$pos}")->applyFromArray(static::getStyles());
    }

    private static function getStyles()
    {
        return [
            'font' => [
                'color' => ['argb' => '696969'],
            ],
            'alignment' => [
                'horizontal' => 'left',
                'vertical'   => 'top',
                'wrapText'   => true
            ],
        ];
    }

    private static function createRichText($heading, $text)
    {
        $richText = new RichText();
        $richText->createText('');

        $price = $richText->createTextRun($heading);
        $price->getFont()->setBold(true);
        $price->getFont()->setSize(12);
        $price
            ->getFont()
            ->getColor()->setARGB('696969');

        $based = $richText->createTextRun($text);
        $based
            ->getFont()
            ->getColor()->setARGB('696969');

        return $richText;
    }

    private static function disclaimer(Worksheet $sheet, $text)
    {
        $pos = static::$pos;
        $merge = $pos + 1;

        $sheet->mergeCells("K{$pos}:T{$merge}");
        $sheet->getCell("K{$pos}")->setValue($text);

        $sheet->getStyle("K{$pos}")->applyFromArray([
            'font' => [
                'color' => ['argb' => '696969'],
            ],
            'alignment' => [
                'horizontal' => 'left',
                'vertical'   => 'center',
                'wrapText'   => true
            ],
        ]);
    }

    private static function title(Worksheet $sheet)
    {
        $pos = static::$pos;
        $merge = $pos + 1;
        $sheet->mergeCells("K{$pos}:T{$merge}");

        $sheet->getCell("K{$pos}")->setValue('IMPORTANT PRICING NOTE');
        $sheet
            ->getStyle("K{$pos}")
            ->applyFromArray([
                'font' => [
                    'color' => ['argb' => '696969'],
                    'size'  => 20,
                    'bold'  => true,
                ],
                'alignment' => [
                    'horizontal' => 'left',
                    'vertical'   => 'center',
                    'wrapText'   => true
                ],
            ]);
    }
}
