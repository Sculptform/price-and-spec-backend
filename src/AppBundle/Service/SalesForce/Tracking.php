<?php

namespace AppBundle\Service\SalesForce;

use GuzzleHttp\Client;
use Psr\Http\Message\ResponseInterface;

/**
 * Class Tracking
 *
 * @package AppBundle\Service\SalesForce
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class Tracking
{
    /**
     * @var array
     */
    private $settings;

    /**
     * @var string|null
     */
    private $accessToken = null;

    /**
     * @param array $settings
     */
    public function __construct(array $settings)
    {
        $this->settings = $settings;
    }

    /**
     * @param Endpoint $endpoint
     *
     * @return string Response variable should contain "Success" string.
     */
    public function track(Endpoint $endpoint)
    {
        if (null === $this->accessToken) {
            $this->authenticate();
        }

        $options = [
            'json'    => $endpoint->getOptions(),
            'headers' => [
                'Authorization' => 'Bearer ' . $this->accessToken,
            ],
        ];

        $response = $this->doRequest($endpoint->getUri(), $options);

        return json_decode($response->getBody()->getContents(), true);
    }

    /**
     * Login to SalesForce and get access token.
     */
    protected function authenticate()
    {
        $uri = '/services/oauth2/token';
        $options = [
            'form_params' => $this->settings['credentials'],
        ];

        $response = $this->doRequest($uri, $options);
        $response = json_decode($response->getBody()->getContents(), true);

        $this->accessToken = $response['access_token'];
    }

    /**
     * @param string $uri
     * @param array  $options
     *
     * @return ResponseInterface
     */
    protected function doRequest($uri, array $options)
    {
        $client = new Client([
            'base_uri' => $this->settings['base_uri']
        ]);

        return $client->post($uri, $options);
    }
}
