<?php

namespace AppBundle\Service\SalesForce;

/**
 * Class Endpoint
 *
 * @package AppBundle\Service\SalesForce
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class Endpoint
{
    protected $uri;
    protected $method   = 'post';
    protected $options  = [];

    public function setUri($uri)
    {
        $this->uri = $uri;

        return $this;
    }

    public function getUri()
    {
        return $this->uri;
    }

    public function setMethod($method)
    {
        $this->method = $method;

        return $this;
    }

    public function getMethod()
    {
        return $this->method;
    }

    public function setOptions(array $options)
    {
        $this->options = $options;

        return $this;
    }

    public function getOptions()
    {
        return $this->options;
    }
}
