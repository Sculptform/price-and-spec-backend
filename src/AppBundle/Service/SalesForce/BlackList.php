<?php

namespace AppBundle\Service\SalesForce;

/**
 * Class BlackList
 *
 * @package AppBundle\Service\SalesForce
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
final class BlackList
{
    private static $emails = [
        'pavel.ryabov',
        'checkinsib2017',
    ];

    private static $domains = [
        'sibers.com',
        'sculptform.com',
        'sculptform.com.au',
    ];

    public static function passes($email)
    {
//        foreach (static::$emails as $pattern) {
//            if (strpos($email, $pattern) !== false) {
//                return false;
//            }
//        }

        $domain = substr($email, strpos($email, '@') + 1);
        if (in_array($domain, static::$domains)) {
            return false;
        }

        return true;
    }
}
