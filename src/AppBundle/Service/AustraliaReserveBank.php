<?php

namespace AppBundle\Service;

/**
 * Class AustraliaReserveBank
 * RSS Feed of Reserve bank of Australia.
 *
 * @package AppBundle\Service\ExchangeRates
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class AustraliaReserveBank
{
    const RSS_URL = 'https://www.rba.gov.au/rss/rss-cb-exchange-rates.xml';
    const RSS_NS  = 'http://www.cbwiki.net/wiki/index.php/Specification_1.2/';

    public static function latestRates()
    {
        $document = new \DOMDocument('1.0', 'utf-8');
        $document->load(self::RSS_URL);

        $items = $document->getElementsByTagName('item');
        $rates = [];

        foreach ($items as $item) {
            /** @var \DOMElement $item */
            $code = $item->getElementsByTagNameNS(self::RSS_NS, 'targetCurrency')->item(0)->nodeValue;
            $rate = $item->getElementsByTagNameNS(self::RSS_NS, 'value')->item(0)->nodeValue;

            $rates[] = [
                'code' => $code,
                'rate' => (float) $rate,
            ];
        }

        return $rates;
    }
}
