<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Service\AWS;


use AppBundle\Event\AWS\RDS\DBCluster\DBClusterCreationInitializedEvent;
use AppBundle\Event\AWS\RDS\DBCluster\DBClusterDeletionInitializedEvent;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceCreationInitialized;
use AppBundle\Event\AWS\RDS\DBInstance\DBInstanceDeletionInitializedEvent;
use Aws\AwsClientInterface;
use Aws\Exception\AwsException;
use Aws\Rds\RdsClient;
use Psr\Log\LoggerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class RDSManagerService
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Service\AWS
 */
class RDSManagerService
{
    /**
     * @var RdsClient $client
     */
    protected $client;

    /**
     * @var LoggerInterface $logger
     */
    protected $logger;

    /**
     * @var EventDispatcherInterface $dispatcher
     */
    protected $dispatcher;

    /**
     * @var string $stagingInstance
     */
    protected $stagingInstance;

    /**
     * @var string $stagingCluster
     */
    protected $stagingCluster;

    /**
     * @var string $sourceCluster
     */
    protected $sourceCluster;

    /**
     * RDSManagerService constructor.
     *
     * @param AwsClientInterface       $client
     * @param LoggerInterface          $logger
     * @param EventDispatcherInterface $dispatcher
     * @param string                   $sourceCluster
     * @param string                   $stagingCluster
     * @param string                   $stagingInstance
     */
    public function __construct(
        AwsClientInterface $client, LoggerInterface $logger,
        EventDispatcherInterface $dispatcher,
        string $sourceCluster, string $stagingCluster, string $stagingInstance
    ) {
        $this->client          = $client;
        $this->logger          = $logger;
        $this->dispatcher      = $dispatcher;
        $this->sourceCluster   = $sourceCluster;
        $this->stagingCluster  = $stagingCluster;
        $this->stagingInstance = $stagingInstance;
    }

    /**
     * @return \Aws\Result
     */
    public function createDbCluster()
    {
        $result = false;

        $this->logger->info(
            sprintf(
                'Creating Db cluster %s -> %s... ', $this->sourceCluster,
                $this->stagingCluster
            )
        );

        $clusterParams = [
            'SourceDBClusterIdentifier' => $this->sourceCluster,
            'UseLatestRestorableTime'   => true,
            'RestoreType'               => 'copy-on-write',
            'DBClusterIdentifier'       => $this->stagingCluster,
            'AvailabilityZones'         => ['ap-southeast-2c'],
            'VpcSecurityGroupIds'       => ['sg-0e2d3e673df96887d'],
        ];

        try {
            $result = $this->client->restoreDBClusterToPointInTime(
                $clusterParams
            );

            $event = new DBClusterCreationInitializedEvent($this);

            $this->dispatcher->dispatch($event::NAME, $event);
        } catch (AwsException $ex) {
            $this->logger->error(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return $result;
    }

    /**
     * @return \Aws\Result
     */
    public function removeDbCluster()
    {
        $result = false;

        $this->logger->info(
            sprintf('Removing Db cluster %s... ', $this->stagingCluster)
        );

        try {
            $result = $this->client->deleteDBCluster(
                [
                    'DBClusterIdentifier' => $this->stagingCluster,
                    'SkipFinalSnapshot'   => true,
                ]
            );

            $event = new DBClusterDeletionInitializedEvent($this);

            $this->dispatcher->dispatch($event::NAME, $event);
        } catch (AwsException $ex) {
            $this->logger->error(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return $result;
    }

    /**
     * @return \Aws\Result
     */
    public function createDbInstance()
    {
        $result = false;
        $this->logger->info(
            sprintf('Creating Db instance %s... ', $this->stagingInstance)
        );
        $instanceParams = [
            'DBInstanceClass'      => 'db.t3.small',
            'DBClusterIdentifier'  => $this->stagingCluster,
            'DBInstanceIdentifier' => $this->stagingInstance,
            'Engine'               => 'aurora-mysql',
            'DBParameterGroupName' => 'sculptform-aurora',
            'AvailabilityZone'     => 'ap-southeast-2c',
        ];

        try {
            $result = $this->client->createDBInstance($instanceParams);

            $event = new DBInstanceCreationInitialized($this);

            $this->dispatcher->dispatch($event::NAME, $event);
        } catch (AwsException $ex) {
            $this->logger->error(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return $result;
    }

    /**
     * @return \Aws\Result
     */
    public function removeDbInstance()
    {
        $result = false;

        $this->logger->info(
            sprintf('Removing Db instance %s... ', $this->stagingInstance)
        );

        try {
            $result = $this->client->deleteDBInstance(
                [
                    'DBInstanceIdentifier' => $this->stagingInstance,
                ]
            );

            $event = new DBInstanceDeletionInitializedEvent($this);

            $this->dispatcher->dispatch($event::NAME, $event);

        } catch (AwsException $ex) {
            $this->logger->error(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return $result;
    }

    /**
     * @return \Aws\Result
     */
    private function checkDbcluster()
    {
        return $this->client->describeDBClusters(
            [
                'DBClusterIdentifier' => $this->stagingCluster,
            ]
        );
    }

    /**
     * @return \Aws\Result
     */
    private function checkDbInstance()
    {
        return $this->client->describeDBInstances(
            [
                'DBInstanceIdentifier' => $this->stagingInstance,
            ]
        );
    }

    /**
     * @return bool
     */
    public function clusterExists()
    {
        try {
            $clusterInfo = $this->checkDbcluster();
        } catch (AwsException $ex) {
            $this->logger->info(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
            if ($ex->getStatusCode() == Response::HTTP_NOT_FOUND) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function clusterAvailable()
    {
        try {
            $clusterInfo = $this->checkDbcluster();
            if ($clusterInfo['DBClusters'][0]['Status']
                == 'available'
            ) {
                return true;
            }
        } catch (AwsException $ex) {
            $this->logger->info(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return false;
    }

    /**
     * @return bool
     */
    public function instanceExists()
    {
        try {
            $instanceInfo = $this->checkDbInstance();
        } catch (AwsException $ex) {
            $this->logger->info(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
            if ($ex->getStatusCode() == Response::HTTP_NOT_FOUND) {
                return false;
            }
        }

        return true;
    }

    /**
     * @return bool
     */
    public function instanceAvailable()
    {
        try {
            $instanceInfo = $this->checkDbInstance();

            if ($instanceInfo['DBInstances'][0]['DBInstanceStatus']
                == 'available'
            ) {
                return true;
            }
        } catch (AwsException $ex) {
            $this->logger->info(
                $ex->getAwsErrorMessage(), $ex->getTrace()
            );
        }

        return false;
    }

    /**
     * @return string
     */
    public function getStagingClusterName()
    {
        return $this->stagingCluster;
    }

    /**
     * @return string
     */
    public function getStagingInstanceName()
    {
        return $this->stagingInstance;
    }

    /**
     * @return string
     */
    public function getSourceClusterName()
    {
        return $this->sourceCluster;
    }

    /**
     * @return LoggerInterface
     */
    public function getLogger()
    {
        return $this->logger;
    }

    /**
     * @return EventDispatcherInterface
     */
    public function getDispatcher()
    {
        return $this->dispatcher;
    }
}
