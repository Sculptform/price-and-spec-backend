<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 17.08.17
 * Time: 11:32
 */

namespace AppBundle\Service;

use AppBundle\Entity\FintraxInfo;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class ProductService implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param string $csvFilename
     */
    public function updateCCLFromCSV($csvFilename)
    {
        gc_enable();

        $em = $this->getEm();
        $file = new \SplFileObject($csvFilename);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        /** @var ProductType $productType */
        $productType = $em->find(ProductType::class, 1);

        /** @var ProductType MaterialType */
        $alMaterialType = $em->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']);

        /** @var ProductType MaterialType */
        $timberMaterialType = $em->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);

        /** @var MaterialFinishGroup[] $timberCCLMaterialGroups */
        $timberCCLMaterialGroups = $em->getRepository(MaterialFinishGroup::class)->findBy([
            'materialType' => $timberMaterialType,
            'productType' => $productType
        ]);

        /** @var MaterialFinishGroup[] $timberCCLMaterialGroups */
        $alCCLMaterialGroups = $em->getRepository(MaterialFinishGroup::class)->findBy([
            'materialType' => $alMaterialType,
            'productType' => $productType
        ]);

        /** @var MaterialFinish[] $aluminiumMaterialFinishes */
        $aluminiumMaterialFinishes = $em->getRepository(MaterialFinish::class)->findBy([
            'group' => $alCCLMaterialGroups
        ]);

        foreach ($reader as $row) {
            $description = trim($row['Item Description']);
            $itemNumber = trim($row['Item Number']);
            $heading = trim(strtolower($row['Sculptform Heading']));
            $price = (float)$row['Sculptform price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice = (float)$row['Cutek'];
            $weight = (float)$row['Weight'];
            $unit = trim($row['Unit of Measure']);

            if (!$description) {
                return;
            }

            /** @var Product[] $products */
            $products = $em->getRepository(Product::class)->findBy(['itemNumber' => $itemNumber]);

            if ($products) {
                $this->logError('Products found! Updating..', [$itemNumber, count($products)]);

                foreach ($products as $product) {
                    $product
                        ->setItemNumber($itemNumber)
                        ->setItemDescription($description)
                        ->setPrice($price)
                        ->setNaturalAccentPrice($enviroproPrice)
                        ->setEnviroproPrice($enviroproPrice)
                        ->setCutekPrice($cutekPrice)
                        ->setUnit($unit)
                        ->setWeight($weight)
                    ;
                }
            } else {
                $this->logError('Products not found! Adding new..', [$itemNumber]);

                preg_match_all('|\d+|', $description, $sizes);
                list($wide, $deep) = $sizes[0];

                if (false !== strpos($description, 'Aluminium')) {

                    foreach (explode(',', $heading) as $materialFinishGroupSelector) {
                        $materialFinishGroupSelector = trim($materialFinishGroupSelector);

                        foreach ($aluminiumMaterialFinishes as $materialFinish) {
                            if (false !== strpos(strtolower($materialFinish->getGroup()->getTitle()), strtolower($materialFinishGroupSelector))) {
                                $shapeTitle = 'Block';

                                if (false !== strpos($description, 'Flute')) {
                                    $shapeTitle = 'Flute';
                                }

                                /** @var Material $material */
                                $material = $em->getRepository(Material::class)->getByProductTypeAndShape(
                                    $productType,
                                    $shapeTitle,
                                    $wide,
                                    $deep,
                                    $materialFinish->getMaterialType()
                                );

                                if ($material) {
                                    $item = new Product();

                                    $item
                                        ->setMaterial($material)
                                        ->setMaterialFinish($materialFinish)
                                        ->setItemNumber($itemNumber)
                                        ->setItemDescription($description)
                                        ->setPrice($price)
                                        ->setNaturalAccentPrice($enviroproPrice)
                                        ->setEnviroproPrice($enviroproPrice)
                                        ->setCutekPrice($cutekPrice)
                                        ->setUnit($unit)
                                        ->setWeight($weight)
                                    ;

                                    $em->persist($item);
                                } else {
                                    $this->logError('Material not found!', [
                                        $productType->getTitle(), $shapeTitle, $wide, $deep
                                    ]);
                                }
                            }
                        }
                    }
                } else {
                    $finishTitle = explode(' - ', $description)[0];

                    $finishTitle = str_replace(['American White Oak'], ['White Oak'], $finishTitle);

                    $materialFinish = $em->getRepository(MaterialFinish::class)->findOneBy([
                        'materialType' => $timberMaterialType,
                        'group' => $timberCCLMaterialGroups,
                        'title' => $finishTitle
                    ]);

                    if ($materialFinish) {
                        /** @var Material $material */
                        $material = $em->getRepository(Material::class)->getByProductTypeAndShape(
                            $productType, $heading, $wide, $deep
                        );

                        if ($material) {
                            $item = new Product();
                            $item
                                ->setMaterial($material)
                                ->setMaterialFinish($materialFinish)
                                ->setItemNumber($itemNumber)
                                ->setItemDescription($description)
                                ->setPrice($price)
                                ->setNaturalAccentPrice($enviroproPrice)
                                ->setEnviroproPrice($enviroproPrice)
                                ->setCutekPrice($cutekPrice)
                                ->setUnit($unit)
                                ->setWeight($weight)
                            ;

                            $em->persist($item);
                        } else {
                            $this->logError('Material not found!', [
                                $productType->getTitle(), $heading, $wide, $deep
                            ]);
                        }
                    } else {
                        $this->logError("Finish not found!", [$finishTitle]);
                    }
                }
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();
    }

    /**
     * @param string $csvFilename
     */
    public function updateABFromCSV($csvFilename)
    {
        gc_enable();

        $em = $this->getEm();
        $file = new \SplFileObject($csvFilename);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        /** @var ProductType $productType */
        $productType = $em->find(ProductType::class, 1);

        /** @var ProductType MaterialType */
        $materialType = $em->getRepository(MaterialType::class)->findOneBy(['title' => 'Acoustic Blades']);

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $em->getRepository(MaterialFinish::class)->findBy(['materialType' => $materialType]);

        foreach ($reader as $row) {
            $description = trim($row['Description']);
            $itemNumber = trim($row['Number']);
            $price = (float)$row['Sculptform Price'];
            $weight = (float)$row['Weight'];
            $unit = trim($row['Unit']);

            if (!$description) {
                return;
            }

            /** @var Product[] $products */
            $products = $em->getRepository(Product::class)->findBy(['itemNumber' => $itemNumber]);

            if ($products) {
                $this->logError('Products found! Updating..', [$itemNumber, count($products)]);

                foreach ($products as $product) {
                    $product
                        ->setItemNumber($itemNumber)
                        ->setItemDescription($description)
                        ->setPrice($price)
                        ->setWeight($weight)
                        ->setUnit($unit)
                    ;
                }
            } else {
                $this->logError('Products not found! Adding new..', [$itemNumber]);

                $normalizedDescription = str_replace(
                    ['Acoustic Linear Blade', 'Acoustic Sculpture Blade'],
                    ['straight -', 'wave -'],
                    $description
                );

                list($shapeTitle, $sizeTitle, $finishTitle) = array_map('trim', explode(' - ', $normalizedDescription));

                preg_match_all('|\d+|', $sizeTitle, $sizes);
                list(, $deep, $wide) = $sizes[0];

                preg_match_all('|\d+|', $finishTitle, $colors);
                list($color) = $colors[0];

                foreach ($materialFinishes as $materialFinish) {
                    if ($this->normalizeName($materialFinish->getTitle()) === $this->normalizeName($color)) {
                        $material = $em->getRepository(Material::class)->getByProductTypeAndShape(
                            $productType, $shapeTitle, $wide, $deep
                        );

                        if ($material) {
                            $item = new Product();

                            $item
                                ->setMaterial($material)
                                ->setMaterialFinish($materialFinish)
                                ->setItemNumber($itemNumber)
                                ->setItemDescription($description)
                                ->setPrice($price)
                                ->setWeight($weight)
                                ->setUnit($unit)
                            ;

                            $em->persist($item);
                        } else {
                            $this->logError('Material not found!', [
                                $productType->getTitle(), $shapeTitle, $wide, $deep
                            ]);
                        }

                        break;
                    }
                }
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();
    }

    /**
     * @param string $csvFilename
     */
    public function updateECLFromCSV($csvFilename)
    {
        gc_enable();

        $em = $this->getEm();
        $file = new \SplFileObject($csvFilename);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        /** @var ProductType $productType */
        $productType = $em->find(ProductType::class, 2);

        /** @var MaterialFinishGroup[] $materialFinishGroups */
        $materialFinishGroups = $em->getRepository(MaterialFinishGroup::class)->findBy(['productType' => $productType]);

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $em->getRepository(MaterialFinish::class)->findBy(['group' => $materialFinishGroups]);

        foreach ($reader as $row) {
            $description = trim($row['Item Description']);
            $itemNumber = trim($row['Item: Item Number']);
            $price = (float)$row['Sculptform Price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice = (float)$row['Cutek'];
            $weight = (float)$row['Weights'];
            $unit = trim($row['Default Unit of Measure']);

            if (!$description) {
                return;
            }

            /** @var Product[] $products */
            $products = $em->getRepository(Product::class)->findBy(['itemNumber' => $itemNumber]);

            if ($products) {
                $this->logError('Products found! Updating..', [$itemNumber, count($products)]);

                foreach ($products as $product) {
                    $product
                        ->setItemNumber($itemNumber)
                        ->setItemDescription($description)
                        ->setPrice($price)
                        ->setCutekPrice($cutekPrice)
                        ->setNaturalAccentPrice($enviroproPrice)
                        ->setEnviroproPrice($enviroproPrice)
                        ->setUnit($unit)
                        ->setWeight($weight)
                    ;
                }
            } else {
                $normalizedDescription = str_replace(
                    ['Profile: ', 'Expression Cladding ', 'Yakisugi'],
                    ['', '', 'Charred'],
                    $description
                );

                list($finishTitle, $sizeTitle, $profileTitle) = array_map('trim', explode(' - ', $normalizedDescription));

                preg_match_all('|\d+|', $sizeTitle, $sizes);
                list($wide, $deep) = $sizes[0];

                $this->logError('Products not found! Adding new..', [
                    $itemNumber, $finishTitle, $profileTitle, $wide, $deep
                ]);

                foreach ($materialFinishes as $materialFinish) {
                    if ($this->normalizeName($materialFinish->getTitle()) === $this->normalizeName($finishTitle)) {
                        $material = $em->getRepository(Material::class)->getByProductTypeAndShape(
                            $productType, $profileTitle, $wide, $deep
                        );

                        if ($material) {
                            $item = new Product();

                            $item
                                ->setMaterial($material)
                                ->setMaterialFinish($materialFinish)
                                ->setItemNumber($itemNumber)
                                ->setItemDescription($description)
                                ->setPrice($price)
                                ->setCutekPrice($cutekPrice)
                                ->setNaturalAccentPrice($enviroproPrice)
                                ->setEnviroproPrice($enviroproPrice)
                                ->setUnit($unit)
                                ->setWeight($weight)
                            ;

                            $em->persist($item);
                        } else {
                            $this->logError('Material not found!', [
                                $productType->getTitle(), $profileTitle, $wide, $deep
                            ]);
                        }

                        break;
                    }
                }
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();
    }

    /**
     * @param string $csvFilename
     */
    public function updateFTXFromCSV($csvFilename)
    {
        gc_enable();

        $em = $this->getEm();
        $file = new \SplFileObject($csvFilename);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        /** @var MaterialShape $baseNosing */
        $baseNosing = $em->getRepository(MaterialShape::class)->findOneBy(['name' => 'nosing']);

        foreach ($reader as $row) {
            $description = trim($row['Item Description(Long)']);
            $itemNumber = trim($row['Item Number']);
            $price = (float)$row['Sculptform Price'];
            $weight = (float)$row['Weight'];

            if (!$description) {
                return;
            }

            /** @var FintraxInfo[] $existedFintraxInfos */
            $existedFintraxInfos = $em->getRepository(FintraxInfo::class)->findBy(['itemNumber' => $itemNumber]);

            if ($existedFintraxInfos) {
                $this->logError('Products found! Updating..', [$itemNumber, count($existedFintraxInfos)]);

                foreach ($existedFintraxInfos as $existedFintraxInfo) {
                    $existedFintraxInfo
                        ->setItemNumber($itemNumber)
                        ->setDescription($description)
                        ->setPrice($price)
                        ->setWeight($weight)
                    ;
                }
            } else {
                $this->logError('Products not found! Adding new..', [$itemNumber]);

                $normalizedDescription = str_replace(
                    [
                        'Aluminium - Fintrax ',
                        'Profile: ',
                        'Anodised 20um (External) - Colour -',
                        'Powdercoated Premium Colour -'
                    ],
                    [
                        '',
                        '',
                        'Anodising',
                        'Powder Coating'
                    ],
                    $description
                );

                list($sizeTitle, $profileTitle, $finishGroupTitle) = array_map(
                    'trim',
                    explode(' - ', $normalizedDescription)
                );

                preg_match_all('|\d+|', $sizeTitle, $sizes);

                list($wide, $deep) = $sizes[0];

                /** @var MaterialFinishGroup $finishGroup */
                $finishGroup = $em->getRepository(MaterialFinishGroup::class)->findOneBy([
                    'productType' => 3,
                    'title' => $finishGroupTitle
                ]);

                /** @var MaterialShape $nosing */
                $nosing = $em->getRepository(MaterialShape::class)->findOneBy([
                    'parent' => $baseNosing,
                    'title' => $profileTitle
                ]);

                if (!$finishGroup) {
                    $this->logError('Finish group not found!', [$finishGroupTitle]);
                }

                if (!$nosing) {
                    $this->logError('Nosing shape not found!', [$nosing]);
                }

                if ($finishGroup && $nosing) {
                    $fintraxInfo = new FintraxInfo();

                    $fintraxInfo
                        ->setItemNumber($itemNumber)
                        ->setPrice($price)
                        ->setWidth($wide)
                        ->setDepth($deep)
                        ->setDescription($description)
                        ->setMaterialShape($nosing)
                        ->setMaterialFinishGroup($finishGroup)
                        ->setWeight($weight)
                    ;

                    $em->persist($fintraxInfo);
                }
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();
    }

    /**
     * @param string $message
     * @param array $context
     */
    private function logError($message, array $context = [])
    {
        $this->container->get('logger')->error($message, $context);
    }

    /**
     * @return \Doctrine\ORM\EntityManager
     */
    private function getEm()
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);
        return $em;
    }

    /**
     * @param string $name
     * @return string
     */
    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }
}