<?php

namespace AppBundle\Twig;

class DateExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFilters()
    {
        return array(
            new \Twig_SimpleFilter('timeAgo', [$this, 'timeAgo']),
        );
    }

    public function timeAgo($datetime)
    {
        if($datetime){
            $time = time() - strtotime($datetime instanceof \DateTime ? $datetime->format('Y-m-d H:i:s') : $datetime);

            $units = array (
                31536000 => 'year',
                2592000 => 'month',
                604800 => 'week',
                86400 => 'day',
                3600 => 'hour',
                60 => 'minute',
                1 => 'second'
            );

            foreach ($units as $unit => $val) {
                if ($time < $unit) continue;
                $numberOfUnits = floor($time / $unit);
                return ($val == 'second')? 'a few seconds' :
                    (($numberOfUnits>1) ? $numberOfUnits : 'a')
                    .' '.$val.(($numberOfUnits>1) ? 's' : '');
            }
        }

        return '';

    }


    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'date_extension';
    }
}