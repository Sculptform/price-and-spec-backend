<?php

namespace AppBundle\Twig;

class ReflectionExtension extends \Twig_Extension
{
    /**
     * {@inheritdoc}
     */
    public function getFunctions()
    {
        return array(
            new \Twig_SimpleFunction('staticCall', [$this, 'callStaticMethod']),
            new \Twig_SimpleFunction('staticGet', [$this, 'getStaticProperty']),
        );
    }

    public function callStaticMethod($class, $method, array $args = [])
    {
        $ref = new \reflectionClass($class);

        // Check that method is static AND public
        if ($ref->hasMethod($method) && $ref->getMethod($method)->isStatic() && $ref->getMethod($method)->isPublic()) {
            return call_user_func_array($class.'::'.$method, $args);
        }

        throw new \RuntimeException(sprintf('Invalid static method call for class %s and method %s', $class, $method));
    }

    public function getStaticProperty($class, $property)
    {
        $ref = new \reflectionClass($class);

        // Check that property is static AND public
        if ($ref->hasProperty($property) && $ref->getProperty($property)->isStatic() && $ref->getProperty($property)->isPublic()) {
            return $ref->getProperty($property)->getValue();
        }

        throw new \RuntimeException(sprintf('Invalid static property get for class %s and property %s', $class, $property));
    }

    /**
     * {@inheritdoc}
     */
    public function getName()
    {
        return 'reflection';
    }
}