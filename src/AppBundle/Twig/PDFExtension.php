<?php

namespace AppBundle\Twig;

use Twig\TwigFilter;
use AppBundle\Entity\Project;
use AppBundle\Util\SequenceFormatter;
use Twig\Extension\AbstractExtension;
use AppBundle\Service\SpeciesExtractor;

/**
 * Class PDFExtension
 *
 * @package AppBundle\Twig
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class PDFExtension extends AbstractExtension
{
    /**
     * @var SpeciesExtractor
     */
    private $speciesExtractor;

    /**
     * @param SpeciesExtractor $speciesExtractor
     */
    public function __construct(SpeciesExtractor $speciesExtractor)
    {
        $this->speciesExtractor = $speciesExtractor;
    }

    /**
     * @return array|TwigFilter[]
     */
    public function getFilters()
    {
        return [
            new TwigFilter('sequence',        [$this, 'sequence']),
            new TwigFilter('create_sequence', [$this, 'createSequence']),
            new TwigFilter('species',         [$this, 'species']),
            new TwigFilter('sequence_length',  [$this, 'sequenceLength']),
        ];
    }

    public function createSequence(Project $project)
    {
        return SequenceFormatter::create($project);
    }

    public function sequenceLength()
    {
        return SequenceFormatter::sequenceLength();
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public function sequence($str)
    {
        return SequenceFormatter::format($str);
    }

    /**
     * @param Project $project
     *
     * @return string
     *
     * @throws \Doctrine\DBAL\DBALException
     */
    public function species(Project $project)
    {
        return $this->speciesExtractor->extract($project);
    }
}
