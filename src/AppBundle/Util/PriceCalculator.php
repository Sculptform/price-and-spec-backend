<?php

namespace AppBundle\Util;

use AppBundle\Entity\Product;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\AcousticBacking;

/**
 * Class PriceCalculator
 *
 * @package AppBundle\Util
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
final class PriceCalculator
{
    use PriceCalculatorTrait;

    private $price = 0;
    private $ratio = 0;
    private $acousticBakingPrice = 0;
    private $defaultAcousticBacking;

    private static $pricingCategory = [
        '0-100'   => 0.18,
        '100-300' => 0,
        '300+'    => -0.17,
    ];

    private static $acousticBaking = [
        'No Backing'               => 0,
        'Group 3 Rated - Standard' => 35,
        'Group 1 Rated'            => 42,
    ];

    public function __construct(AcousticBacking $defaultAcousticBacking = null)
    {
        $this->defaultAcousticBacking = $defaultAcousticBacking;
    }

    public function calculate(Project $project)
    {
        $this->price = 0;
        $this->ratio = $project->getSequenceRatio();

        $this->handleRailing($project);
        $this->handleAcousticBacking($project);
        $this->handleProducts($project);
        $this->handleCategory($project);

        return round($this->price, 2);
    }

    protected function handleRailing(Project $project)
    {
        if ($project->getRailing()) {
            $this->price += $project->getRailing()->getPrice();
        }
    }

    protected function handleAcousticBacking(Project $project)
    {
        if ('Click-on Battens' === $project->getProductType()->getTitle()) {
            if ($project->getAcousticBacking()) {
                $this->price += static::$acousticBaking[$project->getAcousticBacking()->getTitle()];
            }
        }

//        $this->acousticBakingPrice = $this->defaultAcousticBacking->getPrice();
//
//        if ($project->getAcousticBacking() and !$project->getAcousticBacking()->isDefault()) {
//            $this->acousticBakingPrice = $project->getAcousticBacking()->getPrice();
//        }
//
//        $this->price += $this->acousticBakingPrice;
    }

    protected function handleProducts(Project $project)
    {
        list(
            $productMethod,
            $projectProductMethod
        ) = $this->productMethodNames($project);

        $coatingMethod = $this->coatingMethodName($project);

        $projectProducts = $project->{"$projectProductMethod"}();

        foreach ($projectProducts as $projectProduct /** @var ProjectProduct $projectProduct */) {
            /** @var Product $product */
            $product = $projectProduct->{"$productMethod"}();

            if ( ! $product) {
                $product = $projectProduct->getStacker();
            }

            $this->price += $product->getPrice() * $this->ratio;

            if ($coatingMethod) {
                $coatingPrice = $product->{$coatingMethod}();
                if ($coatingPrice) {
                    $this->price += $coatingPrice * $this->ratio;
                }
            }
        }
    }

    protected function handleCategory(Project $project)
    {
        $category = $project->getPricingCategory();
        if ( ! $category) {
            $category = '100-300';
        }

        $price = $this->price + static::$pricingCategory[$category] * $this->price;

        $this->price = ($price * 100) / 100;

//        if ($project->getPricingCategory() === '0-100') {
//            $this->price += (($this->price * 18) / 100);
//        }
//
//        if ($project->getPricingCategory() === '300+') {
//            $this->price -= (($this->price * 17) / 100);
//        }
    }
}
