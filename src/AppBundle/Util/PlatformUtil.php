<?php


namespace AppBundle\Util;

/**
 * Class PlatformUtil
 */
final class PlatformUtil
{
    /**
     * @return string
     */
    public static function getName()
    {
        $uAgent = $_SERVER['HTTP_USER_AGENT'];
        $platform = 'Unknown';

        //Get the platform
        if (preg_match('/linux/i', $uAgent)) {
            $platform = 'nix';
        } elseif (preg_match('/macintosh|mac os x/i', $uAgent)) {
            $platform = 'mac';
        } elseif (preg_match('/windows|win32/i', $uAgent)) {
            $platform = 'win';
        }

        return $platform;
    }
}
