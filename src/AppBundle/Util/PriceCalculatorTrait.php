<?php

namespace AppBundle\Util;

use AppBundle\Entity\Project;
use AppBundle\Entity\MaterialCoating;
use Symfony\Component\Serializer\NameConverter\CamelCaseToSnakeCaseNameConverter;

/**
 * Trait PriceCalculatorTrait
 *
 * @package AppBundle\Util
 */
trait PriceCalculatorTrait
{
    /**
     * @param Project $project
     *
     * @return array
     */
    public function productMethodNames(Project $project)
    {
        $productMethod        = "getProduct";
        $projectProductMethod = "getProjectProducts";

        if ('Tongue & Groove Cladding' === $project->getProductType()->getTitle()) {
            $productMethod        = "getBase";
            $projectProductMethod = "getExpressionCladdingProducts";
        }

        return [
            $productMethod,
            $projectProductMethod,
        ];
    }

    /**
     * @param Project $project
     *
     * @return string|string[]|null
     */
    public function coatingMethodName(Project $project)
    {
        if ($project->getMaterialCoatings()->count() === 0) {
            return null;
        }

        /** @var MaterialCoating $materialCoating */
        $materialCoating = $project->getMaterialCoatings()->first();
        $methodName = "get_{$materialCoating->getProductPriceFieldName()}";

        $nameConverter = new CamelCaseToSnakeCaseNameConverter();

        return $nameConverter->denormalize($methodName);

//        $methodName = '';
//        foreach ($project->getMaterialCoatings()->toArray() as $materialCoating) {
//            /** @var MaterialCoating $materialCoating */
//            $methodName = "get_{$materialCoating->getProductPriceFieldName()}";
//        }
//
//        $nameConverter = new CamelCaseToSnakeCaseNameConverter();
//
//        return $nameConverter->denormalize($methodName);
    }
}
