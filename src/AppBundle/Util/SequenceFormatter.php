<?php

namespace AppBundle\Util;

use AppBundle\Entity\{Product,
    Scene,
    Project,
    MaterialType,
    MaterialShape,
    ProjectProduct,
    MaterialShapeSize,
    MaterialFinishGroup};

/**
 * Class SequenceFormatter
 *
 * @package AppBundle\Util
 * @final
 */
final class SequenceFormatter
{
    protected static $sequenceLength;

    public static function getSequenceAsArray(Project $project)
    {
        if ($project->getScenes()->count() === 0) {
            return '';
        }

        /** @var Scene $scene */
        $scene = $project->getScenes()->first();
        $sceneData = $scene->getScene();

        $sequence = $sceneData['spec']['sequence'];

        $exploded = explode("(", $sequence);
        $exploded = array_filter($exploded);

        foreach ($exploded as $key => &$value) {
            $value = trim("({$value}");
        }

        return $exploded;
    }

    /**
     * @param string $str
     *
     * @return string
     */
    public static function format($str)
    {
        $str = trim($str);
        if (!$str) {
            return '';
        }

        $str = str_replace('  ', ' ', $str);

        $data = explode(',', $str);
        $data = array_chunk($data, 2);

        $response = [];

        foreach ($data as $value) {
            $value = array_map('trim', $value);
            $response[] = implode(", ", $value);
        }

        return implode(", ", $response);
    }

    /**
     * @param Project $project
     *
     * @return string
     */
    public static function create(Project $project)
    {
        $size = "({$project->getSnap()}mm space),";
        $orderedIdentifiers = static::orderedIdentifiers($project);

        $productMethod = "getProduct";
        $projectProductMethod = "getProjectProducts";

        if ('Tongue & Groove Cladding' === $project->getProductType()->getTitle()) {
            $size = "";
            $productMethod = "getBase";
            $projectProductMethod = "getExpressionCladdingProducts";
        }

        $projectProducts = $project->{"$projectProductMethod"}();

        $sequence = [];
        foreach ($orderedIdentifiers as $item) {
            $projectProduct = static::findProjectProduct($projectProducts, $productMethod, $item);

            if (!$projectProduct) {
                continue;
            }

            /** @var Product $product */
            $product = $projectProduct->{"$productMethod"}();
            if (!$product) {
                $product = $projectProduct->getStacker();
            }

            /** @var MaterialShapeSize $shapeSize */
            $shapeSize = $product->getMaterial()->getMaterialShapeSize();
            $depth = $shapeSize->getDepth();
            $width = $shapeSize->getWidth();

            /** @var MaterialType $materialType */
            $materialType = $product->getMaterial()->getMaterialType();
            $type  = $materialType->getTitle();

            /** @var MaterialShape $materialShape */
            $materialShape = $shapeSize->getMaterialShape();
            $shape = $materialShape->getTitle();

            $finishTitle = $product->getMaterialFinish()->getTitle();

            $line = "{$size} {$width}x{$depth} {$type} - {$shape}, {$finishTitle}";

            /** @var MaterialFinishGroup $group */
            $group = $product->getMaterialFinish()->getGroup();
            
            if ($group->getTitle() !== 'Species') {
                $line .= " {$group->getTitle()}";
            }
//            if ($group->getTitle() === 'Wood Finish Aluminium' or $group->getTitle() === 'Real Timber Veneer') {
//                $line .= " {$group->getTitle()}";
//            }

            $sequence[] = $line;
        }

        $sequenceText           = implode(",\n", $sequence);
        static::$sequenceLength = strlen($sequenceText);

        return $sequenceText;
    }

    public static function sequenceLength()
    {
        return static::$sequenceLength;
    }

    private static function findProjectProduct($projectProducts, $productMethod, $data)
    {
        foreach ($projectProducts as $projectProduct /** @var ProjectProduct $projectProduct */) {
            /** @var Product $product */
            $product = $projectProduct->{"$productMethod"}();

            if (!$product) {
                $product = $projectProduct->getStacker();
            }

            if (
                $product->getMaterialId()       == $data['materialId'] and
                $product->getId()               == $data['productId']  and
                $product->getMaterialFinishId() == $data['finishId']
            ) {
                return $projectProduct;
            }
        }

        return null;
    }

    private static function orderedIdentifiers(Project $project)
    {
        if ($project->getScenes()->count() === 0) {
            return [];
        }

        /** @var Scene $scene */
        $scene = $project->getScenes()->first();
        $sceneData = $scene->getScene();

        if (null === $uuids = static::getUuids($sceneData)) {
            return [];
        }

        // Tongue & Groove Cladding|Click On Battens etc...
        $productType = $project->getProductType()->getTitle();

        // Create unique identifiers by material, product and finish ids
        $response = [];
        foreach ($uuids as $uuid) {
            foreach ($sceneData['scene']['object']['children'] as $o) {

                $key = 'uuid';
                if ('Tongue & Groove Cladding' === $productType) {
                    if ($o['woodformShapeType'] === 'element') {
                        $key = 'woodformBase';
                    }
                }

                if (isset($o[$key]) and $uuid == $o[$key]) {
                    $response[] = [
                        'materialId' => (int) $o['materialModel'],
                        'productId'  => (int) $o['productModel'],
                        'finishId'   => (int) $o['materialFinishModel'],
                    ];
                }
            }
        }

        return $response;
    }

    private static function getUuids($sceneData)
    {
        if (!isset($sceneData['spec']['orderUuids']) or count($sceneData['spec']['orderUuids']) === 0) {
            return null;
        }

        $uuids = [];
        foreach ($sceneData['spec']['orderUuids'] as $uuid) {
            $uuids[] = $uuid;
        }

        return $uuids;
    }
}
