<?php
/*
 * This file is part of the AppBundle\Util package.
 *
 * (c) Farukh Narzullaev <faruh.narzullaev@sibers.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace AppBundle\Util;

/**
 * Class Str
 */
final class Str
{
    public static function endsWith($haystack, $needle)
    {
        $length = strlen( $needle );

        if ( !$length ) {
            return true;
        }

        return substr( $haystack, -$length ) === $needle;
    }
}
