<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 11.08.17
 * Time: 12:50
 */

namespace AppBundle\Util;


use Symfony\Component\HttpFoundation\File\File;

class FileUtil
{
    /**
     * @param File $file
     * @return string
     */
    public static function generateFilename(File $file)
    {
        return sprintf(
            '%s.%s',
            md5($file->getFilename() . microtime() . random_bytes(10)),
            $file->guessExtension()?:'svg'
        );
    }
}
