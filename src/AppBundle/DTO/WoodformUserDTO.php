<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 28.09.2016
 * Time: 10:14
 */

namespace AppBundle\DTO;

use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Class WoodformUserDTO
 */
class WoodformUserDTO
{
    /**
     * User id
     * @var int
     *
     * @JMS\Type("integer")
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    protected $uid;

    /**
     * User uuid
     * @var string
     *
     * @JMS\Type("string")
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    protected $uuid;

    /**
     * User Profile id
     * @var int
     *
     * @JMS\Type("integer")
     */
    protected $pid;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    protected $name;

    /**
     * @var string
     *
     * @JMS\Type("string")
     *
     * @Assert\NotNull
     * @Assert\NotBlank
     */
    protected $mail;

    /**
     * @var int
     *
     * @JMS\Type("integer")
     */
    protected $status;

    /**
     * @var array
     *
     * @JMS\Type("array")
     */
    protected $roles = [];

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $firstName;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $surname;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $companyName;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $iAmA;

    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime<'U'>")
     */
    protected $created;

    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime<'U'>")
     */
    protected $changed;

    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime<'U'>")
     */
    protected $access;

    /**
     * @var \DateTime
     *
     * @JMS\Type("DateTime<'U'>")
     */
    protected $login;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $phoneNumber;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $pictureUrl;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $pictureUuid;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $timezone;

    /**
     * @var string
     *
     * @JMS\Type("string")
     */
    protected $state;

    /**
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return int
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * @return int
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getMail()
    {
        return $this->mail;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @return array
     */
    public function getRoles()
    {
        return $this->roles;
    }

    /**
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * @return string
     */
    public function getSurname()
    {
        return $this->surname;
    }

    /**
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * @return string
     */
    public function getIAmA()
    {
        return $this->iAmA;
    }

    /**
     * @return \DateTime
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * @return \DateTime
     */
    public function getChanged()
    {
        return $this->changed;
    }

    /**
     * @return \DateTime
     */
    public function getAccess()
    {
        return $this->access;
    }

    /**
     * @return \DateTime
     */
    public function getLogin()
    {
        return $this->login;
    }

    /**
     * @return string
     */
    public function getPhoneNumber()
    {
        return $this->phoneNumber;
    }

    /**
     * @return string
     */
    public function getPictureUrl()
    {
        return $this->pictureUrl;
    }

    /**
     * @return string
     */
    public function getPictureUuid()
    {
        return $this->pictureUuid;
    }

    /**
     * @return string
     */
    public function getTimezone()
    {
        return $this->timezone;
    }
}
