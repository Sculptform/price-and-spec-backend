<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 01.09.2016
 * Time: 11:23
 */

namespace AppBundle\DTO;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;

class UserStatsDTO
{
    /**
     * Fake id
     * @JMS\Type("integer")
     * @var integer
     */
    protected $id;

    /**
     * Total Quotations Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $totalQuotations;

    /**
     * Created Designs Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $createdDesigns;

    /**
     * Shared Designs Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $sharedDesigns;

    /**
     * Followers Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $followers;

    /**
     * Following Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $following;

    /**
     * Awards
     * @JMS\Type("ArrayCollection<AppBundle\Entity\Award>")
     * @var ArrayCollection
     */
    protected $awards;

    /**
     * Days since last design
     * @JMS\Type("integer")
     * @var integer
     */
    protected $daysSinceLastDesign;

    /**
     * Average projects per day
     * @JMS\Type("string")
     * @var float
     */
    protected $averageProjectPerDay = 0;

    /**
     * @param integer $id
     * @return $this
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @param integer $totalQuotations
     * @return $this
     */
    public function setTotalQuotations($totalQuotations)
    {
        $this->totalQuotations = $totalQuotations;

        return $this;
    }

    /**
     * @param integer $createdDesigns
     * @return $this
     */
    public function setCreatedDesigns($createdDesigns)
    {
        $this->createdDesigns = $createdDesigns;

        return $this;
    }

    /**
     * @param integer $sharedDesigns
     * @return $this
     */
    public function setSharedDesigns($sharedDesigns)
    {
        $this->sharedDesigns = $sharedDesigns;

        return $this;
    }

    /**
     * @return int
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /**
     * @param int $followers
     */
    public function setFollowers($followers)
    {
        $this->followers = $followers;
    }

    /**
     * @return int
     */
    public function getFollowing()
    {
        return $this->following;
    }

    /**
     * @param int $following
     */
    public function setFollowing($following)
    {
        $this->following = $following;
    }

    /**
     * @return int
     */
    public function getDaysSinceLastDesign()
    {
        return $this->daysSinceLastDesign;
    }

    /**
     * @param int $daysSinceLastDesign
     */
    public function setDaysSinceLastDesign($daysSinceLastDesign)
    {
        $this->daysSinceLastDesign = $daysSinceLastDesign;
    }

    /**
     * @return float
     */
    public function getAverageProjectPerDay()
    {
        return $this->averageProjectPerDay;
    }

    /**
     * @param float $averageProjectPerDay
     */
    public function setAverageProjectPerDay($averageProjectPerDay)
    {
        $this->averageProjectPerDay = $averageProjectPerDay;
    }

    /**
     * Set awards
     *
     * @param Collection $awards
     *
     * @return UserStatsDTO
     */
    public function setAwards(Collection $awards)
    {
        $this->awards = $awards;

        return $this;
    }

    /**
     * Add award
     *
     * @param \AppBundle\Entity\Award $award
     *
     * @return UserStatsDTO
     */
    public function addAward(\AppBundle\Entity\Award $award)
    {
        $this->awards[] = $award;

        return $this;
    }

    /**
     * Remove award
     *
     * @param \AppBundle\Entity\Award $award
     */
    public function removeAward(\AppBundle\Entity\Award $award)
    {
        $this->awards->removeElement($award);
    }

    /**
     * Get awards
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getAwards()
    {
        return $this->awards;
    }

}
