<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 20.06.17
 * Time: 16:10
 */

namespace AppBundle\DTO;

use JMS\Serializer\Annotation as JMS;

class DWGExportedProduct
{
    /**
     * @var string
     * @JMS\Type("string")
     */
    protected $dwgWebPath;

    /**
     * @var integer
     * @JMS\Type("integer")
     */
    protected $x = 0;

    /**
     * @var integer
     * @JMS\Type("integer")
     */
    protected $y = 0;

    /**
     * @var integer
     * @JMS\Type("integer")
     */
    protected $angle = 0;

    /**
     * @var boolean
     * @JMS\Type("boolean")
     */
    protected $flipx = false;

    /**
     * @var boolean
     * @JMS\Type("boolean")
     */
    protected $flipy = false;

    public function getDwgWebPath()
    {
        return $this->dwgWebPath;
    }

    public function getDwgAbsolutePath()
    {
        return $this->dwgWebPath ? $this->getDwgUploadRootDir() . '/' . basename($this->dwgWebPath) : null;
    }

    protected function getDwgUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getDwgUploadDir();
    }

    protected function getDwgUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/dwg';
    }

    public function setProduct($dwgWebPath)
    {
        $this->dwgWebPath = $dwgWebPath;
    }

    public function getX()
    {
        return $this->x;
    }

    public function setX($x)
    {
        $this->x = $x;
    }

    public function getY()
    {
        return $this->y;
    }

    public function setY($y)
    {
        $this->y = $y;
    }

    public function getFlipx()
    {
        return $this->flipx;
    }

    public function setFlipx($flipx)
    {
        $this->flipx = $flipx;
    }

    public function getFlipy()
    {
        return $this->flipy;
    }

    public function setFlipy($flipy)
    {
        $this->flipy= $flipy;
    }

    public function getAngle()
    {
        return $this->angle;
    }

    public function setAngle($angle)
    {
        $this->angle = $angle;
    }
}