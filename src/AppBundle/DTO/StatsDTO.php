<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 01.09.2016
 * Time: 11:23
 */

namespace AppBundle\DTO;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\ProductType;
use JMS\Serializer\Annotation as JMS;

class StatsDTO
{
    /**
     * Fake id
     * @JMS\Type("integer")
     * @var integer
     */
    protected $id = 1;

    /**
     * @var ProductType
     * @JMS\Exclude
     */
    protected $mostUsedProductType;

    /**
     * Most Used Product Uses Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $mostUsedProductTypeUses;

    /**
     * @var MaterialShape
     * @JMS\Exclude
     */
    protected $mostUsedShape;

    /**
     * Most Used Shape Preview Uses Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $mostUsedShapePreviewUses;

    /**
     * @var MaterialFinish
     * @JMS\Exclude
     */
    protected $mostUsedFinish;

    /**
     * Most Used Finish Uses Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $mostUsedFinishUses;

    /**
     * @var MaterialFinish
     * @JMS\Exclude
     */
    protected $mostUsedColor;

    /**
     * Most Used Color Uses Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $mostUsedColorUses;

    /**
     * Total Designs Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $totalDesigns;

    /**
     * Total Users Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $totalUsers;

    /**
     * Total Quotations Count
     * @JMS\Type("integer")
     * @var integer
     */
    protected $totalQuotations;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Most Used Product Type Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedProductTypeTitle()
    {
        return $this->mostUsedProductType ? $this->mostUsedProductType->getTitle() : null;
    }

    /**
     * Most Used Product Type Preview
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedProductTypePreview()
    {
        return $this->mostUsedProductType ? $this->mostUsedProductType->getImageWebPath() : null;
    }

    /**
     * Most Used Shape Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedShapePreviewTitle()
    {
        return $this->mostUsedShape ? $this->mostUsedShape->getTitle() : null;
    }

    /**
     * Most Used Shape Preview
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedShapePreview()
    {
        if ($this->mostUsedShape) {
            /** @var MaterialShapeSize $shapeSize */
            $shapeSize = $this->mostUsedShape->getMaterialShapeSizes()->first();

            if ($shapeSize) {
                return $shapeSize->getImageWebPath();
            }
        }

        return null;
    }

    /**
     * Most Used Finish Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedFinishTitle()
    {
        return $this->mostUsedFinish ? $this->mostUsedFinish->getTitle() : null;
    }

    /**
     * Most Used Finish Preview
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string|null
     */
    public function getMostUsedFinishPreview()
    {
        return $this->mostUsedFinish ? $this->mostUsedFinish->getTextureWebPath() : null;
    }

    /**
     * Most Used Color Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return integer|null
     */
    public function getMostUsedColorTitle()
    {
        return $this->mostUsedColor ? $this->mostUsedColor->getTitle() : null;
    }

    /**
     * Most Used Color
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return integer|null
     */
    public function getMostUsedColor()
    {
        return $this->mostUsedColor ? $this->mostUsedColor->getColor() : null;
    }

    /***********************************************/

    /**
     * @param ProductType $mostUsedProductType
     * @return $this
     */
    public function setMostUsedProductType(ProductType $mostUsedProductType)
    {
        $this->mostUsedProductType = $mostUsedProductType;

        return $this;
    }

    /**
     * @param integer $mostUsedProductTypeUses
     * @return $this
     */
    public function setMostUsedProductTypeUses($mostUsedProductTypeUses)
    {
        $this->mostUsedProductTypeUses = $mostUsedProductTypeUses;

        return $this;
    }

    /**
     * @param MaterialShape $mostUsedShape
     * @return $this
     */
    public function setMostUsedShape(MaterialShape $mostUsedShape)
    {
        $this->mostUsedShape = $mostUsedShape;

        return $this;
    }

    /**
     * @param integer $mostUsedShapePreviewUses
     * @return $this
     */
    public function setMostUsedShapePreviewUses($mostUsedShapePreviewUses)
    {
        $this->mostUsedShapePreviewUses = $mostUsedShapePreviewUses;

        return $this;
    }

    /**
     * @param MaterialFinish $mostUsedFinish
     * @return $this
     */
    public function setMostUsedFinish(MaterialFinish $mostUsedFinish)
    {
        $this->mostUsedFinish = $mostUsedFinish;

        return $this;
    }

    /**
     * @param integer $mostUsedFinishUses
     * @return $this
     */
    public function setMostUsedFinishUses($mostUsedFinishUses)
    {
        $this->mostUsedFinishUses = $mostUsedFinishUses;

        return $this;
    }

    /**
     * @param MaterialFinish $mostUsedColor
     * @return $this
     */
    public function setMostUsedColor(MaterialFinish $mostUsedColor)
    {
        $this->mostUsedColor = $mostUsedColor;

        return $this;
    }

    /**
     * @param integer $mostUsedColorUses
     * @return $this
     */
    public function setMostUsedColorUses($mostUsedColorUses)
    {
        $this->mostUsedColorUses = $mostUsedColorUses;

        return $this;
    }

    /**
     * @param integer $totalDesigns
     * @return $this
     */
    public function setTotalDesigns($totalDesigns)
    {
        $this->totalDesigns = $totalDesigns;

        return $this;
    }

    /**
     * @param integer $totalUsers
     * @return $this
     */
    public function setTotalUsers($totalUsers)
    {
        $this->totalUsers = $totalUsers;

        return $this;
    }

    /**
     * @param integer $totalQuotations
     * @return $this
     */
    public function setTotalQuotations($totalQuotations)
    {
        $this->totalQuotations = $totalQuotations;

        return $this;
    }
}
