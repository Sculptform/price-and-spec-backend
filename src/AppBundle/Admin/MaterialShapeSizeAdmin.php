<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MaterialShapeSizeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $materialSize = $this->getSubject();

        $fileFieldOptions = ['required' => false];

        if ($materialSize && ($webPath = $materialSize->getImageWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper
            ->add('materialShape', ModelType::class, ['property' => 'title'])
            ->add('width')
            ->add('depth')
            ->add('coverWidth')
            ->add('imageFile', FileType::class, $fileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('materialShape.title')
            ->add('width')
            ->add('depth')
            ->add('coverWidth');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('materialShape.title', null, ['label' => 'Material Shape'])
            ->add('width')
            ->add('depth')
            ->add('coverWidth')
            ->add('imageFile', null, ['template' => 'AppBundle:CRUD:list_image.html.twig']);
    }

    public function toString($object)
    {
        return 'Material Size';
    }
}