<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Discussion;
use AppBundle\Enum\DiscussionStatus;
use AppBundle\Enum\DiscussionType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class DiscussionAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class);
        $formMapper->add('content', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('type', ChoiceType::class, array(
            'choices' => DiscussionType::getChoices(true)
            ));
        $formMapper->add('status', ChoiceType::class, array(
            'choices' => DiscussionStatus::getChoices(true)
        ));
        $formMapper->add('user', ModelType::class, array(
            'property' => 'fullName',
            'label' => 'Author'
        ));
        $formMapper->add(
            'tags',
            CollectionType::class,
            array(
                'by_reference' => false,
                'type' => AdminType::class
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'id',
            )
        );
        $formMapper->add(
            'comments',
            CollectionType::class,
            array(
                'by_reference' => false,
                'type' => AdminType::class
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'id',
            )
        );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('title', TextType::class, array(
            'editable' => true
        ));
        $listMapper->addIdentifier('content', TextareaType::class);
        $listMapper->add('type', ChoiceType::class, array(
            'choices'  => array_flip(DiscussionType::getChoices(true)),
            'editable' => true,
        ));
        $listMapper->add('status', ChoiceType::class, array(
            'choices'  => array_flip(DiscussionStatus::getChoices(true)),
            'editable' => true,
        ));
        $listMapper->add('user', ModelType::class, array(
            'associated_property' => 'fullName',
            'label' => 'Author'
        ));
        $listMapper->add('createdAt', null, array(
            'label' => 'Posted At'
        ));
    }

    public function toString($object)
    {
        return $object instanceof Discussion
            ? $object->getTitle()
            : 'Discussion';
    }
}