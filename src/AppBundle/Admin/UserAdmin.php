<?php

namespace AppBundle\Admin;

use AppBundle\Entity\User;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\AdminBundle\Show\ShowMapper;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormError;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\Validator\Constraints\Length;

/**
 * Class PollAnswerAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class UserAdmin extends AbstractAdmin
{
    /**
     * @param $object
     */
    public function prePersist($object)
    {
        parent::prePersist($object);
        $this->updateUser($object);

    }

    /**
     * @param $object
     */
    public function preUpdate($object)
    {
        parent::preUpdate($object);
        $this->updateUser($object);

    }

    /**
     * @param User $u
     */
    public function updateUser(User $u)
    {
        $um = $this->getConfigurationPool()->getContainer()->get('fos_user.user_manager');
        $um->updateUser($u, false);
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list', 'show', 'edit'));
    }

    protected function configureFormFields(FormMapper $formMapper)
    {
        $roles = $this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles');
        foreach ($roles as $key => &$value) {
            $value = $key;
        }

        $formMapper->add('firstName', TextType::class, [
            'attr' => [
                'autocomplete' => 'new-first-name',
            ]
        ]);

        $formMapper->add('lastName', TextType::class, [
            'attr' => [
                'autocomplete' => 'new-last-name',
            ]
        ]);

        $formMapper->add('email', EmailType::class, [
            'attr' => [
                'autocomplete' => 'new-first-email',
            ]
        ]);

        $formMapper->add('roles', ChoiceType::class, [
            'choices'  => $roles,
            'multiple' => true,
            'attr' => [
                'autocomplete' => 'new-roles',
            ]
        ]);

        $formMapper->add('enabled', ChoiceType::class, [
            'label'   => 'Status',
            'choices' => ['active' => 1, 'blocked' => 0],
            'attr' => [
                'autocomplete' => 'new-enabled',
            ]
        ]);

        $formMapper->add('plainPassword', 'repeated', [
            'type'            => 'password',
            'options'         => ['translation_domain' => 'FOSUserBundle'],
            'first_options'   => ['label' => 'form.password', 'attr' => ['autocomplete' => 'new-password']],
            'second_options'  => ['label' => 'form.password_confirmation', 'attr' => ['autocomplete' => 'new-confirm']],
            'invalid_message' => 'fos_user.password.mismatch',
            'required'        => false,
            'constraints'     => [
                new Length(['min' => 8])
            ]
        ]);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('username');
        $datagridMapper->add('email');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add(
            'id',
            null,
            array(
                'header_style' => 'width: 50px; text-align: center',
                'row_align'    => 'center',
            )
        );
        $listMapper->addIdentifier('fullName', null, array());
        $listMapper->add('email');
        $listMapper->add(
            'createdAt',
            null,
            array(
                'label'    => 'Member for',
                'sortable' => true,
                'template' => 'AppBundle:User:list_created_at.html.twig',
            )
        );
        $listMapper->add(
            'lastLogin',
            null,
            array(
                'label'    => 'Last access',
                'template' => 'AppBundle:User:list_last_login.html.twig',
            )
        );
        $listMapper->add(
            'lastLoginSFM',
            null,
            array(
                'label'    => 'Last access SFM',
                'template' => 'AppBundle:User:list_last_login.html.twig',
            )
        );
        $listMapper->add(
            'userProjects',
            null,
            array(
                'label'    => 'Projects Created',
                'template' => 'AppBundle:User:list_projects.html.twig',
            )
        );
        $listMapper->add(
            'enabled',
            ChoiceType::class,
            array(
                'label'    => 'Status',
                'choices'  => array(
                    array('text' => 'active', 'value' => 1),
                    array('text' => 'blocked', 'value' => 0),
                ),
                'template' => 'AppBundle:User:list_status.html.twig',
                'editable' => true,
            )
        );
        $listMapper->add(
            'roles',
            ChoiceType::class,
            array(
                'template' => 'AppBundle:User:list_roles.html.twig',
            )
        );

        $listMapper->add(
            '_action',
            null,
            [
                'actions' => [
                    'edit' => [],
                ],
            ]
        );

    }

    protected function configureShowFields(ShowMapper $showMapper)
    {
        $roles = $this->getConfigurationPool()->getContainer()->getParameter('security.role_hierarchy.roles');
        foreach ($roles as $key => &$value) {
            $value = $key;
        }

        $showMapper->add('firstName', TextType::class);
        $showMapper->add('lastName', TextType::class);
        $showMapper->add('email', EmailType::class);
        $showMapper->add(
            'roles',
            'choice',
            array(
                'choices'  => $roles,
                'multiple' => true,
            )
        );
        $showMapper->add('lastLoginSFM', null, array('label' => 'Last Access SFM'));
        $showMapper->add(
            'enabled',
            ChoiceType::class,
            array(
                'label'    => 'Status',
                'choices'  => array(
                    array('text' => 'active', 'value' => 1),
                    array('text' => 'blocked', 'value' => 0),
                ),
                'template' => 'AppBundle:User:show_status.html.twig',
                'editable' => true,
            )
        );

        $showMapper->add(
            'userProjects',
            null,
            array('template' => 'AppBundle:User:show_projects.html.twig',)
        );
    }

    public function toString($object)
    {
        return $object instanceof User
            ? $object->getFullName()
            : 'User';
    }
}