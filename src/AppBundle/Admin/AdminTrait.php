<?php

namespace AppBundle\Admin;

trait AdminTrait
{
    public function generateCustomUrl($name, $object, $parameters = array(), $absolute = false){
        $class  = new \ReflectionClass($object);
        $admins = $this->getConfigurationPool()->getAdminClasses();

        //TODO: uncomment this block after migrating to the PHP 5.6 or higher
        /*$admin  = array_filter(
            $admins,
            function ($key) use ($class) {
                return strpos($key, $class->getShortName()) !== false;
            },
            ARRAY_FILTER_USE_KEY
        );

        $admin      = $this->getConfigurationPool()->getContainer()->get(current($admin)[0]);
        */

        //filter the array of admin classes in case we got a proxy class name for the related object
        $admin  = current(array_filter(
            array_keys($admins),
            function ($key) use ($class) {
                return strpos($key, $class->getShortName()) !== false;
            }
        ));

        $admin = $this->getConfigurationPool()->getAdminByClass($admin);
        $url   = $admin->generateObjectUrl($name, $object, $parameters, $absolute);

        return $url;
    }
}