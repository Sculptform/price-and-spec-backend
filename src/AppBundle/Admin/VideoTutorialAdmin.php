<?php

namespace AppBundle\Admin;

use AppBundle\Entity\VideoTutorial;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class VideoTutorialAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class VideoTutorialAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /*$productSandpit = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($productSandpit && ($webPath = $productSandpit->getImageWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }*/

        $formMapper->add('title', TextType::class);
        $formMapper->add('description', TextareaType::class);
        $formMapper->add('youTubeUrl', TextareaType::class);
        /*$formMapper->add('file', FileType::class, array(
            'upload_type' => 'video',
            'attr' => array(
                'class' => 'video_tutorial_upload',
                'data-url' => $this->getConfigurationPool()->getContainer()->get('oneup_uploader.templating.uploader_helper')->endpoint('video_tutorial')
            )
            ));*/
        //$formMapper->add('video', 'sonata_type_model_list', array(), array('link_parameters' => array('context' => 'video_tutorial')));
//        $formMapper->add('video', MediaType::class, array(
//            'provider' => 'sonata.media.provider.video',
//            'context' => 'video_tutorial',
//            'validation_groups' => array('video_tutorial')
//        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('description');
        $listMapper->add('youTubeUrl');
        /*$listMapper->add('imageFile', null, array(
            'template' => 'AppBundle:CRUD:list_image.html.twig'
        ));*/
    }

    public function toString($object)
    {
        return $object instanceof VideoTutorial
            ? $object->getTitle()
            : 'Video Tutorial';
    }
}