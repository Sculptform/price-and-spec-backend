<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use AppBundle\Form\Type\DwgType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class MaterialAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Material $material */
        $material = $this->getSubject();

        $dwgFileFieldOptions = ['required' => false];

        if ($material && ($webPath = $material->getDwgWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $request = $this->getRequest();
            $dwgFileFieldOptions['file_url'] = $request->getScheme() . '://' . $request->getHttpHost().$fullPath;
            $dwgFileFieldOptions['help'] = 'DWG file already <a href="' . $fullPath . '">loaded</a>';
        }

        $formMapper
            ->add('object3d', ModelType::class, array(
                'property' => 'title',
                'label' => 'Object 3D'
            ))
            ->add('materialType', ModelType::class, array(
                'property' => 'title',
                'label' => 'Material Type'
            ))
            ->add('productType', ModelType::class, array(
                'property' => 'title',
                'label' => 'Product type'
            ))
        ;

        if($this->getSubject()->getMaterialShapeSize()){
            $formMapper->add('materialShapeSize.materialShape', ModelType::class, array(
                'property' => 'title',
                'label' => 'Material shape'
            ));
        }

        $formMapper
            ->add('materialShapeSize')
            ->add('orderWeight')
            ->add('applicationTypes', ChoiceType::class, array(
                'choices' => ApplicationType::getChoices(true),
                'multiple' => true,
            ))
            ->add('dwgFile', DwgType::class, $dwgFileFieldOptions)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('object3d.title', null, array(
                'label' => 'Object 3D',
            ))
            ->add(
                'materialType',
                null,
                [
                    'label' => 'Material type'
                ],
                EntityType::class,
                [
                    'class'    => MaterialType::class,
                    'expanded' => false,
                    'multiple' => false
                ]
            )
            ->add(
                'productType',
                null,
                [
                    'label' => 'Product type'
                ],
                EntityType::class,
                [
                    'class'    => ProductType::class,
                    'expanded' => false,
                    'multiple' => false
                ]
            )
            ->add(
                'materialShapeSize.materialShape',
                null,
                [
                    'label' => 'Material Shape'
                ],
                EntityType::class,
                [
                    'class'    => MaterialShape::class,
                    'expanded' => false,
                    'multiple' => false
                ]
            )
            ->add('orderWeight', null, array(
                'label' => 'Order Weight',
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('id')
            ->add('object3d.title', null, array(
                'label' => 'Object 3D',
                'template' => 'AppBundle:Material:list_object3d.html.twig',
                'route' => array(
                    'name' => 'show'
                )
            ))
            ->add('materialType.title', null, array(
                'label' => 'Material Type',
                'template' => 'AppBundle:Material:list_material_type.html.twig'
            ))
            ->add('productType.title', null, array(
                'label' => 'Product type',
                'template' => 'AppBundle:Material:list_product_type.html.twig'
            ))
            ->add('materialShapeSize', null, array(
                'label' => 'Size',
                'template' => 'AppBundle:Material:list_size.html.twig'
            ))
            ->add('materialShapeSize.materialShape.title', null, array(
                'label' => 'Material Shape',
                'template' => 'AppBundle:Material:list_material_shape.html.twig'
            ))
            ->add('orderWeight');

        /*$listMapper->add('applicationTypes', null, array(
            'template' => 'AppBundle:Material:list_application_types.html.twig',
            'multiple' => true
        ));*/
    }

    public function toString($object)
    {
        return $object instanceof Material
            ? $object->getCompositeTitle()
            : 'Material';
    }
}