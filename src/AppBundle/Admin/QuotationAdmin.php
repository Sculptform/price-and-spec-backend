<?php
/**
 * QuotationAdmin
 */
namespace AppBundle\Admin;

use AppBundle\Entity\Quotation;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;

/**
 * Class QuotationAdmin
 */
class QuotationAdmin extends AbstractAdmin
{
    use AdminTrait;

    /**
     * @param FormMapper $formMapper
     */
    protected function configureFormFields(FormMapper $formMapper)
    {
        //TODO: Implement create\update logic, if necessary
    }

    /**
     * @param DatagridMapper $datagridMapper
     */
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('id');
        $datagridMapper->add('title');
        $datagridMapper->add('project.title');
        $datagridMapper->add('user', null, [
            'label' => 'Requester',
        ]);
    }

    /**
     * @param ListMapper $listMapper
     */
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('title')
            ->add('project.id', null, [
                'mapped' => false,
                'template' => 'AppBundle:Quotation:list_unique_url.html.twig',
                'label' => 'Unique url',
            ])
            ->add('project.title', null, [
                'label' => 'Project',
            ])
            ->add('backCallDate')
            ->add('attachment', null, [
                'template' => 'AppBundle:Quotation:list_attachment.html.twig',
            ])
            ->add('projectArea')
            ->add('content')
            ->add('first_name')
            ->add('last_name')
            ->add('email')
            ->add('phone')
            ->add('state', null, [
                'template' => 'AppBundle:Quotation:list_state.html.twig'
            ])
            ->add('company_name')
            ->add('position')
            ->add('included_in_quantities')
            ->add('attachment_url')
            ->add('createdAt')
            ->add('updatedAt');
    }

    /**
     * @param mixed $object
     *
     * @return string
     */
    public function toString($object)
    {
        return $object instanceof Quotation
            ? $object->getTitle()
            : 'Quotation';
    }

    /**
     * @param RouteCollection $collection
     */
    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('show');
    }
}
