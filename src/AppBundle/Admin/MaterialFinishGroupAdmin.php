<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Enum\ApplicationType;
use Doctrine\ORM\QueryBuilder;
use RedCode\TreeBundle\Admin\AbstractTreeAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MaterialFinishGroupAdmin extends AbstractTreeAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var MaterialFinishGroup $materialFinishGroup */
        $materialFinishGroup = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($materialFinishGroup && ($webPath = $materialFinishGroup->getImageWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper
            ->add('title')
            ->add('materialType', ModelType::class, array(
                'btn_add' => false
            ))
            ->add('productType', ModelType::class, array(
                'property' => 'title',
                'label' => 'Product Type'
            ))
            ->add('applicationTypes', ChoiceType::class, array(
                'choices' => ApplicationType::getChoices(true),
                'multiple' => true,
            ))
            ->add('imageFile', FileType::class, $fileFieldOptions)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('productType', null, [], null, array('expanded' => true, 'multiple' => true))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('title')
            ->add('applicationTypes', ChoiceType::class, array(
                'template' => 'AppBundle:Material:list_application_types.html.twig',
                'multiple' => true,
            ))
            ->add('productType.title', null, array(
                'label' => 'Product type',
                'template' => 'AppBundle:Material:list_product_type.html.twig'
            ))
            ->add('imageFile', null, array(
                'template' => 'AppBundle:CRUD:list_image.html.twig'
            ))
        ;
    }

    public function createQuery($context = 'list')
    {
        /** @var QueryBuilder $query */
        $query = parent::createQuery($context);
        $query->andWhere(
            $query->expr()->isNull($query->getRootAliases()[0] . '.parent')
        );

        return $query;
    }

    public function toString($object)
    {
        return $object instanceof MaterialFinishGroup
            ? $object->getTitle()
            : 'Category';
    }
}