<?php

namespace AppBundle\Admin;

use AppBundle\Entity\AwardType;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\MediaBundle\Form\Type\MediaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class AwardTypeAdmin
 *
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class AwardTypeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title', TextType::class);
        $formMapper->add('description', CKEditorType::class);
        $formMapper->add('image', MediaType::class, array(
            'provider' => 'sonata.media.provider.image',
            'context' => 'award_type',
            'validation_groups' => array('award_type')
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('description');
    }

    public function toString($object)
    {
        return $object instanceof AwardType
            ? $object->getTitle()
            : 'Award Type';
    }
}