<?php

namespace AppBundle\Admin;

use AppBundle\Entity\AcousticBacking;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class AcousticBackingAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('productType')
            ->add('title')
            ->add('color')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('productType')
            ->add('depth')
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('productType')
            ->add('title')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('color')
            ->add('depth')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('productType')
            ->addIdentifier('title')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('color')
            ->add('depth')
        ;
    }

    public function toString($object)
    {
        return $object instanceof AcousticBacking
            ? $object->getTitle()
            : 'Acoustic Backing';
    }
}
