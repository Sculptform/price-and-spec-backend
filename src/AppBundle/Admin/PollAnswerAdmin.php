<?php

namespace AppBundle\Admin;

use AppBundle\Entity\PollAnswer;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class PollAnswerAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class PollAnswerAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('content', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('content');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('content');
    }

    public function toString($object)
    {
        return $object instanceof PollAnswer
            ? $object->getContent()
            : 'Poll Answer';
    }
}