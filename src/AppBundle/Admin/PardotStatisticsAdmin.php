<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Admin;


use AppBundle\Entity\PardotStatistics;
use AppBundle\Entity\Poll;
use AppBundle\Enum\PardotTrackActionsEnum;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Route\RouteCollection;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Sonata\DoctrineORMAdminBundle\Filter\DateRangeFilter;
use Sonata\DoctrineORMAdminBundle\Filter\DateTimeRangeFilter;
use Sonata\Form\Type\DateRangePickerType;
use Sonata\Form\Type\DateTimeRangePickerType;
use Sonata\Form\Type\DateTimeRangeType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class PardotStatisticsAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('firstName', TextType::class);
        $formMapper->add('lastName', TextType::class);
        $formMapper->add('email', TextType::class);
        $formMapper->add('username', TextType::class);
        $formMapper->add('projectId', TextType::class);
        $formMapper->add('actionType');
        $formMapper->add('actionDate', DateType::class);

    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('firstName');
        $datagridMapper->add('lastName');
        $datagridMapper->add('email');
        $datagridMapper->add('username');
        $datagridMapper->add('projectId');
        $datagridMapper->add(
            'actionType', null, [], ChoiceType::class,
            ['choices' => PardotTrackActionsEnum::toArray()]
        );
        $datagridMapper->add(
            'actionDate', DateRangeFilter::class,
            ['label' => 'Action Date'], DateRangePickerType::class,
            ['field_options' => ['format' => 'yyyy-MM-dd']]
        );
        $datagridMapper->add(
            'show_team_members', 'doctrine_orm_callback', [
                'callback'   => function ($queryBuilder, $alias, $field, $value
                ) {
                    if ( ! $value) {
                        return;
                    }

                    $queryBuilder->andWhere(
                        sprintf(
                            '%s.email LIKE :email1 OR  %s.email LIKE :email2',
                            $alias, $alias
                        )
                    );
                    $queryBuilder->setParameter('email1', '%sibers%');
                    $queryBuilder->setParameter(
                        'email2', '%sculptform%'
                    );

                    return true;
                },
                'field_type' => 'checkbox',
            ]
        );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('firstName');
        $listMapper->add('lastName');
        $listMapper->add('email');
        $listMapper->add(
            'projectId', null, [
                'label'    => 'Project URL',
                'template' => 'AppBundle:Pardot:list_pardot_project_unique_url.html.twig',
            ]
        );
        $listMapper->add('actionType');
        $listMapper->add(
            'actionDate', null,
            ['label' => 'Action Date', 'timezone' => 'Australia/Brisbane']
        );
    }

    public function createQuery($context = 'list')
    {
        $filterParams = $this->getFilterParameters();

        $query = parent::createQuery($context);

        if ( ! array_key_exists('show_team_members', $filterParams)
            || ! array_key_exists('value', $filterParams['show_team_members'])
        ) {
            $query->andWhere(
                $query->expr()->notLike(
                    $query->getRootAliases()[0].'.email', ':param1'
                )
            );

            $query->andWhere(
                $query->expr()->notLike(
                    $query->getRootAliases()[0].'.email', ':param2'
                )
            );
            $query->setParameter('param1', '%sibers%');
            $query->setParameter('param2', '%sculptform%');
        }

        return $query;
    }

    public function toString($object)
    {
        return $object instanceof PardotStatistics
            ? $object->getId()
            : 'PardotStatistics';
    }
}
