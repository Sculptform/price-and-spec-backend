<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use AppBundle\Form\Type\DwgType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\DataTransformer\ArrayToModelTransformer;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\ImmutableArrayType;
use Symfony\Component\Form\CallbackTransformer;
use Symfony\Component\Form\Extension\Core\DataTransformer\ArrayToPartsTransformer;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class RailingAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var Railing $railing */
        $railing = $this->getSubject();

        if ($railing && ($webPath = $railing->getImageWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $fileFieldOptions['required'] = false;

        if ($railing && ($dwgPath = $railing->getDwgWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath  = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$dwgPath;
            $request   = $this->getRequest();

            $dwgFileFieldOptions['file_url'] = $request->getScheme() . '://' . $request->getHttpHost().$fullPath;
            $dwgFileFieldOptions['help']     = 'DWG file already <a href="' . $fullPath . '">loaded</a>';
        }

        $dwgFileFieldOptions['required'] = false;


        $formMapper
            ->add('title')
            ->add('description')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('width')
            ->add('height')
            ->add('cavity', TextareaType::class)
            ->add('applicationTypes', ChoiceType::class, [
                'choices' => ApplicationType::getChoices(true),
                'multiple' => true,
                'required' => false,
                'expanded' => true
            ])
            ->add('materialType', ModelType::class, array(
              'property' => 'title',
              'label' => 'Material Type'
            ))
            ->add('modelData', TextareaType::class)
            ->add('imageFile', FileType::class, $fileFieldOptions)
            ->add('dwgFile', DwgType::class, $dwgFileFieldOptions)
        ;

        $formMapper->get('modelData')->addViewTransformer(new CallbackTransformer(
            function ($array) {
                return json_encode($array);
            },
            function ($string) {
                return json_decode($string, true);
            }
        ));

        $formMapper->get('cavity')
        ->addViewTransformer(new CallbackTransformer(
            function ($array) {
                return is_array($array) ? implode(', ', $array) : '';
            },
            function ($string) {
                return array_map(function ($var){ return intval($var);}, explode(',', $string));
            }
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('width')
            ->add('height')
            ->add('materialType.title', null, array(
              'label' => 'Material Type'
            ))
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('title')
            ->add('description')
            ->add('price')
            ->add('retailPrice')
            ->add('wholesalePrice')
            ->add('width')
            ->add('height')
            ->add('materialType.title', null, array(
              'label' => 'Material Type',
              'template' => 'AppBundle:Material:list_material_type.html.twig'
            ))
            ->add('imageFile', null, array(
                'template' => 'AppBundle:CRUD:list_image.html.twig'
            ));
        ;
    }

    public function toString($object)
    {
        return $object instanceof Railing
            ? $object->getWidth() . 'mm (w) x ' . $object->getHeight() . 'mm (h)'
            : 'Railing';
    }
}