<?php

namespace AppBundle\Admin;

use AppBundle\Entity\UserProject;
use AppBundle\Enum\ApplicationType;
use AppBundle\Enum\ProjectStatus;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;

/**
 * Class PollAnswerAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class UserProjectAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var UserProject $userProject */
        $userProject = $this->getSubject();
        $project = $userProject->getProject();

        $fileFieldOptions = array(
            'label'    => 'Image',
            'required' => false,
            'attr'     => array(
                'readonly' => true,
                'disabled' => true
            )
        );

        if ($project && ($webPath = $project->getImageWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add(
            'project.title', null, array(
                'label' => 'Title',
                'attr'  => array(
                    'readonly' => true,
                    'disabled' => true
                )
            )
        );
        $formMapper->add('project.productType', ModelType::class, array(
            'property' => 'title',
            'label'    => 'Product Type',
            'btn_add'  => false,
            'attr'     => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
        $formMapper->add('project.applicationType', ChoiceType::class, array(
            'choices'  => ApplicationType::getChoices(true),
            'multiple' => false,
            'label'    => 'Application Type',
            'attr'     => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
        $formMapper->add('project.imageFile', FileType::class, $fileFieldOptions);
        $formMapper->add('project.price', null, array(
            'label' => 'Indicative Price',
            'attr'  => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
        $formMapper->add('project.wholesalePrice', null, array(
            'attr' => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
        $formMapper->add('project.retailPrice', null, array(
            'attr' => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
        $formMapper->add('project.status', ChoiceType::class, array(
            'choices'  => ProjectStatus::getChoices(true),
            'multiple' => false,
            'attr'     => array(
                'readonly' => true,
                'disabled' => true
            )
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
    }

    protected function configureListFields(ListMapper $listMapper)
    {
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('delete');
    }

    public function toString($object)
    {
        return $object instanceof UserProject
            ? $object->getProject()->getTitle()
            : 'UserProject';
    }
}