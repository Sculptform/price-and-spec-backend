<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaterialShape;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;

class MaterialShapeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var MaterialShape $materialShape */
        $materialShape = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($materialShape && ($webPath = $materialShape->getImageWebPath())) {
            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper
            ->add('title')
            ->add('isShowSubShapes')
            ->add('imageFile', FileType::class, $fileFieldOptions)
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->addIdentifier('title')
            ->add('imageFile', null, array(
                'template' => 'AppBundle:CRUD:list_image.html.twig'
            ))
        ;
    }

    public function toString($object)
    {
        return $object instanceof MaterialShape
            ? $object->getTitle()
            : 'Material Shape';
    }
}