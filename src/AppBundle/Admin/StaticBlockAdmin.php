<?php

namespace AppBundle\Admin;

use AppBundle\Entity\StaticBlock;
use Ivory\CKEditorBundle\Form\Type\CKEditorType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class StaticBlockAdmin
 *
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class StaticBlockAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('name', TextType::class);
        $formMapper->add('title', CKEditorType::class);
        $formMapper->add('content', CKEditorType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('name');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('name');
        $listMapper->add('isVisible', 'boolean', array(
            'editable' => true
        ));
        $listMapper->add('createdAt');
        $listMapper->add('updatedAt');
    }

    public function toString($object)
    {
        return $object instanceof StaticBlock
            ? $object->getName()
            : 'Static Block';
    }
}