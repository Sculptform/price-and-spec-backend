<?php

namespace AppBundle\Admin;

use AppBundle\Entity\RailingFinish;
use AppBundle\Enum\ApplicationType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class RailingFinishAdmin extends AbstractAdmin
{
    const TEXTURE_TYPE_COLOR = 'color';
    const TEXTURE_TYPE_IMAGE = 'image';

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var RailingFinish $finish */
        $finish = $this->getSubject();

        $fileFieldOptions = ['required' => false, 'label' => 'Image file'];
        if ($finish && ($webPath = $finish->getTextureWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview"  />';
        }

        $textureType = null;

        if ($finish->getColor()) {
            $textureType = self::TEXTURE_TYPE_COLOR;
        } elseif ($finish->getTexturePath()) {
            $textureType = self::TEXTURE_TYPE_IMAGE;
        }

        $formMapper->add('title');

        $formMapper
            ->add('applicationTypes', 'choice', [
                'choices' => ApplicationType::getChoices(true),
                'multiple' => true,
                'required' => false,
                'expanded' => true
            ])
            ->add('textureType', 'sonata_type_choice_field_mask', [
                'choices' => [
                    'Color' => self::TEXTURE_TYPE_COLOR,
                    'Image' => self::TEXTURE_TYPE_IMAGE,
                ],
                'map' => [
                    self::TEXTURE_TYPE_COLOR => ['color'],
                    self::TEXTURE_TYPE_IMAGE => ['textureFile'],
                ],
                'placeholder' => 'Which type of the texture to use?',
                'required' => true,
                'mapped' => false,
                'data' => $textureType
            ])
            ->add('color', 'sonata_type_color_selector', [
                'required' => false,
                'placeholder' => 'Not chosen'
            ])
            ->add('textureFile', 'file', $fileFieldOptions)
        ;

        $builder = $formMapper->getFormBuilder();
        $factory = $builder->getFormFactory();

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($factory) {
            $form = $event->getForm();
            $data = $event->getData();

            /** @var RailingFinish $finish */
            $finish = $form->getData();

            if ($data['textureType'] === self::TEXTURE_TYPE_IMAGE) {
                $data['color'] = null;
            } elseif ($data['textureType'] === self::TEXTURE_TYPE_COLOR) {
                if ($finish->getTexturePath() && is_file($finish->getTexturePath())) {
                    unlink($finish->getTexturePath());
                }

                $form->add($factory->createNamed('texturePath', 'hidden', '', [
                    'auto_initialize' => false
                ]));
            }

            $event->setData($data);
        });
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title' )
            ->add('color')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('color', 'sonata_type_color_selector', array(
            'template' => 'AppBundle:MaterialFinish:list_color.html.twig',
        ));
        $listMapper->add('textureFile', null, array(
            'template' => 'AppBundle:MaterialFinish:list_texture.html.twig'
        ));
    }

    public function toString($object)
    {
        return $object instanceof RailingFinish
            ? $object->getTitle()
            : 'Finish';
    }

}