<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Poll;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\CoreBundle\Form\Type\CollectionType;
use Symfony\Component\Form\Extension\Core\Type\DateType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class PollAdmin
 *
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class PollAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('question', TextType::class);
        $formMapper->add('startDate', DateType::class);
        $formMapper->add(
            'answers',
            CollectionType::class,
            array(
                'by_reference' => false,
                'type' => AdminType::class
            ),
            array(
                'edit' => 'inline',
                'inline' => 'table',
                'sortable' => 'id',
            )
        );
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('question');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('question');
        $listMapper->add('startDate');
        $listMapper->add('results', 'string', array('label' => 'Results', 'template' => 'AppBundle:Poll:list_poll_results.html.twig'));
    }

    public function toString($object)
    {
        return $object instanceof Poll
            ? $object->getQuestion()
            : 'Poll';
    }
}