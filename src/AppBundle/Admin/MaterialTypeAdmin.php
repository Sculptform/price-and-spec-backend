<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaterialType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MaterialTypeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $materialType = $this->getSubject();

        $container = $this->getConfigurationPool()->getContainer();

        $fileFieldOptions      = array('required' => false);
        $popupFileFieldOptions = array('required' => false);

        if ($materialType && ($webPath = $materialType->getImageWebPath())) {
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }


        if ($materialType && ($webPath = $materialType->getPopupImageWebPath())) {
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $popupFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add('title');
        $formMapper->add('imageFile', FileType::class, $fileFieldOptions);
        $formMapper->add('popupTitle');
        $formMapper->add('popupContent', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('popupUrl');
        $formMapper->add('popupImageFile', FileType::class, $popupFileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('imagePath', null, array(
            'template' => 'AppBundle:CRUD:list_image.html.twig',
            'label'    => 'Image'
        ));
        $listMapper->add('popupTitle');
        $listMapper->add('popupContent');
        $listMapper->add('popupUrl');
        $listMapper->add('popupImage', null, array(
            'template' => 'AppBundle:Material:list_popup_image.html.twig',
            'label'    => 'Popup Image'
        ));
    }

    public function toString($object)
    {
        return $object instanceof MaterialType
            ? $object->getTitle()
            : 'Material Type';
    }
}