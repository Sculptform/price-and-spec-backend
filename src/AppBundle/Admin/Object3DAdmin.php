<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Object3D;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class Object3DAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $object3D = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($object3D && ($webPath = $object3D->getImageWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add('title');
        $formMapper->add('value', TextareaType::class);
        $formMapper->add('imageFile', FileType::class, $fileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('value', null, array(
            'header_style' => 'width: 50%',
            'collapse' => array(
                'height' => 0, // height in px
                'read_more' => 'Read more', // content of the "read more" link
                'read_less' => 'Read less' // content of the "read less" link
            )
        ));
        $listMapper->add('imagePath', null, array(
            'template' => 'AppBundle:CRUD:list_image.html.twig',
            'label'    => 'Image'
        ));
    }

    public function toString($object)
    {
        return $object instanceof Object3D
            ? $object->getTitle()
            : 'Object 3D';
    }
}