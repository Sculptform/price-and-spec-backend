<?php

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Route\RouteCollection;

class AccountStatisticsAdmin extends AbstractAdmin
{
    protected $baseRoutePattern = 'account-statistics';
    protected $baseRouteName = 'accountStatistics';

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->clearExcept(array('list'));
    }
}