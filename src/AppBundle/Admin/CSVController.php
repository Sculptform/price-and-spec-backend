<?php

namespace AppBundle\Admin;


use AppBundle\Enum\CSVTypes;
use Ddeboer\DataImport\Reader\CsvReader;
use Ddeboer\DataImport\Workflow\StepAggregator;
use Ddeboer\DataImport\Writer\DoctrineWriter;
use Doctrine\ORM\Mapping\ClassMetadataInfo;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Workflow\Workflow;
use Symfony\Component\Routing\Annotation\Route;

class CSVController extends Controller
{

    /**
     * @Route("/csv/import", name="csv_import")
     * @param Request $request
     * @return \Symfony\Component\HttpFoundation\Response
     */
    public function importFileAction(Request $request) {
        //TODO: MAKE REFACTORING OF THIS METHOD!!!
        $param=$request->request;

        $curType=trim($param->get("fileType"));
        $uploadedFile=$request->files->get("csvFile");

        try {
            if (!CSVTypes::existsType($curType) || $uploadedFile==null){
                throw new \RuntimeException('Import error.');
            }


            $dummyImport=getcwd()."/temp";
            $fname="directly.csv";
            $filename=$dummyImport."/".$fname;
            @mkdir($dummyImport);
            @unlink($filename);

            $uploadedFile->move($dummyImport,$fname);

            // open file
            /*$source = fopen($filename, 'r+');
            if ($source===false) die("Can't open filestream $filename");
            $file = $source->getFile();
            if ($file===false)   die("Can't open file $filename");*/

            // Create and configure the reader
            $file      = new \SplFileObject($filename, 'r+');
            $csvReader = new CsvReader($file,",");
            if ($csvReader===false) die("Can't create csvReader $filename");
            $csvReader->setHeaderRowNumber(0);

            // this must be done to import CSVs where one of the data-field has CRs within!
            $file->setFlags(\SplFileObject::READ_CSV |
                \SplFileObject::SKIP_EMPTY |
                \SplFileObject::READ_AHEAD);

            // Set Database into "nonchecking Foreign Keys"
            $em=$this->getDoctrine()->getManager();
            $em->getConnection()->exec("SET FOREIGN_KEY_CHECKS=0;");

            // Create the workflow
            $workflow = new StepAggregator($csvReader);
            if ($workflow===false) {
                throw new \RuntimeException('Unable to create workflow '.$filename);
            }

            $curEntityClass = CSVTypes::getEntityClass($curType);
            $writer         = new DoctrineWriter($em, $curEntityClass);

            $writer->setTruncate(false);

            $entityMetadata = $em->getClassMetadata($curEntityClass);
            $entityMetadata->setIdGeneratorType(ClassMetadataInfo::GENERATOR_TYPE_AUTO);

            $workflow->addWriter($writer);
            $workflow->process();

            // RESetting Database Check Status
            $em->getConnection()->exec("SET FOREIGN_KEY_CHECKS=1;");

        }catch (\Exception $ex){
            $request->getSession()->getFlashBag()->add("error", $ex->getMessage());
            return $this->redirect($this->generateUrl('sonata_admin_dashboard'));
        }

        $request->getSession()->getFlashBag()->add("success", 'Import was successful.');
        return $this->redirect($this->generateUrl('admin_app_product_list'));
    }

}