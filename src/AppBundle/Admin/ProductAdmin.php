<?php

namespace AppBundle\Admin;

use AppBundle\Enum\MaterialUnitType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

/**
 * Class ProductAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class ProductAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('material', ModelType::class, array(
            'btn_add' => false
        ));
        $formMapper->add('materialFinish', ModelType::class, array(
            'label' => 'Finish',
            'property' => 'title'
        ));
        $formMapper->add('unit');
        $formMapper->add('price');
        $formMapper->add('wholesalePrice');
        $formMapper->add('retailPrice');
        $formMapper->add('naturalAccentPrice');
        $formMapper->add('naturalAccentWholesalePrice');
        $formMapper->add('naturalAccentRetailPrice');
        $formMapper->add('enviroproPrice');
        $formMapper->add('enviroproWholesalePrice');
        $formMapper->add('enviroproRetailPrice');
        $formMapper->add('cutekPrice');
        $formMapper->add('cutekWholesalePrice');
        $formMapper->add('cutekRetailPrice');
        $formMapper->add('weight');
        $formMapper->add('acoustic');
        $formMapper->add('itemId');
        $formMapper->add('itemNumber');
        $formMapper->add('itemDescription', TextareaType::class, array('attr' => array('class' => 'ckeditor')));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('itemId');
        $datagridMapper->add('itemNumber');
        $datagridMapper->add('itemDescription');
        $datagridMapper->add('material');
        $datagridMapper->add('material.productType');
        $datagridMapper->add('materialFinish');
        $datagridMapper->add('price');
        $datagridMapper->add('wholesalePrice');
        $datagridMapper->add('retailPrice');
        $datagridMapper->add('naturalAccentPrice');
        $datagridMapper->add('naturalAccentWholesalePrice');
        $datagridMapper->add('naturalAccentRetailPrice');
        $datagridMapper->add('enviroproPrice');
        $datagridMapper->add('enviroproWholesalePrice');
        $datagridMapper->add('enviroproRetailPrice');
        $datagridMapper->add('cutekPrice');
        $datagridMapper->add('cutekWholesalePrice');
        $datagridMapper->add('cutekRetailPrice');
        $datagridMapper->add('weight');
        $datagridMapper->add('acoustic');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->add('itemId', null, array(
            'editable' => true
        ));
        $listMapper->add('itemNumber', null, array(
            'editable' => true
        ));
        $listMapper->addIdentifier('itemDescription', null, array(
            'editable' => true
        ));
        $listMapper->add('material.productType');
        $listMapper->add('material', null, array(
            'template' => 'AppBundle:Product:list_material.html.twig'
        ));
        $listMapper->add('materialFinish.title', null, array(
            'label' => 'Finish',
            'template' => 'AppBundle:Product:list_finish.html.twig'
        ));
        $listMapper->add('unit', ChoiceType::class, array(
            'choices'  => MaterialUnitType::getChoices(true),
            'editable' => true,
        ));
        $listMapper->add('price', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('wholesalePrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('retailPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('naturalAccentPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('naturalAccentWholesalePrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('naturalAccentRetailPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('enviroproPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('enviroproWholesalePrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('enviroproRetailPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('cutekPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('cutekWholesalePrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('cutekRetailPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('weight', 'text', array(
            'editable'   => true,
        ));
        $listMapper->add('acoustic', 'text', array(
            'editable'   => true,
        ));
    }

    public function toString($object)
    {
        return 'Product';
    }
}