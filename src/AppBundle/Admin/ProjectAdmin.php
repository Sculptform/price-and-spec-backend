<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Project;
use AppBundle\Enum\ProjectStatus;
use AppBundle\Enum\UserProjectUserType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Sonata\CoreBundle\Form\Type\DateRangePickerType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class ProjectAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title');
        $formMapper->add('productType', ModelType::class, array(
            'property' => 'title',
            'label' => 'Product Type'
        ));
        $formMapper->add('price', null, array(
            'label' => 'Indicative Price'
        ));
        $formMapper->add('wholesalePrice');
        $formMapper->add('retailPrice');
        $formMapper->add('status', ChoiceType::class, array(
            'choices' => ProjectStatus::getChoices(true),
            'multiple' => false,
        ));
        $formMapper->add('materialCoatings', null, array('expanded' => false, 'multiple' => true));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $now  = new \DateTime();

        $datagridMapper->add('title');
        $datagridMapper->add('price');
        $datagridMapper->add('wholesalePrice');
        $datagridMapper->add('retailPrice');
        $datagridMapper->add('productType', null, [], null, array('expanded' => true, 'multiple' => true));
        $datagridMapper->add('status', null, [], ChoiceType::class, array(
            'choices' => ProjectStatus::getChoices(true),
            'multiple' => false,
        ));
        $datagridMapper
            ->add('updatedAt', 'doctrine_orm_date_range', [
                'input_type'    => 'timestamp'
            ]);
        $datagridMapper->add('owner', 'doctrine_orm_callback', array(
            'callback' => function($queryBuilder, $alias, $field, $value) {
                if (!$value['value']) {
                    return;
                }

                $queryBuilder->leftJoin(sprintf('%s.userProjects', $alias), 'up');
                $queryBuilder->leftJoin('up.user', 'u');
                $queryBuilder->andWhere("up.userType = :type");
                $queryBuilder->andWhere("u.roles not like :role");
                $queryBuilder->andWhere("u.roles not like :role1");
                $queryBuilder->andWhere("u.email not like :email");
                $queryBuilder->andWhere("u.email not like :email1");
                $queryBuilder->andWhere("u.email not like :email2");
                $queryBuilder->andWhere("u.email not like :email3");
                $queryBuilder->setParameter('type' , UserProjectUserType::OWNER);
                $queryBuilder->setParameter('email', '%woodformarch.com%');
                $queryBuilder->setParameter('email1', '%sibers.com%');
                $queryBuilder->setParameter('email2', '%sculptform.com%');
                $queryBuilder->setParameter('email3', '%sculptform.com.au%');
                $queryBuilder->setParameter('role', '%admin%');
                $queryBuilder->setParameter('role1', '%tester%');

                return true;
            },
            'field_type' => CheckboxType::class,
            'label'      => 'Exclude Team Members (admin, tester, @sibers.com, @woodformarch.com, @sculptform.com, @sculptform.com.au)'
        ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id', null, array(
            'label' => 'Unique URL',
            'template' => 'AppBundle:Project:list_project_unique_url.html.twig'
        ));
        $listMapper->addIdentifier('title');
        $listMapper->add('price', 'decimal', array(
            'editable'   => true,
            'label'      => 'Indicative Price',
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('wholesalePrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('retailPrice', 'decimal', array(
            'editable'   => true,
            'attr'       => array('data-step' => '0.01'),
            'template'   => 'AppBundle:CRUD:list_field.html.twig'
        ));
        $listMapper->add('productType.title', null, array(
            'label' => 'Product type',
            'template' => 'AppBundle:Material:list_product_type.html.twig'
        ));
        $listMapper->add('createdAt', null, array(
            'label' => 'Date Created'
        ));
        $listMapper->add('updatedAt', null, array(
            'label' => 'Date Updated'
        ));
        $listMapper->add('projectOwner.fullname', null, array(
            'label' => 'Owner',
            'template' => 'AppBundle:Project:list_project_owner.html.twig'
        ));
        $listMapper->add('projectOwner.username', null, array(
            'label' => 'Owner Username',
            'template' => 'AppBundle:Project:list_project_owner_username.html.twig'
        ));
        $listMapper->add('projectOwner.email', null, array(
            'label' => 'Owner Email',
            'template' => 'AppBundle:Project:list_project_owner_email.html.twig'
        ));
        $listMapper->add('projectOwner.companyName', null, array(
            'label' => 'Owner Company',
            'template' => 'AppBundle:Project:list_project_owner_company_name.html.twig'
        ));
    }

    public function toString($object)
    {
        return $object instanceof Project
            ? $object->getTitle()
            : 'Project';
    }

    public function getExportFields()
    {
        return array('title', 'price', 'wholesalePrice', 'retailPrice', 'productType.title', 'createdAt', 'updatedAt',
                     'owner.fullname', 'owner.username', 'owner.email', 'owner.companyName');
    }

}