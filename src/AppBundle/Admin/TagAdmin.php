<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Tag;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class TagAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('content', TextType::class);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('content', TextType::class);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('tag', TextType::class);
        $listMapper->add('createdAt');
    }

    public function toString($object)
    {
        return $object instanceof Tag
            ? $object->getContent()
            : 'Tag';
    }
}