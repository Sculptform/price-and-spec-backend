<?php

namespace AppBundle\Admin;

use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class FintraxInfoAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class FintraxInfoAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('materialShape', ModelType::class, [
                'property' => 'title',
                'label' => 'Shape',
            ])
            ->add('materialFinishGroup', ModelType::class, [
                'label' => 'Finish Group',
                'property' => 'title'
            ])
            ->add('price')
            ->add('weight')
            ->add('width')
            ->add('depth')
            ->add('itemNumber')
            ->add('description', TextareaType::class, ['attr' => ['class' => 'ckeditor']])
        ;
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('itemNumber')
            ->add('description')
            ->add('price')
            ->add('weight')
            ->add('width')
            ->add('depth')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->add('id')
            ->add('itemNumber', null, array(
                'editable' => true
            ))
            ->addIdentifier('description', null, array(
                'editable' => true
            ))
            ->add('price', 'decimal', array(
                'editable'   => true,
                'attr'       => array('data-step' => '0.01'),
                'template'   => 'AppBundle:CRUD:list_field.html.twig'
            ))
            ->add('weight', TextType::class, array(
                'editable'   => true,
            ))
            ->add('width', TextType::class, array(
                'editable'   => true,
            ))
            ->add('depth', TextType::class, array(
                'editable'   => true,
            ))
        ;
    }

    public function toString($object)
    {
        return 'Fintrax Info';
    }
}