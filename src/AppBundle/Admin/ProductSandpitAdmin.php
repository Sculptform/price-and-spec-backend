<?php

namespace AppBundle\Admin;

use AppBundle\Entity\ProductSandpit;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

/**
 * Class ProductSandpitAdmin
 *
 * @author  Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @package AppBundle\Admin
 */
class ProductSandpitAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $productSandpit = $this->getSubject();

        $fileFieldOptions = array('required' => false);
        if ($productSandpit && ($webPath = $productSandpit->getImageWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add('title', TextType::class);
        $formMapper->add('description', TextareaType::class, array('attr' => array('class' => 'ckeditor')));
        $formMapper->add('imageFile', FileType::class, $fileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->add('description');
        $listMapper->add('imageFile', null, array(
            'template' => 'AppBundle:CRUD:list_image.html.twig'
        ));
    }

    public function toString($object)
    {
        return $object instanceof ProductSandpit
            ? $object->getTitle()
            : 'Product Sandpit';
    }
}