<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaterialCoating;
use AppBundle\Enum\ApplicationType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class MaterialCoatingAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('title');
        $formMapper->add('productPriceFieldName');
        $formMapper->add('productWholesalePriceFieldName');
        $formMapper->add('productRetailPriceFieldName');
        $formMapper->add('description', TextareaType::class, array(
            'attr' => array('ckeditor')
        ));
        $formMapper->add('applicationTypes', ChoiceType::class, array(
            'choices' => ApplicationType::getChoices(true),
            'multiple' => true,
        ));
        $formMapper->add('materialType', ModelType::class, array(
            'property' => 'title',
            'label' => 'Material Type'
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
        $datagridMapper->add('description');
        $datagridMapper->add('applicationTypes');
        $datagridMapper->add('materialType.title', null, array(
            'label' => 'Material Type'
        ));
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('title');
        $listMapper->add('productPriceFieldName', null, array(
            'editable' => true
        ));
        $listMapper->add('productWholesalePriceFieldName', null, array(
            'editable' => true
        ));
        $listMapper->add('productRetailPriceFieldName', null, array(
            'editable' => true
        ));
        $listMapper->add('description', null, array(
            'editable' => true
        ));
        $listMapper->add('applicationTypes', ChoiceType::class, array(
            'template' => 'AppBundle:Material:list_application_types.html.twig',
            'multiple' => true
        ));
        $listMapper->add('materialType', null, array(
            'label' => 'Material Type',
            'template' => 'AppBundle:Material:list_material_type.html.twig'
        ));
    }

    public function toString($object)
    {
        return $object instanceof MaterialCoating
            ? $object->getTitle()
            : 'Material Coating';
    }
}