<?php

namespace AppBundle\Admin;

use AppBundle\Entity\Comment\DiscussionComment;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ModelType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;

class DiscussionCommentAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add('content', TextareaType::class);
        $formMapper->add('user', ModelType::class, array(
            'property' => 'fullName',
            'label' => 'Author',
            'btn_add' => false
        ));
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('content', TextareaType::class);
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('content', TextareaType::class);
        $listMapper->add('user');
    }

    public function toString($object)
    {
        return $object instanceof DiscussionComment
            ? $object->getTitle()
            : 'Discussion Comment';
    }
}