<?php

namespace AppBundle\Admin;

use AppBundle\Entity\ProductType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;

class ProductTypeAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var ProductType $productType */
        $productType = $this->getSubject();

        $fileFieldOptions          = ['required' => false];
        $popupFileFieldOptions     = ['required' => false];
        $pdfHeaderFileFieldOptions = ['required' => false];

        $container = $this->getConfigurationPool()->getContainer();

        if ($productType && ($webPath = $productType->getImageWebPath())) {
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        if ($productType && ($webPath = $productType->getPdfHeaderWebPath())) {
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $pdfHeaderFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        if ($productType && ($webPath = $productType->getPopupImageWebPath())) {
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $popupFileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview" />';
        }

        $formMapper->add('title', TextType::class);
        $formMapper->add('fullTitle', TextType::class);
        $formMapper->add('description', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('imageFile', FileType::class, $fileFieldOptions);
        $formMapper->add('pdfHeaderFile', FileType::class, $pdfHeaderFileFieldOptions);
        $formMapper->add('popupTitle');
        $formMapper->add('popupContent', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('priceIncludeNote', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('priceExcludeNote', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('priceNote', TextareaType::class, ['attr' => ['class' => 'ckeditor']]);
        $formMapper->add('popupUrl');
        $formMapper->add('popupImageFile', FileType::class, $popupFileFieldOptions);
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper->add('title');
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->addIdentifier('fullTitle');
        $listMapper->add('description');
        $listMapper->add('imageFile', null, array(
            'template' => 'AppBundle:CRUD:list_image.html.twig'
        ));
        $listMapper->add('popupTitle');
        $listMapper->add('popupContent');
        $listMapper->add('popupUrl');
        $listMapper->add('popupImage', null, array(
            'template' => 'AppBundle:Material:list_popup_image.html.twig',
            'label'    => 'Popup Image'
        ));
    }

    public function toString($object)
    {
        return $object instanceof ProductType
            ? $object->getTitle()
            : 'Product Type';
    }
}
