<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 25.10.17
 * Time: 21:09
 */

namespace AppBundle\Admin;


use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\AdminType;
use Sonata\AdminBundle\Form\Type\CollectionType;
use Sonata\AdminBundle\Form\Type\ModelType;

class DefaultMaterialFinishAdmin extends AbstractAdmin
{
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper->add(
            'materialFinish',
            ModelType::class
        );
        $formMapper->add(
            'productType',
            ModelType::class
        );
        $formMapper->add(
            'materialType',
            ModelType::class
        );
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->addIdentifier('id');
        $listMapper->add('materialFinish', ModelType::class, array(
            'associated_property' => 'title',
            'label' => 'Finish'
        ));
        $listMapper->add('materialType', ModelType::class, array(
            'associated_property' => 'title',
            'label' => 'Material'
        ));
        $listMapper->add('productType', ModelType::class, array(
            'associated_property' => 'title',
            'label' => 'Product'
        ));
    }
}