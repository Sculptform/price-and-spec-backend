<?php

namespace AppBundle\Admin;

use AppBundle\Entity\SampleRequest;
use AppBundle\Enum\SampleRequestProjectSize;
use AppBundle\Enum\SampleType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Route\RouteCollection;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;

class SampleRequestAdmin extends AbstractAdmin
{
    use AdminTrait;

    protected function configureFormFields(FormMapper $formMapper)
    {
        //TODO: Implement create\update logic, if necessary
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('id')
            ->add('user', null, [
                'label' => 'Requester'
            ])
            ->add('email')
            ->add('streetAddress')
            ->add('postCode')
            ->add('phone')
            ->add('projectName')
            ->add('sampleType', null, ['label' => 'Type'], ChoiceType::class, [
                'choices' => SampleType::getChoices(true),
                'multiple' => false
            ])
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->add('sampleType', ChoiceType::class, ['choices' => SampleType::getTitles(), 'label' => 'Type']);
        $listMapper->add('project.id', null, array(
            'mapped'   => false,
            'template' => 'AppBundle:Quotation:list_unique_url.html.twig',
            'label'    => 'Unique url'
        ));
        $listMapper->add('user.fullName', null, ['label' => 'Requester']);
        $listMapper->add('email');
        $listMapper->add('streetAddress');
        $listMapper->add('postCode');
        $listMapper->add('phone');
        $listMapper->add('projectName');
        $listMapper->add('projectSize', null, ['template' => 'AppBundle:Quotation:list_project_size.html.twig']);
        $listMapper->add('createdAt');
        $listMapper->add('updatedAt');
    }

    public function toString($object)
    {
        return $object instanceof SampleRequest
            ? $object->getProjectName()
            : 'SampleRequest';
    }

    protected function configureRoutes(RouteCollection $collection)
    {
        $collection->remove('create');
        $collection->remove('edit');
        $collection->remove('show');
    }

}