<?php

namespace AppBundle\Admin;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Enum\ApplicationType;
use Sonata\AdminBundle\Admin\AbstractAdmin;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\AdminBundle\Form\Type\ChoiceFieldMaskType;
use Sonata\CoreBundle\Form\Type\ColorSelectorType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\FileType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;

class MaterialFinishAdmin extends AbstractAdmin
{
    const TEXTURE_TYPE_COLOR = 'color';
    const TEXTURE_TYPE_IMAGE = 'image';

    protected function configureFormFields(FormMapper $formMapper)
    {
        /** @var MaterialFinish $finish */
        $finish = $this->getSubject();

        $fileFieldOptions = ['required' => false, 'label' => 'Image file'];
        if ($finish && ($webPath = $finish->getTextureWebPath())) {

            $container = $this->getConfigurationPool()->getContainer();
            $fullPath = $container->get('request_stack')->getCurrentRequest()->getBasePath().'/'.$webPath;

            $fileFieldOptions['help'] = '<img src="'.$fullPath.'" class="admin-preview"  />';
        }

        $textureType = null;

        if ($finish->getColor()) {
            $textureType = self::TEXTURE_TYPE_COLOR;
        } elseif ($finish->getTexturePath()) {
            $textureType = self::TEXTURE_TYPE_IMAGE;
        }

        $formMapper->add('title');

        if($this->getSubject()->getMaterialType()) {
            $formMapper->add('materialType', null, [
                'required' => true,
                'expanded' => true
            ]);
        }

        $formMapper
            ->add('applicationTypes', ChoiceType::class, [
                'choices' => ApplicationType::getChoices(true),
                'multiple' => true,
                'required' => false,
                'expanded' => true
            ])
            ->add('group', null, ['label' => 'Category'])
            ->add('textureType', ChoiceFieldMaskType::class, [
                'choices' => [
                    'Color' => self::TEXTURE_TYPE_COLOR,
                    'Image' => self::TEXTURE_TYPE_IMAGE,
                ],
                'map' => [
                    self::TEXTURE_TYPE_COLOR => ['color'],
                    self::TEXTURE_TYPE_IMAGE => ['textureFile'],
                ],
                'placeholder' => 'Which type of the texture to use?',
                'required' => true,
                'mapped' => false,
                'data' => $textureType
            ])
            ->add('color', ColorSelectorType::class, [
                'required' => false,
                'placeholder' => 'Not chosen'
            ])
            ->add('textureFile', FileType::class, $fileFieldOptions)
            ->add('orderWeight')
        ;

        $builder = $formMapper->getFormBuilder();
        $factory = $builder->getFormFactory();

        $builder->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) use ($factory) {
            $form = $event->getForm();
            $data = $event->getData();

            /** @var MaterialFinish $finish */
            $finish = $form->getData();

            if ($data['textureType'] === self::TEXTURE_TYPE_IMAGE) {
                $data['color'] = null;
            } elseif ($data['textureType'] === self::TEXTURE_TYPE_COLOR) {
                if ($finish->getTexturePath() && is_file($finish->getTexturePath())) {
                    unlink($finish->getTexturePath());
                }

                $form->add($factory->createNamed('texturePath', HiddenType::class, '', [
                    'auto_initialize' => false
                ]));
            }

            $event->setData($data);
        });
    }

    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('title' )
            ->add('materialType.title', null, [
                'label' => 'Material Type'
            ])
            ->add(
                'group',
                null,
                [
                    'label' => 'Category'
                ],
                EntityType::class,
                [
                    'class'    => MaterialFinishGroup::class,
                    'expanded' => false,
                    'multiple' => false
                ]
            )
            ->add('color')
        ;
    }

    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper->add('id');
        $listMapper->addIdentifier('title');
        $listMapper->addIdentifier('orderWeight');
        $listMapper->add('materialType.title', null, array(
            'label' => 'Material Type'
        ));
        $listMapper->add('group.parent.title', null, array(
            'label'    => 'Category',
            'template' => 'AppBundle:MaterialFinish:list_category.html.twig'
        ));
        $listMapper->add('color', ColorSelectorType::class, array(
            'template' => 'AppBundle:MaterialFinish:list_color.html.twig',
        ));
        $listMapper->add('textureFile', null, array(
            'template' => 'AppBundle:MaterialFinish:list_texture.html.twig'
        ));
    }

    public function toString($object)
    {
        return $object instanceof MaterialFinish
            ? $object->getTitle()
            : 'Finish';
    }

}
