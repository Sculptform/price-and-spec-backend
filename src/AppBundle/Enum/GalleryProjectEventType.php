<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:34
 */

namespace AppBundle\Enum;

final class GalleryProjectEventType extends BaseEnum
{
    const COMMENT_EVENT = 1;
    const SHARED_EVENT = 2;
}