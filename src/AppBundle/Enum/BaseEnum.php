<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 15:47
 */

namespace AppBundle\Enum;

abstract class BaseEnum
{
    /**
     * @return array
     */
    public static function toArray()
    {
        return (new \ReflectionClass(static::class))->getConstants();
    }

    /**
     * @return array
     */
    public static function toString()
    {
        return implode(',', (new \ReflectionClass(static::class))->getConstants());
    }

    /**
     * @param bool $withLabels
     * @return array
     */
    public static function getChoices($withLabels = false)
    {
        $constants = static::toArray();
        $titles = static::getTitles();

        return $withLabels ? ($titles ? array_flip($titles) : $constants) : array_values($constants);
    }

    /**
     * @return string
     */
    public static function getDescription()
    {
        $constants = static::toArray();
        $titles = static::getTitles();

        return implode(', ', array_map(
            function ($k, $v){ return $k . ' - ' . $v; },
            array_keys($constants),
            $titles ? array_flip($titles) : $constants
        ));
    }

    /**
     * @return array
     */
    public static function getTitles()
    {
        return [];
    }

    /**
     * @param $key
     * @return string
     */
    public static function getTitle($key)
    {
        $titles = static::getTitles();

        if (in_array($key, array_keys($titles))) {
            return $titles[$key];
        } else {
            throw new \InvalidArgumentException(sprintf('Enum has not title for key "%s"', $key));
        }
    }
}
