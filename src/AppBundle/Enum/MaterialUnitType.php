<?php

namespace AppBundle\Enum;

/**
 * Class MaterialUnitType
 * @package AppBundle\Enum
 */
final class MaterialUnitType extends BaseEnum
{
    const LM = 'lm';
    const KG = 'kg';
}