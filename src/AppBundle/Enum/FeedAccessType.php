<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:34
 */

namespace AppBundle\Enum;

final class FeedAccessType extends BaseEnum
{
    const PUBLIC_ACCESS  = 1;
    const PRIVATE_ACCESS = 2;
}