<?php

namespace AppBundle\Enum;

use AppBundle\Entity\Discussion;
use AppBundle\Entity\Feed;
use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\Project;
use AppBundle\Event\Events;

class SharedContentType extends BaseEnum
{
    const FEED            = 1;
    const GALLERY_PROJECT = 2;
    const PROJECT         = 3;
    const DISCUSSION      = 4;

    public static function getTitles() {
        return [
            self::FEED            => 'Feed',
            self::GALLERY_PROJECT => 'GalleryProject',
            self::PROJECT         => 'Project',
            self::DISCUSSION      => 'Discussion'
        ];
    }

    public static function getEvents() {
        return [
            self::FEED            => Events::FEED_SHARE,
            self::GALLERY_PROJECT => Events::GALLERY_PROJECT_SHARE,
            self::PROJECT         => Events::PROJECT_SHARE,
            self::DISCUSSION      => Events::DISCUSSION_SHARE
        ];
    }

    public static function getClasses(){
        return array(
            self::FEED            => Feed::class,
            self::GALLERY_PROJECT => GalleryProject::class,
            self::PROJECT         => Project::class,
            self::DISCUSSION      => Discussion::class
        );
    }
}