<?php

namespace AppBundle\Enum;

/**
 * Class SampleType
 *
 * @package AppBundle\Enum
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
final class SampleType extends BaseEnum
{
    const LOOSE_SAMPLE = 1;
    const SAMPLE_BOARD = 2;

    /**
     * @return array
     */
    public static function getTitles()
    {
        return [
            self::LOOSE_SAMPLE => 'Loose sample',
            self::SAMPLE_BOARD => 'Sample board'
        ];
    }
}
