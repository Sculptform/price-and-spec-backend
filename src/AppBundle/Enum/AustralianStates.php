<?php

namespace AppBundle\Enum;

final class AustralianStates extends BaseEnum
{
    const NONE    = 'none';
    const NSW     = 'nsw';
    const VIC     = 'vic';
    const QLD     = 'qld';
    const WA      = 'wa';
    const SA      = 'sa';
    const TAS     = 'tas';
    const ACT     = 'act';
    const NT      = 'nt';
    const OUTSIDE = 'outside_of_australia';

    /**
     * @return array
     */
    public static function getTitles()
    {
        return [
            self::NONE    => 'n/a',
            self::NSW     => 'New South Wales',
            self::VIC     => 'Victoria',
            self::QLD     => 'Queensland',
            self::WA      => 'Western Australia',
            self::SA      => 'South Australia',
            self::TAS     => 'Tasmania',
            self::ACT     => 'Australian Capital Territory',
            self::NT      => 'Northern Territory',
            self::OUTSIDE => 'Outside of Australia',
        ];
    }

    /**
     * @param $key
     * @return string
     */
    public static function getTitle($key)
    {
        $titles = static::getTitles();

        $key = static::normalize($key);

        if (in_array($key, array_keys($titles))) {
            return $titles[$key];
        } else {
            return 'n/a';
        }
    }

    private static function normalize($key){
        return str_replace(' ', '_', strtolower($key));
    }
}