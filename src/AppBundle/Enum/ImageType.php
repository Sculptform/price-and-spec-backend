<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 16:27
 */

namespace AppBundle\Enum;

final class ImageType extends BaseEnum
{
    const TEAM               = 1;
    const FEED               = 2;
    const COMMENT            = 3;
    const GALLERY_PROJECT    = 4;
    const STATIC_BLOCK       = 5;
}