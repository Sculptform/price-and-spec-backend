<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:53
 */

namespace AppBundle\Enum;

final class UserProjectUserType extends BaseEnum
{
    const OWNER = 1;
    const SHARED = 2;
}