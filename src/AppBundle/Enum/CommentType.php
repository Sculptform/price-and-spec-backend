<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 16:27
 */

namespace AppBundle\Enum;

use AppBundle\Entity\Comment\DiscussionComment;
use AppBundle\Entity\Comment\FeedComment;
use AppBundle\Entity\Comment\GalleryProjectComment;
use AppBundle\Entity\Comment\ProductSandpitComment;
use AppBundle\Entity\Comment\ProjectComment;
use AppBundle\Entity\Comment\TeamComment;

final class CommentType extends BaseEnum
{
    public static $map = [
        self::TEAM => TeamComment::class,
        self::FEED => FeedComment::class,
        self::DISCUSSION => DiscussionComment::class,
        self::PROJECT => ProjectComment::class,
        self::PRODUCT_SANDPIT => ProductSandpitComment::class,
        self::GALLERY_PROJECT => GalleryProjectComment::class
    ];

    const TEAM         = 1;
    const FEED         = 2;
    const DISCUSSION   = 3;
    const PROJECT      = 4;
    const PRODUCT_SANDPIT = 5;
    const GALLERY_PROJECT = 6;
}