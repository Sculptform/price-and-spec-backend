<?php

namespace AppBundle\Enum;

final class SampleRequestProjectSize extends BaseEnum
{
    const SIZE_1 = 1;
    const SIZE_2 = 2;
    const SIZE_3 = 3;
    const SIZE_4 = 4;

    /**
     * @return array
     */
    public static function getTitles()
    {
        return [
            self::SIZE_1 => '0-50m2',
            self::SIZE_2 => '51-150m2',
            self::SIZE_3 => '151-300m2',
            self::SIZE_4 => '> 301m2'
        ];
    }
}