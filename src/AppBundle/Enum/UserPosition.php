<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 16:27
 */

namespace AppBundle\Enum;

final class UserPosition extends BaseEnum
{
    const ARCHITECT    = 'Architect';
    const BUILDER      = 'Builder';
    const DESIGNER     = 'Designer';
    const OWN_BUILDER  = 'Own Builder';
    const OTHER        = 'Other';

    public static function getTitles()
    {
        return [
            self::ARCHITECT => 'Architect',
            self::BUILDER => 'Builder',
            self::DESIGNER => 'Designer',
            self::OWN_BUILDER => 'Own Builder',
            self::OTHER => 'Other'
        ];
    }
}