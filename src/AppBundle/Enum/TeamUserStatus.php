<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 15.12.16
 * Time: 12:48
 */

namespace AppBundle\Enum;

final class TeamUserStatus extends BaseEnum
{
    const CREATED   = 1;
    const INVITED   = 2;
    const CONFIRMED = 3;
    const DISAGREE  = 4;
    const DELETED   = 5;
}