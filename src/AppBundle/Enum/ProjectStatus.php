<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:16
 */

namespace AppBundle\Enum;

final class ProjectStatus extends BaseEnum
{
    const ACTIVE   = 1;
    const DELETED  = 2;
    const ARCHIVED = 3;

    /**
     * @return array
     */
    public static function getTitles()
    {
        return [
            self::ACTIVE   => 'Active',
            self::DELETED  => 'Deleted',
            self::ARCHIVED => 'Archived'
        ];
    }
}