<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 20.07.2016
 * Time: 18:47
 */

namespace AppBundle\Enum;

final class ApplicationType extends BaseEnum
{
    const INTERIOR = 1;
    const EXTERIOR = 2;

    const INTERIOR_LABEL = 'Interior Only';
    const EXTERIOR_LABEL = 'Exterior Only';
    const BOTH_LABEL     = 'Interior / Exterior';


    /**
     * @return array
     */
    public static function getTitles()
    {
        return [
            self::INTERIOR => 'Interior',
            self::EXTERIOR => 'Exterior',
            '' => 'Interior / Exterior'
        ];
    }
}