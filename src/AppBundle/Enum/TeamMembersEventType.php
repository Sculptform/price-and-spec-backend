<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:34
 */

namespace AppBundle\Enum;

final class TeamMembersEventType extends BaseEnum
{
    const PROJECT_EVENT  = 1;
    const GALLERY_PROJECT_EVENT = 2;
    const DISCUSSION_EVENT = 3;
}