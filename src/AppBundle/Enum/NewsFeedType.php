<?php
/**
 * Created by PhpStorm.
 * User: dreyup
 * Date: 24.04.17
 * Time: 10:41
 */

namespace AppBundle\Enum;

class NewsFeedType extends BaseEnum
{
    const DRUPAL = 1;
    const LINKEDIN = 2;
}