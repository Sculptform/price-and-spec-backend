<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:08
 */

namespace AppBundle\Enum;

final class DiscussionType extends BaseEnum
{
    const FEEDBACK   = 1;
    const SUGGESTION = 2;
    const QUESTION   = 3;
    const COMPLAINT  = 4;
}