<?php

namespace AppBundle\Enum;

final class DiscussionStatus extends BaseEnum
{
    const POST  = 1;
    const TRASH = 2;
}