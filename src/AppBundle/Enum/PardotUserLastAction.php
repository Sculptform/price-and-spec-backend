<?php

namespace AppBundle\Enum;

/**
 * Class PardotUserLastAction
 */
final class PardotUserLastAction extends BaseEnum
{
    const DOWNLOAD_DRAWINGS            = 'download-drawings'; //+
    const DOWNLOAD_SPECIFICATION_PDF   = 'download-specification-pdf';
    const DOWNLOAD_SPECIFICATION_WORD  = 'download-specification-word';
    const DOWNLOAD_SPECIFICATION_EXCEL = 'download-specification-excel';
    const REQUEST_SAMPLES              = 'request-sample';
    const REQUEST_QUOTE                = 'request-quote';
    const CREATE_ACCOUNT               = 'create-account';
    const SESSION_LOGIN                = 'session-login';

    /**
     * @return array
     */
    public static function getForms()
    {
        return [
            self::DOWNLOAD_DRAWINGS            => "/l/82542/2019-01-24/dhxlsb",
            self::DOWNLOAD_SPECIFICATION_PDF   => "/l/82542/2019-01-26/dhxn2j",
            self::DOWNLOAD_SPECIFICATION_WORD  => "/l/82542/2019-02-11/dhy2gy",
            self::DOWNLOAD_SPECIFICATION_EXCEL => "/l/82542/2019-07-10/dj2yt2",
            self::REQUEST_SAMPLES              => "/l/82542/2019-01-26/dhxn2l",
            self::REQUEST_QUOTE                => "/l/82542/2019-01-26/dhxn2n",
            self::CREATE_ACCOUNT               => "/l/82542/2021-07-29/dshnsr",
            self::SESSION_LOGIN                => "/l/82542/2021-07-29/dshnst",
        ];
    }

    /**
     * @param string $action
     *
     * @return string
     */
    public static function getForm($action)
    {
        $forms = static::getForms();

        if (!in_array($action, array_keys($forms))) {
            throw new \InvalidArgumentException(sprintf('Enum has not form for action "%s"', $action));
        }

        return $forms[$action];
    }
}
