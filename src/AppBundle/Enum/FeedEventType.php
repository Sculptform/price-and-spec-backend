<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:34
 */

namespace AppBundle\Enum;

final class FeedEventType extends BaseEnum
{
    const LIKE_EVENT  = 1;
    const COMMENT_EVENT = 2;
    const SHARED_EVENT = 3;
}