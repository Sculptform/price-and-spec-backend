<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Enum;


class PardotTrackActionsEnum extends BaseEnum
{
    const DOWNLOAD_PDF = 'download-pdf';
    const REQUEST_SAMPLE = 'request-sample';
    const DOWNLOAD_DWG = 'download-dwg';
    const START_ORDER = 'start-order';
    const DOWNLOAD_XLS = 'download-xls';
}
