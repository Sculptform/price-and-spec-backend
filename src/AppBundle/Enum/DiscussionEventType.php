<?php

namespace AppBundle\Enum;

final class DiscussionEventType extends BaseEnum
{
    const SPAM_EVENT = 1;
    const SHARED_EVENT = 2;
    const COMMENT_EVENT = 3;
}