<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 16:27
 */

namespace AppBundle\Enum;

final class NotificationKind extends BaseEnum
{
    const NEWS      = 1;
    const SHARE     = 2;
    const QUOTATION = 3;
    const USER_FOLLOWER = 4;
    const TEAM_USER = 5;
    const FEED = 6;
    const PROJECT = 7;
    const GALLERY_PROJECT = 8;
    const TEAM_MEMBERS = 9;
    const DISCUSSION = 10;
    const PRODUCT_SANDPIT = 11;
    const FOLLOWER_DISCUSSION = 12;

}