<?php

namespace AppBundle\Enum;

class FollowedContentType extends BaseEnum
{
    const DISCUSSION = 1;

    public static function getTitles() {
        return [
            self::DISCUSSION => 'Discussion'
        ];
    }
}