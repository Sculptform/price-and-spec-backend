<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 22.12.16
 * Time: 17:34
 */

namespace AppBundle\Enum;

final class TeamUserEventType extends BaseEnum
{
    const INVITED_USER_EVENT  = 1;
    const REQUEST_TO_JOIN_TEAM_EVENT = 2;
}