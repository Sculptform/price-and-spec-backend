<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 24.03.17
 * Time: 16:49
 */

namespace AppBundle\Enum;


final class CSVTypes extends BaseEnum {
    const PRODUCT = 1;


    public static function getTitles() {
        return [
            self::PRODUCT => 'Products'
        ];
    }

    public static function getEntityClass($type) {
        switch ($type) {
            case self::PRODUCT: return "AppBundle:Product";
            default: return false;
        }
    }

    public static function existsType($type) {
        $types=array_flip(self::toArray());
        if (isset($types[$type])) return true;

        return false;
    }
}