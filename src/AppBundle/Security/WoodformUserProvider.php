<?php

namespace AppBundle\Security;

use AppBundle\Exception\ValidationException;
use FOS\UserBundle\Security\UserProvider;
use Lexik\Bundle\JWTAuthenticationBundle\Security\Authentication\Token\JWTUserToken;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\Exception\UnsupportedUserException;
use Symfony\Component\Security\Core\User\UserInterface;

class WoodformUserProvider extends UserProvider implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param $key
     * @param bool $isNoCacheUser
     * @return string
     */
    public function getUsernameForWoodformOAuthKey($key, $isNoCacheUser = false) {
        try{
            $cachedUsername = $this->getCache()->getItem("woodform_oauth_usernames.$key");

            if (!$cachedUsername->isHit() || (bool)$isNoCacheUser) {
                $user = $this->getWoodformService()->getUserByOAuthKey($key);

                $cachedUsername->set($user->getUsername());
            }

            $cachedUsername->expiresAfter($this->getAccessExpirationInterval());

            $this->getCache()->save($cachedUsername);

            return $cachedUsername->get();
        } catch (\Exception $e) {
            if ($e instanceof ValidationException) {
                throw new CustomUserMessageAuthenticationException($e->getMessage(), $e->getErrors());
            } else throw new CustomUserMessageAuthenticationException($e->getMessage(), $e->getTrace());
        }
    }

    public function getUsernameForLegacyAuthorizationKey($key){
        $token = new JWTUserToken();

        $token->setRawToken($key);

        $authToken = $this->getAuthenticationManager()->authenticate($token);

        return $authToken->getUsername();
    }

    /**
     * {@inheritDoc}
     */
    public function refreshUser(UserInterface $user)
    {
        throw new UnsupportedUserException();
    }

    /**
     * @return \Symfony\Component\Security\Core\Authentication\AuthenticationProviderManager
     */
    protected function getAuthenticationManager()
    {
        return $this->container->get('security.authentication.manager');
    }

    /**
     * @return \AppBundle\Service\WoodformService
     */
    protected function getWoodformService()
    {
        return $this->container->get('app.service.woodform');
    }

    /**
     * @return int
     */
    protected function getAccessExpirationInterval()
    {
        return (int)$this->container->getParameter('woodform_oauth_access_cache_expiration_interval');
    }

    /**
     * @return \Symfony\Component\Cache\Adapter\TraceableAdapter
     */
    protected function getCache()
    {
        return $this->container->get('cache.app');
    }
}
