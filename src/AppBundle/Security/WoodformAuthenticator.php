<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 27.09.16
 * Time: 9:57
 */

namespace AppBundle\Security;

use Lexik\Bundle\JWTAuthenticationBundle\TokenExtractor\AuthorizationHeaderTokenExtractor;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Security\Core\Authentication\Token\PreAuthenticatedToken;
use Symfony\Component\Security\Core\Authentication\Token\TokenInterface;
use Symfony\Component\Security\Core\Exception\AuthenticationException;
use Symfony\Component\Security\Core\Exception\CustomUserMessageAuthenticationException;
use Symfony\Component\Security\Core\User\UserProviderInterface;
use Symfony\Component\Security\Http\Authentication\AuthenticationFailureHandlerInterface;
use Symfony\Component\Security\Http\Authentication\SimplePreAuthenticatorInterface;

class WoodformAuthenticator implements SimplePreAuthenticatorInterface, AuthenticationFailureHandlerInterface
{
    const WOODFORM_API_HEADER = 'x-api-key';
    const LEGACY_API_HEADER = 'authorization';
    const NO_CACHE_USER_HEADER = 'x-no-cache-user';

    public function createToken(Request $request, $providerKey)
    {
        $legacyTokenExtractor = new AuthorizationHeaderTokenExtractor('Bearer', self::LEGACY_API_HEADER);
        $legacyAuthorizationToken = $legacyTokenExtractor->extract($request);

        $woodformAuthorizationToken =
            $request->headers->has(self::WOODFORM_API_HEADER) ?
            $request->headers->get(self::WOODFORM_API_HEADER) :
            false;

        if($legacyAuthorizationToken || $woodformAuthorizationToken) {
            $credentials = [
                self::WOODFORM_API_HEADER => $woodformAuthorizationToken,
                self::LEGACY_API_HEADER => $legacyAuthorizationToken,
                self::NO_CACHE_USER_HEADER => $request->headers->has(self::NO_CACHE_USER_HEADER)
            ];

            return new PreAuthenticatedToken(
                'anon.',
                $credentials,
                $providerKey
            );
        } else {
            return null;
        }
    }

    public function supportsToken(TokenInterface $token, $providerKey)
    {
        return $token instanceof PreAuthenticatedToken && $token->getProviderKey() === $providerKey;
    }

    public function authenticateToken(TokenInterface $token, UserProviderInterface $userProvider, $providerKey)
    {
        if (!$userProvider instanceof WoodformUserProvider) {
            throw new \InvalidArgumentException(
                sprintf(
                    'The user provider must be an instance of WoodformUserProvider (%s was given).',
                    get_class($userProvider)
                )
            );
        }

        $credentials = (array) $token->getCredentials();

        $legacyAuthorizationToken = &$credentials[self::LEGACY_API_HEADER];
        $woodformAuthorizationToken = &$credentials[self::WOODFORM_API_HEADER];
        $isNoCacheUser = &$credentials[self::NO_CACHE_USER_HEADER];

        if ($woodformAuthorizationToken) {
            $username = $userProvider->getUsernameForWoodformOAuthKey($woodformAuthorizationToken, $isNoCacheUser);

            if (!$username) {
                throw new CustomUserMessageAuthenticationException(
                    sprintf('WoodformArch API Key "%s" does not exist.', $woodformAuthorizationToken)
                );
            }
        } elseif ($legacyAuthorizationToken) {
            $username = $userProvider->getUsernameForLegacyAuthorizationKey($legacyAuthorizationToken);

            if (!$username) {
                throw new CustomUserMessageAuthenticationException(
                    sprintf('API Key "%s" does not exist.', $legacyAuthorizationToken)
                );
            }
        } else {
            throw new CustomUserMessageAuthenticationException(
                sprintf('Authorization token does not exist.')
            );
        }

        $user = $userProvider->loadUserByUsername($username);

        $token = new PreAuthenticatedToken(
            $user,
            $credentials,
            $providerKey,
            $user->getRoles()
        );

        $token->setAttributes([
            self::WOODFORM_API_HEADER => $woodformAuthorizationToken,
            self::LEGACY_API_HEADER => $legacyAuthorizationToken
        ]);

        return $token;
    }

    public function onAuthenticationFailure(Request $request, AuthenticationException $exception)
    {
        $data = [
            'code' => 401,
            'message' => $exception->getMessage()
        ];

        if($exception->getMessageData()) {
            $data['errors'] = $exception->getMessageData();
        }

        return new JsonResponse(
            $data,
            401
        );
    }
}
