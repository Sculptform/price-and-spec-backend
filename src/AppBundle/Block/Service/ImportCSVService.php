<?php

namespace AppBundle\Block\Service;


use AppBundle\Enum\CSVTypes;
use Sonata\AdminBundle\Form\FormMapper;
use Sonata\BlockBundle\Block\BlockContextInterface;
use Sonata\BlockBundle\Block\Service\AbstractBlockService;
use Sonata\BlockBundle\Model\BlockInterface;
use Sonata\CoreBundle\Validator\ErrorElement;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class ImportCSVService extends AbstractBlockService
{

    private $container = null;

    public function __construct($name, $templating, $container = null)
    {
        parent::__construct($name, $templating);
        $this->container = $container;
    }

    public function getName()
    {
        return 'Import CSV';
    }

    public function validateBlock(ErrorElement $errorElement, BlockInterface $block)
    {
        $errorElement
            ->with('settings.template')
            ->assertNotNull(array())
            ->assertNotBlank()
            ->end()
            ->with('settings.title')
            ->assertNotNull(array())
            ->assertNotBlank()
            ->assertMaxLength(array('limit' => 50))
            ->end();
    }

    public function buildEditForm(FormMapper $formMapper, BlockInterface $block)
    {
    }

    public function execute(BlockContextInterface $blockContext, Response $response = null)
    {
        $settings = $blockContext->getSettings();

        //TODO: Refactor the code line below, to pass settings via OptionsResolverInterface
        $blockContext->setSetting('template', 'AppBundle:Block:block_import_csv.html.twig');

        return $this->renderResponse(
            $blockContext->getTemplate(),
            array(
                'block' => $blockContext->getBlock(),
                'types' => CSVTypes::getTitles(),
                'settings' => $settings
            ),
            $response
        );
    }
}