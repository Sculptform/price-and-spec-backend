<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170411131051 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE video_tutorial ADD video_id INT DEFAULT NULL, DROP image_path');
        $this->addSql('ALTER TABLE video_tutorial ADD CONSTRAINT FK_6EE9CD5529C1004E FOREIGN KEY (video_id) REFERENCES media__media (id)');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_6EE9CD5529C1004E ON video_tutorial (video_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE video_tutorial DROP FOREIGN KEY FK_6EE9CD5529C1004E');
        $this->addSql('DROP INDEX UNIQ_6EE9CD5529C1004E ON video_tutorial');
        $this->addSql('ALTER TABLE video_tutorial ADD image_path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, DROP video_id');
    }
}
