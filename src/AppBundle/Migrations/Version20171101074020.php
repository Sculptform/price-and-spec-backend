<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialFinish;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171101074020 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $titles = ['Charred Burnt Ash' => 'Yakisugi Ash', 'Charred Spotted Gum' => 'Yakisugi Pine'];

        foreach ($titles as $key=>$value){
            $item = $em->getRepository(MaterialFinish::class)->findOneBy(['title' => $key]);
            if($item){
                $item->setTitle($value);
                $em->persist($item);
            }
        }
            $em->flush();
    }
}
