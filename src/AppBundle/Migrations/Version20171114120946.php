<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Object3D;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171114120946 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $ymlData = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/object3ds.yml')
        )['AppBundle\Entity\Object3D'];

        $object = $em->getRepository(Object3D::class)->findOneBy(['title' => 'Wave 12x150']);

        if($object){
            $object->setValue($ymlData['object3d_wave_12x150']['value']);
            $em->persist($object);
            $em->flush();
        }
    }
}
