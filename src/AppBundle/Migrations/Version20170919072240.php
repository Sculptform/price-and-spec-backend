<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Provider\UploadedFileProvider;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Service\ProductService;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170919072240 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $uploadedFileProvider = UploadedFileProvider::create();

        $materialTypeTimber = $em->find(MaterialType::class, 1);
        $productTypeECL = $em->find(ProductType::class, 2);

        $yakisugiFinishGroup = new MaterialFinishGroup();

        $yakisugiFinishGroupImageFile = $uploadedFileProvider->uploadedFile('Data/FinishGroups/ECL/Timber/Yakisugi Charred Species.png');

        $yakisugiFinishGroup
            ->setProductType($productTypeECL)
            ->setMaterialType($materialTypeTimber)
            ->setApplicationTypes([1,2])
            ->setTitle('Yakisugi Charred Species')
            ->setImageFile($yakisugiFinishGroupImageFile)
        ;

        $basePath = __DIR__ . '/../DataFixtures/Data/Finishes/ECL/Timber/Yakisugi Charred Species';

        /** @var SplFileInfo[] $files */
        $files = Finder::create()->files()->in($basePath);

        foreach ($files as $file) {
            $textureFile = $uploadedFileProvider->uploadedFile(
                $file->getFilename(),
                $file->getFilename(),
                $basePath
            );

            $materialFinish = new MaterialFinish();

            $materialFinish
                ->setTitle($file->getBasename('.' . $file->getExtension()))
                ->setGroup($yakisugiFinishGroup)
                ->setApplicationTypes([1,2])
                ->setMaterialType($materialTypeTimber)
                ->setTextureFile($textureFile)
            ;

            $em->persist($materialFinish);
        }

        $em->flush();

        /** @var ProductService $productService */
        $productService = $this->container->get('app.service.product');

        $productService->updateECLFromCSV(__DIR__ . '/../DataFixtures/Data/Products/Sculptform ECL 18.09.csv');
    }
}
