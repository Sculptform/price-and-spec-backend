<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170605005538 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE expression_claddings (id INT AUTO_INCREMENT NOT NULL, project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', product_id INT DEFAULT NULL, base_finish_id INT DEFAULT NULL, stacker_finish_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, INDEX IDX_8F95A86E166D1F9C (project_id), INDEX IDX_8F95A86E4584665A (product_id), INDEX IDX_8F95A86E986AF666 (base_finish_id), INDEX IDX_8F95A86E843A947A (stacker_finish_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86E166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86E986AF666 FOREIGN KEY (base_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86E843A947A FOREIGN KEY (stacker_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE expression_claddings');
    }
}
