<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170419054445 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE notification_follower_discussion (id INT NOT NULL, discussion_id INT NOT NULL, event_user_id INT NOT NULL, INDEX IDX_85B66F3B1ADED311 (discussion_id), INDEX IDX_85B66F3B22397A3A (event_user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE notification_follower_discussion ADD CONSTRAINT FK_85B66F3B1ADED311 FOREIGN KEY (discussion_id) REFERENCES discussion (id)');
        $this->addSql('ALTER TABLE notification_follower_discussion ADD CONSTRAINT FK_85B66F3B22397A3A FOREIGN KEY (event_user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE notification_follower_discussion ADD CONSTRAINT FK_85B66F3BBF396750 FOREIGN KEY (id) REFERENCES notification (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE notification_follower_discussion');
    }
}
