<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190417111953 extends AbstractMigration implements
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema): void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema): void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        /**
         * @var EntityManagerInterface $em
         */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $notes = [
            'Click-on Battens' => [
                'priceInclude' => '<p>All Standard Componentry - Corner trims, End caps, L-profiles, Clips, Tracks and Back Cover Strip. Freight to site within Australia (minimum order applies for free international freight to nearest port, customer to clear goods, pay duties, taxes, etc).</p>',
                'priceExclude' => '<p>GST, any custom corner/edging treatment, flashing, installation, substrates, hangers and TCR for suspended ceilings, fixings for substrate or insulation batts. Pricing is based on the amount of product required (m²) as specified in the pricing panel.</p>',
                'priceNote'    => '<p>Please note: We have a minimum order of $15k due to manufacturing costs (see our <a href="https://sculptform.com.au/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use" target="_blank">Terms of Use</a> for more details.</p>',
            ],
            'Tongue & Groove Cladding' => [
                'priceInclude' => '<p>All Standard Componentry – Our proprietary corner profiles, fixing screws, starter extrusions. Freight to site within Australia (minimum order applies for free international freight to nearest port, customer to clear goods, pay duties, taxes, etc).</p>',
                'priceExclude' => '<p>GST, any custom corner/edging treatment, flashing, breathable sarking,  installation, substrates, hangers and TCR for suspended ceilings, fixings for substrate or insulation batts. Pricing is based on the amount of product required (m²) as specified in the pricing panel.</p>',
                'priceNote'    => '<p>Please note: We have a minimum order of $15k due to manufacturing costs (see our <a href="https://sculptform.com.au/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use" target="_blank">Terms of Use</a> for more details.</p>',
            ],
            'Facade Blades' => [
                'priceInclude' => '<p>All Standard Componentry – mounting rail, blade brackets, blade endcaps, rail endcaps. Freight to site within Australia (minimum order applies for free international freight to nearest port, customer to clear goods, pay duties, taxes, etc).</p>',
                'priceExclude' => '<p>GST, any custom corner/edging treatment, installation, substrates, fixings for substrate, flashing. Pricing is based on the amount of product required (m²) as specified in the pricing panel.</p>',
                'priceNote'    => '<p>Please note: We have a minimum order of $15k due to manufacturing costs (see our <a href="https://sculptform.com.au/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use" target="_blank">Terms of Use</a> for more details.</p>',
            ],
        ];

        $productTypes = $em->getRepository(ProductType::class)->findAll();

        /**
         * @var ProductType $type
         */
        foreach ($productTypes as $type) {
            $type->setPriceIncludeNote(
                @$notes[$type->getTitle()]['priceInclude']
            );
            $type->setPriceExcludeNote(
                @$notes[$type->getTitle()]['priceExclude']
            );

            $type->setPriceNote(@$notes[$type->getTitle()]['priceNote']);

            $em->persist($type);
            $em->flush();
        }
    }
}
