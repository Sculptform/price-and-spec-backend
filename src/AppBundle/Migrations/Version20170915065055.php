<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170915065055 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE railing_finish (id INT AUTO_INCREMENT NOT NULL, material_type_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, color VARCHAR(255) DEFAULT NULL, application_types LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\', image_path VARCHAR(255) DEFAULT NULL, INDEX IDX_C101505974D6573C (material_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE railing_finish ADD CONSTRAINT FK_C101505974D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC890BD0C');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC890BD0C FOREIGN KEY (railing_finish_id) REFERENCES railing_finish (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC890BD0C');
        $this->addSql('DROP TABLE railing_finish');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC890BD0C FOREIGN KEY (railing_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20170914150148\LoadFixtures()]);
    }
}
