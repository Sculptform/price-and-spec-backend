<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511144429 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE ontrax (id INT AUTO_INCREMENT NOT NULL, object3d_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, color VARCHAR(255) NOT NULL, price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, retail_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, wholesale_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, width INT NOT NULL, height INT NOT NULL, INDEX IDX_FF72FD1DCDD70535 (object3d_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ontrax ADD CONSTRAINT FK_FF72FD1DCDD70535 FOREIGN KEY (object3d_id) REFERENCES object3d (id)');
        $this->addSql('ALTER TABLE acoustic_backing ADD retail_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, ADD wholesale_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, CHANGE price price DOUBLE PRECISION DEFAULT \'0\' NOT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE ontrax');
        $this->addSql('ALTER TABLE acoustic_backing DROP retail_price, DROP wholesale_price, CHANGE price price DOUBLE PRECISION NOT NULL');
    }
}
