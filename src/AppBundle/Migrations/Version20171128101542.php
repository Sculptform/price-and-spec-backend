<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\ProductType;
use AppBundle\Service\ProductService;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171128101542 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        /** @var ProductService $productService */
        $productService = $this->container->get('app.service.product');
        $thumbnailsDir  = __DIR__ . '/../DataFixtures/Data/ToolboxThumbnails';
        $ymlData        = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/object3ds.yml')
        )['AppBundle\Entity\Object3D'];

        /**
         * @var EntityManager $em
         */
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $fs = new Filesystem();

        $object3D          = new Object3D();
        $material          = new Material();
        $materialShapeSize = new MaterialShapeSize();

        $materialShapeSize->setWidth(100);
        $materialShapeSize->setDepth(25);
        $materialShapeSize->setMaterialShape($em->getRepository(MaterialShape::class)->findOneBy(['name' => 'aluminium_block']));

        $object3D->setTitle($ymlData['object3d_block_100x25']['title']);
        $object3D->setValue($ymlData['object3d_block_100x25']['value']);


        $imagePath = implode(
            '/',
            [
                $thumbnailsDir,
                'Concept Click',
                'Aluminium',
                'Block',
                '100x25.png'
            ]
        );

        if ($fs->exists($imagePath)) {
            $imageFilename = md5($imagePath);
            $imageFileInfo = new \SplFileInfo($imagePath);

            $tmpFilePath = $fs->tempnam('/tmp', 'uploaded-file-');

            $fs->copy($imagePath, $tmpFilePath, true);

            $imageFile = new UploadedFile(
                $tmpFilePath,
                $imageFilename,
                MimeTypeGuesser::getInstance()->guess($imagePath),
                $imageFileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );
            $materialShapeSize->setImageFile($imageFile);
        }

        $em->persist($materialShapeSize);
        $em->persist($object3D);

        $material->setApplicationTypes([1, 2]);
        $material->setProductType($em->getRepository(ProductType::class)->findOneBy(['title' => 'Concept Click']));
        $material->setMaterialType($em->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']));
        $material->setMaterialShapeSize($materialShapeSize);
        $material->setObject3d($object3D);

        $em->persist($material);
        $em->flush();

        $productService->updateCCLFromCSV(__DIR__ . '/Fixtures/Version20171128101542/Sculptform CCL 28.11.csv');
    }
}
