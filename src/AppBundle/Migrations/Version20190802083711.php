<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190802083711 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE material_shape SET name='queenscliff_element', title='Queenscliff' WHERE name='queenscliff_element'");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE material_shape SET name='queenscliff_element', title='Queenscliff & Element' WHERE name='queenscliff_element'");
    }
}
