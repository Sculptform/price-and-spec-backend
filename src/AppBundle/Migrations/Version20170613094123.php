<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170613094123 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintraxes DROP FOREIGN KEY FK_C6028A3A4584665A');
        $this->addSql('DROP INDEX IDX_C6028A3A4584665A ON fintraxes');
        $this->addSql('ALTER TABLE fintraxes ADD nosing_product_id INT DEFAULT NULL, CHANGE product_id base_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3AD63EB556 FOREIGN KEY (base_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A9CB80083 FOREIGN KEY (nosing_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C6028A3AD63EB556 ON fintraxes (base_product_id)');
        $this->addSql('CREATE INDEX IDX_C6028A3A9CB80083 ON fintraxes (nosing_product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintraxes DROP FOREIGN KEY FK_C6028A3AD63EB556');
        $this->addSql('ALTER TABLE fintraxes DROP FOREIGN KEY FK_C6028A3A9CB80083');
        $this->addSql('DROP INDEX IDX_C6028A3AD63EB556 ON fintraxes');
        $this->addSql('DROP INDEX IDX_C6028A3A9CB80083 ON fintraxes');
        $this->addSql('ALTER TABLE fintraxes ADD product_id INT DEFAULT NULL, DROP base_product_id, DROP nosing_product_id');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_C6028A3A4584665A ON fintraxes (product_id)');
    }
}
