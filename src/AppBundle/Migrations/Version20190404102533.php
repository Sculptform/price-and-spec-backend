<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190404102533 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_filter (id INT AUTO_INCREMENT NOT NULL, name VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE project_filters (project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', filter_id INT NOT NULL, INDEX IDX_EE696BE8166D1F9C (project_id), INDEX IDX_EE696BE8D395B25E (filter_id), PRIMARY KEY(project_id, filter_id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_filters ADD CONSTRAINT FK_EE696BE8166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_filters ADD CONSTRAINT FK_EE696BE8D395B25E FOREIGN KEY (filter_id) REFERENCES project_filter (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project_filters DROP FOREIGN KEY FK_EE696BE8D395B25E');
        $this->addSql('DROP TABLE project_filter');
        $this->addSql('DROP TABLE project_filters');
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20190404102533\LoadFixtures()]);
    }
}
