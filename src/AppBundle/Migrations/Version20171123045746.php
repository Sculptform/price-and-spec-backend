<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123045746 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material ADD order_weight INT DEFAULT NULL');

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 1
            WHERE ms.name = 'base_stacker'
        ");

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 2
            WHERE ms.name = 'stacker'
        ");

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 3
            WHERE ms.name = 'nosing_block'
        ");

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 4
            WHERE ms.name = 'nosing_dome'
        ");

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 5
            WHERE ms.name = 'nosing_flute'
        ");

        $this->addSql("
            UPDATE material AS m
            JOIN material_shape_size AS mss
            ON mss.id = m.material_shape_size_id
            JOIN material_shape AS ms
            ON mss.material_shape_id = ms.id
            SET m.order_weight = 6
            WHERE ms.name = 'nosing_peak'
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material DROP order_weight');
    }
}
