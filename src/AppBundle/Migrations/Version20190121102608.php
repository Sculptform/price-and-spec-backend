<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190121102608 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings ADD position INT DEFAULT NULL');
        $this->addSql("UPDATE railings SET title='Standard Track', position = 1 WHERE title='45x17mm Standard'");
        $this->addSql("UPDATE railings SET title='Curving Track', position = 3 WHERE title='45x17mm Curving (notched)'");
        $this->addSql("UPDATE railings SET title='Suspended Ceiling Track', position = 2 WHERE title='45x30mm Suspended Ceilings'");
        $this->addSql("UPDATE railings SET title='Standard Track', position = 1 WHERE title='Railing'");
        $this->addSql("UPDATE railings SET title='Standard Track', material_type_id = 1, position = 1 WHERE title='Frame'");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings DROP position');
        $this->addSql("UPDATE railings SET title='45x17mm Standard' WHERE title='Standard Track'");
        $this->addSql("UPDATE railings SET title='45x17mm Curving (notched)' WHERE title='Curving Track'");
        $this->addSql("UPDATE railings SET title='45x30mm Suspended Ceilings' WHERE title='Suspended Ceiling Track'");
        $this->addSql("UPDATE railings SET title='Railing' WHERE product_type_id = 4");
        $this->addSql("UPDATE railings SET title='Frame' WHERE product_type_id = 2");
    }
}
