<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170329065121 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC07866C5');
        $this->addSql('CREATE TABLE material_coating (id INT AUTO_INCREMENT NOT NULL, material_type_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, product_price_field_name VARCHAR(255) DEFAULT NULL, application_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', INDEX IDX_40ABC46D74D6573C (material_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE material_coating ADD CONSTRAINT FK_40ABC46D74D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE timber_coating_type');
        $this->addSql('DROP INDEX IDX_2FB3D0EEC07866C5 ON project');
        $this->addSql('ALTER TABLE project CHANGE timber_coating_type_id material_coating_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC8DCF529 FOREIGN KEY (material_coating_id) REFERENCES material_coating (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEC8DCF529 ON project (material_coating_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC8DCF529');
        $this->addSql('CREATE TABLE timber_coating_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, application_types LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\', product_price_field_name VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('DROP TABLE material_coating');
        $this->addSql('DROP INDEX IDX_2FB3D0EEC8DCF529 ON project');
        $this->addSql('ALTER TABLE project CHANGE material_coating_id timber_coating_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC07866C5 FOREIGN KEY (timber_coating_type_id) REFERENCES timber_coating_type (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEC07866C5 ON project (timber_coating_type_id)');
    }
}
