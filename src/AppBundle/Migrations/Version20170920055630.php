<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Railing;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170920055630 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        $railing = $em->getRepository(Railing::class)->findOneBy(array('title' => 'Frame'));

        if($railing){
            $railing->setModelData(json_decode('{"normals":[0,0,1,-1,-0,0,-0,-0,-1,1,0,-0,0,-1,0,0,1,0],"faces":[41,2,0,1,3,2,3,0,1,0,0,0,0,41,3,7,6,2,4,5,6,7,1,1,1,1,41,7,5,4,6,1,8,9,10,2,2,2,2,41,0,4,5,1,13,14,11,12,3,3,3,3,41,0,2,6,4,15,16,3,17,4,4,4,4,41,5,7,3,1,18,19,20,21,5,5,5,5],"name":"CubeGeometry","metadata":{"normals":6,"faces":6,"generator":"io_three","vertices":8,"type":"Geometry","version":3,"uvs":1},"vertices":[-0.001881,-0.35,-2.32148,-0.001882,-0.349999,2.67852,-0.351881,-0.35,-2.32148,-0.351882,-0.349999,2.67852,-0.001882,0.35,-2.32148,-0.001883,0.350001,2.67852,-0.351882,0.349999,-2.32148,-0.351883,0.350001,2.67852],"uvs":[[0.857143,1,0.714286,1,0.714286,0,0.857143,0,0.285715,1,0,1,0,0,0.285714,0,0.571429,1,0.571429,0,0.714286,0,0.285715,0,0.571429,0,0.571429,1,0.285715,1,1,0.14,0.857143,0.14,1,0,0.857143,0.14,1,0.14,1,0.28,0.857143,0.28]]}'));

            $em->persist($railing);
            $em->flush();
        }
    }
}
