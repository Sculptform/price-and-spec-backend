<?php

namespace AppBundle\Migrations\Fixtures\Version20200227090701;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * Rename "Rename Timber Look Wrap" to "Wood Finish Aluminium"
 * 
 * @see     https://woodform.atlassian.net/browse/SFM-723
 * @package AppBundle\Migrations\Fixtures\Version20191111123339
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $groups = $manager
            ->getRepository('AppBundle:MaterialFinishGroup')
            ->findBy(
                ['title' => 'Timber-look Wraps']
            );

        foreach ($groups as $group) {
            $group->setTitle('Wood Finish Aluminium');
            $manager->persist($group);
        }
        
        $manager->flush();
    }
}
