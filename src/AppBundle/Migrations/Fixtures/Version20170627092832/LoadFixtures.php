<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\Migrations\Fixtures\Version20170627092832;

use AppBundle\Entity\Material;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170627092832
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $dwgModelsDir = __DIR__ . '/Models';

        $railings = $manager->getRepository(Railing::class)->findAll();

        /** @var Railing $railing */
        foreach ($railings as $railing) {
            if($railing->getProductType()->getId() == ProductType::FINTRAX){
                $dwgPath = implode('/', [
                    $dwgModelsDir,
                    'railing',
                    '1000x12.dwg'
                ]);
            }else{
                $dwgPath = implode('/', [
                    $dwgModelsDir,
                    'railing',
                    '500x12.dwg'
                ]);
            }

            if($this->fs->exists($dwgPath)) {
                $dwgFilename = md5($dwgPath);
                $dwgFileInfo = new \SplFileInfo($dwgPath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($dwgPath, $tmpFilePath, true);

                $dwgFile = new UploadedFile(
                    $tmpFilePath,
                    $dwgFilename,
                    MimeTypeGuesser::getInstance()->guess($dwgPath),
                    $dwgFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $railing->setDwgFile($dwgFile);
            } else {
                $this->container->get('logger')->error('Railing DWG file loading error', [
                    $dwgPath
                ]);
            }
        }

        $materials = $manager->getRepository(Material::class)->findAll();

        /** @var Material $material */
        foreach ($materials as $material) {
            $shapeSize = $material->getMaterialShapeSize();
            $shape = $shapeSize->getMaterialShape();
            $materialType = $material->getMaterialType();
            $productType = $material->getProductType();

            $productTypeTitle = $productType->getTitle();
            $width = $shapeSize->getWidth();
            $depth = $shapeSize->getDepth();

            $dwgPathParts = [$dwgModelsDir, $productTypeTitle];

            if ('Concept Click' === $productTypeTitle) {
                if ('Aluminium' === $materialType->getTitle()) {
                    if ('Flute' === $shape->getTitle()) {
                        $manager->remove($material);
                        continue;
                    }

                    $dwgPathParts = array_merge($dwgPathParts, [
                        $materialType->getTitle(),
                        sprintf('%sx%s.dwg', $width, $depth)
                    ]);
                } elseif ('Timber' === $materialType->getTitle()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        $materialType->getTitle(),
                        sprintf('%sx%s.dwg', $width, $depth)
                    ]);
                } elseif ('Acoustic Blades' === $materialType->getTitle()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        $materialType->getTitle(),
                        $shape->getName(),
                        sprintf('%sx%s.dwg', $width, $depth)
                    ]);
                }
            } elseif ('Fintrax' === $productTypeTitle) {
                if ('base_stacker' === $shape->getName()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        'base',
                        sprintf('%s.dwg', $width)
                    ]);
                } elseif ('stacker' === $shape->getName()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        'stacker',
                        sprintf('%s.dwg', $width)
                    ]);
                } elseif ($shape->getParent() && 'nosing' === $shape->getParent()->getName()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        'nosing',
                        strtolower($shape->getTitle()),
                        sprintf('%s.dwg', $width)
                    ]);
                }
            } elseif ('Expression Cladding' === $productTypeTitle) {
                if (in_array($shape->getName(), ['queenscliff', 'sorrento', 'element'])) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        $shape->getName(),
                        sprintf('%sx%s.dwg', $width, $depth)
                    ]);
                } elseif ('base_element' === $shape->getName()) {
                    $dwgPathParts = array_merge($dwgPathParts, [
                        'element',
                        'base.dwg'
                    ]);
                }
            }

            $dwgPath = implode(DIRECTORY_SEPARATOR, $dwgPathParts);

            if(is_file($dwgPath)) {
                $dwgFilename = md5($dwgPath);
                $dwgFileInfo = new \SplFileInfo($dwgPath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($dwgPath, $tmpFilePath, true);

                $dwgFile = new UploadedFile(
                    $tmpFilePath,
                    $dwgFilename,
                    MimeTypeGuesser::getInstance()->guess($dwgPath),
                    $dwgFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $material->setDwgFile($dwgFile);
            } else {
                $this->container->get('logger')->error('Material DWG file loading error', [
                    $productTypeTitle,
                    $materialType->getTitle(),
                    $shape->getTitle(),
                    sprintf('%sx%s', $width, $depth),
                    $dwgPath
                ]);
            }
        }

        $manager->flush();
    }
}