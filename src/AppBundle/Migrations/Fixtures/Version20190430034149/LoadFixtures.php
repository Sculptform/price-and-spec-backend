<?php

namespace AppBundle\Migrations\Fixtures\Version20190430034149;

use AppBundle\Entity\Currency;
use AppBundle\Entity\Project;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190430034149
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $defaultCurrency = $manager->getRepository(Currency::class)->findOneBy(['code' => 'AUD']);

        if (!$defaultCurrency) {
            return;
        }

        $projects = $manager->getRepository(Project::class)->findAll();

        foreach ($projects as &$project) {
            $project->setCurrency($defaultCurrency);
        }

        $manager->flush();
    }
}
