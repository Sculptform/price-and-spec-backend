<?php

namespace AppBundle\Migrations\Fixtures\Version20190918061505;

use AppBundle\Entity\{Material, MaterialFinish, MaterialFinishGroup, Object3D, Product, ProductType, MaterialType};

use Doctrine\Common\DataFixtures\{
    AbstractFixture, FixtureInterface,
};

use Symfony\Component\DependencyInjection\{
    ContainerAwareTrait, ContainerAwareInterface
};

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190729072903
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Click-on Battens']);

        // 1
        $materialType = $manager->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Timber']);

        $materialFinishGroup = $manager->getRepository(MaterialFinishGroup::class)
            ->findOneBy(['materialType' => $materialType, 'productType' => $materialType, 'title' => 'Species']);

        $materialFinish = $manager->getRepository(MaterialFinish::class)
            ->findOneBy(['materialType' => $materialType, 'group' => $materialFinishGroup, 'title' => 'Banjo Pine']);

        foreach (['Dome 32x19', 'Dome 42x32', 'Dome 60x32'] as $title) {
            $object3D = $manager->getRepository(Object3D::class)
                ->findOneBy(['title' => $title]);

            if (!$object3D) {
                continue;
            }

            $material = $object3D = $manager->getRepository(Material::class)
                ->findOneBy([
                    'materialType' => $materialType,
                    'productType'  => $productType,
                    'object3d'     => $object3D,
                ]);

            $product = $manager->getRepository(Product::class)->findOneBy([
                'material' => $material,
                'materialFinish' => $materialFinish,
            ]);

            if (!$material or !$product) {
                continue;
            }

            if ($product) {
                $manager->remove($product);
            }
        }
        $manager->flush();
    }
}
