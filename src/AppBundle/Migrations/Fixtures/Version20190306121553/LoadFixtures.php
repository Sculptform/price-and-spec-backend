<?php

namespace AppBundle\Migrations\Fixtures\Version20190306121553;

use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190215111652
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait;

    /** @var Filesystem */
    private $filesystem;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $productTypeId = $this->productTypeId();

        $data = [
            [
                'type' => 'Timber',
                'group' => 'Species',
                'finishes' => [
                    'White Oak',
                    'Vic Ash',
                    'Spotted Gum',
                    'Pacific Teak',
                    'Blackbutt',
                    'Banjo Pine',
                ],
            ],
            [
                'type' => 'Aluminium',
                'group' => 'Real Timber Veneer',
                'finishes' => [
                    'Blackbutt',
                    'Spotted Gum',
                    'Walnut',
                    'White Oak',
                ],
            ]
        ];

        $uploadPath = __DIR__."/../../../../../web/uploads/materialShapeFinishes";
        $uploadPath = realpath($uploadPath);

        foreach ($data as $item) {
            $typeId = $this->getMaterialTypeId($item['type']);
            $groupId = $this->getMaterialGroupId($item['group'], $productTypeId);

            $finishes = $this->getMaterialFinishes($typeId, $groupId, $item['finishes']);

            foreach ($finishes as $finish) {
                $origin = __DIR__."/Finishes/{$finish['title']}.jpg";
                $target = "$uploadPath/{$finish['title']}.jpg";
                $name   = "$uploadPath/{$finish['image_path']}";

                $this->filesystem->copy($origin, $target, true);
                $this->filesystem->rename($target, $name, true);
            }
        }
    }
}
