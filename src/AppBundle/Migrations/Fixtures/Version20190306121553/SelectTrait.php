<?php

namespace AppBundle\Migrations\Fixtures\Version20190306121553;

use Doctrine\ORM\EntityManager;

/**
 * Trait RepositoryTrait
 */
trait SelectTrait
{
    public function productTypeId()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM product_type WHERE title = 'Click-on Battens'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialTypeId($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM material_type WHERE title = '{$title}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialGroupId($title, $productTypeId = 1)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */
                "SELECT id FROM material_finish_groups WHERE title = '{$title}' AND product_type_id = '{$productTypeId}'"
            )
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialFinishes($materialTypeId, $materialGroupId, array $titles = [])
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $condition = [];
        foreach ($titles as $title) {
            $condition[] = "'{$title}'";
        }

        $in = implode(', ', $condition);

        $query = /** @lang text */
            "SELECT * FROM material_finish WHERE 
                material_type_id = '{$materialTypeId}' AND 
                group_id = '{$materialGroupId}'        AND
                title IN({$in})";

        $response = $em->getConnection()->executeQuery($query)->fetchAll();

        return (count($response) > 0) ? $response : null;
    }
}
