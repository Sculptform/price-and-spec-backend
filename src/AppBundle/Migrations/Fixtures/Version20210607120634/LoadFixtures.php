<?php

namespace AppBundle\Migrations\Fixtures\Version20210607120634;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @author        Oleg Sviatchenko <oleg.sviatchenko@sibers.com>
 * @copyright (c) 2021, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20210607120634
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $em          = $manager;
        $productType = $em->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);
        $defaultMaterialFinishes = $em->getRepository(DefaultMaterialFinish::class)->findBy(['productType' => $productType]);
        foreach ($defaultMaterialFinishes as $defaultMaterialFinish) {
            $em->remove($defaultMaterialFinish);
        }

        $materials = $em->getRepository(Material::class)->findBy(['productType' => $productType]);
        foreach ($materials as $material) {
            $em->remove($material->getObject3d());
            $em->remove($material);
        }

        $projects = $em->getRepository(Project::class)->findBy(['productType' => $productType]);
        foreach ($projects as $project) {
            foreach ($project->getProjectProducts() as $projectProduct) {
                $em->remove($projectProduct);
            }
            $em->remove($project);
        }

        $materialFinishGroups = $em->getRepository(MaterialFinishGroup::class)->findBy(['productType' => $productType]);
        foreach ($materialFinishGroups as $materialFinishGroup) {
            $em->remove($materialFinishGroup);
        }

        $em->remove($productType);

        $em->flush();
    }
}
