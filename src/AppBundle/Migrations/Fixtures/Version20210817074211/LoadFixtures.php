<?php

namespace AppBundle\Migrations\Fixtures\Version20210817074211;

use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use AppBundle\Entity\SharedContent\SharedProject;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @author        Oleg Sviatchenko <oleg.sviatchenko@sibers.com>
 * @copyright (c) 2021, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20210817074211
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $em          = $manager;
        $productType = $em->getRepository(ProductType::class)->findOneBy(['title' => 'Click-on Battens']);

        $projects = $em->getRepository(Project::class)->findBy(['productType' => $productType]);
        /** @var Project $project */
        foreach ($projects as $project) {
            if ($project->getCreatedAt() >= new \DateTime('2021-08-16 10:30:00')) {
                continue;
            }
            foreach ($project->getProjectProducts() as $projectProduct) {
                $em->remove($projectProduct);
            }
            $sharedProjects = $em->getRepository(SharedProject::class)->findBy(['project' => $project]);
            foreach ($sharedProjects as $sharedProject) {
                $em->remove($sharedProject);
            }
            $em->remove($project);
        }

        $em->flush();
    }
}
