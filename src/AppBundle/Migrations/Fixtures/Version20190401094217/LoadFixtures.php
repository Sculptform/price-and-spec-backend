<?php

namespace AppBundle\Migrations\Fixtures\Version20190401094217;

use AppBundle\Entity\MaterialCoating;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        /** @var MaterialFinishGroup[] $materialFinishGroups */
        $materialFinishGroups = $manager
            ->getRepository(MaterialFinishGroup::class)
            ->findBy(['productType' => $productType]);


        foreach ($materialFinishGroups as &$materialFinishGroup) {
            $materialFinishGroup
                ->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);

            $materialFinishes = $manager
                ->getRepository(MaterialFinish::class)
                ->findBy(['group' => $materialFinishGroup]);

            foreach ($materialFinishes as &$finish) {
                $finish->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            }
        }

        $manager->flush();
    }
}
