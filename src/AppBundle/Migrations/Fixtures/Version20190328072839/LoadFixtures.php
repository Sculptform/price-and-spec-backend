<?php

namespace AppBundle\Migrations\Fixtures\Version20190328072839;

use AppBundle\Enum\ApplicationType;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        /** @var MaterialFinishGroup $materialFinishGroup */
        $materialFinishGroup = $manager
            ->getRepository(MaterialFinishGroup::class)
            ->findOneBy(['title' => 'Timber-look Wraps']);

        $materialFinishGroup->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);

        $materialFinishes = $manager
            ->getRepository(MaterialFinish::class)
            ->findBy(['group' => $materialFinishGroup]);

        foreach ($materialFinishes as &$finish) {
            $finish->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
        }

        $manager->flush();
    }
}
