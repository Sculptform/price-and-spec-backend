<?php

declare(strict_types=1);


namespace AppBundle\Migrations\Fixtures\Version20210210122232;

use AppBundle\Entity\MaterialCoating;
use AppBundle\Enum\ApplicationType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @see https://woodform.atlassian.net/browse/SFM-744
 * @package AppBundle\Migrations\Fixtures\Version20210208071258
 * @author Polvanov Igor <igor.polvanov@sibers.com>
 * @copyright 2021 (c) Sibers
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc } */
    public function load(ObjectManager $manager)
    {
        /** @var MaterialCoating $rubioCoating */
        $rubioCoating = $manager->getRepository(MaterialCoating::class)->find('4');
        $rubioCoating->setTitle('Rubio Interior Coating');
        $rubioCoating->setApplicationTypes([ApplicationType::INTERIOR]);
        $manager->flush();
    }
}