<?php

namespace AppBundle\Migrations\Fixtures\Version20190226112250;

use AppBundle\Entity\ProductType;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190215111652
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );

        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(
            ['title' => 'Aluminium']
        );

        /** @var MaterialFinishGroup $materialGroup */
        $materialGroup = $manager->getRepository(MaterialFinishGroup::class)->findOneBy([
            'productType'  => $productType,
            'materialType' => $materialType,
            'title'        => 'Timber Veneer'
        ]);

        $materialGroup->setTitle('Real Timber Veneer');

        $manager->flush();
    }
}
