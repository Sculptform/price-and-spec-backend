<?php

namespace AppBundle\Migrations\Fixtures\Version20190409121442;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190430034149
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this
            ->container
            ->get('currency.updater')
            ->update();
    }
}
