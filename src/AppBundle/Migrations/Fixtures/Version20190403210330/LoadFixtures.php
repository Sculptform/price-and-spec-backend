<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190403210330;


use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    /** @var Filesystem */
    private $filesystem;

    /**
     * @var ObjectManager
     */
    private $manager;

    protected $productTypeId;
    protected $materialTypeId;
    protected $finishToDelete;
    protected $defaultFinish;
    protected $materialFinishGroup;

    protected $finishIdList = [];
    protected $productIdList = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $productType = $this->manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Facade Blades']);

        $materialType = $this->manager->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Aluminium']);

        $this->productTypeId = $productType->getId();

        $defaultFinish = $this->manager->getRepository(
            DefaultMaterialFinish::class
        )
            ->findOneBy(
                ['productType' => $productType, 'materialType' => $materialType]
            )->getMaterialFinish();

        $this->materialTypeId = $materialType->getId();
        $this->defaultFinish  = $this->getDefaultFinish();
        $this->finishToDelete = $this->getMaterialFinishes(
            ['Interior Foil Wraps', 'Exterior Foil Wraps']
        );

        $this->removeFinishes($defaultFinish);

        if (count($this->productIdList) > 0) {
            $this->removeFromList('product', $this->productIdList);
        }

        if (count($this->finishIdList) > 0) {
            $this->removeFromList('material_finish', $this->finishIdList);
        }

        $this->removeFinishGroup('Interior Foil Wraps');
        $this->renameFinishGroup('Exterior Foil Wraps', 'Timber Look Wraps');
        $this->materialFinishGroup = $this->getFinishGroup('Timber Look Wraps');
        $this->uploadImages($manager);
        $this->parseCSV();
        $this->renameFinish($productType, $materialType);

        $materialFinishGroups = $manager
            ->getRepository(MaterialFinishGroup::class)
            ->findBy(['title' => 'Timber-look Wraps']);

        foreach ($materialFinishGroups as &$materialFinishGroup){
            $materialFinishGroup->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            $materialFinishes = $manager
                ->getRepository(MaterialFinish::class)
                ->findBy(['group' => $materialFinishGroup]);

            foreach ($materialFinishes as &$finish) {
                $finish->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            }
        }

        $manager->flush();
    }

    private function parseCSV()
    {
        if ( ! $this->materialFinishGroup) {
            $params = [
                'Version20190403210330',
                __CLASS__."::".__METHOD__."::".__LINE__,
            ];
            $this->container->get('logger')->error(
                '[Finish Group]: Invalid Exterior Foil Wraps', $params
            );

            return;
        }

        $csvFilePath = realpath(__DIR__.'/TimberLookWraps-14-02-19.csv');

        if ($this->filesystem->exists($csvFilePath)) {
            $file   = new \SplFileObject($csvFilePath);
            $reader = new CsvReader($file);
            $reader->setHeaderRowNumber(0);

            $data = [];
            foreach ($reader as $row) {
                $number      = trim($row['Item Number']);
                $description = trim($row['Item Description(Long)']);
                $weight      = (float)trim($row['Net Weight']);
                $price       = trim($row['Sculptform Price']);
                $price       = (float)str_replace('$', '', $price);
                $unit        = 'L/M';

                if ( ! $description) {
                    return;
                }

                $description = str_replace(
                    'Exterior Foil Wrap', 'Timber Look Wrap', $description
                );

                if (false !== strpos($description, 'Aluminium')) {
                    $materialFinishes = $this->getMaterialFinishes(
                        ['Timber Look Wraps']
                    );

                    foreach ($materialFinishes as $materialFinish) {
                        preg_match_all('|\d+|', $description, $sizes);
                        list($wide, $deep) = $sizes[0];

                        $object3dId = $this->getObject3DId(
                            sprintf('Facade Blade %sx%s', $deep, $wide)
                        );

                        $materialId = $this->getMaterialId($object3dId);

                        if ($materialId) {
                            $data[] = [
                                'number'           => $number,
                                'description'      => $description,
                                'materialId'       => $materialId,
                                'materialFinishId' => $materialFinish['id'],
                                'price'            => $price,
                                'weight'           => $weight,
                                'unit'             => $unit,
                            ];
                        }
                    }
                }
            }
            $this->insertProducts($data);
        }
    }

    private function uploadImages(ObjectManager $manager)
    {
        if ( ! $this->materialFinishGroup) {
            $params = [
                'Version20190403210330',
                __CLASS__."::".__METHOD__."::".__LINE__,
            ];
            $this->container->get('logger')->error(
                '[Finish Group]: Invalid Exterior Foil Wraps', $params
            );

            return;
        }

        $imagesSourcePath = $this->imagesSourcePath();
        $data             = [];
        foreach (ImageTrait::$imageNames as $name => $filename) {
            $imageSourcePath = "{$imagesSourcePath}/{$filename}";

            if ($this->filesystem->exists($imageSourcePath)) {
                $image          = $this->uploadImage($imageSourcePath);
                $materialFinish = new MaterialFinish();
                $materialFinish->setTitle($name);
                $materialFinish->setApplicationTypes(
                    [ApplicationType::EXTERIOR]
                );
                $materialFinish->setTextureFile($image);

                $manager->persist($materialFinish);
                $manager->flush();

                $data[] = $materialFinish->getId();
            } else {
                $this->container->get('logger')->error(
                    'Material finish texture file loading error',
                    [$imageSourcePath]
                );
            }
        }

        $this->updateMaterialFinish($data);
    }

    private function removeFinishes($defaultFinish)
    {
        $host              = $this->container->getParameter('host');
        $defaultTextureUrl = sprintf(
            '%s/uploads/materialShapeFinishes/%s', $host,
            $defaultFinish->getTexturePath()
        );

        if ($this->finishToDelete and count($this->finishToDelete) > 0) {
            foreach ($this->finishToDelete as $item) {
                $products = $this->getProducts($item['id']);

                foreach ($products as $product) {
                    $defaultProduct  = $this->getDefaultProduct(
                        $product['material_id'], $this->defaultFinish['id']
                    );
                    $projectProducts = $this->getProjectProducts(
                        $product['id']
                    );

                    foreach ($projectProducts as $projectProduct) {
                        $this->updateProjectProduct(
                            $projectProduct['id'], $defaultProduct['id']
                        );

                        $scene       = $this->getScene(
                            $projectProduct['project_id']
                        );
                        $sceneObject = json_decode($scene['scene'], true);

                        if ($scene and isset($sceneObject['scene']['materials'])
                            and isset($sceneObject['scene']['object']['children'])
                        ) {
                            $materials = [];
                            foreach (
                                $sceneObject['scene']['object']['children'] as &
                                $o
                            ) {
                                if ($o['productModel'] == $product['id']) {
                                    $o['productModel'] = $defaultProduct['id'];
                                }
                                if ($o['materialFinishModel'] == $item['id']) {
                                    $o['materialFinishModel']
                                                 = $this->defaultFinish['id'];
                                    $materials[] = $o['material'];
                                }
                            }

                            $maps = [];
                            foreach ($sceneObject['scene']['materials'] as &$i)
                            {
                                if (isset($i['map']) and in_array(
                                        $i['uuid'], $materials
                                    )
                                ) {
                                    $maps[] = $i['map'];
                                    unset($i['map']);
                                }
                            }

                            $images = [];
                            if (isset($sceneObject['scene']['textures'])) {
                                foreach (
                                    $sceneObject['scene']['textures'] as $key =>
                                &$i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $maps
                                        )
                                    ) {
                                        if (isset($i['image'])) {
                                            $images[] = $i['image'];
                                        }
                                        unset($i[$key]);
                                    }
                                }
                            }

                            if (isset($sceneObject['scene']['images'])) {
                                foreach (
                                    $sceneObject['scene']['images'] as $key => &
                                    $i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $images
                                        )
                                    ) {
                                        unset($i[$key]);
                                    }
                                }
                            }

                            $newImageUuid   = $this->getGUID();
                            $newTextureUuid = $this->getGUID();

                            $sceneObject['scene']['images'][] = $this->getImageBlock(
                                $newImageUuid,
                                $defaultTextureUrl
                            );

                            $sceneObject['scene']['textures'][] = $this->getTextureBlock(
                                $newTextureUuid, $newImageUuid
                            );

                            foreach ($sceneObject['scene']['materials'] as &$m) {
                                if (in_array($m['uuid'], $materials)) {
                                    $m['map'] = $newTextureUuid;
                                }
                            }

                            $this->updateScene($scene['id'], $sceneObject);
                        }
                    }
                    $this->productIdList[] = $product['id'];
                }
                $this->finishIdList[] = $item['id'];
            }
        }
    }

    protected function renameFinish($productType, $materialType)
    {
        /** @var MaterialFinishGroup $materialGroup */
        $materialGroup = $this->manager->getRepository(
            MaterialFinishGroup::class
        )->findOneBy(
            [
                'productType'  => $productType,
                'materialType' => $materialType,
                'title'        => 'Timber Look Wraps',
            ]
        );

        if ($materialGroup) {
            $materialGroup->setTitle('Timber-look Wraps');

            $materialFinish = $this->manager->getRepository(
                MaterialFinish::class
            )->findOneBy(
                [
                    'group' => $materialGroup,
                    'title' => 'Charcoal Oak',
                ]
            );

            if ($materialFinish) {
                $materialFinish->setTitle('Smoked Oak');
            }
        }
        $this->manager->flush();
    }

    private function getGUID()
    {
        mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid   = ''
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid, 12, 4).$hyphen
            .substr($charid, 16, 4).$hyphen
            .substr($charid, 20, 12);

        return $uuid;
    }

    /**
     * Returns the texture block for scene
     *
     * @param $uuid
     * @param $imageUuid
     *
     * @return array
     */
    private function getTextureBlock($uuid, $imageUuid)
    {
        return [
            'uuid'       => $uuid,
            'name'       => '',
            'mapping'    => 300,
            'repeat'     => [1, 1],
            'offset'     => [0, 0],
            'wrap'       => [1001, 1001],
            'minFilter'  => 1008,
            'magFilter'  => 1006,
            'anisotropy' => 1,
            'image'      => $imageUuid,
        ];
    }

    /**
     * Returns the image block for scene
     *
     * @param $uuid
     * @param $imageUrl
     *
     * @return array
     */
    private function getImageBlock($uuid, $imageUrl)
    {
        return [
            'uuid' => $uuid,
            'url'  => $imageUrl,
        ];
    }
}
