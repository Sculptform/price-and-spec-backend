<?php

namespace AppBundle\Migrations\Fixtures\Version20190910094442;

use Doctrine\ORM\EntityManager;

/**
 * Trait RepositoryTrait
 */
trait SelectTrait
{
    public function getProductType($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM product_type WHERE title = '{$title}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialType($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM material_type WHERE title = '{$title}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialFinishes($materialTypeId, $productTypeId, $data = [])
    {
        $where = null;
        if (count($data) > 0) {
            $condition = [];
            foreach ($data as $title) {
                $condition[] = "'{$title}'";
            }

            $where = implode(', ', $condition);
        }

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT mf.* FROM material_finish mf 
                LEFT JOIN material_finish_groups mfg ON(mf.group_id = mfg.id) 
            WHERE mfg.product_type_id = '{$productTypeId}' AND mfg.material_type_id = '{$materialTypeId}'";

        if (null !== $where) {
            $query .= "AND mfg.title IN ({$where})";
        }

        $response = $em->getConnection()->executeQuery($query)->fetchAll();

        return (count($response) > 0) ? $response : null;
    }

    public function getByProductTypeAndShape($productTypeId, $materialTypeId, $shapeTitle, $wide, $deep)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = "
            SELECT m.* FROM material m 
            INNER JOIN material_shape_size mss 
            INNER JOIN material_shape ms
            WHERE m.product_type_id = {$productTypeId}     AND
                  m.material_type_id = {$materialTypeId}   AND 
                  LOWER(ms.title) = LOWER('{$shapeTitle}') AND
                  mss.depth = {$deep}                      AND
                  mss.width = {$wide}
        ";

        $response = $em
            ->getConnection()
            ->executeQuery($query)
            ->fetch();

//        dump($response);

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialFinish($materialTypeId, $title)
    {
        $query = /** @lang text */ "SELECT * FROM material_finish WHERE material_type_id = '{$materialTypeId}' AND title = '{$title}'";

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery($query)
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getDefaultFinish()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT mf.* FROM default_material_finish dmf
                LEFT JOIN material_finish mf ON(mf.id = dmf.material_finish_id)
            WHERE dmf.product_type_id = '{$this->productTypeId}' AND dmf.material_type_id = '{$this->materialTypeId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getProducts($finishId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM product WHERE material_finish_id = '{$finishId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getProjectProducts($productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM project_product WHERE product_id = '{$productId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getExpressionCladdingProjectProducts($productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM expression_claddings WHERE base_product_id = '{$productId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getDefaultProduct($materialId, $defaultFinishId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT * FROM product WHERE material_id = '{$materialId}' AND material_finish_id = '{$defaultFinishId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getFinishGroup($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "
            SELECT * FROM material_finish_groups WHERE title = '{$title}' AND product_type_id = '{$this->productTypeId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getObject3DId($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM object3d WHERE title = '{$title}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialId($object3DId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM material WHERE object3d_id = '{$object3DId}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }
}
