<?php

namespace AppBundle\Migrations\Fixtures\Version20190910094442;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\ParameterType;

/**
 * Trait UpdateTrait
 */
trait UpdateTrait
{
    public function updateProjectProduct($id, $productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query =
            /** @lang text */
            "UPDATE project_product SET product_id = '{$productId}' WHERE id = '{$id}'";

        $em->getConnection()->executeQuery($query);
    }

    public function updateExpressionCladdingProjectProduct($id, $productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query =
            /** @lang text */
            "UPDATE expression_claddings SET base_product_id = '{$productId}' WHERE id = '{$id}'";

        $em->getConnection()->executeQuery($query);
    }

    public function updateScene($sceneId, $sceneValue)
    {
        $sceneValue = json_encode($sceneValue);

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "UPDATE scene SET scene = '{$sceneValue}' WHERE id = '{$sceneId}'";

        $em->getConnection()->executeQuery($query);
    }

    public function removeFromList($table, $data, $field = 'id')
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');
        $str = implode(', ', $data);

        $em->getConnection()->executeQuery(/** @lang text */ "DELETE FROM {$table} WHERE {$field} IN($str)");
    }

    public function removeFinishGroup($title)
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "DELETE FROM material_finish_groups WHERE product_type_id = '{$this->productTypeId}' AND title = '{$title}'";

        $em->getConnection()->executeQuery($query);
    }

    public function renameFinishGroup($oldTitle, $newTitle)
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */  "
            UPDATE material_finish_groups SET title = '{$newTitle}'  
                WHERE title = '{$oldTitle}' AND product_type_id = '{$this->productTypeId}'";

        $em->getConnection()->executeQuery($query);
    }

    public function updateMaterialFinish(array $data)
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');

        $queries = [];
        foreach ($data as $item) {
            $queries[] = /** @lang text */
                "UPDATE material_finish SET material_type_id = '{$this->materialTypeId}', group_id = '{$this->materialFinishGroup['id']}' WHERE id = '{$item}';";
        }

        $str = implode('', $queries);

        $em->getConnection()->executeQuery($str);
    }

    public function insertProducts(array $data)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $queries = [];
        foreach ($data as $product) {
            $naturalAccentPrice = ($product['naturalAccentPrice'] === null) ? "NULL" : $product['naturalAccentPrice'];
            $enviroProPrice     = ($product['enviroProPrice'] === null)     ? "NULL" : $product['enviroProPrice'];
            $cutekPrice         = ($product['cutekPrice'] === null)         ? "NULL" : $product['cutekPrice'];

            $query = /** @lang text */
                "INSERT INTO product(
                    `material_id`, 
                    `material_finish_id`, 
                    `item_number`, 
                    `item_description`, 
                    `price`, 
                    `natural_accent_price`, 
                    `enviropro_price`, 
                    `cutek_price`, 
                    `weight`, 
                    `unit`)
                     
                VALUES(
                    {$product['materialId']}, 
                    {$product['materialFinishId']}, 
                    '{$product['number']}', 
                    '{$product['description']}', 
                    {$product['price']}, 
                    {$naturalAccentPrice}, 
                    {$enviroProPrice}, 
                    {$cutekPrice}, 
                    '{$product['weight']}', 
                    '{$product['unit']}');
            ";

            $query = trim(preg_replace('/\s+/', ' ', $query));

            $queries[] = $query;
        }

//        dump($queries);

        $str = implode('', $queries);

        $em->getConnection()->executeQuery($str);
    }
}
