<?php

namespace AppBundle\Migrations\Fixtures\Version20190910094442;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Psr\Log\LoggerInterface;
use SplFileObject;
use Doctrine\ORM\EntityManager;
use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\DependencyInjection\{
    ContainerAwareInterface, ContainerAwareTrait
};
use Doctrine\Common\{DataFixtures\AbstractFixture,
    DataFixtures\FixtureInterface,
    Persistence\ObjectManager,
    Util\Inflector};

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190910094442
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait;

    /**
     * @var EntityManager
     */
    protected $em;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');

        $this->handleClickOnBattenAluminium();
        $this->handleClickOnBattenTimber();
        $this->handleFacadeBladesAluminium();
        $this->handleTongueAndGroove();
    }

    protected function handleClickOnBattenAluminium()
    {
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );

        $reader = $this->createReader('Click On Battens Aluminium.csv');
        foreach ($reader as $key => $row) {
            $number      = trim($row['Item Number']);
            $description = trim($row['Item Description(Long)']);
            $weight      = (float) trim($row['Net Weight']);
            $price       = (float) str_replace('$', '', trim($row['Sculptform Price']));
            $unit        = 'L/M';

            // $description = str_replace('Exterior Timber-look Wrap', 'Timber-look Wraps', $description);

            $normalizedDescription = str_replace(
                [
                    'Aluminium - Click-on Batten ',
                    'Premium Powdercoat - Colour -',
                    'Standard Powdercoat - Colour -',
                    'Anodised 20um (External) - Colour -',
                    'Exterior Timber-look Wrap – Colour -',
                    'Exterior Timber-look Wrap - Colour -',
                    'Veneer - Species -',
                ],
                [
                    '',
                    'Alphatec-Premium|Duratec-Premium|Precious-Premium',
                    'Duralloy-Standard',
                    'Anodising',
                    'Timber-look Wraps',
                    'Timber-look Wraps',
                    'Real Timber Veneer',
                ],
                $description
            );

            list($sizeTitle, $finishGroupTitle) = array_map('trim', explode(' - ', $normalizedDescription));

            if (false !== strpos(strtolower($normalizedDescription), 'tube')) {
                $object3d = $this->em->getRepository(Object3D::class)->findOneBy(
                    ['title' => 'Aluminium Tube 50x50']
                );
            } else {
                preg_match_all('|\d+|', $sizeTitle, $sizes);
                list($wide, $deep) = $sizes[0];

                $object3d = $this->em->getRepository(Object3D::class)->findOneBy(
                    ['title' => sprintf('Aluminium Block %sx%s', $wide, $deep)]
                );
            }

            $groups = [];
            if (false !== strpos($finishGroupTitle, '|')) {
                $finishGroupTitle = str_replace('-', ' - ', $finishGroupTitle);
                $groups = explode('|', $finishGroupTitle);
            } else if ('Duralloy-Standard' === $finishGroupTitle) {
                $groups[] = str_replace('-', ' - ', $finishGroupTitle);
            } else {
                $groups[] = $finishGroupTitle;
            }

            foreach ($groups as $item) {
                $materialFinishGroup = $this->em->getRepository(MaterialFinishGroup::class)->findOneBy(
                    ['productType' => $productType, 'title' => $item]
                );

                if (!$materialFinishGroup) {
                    continue;
                }

                $materialFinishes = $this->em->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $materialFinishGroup]
                );

                foreach ($materialFinishes as $materialFinish) {
                    $material = $this->em->getRepository(Material::class)->findOneBy(
                        ['object3d' => $object3d, 'softDeleted' => false]
                    );

                    if ( ! $material) {
                        continue;
                    }

                    $product = $this->em->getRepository(Product::class)->findOneBy([
                        'material' => $material,
                        'materialFinish' => $materialFinish,
                    ]);

                    if (!$product) {
                        $product = new Product();
                    }

                    $product
                        ->setItemNumber($number)
                        ->setItemDescription($description)
                        ->setMaterial($material)
                        ->setMaterialFinish($materialFinish)
                        ->setPrice($price)
                        ->setCutekPrice(null)
                        ->setNaturalAccentPrice(null)
                        ->setEnviroproPrice(null)
                        ->setUnit($unit)
                        ->setWeight($weight);

                    $this->em->persist($product);
                }
                $this->em->flush();
            }
        }
    }

    protected function handleClickOnBattenTimber()
    {
        /** @var LoggerInterface $logger */
        $logger = $this->container->get('monolog.logger.products');

        $logger->info('##############################################################################################');
        // ------------------------------------------------------------------------
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(
            [
                'title' => 'Click-on Battens'
            ]
        );

        $logger->info('Product Type.', [
            'Migration' => 'Version20190910094442',
            'id'        => $productType->getId(),
            'title'     => $productType->getTitle(),
        ]);
        // ------------------------------------------------------------------------
        $materialType = $this->em->getRepository(MaterialType::class)->findOneBy(
            [
                'title' => 'Timber'
            ]
        );

        $logger->info('Material Type.', [
            'Migration' => 'Version20190910094442',
            'id'        => $materialType->getId(),
            'title'     => $materialType->getTitle(),
        ]);
        // ------------------------------------------------------------------------
        $materialFinishGroup = $this->em->getRepository(MaterialFinishGroup::class)->findOneBy(
            [
                'materialType' => $materialType,
                'productType'  => $productType,
                'title'        => 'Species',
            ]
        );

        $logger->info('Material Finish Group.', [
            'Migration' => 'Version20190910094442',
            'id'        => $materialFinishGroup->getId(),
            'title'     => $materialFinishGroup->getTitle(),
        ]);
        // ------------------------------------------------------------------------

        $reader = $this->createReader('Click On Battens Timber.csv');
        foreach ($reader as $key => $row) {
            $logger->info('##############################################################################################');

            $description      = trim($row['Item Description(Long)']);
            $number           = trim($row['Item Number']);
            $price            = (float) str_replace('$', '', trim($row['Sculptform Price']));;
            $weight           = (float) $row['Net Weight'];
            $unit             = 'L/M';
            $naturalAndEnviro = (float) $row['Coating'];
            $cutekOil         = (float) $row['Oil'];

            $woodType = explode(' - ', $description)[0];
            $woodType = str_replace(['American White Oak'], ['White Oak'], $woodType);

            $normalizedDescription = str_replace(
                ['Profile: ', 'Click-on Batten', '- Set Lengths -', '(max length we can supply in set length is 3.0m)'],
                ['', '', '', ''],
                $description
            );
            list($finishTitle, $sizeTitle, $profileTitle) = array_map('trim', explode(' - ', $normalizedDescription));

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            $logger->info('CSV item.', [
                'Migration' => 'Version20190910094442',
                'number'    => $number,
                'price'     => $price,
                'finish'    => $woodType,
                'profile'   => $profileTitle,
                'size'      => "{$wide}x{$deep}",
            ]);

            $object3d = $this->em->getRepository(Object3D::class)->findOneBy(
                ['title' => sprintf('%s %sx%s', $profileTitle, $wide, $deep)]
            );

            if ( ! $object3d) {
                $logger->error('Object3D does not exist.', [
                    'Migration'      => 'Version20190910094442',
                    'Object3D TITLE' => sprintf('%s %sx%s', $profileTitle, $wide, $deep),
                ]);
                $logger->info('=======================================================');
                continue;
            }

            $material = $this->em->getRepository(Material::class)->findOneBy([
                'object3d'    => $object3d,
                'softDeleted' => false,
            ]);

            if ( ! $material) {
                $logger->error('Material does not exist.', [
                    'Migration' => 'Version20190910094442',
                    'Object3D'  => "({$object3d->getId()} - {$object3d->getTitle()})",
                ]);
                $logger->info('=======================================================');
                continue;
            }

            $materialFinish = $this->em->getRepository(MaterialFinish::class)->findOneBy([
                'title'        => $woodType,
                'group'        => $materialFinishGroup,
                'materialType' => $materialType,
            ]);

            if ( ! $materialFinish) {
                $logger->error('Material Finish does not exist.', [
                    'Migration'     => 'Version20190910094442',
                    'Object3D'      => "({$object3d->getId()} - {$object3d->getTitle()})",
                    'Material ID'   => $material->getId(),
                    'Material Type' => "({$materialType->getId()} - {$materialType->getTitle()})",
                    'Finish Group'  => "({$woodType} - {$materialFinishGroup->getId()} - {$materialFinishGroup->getTitle()})",
                ]);
                $logger->info('=======================================================');
                continue;
            }

            $product = $this->em->getRepository(Product::class)->findOneBy([
                'material' => $material,
                'materialFinish' => $materialFinish,
            ]);

            if ( ! $product) {
                $logger->warning('Product does not EXIST.', [
                    'Migration'       => 'Version20190910094442',
                    'Material ID'     => $material->getId(),
                    'Material Finish' => "({$materialType->getId()} - {$materialType->getTitle()})",
                    'Finish Group'    => "({$woodType} - {$materialFinishGroup->getId()} - {$materialFinishGroup->getTitle()})",
                ]);
                $logger->info('Create One.');

                $product = new Product();
            } else {
                $logger->info('Product EXISTS.', [
                    'Migration' => 'Version20190910094442',
                    'Product'   => "{$product->getId()} - {$product->getItemNumber()} - {$product->getPrice()}",
                ]);
                $logger->info('Change One.');
            }

            $product
                ->setItemNumber($number)
                ->setItemDescription($description)
                ->setMaterial($material)
                ->setMaterialFinish($materialFinish)
                ->setPrice($price)
                ->setNaturalAccentPrice($naturalAndEnviro)
                ->setEnviroproPrice($naturalAndEnviro)
                ->setCutekPrice($cutekOil)
                ->setUnit($unit)
                ->setWeight($weight);

            $this->em->persist($product);

            $logger->info("Save product {$number}");
            $this->em->flush();
        }
    }

    protected function handleFacadeBladesAluminium()
    {
        $obj3dRepo   = $this->em->getRepository(Object3D::class);
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        $reader = $this->createReader('Facade Blades.csv');
        foreach ($reader as $key => $row) {
            $number      = trim($row['Item Number']);
            $description = trim($row['Item Description(Long)']);
            $weight      = (float) trim($row['Net Weight']);
            $price       = (float) str_replace('$', '', trim($row['Sculptform Price']));
            $unit        = 'L/M';

            $normalizedDescription = str_replace(
                [
                    'Aluminium – Facade Blade ',
                    'Profile: ',
                    'Anodised 20um (External) - Colour -',
                    'Powdercoated Premium Colour -',
                    'Exterior Timber-look Wrap – Colour -',
                    'Exterior Timber-look Wrap - Colour -',
                ],
                [
                    '',
                    '',
                    'Anodising',
                    'Powder Coating',
                    'Timber-look Wraps',
                    'Timber-look Wraps',
                ],
                $description
            );

            list($sizeTitle, $profileTitle, $finishGroupTitle) = array_map('trim', explode(' - ', $normalizedDescription));

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            $materialFinishGroup = $this->em->getRepository(MaterialFinishGroup::class)->findOneBy(
                ['productType' => $productType, 'title' => $finishGroupTitle]
            );

            if (!$materialFinishGroup) {
                continue;
            }

            $materialFinishes = $this->em->getRepository(MaterialFinish::class)->findBy(
                ['group' => $materialFinishGroup]
            );

            foreach ($materialFinishes as $materialFinish) {
                $object3d = $obj3dRepo->findOneBy(['title' => sprintf('Facade Blade %sx%s', $deep, $wide)]);
                $material = $this->em->getRepository(Material::class)->findOneBy(
                    ['object3d' => $object3d, 'softDeleted' => false]
                );

                if ( ! $material) {
                    continue;
                }

                $product = $this->em->getRepository(Product::class)->findOneBy([
                    'material' => $material,
                    'materialFinish' => $materialFinish,
                ]);

                if (!$product) {
                    $product = new Product();
                }

                $product
                    ->setItemNumber($number)
                    ->setItemDescription($description)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setPrice($price)
                    ->setCutekPrice(null)
                    ->setNaturalAccentPrice(null)
                    ->setEnviroproPrice(null)
                    ->setUnit($unit)
                    ->setWeight($weight);

                $this->em->persist($product);
            }
            $this->em->flush();
        }
    }

    protected function handleTongueAndGroove()
    {
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Tongue & Groove Cladding']
        );

        $materialFinishes = [];
        $materialTypeTimber = $this->em->getRepository(MaterialType::class)->findOneBy(
            ['title' => 'Timber']
        );

        $materialFinishGroups = $this->em->getRepository(MaterialFinishGroup::class)->findBy(
            ['productType' => $productType]
        );

        foreach ($materialFinishGroups as $group) {
            if ( ! $group->getParent()) {
                continue;
            }
            $materialFinishes += $this->em->getRepository(MaterialFinish::class)->findBy(
                ['group' => $group, 'materialType' => $materialTypeTimber]
            );
        }

        $reader = $this->createReader('Tongue And Groove.csv');
        foreach ($reader as $row) {
            $description      = trim($row['Item Description(Long)']);
            $number           = trim($row['Item Number']);
            $price            = (float) str_replace('$', '', $row['Sculptform Price']);
            $weight           = (float) $row['Net Weight'];
            $unit             = 'L/M';
            $naturalAndEnviro = (float) $row['Coating'];
            $cutekOil         = (float) $row['Oil'];

            $normalizedDescription = str_replace(
                [
                    'Tongue & Groove Interior',
                    'Tongue & Groove Cladding',
                    'Expression Cladding',
                    'Profile: ',
                    'Set length fee included',
                    '(max length we can supply in set length is 3.0m)',
                ],
                [
                    '',
                    '',
                    '',
                    '',
                    '',
                    '',
                ],
                $description
            );

            list($finishTitle, $sizeTitle, $profileTitle) = array_map(
                'trim', explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            foreach ($materialFinishes as $materialFinish) {
                if ($this->normalizeName($materialFinish->getTitle()) === $this->normalizeName($finishTitle)) {
                    $material = $this->em->getRepository(Material::class)->getByProductTypeAndShape(
                        $productType,
                        $profileTitle,
                        $wide,
                        $deep
                    );

                    if ( ! $material) {
                        continue;
                    }

                    $product = $this->em->getRepository(Product::class)->findOneBy([
                        'material' => $material,
                        'materialFinish' => $materialFinish,
                    ]);

                    if (!$product) {
                        $product = new Product();
                    }

                    $product
                        ->setItemNumber($number)
                        ->setItemDescription($description)
                        ->setMaterial($material)
                        ->setMaterialFinish($materialFinish)
                        ->setPrice($price)
                        ->setCutekPrice($cutekOil)
                        ->setNaturalAccentPrice($naturalAndEnviro)
                        ->setEnviroproPrice($naturalAndEnviro)
                        ->setUnit($unit)
                        ->setWeight($weight);

                    $this->em->persist($product);
                }
            }
            $this->em->flush();
        }
    }

    /**
     * @param string $filename
     *
     * @return CsvReader
     */
    private function createReader($filename)
    {
        $path = __DIR__."/../../../DataFixtures/Data/Products/30-august-2019/{$filename}";

        $file = new SplFileObject(
            realpath($path)
        );

        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        return $reader;
    }

    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }
}
