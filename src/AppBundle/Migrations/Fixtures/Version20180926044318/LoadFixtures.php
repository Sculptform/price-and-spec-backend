<?php

namespace AppBundle\Migrations\Fixtures\Version20180926044318;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $object3ds = $manager->getRepository(Object3D::class)->findBy(['title' => 'Flute 60x32']);

        /**
         * @var Material $material
         */
        $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3ds[0]]);
        $material->setObject3d($object3ds[1]);

        $manager->flush();

        $productType           = $manager->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );
        $materialTypeAluminium = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']);
        $materialTypeTimber    = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);

        $file   = new \SplFileObject(__DIR__.'/../../../DataFixtures/Data/Products/Click-on Battens 17.09.18.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description      = trim($row['Item Description(Long)']);
            $itemNumber       = trim($row['Item Number']);
            $price            = (float)$row['Sculptform Price'];
            $weight           = (float)$row['Net Weight'];
            $unit             = 'L/M';
            $naturalAndEnviro = (float)$row['Coating'];
            $cutekOil         = (float)$row['Oil'];

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                ['Profile: ', 'Click-on Batten', '- Set Lengths -', '(max length we can supply in set length is 3.0m)'],
                ['', '', '', ''],
                $description
            );

            $materialFinishGroups = $manager->getRepository(MaterialFinishGroup::class)->findBy(
                ['productType' => $productType]
            );

            $aluminiumMaterialFinishes = [];
            $timberMaterialFinishes    = [];
            foreach ($materialFinishGroups as $group) {
                if ( ! $group->getParent()) {
                    continue;
                }
                $aluminiumMaterialFinishes += $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $group, 'materialType' => $materialTypeAluminium]
                );
                $timberMaterialFinishes    += $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $group, 'materialType' => $materialTypeTimber]
                );
            }

            $products = $manager->getRepository(Product::class)->findBy(['itemNumber' => $itemNumber]);

            if ( ! $products) {
                if (false !== strpos($description, 'Aluminium')) {
                    foreach ($aluminiumMaterialFinishes as $materialFinishKey => $materialFinish) {

                        /** @var Material $material */
                        $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                            ['title' => 'Aluminium Tube 50x50']
                        );
                        $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                        if (false === strpos($description, 'tube')) {
                            preg_match_all('|\d+|', $normalizedDescription, $sizes);
                            list($wide, $deep) = $sizes[0];

                            $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                                ['title' => sprintf('Aluminium Block %sx%s', $wide, $deep)]
                            );
                            $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);
                        }

                        if ($material) {
                            $item = new Product();

                            $item
                                ->setItemNumber($itemNumber)
                                ->setItemDescription($description)
                                ->setMaterial($material)
                                ->setMaterialFinish($materialFinish)
                                ->setPrice($price)
                                ->setWeight($weight)
                                ->setUnit($unit);

                            $manager->persist($item);
                        }
                    }
                } else {
                    $woodType = explode(' - ', $description)[0];

                    list($finishTitle, $sizeTitle, $profileTitle) = array_map(
                        'trim',
                        explode(' - ', $normalizedDescription)
                    );

                    preg_match_all('|\d+|', $sizeTitle, $sizes);
                    list($wide, $deep) = $sizes[0];

                    $woodType = str_replace(['American White Oak'], ['White Oak'], $woodType);


                    $materialFinish = $manager->getRepository(MaterialFinish::class)->findOneBy(
                        ['title' => $woodType, 'materialType' => $materialTypeTimber]
                    );

                    $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                        ['title' => sprintf('%s %sx%s', $profileTitle, $wide, $deep)]
                    );

                    if (false !== strpos($description, 'Flute')) {
                        //Since there are 2 Flutes 60x32 old one and new one, we need last one
                        $object3d = $manager->getRepository(Object3D::class)->findBy(
                            ['title' => sprintf('%s %sx%s', $profileTitle, $wide, $deep)]
                        );
                        $object3d = $object3d[1];
                    }

                    $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                    if ($materialFinish) {
                        if ($material) {
                            $item = new Product();
                            $item
                                ->setItemNumber($itemNumber)
                                ->setItemDescription($description)
                                ->setMaterial($material)
                                ->setMaterialFinish($materialFinish)
                                ->setPrice($price)
                                ->setNaturalAccentPrice($naturalAndEnviro)
                                ->setEnviroproPrice($naturalAndEnviro)
                                ->setCutekPrice($cutekOil)
                                ->setUnit($unit)
                                ->setWeight($weight);

                            $manager->persist($item);
                        }
                    }
                }
            } else {
                foreach ($products as $product) {
                    /**
                     * @var Product $product
                     */
                    $product->setEnviroproPrice($naturalAndEnviro);
                    $product->setNaturalAccentPrice($naturalAndEnviro);
                    $product->setCutekPrice($cutekOil);
                }
            }

            $manager->flush();
        }

        //Create default material finish for Facade Blade
        $materialFinishes = $manager->getRepository(MaterialFinish::class)->findBy(['title' => 'Shimmering Champagne' ]);
        foreach ($materialFinishes as $materialFinish) {
            if($materialFinish->getGroup()->getProductType()->getTitle() == 'Facade Blades'){
                $dFinish = new DefaultMaterialFinish();
                $dFinish->setProductType($materialFinish->getGroup()->getProductType());
                $dFinish->setMaterialType($materialTypeAluminium);
                $dFinish->setMaterialFinish($materialFinish);

                $manager->persist($dFinish);
                break;
            }
        }

        $materialShape = $manager->getRepository(MaterialShape::class)->findOneBy(['name' => 'base_stacker']);
        $materialShape->setTitle('Blade profiles');
        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalizeName(string $name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private function checkImage(string $imgPath)
    {
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Image loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}