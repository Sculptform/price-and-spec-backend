<?php

namespace AppBundle\Migrations\Fixtures\Version20170914150148;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\MaterialType;
use Doctrine\Common\Util\Inflector;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170828113915
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 */
class LoadFixtures extends AbstractAliceFixturesLoader
{
    /** @var array */
    protected $fixtures = [];

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        parent::setUp();

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var MaterialType[] $materialTypes */
        $materialTypes = $em->getRepository(MaterialType::class)->findAll();

        foreach ($materialTypes as $materialType) {
            $this->referenceRepository->addReference(
                sprintf('material_type_%s', $this->formatTitle($materialType->getTitle())),
                $materialType
            );
        }


        $this->fixtures = [
            __DIR__ . '/railingFinishesTimber.yml',
            __DIR__ . '/railingFinishesAluminium.yml',
        ];
    }

    private function formatTitle($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }
}
