<?php

namespace AppBundle\Migrations\Fixtures\Version20190524123832;

use AppBundle\Entity\Currency;
use AppBundle\Entity\Project;
use AppBundle\Entity\Railing;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190430034149
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    private static $prices = [
        'Click-on Battens'         => 30.7,
        'Facade Blades'            => 57.2,
        'Tongue & Groove Cladding' => 0,
    ];

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        /** @var Railing[] $railings */
        $railings = $manager->getRepository(Railing::class)->findAll();

        foreach ($railings as &$railing) {
            $price = static::$prices[$railing->getProductType()->getTitle()];
            $railing->setPrice($price);
        }

        $manager->flush();
    }
}
