<?php

namespace AppBundle\Migrations\Fixtures\Version20180925115311;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imageDir = __DIR__.'/../../../DataFixtures/Data/ProductTypes/';
        $images   = ['ptype_concept_click' => 'click_on_batten.png', 'ptype_facade_blade' => 'facade_blades.png', 'ptype_expression_cladding' => 'tongue_groove_cladding.png',];

        $productTypeRepo = $manager->getRepository(ProductType::class);

        $pdfHeader = $imageDir.'pdf_header_logo.png';

        $data = Yaml::parse(file_get_contents(__DIR__.'/../../../DataFixtures/ORM/productTypes.yml'));
        $mapping = ['ptype_concept_click' => 'Concept Click', 'ptype_expression_cladding' => 'Expression Cladding', 'ptype_facade_blade' => 'Facade Blades'];

        foreach ($data['AppBundle\Entity\ProductType'] as $key => $item){
            if($key == 'ptype_fintrax'){
                continue;
            }
            $productType = $productTypeRepo->findOneBy(['title' => $mapping[$key]]);

            if(! $productType){
                $productType = new ProductType();
            }

            $productType->setTitle($item['title']);
            $productType->setPopupTitle($item['popupTitle']);
            $productType->setPopupContent($item['popupContent']);
            $productType->setPopupUrl($item['popupUrl']);
            $productType->setFullTitle($item['fullTitle']);
            $productType->setDescription($item['description']);
            $productType->setDescription($item['description']);

            if($img = $this->checkImage($imageDir.$images[$key])){
                $productType->setImageFile($img);
            }

            if($pdf = $this->checkImage($pdfHeader)){
                $productType->setPdfHeaderFile($pdf);
            }

            $manager->persist($productType);
        }

        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalizeName(string $name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private function checkImage(string $imgPath){
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Material finish texture file loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}