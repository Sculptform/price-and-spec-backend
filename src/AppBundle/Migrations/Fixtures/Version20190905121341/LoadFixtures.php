<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190905121341;


use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\ProductType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190905121341
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    public function load(ObjectManager $manager)
    {
        $fixturesDir = __DIR__.'/../../../DataFixtures/ORM';
        $imageDir    = __DIR__.'/../../../DataFixtures/Data/ToolboxThumbnails';
        $dwgDir      = __DIR__.'/../../../DataFixtures/Data/DWG';

        try {
            $object3dData = Yaml::parse(file_get_contents($fixturesDir.'/object3ds.yml'))['AppBundle\Entity\Object3D'];
            $materialData = Yaml::parse(file_get_contents($fixturesDir.'/materials.yml'))['AppBundle\Entity\Material'];
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }

        $productTypeObject = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Click-on Battens']);

        $profiles = [
            'ClickOnBattens' => [
                'Timber'    => [
                    'Block' => [
                        '32x19',
                        '42x19',
                        '60x19',
                        '32x32',
                        '22x42',
                        '42x42',
                        '32x60',
                    ],
                    'Flute' => [
                        '32x19',
                        '32x32', // Because duplicate material are always created.
                        '42x19',
                        '60x19',
                    ],
                    'Dome'  => [
                        '22x42',
                        '32x19',
                        '32x32',
                        '32x42',
                        '32x60',
                        '42x32',
                        '60x32',
                    ],
                ],
                'Aluminium' => [
                    'Block' => [
                        '32x60',
                        '25x50',
                        '25x100',
                        '25x150',
                        '50x25',
                        '50x50',
                        '50x100',
                        '50x150',
                        '100x25',
                    ],
                    'Tube'  => [
                        '50x50',
                    ],
                ],
            ],
        ];

        foreach ($profiles as $productType => $productTypes) {
            foreach ($productTypes as $material => $materials) {
                foreach ($materials as $shape => $shapes) {
                    foreach ($shapes as $size) {
                        list($width, $depth) = explode('x', $size);
                        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(
                            ['title' => $material]
                        );

                        $materialName = strtolower($shape);
                        if ('Block' === $shape or 'Tube' === $shape) {
                            $materialName = strtolower($material).'_'.strtolower($shape);
                        }

                        $materialShape = $manager->getRepository(MaterialShape::class)->findOneBy(
                            ['name' => $materialName]
                        );

                        $materialShapeSize = $manager->getRepository(MaterialShapeSize::class)->findBy(
                            ['materialShape' => $materialShape, 'width' => $width, 'depth' => $depth]
                        );

                        //START of the hack to remove material shape size duplicates
                        if (count($materialShapeSize) > 1) {
                            foreach ($materialShapeSize as $s) {
                                $materialObj = $manager->getRepository(Material::class)->findOneBy(
                                    [
                                        'productType'       => $productTypeObject,
                                        'materialShapeSize' => $s,
                                        'materialType'      => $materialType,
                                    ]
                                );

                                //Make sure that we remove unused shape sizes
                                if (! $materialObj) {
                                    $manager->remove($s);
                                    $manager->flush();
                                }
                            }
                        }
                        //END of the hack


                        $materialShapeSize = $manager->getRepository(MaterialShapeSize::class)->findOneBy(
                            ['materialShape' => $materialShape, 'width' => $width, 'depth' => $depth]
                        );

                        if (! $materialShapeSize) {
                            $materialShapeSize = new MaterialShapeSize();
                            $materialShapeSize->setDepth($depth);
                            $materialShapeSize->setWidth($width);
                            $materialShapeSize->setMaterialShape($materialShape);
                        }

                        $imagePath = implode(
                            '/',
                            [
                                $imageDir,
                                $productType,
                                $material,
                                $shape == 'Tube' ? 'Aluminium Tube' : $shape,
                                sprintf('%sx%s.svg', $width, $depth),
                            ]
                        );

                        $dwgPath = implode(
                            '/',
                            [
                                $dwgDir,
                                $productType,
                                $material,
                                $shape,
                                sprintf('%sx%s.dwg', $width, $depth),
                            ]
                        );

                        if ($this->fs->exists($imagePath)) {
                            $imageFilename = md5($imagePath);
                            $imageFileInfo = new \SplFileInfo($imagePath);

                            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                            $this->fs->copy($imagePath, $tmpFilePath, true);

                            $imageFile = new UploadedFile(
                                $tmpFilePath,
                                $imageFilename,
                                MimeTypeGuesser::getInstance()->guess($imagePath),
                                $imageFileInfo->getSize(),
                                UPLOAD_ERR_OK,
                                true
                            );

                            $materialShapeSize->setImageFile($imageFile);
                        } else {
                            $this->container->get('logger')->error(
                                'Toolbox thumbnails loading error',
                                [
                                    $imagePath,
                                ]
                            );
                        }


                        $object3dKey = sprintf(
                            'object3d_%s_%s_%s',
                            strtolower($shape),
                            strtolower($material),
                            $size
                        );

                        $materialKey = sprintf(
                            'material_concept_click_%s_%s_%s',
                            strtolower($material),
                            strtolower($shape),
                            $size
                        );

                        $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                            ['title' => $object3dData[$object3dKey]['title']]
                        );

                        if (! $object3d) {
                            $object3d = new Object3D();
                        }

                        $object3d->setTitle($object3dData[$object3dKey]['title']); //Data pulled from object3d.yml file
                        $object3d->setValue($object3dData[$object3dKey]['value']); //Data pulled from object3d.yml file

                        $manager->persist($object3d);
                        $manager->persist($materialShapeSize);

                        $materialObj = $manager->getRepository(Material::class)->findOneBy(
                            [
                                'object3d'          => $object3d,
                                'productType'       => $productTypeObject,
                                'materialShapeSize' => $materialShapeSize,
                                'materialType'      => $materialType,
                            ]
                        );

                        if (! $materialObj) {
                            $materialObj = new Material();
                            $materialObj->setMaterialShapeSize($materialShapeSize);
                            $materialObj->setMaterialType($materialType);
                            $materialObj->setApplicationTypes(
                                $materialData[$materialKey]['applicationTypes']
                            ); //Data pulled from materials.yml file
                            $materialObj->setObject3d($object3d);
                            $materialObj->setProductType($productTypeObject);
                            $materialObj->setOrderWeight(
                                $materialData[$materialKey]['orderWeight']
                            ); //Data pulled from materials.yml file

                            $softDeleted = false;
                            if (array_key_exists('softDeleted', $materialData[$materialKey])) {
                                $softDeleted = $materialData[$materialKey]['softDeleted'];
                            }

                            $materialObj->setSoftDeleted($softDeleted);
                        }

                        $manager->persist($materialObj);

                        if ($this->fs->exists($dwgPath)) {
                            $dwgFilename = md5($dwgPath);
                            $dwgFileInfo = new \SplFileInfo($dwgPath);

                            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                            $this->fs->copy($dwgPath, $tmpFilePath, true);

                            $dwgFile = new UploadedFile(
                                $tmpFilePath,
                                $dwgFilename,
                                MimeTypeGuesser::getInstance()->guess($dwgPath),
                                $dwgFileInfo->getSize(),
                                UPLOAD_ERR_OK,
                                true
                            );

                            $materialObj->setDwgFile($dwgFile);
                        } else {
                            $this->container->get('logger')->error(
                                'DWG loading error',
                                [
                                    $dwgPath,
                                ]
                            );
                        }
                    }
                }
            }
        }

        $manager->flush();
    }
}
