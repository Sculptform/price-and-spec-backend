<?php

namespace AppBundle\Migrations\Fixtures\Version20200527050101;

use AppBundle\Entity\MaterialCoating;
use AppBundle\Entity\MaterialType;
use AppBundle\Enum\ApplicationType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20200527050101
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        /** @var MaterialType $materialType */
        $materialType =  $manager
            ->getRepository('AppBundle:MaterialType')
            ->findOneBy(['title' => 'Timber']);

        $coating = new MaterialCoating();
        $coating->setMaterialType($materialType);
        $coating->setTitle('Rubio Interior / Exterior Coating');
        $coating->setDescription('Some Description');
        $coating->setProductPriceFieldName('enviropro_price');
        $coating->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
        $coating->setProductWholesalePriceFieldName('enviropro_wholesale_price');
        $coating->setProductRetailPriceFieldName('enviropro_retail_price');

        $manager->persist($coating);

        $manager->flush();
    }
}
