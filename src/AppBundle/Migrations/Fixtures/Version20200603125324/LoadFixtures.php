<?php

namespace AppBundle\Migrations\Fixtures\Version20200603125324;

use AppBundle\Entity\MaterialFinish;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20200603125324
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var \Doctrine\DBAL\Connection $connection */
        $connection = $em->getConnection();

        /** @var MaterialFinish $burntAsh */
        $burntAsh = $em
            ->getRepository('AppBundle:MaterialFinish')
            ->findOneBy(['title' => 'Burnt Ash']);

        //SELECT * FROM `expression_claddings` WHERE `base_finish_id` IN(5, 247) OR `stacker_finish_id` IN(5, 247)

        $stmt = $connection
            ->query("
                SELECT project_id FROM expression_claddings 
                    WHERE 
                        base_finish_id = {$burntAsh->getId()} 
                    OR 
                        stacker_finish_id = {$burntAsh->getId()}");

        $projects = array_values(
            array_unique(
                array_column($stmt->fetchAll(), 'project_id')
            )
        );

        $pIn = '"' . implode('", "', $projects) . '"';

        $connection->executeQuery("DELETE FROM project_filters WHERE project_id IN($pIn)");
        $connection->executeQuery("DELETE FROM expression_claddings WHERE base_finish_id = {$burntAsh->getId()} OR stacker_finish_id = {$burntAsh->getId()}");
        $connection->executeQuery("DELETE FROM product WHERE material_finish_id = {$burntAsh->getId()}");
        $connection->executeQuery("DELETE FROM project WHERE id IN($pIn)");
        $connection->executeQuery("DELETE FROM material_finish WHERE title = 'Burnt Ash'");
    }
}
