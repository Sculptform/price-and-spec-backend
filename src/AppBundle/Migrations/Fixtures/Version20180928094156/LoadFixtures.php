<?php

namespace AppBundle\Migrations\Fixtures\Version20180928094156;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $dwgModelsDir = __DIR__.'/';
        $productType  = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Click-on Battens']);
        $materials    = $manager->getRepository(Material::class)->findBy(['productType' => $productType]);
        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']);
        $products     = $manager->getRepository(Product::class)->findAll();

        foreach ($products as $product) {
            /**
             * @var Product $product
             */
            if ($product->getMaterial()->getMaterialType() == 'Aluminium'
                && $product->getMaterial()->getProductType() == 'Click-on Battens') {
                $manager->remove($product);
            }
        }

        $manager->flush();

        $file   = new \SplFileObject(__DIR__.'/../../../DataFixtures/Data/Products/Click-on Battens 17.09.18.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description      = trim($row['Item Description(Long)']);
            $itemNumber       = trim($row['Item Number']);
            $price            = (float)$row['Sculptform Price'];
            $weight           = (float)$row['Net Weight'];
            $unit             = 'L/M';
            $naturalAndEnviro = (float)$row['Coating'];
            $cutekOil         = (float)$row['Oil'];

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                ['Profile: ', 'Click-on Batten', '- Set Lengths -', '(max length we can supply in set length is 3.0m)'],
                ['', '', '', ''],
                $description
            );

            if (false === strpos($description, 'Aluminium')) {
                continue;
            }

            list($materialTitle, $sizeTitle, $finishTitle) = array_map(
                'trim',
                explode(' - ', $normalizedDescription)
            );

            $normalizedFinish = str_replace(
                ['Interior Foil Wrap', 'Exterior Foil Wrap', 'Veneer'],
                ['Interior Foil Wraps', 'Exterior Foil Wraps', 'Timber Veneer'],
                $finishTitle
            );


            if (false !== strpos($normalizedFinish, 'Anodised')) {
                $materialCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                    ['productType' => $productType, 'materialType' => $materialType, 'title' => 'Anodising']
                );

                $materialFinish = $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $materialCategory]
                );

                $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                    ['title' => 'Aluminium Tube 50x50']
                );

                if (false === strpos($sizeTitle, 'tube')) {
                    preg_match_all('|\d+|', $sizeTitle, $sizes);
                    list($wide, $deep) = $sizes[0];
                    $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                        ['title' => sprintf('Aluminium Block %sx%s', $wide, $deep)]
                    );
                }

                $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                foreach ($materialFinish as $child) {
                    if ($material) {
                        $item = new Product();
                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($child)
                            ->setPrice($price)
                            ->setNaturalAccentPrice($naturalAndEnviro)
                            ->setEnviroproPrice($naturalAndEnviro)
                            ->setCutekPrice($cutekOil)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);
                    }
                }
            } else {
                if (false !== strpos($normalizedFinish, 'Powdercoat')) {

                    $type = explode(' ', $normalizedFinish)[0];

                    $coating = [
                        'Premium'  => [
                            'Alphatec',
                            'Duratec',
                            'Precious',
                        ],
                        'Standard' => ['Duralloy'],
                    ];

                    foreach ($coating[$type] as $coat) {
                        $coatTitle        = sprintf('%s - %s', $coat, $type);
                        $materialCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                            ['productType' => $productType, 'materialType' => $materialType, 'title' => $coatTitle]
                        );

                        $materialFinish = $manager->getRepository(MaterialFinish::class)->findBy(
                            ['group' => $materialCategory]
                        );

                        $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                            ['title' => 'Aluminium Tube 50x50']
                        );

                        if (false === strpos($sizeTitle, 'tube')) {
                            preg_match_all('|\d+|', $sizeTitle, $sizes);
                            list($wide, $deep) = $sizes[0];
                            $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                                ['title' => sprintf('Aluminium Block %sx%s', $wide, $deep)]
                            );
                        }

                        $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                        foreach ($materialFinish as $child) {
                            if ($material) {
                                $item = new Product();
                                $item
                                    ->setItemNumber($itemNumber)
                                    ->setItemDescription($description)
                                    ->setMaterial($material)
                                    ->setMaterialFinish($child)
                                    ->setPrice($price)
                                    ->setNaturalAccentPrice($naturalAndEnviro)
                                    ->setEnviroproPrice($naturalAndEnviro)
                                    ->setCutekPrice($cutekOil)
                                    ->setUnit($unit)
                                    ->setWeight($weight);

                                $manager->persist($item);
                            }
                        }
                    }
                }

                $materialCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                    ['productType' => $productType, 'materialType' => $materialType, 'title' => $normalizedFinish]
                );

                $materialFinish = $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $materialCategory]
                );

                $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                    ['title' => 'Aluminium Tube 50x50']
                );

                if (false === strpos($sizeTitle, 'tube')) {
                    preg_match_all('|\d+|', $sizeTitle, $sizes);
                    list($wide, $deep) = $sizes[0];
                    $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                        ['title' => sprintf('Aluminium Block %sx%s', $wide, $deep)]
                    );
                }

                $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                foreach ($materialFinish as $child) {
                    if ($material) {
                        $item = new Product();
                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($child)
                            ->setPrice($price)
                            ->setNaturalAccentPrice($naturalAndEnviro)
                            ->setEnviroproPrice($naturalAndEnviro)
                            ->setCutekPrice($cutekOil)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);
                    }
                }
            }
        }

        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private
    function normalizeName(
        string $name
    ) {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private
    function checkImage(
        string $imgPath
    ) {
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Image loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}