<?php

namespace AppBundle\Migrations\Fixtures\Version20190918074333;

use AppBundle\Entity\{Material, MaterialFinish, MaterialFinishGroup, Object3D, Product, ProductType, MaterialType};

use Doctrine\Common\DataFixtures\{
    AbstractFixture, FixtureInterface,
};

use Symfony\Component\DependencyInjection\{
    ContainerAwareTrait, ContainerAwareInterface
};

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190918074333
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var ObjectManager */
    protected $manager;

    /** @var ProductType */
    protected $clickOnBattenType;

    /** @var MaterialType */
    protected $timberMaterialType;

    /** @var MaterialType */
    protected $aluminiumMaterialType;

    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;
        $this->clickOnBattenType  = $manager->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );
//        $this->timberMaterialType = $manager->getRepository(MaterialType::class)->findOneBy(
//            ['title' => 'Timber']
//        );
        $this->aluminiumMaterialType = $manager->getRepository(MaterialType::class)->findOneBy(
            ['title' => 'Aluminium']
        );

        //$this->makeFluteMaterialsInteriorOnly(['Flute 32x19', 'Flute 42x19', 'Flute 60x19']);
        $this->changeApplicationTypes('Timber-look Wraps',  [1, 2]);
        $this->changeApplicationTypes('Real Timber Veneer', [1]);
    }

    protected function changeApplicationTypes($group, $applicationTypes = [1, 2])
    {
        /** @var MaterialFinishGroup $finishGroup */
        $finishGroup = $this
            ->manager
            ->getRepository(MaterialFinishGroup::class)
            ->findOneBy([
                'materialType' => $this->aluminiumMaterialType,
                'productType'  => $this->clickOnBattenType,
                'title'        => $group,
            ]);

        if ( ! $finishGroup) {
            return;
        }

        $finishGroup->setApplicationTypes($applicationTypes);

        $materialFinishes = $this
            ->manager
            ->getRepository(MaterialFinish::class)
            ->findBy([
                'materialType' => $this->aluminiumMaterialType,
                'group'        => $finishGroup,
            ]);

        foreach ($materialFinishes as $finish) {
            $finish->setApplicationTypes($applicationTypes);
        }
        $this->manager->flush();
    }
    
    protected function makeFluteMaterialsInteriorOnly($flutes)
    {
        foreach ($flutes as $title) {
            $object3D = $this->manager->getRepository(Object3D::class)->findOneBy(['title' => $title]);

            if (!$object3D) {
                continue;
            }

            $material = $object3D = $this->manager->getRepository(Material::class)
                ->findOneBy([
                    'materialType' => $this->timberMaterialType,
                    'productType'  => $this->clickOnBattenType,
                    'object3d'     => $object3D,
                ]);

            if ( ! $material) {
                continue;
            }

            $material->setApplicationTypes([1]);

//            $materialFinishes = $this
//                ->manager
//                ->getRepository(MaterialFinish::class)
//                ->findBy([
//                    'materialType' => $this->timberMaterialType,
//                    'group'        => $finishGroup,
//                ]);
//
//            foreach ($materialFinishes as $finish) {
//                $finish->setApplicationTypes([1]);
//            }
        }
        $this->manager->flush();
    }
}
