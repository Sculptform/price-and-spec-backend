<?php

namespace AppBundle\Migrations\Fixtures\Version20200526222106;

use AppBundle\Entity\Project;
use AppBundle\Entity\Currency;
use AppBundle\Util\PriceCalculator;
use AppBundle\Entity\AcousticBacking;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * Acoustic Backing.
 * Change the price of the Group 1 option pricing.
 *
 * @see     https://woodform.atlassian.net/browse/SFM-729
 * @package AppBundle\Migrations\Fixtures\Version20200526222106
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var EntityManagerInterface
     */
    protected $em;

    /**
     * @var array
     */
    protected $update = [];

    /**
     * @var Currency
     */
    protected $audCurrency;

    public function init()
    {
        $this->em = $this->container->get('doctrine.orm.entity_manager');

        $this->audCurrency = $this->em->getRepository('AppBundle:Currency')->findOneBy([
            'code' => 'AUD'
        ]);
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->init();

        $groupOneRated = $this->getAcousticBaking('Group 1 Rated');
        $groupOneRated->setPrice(72.70);
        $this->em->flush();

        $projects = $this->getProjects(
            $this->getAcousticBaking('Group 3 Rated - Standard')
        );

        $this->recalculateProjects($projects, $groupOneRated);

        $projects = $this->getProjects($groupOneRated);

        $this->recalculateProjects($projects);
    }

    /**
     * @param string $title
     *
     * @return AcousticBacking|object|null
     */
    protected function getAcousticBaking($title)
    {
        return $this
            ->em
            ->getRepository('AppBundle:AcousticBacking')
            ->findOneBy(['title' => $title]); // Group 3 Rated - Standard
    }

    /**
     * @param AcousticBacking $acousticBacking
     *
     * @return Project[]|object[]
     */
    protected function getProjects(AcousticBacking $acousticBacking)
    {
        return $this
            ->em
            ->getRepository('AppBundle:Project')
            ->findBy([
                'outdated'        => false,
                'acousticBacking' => $acousticBacking,
            ]);
    }

    protected function recalculateProjects(array $projects, $acousticBaking = null)
    {
        $calculator = new PriceCalculator();

        foreach ($projects as $project) {
            if ($acousticBaking) {
                $project->setAcousticBacking($acousticBaking);
            }

            $price = $calculator->calculate($project);
            $project->setPrice($price);

            if ( ! $project->getCurrency()) {
                $project->setCurrency($this->audCurrency);
            }

            if ( ! $project->getPricingCategory()) {
                $project->setPricingCategory('100-300');
            }
        }

        $this->em->flush();
    }
}
