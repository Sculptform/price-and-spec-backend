<?php

namespace AppBundle\Migrations\Fixtures\Version20190409121444;

use AppBundle\Entity\Project;
use AppBundle\Entity\ProductType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190409121444
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        $projects = $manager->getRepository(Project::class)->findBy(['productType' => $productType]);

        foreach ($projects as &$project) {
            $project->setApplicationType(null);
        }

        $manager->flush();
    }
}
