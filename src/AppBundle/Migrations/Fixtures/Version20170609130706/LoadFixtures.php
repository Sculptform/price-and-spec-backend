<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\Migrations\Fixtures\Version20170609130706;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\MaterialType;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170609130706
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadFixtures extends AbstractAliceFixturesLoader
{
    /** @var array */
    protected $fixtures = [];

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        parent::setUp();

        $em = $this->getContainer()->get('doctrine')->getManager();

        $materialTypeRepository = $em->getRepository(MaterialType::class);

        $materialTypeAluminium = $materialTypeRepository->findOneBy(['title' => 'Aluminium']);

        if ($materialTypeAluminium) {
            $this->referenceRepository->addReference(
                'material_type_aluminium',
                $materialTypeAluminium
            );

            $this->fixtures = [
                __DIR__ . '/materialFinishGroups.yml',
                __DIR__ . '/materialFinishesAluminium.yml'
            ];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }
}
