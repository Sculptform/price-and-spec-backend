<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2018, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20181206110122;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\ProductType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2018, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20181206110122
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $orders        = [
            'Click-on Battens'         => [
                'Timber'    => [
                    'Block' => [
                        '32x60' => 1,
                        '22x42' => 2,
                        '42x42' => 3,
                        '32x32' => 4,
                        '42x19' => 5,
                        '60x19' => 6,
                    ],
                    'Flute' => ['32x32' => 7, '60x32' => 8],
                    'Dome'  => ['42x32' => 9, '60x32' => 10],
                ],
                'Aluminium' => [
                    'Block'          => [
                        '50x50'  => 11,
                        '50x100' => 12,
                        '50x150' => 13,
                        '25x50'  => 14,
                        '25x100' => 15,
                        '25x150' => 16,
                        '100x25' => 17,
                    ],
                    'Aluminium Tube' => ['50x50' => 18],
                ],
            ],
            'Tongue & Groove Cladding' => [
                'Element' => [
                    '32x32' => 13,
                    '32x42' => 14,
                    '32x60' => 15
                ],
                'Sorrento'    => [
                    '68x19' => 1, '68x32' => 2, '88x19' => 3, '138x19' => 4,
                ],
                'Queenscliff' => [
                    '32x32'  => 1,
                    '32x42'  => 2,
                    '32x60'  => 3,
                    '68x19'  => 4,
                    '88x19'  => 5,
                    '138x19' => 6,
                    '68x26'  => 7,
                    '88x26'  => 8,
                    '138x26' => 9,
                    '68x32'  => 10,
                    '88x32'  => 11,
                    '138x32' => 12,
                ],
            ],
            'Facade Blades'            => [
                'Blade profiles' => [
                    '50x150' => 1, '50x200' => 2, '50x300' => 3,
                ],

            ],
        ];
        $thumbnailsDir = __DIR__.'/ToolboxThumbnails';

        $shapeSizes = $manager->getRepository(MaterialShapeSize::class)
            ->findAll();

        /** @var MaterialShapeSize $shapeSize */
        foreach ($shapeSizes as &$shapeSize) {
            $shape = $shapeSize->getMaterialShape();

            /** @var Material $material */
            foreach ($shapeSize->getMaterials() as &$material) {
                $materialType = $material->getMaterialType();
                $productType  = $material->getProductType();

                $imagePath = '';

                if ('Click-on Battens' === $productType->getTitle()) {
                    $imagePath = implode(
                        '/',
                        [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            $materialType->getTitle(),
                            $shape->getTitle(),
                            sprintf(
                                '%sx%s.svg', $shapeSize->getWidth(),
                                $shapeSize->getDepth()
                            ),
                        ]
                    );

                    $material->setOrderWeight(
                        $orders[$productType->getTitle(
                        )][$material->getMaterialType()->getTitle(
                        )][$shape->getTitle()][sprintf(
                            '%sx%s', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        )]
                    );
                } elseif ('Tongue & Groove Cladding' === $productType->getTitle(
                    )
                ) {
                    if ('Base' === $shape->getTitle()) {
                        continue;
                    }

                    if ($shapeSize->getWidth() == 188
                        && $shapeSize->getDepth() == 19
                        && $shape->getTitle() == 'Queenscliff'
                    ) {
                        $ext = 'png';
                    } else {
                        $ext = 'svg';
                    }

                    if (in_array(
                        $shape->getName(),
                        ['base_element', 'vaucluse', 'whitsunday']
                    )
                    ) {
                        continue;
                    }

                    $imagePath = implode(
                        '/',
                        [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            $shape->getTitle(),
                            sprintf(
                                '%sx%s.%s', $shapeSize->getWidth(),
                                $shapeSize->getDepth(), $ext
                            ),
                        ]
                    );

                    $material->setOrderWeight(
                        $orders[$productType->getTitle()][$shape->getTitle(
                        )][sprintf(
                            '%sx%s', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        )]
                    );
                } elseif ('Facade Blades' === $productType->getTitle()) {
                    $imagePath = implode(
                        '/',
                        [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            $shape->getTitle(),
                            sprintf(
                                '%sx%s.svg', $shapeSize->getWidth(),
                                $shapeSize->getDepth()
                            ),
                        ]
                    );

                    $material->setOrderWeight(
                        $orders[$productType->getTitle()][$shape->getTitle(
                        )][sprintf(
                            '%sx%s', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        )]
                    );
                } elseif ('Fintrax' == $productType->getTitle()) {
                    continue;
                } else {
                    $this->container->get('logger')->error(
                        'Toolbox thumbnails loading error',
                        [
                            $productType->getTitle(),
                            $materialType->getTitle(),
                            $shape->getTitle(),
                            sprintf(
                                '%sx%s.svg', $shapeSize->getWidth(),
                                $shapeSize->getDepth()
                            ),
                        ]
                    );
                }

                if ($this->fs->exists($imagePath)) {
                    $imageFilename = md5($imagePath);
                    $imageFileInfo = new \SplFileInfo($imagePath);

                    $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                    $this->fs->copy($imagePath, $tmpFilePath, true);

                    $imageFile = new UploadedFile(
                        $tmpFilePath,
                        $imageFilename,
                        MimeTypeGuesser::getInstance()->guess($imagePath),
                        $imageFileInfo->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $shapeSize->setImageFile($imageFile);
                } else {
                    $this->container->get('logger')->error(
                        'Toolbox thumbnails loading error',
                        [
                            $imagePath,
                        ]
                    );
                }
            }
        }

        $manager->flush();
    }
}
