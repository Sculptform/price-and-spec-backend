<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\Migrations\Fixtures\Version20170614111541;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\ProductType;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170614111541
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadFixtures extends AbstractAliceFixturesLoader
{
    /** @var array */
    protected $fixtures = [];

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        parent::setUp();

        $em = $this->getContainer()->get('doctrine')->getManager();

        $productTypeCC = $em->find(ProductType::class, 1);

        if ($productTypeCC) {
            $this->referenceRepository->addReference('ptype_concept_click', $productTypeCC);

            $this->fixtures = [
                __DIR__ . '/materialTypes.yml',
                __DIR__ . '/materialShapes.yml',
                __DIR__ . '/materialShapeSizes.yml',
                __DIR__ . '/object3ds.yml',
                __DIR__ . '/materials.yml',
            ];
        }
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }
}
