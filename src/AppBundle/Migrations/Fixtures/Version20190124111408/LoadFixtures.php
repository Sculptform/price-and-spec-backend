<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190124111408;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Ddeboer\DataImport\Reader\CsvReader;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190115084537
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $file   = new \SplFileObject(
            __DIR__.'/price_and_spec_csv_updated_14.12.18.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        $headers = $reader->getColumnHeaders();

        $headers[0] = 'Item Number';

        $reader->setColumnHeaders($headers);

        foreach ($reader as $row) {
            $itemNumber = trim($row['Item Number']);
            $weight     = (float)$row['Net Weight'];

            $products = $manager->getRepository(Product::class)->findBy(
                ['itemNumber' => $itemNumber]
            );

            if ($products) {
                foreach ($products as $item) {
                    $item->setWeight($weight);
                }
            } else {
                $this->container->get('logger')->error(
                    'Product not found!',
                    [
                        $itemNumber,
                    ]
                );
            }
        }

        $manager->flush();

        $this->reduceYakisugiDuplicates($manager);
    }

    private function reduceYakisugiDuplicates(ObjectManager $manager)
    {
        $file   = new \SplFileObject(
            __DIR__.'/yakisugi_edited_s_and_q.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        $headers = $reader->getColumnHeaders();

        $headers[0] = 'Item Number';

        $reader->setColumnHeaders($headers);

        foreach ($reader as $row) {
            $itemNumber = trim($row['Item Number']);

            $products = $manager->getRepository(Product::class)->findBy(
                ['itemNumber' => substr($itemNumber, 0, -1)]
            );

            foreach ($products as &$item) {
                if (false !== strpos(
                        $item->getItemDescription(), 'Queenscliff'
                    )
                ) {
                    if (substr($item->getItemNumber(), -1) != 'q') {
                        $item->setItemNumber($item->getItemNumber().'q');
                    }
                } elseif (false !== strpos(
                        $item->getItemDescription(), 'Sorrento'
                    )
                ) {
                    if (substr($item->getItemNumber(), -1) != 's') {
                        $item->setItemNumber($item->getItemNumber().'s');
                    }
                };
            }

            if ( ! $products) {
                $this->container->get('logger')->error(
                    'Duplicate not found!',
                    [
                        $itemNumber,
                    ]
                );
            }
        }

        $manager->flush();

    }

}
