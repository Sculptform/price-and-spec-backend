<?php

namespace AppBundle\Migrations\Fixtures\Version20190215111652;

use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

/**
 * Class ImageTrait
 */
trait ImageTrait
{
    public static $imageNames = [
        'Charcoal Oak'        => 'Charcoal Oak.jpg',
        'Chocolate Oak'       => 'Chocolate Oak.jpg',
        'Greywash Oak'        => 'Greywash Oak.jpg',
        'Spotted Gum'         => 'Spotted Gum.jpg',
        'Walnut'              => 'Walnut.jpg',
        'White Oak'           => 'White Oak.jpg',
        'Whitewash Blackbutt' => 'Whitewash Blackbutt.jpg',
        'Whitewash Oak'       => 'Whitewash Oak.jpg',
    ];

    public function imagesSourcePath()
    {
        return realpath(__DIR__.'/../../../DataFixtures/Data/Finishes/CCL/Aluminium/Timber Look Wraps/');
    }

    public function createImage($tmpImagePath, $filename, $imageSourcePath, \SplFileInfo $fileInfo)
    {
        return new UploadedFile(
            $tmpImagePath,
            $filename,
            MimeTypeGuesser::getInstance()->guess($imageSourcePath),
            $fileInfo->getSize(),
            UPLOAD_ERR_OK,
            true
        );
    }

    public function uploadImage($imageSourcePath)
    {
        $filename     = md5($imageSourcePath);
        $fileInfo     = new \SplFileInfo($imageSourcePath);
        $tmpImagePath = $this->filesystem->tempnam('/tmp', 'uploaded-file-');

        $this
            ->filesystem
            ->copy($imageSourcePath, $tmpImagePath, true);

        return $this->createImage($tmpImagePath, $filename, $imageSourcePath, $fileInfo);
    }
}
