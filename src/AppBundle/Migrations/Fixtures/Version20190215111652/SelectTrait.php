<?php

namespace AppBundle\Migrations\Fixtures\Version20190215111652;

use Doctrine\ORM\EntityManager;

/**
 * Trait RepositoryTrait
 */
trait SelectTrait
{
    public function getProductType()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM product_type WHERE title = 'Click-on Battens'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialType()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM material_type WHERE title = 'Aluminium'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialFinishes($data)
    {
        $condition = [];
        foreach ($data as $title) {
            $condition[] = "'{$title}'";
        }

        $where = implode(', ', $condition);

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT mf.* FROM material_finish mf 
                LEFT JOIN material_finish_groups mfg ON(mf.group_id = mfg.id) 
            WHERE mfg.product_type_id = '{$this->productTypeId}' AND mfg.material_type_id = '{$this->materialTypeId}' 
                AND mfg.title IN ({$where})";

        $response = $em->getConnection()->executeQuery($query)->fetchAll();

        return (count($response) > 0) ? $response : null;
    }

    public function getDefaultFinish()
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT mf.* FROM default_material_finish dmf
                LEFT JOIN material_finish mf ON(mf.id = dmf.material_finish_id)
            WHERE dmf.product_type_id = '{$this->productTypeId}' AND dmf.material_type_id = '{$this->materialTypeId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getProducts($finishId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM product WHERE material_finish_id = '{$finishId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getProjectProducts($productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM project_product WHERE product_id = '{$productId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getExpressionCladdingProjectProducts($productId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM expression_claddings WHERE base_product_id = '{$productId}'";

        return $em->getConnection()->executeQuery($query)->fetchAll();
    }

    public function getDefaultProduct($materialId, $defaultFinishId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */
            "SELECT * FROM product WHERE material_id = '{$materialId}' AND material_finish_id = '{$defaultFinishId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getScene($projectId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "SELECT * FROM scene WHERE project_id = '{$projectId}' ORDER by created_at ASC LIMIT 1";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getFinishGroup($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $query = /** @lang text */ "
            SELECT * FROM material_finish_groups WHERE title = '{$title}' AND product_type_id = '{$this->productTypeId}'";

        return $em->getConnection()->executeQuery($query)->fetch();
    }

    public function getObject3DId($title)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM object3d WHERE title = '{$title}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }

    public function getMaterialId($object3DId)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()
            ->executeQuery(/** @lang text */ "SELECT id FROM material WHERE object3d_id = '{$object3DId}'")
            ->fetch();

        return ($response) ? (int) $response['id'] : null;
    }
}
