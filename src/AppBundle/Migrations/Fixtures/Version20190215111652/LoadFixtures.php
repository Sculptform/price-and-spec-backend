<?php

namespace AppBundle\Migrations\Fixtures\Version20190215111652;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\MaterialType;
use AppBundle\Enum\ApplicationType;
use AppBundle\Entity\MaterialFinish;
use Ddeboer\DataImport\Reader\CsvReader;
use AppBundle\Entity\MaterialFinishGroup;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190215111652
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    /** @var Filesystem */
    private $filesystem;

    protected $productTypeId;
    protected $materialTypeId;
    protected $finishToDelete;
    protected $defaultFinish;
    protected $materialFinishGroup;

    protected $finishIdList = [];
    protected $productIdList = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->productTypeId  = $this->getProductType();
        $this->materialTypeId = $this->getMaterialType();
        $this->defaultFinish  = $this->getDefaultFinish();
        $this->finishToDelete = $this->getMaterialFinishes(
            ['Interior Foil Wraps', 'Exterior Foil Wraps']
        );

        $this->removeFinishes();

        if (count($this->productIdList) > 0) {
            $this->removeFromList('product', $this->productIdList);
        }

        if (count($this->finishIdList) > 0) {
            $this->removeFromList('material_finish', $this->finishIdList);
        }

        $this->removeFinishGroup('Interior Foil Wraps');
        $this->renameFinishGroup('Exterior Foil Wraps', 'Timber Look Wraps');
        $this->materialFinishGroup = $this->getFinishGroup('Timber Look Wraps');
        $this->uploadImages($manager);
        $this->parseCSV();
    }

    private function parseCSV()
    {
        if ( ! $this->materialFinishGroup) {
            $params = [
                'Version20190215111652', __CLASS__."::".__METHOD__."::".__LINE__,
            ];
            $this->container->get('logger')->error(
                '[Finish Group]: Invalid Exterior Foil Wraps', $params
            );

            return;
        }

        $csvFilePath = realpath(__DIR__.'/TimberLookWraps-14-02-19.csv');

        if ($this->filesystem->exists($csvFilePath)) {
            $file   = new \SplFileObject($csvFilePath);
            $reader = new CsvReader($file);
            $reader->setHeaderRowNumber(0);

            $data = [];
            foreach ($reader as $row) {
                $number      = trim($row['Item Number']);
                $description = trim($row['Item Description(Long)']);
                $weight      = (float)trim($row['Net Weight']);
                $price       = trim($row['Sculptform Price']);
                $price       = (float)str_replace('$', '', $price);
                $unit        = 'L/M';

                if ( ! $description) {
                    return;
                }

                $description = str_replace(
                    'Interior Foil Wrap', 'Timber Look Wrap', $description
                );

                if (false !== strpos($description, 'Aluminium')) {
                    $materialFinishes = $this->getMaterialFinishes(
                        ['Timber Look Wraps']
                    );

                    foreach ($materialFinishes as $materialFinish) {
                        if (false === strpos($description, 'tube')) {
                            preg_match_all('|\d+|', $description, $sizes);
                            list($wide, $deep) = $sizes[0];

                            $object3dId = $this->getObject3DId(
                                sprintf('Aluminium Block %sx%s', $wide, $deep)
                            );
                        } else {
                            $object3dId = $this->getObject3DId(
                                'Aluminium Tube 50x50'
                            );
                        }

                        $materialId = $this->getMaterialId($object3dId);

                        if ($materialId) {
                            $data[] = [
                                'number'           => $number,
                                'description'      => $description,
                                'materialId'       => $materialId,
                                'materialFinishId' => $materialFinish['id'],
                                'price'            => $price,
                                'weight'           => $weight,
                                'unit'             => $unit,
                            ];
                        }
                    }
                }
            }
            $this->insertProducts($data);
        }
    }

    private function uploadImages(ObjectManager $manager)
    {
        if ( ! $this->materialFinishGroup) {
            $params = [
                'Version20190215111652', __CLASS__."::".__METHOD__."::".__LINE__,
            ];
            $this->container->get('logger')->error(
                '[Finish Group]: Invalid Exterior Foil Wraps', $params
            );

            return;
        }

        $imagesSourcePath = $this->imagesSourcePath();
        $data             = [];
        foreach (ImageTrait::$imageNames as $name => $filename) {
            $imageSourcePath = "{$imagesSourcePath}/{$filename}";

            if ($this->filesystem->exists($imageSourcePath)) {
                $image          = $this->uploadImage($imageSourcePath);
                $materialFinish = new MaterialFinish();
                $materialFinish->setTitle($name);
                $materialFinish->setApplicationTypes(
                    [ApplicationType::EXTERIOR]
                );
                $materialFinish->setTextureFile($image);

                $manager->persist($materialFinish);
                $manager->flush();

                $data[] = $materialFinish->getId();
            } else {
                $this->container->get('logger')->error(
                    'Material finish texture file loading error',
                    [$imageSourcePath]
                );
            }
        }

        $this->updateMaterialFinish($data);
    }

    private function removeFinishes()
    {
        // $defaultTextureURL = $this->getDefaultTextureURL();

        if ($this->finishToDelete and count($this->finishToDelete) > 0) {
            foreach ($this->finishToDelete as $item) {
                $products = $this->getProducts($item['id']);

                foreach ($products as $product) {
                    $defaultProduct  = $this->getDefaultProduct(
                        $product['material_id'], $this->defaultFinish['id']
                    );
                    $projectProducts = $this->getProjectProducts(
                        $product['id']
                    );

                    foreach ($projectProducts as $projectProduct) {
                        $this->updateProjectProduct(
                            $projectProduct['id'], $defaultProduct['id']
                        );

                        $scene       = $this->getScene(
                            $projectProduct['project_id']
                        );
                        $sceneObject = json_decode($scene['scene'], true);

                        if ($scene and isset($sceneObject['scene']['materials'])
                            and isset($sceneObject['scene']['object']['children'])
                        ) {
                            $materials = [];
                            foreach (
                                $sceneObject['scene']['object']['children'] as &
                                $o
                            ) {
                                if ($o['productModel'] == $product['id']) {
                                    $o['productModel'] = $defaultProduct['id'];
                                }
                                if ($o['materialFinishModel'] == $item['id']) {
                                    $o['materialFinishModel']
                                                 = $this->defaultFinish['id'];
                                    $materials[] = $o['material'];
                                }
                            }

                            foreach ($sceneObject['scene']['materials'] as &$i)
                            {
                                if (in_array($i['uuid'], $materials)) {
                                    $i['color'] = hexdec(
                                        $this->defaultFinish['color']
                                    );
                                }
                            }

                            $maps = [];
                            foreach ($sceneObject['scene']['materials'] as &$i)
                            {
                                if (isset($i['map']) and in_array(
                                        $i['uuid'], $materials
                                    )
                                ) {
                                    $maps[] = $i['map'];
                                    unset($i['map']);
                                }
                            }

                            $images = [];
                            if (isset($sceneObject['scene']['textures'])) {
                                foreach (
                                    $sceneObject['scene']['textures'] as $key =>
                                &$i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $maps
                                        )
                                    ) {
                                        if (isset($i['image'])) {
                                            $images[] = $i['image'];
                                        }
                                        unset($i[$key]);
                                    }
                                }
                            }

                            if (isset($sceneObject['scene']['images'])) {
                                foreach (
                                    $sceneObject['scene']['images'] as $key => &
                                    $i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $images
                                        )
                                    ) {
                                        unset($i[$key]);
                                    }
                                }
                            }

                            $this->updateScene($scene['id'], $sceneObject);
                        }
                    }
                    $this->productIdList[] = $product['id'];
                }
                $this->finishIdList[] = $item['id'];
            }
        }
    }

//    private function buildTextureURL($imagePath)
//    {
//        $host = $this->container->getParameter('host');
//
//        if ($imagePath) {
//            return "{$host}/uploads/materialShapeFinishes/{$imagePath}";
//        }
//
//        return null;
//    }
//
//    private function getDefaultTextureURL()
//    {
//        $host = $this->container->getParameter('host');
//
//        if ($this->defaultFinish['image_path']) {
//            return "{$host}/uploads/materialShapeFinishes/{$this->defaultFinish['image_path']}";
//        }
//
//        return null;
//    }
}
