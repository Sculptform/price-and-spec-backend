<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190709114744;


use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190709114744
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    public function load(ObjectManager $manager)
    {
        $thumbnailsDir = __DIR__
            .'/../../../DataFixtures/Data/ToolboxThumbnails';

        $shapeSizes = $manager->getRepository(MaterialShapeSize::class)
            ->findAll();

        /** @var MaterialShapeSize $shapeSize */
        foreach ($shapeSizes as $shapeSize) {
            $shape = $shapeSize->getMaterialShape();

            /** @var Material $material */
            foreach ($shapeSize->getMaterials() as $material) {
                $materialType = $material->getMaterialType();
                $productType  = $material->getProductType();

                $titleMap = ['Click-on Battens' => 'ClickOnBattens'];

                if ('Click-on Battens' !== $productType->getTitle()
                    || 'Aluminium' !== $materialType->getTitle()
                ) {
                    continue;
                }

                $imagePath = implode(
                    '/', [
                        $thumbnailsDir,
                        $titleMap[$productType->getTitle()],
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.svg',
                            $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]
                );

                if ($this->fs->exists($imagePath)) {
                    $imageFilename = md5($imagePath);
                    $imageFileInfo = new \SplFileInfo($imagePath);

                    $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                    $this->fs->copy($imagePath, $tmpFilePath, true);

                    $imageFile = new UploadedFile(
                        $tmpFilePath,
                        $imageFilename,
                        MimeTypeGuesser::getInstance()->guess($imagePath),
                        $imageFileInfo->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $shapeSize->setImageFile($imageFile);
                } else {
                    $this->container->get('logger')->error(
                        'Toolbox thumbnails loading error', [$imagePath]
                    );
                }
            }
        }

        $manager->flush();
    }
}
