<?php
namespace AppBundle\Migrations\Fixtures\Version20190917130417;

use AppBundle\Entity\Material;
use AppBundle\Entity\Object3D;
use Psr\Log\LoggerInterface;
use Symfony\Component\Yaml\Yaml;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190905121341
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    public function load(ObjectManager $manager)
    {
        $fixturesDir = __DIR__.'/../../../DataFixtures/ORM';

        $object3dData = [];
        $materialData = [];
        try {
            $object3dData = Yaml::parse(file_get_contents($fixturesDir.'/object3ds.yml'))['AppBundle\Entity\Object3D'];
            $materialData = Yaml::parse(file_get_contents($fixturesDir.'/materials.yml'))['AppBundle\Entity\Material'];
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }

        /** @var LoggerInterface $logger */
        $logger = $this->container->get('monolog.logger.products');

        $productTypeObject = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Click-on Battens']);

        $profiles = [
            'ClickOnBattens' => [
                'Timber'    => [
                    'Block' => [
                        '32x19',
                        '42x19',
                        '60x19',
                        '32x32',
                        '22x42',
                        '42x42',
                        '32x60',
                    ],
                    'Dome'  => [
                        '32x19',
                        '32x32',
                        '42x32',
                        '60x32',
                        '22x42',
                        '32x42',
                        '32x60',
                    ],
                    'Flute' => [
                        '32x19',
                        '42x19',
                        '60x19',
                        '32x32',
                        '60x32',
                    ],
                ],
                'Aluminium' => [
                    'Block' => [
                        '50x50',
                        '50x100',
                        '50x150',
                        '25x50',
                        '25x100',
                        '25x150',
                        '50x25',
                        '100x25',
                        '32x60',
                    ],
                    'Tube' => [
                        '50x50',
                    ]
                ],
            ],
        ];

        foreach ($profiles as $productType => $productTypes) {
            foreach ($productTypes as $material => $materials) {
                foreach ($materials as $shape => $shapes) {
                    foreach ($shapes as $size) {
                        list($width, $depth) = explode('x', $size);

                        $materialName = strtolower($shape);
                        if ('Block' === $shape or 'Tube' === $shape) {
                            $materialName = strtolower($material).'_'.strtolower($shape);
                        }

                        $materialShape     = $manager->getRepository(MaterialShape::class)->findOneBy(
                            ['name' => $materialName]
                        );

                        if ( ! $materialShape) {
                            $logger->warning("MaterialShape doesn't exist", [
                                "MaterialShape Name: {$materialName} [{$width}x{$depth}]",
                            ]);
                            continue;
                        }

                        $materialShapeSizes = $manager->getRepository(MaterialShapeSize::class)->findBy(
                            ['materialShape' => $materialShape, 'width' => $width, 'depth' => $depth]
                        );

                        foreach ($materialShapeSizes as $materialShapeSize) {
                            if ( ! $materialShapeSize) {
                                $logger->warning("MaterialShapeSize doesn't exist", [
                                    "MaterialShape ID: {$materialShape->getId()}",
                                    "MaterialShape Name: {$materialName}",
                                    "[{$width}x{$depth}]",
                                ]);
                                continue;
                            }

                            $object3dKey = sprintf('object3d_%s_%s_%s', strtolower($shape), strtolower($material), $size);
                            $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                                ['title' => $object3dData[$object3dKey]['title']]
                            );

                            if ( ! $object3d) {
                                $logger->warning("Object3D doesn't exist", [
                                    $object3dData[$object3dKey]['title']
                                ]);
                                continue;
                            }

                            $materialObj = $manager->getRepository(Material::class)->findOneBy(
                                [
                                    'object3d'          => $object3d,
                                    'productType'       => $productTypeObject,
                                    'materialShapeSize' => $materialShapeSize,
                                ]
                            );

                            if ($materialObj) {
                                $materialKey = sprintf('material_concept_click_%s_%s_%s', strtolower($material), strtolower($shape), $size);
                                $softDeleted = false;

                                $logger->warning("Material: {$materialKey} - Order: {$materialData[$materialKey]['orderWeight']}");

                                $materialObj->setOrderWeight($materialData[$materialKey]['orderWeight']); //Data pulled from materials.yml file
                                if (array_key_exists('softDeleted', $materialData[$materialKey])) {
                                    $softDeleted = $materialData[$materialKey]['softDeleted'];
                                }

                                $materialObj->setSoftDeleted($softDeleted);
                                $materialObj->setApplicationTypes($materialData[$materialKey]['applicationTypes']);
                            }
                        }
                        $manager->flush();
                    }
                }
            }
        }
    }
}
