<?php

namespace AppBundle\Migrations\Fixtures\Version20210630071649;

use AppBundle\Entity\Product;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use SplFileObject;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @see     https://woodform.atlassian.net/browse/SFM-752
 * @package AppBundle\Migrations\Fixtures\Version20210630071649
 * @author  Oleg Sviatchenko <oleg.sviatchenko@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        foreach (['2._TGC_pricing_09.06.21_AWO.csv', '3._COB_pricing_09.06.21_AWO.csv'] as $file) {
            $this->container->get('logger')->info(sprintf('Update %s file', $file));
            $reader = $this->createReader($file);
            foreach ($reader as $row) {
                $number   = trim($row['Item Number']);
                $price    = (float) str_replace('$', '', trim($row['P & S Price']));
                $cutekPrice         = (float) trim($row['Clear Oil']);
                $naturalAccentPrice = (float) trim($row['Clear Poly']);
                $enviroproPrice     = (float) trim($row['Rubio/Enviropro']);
                $products = $manager->getRepository('AppBundle:Product')
                    ->findByItemNumber($number);
                /** @var Product $product */
                foreach ($products as $product) {
                    $this->container->get('logger')->info(sprintf('IN: %s - Old Price %s - New Price %s%s', $number, $product->getPrice(), $price, PHP_EOL));
                    $product->setPrice($price);
                    $product->setCutekPrice($cutekPrice);
                    $product->setNaturalAccentPrice($naturalAccentPrice);
                    $product->setEnviroproPrice($enviroproPrice);
                }
                $manager->flush();
                unset($products);
            }
        }

        $manager->flush();
    }


    /**
     * @param string $filename
     *
     * @return CsvReader
     */
    private function createReader($filename)
    {
        $path = __DIR__."/../../../DataFixtures/Data/Products/20210629/{$filename}";

        $file = new SplFileObject(
            realpath($path)
        );

        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        return $reader;
    }
}
