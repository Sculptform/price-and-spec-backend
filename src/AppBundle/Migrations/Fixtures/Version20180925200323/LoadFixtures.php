<?php

namespace AppBundle\Migrations\Fixtures\Version20180925200323;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imageDir = __DIR__.'/../../../DataFixtures/Data/ToolboxThumbnails/ClickOnBattens/';

        $images = [
            'ptype_concept_click'       => 'click_on_batten.png',
            'ptype_facade_blade'        => 'facade_blades.png',
            'ptype_expression_cladding' => 'tongue_groove_cladding.png',
        ];

        $sizes = [
            'Timber'    =>
                [
                    'block' => ['32x60', '22x42', '42x42', '32x32', '42x19', '60x19'],
                    'flute' => ['32x32', '60x32'],
                    'dome'  => ['42x32', '60x32'],
                ],
            'Aluminium' => [
                'block' => ['50x50', '50x100', '50x150', '100x25', '25x50', '25x100', '25x150'],
                'tube'  => ['50x50'],
            ],
        ];

        $obj3dRepo             = $manager->getRepository(Object3D::class);
        $materialTypeAluminium = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']);
        $materialTypeTimber    = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);
        $productType           = $manager->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );

        $finishG = ['Anodising', 'Exterior Foil Wraps', 'Powder Coating'];

        $obj3dData               = Yaml::parse(file_get_contents(__DIR__.'/../../../DataFixtures/ORM/object3ds.yml'));
        $materialFinishData      = Yaml::parse(
            file_get_contents(__DIR__.'/../../../DataFixtures/ORM/materialFinishesFBAluminium.yml')
        );
        $materialFinishGroupData = Yaml::parse(
            file_get_contents(__DIR__.'/../../../DataFixtures/ORM/materialFinishGroups.yml')
        );


        foreach ($sizes['Aluminium']['block'] as $block) {
            $obj3d = new Object3D();
            $obj3d->setTitle($obj3dData['AppBundle\Entity\Object3D']['object3d_block_aluminium_'.$block]['title']);
            $obj3d->setValue($obj3dData['AppBundle\Entity\Object3D']['object3d_block_aluminium_'.$block]['value']);

            $manager->persist($obj3d);
            $manager->flush();
        }

        foreach ($sizes['Aluminium']['tube'] as $tube) {
            $obj3d = new Object3D();
            $obj3d->setTitle($obj3dData['AppBundle\Entity\Object3D']['object3d_tube_aluminium_'.$tube]['title']);
            $obj3d->setValue($obj3dData['AppBundle\Entity\Object3D']['object3d_tube_aluminium_'.$tube]['value']);

            $manager->persist($obj3d);
            $manager->flush();
        }

        foreach ($sizes['Timber'] as $key => $item) {
            foreach ($item as $subitem) {
                $obj3d = new Object3D();
                $obj3d->setTitle(
                    $obj3dData['AppBundle\Entity\Object3D']['object3d_'.$key.'_timber_'.$subitem]['title']
                );
                $obj3d->setValue(
                    $obj3dData['AppBundle\Entity\Object3D']['object3d_'.$key.'_timber_'.$subitem]['value']
                );
                $manager->persist($obj3d);
                $manager->flush();
            }
        }

        /**
         * @var MaterialShape $materialShape
         */
        $materialTubeShape = new MaterialShape();
        $materialTubeShape->setTitle('Aluminium Tube');
        $materialTubeShape->setName('aluminium_tube');

        $manager->persist($materialTubeShape);
        $manager->flush();

        $materials = $manager->getRepository(Material::class)->findBy(['productType' => $productType]);

        foreach ($materials as $item) {
            $manager->remove($item);
        }

        $manager->flush();

        foreach ($sizes as $materialTypeKey => $materialType) {
            foreach ($materialType as $shapeKey => $shape) {
                if ($shapeKey == 'block') {
                    $materialShape = $manager->getRepository(MaterialShape::class)->findOneBy(
                        ['name' => strtolower($materialTypeKey).'_block']
                    );
                } elseif ($shapeKey == 'tube') {
                    $materialShape = $materialTubeShape;
                } else {
                    $materialShape = $manager->getRepository(MaterialShape::class)->findOneBy(['name' => $shapeKey]);
                }

                foreach ($shape as $size) {
                    $m = 'materialType'.$materialTypeKey;
                    list($width, $depth) = explode('x', $size);
                    $materialShapeSize = new MaterialShapeSize();
                    $materialShapeSize->setDepth($depth);
                    $materialShapeSize->setWidth($width);
                    $materialShapeSize->setMaterialShape($materialShape);
                    if ($shapeKey == 'tube') {
                        $path = $imageDir.$materialTypeKey.'/Aluminium Tube/'.$size.'.png';
                    } else {
                        $path = $imageDir.$materialTypeKey.'/'.ucfirst($shapeKey).'/'.$size.'.png';
                    }
                    if ($img = $this->checkImage($path)) {
                        $materialShapeSize->setImageFile($img);
                    }

                    $manager->persist($materialShapeSize);

                    if ($materialTypeKey == 'Aluminium') {
                        $title    = sprintf('Aluminium %s %sx%s', ucfirst($shapeKey), $width, $depth);
                        $object3d = $obj3dRepo->findOneBy(
                            ['title' => $title]
                        );
                    } else {
                        $title    = sprintf('%s %sx%s', ucfirst($shapeKey), $width, $depth);
                        $object3d = $obj3dRepo->findOneBy(
                            ['title' => $title]
                        );
                    }

                    if ($object3d) {
                        $material = new Material();
                        $material->setObject3d($object3d);
                        $material->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
                        $material->setMaterialShapeSize($materialShapeSize);
                        $material->setMaterialType($$m);
                        $material->setProductType($productType);

                        $manager->persist($material);

                        $manager->flush();
                    }
                }
            }
        }

        $file   = new \SplFileObject(__DIR__.'/../../../DataFixtures/Data/Products/Click-on Battens 17.09.18.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description(Long)']);
            $itemNumber  = trim($row['Item Number']);
            $price       = (float)$row['Sculptform Price'];
            $weight      = (float)$row['Net Weight'];
            $unit        = 'L/M';

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                ['Profile: ', 'Click-on Batten', '- Set Lengths -', '(max length we can supply in set length is 3.0m)'],
                ['', '', '', ''],
                $description
            );

            $materialFinishGroups = $manager->getRepository(MaterialFinishGroup::class)->findBy(
                ['productType' => $productType]
            );

            $aluminiumMaterialFinishes = [];
            $timberMaterialFinishes    = [];
            foreach ($materialFinishGroups as $group) {
                if ( ! $group->getParent()) {
                    continue;
                }
                $aluminiumMaterialFinishes += $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $group, 'materialType' => $materialTypeAluminium]
                );
                $timberMaterialFinishes    += $manager->getRepository(MaterialFinish::class)->findBy(
                    ['group' => $group, 'materialType' => $materialTypeTimber]
                );
            }


            if (false !== strpos($description, 'Aluminium')) {
                foreach ($aluminiumMaterialFinishes as $materialFinishKey => $materialFinish) {

                    /** @var Material $material */
                    $object3d = $obj3dRepo->findOneBy(['title' => 'Aluminium Tube 50x50']);
                    $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);

                    if (false === strpos($description, 'tube')) {
                        preg_match_all('|\d+|', $normalizedDescription, $sizes);
                        list($wide, $deep) = $sizes[0];

                        $object3d = $obj3dRepo->findOneBy(['title' => sprintf('Aluminium Block %sx%s', $deep, $wide)]);
                        $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);
                    }

                    if ($material) {
                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setWeight($weight)
                            ->setUnit($unit);

                        $manager->persist($item);
                    }
                }
            } else {
                $woodType = explode(' - ', $description)[0];

                list($finishTitle, $sizeTitle, $profileTitle) = array_map(
                    'trim',
                    explode(' - ', $normalizedDescription)
                );

                preg_match_all('|\d+|', $sizeTitle, $sizes);
                list($wide, $deep) = $sizes[0];

                $woodType = str_replace(['American White Oak'], ['White Oak'], $woodType);


                $materialFinish = $manager->getRepository(MaterialFinish::class)->findOneBy(
                    ['title' => $woodType, 'materialType' => $materialTypeTimber]
                );

                $object3d = $obj3dRepo->findOneBy(['title' => sprintf('%s %sx%s', $profileTitle, $deep, $wide)]);
                $material = $manager->getRepository(Material::class)->findOneBy(['object3d' => $object3d]);


                if ($materialFinish) {
                    if ($material) {
                        $item = new Product();
                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setNaturalAccentPrice(0)
                            ->setEnviroproPrice(0)
                            ->setCutekPrice(0)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);
                    }
                }
            }
        }
        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalizeName(string $name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private function checkImage(string $imgPath)
    {
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Image loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}