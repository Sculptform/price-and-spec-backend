<?php

namespace AppBundle\Migrations\Fixtures\Version20190726112724;

use Ddeboer\DataImport\Reader\CsvReader;
use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Persistence\ObjectManager;

use Doctrine\Common\DataFixtures\ {
    AbstractFixture,
    FixtureInterface,
};

use Symfony\Component\DependencyInjection\ {
    ContainerAwareTrait,
    ContainerAwareInterface,
};

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190726112724
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait;

    /** @var Filesystem */
    private $filesystem;

    protected $productTypeId;
    protected $materialTypeId;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->filesystem = new Filesystem();
    }

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->productTypeId  = $this->getProductType();
        $this->materialTypeId = $this->getMaterialType();

        $this->parseCSV();
    }

    private function parseCSV()
    {
        $csvFilePath = realpath(__DIR__.'/dome_availability_in_banjo_pine.csv');

        if ( ! $this->filesystem->exists($csvFilePath)) {
            $params = [
                'Version20190726112724', __CLASS__."::".__METHOD__."::".__LINE__,
            ];
            $this->container->get('logger')->error(
                '[CSV]: File does not exist.', $params
            );

            return;
        }

        $materialFinish = $this->getMaterialFinish('Species', 'Banjo Pine');

        $file   = new \SplFileObject($csvFilePath);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        $data = [];
        foreach ($reader as $row) {
            $number      = trim($row['Item Number']);
            $description = trim($row['Item Description(Long)']);
            $weight      = (float)trim($row['Net Weight']);
            $price       = trim($row['Sculptform Price']);
            $price       = (float)str_replace('$', '', $price);
            $unit        = 'L/M';

            if ( ! $description) {
                return;
            }

            preg_match_all('|\d+|', $description, $sizes);
            list($wide, $deep) = $sizes[0];

            $object3dId = $this->getObject3DId(
                sprintf('Dome %sx%s', $wide, $deep)
            );

            $materialId = $this->getMaterialId($object3dId);

            if ($materialId) {
                $data[] = [
                    'number'           => $number,
                    'description'      => $description,
                    'materialId'       => $materialId,
                    'materialFinishId' => $materialFinish['id'],
                    'price'            => $price,
                    'weight'           => $weight,
                    'unit'             => $unit,
                ];
            }
        }

        $this->insertProducts($data);
    }
}
