<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190326095741;


use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Enum\ApplicationType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        /**
         * @var MaterialFinishGroup $materialFinishGroup
         */
        $materialFinishGroup = $manager->getRepository(
            MaterialFinishGroup::class
        )->findOneBy(['title' => 'Alphatec - Premium']);
        $materialFinishGroup->setApplicationTypes([ApplicationType::INTERIOR]);

        $materialFinishes = $manager->getRepository(MaterialFinish::class)
            ->findBy(['group' => $materialFinishGroup]);

        foreach ($materialFinishes as &$finish) {
            $finish->setApplicationTypes([ApplicationType::INTERIOR]);
        }

        $manager->flush();
    }
}
