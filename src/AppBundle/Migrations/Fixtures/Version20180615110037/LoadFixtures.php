<?php

namespace AppBundle\Migrations\Fixtures\Version20180615110037;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imageDir = __DIR__ . '/../../../DataFixtures/Data/Finishes/ECL/Timber/Yakisugi Charred Species/';
        $images = ['Yakisugi Pine - Full Char' => 'Yakisugi Pine Full Char.jpg'];

        $materialfinishCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'Yakisugi Charred Species']);
        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);

        foreach ($images as $key => $img) {

            $materialFinish = new MaterialFinish();
            $imgPath = $imageDir . $img;

            $materialFinish->setTitle($key);
            $materialFinish->setMaterialType($materialType);
            $materialFinish->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            $materialFinish->setGroup($materialfinishCategory);

            if ($this->fs->exists($imgPath)) {
                $filename = md5($imgPath);
                $fileInfo = new \SplFileInfo($imgPath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($imgPath, $tmpFilePath, true);

                $file = new UploadedFile(
                    $tmpFilePath,
                    $filename,
                    MimeTypeGuesser::getInstance()->guess($imgPath),
                    $fileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $materialFinish->setTextureFile($file);
            } else {
                $this->container->get('logger')->error('Material finish texture file loading error', [
                    $imgPath
                ]);
            }
            $manager->persist($materialFinish);
            $manager->flush();
        }

        $this->loadECLFromCSV($manager);
    }

    private function loadECLFromCSV(ObjectManager $manager)
    {
        $materialFinishCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'Yakisugi Charred Species']);
        $materialFinishItems = $materialFinishCategory->getMaterialFinishes();
        $productType = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Expression Cladding']);
        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);
        $finishMap = [
            'Charred Burnt Ash Full Char' => 'Yakisugi Ash - Full Char',
            'Charred Burnt Ash Burnt and Brushed' => 'Yakisugi Ash - Burnt and Brushed',
            'Charred Pine Flame Lick' => 'Yakisugi Pine - Flame Licked',
            'Charred Pine Full Char' => 'Yakisugi Pine - Full Char'
        ];

        $count = 0;

        $file = new \SplFileObject(__DIR__ . '/../../../DataFixtures/Data/Products/Yakisugi - 19.07.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description']);
            $itemNumber = trim($row['Item Number']);
            $price = (float)$row['Sculptform Price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice = (float)$row['Cutek'];
            $weight = (float)$row['Weights'];
            $unit = trim($row['Default Unit of Measure']);

            if (!$description) {
                return;
            }

            $normalizedDescription = str_replace(
                ['Profile: ', 'Expression Cladding ', 'Yakisugi'],
                ['', '', 'Charred'],
                $description
            );

            list($finishTitle_1, $sizeTitle, $profileTitle, $finishTitle_2) = array_map('trim', explode(' - ', $normalizedDescription));

            $finishTitle = $finishTitle_1.' '.$finishTitle_2;

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            $count++;

            /**
             * @var $obj3D Object3D
             */
            $obj3D = $manager->getRepository(Object3D::class)->findOneBy(['title' => sprintf('%s %sx%s', $profileTitle, $wide, $deep)]);

            /**
             * @var $material Material
             */
            $material   = $manager->getRepository(Material::class)->findOneBy(['object3d' => $obj3D, 'materialType' => $materialType, 'productType' => $productType]);
            $itemFinish = $manager->getRepository(MaterialFinish::class)->findOneBy(['title' => $finishMap[$finishTitle]]);


            if ($material) {
                $item = new Product();

                $item
                    ->setItemNumber($itemNumber)
                    ->setItemDescription($description)
                    ->setMaterial($material)
                    ->setMaterialFinish($itemFinish)
                    ->setPrice($price)
                    ->setCutekPrice($cutekPrice)
                    ->setNaturalAccentPrice($enviroproPrice)
                    ->setEnviroproPrice($enviroproPrice)
                    ->setUnit($unit)
                    ->setWeight($weight);

                $manager->persist($item);
                $manager->flush();
            }
        }

        $manager->flush();
    }

    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }
}