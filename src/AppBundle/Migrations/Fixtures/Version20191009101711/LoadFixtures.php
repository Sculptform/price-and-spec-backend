<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20191009101711;

use AppBundle\Entity\Object3D;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190905121341
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    public function load(ObjectManager $manager)
    {
        $fixturesDir = __DIR__.'/../../../DataFixtures/ORM';

        try {
            $object3dData = Yaml::parse(file_get_contents($fixturesDir.'/object3ds.yml'))['AppBundle\Entity\Object3D'];
        } catch (ParseException $e) {
            printf("Unable to parse the YAML string: %s", $e->getMessage());
        }

        $profiles = [
            'ClickOnBattens' => [
                'Timber'    => [
                    'Block' => [
                        '32x32',
                        '42x42'
                    ],
                    'Flute' => [
                        '32x19',
                        '32x32'
                    ]
                ]
            ]
        ];

        foreach ($profiles as $productType => $productTypes) {
            foreach ($productTypes as $material => $materials) {
                foreach ($materials as $shape => $shapes) {
                    foreach ($shapes as $size) {
                        $object3dKey = sprintf(
                            'object3d_%s_%s_%s',
                            strtolower($shape),
                            strtolower($material),
                            $size
                        );

                        $object3d = $manager->getRepository(Object3D::class)->findOneBy(
                            ['title' => $object3dData[$object3dKey]['title']]
                        );

                        if (! $object3d) {
                            $object3d = new Object3D();
                        }

                        $object3d->setTitle($object3dData[$object3dKey]['title']); //Data pulled from object3d.yml file
                        $object3d->setValue($object3dData[$object3dKey]['value']); //Data pulled from object3d.yml file

                        $manager->persist($object3d);
                    }
                }
            }
        }

        $manager->flush();
    }
}
