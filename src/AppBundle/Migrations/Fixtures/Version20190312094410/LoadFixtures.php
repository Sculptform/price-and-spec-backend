<?php

namespace AppBundle\Migrations\Fixtures\Version20190312094410;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190215111652
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $productTypes = ['Click-on Battens', 'Facade Blades'];

        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(
            ['title' => 'Aluminium']
        );

        foreach ($productTypes as $type) {
            $productType = $manager->getRepository(ProductType::class)
                ->findOneBy(
                    ['title' => $type]
                );

            $this->renameFinish($productType, $materialType);
        }
    }

    protected function renameFinish($productType, $materialType)
    {
        /** @var MaterialFinishGroup $materialGroup */
        $materialGroup = $this->manager->getRepository(
            MaterialFinishGroup::class
        )->findOneBy(
            [
                'productType'  => $productType,
                'materialType' => $materialType,
                'title'        => 'Timber Look Wraps',
            ]
        );

        if ($materialGroup) {
            $materialGroup->setTitle('Timber-look Wraps');

            $materialFinish = $this->manager->getRepository(
                MaterialFinish::class
            )->findOneBy(
                [
                    'group' => $materialGroup,
                    'title' => 'Charcoal Oak',
                ]
            );

            if ($materialFinish) {
                $materialFinish->setTitle('Smoked Oak');
            }
        }
        $this->manager->flush();
    }
}
