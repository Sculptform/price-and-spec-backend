<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190820125048;


use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190820125048
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    public function load(ObjectManager $manager)
    {
        $orderMap = [
            'Click-on Battens'         => [
                'Species' => [
                    'Spotted Gum',
                    'Blackbutt',
                    'White Oak',
                    'Vic Ash',
                    'Pacific Teak',
                    'Banjo Pine',
                ],
                'Timber-look Wraps' => [
                    'Smoked Oak',
                    'Chocolate Oak',
                    'Walnut',
                    'Rivergum',
                    'Blackbutt',
                    'Spotted Gum',
                    'Greywash Oak',
                    'White Oak',
                    'Whitewash Blackbutt',
                    'Whitewash Oak',
                ],
            ],
            'Facade Blades' => [
                'Timber-look Wraps' => [
                    'Smoked Oak',
                    'Chocolate Oak',
                    'Walnut',
                    'Spotted Gum',
                    'Greywash Oak',
                    'White Oak',
                    'Whitewash Blackbutt',
                    'Whitewash Oak',
                ]
            ],
            'Tongue & Groove Cladding' => [
                'Species' => [
                    'Burnt Ash',
                    'Spotted Gum',
                    'Blackbutt',
                    'American White Oak',
                    'Vic Ash',
                    'Pacific Teak',
                    'Banjo Pine',
                ],
            ],
        ];

        foreach ($orderMap as $pType => $fGroup) {
            $productType = $manager->getRepository(ProductType::class)->findOneBy(
                ['title' => $pType]
            );
            foreach ($fGroup as $gTitle => $finishes) {
                $group = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                    ['title' => $gTitle, 'productType' => $productType]
                );
                foreach ($group->getMaterialFinishes() as $finish) {
                    $finish->setOrderWeight(array_search($finish->getTitle(), $finishes));
                }
            }
        }

        $manager->flush();
    }
}

