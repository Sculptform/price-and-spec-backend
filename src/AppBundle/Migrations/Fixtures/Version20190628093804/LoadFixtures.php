<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190628093804;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\MaterialCoating;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\User;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190214133237
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    private $manager;

    protected $finishIdList = [];
    protected $productIdList = [];
    protected $productTypeId;
    protected $materialTypeId;
    protected $finishToDelete;
    protected $defaultFinish;
    protected $materialFinishGroup;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $productType = $this->manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Tongue & Groove Cladding']);

        $materialType = $this->manager->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Timber']);

        $this->productTypeId = $productType->getId();

        $this->materialTypeId = $materialType->getId();
        $this->defaultFinish  = $this->getDefaultFinish();
        $this->finishToDelete = $this->getMaterialFinishes(
            ['Yakisugi Charred Species']
        );

        $this->removeFinishes();

        if (count($this->finishIdList) > 0) {
            $this->removeFromList('material_finish', $this->finishIdList);
        }
    }

    private function removeFinishes()
    {
        $host = $this->container->getParameter('host');

        $defaultTextureUrl = sprintf(
            '%s/uploads/materialShapeFinishes/%s', $host,
            $this->defaultFinish['image_path']
        );

        if ($this->finishToDelete and count($this->finishToDelete) > 0) {
            foreach ($this->finishToDelete as $item) {
                $products = $this->getProducts($item['id']);

                foreach ($products as $product) {
                    $defaultProduct = $this->getDefaultProduct(
                        $product['material_id'], $this->defaultFinish['id']
                    );
                    $projectProducts
                                    = $this->getExpressionCladdingProjectProducts(
                        $product['id']
                    );

                    foreach ($projectProducts as $projectProduct) {
                        $this->updateExpressionCladdingProjectProduct(
                            $projectProduct['id'], $defaultProduct['id']
                        );

                        $scene       = $this->getScene(
                            $projectProduct['project_id']
                        );
                        $sceneObject = json_decode($scene['scene'], true);

                        if ($scene and isset($sceneObject['scene']['materials'])
                            and isset($sceneObject['scene']['object']['children'])
                        ) {
                            $materials = [];
                            foreach (
                                $sceneObject['scene']['object']['children'] as &
                                $o
                            ) {
                                if ($o['productModel'] == $product['id']) {
                                    $o['productModel'] = $defaultProduct['id'];
                                }
                                if ($o['materialFinishModel'] == $item['id']) {
                                    $o['materialFinishModel']
                                                 = $this->defaultFinish['id'];
                                    $materials[] = $o['material'];
                                }
                            }

                            $maps = [];
                            foreach ($sceneObject['scene']['materials'] as &$i)
                            {
                                if (isset($i['map']) and in_array(
                                        $i['uuid'], $materials
                                    )
                                ) {
                                    $maps[] = $i['map'];
                                }
                            }

                            $images = [];
                            if (isset($sceneObject['scene']['textures'])) {
                                foreach (
                                    $sceneObject['scene']['textures'] as $key =>
                                &$i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $maps
                                        )
                                    ) {
                                        if (isset($i['image'])) {
                                            $images[] = $i['image'];
                                        }
                                    }
                                }
                            }

                            if (isset($sceneObject['scene']['images'])) {
                                foreach (
                                    $sceneObject['scene']['images'] as $key => &
                                    $i
                                ) {
                                    if (isset($i['uuid']) and in_array(
                                            $i['uuid'], $images
                                        )
                                    ) {
                                        $i['url'] = $defaultTextureUrl;
                                    }
                                }
                            }

                            $this->updateScene($scene['id'], $sceneObject);
                        }
                    }
                    $this->productIdList[] = $product['id'];
                }
                $this->finishIdList[] = $item['id'];
            }
        }
    }
}


