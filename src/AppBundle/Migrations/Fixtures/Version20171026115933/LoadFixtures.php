<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\Migrations\Fixtures\Version20171026115933;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadFixtures
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $thumbnailsDir = __DIR__ . '/../../../DataFixtures/Data/ToolboxThumbnails';

        $shapeSizes = $manager->getRepository(MaterialShapeSize::class)->findAll();

        /** @var MaterialShapeSize $shapeSize */
        foreach ($shapeSizes as $shapeSize) {
            $shape = $shapeSize->getMaterialShape();

            /** @var Material $material */
            foreach ($shapeSize->getMaterials() as $material) {
                $materialType = $material->getMaterialType();
                $productType = $material->getProductType();

                $imagePath = '';

                if ('Concept Click' === $productType->getTitle()) {
                    $imagePath = implode('/', [
                        $thumbnailsDir,
                        $productType->getTitle(),
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.png',
                            $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]);
                } elseif ('Expression Cladding' === $productType->getTitle()) {
                    if ('Base' === $shape->getTitle()) {
                        continue;
                    }

                    $imagePath = implode('/', [
                        $thumbnailsDir,
                        $productType->getTitle(),
                        $shape->getTitle(),
                        sprintf('%sx%s.png', $shapeSize->getWidth(), $shapeSize->getDepth()),
                    ]);
                } elseif ('Fintrax' === $productType->getTitle()) {
                    if ('base_stacker' === $shape->getName()) {
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Base',
                            sprintf('%s.png', $shapeSize->getWidth()),
                        ]);
                    } elseif ('stacker' === $shape->getName()) {
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Stackers',
                            sprintf(
                                '%sx%s.png',
                                $shapeSize->getWidth(),
                                $shapeSize->getDepth()
                            ),
                        ]);
                    } else { // nosing
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Nosing',
                            $shapeSize->getWidth(),
                            sprintf('%s.png', $shape->getTitle()),
                        ]);
                    }
                } else {
                    $this->container->get('logger')->error('Toolbox thumbnails loading error', [
                        $productType->getTitle(),
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.png',
                            $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]);
                }

                if ($this->fs->exists($imagePath)) {
                    $imageFilename = md5($imagePath);
                    $imageFileInfo = new \SplFileInfo($imagePath);

                    $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                    $this->fs->copy($imagePath, $tmpFilePath, true);

                    $imageFile = new UploadedFile(
                        $tmpFilePath,
                        $imageFilename,
                        MimeTypeGuesser::getInstance()->guess($imagePath),
                        $imageFileInfo->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $shapeSize->setImageFile($imageFile);
                } else {
                    $this->container->get('logger')->error('Toolbox thumbnails loading error', [$imagePath]);
                }
            }
        }

        $manager->flush();
    }
}
