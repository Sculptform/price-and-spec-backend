<?php

namespace AppBundle\Migrations\Fixtures\Version20210707070707;

use AppBundle\Entity\Material;
use AppBundle\Entity\Object3D;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

/**
 * Class LoadFixtures
 *
 * @see     https://woodform.atlassian.net/browse/SFM-754
 * @package AppBundle\Migrations\Fixtures\Version20210707070707
 * @author  Oleg Sviatchenko <oleg.sviatchenko@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @var string[]
     */
    private $objectFiles = [
        'object3ds_block.yml',
        'object3ds_dome.yml',
        'object3ds_flute.yml'
    ];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        try {
            foreach ($this->objectFiles as $objectFile) {
                $source = file_get_contents(__DIR__.'/'.$objectFile);
                $objects = Yaml::parse($source);
                foreach ($objects['AppBundle\Entity\Object3D'] as $key => $object) {
                    /** @var Object3D $object3d */
                    $object3d = $manager->getRepository(Object3D::class)->findOneByTitle($object['old_title']);
                    print_r(
                        sprintf(
                            'Model - %s; Old title - %s; found - %s%s',
                            $object['title'],
                            $object['old_title'],
                            $object3d ? 'Yes' : 'No',
                            PHP_EOL
                        )
                    );
                    if ($object3d) {
                        $object3d->setTitle($object['title']);
                        $object3d->setValue($object['value']);
                        /** @var Material $material */
                        $material = $object3d->getMaterials()->first();
                        if ($material) {
                            preg_match_all('|\d+|', $object['title'], $sizes);
                            [$width, $depth] = $sizes[0];
                            $materialShapeSize = $material->getMaterialShapeSize();
                            print_r(
                                sprintf(
                                    'MaterialShapeSize ID %s; OLD width %s x depth %s%s',
                                    $materialShapeSize->getId(),
                                    $width,
                                    $depth,
                                    PHP_EOL
                                )
                            );
                            $materialShapeSize->setWidth($width);
                            $materialShapeSize->setDepth($depth);
                            $originalName = str_replace('object3d_', '', $key).'.svg';
                            $materialShapeSize->setImageFile(
                                new UploadedFile(
                                    __DIR__.'/svg/'.$originalName,
                                    $originalName,
                                    'image/svg',
                                    null,
                                    null,
                                    true
                                )
                            );
                        }
                    }
                }
            }
        } catch (\Exception $exception) {
            var_dump($exception->getMessage(), $exception->getFile().':'.$exception->getLine());
        }
        $manager->flush();
    }
}
