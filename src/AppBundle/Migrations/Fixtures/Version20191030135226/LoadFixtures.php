<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20191030135226;


use AppBundle\Entity\AcousticBacking;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use AppBundle\Enum\ApplicationType;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Util\Inflector;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Filesystem\Filesystem;
use SplFileObject;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20191030135226
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * @var EntityManagerInterface
     */
    private $em;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->em = $manager;

        $finishesDir = __DIR__
            .'/../../../DataFixtures/Data/Finishes/CCL/Aluminium/Timber Look Wraps';

        $finishes = [
            'Rivergum',
            'Blackbutt',
        ];

        $materialType = $manager->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Aluminium']);
        $productType  = $manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Facade Blades']);
        $finishGroup  = $manager->getRepository(MaterialFinishGroup::class)
            ->findOneBy(['title' => 'Timber-look Wraps', 'productType' => $productType]);

        foreach ($finishes as $finish) {
            $item = new MaterialFinish();
            $item->setTitle($finish);
            $item->setApplicationTypes(
                [ApplicationType::INTERIOR, ApplicationType::EXTERIOR]
            );
            $item->setGroup($finishGroup);
            $item->setMaterialType($materialType);

            $imagePath = sprintf('%s/%s.jpg', $finishesDir, $finish);

            if ($this->fs->exists($imagePath)) {
                $imageFilename = md5($imagePath);
                $imageFileInfo = new \SplFileInfo($imagePath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($imagePath, $tmpFilePath, true);

                $imageFile = new UploadedFile(
                    $tmpFilePath,
                    $imageFilename,
                    MimeTypeGuesser::getInstance()->guess($imagePath),
                    $imageFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $item->setTextureFile($imageFile);

                $manager->persist($item);
            } else {
                $this->container->get('logger')->error(
                    'Toolbox thumbnails loading error',
                    [$imagePath]
                );
            }
        }

        $manager->flush();

        $orderMap = [
            'Click-on Battens'         => [
                'Species'           => [
                    'Spotted Gum',
                    'Blackbutt',
                    'White Oak',
                    'Vic Ash',
                    'Pacific Teak',
                    'Banjo Pine',
                ],
                'Timber-look Wraps' => [
                    'Smoked Oak',
                    'Chocolate Oak',
                    'Walnut',
                    'Rivergum',
                    'Blackbutt',
                    'Spotted Gum',
                    'Greywash Oak',
                    'White Oak',
                    'Whitewash Blackbutt',
                    'Whitewash Oak',
                ],
            ],
            'Facade Blades'            => [
                'Timber-look Wraps' => [
                    'Smoked Oak',
                    'Chocolate Oak',
                    'Walnut',
                    'Rivergum',
                    'Blackbutt',
                    'Spotted Gum',
                    'Greywash Oak',
                    'White Oak',
                    'Whitewash Blackbutt',
                    'Whitewash Oak',
                ],
            ],
            'Tongue & Groove Cladding' => [
                'Species' => [
                    'Burnt Ash',
                    'Spotted Gum',
                    'Blackbutt',
                    'American White Oak',
                    'Vic Ash',
                    'Pacific Teak',
                    'Banjo Pine',
                ],
            ],
        ];

        foreach ($orderMap as $pType => $fGroup) {
            $productType = $manager->getRepository(ProductType::class)->findOneBy(
                ['title' => $pType]
            );
            foreach ($fGroup as $gTitle => $finishes) {
                $group = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                    ['title' => $gTitle, 'productType' => $productType]
                );
                foreach ($group->getMaterialFinishes() as $finish) {
                    $finish->setOrderWeight(array_search($finish->getTitle(), $finishes));
                }
            }
        }

        $manager->flush();

        $this->handleFacadeBladesAluminium();
        $this->handleAcousticBackingsForOldProjects();
        $this->fixProductPricing();
    }

    protected function handleFacadeBladesAluminium()
    {
        $obj3dRepo   = $this->em->getRepository(Object3D::class);
        $productType = $this->em->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        $reader = $this->createReader('Facade Blades.csv');
        foreach ($reader as $key => $row) {
            $number      = trim($row['Item Number']);
            $description = trim($row['Item Description(Long)']);
            $weight      = (float) trim($row['Net Weight']);
            $price       = (float) str_replace('$', '', trim($row['Sculptform Price']));
            $unit        = 'L/M';

            $normalizedDescription = str_replace(
                [
                    'Aluminium – Facade Blade ',
                    'Profile: ',
                    'Anodised 20um (External) - Colour -',
                    'Powdercoated Premium Colour -',
                    'Exterior Timber-look Wrap – Colour -',
                    'Exterior Timber-look Wrap - Colour -',
                ],
                [
                    '',
                    '',
                    'Anodising',
                    'Powder Coating',
                    'Timber-look Wraps',
                    'Timber-look Wraps',
                ],
                $description
            );

            list($sizeTitle, $profileTitle, $finishGroupTitle) = array_map(
                'trim',
                explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            $materialFinishGroup = $this->em->getRepository(MaterialFinishGroup::class)->findOneBy(
                ['productType' => $productType, 'title' => $finishGroupTitle]
            );

            if (! $materialFinishGroup) {
                continue;
            }

            $materialFinishes = $this->em->getRepository(MaterialFinish::class)->findBy(
                ['group' => $materialFinishGroup]
            );

            foreach ($materialFinishes as $materialFinish) {
                $object3d = $obj3dRepo->findOneBy(['title' => sprintf('Facade Blade %sx%s', $deep, $wide)]);
                $material = $this->em->getRepository(Material::class)->findOneBy(
                    ['object3d' => $object3d, 'softDeleted' => false]
                );

                if (! $material) {
                    continue;
                }

                $product = $this->em->getRepository(Product::class)->findOneBy(
                    [
                        'material'       => $material,
                        'materialFinish' => $materialFinish,
                    ]
                );

                if (! $product) {
                    $product = new Product();
                }

                $product
                    ->setItemNumber($number)
                    ->setItemDescription($description)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setPrice($price)
                    ->setCutekPrice(null)
                    ->setNaturalAccentPrice(null)
                    ->setEnviroproPrice(null)
                    ->setUnit($unit)
                    ->setWeight($weight);

                $this->em->persist($product);
            }
            $this->em->flush();
        }
    }

    protected function handleAcousticBackingsForOldProjects()
    {
        $productType   = $this->em->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Click-on Battens']);
        $noBacking     = $this->em->getRepository(AcousticBacking::class)
            ->findOneBy(['title' => 'No Backing', 'productType' => $productType]);
        $group3Backing = $this->em->getRepository(AcousticBacking::class)
            ->findOneBy(['title' => 'Group 3 Rated - Standard', 'productType' => $productType]);
        $wrongBacking  = $this->em->getRepository(AcousticBacking::class)
            ->findOneBy(['title' => '7mm PET Black']);
        $projects      = $this->em->getRepository(Project::class)
            ->findBy(['productType' => $productType]);

        foreach ($projects as $project) {
            /**
             * @var Project $project
             */
            if ($project->getAcousticBacking() && $project->getAcousticBackingId() != $wrongBacking->getId()) {
                continue;
            }

            $acoustic = $noBacking;

            if($project->getAcousticRating() && $project->getAcousticRating()->getNrc() != 0 ){
                $acoustic = $group3Backing;
            }

            $project->setAcousticBacking($acoustic);
        }

        $this->em->flush();
    }

    protected function fixProductPricing()
    {
        $productType  = $this->em->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Click-on Battens']);
        $materialType = $this->em->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Aluminium']);

        $materials = $this->em->getRepository(Material::class)
            ->findBy(['productType' => $productType, 'materialType' => $materialType]);

        $materialGroup = $this->em->getRepository(MaterialFinishGroup::class)->findOneBy(
            ['productType' => $productType, 'materialType' => $materialType, 'title' => 'Anodising']
        );

        $finishes = $materialGroup->getMaterialFinishes();

        foreach ($materials as $material) {
            foreach ($finishes as $finish) {
                $products = $this->em->getRepository(Product::class)->findBy(
                    [
                        'material'       => $material,
                        'materialFinish' => $finish,
                    ]
                );

                if (count($products) > 1) {
                    $this->em->remove($products[1]);
                }
            }
        }

        $this->em->flush();
    }

    /**
     * @param string $filename
     *
     * @return CsvReader
     */
    private function createReader($filename)
    {
        $path = __DIR__."/../../../DataFixtures/Data/Products/30-august-2019/{$filename}";

        $file = new SplFileObject(
            realpath($path)
        );

        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        return $reader;
    }

    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }
}
