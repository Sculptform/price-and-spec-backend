<?php

namespace AppBundle\Migrations\Fixtures\Version20190404102533;

use AppBundle\Entity\ProjectFilter;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $filters = [
            'Most popular',
            'Timber',
            'Aluminium',
            'Affordable'
        ];

        foreach ($filters as $filterName) {
            $filter = new ProjectFilter();
            $filter->setName($filterName);
            $manager->persist($filter);
        }

        $manager->flush();
    }
}
