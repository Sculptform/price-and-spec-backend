<?php

namespace AppBundle\Migrations\Fixtures\Version20191111123339;

use AppBundle\Util\PriceCalculator;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * Projects' prices recalculation.
 *
 * @package AppBundle\Migrations\Fixtures\Version20191111123339
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $data = [];

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $defaultAcousticBacking = $manager->getRepository('AppBundle:AcousticBacking')->findOneBy([
            'isDefault' => true,
        ]);

        $audCurrency = $manager->getRepository('AppBundle:Currency')->findOneBy([
            'code' => 'AUD'
        ]);

        $calculator = new PriceCalculator($defaultAcousticBacking);

        $projects = $manager->getRepository('AppBundle:Project')->findBy([
            'outdated' => false,
        ]);

        foreach ($projects as $project) {
            $price    = $calculator->calculate($project);
            $currency = $project->getCurrency();

            if ( ! $currency) {
                $currency = $audCurrency;
            }

            $pricingCategory = $project->getPricingCategory();
            if ( ! $pricingCategory) {
                $pricingCategory = '100-300';
            }

            $query = "UPDATE project SET price={$price}, currency_id={$currency->getId()}, pricing_category='{$pricingCategory}' WHERE id='{$project->getId()}';";

            $this->data[] = $query;

            if (count($this->data) >= 100) {
                $this->update();
            }
        }

        $this->update();
    }

    private function update()
    {
        $queries = implode('', $this->data);

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em
            ->getConnection()
            ->exec($queries);

        $this->data = [];
    }
}
