<?php

namespace AppBundle\Migrations\Fixtures\Version20190328081610;

use AppBundle\Entity\MaterialCoating;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $coatings = $manager
            ->getRepository(MaterialCoating::class)
            ->findAll();

        $coatingReplacement = [
            'Clear Poly' => 'Clear Poly Interior Coating',
            'Clear Oil'  => 'Clear Oil Interior / Exterior',
        ];

        foreach ($coatings as &$coating) {
            if (array_key_exists($coating->getTitle(), $coatingReplacement)) {
                $coating->setTitle($coatingReplacement[$coating->getTitle()]);
            }
        }

        $manager->flush();
    }
}
