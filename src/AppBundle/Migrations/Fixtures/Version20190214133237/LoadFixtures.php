<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190214133237;


use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\MaterialCoating;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\User;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190214133237
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $shapes = [];
    private $manager;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->manager = $manager;

        $productType = $this->manager->getRepository(ProductType::class)
            ->findOneBy(
                ['title' => 'Click-on Battens']
            );

        $materialType = $this->manager->getRepository(MaterialType::class)
            ->findOneBy(
                ['title' => 'Timber']
            );

        $finishesToReplace = ['Banjo Pine', 'Vic Ash'];
        $this->shapes      = [
            'Banjo Pine' => ['Flute', 'Dome'], 'Vic Ash' => ['Dome'],
        ];

        $defaultFinish = $this->manager->getRepository(
            DefaultMaterialFinish::class
        )
            ->findOneBy(
                ['productType' => $productType, 'materialType' => $materialType]
            )->getMaterialFinish();

        foreach ($finishesToReplace as $item) {
            $finishToReplace = $this->manager->getRepository(
                MaterialFinish::class
            )
                ->findByProductType($productType, $item);

            if ($finishToReplace) {
                $this->replaceProducts(
                    $finishToReplace, $defaultFinish
                );

                $this->manager->flush();
            }
        }

        $this->updateUserNames();
        $this->renameCoatings();
    }

    private function updateUserNames()
    {
        $users = $this->manager->getRepository(User::class)->findAll();

        foreach ($users as &$user) {
            $firstName = trim($user->getFirstName());
            $lastName  = trim($user->getLastName());

            // Test if user already has glued first name and last name into the first_name field
            //TODO: not a perfect solution in general, can be refactored in the future
            $testName = explode(' ', $firstName);
            if (count($testName) > 1) {
                continue;
            }

            $user->setFirstName(trim(implode(' ', [$firstName, $lastName])));
        }

        $this->manager->flush();
    }

    private function replaceProducts(
        MaterialFinish $finishToReplace, MaterialFinish $defaultFinish
    ) {
        $host              = $this->container->getParameter('host');
        $defaultTextureUrl = sprintf(
            '%s/uploads/materialShapeFinishes/%s', $host,
            $defaultFinish->getTexturePath()
        );

        foreach ($finishToReplace->getProducts() as &$p) {
            $projectProducts = $p->getProjectProducts();
            foreach ($projectProducts as &$projectProduct) {
                $shapeTitle = $p->getMaterial()->getMaterialShapeSize()
                    ->getMaterialShape()->getTitle();

                if ( ! in_array(
                    $shapeTitle, $this->shapes[$finishToReplace->getTitle()]
                )
                ) {
                    continue;
                }

                $defaultProduct = $this->manager->getRepository(Product::class)
                    ->findOneBy(
                        [
                            'material'       => $p->getMaterial(),
                            'materialFinish' => $defaultFinish,
                        ]
                    );

                $projectProduct->setProduct($defaultProduct);

                $project = $projectProduct->getProject();
                $scene   = $project->getScenes()->first();

                if ( ! $scene) {
                    continue;
                }

                $scene = $scene->getScene();

                $sceneMaterials = [];

                foreach ($scene['scene']['object']['children'] as $item) {
                    if (($item['woodformShapeType'] == 'flute'
                            || $item['woodformShapeType'] == 'dome')
                        && @$item['materialFinishModel']
                        == $finishToReplace->getId()
                    ) {
                        $sceneMaterials[] = $item['material'];
                    }
                }

                $newImageUuid   = $this->getGUID();
                $newTextureUuid = $this->getGUID();

                $scene['scene']['images'][] = $this->getImageBlock(
                    $newImageUuid,
                    $defaultTextureUrl
                );

                $scene['scene']['textures'][] = $this->getTextureBlock(
                    $newTextureUuid, $newImageUuid
                );

                foreach ($scene['scene']['materials'] as &$m) {
                    if (in_array($m['uuid'], $sceneMaterials)) {
                        $m['map'] = $newTextureUuid;
                    }
                }

                foreach ($scene['scene']['object']['children'] as &$o) {
                    if (@$o['productModel'] == $p->getId()) {
                        @$o['productModel'] = $defaultProduct->getId();
                    }
                    if (@$o['materialFinishModel'] == $finishToReplace->getId()
                    ) {
                        @$o['materialFinishModel'] = $defaultFinish->getId();
                    }
                }

                $project->getScenes()->first()->setScene($scene);

                $this->manager->remove($p);
            }
        }

        $this->manager->flush();
    }

    private function disableCutekClearOilCoating()
    {
        $shapes = ['Dome', 'Flute'];

        foreach ($shapes as $item) {
            $shape = $this->manager->getRepository(MaterialShape::class)
                ->findOneBy(['title' => $item]);

            $products = $this->manager->getRepository(Product::class)
                ->findAllByParams(['materialShape' => $shape->getId()]);

            /**
             * @var Product $product
             */
            foreach ($products as &$product) {
                $product->setCutekPrice(null);
                $product->setCutekRetailPrice(null);
                $product->setCutekWholesalePrice(null);
            }

            $this->manager->flush();
        }
    }

    private function renameCoatings()
    {
        $coatings = $this->manager->getRepository(MaterialCoating::class)
            ->findAll();

        $coatingReplacement = [
            'Natural Accent'  => 'Clear Poly',
            'Cutek Clear Oil' => 'Clear Oil',
            'Enviropro'       => 'Enviropro Exterior Coating',
        ];

        foreach ($coatings as &$coating) {
            $coating->setTitle($coatingReplacement[$coating->getTitle()]);
        }

        $this->manager->flush();
    }

    private function getGUID()
    {
        mt_srand((double)microtime() * 10000);//optional for php 4.2.0 and up.
        $charid = strtoupper(md5(uniqid(rand(), true)));
        $hyphen = chr(45);// "-"
        $uuid   = ''
            .substr($charid, 0, 8).$hyphen
            .substr($charid, 8, 4).$hyphen
            .substr($charid, 12, 4).$hyphen
            .substr($charid, 16, 4).$hyphen
            .substr($charid, 20, 12);

        return $uuid;
    }

    /**
     * Returns the texture block for scene
     *
     * @param $uuid
     * @param $imageUuid
     *
     * @return array
     */
    private function getTextureBlock($uuid, $imageUuid)
    {
        return [
            'uuid'       => $uuid,
            'name'       => '',
            'mapping'    => 300,
            'repeat'     => [1, 1],
            'offset'     => [0, 0],
            'wrap'       => [1001, 1001],
            'minFilter'  => 1008,
            'magFilter'  => 1006,
            'anisotropy' => 1,
            'image'      => $imageUuid,
        ];
    }

    /**
     * Returns the image block for scene
     *
     * @param $uuid
     * @param $imageUrl
     *
     * @return array
     */
    private function getImageBlock($uuid, $imageUrl)
    {
        return [
            'uuid' => $uuid,
            'url'  => $imageUrl,
        ];
    }
}
