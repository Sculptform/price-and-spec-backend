<?php

namespace AppBundle\Migrations\Fixtures\Version20190226115739;

use Symfony\Component\Filesystem\Filesystem;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190215111652
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait;

    /** @var Filesystem */
    private $filesystem;

    protected $productTypeId;
    protected $materialTypeId;
    protected $finishToDelete;
    protected $defaultFinish;

    protected $finishIdList  = [];
    protected $productIdList = [];

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $this->productTypeId  = $this->getProductType();
        $this->materialTypeId = $this->getMaterialType();
        $this->defaultFinish  = $this->getDefaultFinish();
        $this->finishToDelete = $this->getMaterialFinishes(
            'Real Timber Veneer', ['Mocha Oak', 'Lightwash Oak', 'Smoked Oak', 'Victorian Ash']);

        $this->removeFinishes();

        if (count($this->productIdList) > 0) {
            $this->removeFromList('product', $this->productIdList);
        }

        if (count($this->finishIdList) > 0) {
            $this->removeFromList('material_finish', $this->finishIdList);
        }
    }

    private function removeFinishes()
    {
        if ($this->finishToDelete and count($this->finishToDelete) > 0) {
            foreach ($this->finishToDelete as $item) {
                $products = $this->getProducts($item['id']);

                foreach ($products as $product) {
                    $defaultProduct  = $this->getDefaultProduct($product['material_id'], $this->defaultFinish['id']);
                    $projectProducts = $this->getProjectProducts($product['id']);

                    foreach ($projectProducts as $projectProduct) {
                        $this->updateProjectProduct($projectProduct['id'], $defaultProduct['id']);

                        $scene = $this->getScene($projectProduct['project_id']);
                        $sceneObject = json_decode($scene['scene'], true);

                        if ($scene and isset($sceneObject['scene']['materials']) and isset($sceneObject['scene']['object']['children'])) {
                            $materials = [];
                            foreach ($sceneObject['scene']['object']['children'] as &$o) {
                                if ($o['productModel'] == $product['id']) {
                                    $o['productModel'] = $defaultProduct['id'];
                                }
                                if ($o['materialFinishModel'] == $item['id']) {
                                    $o['materialFinishModel'] = $this->defaultFinish['id'];
                                    $materials[] = $o['material'];
                                }
                            }

                            foreach ($sceneObject['scene']['materials'] as &$i) {
                                if (in_array($i['uuid'], $materials)) {
                                    $i['color'] = hexdec($this->defaultFinish['color']);
                                }
                            }

                            $maps = [];
                            foreach ($sceneObject['scene']['materials'] as &$i) {
                                if (isset($i['map']) and in_array($i['uuid'], $materials)) {
                                    $maps[] = $i['map'];
                                    unset($i['map']);
                                }
                            }

                            $images = [];
                            if (isset($sceneObject['scene']['textures'])) {
                                foreach ($sceneObject['scene']['textures'] as $key => &$i) {
                                    if (isset($i['uuid']) and in_array($i['uuid'], $maps)) {
                                        if (isset($i['image'])) {
                                            $images[] = $i['image'];
                                        }
                                        unset($i[$key]);
                                    }
                                }
                            }

                            if (isset($sceneObject['scene']['images'])) {
                                foreach ($sceneObject['scene']['images'] as $key => &$i) {
                                    if (isset($i['uuid']) and in_array($i['uuid'], $images)) {
                                        unset($i[$key]);
                                    }
                                }
                            }

                            $this->updateScene($scene['id'], $sceneObject);
                        }
                    }
                    $this->productIdList[] = $product['id'];
                }
                $this->finishIdList[] = $item['id'];
            }
        }
    }
}
