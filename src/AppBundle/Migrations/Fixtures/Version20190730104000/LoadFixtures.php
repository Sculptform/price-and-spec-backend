<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190730104000;


use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use AppBundle\Migrations\Fixtures\Version20190215111652\ImageTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\SelectTrait;
use AppBundle\Migrations\Fixtures\Version20190215111652\UpdateTrait;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190730104000
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait, ImageTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    public function load(ObjectManager $manager)
    {
        $finishesDir = __DIR__
            .'/../../../DataFixtures/Data/Finishes/CCL/Aluminium/Timber Look Wraps';

        $finishes = [
            'Rivergum',
            'Blackbutt',
        ];

        $materialType = $manager->getRepository(MaterialType::class)
            ->findOneBy(['title' => 'Aluminium']);
        $productType  = $manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Click-on Battens']);
        $finishGroup  = $manager->getRepository(MaterialFinishGroup::class)
            ->findOneBy(['title' => 'Timber-look Wraps', 'productType' => $productType]);

        foreach ($finishes as $finish) {
            $item = new MaterialFinish();
            $item->setTitle($finish);
            $item->setApplicationTypes(
                [ApplicationType::INTERIOR, ApplicationType::EXTERIOR]
            );
            $item->setGroup($finishGroup);
            $item->setMaterialType($materialType);

            $imagePath = sprintf('%s/%s.jpg', $finishesDir, $finish);

            if ($this->fs->exists($imagePath)) {
                $imageFilename = md5($imagePath);
                $imageFileInfo = new \SplFileInfo($imagePath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($imagePath, $tmpFilePath, true);

                $imageFile = new UploadedFile(
                    $tmpFilePath,
                    $imageFilename,
                    MimeTypeGuesser::getInstance()->guess($imagePath),
                    $imageFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $item->setTextureFile($imageFile);

                $manager->persist($item);
            } else {
                $this->container->get('logger')->error(
                    'Toolbox thumbnails loading error',
                    [$imagePath]
                );
            }
        }

        $manager->flush();
    }
}

