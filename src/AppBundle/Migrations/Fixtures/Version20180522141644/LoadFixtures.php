<?php

namespace AppBundle\Migrations\Fixtures\Version20180522141644;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imageDir = __DIR__ . '/../../../DataFixtures/Data/Finishes/ECL/Timber/Yakisugi Charred Species/';
        $images   = ['Yakisugi Ash - Burnt and Brushed' => 'Yakisugi Ash Burnt Brushed.jpg',
                     'Yakisugi Ash - Full Char'         => 'Yakisugi Ash Full Char.jpg',
                     'Yakisugi Pine - Flame Licked'     => 'Yakisugi Pine Flame Licked.jpg'];

        $materialfinishCategory = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'Yakisugi Charred Species']);
        $materialType           = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Timber']);

        foreach ($images as $key => $img){

            $materialFinish = new MaterialFinish();
            $imgPath        = $imageDir.$img;

            $materialFinish->setTitle($key);
            $materialFinish->setMaterialType($materialType);
            $materialFinish->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            $materialFinish->setGroup($materialfinishCategory);

            if($this->fs->exists($imgPath)) {
                $filename = md5($imgPath);
                $fileInfo = new \SplFileInfo($imgPath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($imgPath, $tmpFilePath, true);

                $file = new UploadedFile(
                    $tmpFilePath,
                    $filename,
                    MimeTypeGuesser::getInstance()->guess($imgPath),
                    $fileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $materialFinish->setTextureFile($file);
            } else {
                $this->container->get('logger')->error('Material finish texture file loading error', [
                    $imgPath
                ]);
            }
            $manager->persist($materialFinish);
            $manager->flush();
        }
    }
}