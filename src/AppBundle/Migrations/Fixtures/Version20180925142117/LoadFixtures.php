<?php

namespace AppBundle\Migrations\Fixtures\Version20180925142117;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imageDir = __DIR__.'/../../../DataFixtures/Data/ToolboxThumbnails/Facade Blades/';

        $images = [
            'ptype_concept_click'       => 'click_on_batten.png',
            'ptype_facade_blade'        => 'facade_blades.png',
            'ptype_expression_cladding' => 'tongue_groove_cladding.png',
        ];

        $sizes = ['50x150', '50x200', '50x300'];

        $obj3dRepo    = $manager->getRepository(Object3D::class);
        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Aluminium']);
        $productType  = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        $finishG = ['Anodising', 'Exterior Foil Wraps', 'Powder Coating'];

        $obj3dData               = Yaml::parse(file_get_contents(__DIR__.'/../../../DataFixtures/ORM/object3ds.yml'));
        $materialFinishData      = Yaml::parse(
            file_get_contents(__DIR__.'/../../../DataFixtures/ORM/materialFinishesFBAluminium.yml')
        );
        $materialFinishGroupData = Yaml::parse(
            file_get_contents(__DIR__.'/../../../DataFixtures/ORM/materialFinishGroups.yml')
        );


        foreach ($obj3dData['AppBundle\Entity\Object3D'] as $key => $item) {
            if (false === strpos($key, 'facade_blade')) {
                continue;
            }

            $obj3d = new Object3D();
            $obj3d->setTitle($item['title']);
            $obj3d->setValue($item['value']);

            $manager->persist($obj3d);
            $manager->flush();
        }

        /**
         * @var MaterialShape $materialShape
         */
        $materialShape = $manager->getRepository(MaterialShape::class)->findOneBy(['name' => 'base_stacker']);

        $materialShape->setParent(null);

        foreach ($sizes as $size) {
            list($width, $depth) = explode('x', $size);
            $materialShapeSize = new MaterialShapeSize();
            $materialShapeSize->setDepth($depth);
            $materialShapeSize->setWidth($width);
            $materialShapeSize->setMaterialShape($materialShape);
            if ($img = $this->checkImage($imageDir.$size.'.png')) {
                $materialShapeSize->setImageFile($img);
            }

            $manager->persist($materialShapeSize);

            $material = new Material();
            $material->setObject3d($obj3dRepo->findOneBy(['title' => sprintf('Facade Blade %sx%s', $depth, $width)]));
            $material->setApplicationTypes([ApplicationType::INTERIOR, ApplicationType::EXTERIOR]);
            $material->setMaterialShapeSize($materialShapeSize);
            $material->setMaterialType($materialType);
            $material->setProductType($productType);

            $manager->persist($material);

            $manager->flush();
        }

        $finishGroupImages = [
            'Anodising'           => __DIR__
                                     .'/../../../DataFixtures/Data/FinishGroups/FTX/Anodising.png',
            'Powder Coating'      => __DIR__
                                     .'/../../../DataFixtures/Data/FinishGroups/FTX/Powder Coating.png',
            'Exterior Foil Wraps' => __DIR__
                                     .'/../../../DataFixtures/Data/FinishGroups/CCL/Aluminium/Exterior Foil Wraps.png',
            'FB AL Colours'       => '',
        ];
        foreach ($materialFinishGroupData['AppBundle\Entity\MaterialFinishGroup'] as $key => $g) {
            if (false === strpos($key, 'facade_blade')) {
                continue;
            }

            $group = new MaterialFinishGroup();
            $group->setTitle($g['title']);
            $group->setMaterialType($materialType);
            $group->setApplicationTypes([ApplicationType::EXTERIOR]);
            $group->setProductType($productType);
            if ($img = $this->checkImage($finishGroupImages[$g['title']])) {
                $group->setImageFile($img);
            }

            $manager->persist($group);


            if(array_key_exists('parent', $g)){
                $parent = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'FB AL Colours']);
                $group->setParent($parent);
            }


            $manager->flush();
        }

        $groups = [
            '@material_finish_group_facade_blade_aluminium_anodising'           => 'Anodising',
            '@material_finish_group_facade_blade_aluminium_powder_coating'      => 'Powder Coating',
            '@material_finish_group_facade_blade_aluminium_exterior_foil_wraps' => 'Exterior Foil Wraps',
        ];

        foreach ($materialFinishData['AppBundle\Entity\MaterialFinish'] as $key => $f) {
            $group = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                ['title' => $groups[$f['group']], 'productType' => $productType]
            );

            $ext = 'jpg';

            if ($f['group'] == '@material_finish_group_facade_blade_aluminium_powder_coating') {
                $ext = 'png';
            }

            $finish = new MaterialFinish();
            $finish->setMaterialType($materialType);
            $finish->setTitle($f['title']);
            $finish->setGroup($group);
            $finish->setApplicationTypes([ApplicationType::EXTERIOR]);
            if ($img = $this->checkImage(
                sprintf(
                    __DIR__.'/../../../DataFixtures/Data/Finishes/CCL/Aluminium/%s/%s.%s',
                    $groups[$f['group']],
                    $f['title'],
                    $ext
                )
            )) {
                $finish->setTextureFile($img);
            }

            $manager->persist($finish);
            $manager->flush();

        }

        $file   = new \SplFileObject(__DIR__.'/../../../DataFixtures/Data/Products/Facade Blades Data.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description(Long)']);
            $itemNumber  = trim($row['Item Number']);
            $price       = (float)$row['Sculptform Price'];
            $weight      = (float)$row['Net Weight'];
            $unit        = trim($row['Default Unit of Measure']);

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                [
                    'Aluminium – Facade Blade ',
                    'Profile: ',
                    'Anodised 20um (External) - Colour -',
                    'Powdercoated Premium Colour -',
                    'Exterior Foil Wrap - Colour -',
                ],
                [
                    '',
                    '',
                    'Anodising',
                    'Powder Coating',
                    'Exterior Foil Wraps',
                ],
                $description
            );


            list($sizeTitle, $profileTitle, $finishGroupTitle) = array_map(
                'trim',
                explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);

            list($wide, $deep) = $sizes[0];

            $materialFinishGroup = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(
                ['productType' => $productType, 'title' => $finishGroupTitle]
            );
            $materialFinishes    = $manager->getRepository(MaterialFinish::class)->findBy(
                ['group' => $materialFinishGroup]
            );

            foreach ($materialFinishes as $materialFinish) {

                $material = $manager->getRepository(Material::class)->findOneBy(
                    ['object3d' => $obj3dRepo->findOneBy(['title' => sprintf('Facade Blade %sx%s', $deep, $wide)])]
                );

                $item = new Product();

                $item
                    ->setItemNumber($itemNumber)
                    ->setItemDescription($description)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setPrice($price)
                    ->setCutekPrice(0)
                    ->setNaturalAccentPrice(0)
                    ->setEnviroproPrice(0)
                    ->setUnit($unit)
                    ->setWeight($weight);

                $manager->persist($item);
                $manager->flush();
            }
        }

        $railingData = Yaml::parse(
            file_get_contents(__DIR__.'/../../../DataFixtures/ORM/railings.yml')
        );

        $productTypes = [
            '@ptype_concept_click'       => 'Click-on Battens',
            '@ptype_expression_cladding' => 'Tongue & Groove Cladding',
            '@ptype_facade_blade'        => 'Facade Blades',
        ];

        $railings = $manager->getRepository(Railing::class)->findAll();

        foreach ($railings as $railing){
            $manager->remove($railing);
            $manager->flush();
        }

        foreach ($railingData['AppBundle\Entity\Railing'] as $key => $r) {
            $productType = $manager->getRepository(ProductType::class)->findOneBy(
                ['title' => $productTypes[$r['productType']]]
            );

            $railing = new Railing();
            $railing->setTitle($r['title']);
            $railing->setPrice($r['price']);
            $railing->setRetailPrice($r['retailPrice']);
            $railing->setWholesalePrice($r['wholesalePrice']);
            $railing->setWidth($r['width']);
            $railing->setHeight($r['height']);
            $railing->setDescription($r['description']);
            $railing->setMaterialType($materialType);
            $railing->setApplicationTypes($r['applicationTypes']);
            $railing->setModelData($r['modelData']);
            $railing->setProductType($productType);

            $manager->persist($railing);
            $manager->flush();
        }


        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalizeName(string $name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private function checkImage(string $imgPath)
    {
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Image loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}