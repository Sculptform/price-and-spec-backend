<?php

namespace AppBundle\Migrations\Fixtures\Version20190405095136;


use AppBundle\Entity\User;
use AppBundle\Enum\ApplicationType;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $manager;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        /** @var User[] $users */
        $users = $this->getUsers($manager);

        foreach ($users as $user) {
            $value = $user->getFirstName();
            $parts = $this->explodeFirstName($value);

            $firstName = array_shift($parts);
            $lastName  = null;

            if (count($parts) > 0) {
                $lastName = implode(' ', $parts);
            }

            $user->setFirstName($firstName);
            $user->setLastName($lastName);
        }

        $manager->flush();
    }

    private function explodeFirstName($firstName)
    {
        $parts = explode(' ', $firstName);

        return array_map('trim', $parts);
    }

    /**
     * @param ObjectManager $manager
     *
     * @return User[]
     */
    private function getUsers(ObjectManager $manager)
    {
        return $manager
            ->getRepository(User::class)
            ->createQueryBuilder('user')
            ->where('user.firstName IS NOT NULL')
            ->getQuery()
            ->getResult();
    }
}
