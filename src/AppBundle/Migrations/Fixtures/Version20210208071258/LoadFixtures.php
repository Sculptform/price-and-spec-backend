<?php

declare(strict_types=1);


namespace AppBundle\Migrations\Fixtures\Version20210208071258;


use AppBundle\Entity\AcousticBacking;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @see https://woodform.atlassian.net/browse/SFM-745
 * @package AppBundle\Migrations\Fixtures\Version20210208071258
 * @author Polvanov Igor <igor.polvanov@sibers.com>
 * @copyright 2021 (c) Sibers
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc } */
    public function load(ObjectManager $manager)
    {

        $railingTitles = [
            'No Backing',
        ];

         $backings = $manager->getRepository(AcousticBacking::class)->findAll();

         /** @var AcousticBacking $backing */
        foreach ($backings as &$backing) {
            if (in_array($backing->getTitle(), $railingTitles)) {
                $backing->setPrice(33.7);
            }
        }
        $manager->flush();
    }
}