<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Migrations\Fixtures\Version20190115084537;


use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\ProjectProduct;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadFixtures
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Migrations\Fixtures\Version20190115084537
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface,
    ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)->findOneBy(
            ['title' => 'Click-on Battens']
        );

        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(
            ['title' => 'Timber']
        );

        $finishToDelete = $manager->getRepository(MaterialFinish::class)
            ->findByProductType($productType, 'Burnt Ash');

        $defaultFinish = $manager->getRepository(DefaultMaterialFinish::class)
            ->findOneBy(
                ['productType' => $productType, 'materialType' => $materialType]
            )->getMaterialFinish();

        $host = $this->container->getParameter('host');


        $defaultTextureUrl = sprintf(
            '%s/uploads/materialShapeFinishes/%s', $host,
            $defaultFinish->getTexturePath()
        );

        if ($finishToDelete) {
            foreach ($finishToDelete->getProducts() as &$p) {
                $projectProducts = $p->getProjectProducts();
                foreach ($projectProducts as &$projectProduct) {
                    $defaultProduct = $manager->getRepository(Product::class)
                        ->findOneBy(
                            [
                                'material' => $p->getMaterial(),
                                'materialFinish' => $defaultFinish,
                            ]
                        );

                    $projectProduct->setProduct($defaultProduct);

                    $project = $projectProduct->getProject();
                    $scene   = $project->getScenes()->first()->getScene();

                    foreach ($scene['scene']['images'] as &$i) {
                        if (false !== strpos(
                                $i['url'], $finishToDelete->getTexturePath()
                            )
                        ) {
                            $i['url']
                                = $defaultTextureUrl;  // replace finish texture url
                        }
                    }
                    foreach ($scene['scene']['object']['children'] as &$o) {
                        if ($o['productModel'] == $p->getId()) {
                            $o['productModel'] = $defaultProduct->getId();
                        }
                        if ($o['materialFinishModel'] == $finishToDelete->getId(
                            )
                        ) {
                            $o['materialFinishModel'] = $defaultFinish->getId();
                        }
                    }

                    $project->getScenes()->first()->setScene($scene);


                    $manager->remove($p);
                }
            }

            $manager->remove($finishToDelete);

            $manager->flush();
        }
    }

}