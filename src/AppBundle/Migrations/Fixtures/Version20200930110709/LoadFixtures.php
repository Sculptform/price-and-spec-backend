<?php

namespace AppBundle\Migrations\Fixtures\Version20200930110709;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @see     https://woodform.atlassian.net/browse/SFM-739
 * @package AppBundle\Migrations\Fixtures\Version20200930110709
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $currency = $manager->getRepository('AppBundle:Currency')->findOneBy([
            'code' => 'NZD',
        ]);

        $projects = $manager->getRepository('AppBundle:Project')->findBy([
            'currency' => $currency,
        ]);

        foreach ($projects as $project) {
            $price = $project->getPrice();
            if ($price > 0) {
                $project->setPrice((($price / 100) * 10) + $price);
            }
        }

        $manager->flush();
    }
}
