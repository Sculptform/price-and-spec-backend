<?php

namespace AppBundle\Migrations\Fixtures\Version20210715112539;

use AppBundle\Entity\Product;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use SplFileObject;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @see     https://woodform.atlassian.net/browse/SFM-759
 * @package AppBundle\Migrations\Fixtures\Version20210712083456
 * @author  Oleg Sviatchenko <oleg.sviatchenko@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $file = '3._COB_pricing_09.06.21_AWO_tube_only.csv';
        $this->container->get('logger')->info(sprintf('Update %s file', $file));
        $reader = $this->createReader($file);
        foreach ($reader as $row) {
            $number   = trim($row['Item Number']);
            $price    = (float) str_replace('$', '', trim($row['P & S Price']));
            $products = $manager->getRepository('AppBundle:Product')
                ->findByItemNumber($number);
            /** @var Product $product */
            foreach ($products as $product) {
                $this->container->get('logger')->info(sprintf('IN: %s - Old Price %s - New Price %s%s', $number, $product->getPrice(), $price, PHP_EOL));
                $product->setPrice($price);
            }
            $manager->flush();
            unset($products);
        }

        $manager->flush();
    }


    /**
     * @param string $filename
     *
     * @return CsvReader
     */
    private function createReader($filename)
    {
        $path = __DIR__."/../../../DataFixtures/Data/Products/20210715/{$filename}";

        $file = new SplFileObject(
            realpath($path)
        );

        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        return $reader;
    }
}
