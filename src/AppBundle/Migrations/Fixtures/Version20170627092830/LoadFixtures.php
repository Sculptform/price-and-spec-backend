<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\Migrations\Fixtures\Version20170627092830;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use Doctrine\Common\Util\Inflector;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170627092830
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadFixtures extends AbstractAliceFixturesLoader
{
    /** @var array */
    protected $fixtures = [];

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        parent::setUp();

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var MaterialType[] $materialTypes */
        $materialTypes = $em->getRepository(MaterialType::class)->findAll();

        foreach ($materialTypes as $materialType) {
            $this->referenceRepository->addReference(
                sprintf('material_type_%s', $this->formatTitle($materialType->getTitle())),
                $materialType
            );
        }

        /** @var ProductType[] $productTypes */
        $productTypes = $em->getRepository(ProductType::class)->findAll();

        foreach ($productTypes as $productType) {
            $this->referenceRepository->addReference(
                sprintf('ptype_%s', $this->formatTitle($productType->getTitle())),
                $productType
            );
        }

        $this->fixtures = [
            __DIR__ . '/materialFinishGroups.yml',
            __DIR__ . '/materialFinishesFintraxAluminium.yml',
            __DIR__ . '/materialFinishesAcousticBlades.yml',
            __DIR__ . '/materialFinishesExpressionCladdingTimber.yml',
            __DIR__ . '/materialShapes.yml',
            __DIR__ . '/materialShapeSizes.yml',
            __DIR__ . '/object3ds.yml',
            __DIR__ . '/materials.yml',
        ];
    }

    private function formatTitle($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }
}
