<?php

namespace AppBundle\Migrations\Fixtures\Version20190401100835;

use AppBundle\Entity\Railing;
use AppBundle\Entity\ProductType;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** {@inheritdoc} */
    public function load(ObjectManager $manager)
    {
        $productType = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);

        $railing = $manager
            ->getRepository(Railing::class)
            ->findOneBy(['productType' => $productType]);

        $railing->setWidth(50);
        $railing->setHeight(50);

        $manager->flush();
    }
}
