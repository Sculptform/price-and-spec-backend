<?php

namespace AppBundle\Migrations\Fixtures\Version20180928062046;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Enum\ApplicationType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /** @var array */
    protected $fixtures = [];

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $dwgModelsDir = __DIR__.'/';
        $productType  = $manager->getRepository(ProductType::class)->findOneBy(['title' => 'Facade Blades']);
        $materials    = $manager->getRepository(Material::class)->findBy(['productType' => $productType]);

        foreach ($materials as $material) {
            /**
             * @var Material $material
             */
            $materialType = $material->getMaterialType()->getTitle();
            $shape        = $material->getMaterialShapeSize()->getMaterialShape()->getTitle();
            $size         = sprintf(
                '%sx%s',
                $material->getMaterialShapeSize()->getWidth(),
                $material->getMaterialShapeSize()->getDepth()
            );

            $path = implode(
                '/',
                [
                    $materialType,
                    $shape,
                    $size.'.dwg',
                ]
            );

            if($this->fs->exists($dwgPath = $dwgModelsDir.$path)) {
                $dwgFilename = md5($dwgPath);
                $dwgFileInfo = new \SplFileInfo($dwgPath);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($dwgPath, $tmpFilePath, true);

                $dwgFile = new UploadedFile(
                    $tmpFilePath,
                    $dwgFilename,
                    MimeTypeGuesser::getInstance()->guess($dwgPath),
                    $dwgFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $material->setDwgFile($dwgFile);
            } else {
                $this->container->get('logger')->error('DWG file loading error', [
                    $dwgPath
                ]);
            }
        }

        $manager->flush();
    }

    /**
     * @param string $name
     *
     * @return string
     */
    private function normalizeName(string $name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * @param string $imgPath
     *
     * @return bool|UploadedFile
     */
    private function checkImage(string $imgPath)
    {
        $result = false;

        if ($this->fs->exists($imgPath)) {
            $filename = md5($imgPath);
            $fileInfo = new \SplFileInfo($imgPath);

            $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

            $this->fs->copy($imgPath, $tmpFilePath, true);

            $file = new UploadedFile(
                $tmpFilePath,
                $filename,
                MimeTypeGuesser::getInstance()->guess($imgPath),
                $fileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $result = $file;
        } else {
            $this->container->get('logger')->error(
                'Image loading error',
                [
                    $imgPath,
                ]
            );
        }

        return $result;
    }
}