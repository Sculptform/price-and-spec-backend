<?php

namespace AppBundle\Migrations\Fixtures\Version20200527014636;

use AppBundle\Entity\MaterialFinish;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20200527014636
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param ObjectManager $manager
     */
    public function load(ObjectManager $manager)
    {
        $this->removeTongueAndGroove();
        $this->removeClickOnBattens();
    }

    protected function removeClickOnBattens()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var \Doctrine\DBAL\Connection $connection */
        $connection = $em->getConnection();

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $em
            ->getRepository('AppBundle:MaterialFinish')
            ->findBy(['title' => 'Pacific Teak']);

        $ids = [];
        foreach ($materialFinishes as $materialFinish) {
            $ids[] = $materialFinish->getId();
        }

        $mIn = implode(',', $ids);
        $stmt = $connection
            ->query("SELECT id FROM product WHERE material_finish_id IN({$mIn})");

        $products = array_values(
            array_unique(
                array_column($stmt->fetchAll(), 'id')
            )
        );

        $productsIn  = implode(', ', $products);
        $stmt = $connection
            ->query("SELECT project_id FROM project_product WHERE product_id IN({$productsIn}) GROUP BY project_id");

        $projects = array_values(
            array_unique(
                array_column($stmt->fetchAll(), 'project_id')
            )
        );

        $projectsIn = '"' . implode('", "', $projects) . '"';
        $connection->executeQuery("DELETE FROM project_filters WHERE project_id IN($projectsIn)");
        $connection->executeQuery("DELETE FROM project_product WHERE product_id IN({$productsIn})");
        $connection->executeQuery("DELETE FROM product WHERE id IN({$productsIn})");
        $connection->executeQuery("DELETE FROM project WHERE id IN($projectsIn)");
        $connection->executeQuery("DELETE FROM material_finish WHERE id IN({$mIn})");
    }

    protected function removeTongueAndGroove()
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        /** @var \Doctrine\DBAL\Connection $connection */
        $connection = $em->getConnection();

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $em
            ->getRepository('AppBundle:MaterialFinish')
            ->findBy(['title' => 'Pacific Teak']);

        $ids = [];
        foreach ($materialFinishes as $materialFinish) {
            $ids[] = $materialFinish->getId();
        }

        //SELECT * FROM `expression_claddings` WHERE `base_finish_id` IN(5, 247) OR `stacker_finish_id` IN(5, 247)

        $mIn = implode(', ', $ids);
        $stmt = $connection
            ->query("SELECT project_id FROM expression_claddings WHERE base_finish_id IN({$mIn}) OR stacker_finish_id IN({$mIn})");

        $projects = array_values(
            array_unique(
                array_column($stmt->fetchAll(), 'project_id')
            )
        );

        $pIn = '"' . implode('", "', $projects) . '"';

        $connection->executeQuery("DELETE FROM project_filters WHERE project_id IN($pIn)");
        $connection->executeQuery("DELETE FROM expression_claddings WHERE base_finish_id IN({$mIn}) OR stacker_finish_id IN({$mIn})");
        $connection->executeQuery("DELETE FROM project WHERE id IN($pIn)");
    }
}
