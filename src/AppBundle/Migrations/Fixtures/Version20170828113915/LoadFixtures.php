<?php

namespace AppBundle\Migrations\Fixtures\Version20170828113915;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use Doctrine\Common\Util\Inflector;

/**
 * Class LoadFixtures
 * @package AppBundle\Migrations\Fixtures\Version20170828113915
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 */
class LoadFixtures extends AbstractAliceFixturesLoader
{
    /** @var array */
    protected $fixtures = [];

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        parent::setUp();

        $em = $this->getContainer()->get('doctrine')->getManager();

        /** @var MaterialType[] $materialTypes */
        $materialTypes = $em->getRepository(MaterialType::class)->findAll();

        foreach ($materialTypes as $materialType) {
            $this->referenceRepository->addReference(
                sprintf('material_type_%s', $this->formatTitle($materialType->getTitle())),
                $materialType
            );
        }

        /** @var ProductType[] $productTypes */
        $productTypes = $em->getRepository(ProductType::class)->findAll();

        foreach ($productTypes as $productType) {
            $this->referenceRepository->addReference(
                sprintf('ptype_%s', $this->formatTitle($productType->getTitle())),
                $productType
            );
        }

        $this->referenceRepository->addReference(
            'material_finish_group_aluminium_anodising',
            $em->find(MaterialFinishGroup::class, 1)
        );

        $this->fixtures = [
            __DIR__ . '/materialFinishGroups.yml',
            __DIR__ . '/materialFinishesCCLAluminium.yml',
        ];
    }

    private function formatTitle($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return $this->fixtures;
    }
}
