<?php

namespace AppBundle\Migrations\Fixtures\Version20190729140553;

use AppBundle\Entity\{
    Material, Object3D, ProductType
};

use Doctrine\Common\DataFixtures\{
    AbstractFixture, FixtureInterface,
};

use Symfony\Component\DependencyInjection\{
    ContainerAwareTrait, ContainerAwareInterface
};

use Doctrine\Common\Persistence\ObjectManager;

/**
 * Class LoadFixtures
 *
 * @package AppBundle\Migrations\Fixtures\Version20190729140553
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadFixtures extends AbstractFixture implements FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait, SelectTrait, UpdateTrait;

    public function load(ObjectManager $manager)
    {
        $deletedMaterials = [];

        $productType = $manager->getRepository(ProductType::class)
            ->findOneBy(['title' => 'Tongue & Groove Cladding']);

        foreach (['Element 32x32', 'Element 32x42', 'Element 32x60'] as $title) {
            $object3D = $manager->getRepository(Object3D::class)
                ->findOneBy(['title' => $title]);

            if (!$object3D) {
                continue;
            }

            $material = $object3D = $manager->getRepository(Material::class)
                ->findOneBy([
                    'productType'  => $productType,
                    'object3d'     => $object3D,
                ]);

            if ($material) {
                $material->setSoftDeleted(true);
                $deletedMaterials[] = $material->getId();
            }
            $manager->flush();
        }

        $projects = $this->projectsByMaterials($deletedMaterials);
        if ($projects) {
            $this->markAsOutdated($projects);
        }

        $templates = $this->projectsByMaterials($deletedMaterials, 1);
        if ($templates) {
            $this->delete($templates);
        }
    }
}
