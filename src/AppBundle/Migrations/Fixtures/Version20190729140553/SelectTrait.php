<?php

namespace AppBundle\Migrations\Fixtures\Version20190729140553;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;

/**
 * Trait RepositoryTrait
 */
trait SelectTrait
{
    /**
     * @param array $data
     * @param int   $templates
     *
     * @return mixed[]|null
     *
     * @throws DBALException
     */
    public function projectsByMaterials(array $data, $templates = 0)
    {
        $where = implode(', ', $data);

        $query = /** @lang text */
            "SELECT p.id FROM project p
                LEFT JOIN expression_claddings pp ON(pp.project_id = p.id)
                LEFT JOIN product pr ON(pr.id = pp.stacker_product_id)
            WHERE 
                p.is_template = {$templates} AND pr.material_id IN({$where}) GROUP BY p.id";

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()->executeQuery($query)->fetchAll();

        return (count($response) > 0) ? array_column($response, 'id') : null;


    }



}
