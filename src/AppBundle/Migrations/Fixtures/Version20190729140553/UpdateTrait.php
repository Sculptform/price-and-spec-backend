<?php

namespace AppBundle\Migrations\Fixtures\Version20190729140553;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;

/**
 * Trait UpdateTrait
 */
trait UpdateTrait
{
    /**
     * @param array $data
     *
     * @throws DBALException
     */
    public function markAsOutdated(array $data)
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');

        $queries = [];
        foreach ($data as $id) {
            $queries[] = /** @lang text */
                "UPDATE project SET outdated = 1 WHERE id = '{$id}';";
        }

        $str = implode('', $queries);
        $em->getConnection()->executeQuery($str);
    }

    /**
     * @param array $data
     *
     * @throws DBALException
     */
    public function delete(array $data)
    {
        /** @var EntityManager $em */
        $em  = $this->container->get('doctrine.orm.entity_manager');

        $queries = [];
        foreach ($data as $id) {
            $queries[] = /** @lang text */
                "DELETE FROM project WHERE id = '{$id}';";
        }

        $str = implode('', $queries);
        $em->getConnection()->executeQuery($str);
    }
}
