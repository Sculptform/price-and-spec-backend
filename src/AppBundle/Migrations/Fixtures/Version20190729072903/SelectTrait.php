<?php

namespace AppBundle\Migrations\Fixtures\Version20190729072903;

use Doctrine\ORM\EntityManager;
use Doctrine\DBAL\DBALException;

/**
 * Trait RepositoryTrait
 */
trait SelectTrait
{
    /**
     * @param array $data
     * @param int   $templates
     *
     * @return mixed[]|null
     *
     * @throws DBALException
     */
    public function projectsByMaterials(array $data, $templates = 0)
    {
        $where = implode(', ', $data);

        $query = /** @lang text */
            "SELECT p.id FROM project p
	            LEFT JOIN project_product pp ON(pp.project_id = p.id)
	            LEFT JOIN product pr ON(pr.id = pp.product_id)
            WHERE 
	            p.is_template = {$templates} AND pr.material_id IN({$where}) GROUP BY p.id";

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        $response = $em
            ->getConnection()->executeQuery($query)->fetchAll();

        return (count($response) > 0) ? array_column($response, 'id') : null;
    }
}
