<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170531124552 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fintraxes (id INT AUTO_INCREMENT NOT NULL, project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', product_id INT DEFAULT NULL, material_finish_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, INDEX IDX_C6028A3A166D1F9C (project_id), INDEX IDX_C6028A3A4584665A (product_id), INDEX IDX_C6028A3A13D87D0B (material_finish_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('CREATE TABLE fintrax_stackers (fintrax_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_CF8EDD4BC7C60B69 (fintrax_id), INDEX IDX_CF8EDD4B4584665A (product_id), PRIMARY KEY(fintrax_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_CF8EDD4BC7C60B69 FOREIGN KEY (fintrax_id) REFERENCES fintraxes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_CF8EDD4B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintrax_stackers DROP FOREIGN KEY FK_CF8EDD4BC7C60B69');
        $this->addSql('DROP TABLE fintraxes');
        $this->addSql('DROP TABLE fintrax_stackers');
    }
}
