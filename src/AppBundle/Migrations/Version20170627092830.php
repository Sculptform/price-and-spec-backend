<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627092830 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20170627092830\LoadFixtures()]);

        $em = $this->container->get('doctrine.orm.entity_manager');

        $finishGroup = $em->getRepository(MaterialFinishGroup::class)
            ->findOneBy(['productType' => 1, 'title' => 'Species']);

        /** @var MaterialFinish[] $finishes */
        $finishes = $em->getRepository(MaterialFinish::class)->findBy(['group' => $finishGroup]);

        foreach ($finishes as $finish) {
            switch ($finish->getTitle()) {
                case 'White Oak': $finish->setApplicationTypes([1]); break;
                case 'Vic Ash': $finish->setApplicationTypes([1]); break;
                case 'Banjo Pine': $finish->setApplicationTypes([1]); break;
                case 'Walnut': $em->remove($finish); break;
            }
        }

        $em->flush();
    }
}
