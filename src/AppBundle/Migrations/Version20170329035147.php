<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170329035147 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE timber_coating_type (id INT AUTO_INCREMENT NOT NULL, title VARCHAR(255) NOT NULL, description VARCHAR(255) NOT NULL, application_types LONGTEXT NOT NULL COMMENT \'(DC2Type:array)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE product ADD cutek_price DOUBLE PRECISION DEFAULT NULL, ADD enviropro_price DOUBLE PRECISION DEFAULT NULL, CHANGE original_price natural_accent_price DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD timber_coating_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC07866C5 FOREIGN KEY (timber_coating_type_id) REFERENCES timber_coating_type (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEC07866C5 ON project (timber_coating_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC07866C5');
        $this->addSql('DROP TABLE timber_coating_type');
        $this->addSql('ALTER TABLE product ADD original_price DOUBLE PRECISION DEFAULT NULL, DROP natural_accent_price, DROP cutek_price, DROP enviropro_price');
        $this->addSql('DROP INDEX IDX_2FB3D0EEC07866C5 ON project');
        $this->addSql('ALTER TABLE project DROP timber_coating_type_id');
    }
}
