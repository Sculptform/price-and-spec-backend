<?php declare(strict_types = 1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use AppBundle\Entity\MaterialFinish;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180615110037 extends AbstractFixtureMigration
{
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20180615110037\LoadFixtures()]);

        /**
         * @var EntityManager $em
         */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        try{
            $titles = ['Yakisugi Ash', 'Yakisugi Pine'];

            foreach ($titles as $item){
                $finish = $em->getRepository(MaterialFinish::class)->findOneBy(['title' => $item]);
                if($finish) $em->remove($finish);
            }


            $em->flush();
        }catch (ORMException $ex){
            dump($ex->getMessage());
        };

    }
}
