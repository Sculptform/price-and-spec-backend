<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170607104802 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_shape ADD parent_id INT DEFAULT NULL, ADD is_show_sub_shapes TINYINT(1) DEFAULT \'1\' NOT NULL, ADD root INT DEFAULT NULL, ADD lvl INT NOT NULL, ADD lft INT NOT NULL, ADD rgt INT NOT NULL');
        $this->addSql('ALTER TABLE material_shape ADD CONSTRAINT FK_8BD75396727ACA70 FOREIGN KEY (parent_id) REFERENCES material_shape (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_8BD75396727ACA70 ON material_shape (parent_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_shape DROP FOREIGN KEY FK_8BD75396727ACA70');
        $this->addSql('DROP INDEX IDX_8BD75396727ACA70 ON material_shape');
        $this->addSql('ALTER TABLE material_shape DROP parent_id, DROP is_show_sub_shapes, DROP root, DROP lvl, DROP lft, DROP rgt');
    }
}
