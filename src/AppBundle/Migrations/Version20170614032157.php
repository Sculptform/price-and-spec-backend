<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614032157 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9166D1F9C');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gallery_project DROP FOREIGN KEY FK_17C991C6166D1F9C');
        $this->addSql('ALTER TABLE gallery_project ADD CONSTRAINT FK_17C991C6166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE project_product DROP FOREIGN KEY FK_455408C8166D1F9C');
        $this->addSql('ALTER TABLE project_product ADD CONSTRAINT FK_455408C8166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_rating_user DROP FOREIGN KEY FK_8124B237166D1F9C');
        $this->addSql('ALTER TABLE project_rating_user ADD CONSTRAINT FK_8124B237166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4166D1F9C');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4A76ED395');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE scene DROP FOREIGN KEY FK_D979EFDA166D1F9C');
        $this->addSql('ALTER TABLE scene ADD CONSTRAINT FK_D979EFDA166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEDDAABD01');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEDDAABD01 FOREIGN KEY (acoustic_backing_id) REFERENCES acoustic_backing (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE notification_quotation DROP FOREIGN KEY FK_E1A3CAE6B4EA4E60');
        $this->addSql('ALTER TABLE notification_quotation ADD CONSTRAINT FK_E1A3CAE6B4EA4E60 FOREIGN KEY (quotation_id) REFERENCES quotation (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE notification_share DROP FOREIGN KEY FK_29B278A3166D1F9C');
        $this->addSql('ALTER TABLE notification_share ADD CONSTRAINT FK_29B278A3166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE sample_request DROP FOREIGN KEY FK_2BDD4209166D1F9C');
        $this->addSql('ALTER TABLE sample_request ADD CONSTRAINT FK_2BDD4209166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_comments DROP FOREIGN KEY FK_62D01DF1166D1F9C');
        $this->addSql('ALTER TABLE project_comments ADD CONSTRAINT FK_62D01DF1166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE gallery_project DROP FOREIGN KEY FK_17C991C6166D1F9C');
        $this->addSql('ALTER TABLE gallery_project ADD CONSTRAINT FK_17C991C6166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE notification_quotation DROP FOREIGN KEY FK_E1A3CAE6B4EA4E60');
        $this->addSql('ALTER TABLE notification_quotation ADD CONSTRAINT FK_E1A3CAE6B4EA4E60 FOREIGN KEY (quotation_id) REFERENCES quotation (id)');
        $this->addSql('ALTER TABLE notification_share DROP FOREIGN KEY FK_29B278A3166D1F9C');
        $this->addSql('ALTER TABLE notification_share ADD CONSTRAINT FK_29B278A3166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEDDAABD01');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEDDAABD01 FOREIGN KEY (acoustic_backing_id) REFERENCES acoustic_backing (id)');
        $this->addSql('ALTER TABLE project_comments DROP FOREIGN KEY FK_62D01DF1166D1F9C');
        $this->addSql('ALTER TABLE project_comments ADD CONSTRAINT FK_62D01DF1166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_product DROP FOREIGN KEY FK_455408C8166D1F9C');
        $this->addSql('ALTER TABLE project_product ADD CONSTRAINT FK_455408C8166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_rating_user DROP FOREIGN KEY FK_8124B237166D1F9C');
        $this->addSql('ALTER TABLE project_rating_user ADD CONSTRAINT FK_8124B237166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9166D1F9C');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE sample_request DROP FOREIGN KEY FK_2BDD4209166D1F9C');
        $this->addSql('ALTER TABLE sample_request ADD CONSTRAINT FK_2BDD4209166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE scene DROP FOREIGN KEY FK_D979EFDA166D1F9C');
        $this->addSql('ALTER TABLE scene ADD CONSTRAINT FK_D979EFDA166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4A76ED395');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4166D1F9C');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4A76ED395 FOREIGN KEY (user_id) REFERENCES user (id)');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }
}
