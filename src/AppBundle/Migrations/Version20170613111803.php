<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170613111803 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintrax_stackers DROP FOREIGN KEY FK_8313616AC7C60B69');
        $this->addSql('CREATE TABLE fintrax_products (id INT AUTO_INCREMENT NOT NULL, project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\', base_product_id INT DEFAULT NULL, nosing_product_id INT DEFAULT NULL, material_finish_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL, INDEX IDX_48EAA96A166D1F9C (project_id), INDEX IDX_48EAA96AD63EB556 (base_product_id), INDEX IDX_48EAA96A9CB80083 (nosing_product_id), INDEX IDX_48EAA96A13D87D0B (material_finish_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintrax_products ADD CONSTRAINT FK_48EAA96A166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintrax_products ADD CONSTRAINT FK_48EAA96AD63EB556 FOREIGN KEY (base_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintrax_products ADD CONSTRAINT FK_48EAA96A9CB80083 FOREIGN KEY (nosing_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintrax_products ADD CONSTRAINT FK_48EAA96A13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE fintraxes');
        $this->addSql('DROP INDEX IDX_CF8EDD4BC7C60B69 ON fintrax_stackers');
        $this->addSql('ALTER TABLE fintrax_stackers CHANGE fintrax_id fintrax_product_id INT NOT NULL');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_CF8EDD4BBE76085F FOREIGN KEY (fintrax_product_id) REFERENCES fintrax_products (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_CF8EDD4BBE76085F ON fintrax_stackers (fintrax_product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintrax_stackers DROP FOREIGN KEY FK_CF8EDD4BBE76085F');
        $this->addSql('CREATE TABLE fintraxes (id INT AUTO_INCREMENT NOT NULL, material_finish_id INT DEFAULT NULL, project_id CHAR(36) NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:guid)\', nosing_product_id INT DEFAULT NULL, base_product_id INT DEFAULT NULL, title VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_C6028A3A166D1F9C (project_id), INDEX IDX_C6028A3A13D87D0B (material_finish_id), INDEX IDX_C6028A3AD63EB556 (base_product_id), INDEX IDX_C6028A3A9CB80083 (nosing_product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3A9CB80083 FOREIGN KEY (nosing_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintraxes ADD CONSTRAINT FK_C6028A3AD63EB556 FOREIGN KEY (base_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('DROP TABLE fintrax_products');
        $this->addSql('DROP INDEX IDX_CF8EDD4BBE76085F ON fintrax_stackers');
        $this->addSql('ALTER TABLE fintrax_stackers CHANGE fintrax_product_id fintrax_id INT NOT NULL');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_8313616AC7C60B69 FOREIGN KEY (fintrax_id) REFERENCES fintraxes (id) ON DELETE CASCADE');
        $this->addSql('CREATE INDEX IDX_CF8EDD4BC7C60B69 ON fintrax_stackers (fintrax_id)');
    }
}
