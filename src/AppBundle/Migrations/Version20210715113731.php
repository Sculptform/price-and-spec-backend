<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210715113731 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql("UPDATE acoustic_backing SET price = '37.07' WHERE title='No Backing'");
        $this->addSql("UPDATE acoustic_backing SET price = '83.6' WHERE title='Group 1 Rated'");
    }

    public function down(Schema $schema) : void
    {
        $this->addSql("UPDATE acoustic_backing SET price = '33.7' WHERE title='No Backing'");
        $this->addSql("UPDATE acoustic_backing SET price = '72.7' WHERE title='Group 1 Rated'");
    }
}
