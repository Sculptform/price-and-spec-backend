<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Provider\UploadedFileProvider;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\Product;
use AppBundle\Entity\Scene;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170919094511 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $uploadedFileProvider = UploadedFileProvider::create();
        $basePath = __DIR__ . '/../DataFixtures/Data/Finishes/ECL/Timber/Species';

        $speciesECLfinishGroup = $em->find(MaterialFinishGroup::class, 23);

        $finishes = $speciesECLfinishGroup->getMaterialFinishes();

        foreach ($finishes as $materialFinish) {
            $this->container->get('logger')->err('Updating finish..', [$materialFinish->getTitle()]);

            $textureFile = $uploadedFileProvider->uploadedFile(
                sprintf('%s.jpg', $materialFinish->getTitle()),
                null,
                $basePath
            );

            $materialFinish->setTextureFile($textureFile);

            $em->flush($materialFinish);

            $this->postUpdateECLFinish($materialFinish, $em);
        }
    }

    private function postUpdateECLFinish(MaterialFinish $finish, EntityManager $em)
    {
        $hostUrl = $this->container->getParameter('host');

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('scene')
            ->distinct(true)
            ->join('scene.project', 'project')
            ->join('project.expressionCladdingProducts', 'expressionCladdingProduct')
            ->where('expressionCladdingProduct.baseFinish = :materialFinish')
            ->setParameter('materialFinish', $finish)
            ->getQuery();

        foreach ($scenesQuery->iterate() as $sceneEntity) {
            /** @var Scene $sceneEntity */
            $sceneEntity = $sceneEntity[0];
            $sceneData = $sceneEntity->getScene();

            $this->container->get('logger')->err('Updating scenes..', [
                $sceneEntity->getProject()->getId()
            ]);

            $scene = &$this->getSubArray($sceneData, 'scene');
            $sceneObject = &$this->getSubArray($scene, 'object');
            $sceneChildren = &$this->getSubArray($sceneObject, 'children');
            $sceneMaterials = &$this->getSubArray($scene, 'materials');
            $sceneTextures = &$this->getSubArray($scene, 'textures');
            $sceneImages = &$this->getSubArray($scene, 'images');

            $sceneChildren = is_array($sceneChildren) ? $sceneChildren : [];

            foreach ($sceneChildren as &$sceneChild) {
                $product = null;

                if (array_key_exists('productModel', $sceneChild)) {
                    $product = $em->find(Product::class, $sceneChild['productModel']);
                }

                if (
                    (
                        array_key_exists('materialFinishModel', $sceneChild) &&
                        $sceneChild['materialFinishModel'] == $finish->getId()
                    ) ||
                    (
                        $product && $product->getMaterialFinishId() == $finish->getId()
                    )
                ) {
                    if (array_key_exists('material', $sceneChild)) {
                        $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                        if (false !== $materialKey) {
                            $material = &$sceneMaterials[$materialKey];

                            if (array_key_exists('map', $material)) {
                                $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                if (false !== $sceneTextureKey) {
                                    $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                    if (array_key_exists('image', $sceneTexture)) {
                                        $sceneImageKey = $this->findKey(
                                            $sceneImages,
                                            'uuid',
                                            $sceneTexture['image']
                                        );

                                        if (false !== $sceneImageKey) {
                                            $sceneImages[$sceneImageKey]['url'] = $hostUrl . '/' . $finish->getTextureWebPath();
                                        }
                                    }

                                    unset($sceneTexture);
                                }
                            }

                            $material['color'] = $finish->getDecimalColor();

                            unset($material);
                        }
                    }
                }

                gc_collect_cycles();
            }

            $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);
            $sceneEntity->setScene($sceneData);
            $em->flush();

            unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);

            gc_collect_cycles();
        }
    }

    private function &getSubArray(&$array, $key)
    {
        if (!array_key_exists($key, $array)) {
            $array[$key] = [];
        }

        return $array[$key];
    }

    private function findKey($array, $searchKey, $searchValue)
    {
        $key = array_search($searchValue, array_column($array, $searchKey));

        return $key === false ? false : array_keys($array)[$key];
    }
}
