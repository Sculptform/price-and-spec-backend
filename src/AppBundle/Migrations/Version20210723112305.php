<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20210723112305 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('update material_finish set title = REPLACE(title, "White Oak", "American Oak") where title like "%White Oak%" and title not like "%American White Oak%"');
        $this->addSql('update material_finish set title = "American Oak" where title = "American White Oak"');

        $this->addSql('update railing_finish set title = REPLACE(title, "White Oak", "American Oak") where title like "%White Oak%"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }
}
