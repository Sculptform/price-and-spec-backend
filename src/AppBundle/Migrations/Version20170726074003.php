<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Object3D;
use AppBundle\Entity\Product;
use AppBundle\Entity\Project;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Railing;
use AppBundle\Entity\Scene;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170726074003 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        gc_enable();

        ini_set('memory_limit','-1');

        $em = $this->container->get('doctrine.orm.entity_manager');

        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $deletedProjects = [];

        $projectsQuery = $em->getRepository(Project::class)->createQueryBuilder('p')->getQuery();

        /** @var Project $project */
        foreach ($projectsQuery->iterate() as $project) {
            $project = $project[0];

            if (!$project->getOwner()) {
                $deletedProjects[] = $project->getId();
                $em->remove($project);
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('s')
            ->orderBy('s.project', 'asc')
            ->addOrderBy('s.createdAt', 'desc')
            ->getQuery()
        ;

        $projectScenes = [];

        /** @var Scene $scene */
        foreach ($scenesQuery->iterate() as $scene) {
            $scene = $scene[0];
            $projectScenes[$scene->getProject()->getId()][] = $scene;
        }

        foreach ($projectScenes as $projectId => $projectScenesByProjectId) {
            array_shift($projectScenesByProjectId);
            foreach ((array)$projectScenesByProjectId as $projectScene) {
                $em->remove($projectScene);
            }
        }

        unset($projectScenes);

        $em->flush();
        $em->clear();
        gc_collect_cycles();

        $peakObjects3dQuery = $em->getRepository(Object3D::class)
            ->createQueryBuilder('o')
            ->where('LOWER(o.title) LIKE LOWER(:title1)')
            ->andWhere('LOWER(o.title) NOT LIKE LOWER(:title2)')
            ->setParameter('title1', 'peak%')
            ->setParameter('title2', 'peak %')
            ->getQuery();

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('s')
            ->getQuery()
        ;

        /** @var Scene $sceneEntity */
        foreach ($scenesQuery->iterate() as $sceneEntity) {
            $sceneEntity = $sceneEntity[0];
            $sceneData = $sceneEntity->getScene();

            if (!is_array($sceneData)) {
                $sceneData = (array)json_decode((string)$sceneData, true);
            }

            $scene = &$sceneData['scene'];

            $sceneObjects = &$scene['object']['children'];
            $sceneGeometries = &$scene['geometries'];

            $project = $sceneEntity->getProject();

            /** @var ProjectProduct[] $projectProducts */
            $projectProducts = $project->getProjectProducts();

            /** @var Railing $defaultRailing */
            $defaultRailing = $em->getRepository(Railing::class)->findOneBy(['productType'=>$project->getProductType()]);

            $project->setRailing($defaultRailing);

            $em->persist($project);

            if ($sceneObjects) {
                foreach ($sceneObjects as &$sceneObject) {
                    if (!isset($sceneObject['woodformShapeType'])) {
                        $sceneObject['woodformShapeType'] = '';
                    }

                    if (isset($sceneObject['geometry'])) {
                        $sceneGeometryKey = array_search($sceneObject['geometry'], array_column((array)$sceneGeometries, 'uuid'));

                        if (null !== $sceneGeometryKey) {
                            $sceneGeometry = &$sceneGeometries[$sceneGeometryKey];

                            $sceneGeometryVertices = &$sceneGeometry['data']['vertices'];

                            foreach ($peakObjects3dQuery->iterate() as $peakObject3d) {
                                /** @var Object3D $peakObject3d */
                                $peakObject3d = $peakObject3d[0];
                                $peakObject3dVertices = (array)json_decode($peakObject3d->getValue(), true)["vertices"];
                                if ($sceneGeometryVertices == $peakObject3dVertices) {
                                    $sceneObject['canFlip'] = true;
                                }
                            }
                        }
                    }

                    if (isset($sceneObject['productModel'])) {
                        $isProductFound = false;

                        foreach ($projectProducts as $projectProduct) {
                            if ($projectProduct->getProductId() == $sceneObject['productModel']) {
                                $isProductFound = true;
                                break;
                            }
                        }

                        if (!$isProductFound) {
                            $product = $em->find(Product::class, $sceneObject['productModel']);

                            if ($product) {
                                $projectProduct = new ProjectProduct();

                                $projectProduct
                                    ->setHeight(1)
                                    ->setProduct($product)
                                    ->setProject($project)
                                    ->setUuid($sceneObject['uuid']);

                                $em->persist($projectProduct);
                            } else {
                                $sceneObject = null;
                            }
                        }
                    }
                }

                $sceneObjects = array_filter($sceneObjects, 'is_array');

                $projectProducts = $project->getProjectProducts();

                foreach ($projectProducts as $projectProduct) {
                    $product = $projectProduct->getProduct();
                    $material = $product->getMaterial();

                    $sceneObjectKey = array_search($projectProduct->getUuid(), array_column((array)$sceneObjects, 'uuid'));

                    if (null !== $sceneObjectKey) {
                        $sceneObject = &$sceneObjects[$sceneObjectKey];

                        $sceneObject['woodformShapeType'] = $material->getMaterialShapeSize()->getMaterialShape()->getName();

                        if (!isset($sceneObject['materialModel'])) {
                            $sceneObject['materialModel'] = $material->getId();
                        }
                    }
                }
            }

            $sceneEntity->setScene($sceneData);

            $em->persist($sceneEntity);

            $em->flush();
            $em->clear();
            gc_collect_cycles();
        }
    }
}
