<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170519081158 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE material_shape_preview');
        $this->addSql('ALTER TABLE material_shape DROP application_types');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE material_shape_preview (id INT AUTO_INCREMENT NOT NULL, material_shape_id INT NOT NULL, material_type_id INT NOT NULL, image_path VARCHAR(255) DEFAULT NULL COLLATE utf8_unicode_ci, INDEX IDX_FF122D0516FE4819 (material_shape_id), INDEX IDX_FF122D0574D6573C (material_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE material_shape_preview ADD CONSTRAINT FK_FF122D0516FE4819 FOREIGN KEY (material_shape_id) REFERENCES material_shape (id)');
        $this->addSql('ALTER TABLE material_shape_preview ADD CONSTRAINT FK_FF122D0574D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id)');
        $this->addSql('ALTER TABLE material_shape ADD application_types LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:array)\'');
    }
}
