<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181218133227 extends AbstractMigration
{
    public function up(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("UPDATE material_coating SET title='Clear Poly' WHERE title='Natural Accent'");
        $this->addSql("UPDATE material_coating SET title='Clear Oil' WHERE title='Cutek Clear Oil'");
        $this->addSql("UPDATE material_coating SET title='Enviropro Exterior Coating' WHERE title='Enviropro'");

    }

    public function down(Schema $schema): void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql("UPDATE material_coating SET title='Natural Accent' WHERE title='Clear Poly'");
        $this->addSql("UPDATE material_coating SET title='Cutek Clear Oil' WHERE title='Clear Oil'");
        $this->addSql("UPDATE material_coating SET title='Enviropro' WHERE title='Enviropro Exterior Coating'");

    }
}
