<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171117071004 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_shape_size ADD cover_width DOUBLE PRECISION DEFAULT NULL');

        $this->addSql('ALTER TABLE material_shape_size CHANGE width width DOUBLE PRECISION NOT NULL, CHANGE depth depth DOUBLE PRECISION NOT NULL');

        $this->addSql('UPDATE material_shape_size SET cover_width=35 WHERE material_shape_id=8 AND width=32');
        $this->addSql('UPDATE material_shape_size SET cover_width=48.5 WHERE material_shape_id=9 AND width=68');
        $this->addSql('UPDATE material_shape_size SET cover_width=68.5 WHERE material_shape_id=9 AND width=88');
        $this->addSql('UPDATE material_shape_size SET cover_width=118.5 WHERE material_shape_id=9 AND width=138');
        $this->addSql('UPDATE material_shape_size SET cover_width=48.5 WHERE material_shape_id=11 AND width=68');
        $this->addSql('UPDATE material_shape_size SET cover_width=68.5 WHERE material_shape_id=11 AND width=88');
        $this->addSql('UPDATE material_shape_size SET cover_width=118.5 WHERE material_shape_id=11 AND width=138');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_shape_size DROP cover_width');

        $this->addSql('ALTER TABLE material_shape_size CHANGE width width NUMERIC(10, 0) NOT NULL, CHANGE depth depth NUMERIC(10, 0) NOT NULL');
    }
}
