<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190409121444 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE product_type ADD price_include_note LONGTEXT DEFAULT NULL, ADD price_exclude_note LONGTEXT DEFAULT NULL, ADD price_note LONGTEXT DEFAULT NULL'
        );
    }

    public function down(Schema $schema) : void
    {
        $this->abortIf(
            $this->connection->getDatabasePlatform()->getName() !== 'mysql',
            'Migration can only be executed safely on \'mysql\'.'
        );

        $this->addSql(
            'ALTER TABLE product_type DROP price_include_note, DROP price_exclude_note, DROP price_note'
        );
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20190409121444\LoadFixtures()]);
    }
}
