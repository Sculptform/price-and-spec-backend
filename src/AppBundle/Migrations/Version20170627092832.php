<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627092832 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings ADD product_type_id INT NOT NULL, ADD description VARCHAR(255) DEFAULT NULL, ADD image_path VARCHAR(255) DEFAULT NULL, ADD created_at DATETIME NOT NULL, ADD updated_at DATETIME NOT NULL');
        $this->addSql('ALTER TABLE railings ADD CONSTRAINT FK_3278494E14959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id)');
        $this->addSql('CREATE INDEX IDX_3278494E14959723 ON railings (product_type_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings DROP FOREIGN KEY FK_3278494E14959723');
        $this->addSql('DROP INDEX IDX_3278494E14959723 ON railings');
        $this->addSql('ALTER TABLE railings DROP product_type_id, DROP description, DROP image_path, DROP created_at, DROP updated_at');
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20170707115128\LoadFixtures()]);
        $this->loadFixtures([new Fixtures\Version20170627092832\LoadFixtures()]);
    }
}
