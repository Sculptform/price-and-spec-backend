<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170410050219 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9166D1F9C');
        $this->addSql('ALTER TABLE team_projects DROP FOREIGN KEY FK_E4D16FDA166D1F9C');
        $this->addSql('ALTER TABLE gallery_project DROP FOREIGN KEY FK_17C991C6166D1F9C');
        $this->addSql('ALTER TABLE feed_projects DROP FOREIGN KEY FK_7C462403166D1F9C');
        $this->addSql('ALTER TABLE project_product DROP FOREIGN KEY FK_455408C8166D1F9C');
        $this->addSql('ALTER TABLE project_rating_user DROP FOREIGN KEY FK_8124B237166D1F9C');
        $this->addSql('ALTER TABLE user_project DROP FOREIGN KEY FK_77BECEE4166D1F9C');
        $this->addSql('ALTER TABLE scene DROP FOREIGN KEY FK_D979EFDA166D1F9C');
        $this->addSql('ALTER TABLE project_material_coatings DROP FOREIGN KEY FK_1C9E2515166D1F9C');
        $this->addSql('ALTER TABLE notification_share DROP FOREIGN KEY FK_29B278A3166D1F9C');
        $this->addSql('ALTER TABLE notification_project DROP FOREIGN KEY FK_3AAD5A59166D1F9C');
        $this->addSql('ALTER TABLE comment_projects DROP FOREIGN KEY FK_41D7D0A2166D1F9C');
        $this->addSql('ALTER TABLE project_comments DROP FOREIGN KEY FK_62D01DF1166D1F9C');
        $this->addSql('ALTER TABLE shared_content DROP FOREIGN KEY FK_DF123C1E166D1F9C');

        $this->addSql('ALTER TABLE quotation CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE team_projects CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE gallery_project CHANGE project_id project_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE feed_projects CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE project_product CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE project_rating_user CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE user_project CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE scene CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE project CHANGE id id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE project_material_coatings CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE shared_content CHANGE project_id project_id CHAR(36) DEFAULT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE notification_project CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE notification_share CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE comment_projects CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');
        $this->addSql('ALTER TABLE project_comments CHANGE project_id project_id CHAR(36) NOT NULL COMMENT \'(DC2Type:guid)\'');

        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE team_projects ADD CONSTRAINT FK_E4D16FDA166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE gallery_project ADD CONSTRAINT FK_17C991C6166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE feed_projects ADD CONSTRAINT FK_7C462403166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_product ADD CONSTRAINT FK_455408C8166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_rating_user ADD CONSTRAINT FK_8124B237166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE user_project ADD CONSTRAINT FK_77BECEE4166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE scene ADD CONSTRAINT FK_D979EFDA166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE project_material_coatings ADD CONSTRAINT FK_1C9E2515166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE shared_content ADD CONSTRAINT FK_DF123C1E166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE notification_project ADD CONSTRAINT FK_3AAD5A59166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE notification_share ADD CONSTRAINT FK_29B278A3166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
        $this->addSql('ALTER TABLE comment_projects ADD CONSTRAINT FK_41D7D0A2166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_comments ADD CONSTRAINT FK_62D01DF1166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE comment_projects CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE feed_projects CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE gallery_project CHANGE project_id project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE notification_project CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE notification_share CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE project CHANGE id id INT AUTO_INCREMENT NOT NULL');
        $this->addSql('ALTER TABLE project_comments CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE project_material_coatings CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE project_product CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE project_rating_user CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE quotation CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE scene CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE shared_content CHANGE project_id project_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE team_projects CHANGE project_id project_id INT NOT NULL');
        $this->addSql('ALTER TABLE user_project CHANGE project_id project_id INT NOT NULL');
    }
}
