<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Provider\UploadedFileProvider;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Repository\MaterialFinishGroupRepository;
use AppBundle\Repository\MaterialShapeRepository;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\Exception\FileNotFoundException;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170907031430 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        /** @var MaterialShapeRepository $materialShapeRepo */
        $materialShapeRepo = $em->getRepository(MaterialShape::class);

        /** @var MaterialFinishGroupRepository $materialFinishGroupRepo */
        $materialFinishGroupRepo = $em->getRepository(MaterialFinishGroup::class);

        $materialTypeAluminium = $em->find(MaterialType::class, 2);

        $productTypeFTX = $em->find(ProductType::class, 3);
        $productTypeECL = $em->find(ProductType::class, 2);
        $productTypeCCL = $em->find(ProductType::class, 1);

        $uploadedFileProvider = new UploadedFileProvider();

        $fbbShape = new MaterialShape();

        $fbbShape
            ->setTitle('Fin Building Blocks')
            ->setName('fin_building_blocks')
        ;

        $em->persist($fbbShape);

        $fbbChildren = $materialShapeRepo->findBy([
            'name' => ['base_stacker', 'stacker', 'nosing']
        ]);

        foreach ($fbbChildren as $fbbChild) {
            $materialShapeRepo->persistAsLastChildOf($fbbChild, $fbbShape);
        }

        $finColorsGroup = new MaterialFinishGroup();

        $finColorsImageFile = $uploadedFileProvider->uploadedFile('Data/FinishGroups/FTX/Fin Colours.png');

        $finColorsGroup
            ->setTitle('Fin Colours')
            ->setApplicationTypes([2])
            ->setMaterialType($materialTypeAluminium)
            ->setProductType($productTypeFTX)
            ->setImageFile($finColorsImageFile)
        ;

        $em->persist($finColorsGroup);

        $finColorsGroupChildren = $materialFinishGroupRepo->findBy([
            'title' => ['Anodising', 'Woodgrain', 'Powder Coating'],
            'productType' => $productTypeFTX,
            'materialType' => $materialTypeAluminium
        ]);

        foreach ($finColorsGroupChildren as $finColorsGroupChild) {
            $materialFinishGroupRepo->persistAsLastChildOf($finColorsGroupChild, $finColorsGroup);
        }

        $em->flush();

        $productTypeShortTitles = [
            1 => 'CCL',
            2 => 'ECL',
            3 => 'FTX'
        ];

        /** @var MaterialFinishGroup[] $cclFinishGroups */
        $cclFinishGroups = $materialFinishGroupRepo->findBy([
            'productType' => [$productTypeCCL, $productTypeECL]
        ]);

        foreach ($cclFinishGroups as $finishGroup) {
            try {
                $productTypeShortTitle = $productTypeShortTitles[$finishGroup->getProductTypeId()];

                $imagePath = sprintf(
                    'Data/FinishGroups/%s/%s/%s.png',
                    $productTypeShortTitle,
                    $finishGroup->getMaterialType()->getTitle(),
                    $finishGroup->getTitle()
                );

                $imageFile = $uploadedFileProvider->uploadedFile($imagePath);

                $finishGroup->setImageFile($imageFile);
            } catch (FileNotFoundException $exception) {
                $this->container->get('logger')->err('Finish group image not found!', [
                    $finishGroup->getTitle()
                ]);
            }
        }

        /** @var MaterialShape[] $cclShapes */
        $cclShapes = $materialShapeRepo->findAll();

        foreach ($cclShapes as $shape) {
            try {
                $imagePath = sprintf('Data/MaterialShapes/%s.png', $shape->getName());
                $imageFile = $uploadedFileProvider->uploadedFile($imagePath);
                $shape->setImageFile($imageFile);
            } catch (FileNotFoundException $exception) {
                $this->container->get('logger')->err('Shape image not found!', [
                    $shape->getName()
                ]);
            }
        }

        $em->flush();
    }
}
