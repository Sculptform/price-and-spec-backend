<?php declare(strict_types = 1);

namespace AppBundle\Migrations;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Project;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\ORMException;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180705114300 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema)
    {
        $this->addSql('ALTER TABLE notification_project DROP FOREIGN KEY FK_3AAD5A59166D1F9C');
        $this->addSql('ALTER TABLE notification_project ADD CONSTRAINT FK_3AAD5A59166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');

    }

    public function down(Schema $schema)
    {
        $this->addSql('ALTER TABLE notification_project DROP FOREIGN KEY FK_3AAD5A59166D1F9C');
        $this->addSql('ALTER TABLE notification_project ADD CONSTRAINT FK_3AAD5A59166D1F9C FOREIGN KEY (project_id) REFERENCES project (id)');

    }

    public function postUp(Schema $schema)
    {
        /**
         * @var EntityManager $em
         */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        try{
            $qb = $em->getRepository(Project::class)->createQueryBuilder('p');
            $qb->leftJoin('p.projectProducts', 'pp');
            $qb->leftJoin('pp.product', 'ppr');
            $qb->leftJoin('ppr.material', 'm');
            $qb->leftJoin('m.materialType', 'mt');
            $qb->where("mt.title LIKE '%Acoustic Blades%'");

            $result = $qb->getQuery()->execute();

            foreach ($result as $item){
                $em->remove($item);
            }

            $materialType = $em->getRepository(MaterialType::class)->findOneBy(['title' => 'Acoustic Blades']);
            $productType  = $em->getRepository(ProductType::class)->findOneBy(['title' => 'Concept Click']);

            if($materialType && $productType){
                $materials = $em->getRepository(Material::class)->findBy(['materialType' => $materialType, 'productType' => $productType]);
                foreach ($materials as $material){
                    $em->remove($material);
                };
            }

            $materialFinishGroup_1 = $em->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'CCL AB Colours']);
            $materialFinishGroup_2 = $em->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'Colour Range']);

            $em->remove($materialFinishGroup_1);
            $em->remove($materialFinishGroup_2);

            $em->flush();
        }catch (ORMException $ex){
            dump($ex->getMessage());
        };

    }
}
