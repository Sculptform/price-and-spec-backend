<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialShape;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171123071616 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql("UPDATE material_shape SET name = 'timber_block' WHERE name = 'block'");

        $this->addSql("
            INSERT INTO material_shape (name, title, is_show_sub_shapes, root, lvl, lft, rgt) 
            VALUES ('aluminium_block', 'Block', 1, 50, 0, 1, 2)
        ");

        $this->addSql("
            UPDATE material_shape_size SET material_shape_id = LAST_INSERT_ID() 
            WHERE (width = 25 AND depth IN (25, 42, 88))
            OR (width = 50 AND depth IN (62, 112, 162))
            OR (width = 66 AND depth IN (25))
        ");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $fs = new Filesystem();

        $ms = $em->getRepository(MaterialShape::class)->findOneBy(['name' => 'aluminium_block']);

        $imagePath = __DIR__ . '/../DataFixtures/Data/MaterialShapes/aluminium_block.png';

        if($fs->exists($imagePath)) {
            $imageFilename = md5($imagePath);
            $imageFileInfo = new \SplFileInfo($imagePath);

            $tmpFilePath = $fs->tempnam('/tmp', 'uploaded-file-');

            $fs->copy($imagePath, $tmpFilePath, true);

            $imageFile = new UploadedFile(
                $tmpFilePath,
                $imageFilename,
                MimeTypeGuesser::getInstance()->guess($imagePath),
                $imageFileInfo->getSize(),
                UPLOAD_ERR_OK,
                true
            );

            $ms->setImageFile($imageFile);
        } else {
            $this->container->get('logger')->error('Material shape aluminium block image loading error', [
                $imagePath
            ]);
        }

        $em->flush();
    }
}
