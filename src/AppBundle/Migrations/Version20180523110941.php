<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialFinishGroup;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20180523110941 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    private $changes    = ['Alphatec' => 'Alphatec - Premium',
                           'Duralloy' => 'Duralloy - Standard',
                           'Duratec'  => 'Duratec - Premium',
                           'Precious' => 'Precious - Premium'];

    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        /**
         * @var $em EntityManager
         */
        $em        = $this->container->get('doctrine.orm.default_entity_manager');
        $groupRepo = $em->getRepository(MaterialFinishGroup::class);

        foreach ($this->changes as $key => $value){
            $group = $groupRepo->findOneBy(['title' => $key]);
            if($group){
                $group->setTitle($value);
                $em->persist($group);
                $em->flush();
            }
        }
    }

    public function postDown(Schema $schema)
    {
        /**
         * @var $em EntityManager
         */
        $em        = $this->container->get('doctrine.orm.default_entity_manager');
        $groupRepo = $em->getRepository(MaterialFinishGroup::class);

        foreach ($this->changes as $key => $value){
            $group = $groupRepo->findOneBy(['title' => $value]);
            if($group){
                $group->setTitle($key);
                $em->persist($group);
                $em->flush();
            }
        }
    }
}
