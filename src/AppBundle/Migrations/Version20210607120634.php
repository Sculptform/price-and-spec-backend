<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Class Version20210607120634
 *
 * @package AppBundle\Migrations
 */
final class Version20210607120634 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20210607120634\LoadFixtures()]);
    }
}
