<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031133537 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $fs = new Filesystem();
        $thumbnailsDir = __DIR__ . '/../DataFixtures/Data/ToolboxThumbnails';

        $shapeSizes = $em->getRepository(MaterialShapeSize::class)->findAll();

        /** @var MaterialShapeSize $shapeSize */
        foreach ($shapeSizes as $shapeSize) {
            $shape = $shapeSize->getMaterialShape();

            /** @var Material $material */
            foreach ($shapeSize->getMaterials() as $material) {
                $materialType = $material->getMaterialType();
                $productType = $material->getProductType();

                $imagePath = '';

                if ('Concept Click' === $productType->getTitle()) {
                    $imagePath = implode('/', [
                        $thumbnailsDir,
                        $productType->getTitle(),
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf('%sx%s.png', $shapeSize->getWidth(), $shapeSize->getDepth())
                    ]);
                } elseif ('Expression Cladding' === $productType->getTitle()) {
                    if ('Base' === $shape->getTitle()) {
                        continue;
                    }

                    $imagePath = implode('/', [
                        $thumbnailsDir,
                        $productType->getTitle(),
                        $shape->getTitle(),
                        sprintf('%sx%s.png', $shapeSize->getWidth(), $shapeSize->getDepth())
                    ]);
                } elseif ('Fintrax' === $productType->getTitle()) {
                    if ('base_stacker' === $shape->getName()) {
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Base',
                            sprintf('%s.png', $shapeSize->getWidth())
                        ]);
                    } elseif ('stacker' === $shape->getName()) {
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Stackers',
                            sprintf('%sx%s.png', $shapeSize->getWidth(), $shapeSize->getDepth())
                        ]);
                    } else { // nosing
                        $imagePath = implode('/', [
                            $thumbnailsDir,
                            $productType->getTitle(),
                            'Nosing',
                            $shapeSize->getWidth(),
                            sprintf('%s.png', $shape->getTitle())
                        ]);
                    }
                } else {
                    $this->container->get('logger')->error('Toolbox thumbnails loading error', [
                        $productType->getTitle(),
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf('%sx%s.png', $shapeSize->getWidth(), $shapeSize->getDepth())
                    ]);
                }

                if($fs->exists($imagePath)) {
                    $imageFilename = md5($imagePath);
                    $imageFileInfo = new \SplFileInfo($imagePath);

                    $tmpFilePath = $fs->tempnam('/tmp', 'uploaded-file-');

                    $fs->copy($imagePath, $tmpFilePath, true);

                    $imageFile = new UploadedFile(
                        $tmpFilePath,
                        $imageFilename,
                        MimeTypeGuesser::getInstance()->guess($imagePath),
                        $imageFileInfo->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $shapeSize->setImageFile($imageFile);
                } else {
                    $this->container->get('logger')->error('Toolbox thumbnails loading error', [
                        $imagePath
                    ]);
                }
            }
        }
        $em->flush();
    }
}
