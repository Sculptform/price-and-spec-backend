<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614111541 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->addSql('UPDATE material_shape SET `name`=LOWER(`title`), root=id, lft=1, rgt=2');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20170614111541\LoadFixtures()]);
    }
}
