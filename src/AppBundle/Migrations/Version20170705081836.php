<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170705081836 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE expression_claddings DROP FOREIGN KEY FK_8F95A86E4584665A');
        $this->addSql('DROP INDEX IDX_8F95A86E4584665A ON expression_claddings');
        $this->addSql('ALTER TABLE expression_claddings ADD stacker_product_id INT DEFAULT NULL, CHANGE product_id base_product_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86ED63EB556 FOREIGN KEY (base_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86EC223B97B FOREIGN KEY (stacker_product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_8F95A86ED63EB556 ON expression_claddings (base_product_id)');
        $this->addSql('CREATE INDEX IDX_8F95A86EC223B97B ON expression_claddings (stacker_product_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE expression_claddings DROP FOREIGN KEY FK_8F95A86ED63EB556');
        $this->addSql('ALTER TABLE expression_claddings DROP FOREIGN KEY FK_8F95A86EC223B97B');
        $this->addSql('DROP INDEX IDX_8F95A86ED63EB556 ON expression_claddings');
        $this->addSql('DROP INDEX IDX_8F95A86EC223B97B ON expression_claddings');
        $this->addSql('ALTER TABLE expression_claddings ADD product_id INT DEFAULT NULL, DROP base_product_id, DROP stacker_product_id');
        $this->addSql('ALTER TABLE expression_claddings ADD CONSTRAINT FK_8F95A86E4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_8F95A86E4584665A ON expression_claddings (product_id)');
    }
}
