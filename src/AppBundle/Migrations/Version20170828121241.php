<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Material;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\Scene;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Migrations\Version;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\VarDumper\Cloner\VarCloner;
use Symfony\Component\VarDumper\Dumper\CliDumper;
use Symfony\Component\Yaml\Exception\ParseException;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170828121241 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');
        $ymlData = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/object3ds.yml')
        )['AppBundle\Entity\Object3D'];

        $conceptClickRange = ['dome', 'flute', 'peak', 'block', 'wave'];
        $conceptClick = [];

        foreach ($conceptClickRange as $range) {
            $conceptClick = array_merge($conceptClick, $this->getObjects($range));
            if ($range == 'block') {
                $conceptClick = array_merge($conceptClick, $this->getObjects($range, 'aluminium'));
            }
            if ($range == 'wave') {
                $conceptClick = array_merge($conceptClick, $this->getObjects($range, 'acoustic'));
            }
        }

            foreach ($conceptClick as &$obj) {
                /** @var Object3D $obj */
                $obj->setValue($ymlData[$this->normalizeTitle($obj->getTitle())]['value']);
                $em->persist($obj);
            }

            $em->flush();
    }

    private function getObjects($title, $type = 'timber')
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $qb = $em->getRepository(Object3D::class)
            ->createQueryBuilder('o');
        switch ($type) {
            case 'timber' : {
                $qb = $qb->where('LOWER(o.title) LIKE LOWER(:title1)')
                    ->andWhere('LOWER(o.title) NOT LIKE LOWER(:title2)')
                    ->setParameter('title1', $title . '%')
                    ->setParameter('title2', $title . ' %');

                if ($title == 'flute') {
                    $qb->orWhere('LOWER(o.title) LIKE LOWER(:title3)')
                        ->setParameter('title3', $title . ' 60x32');
                }
            };
                break;

            case 'aluminium' : {
                $range = [
                    'block 25x25',
                    'block 25x42',
                    'block 25x88',
                    'block 50x62',
                    'block 50x112',
                    'block 50x162',
                    'block 66x25'
                ];
                $qb = $qb->where('LOWER(o.title) IN (:range)')
                    ->setParameter('range', $range);
            };
                break;

            case 'acoustic' : {
                $range = [
                    'straight 12x150',
                    'straight 12x300',
                    'straight 12x400',
                    'wave 12x150'
                ];
                $qb = $qb->where('LOWER(o.title) IN (:range)')
                    ->setParameter('range', $range);
            };
                break;
        }


        return $qb->getQuery()->execute();
    }

    private function normalizeTitle($title)
    {
        return sprintf('object3d_%s', strtolower(str_replace(' ', '_', $title)));
    }
}
