<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\Util\Inflector;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170705121957 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE weight weight DOUBLE PRECISION DEFAULT NULL, CHANGE acoustic acoustic DOUBLE PRECISION DEFAULT NULL');
        $this->addSql('ALTER TABLE scene CHANGE scene scene LONGTEXT NOT NULL COMMENT \'(DC2Type:json_array)\'');
        $this->addSql('ALTER TABLE product CHANGE item_number item_number VARCHAR(255) DEFAULT NULL, CHANGE item_description item_description VARCHAR(255) DEFAULT NULL');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE13D87D0B');
        $this->addSql('DROP INDEX IDX_2FB3D0EE13D87D0B ON project');
        $this->addSql('ALTER TABLE project CHANGE material_finish_id railing_finish_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC890BD0C FOREIGN KEY (railing_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEC890BD0C ON project (railing_finish_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE weight weight NUMERIC(10, 0) DEFAULT NULL, CHANGE acoustic acoustic NUMERIC(10, 0) DEFAULT NULL');
        $this->addSql('ALTER TABLE scene CHANGE scene scene LONGTEXT NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:json)\'');
        $this->addSql('ALTER TABLE product CHANGE item_number item_number VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, CHANGE item_description item_description VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC890BD0C');
        $this->addSql('DROP INDEX IDX_2FB3D0EEC890BD0C ON project');
        $this->addSql('ALTER TABLE project CHANGE railing_finish_id material_finish_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE13D87D0B ON project (material_finish_id)');
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $this->loadECL($em);
        $this->loadAB($em);
        $this->updateCCL($em);
        $this->loadFTX($em);
    }

    private function loadECL(ObjectManager $manager)
    {
        /** @var ProductType $productType */
        $productType = $manager->find(ProductType::class, 2);

        /** @var MaterialFinishGroup $materialFinishGroup */
        $materialFinishGroup = $manager->getRepository(MaterialFinishGroup::class)->findOneBy(['productType' => $productType]);

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $manager->getRepository(MaterialFinish::class)->findBy(['group' => $materialFinishGroup]);

        $file = new \SplFileObject(__DIR__ . '/../DataFixtures/Data/Products/Sculptform ECL 25.07.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description']);
            $itemNumber = trim($row['Item Number']);
            $price = (float)$row['Sculptform Price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice = (float)$row['Cutek'];

            if (!$description) {
                return;
            }

            $normalizedDescription = str_replace(['Profile: ', 'Expression Cladding '], '', $description);

            list($finishTitle, $sizeTitle, $profileTitle) = array_map('trim', explode(' - ', $normalizedDescription));

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            foreach ($materialFinishes as $materialFinish) {
                if ($this->normalizeName($materialFinish->getTitle()) === $this->normalizeName($finishTitle)) {
                    $material = $manager->getRepository(Material::class)->getByProductTypeAndShape(
                        $productType,
                        $profileTitle,
                        $wide,
                        $deep
                    );

                    if ($material) {
                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setCutekPrice($cutekPrice)
                            ->setNaturalAccentPrice($enviroproPrice)
                            ->setEnviroproPrice($enviroproPrice)
                            ->setUnit('lm');

                        $manager->persist($item);
                    } else {
                        $this->container->get('logger')->error('Material not found!', [
                            $productType->getTitle(),
                            $profileTitle,
                            $wide,
                            $deep
                        ]);
                    }

                    break;
                }
            }

            $manager->flush();
        }
    }

    private function loadAB(ObjectManager $manager){
        /** @var ProductType $productType */
        $productType = $manager->find(ProductType::class, 1);

        /** @var ProductType MaterialType */
        $materialType = $manager->getRepository(MaterialType::class)->findOneBy(['title' => 'Acoustic Blades']);

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $manager->getRepository(MaterialFinish::class)->findBy(['materialType' => $materialType]);

        $file = new \SplFileObject(__DIR__ . '/../DataFixtures/Data/Products/Sculptform AB 31.07.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Description']);
            $itemNumber = trim($row['Number']);
            $price = (float)$row['Sculptform Price'];
            $weight = (float)$row['Weight'];

            if (!$description) {
                return;
            }

            $normalizedDescription = str_replace(
                [
                    'Acoustic Linear Blade',
                    'Acoustic Sculpture Blade'
                ],
                [
                    'straight -',
                    'wave -'
                ],
                $description
            );

            list($shapeTitle, $sizeTitle, $finishTitle) = array_map('trim', explode(' - ', $normalizedDescription));

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list(, $deep, $wide) = $sizes[0];

            preg_match_all('|\d+|', $finishTitle, $colors);
            list($color) = $colors[0];

            foreach ($materialFinishes as $materialFinish) {
                if ($this->normalizeName($materialFinish->getTitle()) === $this->normalizeName($color)) {
                    $material = $manager->getRepository(Material::class)->getByProductTypeAndShape(
                        $productType,
                        $shapeTitle,
                        $wide,
                        $deep
                    );

                    if ($material) {
                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setWeight($weight)
                            ->setUnit('lm');

                        $manager->persist($item);
                    } else {
                        $this->container->get('logger')->error('Material not found!', [
                            $productType->getTitle(),
                            $shapeTitle,
                            $wide,
                            $deep
                        ]);
                    }

                    break;
                }
            }

            $manager->flush();
        }
    }

    private function updateCCL(ObjectManager $manager){
        $file = new \SplFileObject(__DIR__ . '/../DataFixtures/Data/Products/Sculptform CCL 25.07.csv');
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description']);
            $itemNumber = trim($row['Item Number']);
            $price = (float)$row['Sculptform price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice = (float)$row['Cutek'];
            $weight = (float)$row['Weight'];

            if (!$description) {
                return;
            }

            if (false !== strpos($description, 'Aluminium')) {
                /** @var Product[] $products */
                $products = $manager->getRepository(Product::class)->findBy(['itemNumber' => $itemNumber]);
            } else {
                $oldItemNumber = str_replace('tsl', 't', $itemNumber);

                /** @var Product[] $products */
                $products = $manager->getRepository(Product::class)->findBy(['itemNumber' => $oldItemNumber]);
            }

            if (!$products) {
                $this->container->get('logger')->error('Products not found!', [$itemNumber]);
            }

            foreach ($products as $product) {
                $product
                    ->setItemNumber($itemNumber)
                    ->setPrice($price)
                    ->setEnviroproPrice($enviroproPrice)
                    ->setNaturalAccentPrice($enviroproPrice)
                    ->setCutekPrice($cutekPrice)
                    ->setWeight($weight);
            }

            $manager->flush();
        }
    }

    private function loadFTX(ObjectManager $manager)
    {
        $productType = $manager->find(ProductType::class, 3);

        /** @var MaterialFinishGroup[] $finishGroups */
        $finishGroups = $manager->getRepository(MaterialFinishGroup::class)->findBy(['productType' => $productType]);

        $materialFinishes = [];

        foreach ($finishGroups as $finishGroup) {
            $materialFinishes = array_merge($materialFinishes, $finishGroup->getMaterialFinishes()->toArray());
        }

        /** @var Material[] $materials */
        $materials = $manager->getRepository(Material::class)->findBy(['productType' => $productType]);

        foreach ($materials as $material) {
            foreach ($materialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setUnit('lm');

                $manager->persist($item);
            }
        }

        $manager->flush();
    }

    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }
}
