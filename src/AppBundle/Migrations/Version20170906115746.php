<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170906115746 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_type ADD popup_title VARCHAR(255) DEFAULT NULL, ADD popup_content LONGTEXT DEFAULT NULL, ADD popup_url VARCHAR(255) DEFAULT NULL, ADD popup_image_path VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_type DROP popup_title, DROP popup_content, DROP popup_url, DROP popup_image_path');
    }

    public function postUp(Schema $schema){
        $em = $this->container->get('doctrine.orm.entity_manager');
        $fs = new Filesystem();
        $ymlData = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/materialTypes.yml')
        )['AppBundle\Entity\MaterialType'];

        $imagesDir     =__DIR__ . '/../DataFixtures/Data/ToolboxPopup/MaterialTypes';
        $materialTypes = ['timber', 'aluminium', 'acoustic_blades'];

        foreach ($materialTypes as $materialType){
            $material = $em->getRepository(MaterialType::class)->findOneBy(['title' => ucwords(str_replace('_', ' ', $materialType))]);
            $path = sprintf('%s/%s.jpg', $imagesDir, $materialType);
            if($material && $fs->exists($path)) {
                $imageFilename = md5($path);
                $imageFileInfo = new \SplFileInfo($path);

                $tmpFilePath = $fs->tempnam('/tmp', 'uploaded-file-');

                $fs->copy($path, $tmpFilePath, true);

                $imageFile = new UploadedFile(
                    $tmpFilePath,
                    $imageFilename,
                    MimeTypeGuesser::getInstance()->guess($path),
                    $imageFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );
                $material->setPopupTitle($ymlData['material_type_'.$materialType]['popupTitle']);
                $material->setPopupUrl($ymlData['material_type_'.$materialType]['popupUrl']);
                $material->setPopupContent($ymlData['material_type_'.$materialType]['popupContent']);
                $material->setPopupImageFile($imageFile);
            } else {
                $this->container->get('logger')->error('Toolbox popup images for material types loading error', [
                    $path
                ]);
            }
        }

        $em->flush();
    }
}
