<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170609115013 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintrax_stackers RENAME INDEX idx_8313616ac7c60b69 TO IDX_CF8EDD4BC7C60B69');
        $this->addSql('ALTER TABLE fintrax_stackers RENAME INDEX idx_8313616a4584665a TO IDX_CF8EDD4B4584665A');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE fintrax_stackers RENAME INDEX idx_cf8edd4bc7c60b69 TO IDX_8313616AC7C60B69');
        $this->addSql('ALTER TABLE fintrax_stackers RENAME INDEX idx_cf8edd4b4584665a TO IDX_8313616A4584665A');
    }
}
