<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Scene;
use AppBundle\Util\FileUtil;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\HttpFoundation\File\File;

/**
 * Remove all but 1 scene from projects
 * Rename texture file for some [160, 250] MaterialFinishes
 */
class Version20170811063620 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        gc_enable();

        ini_set('memory_limit','-1');

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $scenesData = $em->getRepository(Scene::class)
            ->createQueryBuilder('s')
            ->select('project.id as project_id, s.id as scene_id')
            ->join('s.project', 'project')
            ->addOrderBy('s.project', 'asc')
            ->addOrderBy('s.createdAt', 'desc')
            ->getQuery()
            ->getScalarResult()
        ;

        $projectScenes = [];

        /** @var array $sceneData */
        foreach ($scenesData as $sceneData) {
            $projectScenes[$sceneData['project_id']][] = $sceneData['scene_id'];
        }

        foreach ($projectScenes as $projectId => $projectSceneIdsByProjectId) {
            array_shift($projectSceneIdsByProjectId);

            if ($projectSceneIdsByProjectId) {
                $this->container->get('logger')->error('Delete extra scenes for project', [
                    $projectId,
                    $projectSceneIdsByProjectId
                ]);

                $em
                    ->getRepository(Scene::class)
                    ->createQueryBuilder('s')
                    ->delete()
                    ->where('s.id IN (:projectSceneIds)')
                    ->setParameter('projectSceneIds', $projectSceneIdsByProjectId)
                    ->getQuery()
                    ->execute();
                ;
            }
        }

        unset($projectScenes);

        $em->flush();
        $em->clear();
        gc_collect_cycles();

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = $em->getRepository(MaterialFinish::class)->findBy(['id' => [160, 250]]);
        foreach ($materialFinishes as $materialFinish) {
            if(is_file($materialFinish->getTextureAbsolutePath())){
                $textureFile = new File($materialFinish->getTextureAbsolutePath());
                $textureNewFilename = FileUtil::generateFilename($textureFile);
                $materialFinish->setTexturePath($textureNewFilename);

                copy($textureFile->getRealPath(), $materialFinish->getTextureAbsolutePath());
            }
        }

        $em->flush();
        $em->clear();
        gc_collect_cycles();
    }
}
