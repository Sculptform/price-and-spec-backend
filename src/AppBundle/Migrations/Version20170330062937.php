<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170330062937 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE project_material_coatings (project_id INT NOT NULL, material_coating_id INT NOT NULL, INDEX IDX_1C9E2515166D1F9C (project_id), INDEX IDX_1C9E2515C8DCF529 (material_coating_id), PRIMARY KEY(project_id, material_coating_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE project_material_coatings ADD CONSTRAINT FK_1C9E2515166D1F9C FOREIGN KEY (project_id) REFERENCES project (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project_material_coatings ADD CONSTRAINT FK_1C9E2515C8DCF529 FOREIGN KEY (material_coating_id) REFERENCES material_coating (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEC8DCF529');
        $this->addSql('DROP INDEX IDX_2FB3D0EEC8DCF529 ON project');
        $this->addSql('ALTER TABLE project DROP material_coating_id');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE project_material_coatings');
        $this->addSql('ALTER TABLE project ADD material_coating_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEC8DCF529 FOREIGN KEY (material_coating_id) REFERENCES material_coating (id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEC8DCF529 ON project (material_coating_id)');
    }
}
