<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\Railing;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171002070747 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings ADD application_types LONGTEXT NOT NULL COMMENT \'(DC2Type:simple_array)\'');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings DROP application_types');
    }

    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        /**
         * @var Railing $railing
         */
        $railings = $em->getRepository(Railing::class)->findAll();
        $ymlData  = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/railings.yml')
        )['AppBundle\Entity\Railing'];

        foreach ($railings as $railing){
            $railing->setCavity($ymlData[sprintf('railing_%s_%sx%s', strtolower($railing->getMaterialType()->getTitle()), $railing->getWidth(), $railing->getHeight())]['cavity']);
            $railing->setApplicationTypes($ymlData[sprintf('railing_%s_%sx%s', strtolower($railing->getMaterialType()->getTitle()), $railing->getWidth(), $railing->getHeight())]['applicationTypes']);
        }

        $em->flush();
    }
}
