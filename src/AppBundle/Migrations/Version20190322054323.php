<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190322054323 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $query = /** @lang text */
            "UPDATE railings r LEFT JOIN product_type p ON(r.product_type_id = p.id) SET r.title = 'Standard' WHERE p.title IN('Tongue & Groove Cladding', 'Facade Blades');";
        $this->addSql($query);
    }

    public function down(Schema $schema) : void
    {
        $query = /** @lang text */
            "UPDATE railings r LEFT JOIN product_type p ON(r.product_type_id = p.id) SET r.title = 'Standard Track' WHERE p.title IN('Tongue & Groove Cladding', 'Facade Blades');";
        $this->addSql($query);
    }
}
