<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170627092831 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        $this->addSql("UPDATE material_shape SET name='element', title='Element' WHERE name='elemet'");
        $this->addSql("UPDATE material_shape SET name='queenscliff_element', title='Queenscliff & Element' WHERE name='queenscliff_elemet'");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        $this->addSql("UPDATE material_shape SET name='elemet', title='Elemet' WHERE name='element'");
        $this->addSql("UPDATE material_shape SET name='queenscliff_elemet', title='Queenscliff & Elemet' WHERE name='queenscliff_element'");
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20170627092831\LoadFixtures()]);
    }
}
