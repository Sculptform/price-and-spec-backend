<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190717104518 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pardot_statistics CHANGE action_type action_type ENUM(\'download-pdf\', \'request-sample\', \'download-dwg\', \'start-order\', \'download-xls\') NOT NULL COMMENT \'(download-pdf,request-sample,download-dwg,start-order,download-xls)(DC2Type:pardot_last_action)\'');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE pardot_statistics CHANGE action_type action_type ENUM(\'download-pdf\', \'request-sample\', \'download-dwg\', \'start-order\', \'download-xls\') NOT NULL COLLATE utf8_unicode_ci COMMENT \'(DC2Type:pardot_last_action)\'');
    }
}
