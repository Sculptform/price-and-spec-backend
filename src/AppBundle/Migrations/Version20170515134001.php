<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170515134001 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE railings (id INT AUTO_INCREMENT NOT NULL, material_type_id INT DEFAULT NULL, price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, retail_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, wholesale_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, width INT NOT NULL, height INT NOT NULL, model_data LONGTEXT DEFAULT NULL COMMENT \'(DC2Type:json_array)\', INDEX IDX_3278494E74D6573C (material_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE railings ADD CONSTRAINT FK_3278494E74D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id)');
        $this->addSql('DROP TABLE ontrax');
        $this->addSql('ALTER TABLE project ADD railing_id INT DEFAULT NULL, ADD material_finish_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EEEF947113 FOREIGN KEY (railing_id) REFERENCES railings (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE project ADD CONSTRAINT FK_2FB3D0EE13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id) ON DELETE SET NULL');
        $this->addSql('CREATE INDEX IDX_2FB3D0EEEF947113 ON project (railing_id)');
        $this->addSql('CREATE INDEX IDX_2FB3D0EE13D87D0B ON project (material_finish_id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EEEF947113');
        $this->addSql('CREATE TABLE ontrax (id INT AUTO_INCREMENT NOT NULL, object3d_id INT DEFAULT NULL, title VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, color VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci, price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, retail_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, wholesale_price DOUBLE PRECISION DEFAULT \'0\' NOT NULL, width INT NOT NULL, height INT NOT NULL, INDEX IDX_FF72FD1DCDD70535 (object3d_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE ontrax ADD CONSTRAINT FK_FF72FD1DCDD70535 FOREIGN KEY (object3d_id) REFERENCES object3d (id)');
        $this->addSql('DROP TABLE railings');
        $this->addSql('ALTER TABLE project DROP FOREIGN KEY FK_2FB3D0EE13D87D0B');
        $this->addSql('DROP INDEX IDX_2FB3D0EEEF947113 ON project');
        $this->addSql('DROP INDEX IDX_2FB3D0EE13D87D0B ON project');
        $this->addSql('ALTER TABLE project DROP railing_id, DROP material_finish_id');
    }
}
