<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Provider\UploadedFileProvider;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Finder\SplFileInfo;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170911092241 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $anodisingFTXfinishGroup = $em->find(MaterialFinishGroup::class, 16);
        $materialTypeAluminium = $em->find(MaterialType::class, 2);
        $productTypeFTX = $em->find(ProductType::class, 3);

        /** @var Material[] $materials */
        $ftxMaterials = $em->getRepository(Material::class)->findBy(['productType' => $productTypeFTX]);

        foreach ($anodisingFTXfinishGroup->getChildren() as $child) {
            $finishes = $child->getMaterialFinishes();

            foreach ($finishes as $materialFinish) {
                $products = $materialFinish->getProducts();

                foreach ($products as $product) {
                    $projectProducts = $product->getProjectProducts();

                    foreach ($projectProducts as $projectProduct) {
                        $em->remove($projectProduct);
                    }

                    $em->remove($product);
                }

                $em->remove($materialFinish);
            }

            $em->remove($child);
        }

        $em->flush();

        $uploadedFileProvider = UploadedFileProvider::create();
        $basePath = __DIR__ . '/../DataFixtures/Data/Finishes/FTX/Aluminium/Anodising';

        /** @var SplFileInfo[] $files */
        $files = Finder::create()->files()->in($basePath);

        foreach ($files as $file) {
            $textureFile = $uploadedFileProvider->uploadedFile(
                $file->getFilename(),
                $file->getFilename(),
                $basePath
            );

            $materialFinish = new MaterialFinish();

            $materialFinish
                ->setTitle($file->getBasename('.' . $file->getExtension()))
                ->setGroup($anodisingFTXfinishGroup)
                ->setApplicationTypes([2])
                ->setMaterialType($materialTypeAluminium)
                ->setTextureFile($textureFile)
            ;

            $em->persist($materialFinish);

            foreach ($ftxMaterials as $material) {
                $product = new Product();

                $product
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setUnit('lm');

                $em->persist($product);
            }
        }

        $em->flush();
    }
}
