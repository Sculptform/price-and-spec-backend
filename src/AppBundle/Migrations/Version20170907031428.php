<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170907031428 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_type ADD popup_title VARCHAR(255) DEFAULT NULL, ADD popup_content LONGTEXT DEFAULT NULL, ADD popup_url VARCHAR(255) DEFAULT NULL, ADD popup_image_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product_type ADD full_title VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product_type DROP popup_title, DROP popup_content, DROP popup_url, DROP popup_image_path');
        $this->addSql('ALTER TABLE product_type DROP full_title');
    }

    public function postUp(Schema $schema){

        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.entity_manager');
        $fs = new Filesystem();

        $ymlData = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/productTypes.yml')
        )['AppBundle\Entity\ProductType'];

        $imagesDir    =__DIR__ . '/../DataFixtures/Data/ToolboxPopup/ProductTypes';
        $productTypes = ['fintrax', 'expression_cladding'];

        foreach ($productTypes as $productType){
            $product = $em->getRepository(ProductType::class)->findOneBy(['title' => ucwords(str_replace('_', ' ', $productType))]);
            $path = sprintf('%s/%s.jpg', $imagesDir, $productType);
            if($product && $fs->exists($path)) {
                $imageFilename = md5($path);
                $imageFileInfo = new \SplFileInfo($path);

                $tmpFilePath = $fs->tempnam('/tmp', 'uploaded-file-');

                $fs->copy($path, $tmpFilePath, true);

                $imageFile = new UploadedFile(
                    $tmpFilePath,
                    $imageFilename,
                    MimeTypeGuesser::getInstance()->guess($path),
                    $imageFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );
                $product->setPopupTitle($ymlData['ptype_'.$productType]['popupTitle']);
                $product->setPopupContent($ymlData['ptype_'.$productType]['popupContent']);
                $product->setPopupUrl($ymlData['ptype_'.$productType]['popupUrl']);
                $product->setPopupImageFile($imageFile);
            } else {
                $this->container->get('logger')->error('Toolbox popup images for product types loading error', [
                    $path
                ]);
            }
        }

        /** @var ProductType $productTypeCCL */
        $productTypeCCL = $em->find(ProductType::class, 1);
        $productTypeCCL->setFullTitle('Concept Click Batten Screening');

        /** @var ProductType $productTypeECL */
        $productTypeECL = $em->find(ProductType::class, 2);
        $productTypeECL->setFullTitle('Expression Cladding');

        /** @var ProductType $productTypeFTX */
        $productTypeFTX = $em->find(ProductType::class, 3);
        $productTypeFTX->setFullTitle('Fintrax Linear Facades');

        $em->flush();
    }
}
