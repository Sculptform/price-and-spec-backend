<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170609114656 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fintrax_stackers');

        $this->addSql('CREATE TABLE fintrax_stackers (id INT AUTO_INCREMENT NOT NULL, fintrax_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_8313616AC7C60B69 (fintrax_id), INDEX IDX_8313616A4584665A (product_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_8313616AC7C60B69 FOREIGN KEY (fintrax_id) REFERENCES fintraxes (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_8313616A4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fintrax_stackers');

        $this->addSql('CREATE TABLE fintrax_stackers (fintrax_id INT NOT NULL, product_id INT NOT NULL, INDEX IDX_CF8EDD4BC7C60B69 (fintrax_id), INDEX IDX_CF8EDD4B4584665A (product_id), PRIMARY KEY(fintrax_id, product_id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_CF8EDD4B4584665A FOREIGN KEY (product_id) REFERENCES product (id) ON DELETE CASCADE');
        $this->addSql('ALTER TABLE fintrax_stackers ADD CONSTRAINT FK_CF8EDD4BC7C60B69 FOREIGN KEY (fintrax_id) REFERENCES fintraxes (id) ON DELETE CASCADE');
    }
}
