<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\DefaultMaterialFinish;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171025141613 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE default_material_finish (id INT AUTO_INCREMENT NOT NULL, material_finish_id INT DEFAULT NULL, product_type_id INT DEFAULT NULL, material_type_id INT DEFAULT NULL, INDEX IDX_29ADC8FF13D87D0B (material_finish_id), INDEX IDX_29ADC8FF14959723 (product_type_id), INDEX IDX_29ADC8FF74D6573C (material_type_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE default_material_finish ADD CONSTRAINT FK_29ADC8FF13D87D0B FOREIGN KEY (material_finish_id) REFERENCES material_finish (id)');
        $this->addSql('ALTER TABLE default_material_finish ADD CONSTRAINT FK_29ADC8FF14959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id)');
        $this->addSql('ALTER TABLE default_material_finish ADD CONSTRAINT FK_29ADC8FF74D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id)');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE default_material_finish');
    }

    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine')->getManager();

        /**
         * @var MaterialType $railing
         */
        $materialTypes = $em->getRepository(MaterialType::class)->findAll();

        /**
         * @var ProductType $railing
         */
        $productTypes = $em->getRepository(ProductType::class)->findAll();

        foreach ($materialTypes as $material){
            foreach ($productTypes as $product){
                $defaultMaterialFinish = new DefaultMaterialFinish();
                $defaultMaterialFinish->setMaterialType($material);
                $defaultMaterialFinish->setProductType($product);

                $finish = null;

                if($material->getTitle() == 'Timber'){
                    $finishGroup = $em->getRepository(MaterialFinishGroup::class)->findOneBy(['title' => 'Species', 'productType' => $product]);
                    $finish = $em->getRepository(MaterialFinish::class)->findOneBy(['title' => 'Spotted Gum', 'group' => $finishGroup]);
                }

                if($material->getTitle() == 'Aluminium' && $product != 'Expression Cladding'){
                    $finish = $em->getRepository(MaterialFinish::class)->findOneBy(['title' => 'Surfmist']);
                }

                if($material->getTitle() == 'Acoustic Blades' && $product == 'Concept Click'){
                    $finish = $em->getRepository(MaterialFinish::class)->findOneBy(['title' => '002']);
                }

                $defaultMaterialFinish->setMaterialFinish($finish);

                if($defaultMaterialFinish->getMaterialFinish() != null){
                    $em->persist($defaultMaterialFinish);
                }
            }
        }

        $em->flush();
    }
}
