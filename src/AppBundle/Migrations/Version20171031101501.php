<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Provider\UploadedFileProvider;
use AppBundle\Entity\Material;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171031101501 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs

    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $uploadedFileProvider = UploadedFileProvider::create();
        $path = __DIR__ . '/../DataFixtures/Data/ToolboxThumbnails/Concept Click/Acoustic Blades/Straight';

        $qb = $em->getRepository(Material::class)
                 ->createQueryBuilder('m')
                 ->leftJoin('m.productType', 'pt')
                 ->leftJoin('m.materialType', 'mt')
                 ->leftJoin('m.object3d', 'o')
                 ->where('pt.title = :title')
                 ->andWhere('mt.title = :mtitle')
                 ->andWhere('o.title like :otitle')
                 ->setParameter('title', 'Concept Click')
                 ->setParameter('mtitle', 'Acoustic Blades')
                 ->setParameter('otitle', '%Straight%');

        foreach ($qb->getQuery()->getResult() as $item) {
            $imageFile = $uploadedFileProvider->uploadedFile(
                sprintf('%s.png', trim(str_replace('Straight', '', $item->getObject3D()->getTitle()))),
                null,
                $path
            );

            $item->getObject3D()->setImageFile($imageFile);

            $em->flush($item->getObject3D());
        }
    }
}
