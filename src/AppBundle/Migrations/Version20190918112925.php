<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190918112925 extends AbstractFixtureMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $types = [
            'Click-on Battens' => [
                'disclaimer'   => '<p>Pricing is based on the amount of product required as specified in the pricing panel. Quote is valid for 45 days from date of issue.</p>',
                'priceInclude' => '<p>All standard componentry – corner trims, end caps, L-profiles, clips, mounting tracks, acoustic backing (if selected) and back cover strip. Freight to site within Australia. For international customers please see <a href="https://sculptform.com.au/international-orders/" target="_blank">international terms</a> for more information.</p>',
                'priceExclude' => '<p>GST, custom corner/edging treatment, flashing, installation, substrates, hangers and TCR for suspended ceilings, fixings for substrate, fixing screws or insulation batts.</p>',
                'priceNote'    => '<p><strong>Please note: We have a minimum order of $5,000 AUD due to manufacturing costs</strong> (see our <a href="https://sculptform.com.au/sculptform-minimum-order/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use/" target="_blank">terms of use</a> for more details.</p>',
            ],
            'Tongue & Groove Cladding' => [
                'disclaimer'   => '<p>Pricing is based on the amount of product required as specified in the pricing panel. Quote is valid for 45 days from date of issue.</p>',
                'priceInclude' => '<p>All standard componentry – our proprietary corner profiles, fixing screws, starter extrusions. Freight to site within Australia. For international customers please see our <a href="https://sculptform.com.au/international-orders/" target="_blank">international terms</a> for more information.</p>',
                'priceExclude' => '<p>GST, custom corner/edging treatment, flashing, installation, substrates, hangers and TCR for suspended ceilings, fixings for substrate, breathable sarking or insulation batts.</p>',
                'priceNote'    => '<p><strong>Please note: We have a minimum order of $5,000 AUD due to manufacturing costs</strong> (see our <a href="https://sculptform.com.au/sculptform-minimum-order/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use/" target="_blank">terms of use</a> for more details.</a>',
            ],
            'Facade Blades' => [
                'disclaimer'   => '<p>Pricing is based on the amount of product required as specified in the pricing panel. Quote is valid for 45 days from date of issue.</p>',
                'priceInclude' => '<p>All standard componentry – mounting rail, blade brackets, blade endcaps, rail endcaps. Freight to site within Australia. For international customers please see our <a href="https://sculptform.com.au/international-orders/" target="_blank">international terms</a> for more information.</p>',
                'priceExclude' => '<p>GST, custom corner/edging treatment, flashing, installation, substrates or fixings for substrate.</p>',
                'priceNote'    => '<p><strong>Please note: We have a minimum order of $5,000 AUD due to manufacturing costs</strong> (see our <a href="https://sculptform.com.au/sculptform-minimum-order/" target="_blank">website</a> for more information). The information provided in the Price & Spec Tool is a guideline for your convenience. Every care has been taken to ensure reasonable accuracy however variations may occur. See our <a href="https://sculptform.com.au/terms-of-use/" target="_blank">terms of use</a> for more details.</p>',
            ],
        ];

        /** @var EntityManagerInterface $em */
        $em = $this->container->get('doctrine.orm.entity_manager');

        foreach ($types as $title => $data) {
            $productType = $em->getRepository(ProductType::class)->findOneBy(['title' => $title]);

            $productType->setDisclaimer($data['disclaimer']);
            $productType->setPriceIncludeNote($data['priceInclude']);
            $productType->setPriceExcludeNote($data['priceExclude']);
            $productType->setPriceNote($data['priceNote']);
        }
        $em->flush();
    }
}
