<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170325101814 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_finish_groups DROP FOREIGN KEY FK_65D5BA1B74D6573C');
        $this->addSql('ALTER TABLE material_finish_groups ADD CONSTRAINT FK_65D5BA1B74D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_finish_groups DROP FOREIGN KEY FK_65D5BA1B74D6573C');
        $this->addSql('ALTER TABLE material_finish_groups ADD CONSTRAINT FK_65D5BA1B74D6573C FOREIGN KEY (material_type_id) REFERENCES material_type (id)');
    }
}
