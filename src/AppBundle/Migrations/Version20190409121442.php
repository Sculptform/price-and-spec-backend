<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20190409121442 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('CREATE TABLE IF NOT EXISTS currencies (id INT AUTO_INCREMENT NOT NULL, code VARCHAR(3) NOT NULL, rate DOUBLE PRECISION NOT NULL, is_active TINYINT(1) NOT NULL, updated_at DATETIME NOT NULL, UNIQUE INDEX unique_currency (code), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE UTF8_unicode_ci ENGINE = InnoDB');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');
        $this->addSql('DROP TABLE currencies');
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20190409121442\LoadFixtures()]);
    }
}
