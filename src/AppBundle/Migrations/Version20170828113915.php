<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Scene;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170828113915 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        gc_enable();

        ini_set('memory_limit', '-1');
        ini_set('max_execution_time', '-1');

        $em = $this->container->get('doctrine.orm.entity_manager');
        $em->getConnection()->getConfiguration()->setSQLLogger(null);

        $materialFinishGroupRepo = $em->getRepository(MaterialFinishGroup::class);

        /** @var MaterialFinishGroup[] $finishGroupsForRemove */
        $finishGroupsForRemove = $materialFinishGroupRepo->findBy(['id' => [2,3,4,5,11,13,14,15]]);

        foreach ($finishGroupsForRemove as $finishGroupForRemove) {
            /** @var MaterialFinish[] $finishes */
            $finishes = $finishGroupForRemove->getMaterialFinishes();

            foreach ($finishes as $finish) {
                $this->container->get('logger')->err('Deleting finish..', [$finish->getTitle()]);

                $this->preRemoveFinish($finish, $em);

                /** @var Product[] $products */
                $products = $finish->getProducts();

                foreach ($products as $product) {
                    /** @var ProjectProduct[] $projectProducts */
                    $projectProducts = $product->getProjectProducts();

                    foreach ($projectProducts as $projectProduct) {
                        $em->remove($projectProduct);
                    }

                    $em->remove($product);
                }

                gc_collect_cycles();

                $em->flush();

                $em->remove($finish);
                $em->flush();
            }

            $em->remove($finishGroupForRemove);
            gc_collect_cycles();
        }

        $em->flush();
        gc_collect_cycles();

        /** @var MaterialFinishGroup $abColourRangeGroup */
        $abColourRangeGroup = $materialFinishGroupRepo->find(22);

        /** @var MaterialFinish $materialFinish */
        foreach ($abColourRangeGroup->getMaterialFinishes() as $materialFinish) {
            $this->container->get('logger')->err('Updating finish..', [$materialFinish->getTitle()]);
            $path = sprintf(
                '%s/../DataFixtures/Data/Finishes/CCL/Acoustic Blades/%s.jpg',
                __DIR__,
                $materialFinish->getTitle()
            );
            $file = $this->getUploadedFile($path);
            $materialFinish->setTextureFile($file);
            $em->flush($materialFinish);
            $this->postUpdateFinish($materialFinish, $em);
        }

        gc_collect_cycles();

        /** @var MaterialFinishGroup $timberSpeciesGroup */
        $timberSpeciesGroup = $materialFinishGroupRepo->find(12);

        /** @var MaterialFinish $materialFinish */
        foreach ($timberSpeciesGroup->getMaterialFinishes() as $materialFinish) {
            $this->container->get('logger')->err('Updating finish..', [$materialFinish->getTitle()]);
            $path = sprintf(
                '%s/../DataFixtures/Data/Finishes/CCL/Timber/%s.jpg',
                __DIR__,
                $materialFinish->getTitle()
            );
            $file = $this->getUploadedFile($path);
            $materialFinish->setTextureFile($file);
            $em->flush($materialFinish);
            $this->postUpdateFinish($materialFinish, $em);
        }

        gc_collect_cycles();

        $this->loadFixtures([new Fixtures\Version20170828113915\LoadFixtures()]);
    }

    /**
     * @param string $path
     * @return UploadedFile
     */
    private function getUploadedFile($path)
    {
        $origFile = new File($path);

        $tmpFilename = tempnam('/tmp', 'uploaded');

        $fs = new Filesystem();
        $fs->copy($origFile->getRealPath(), $tmpFilename, true);

        $tmpFile = new File($tmpFilename);

        return new UploadedFile(
            $tmpFile->getPathname(),
            $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            $tmpFile->getSize(),
            UPLOAD_ERR_OK,
            true
        );
    }

    private function preRemoveFinish(MaterialFinish $finish, EntityManager $em)
    {
        gc_enable();

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('scene')
            ->distinct(true)
            ->join('scene.project', 'project')
            ->join('project.projectProducts', 'projectProduct')
            ->join('projectProduct.product', 'product')
            ->where('product.materialFinish = :materialFinish')
            ->setParameter('materialFinish', $finish)
            ->getQuery();

        foreach ($scenesQuery->iterate() as $sceneEntity) {
            /** @var Scene $sceneEntity */
            $sceneEntity = $sceneEntity[0];
            $sceneData = $sceneEntity->getScene();

            $this->container->get('logger')->err('Updating scenes..', [
                $sceneEntity->getProject()->getId()
            ]);

            $scene = &$this->getSubArray($sceneData, 'scene');
            $sceneObject = &$this->getSubArray($scene, 'object');
            $sceneChildren = &$this->getSubArray($sceneObject, 'children');
            $sceneMaterials = &$this->getSubArray($scene, 'materials');
            $sceneTextures = &$this->getSubArray($scene, 'textures');
            $sceneImages = &$this->getSubArray($scene, 'images');

            $sceneChildren = is_array($sceneChildren) ? $sceneChildren : [];

            foreach ($sceneChildren as &$sceneChild) {
                $product = null;

                if (array_key_exists('productModel', $sceneChild)) {
                    $product = $em->find(Product::class, $sceneChild['productModel']);
                }

                if (
                    (
                        array_key_exists('materialFinishModel', $sceneChild) &&
                        $sceneChild['materialFinishModel'] == $finish->getId()
                    ) ||
                    (
                        $product && $product->getMaterialFinishId() == $finish->getId()
                    )
                ) {
                    unset($sceneChild['materialFinishModel']);

                    if (array_key_exists('productModel', $sceneChild)) {
                        unset($sceneChild['productModel']);
                    }

                    if (array_key_exists('material', $sceneChild)) {
                        $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                        if (false !== $materialKey) {
                            $material = &$sceneMaterials[$materialKey];

                            if (array_key_exists('map', $material)) {
                                $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                if (false !== $sceneTextureKey) {
                                    $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                    if (array_key_exists('image', $sceneTexture)) {
                                        $sceneImageKey = $this->findKey(
                                            $sceneImages,
                                            'uuid',
                                            $sceneTexture['image']
                                        );

                                        if (false !== $sceneImageKey) {
                                            $material['color'] = $finish->getDecimalColor(true);
                                            unset($sceneImages[$sceneImageKey], $sceneTextures[$sceneTextureKey], $material['map']);
                                        }
                                    }
                                    unset($sceneTexture);
                                }
                            }
                            unset($material);
                        }
                    }
                }

                gc_collect_cycles();
            }

            unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);

            $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);

            $sceneEntity->setScene($sceneData);
            $em->flush();
            gc_collect_cycles();
        }
    }

    private function postUpdateFinish(MaterialFinish $finish, EntityManager $em)
    {
        $hostUrl = $this->container->getParameter('host');

        $scenesQuery = $em->getRepository(Scene::class)
            ->createQueryBuilder('scene')
            ->distinct(true)
            ->join('scene.project', 'project')
            ->join('project.projectProducts', 'projectProduct')
            ->join('projectProduct.product', 'product')
            ->where('product.materialFinish = :materialFinish')
            ->setParameter('materialFinish', $finish)
            ->getQuery();

        foreach ($scenesQuery->iterate() as $sceneEntity) {
            /** @var Scene $sceneEntity */
            $sceneEntity = $sceneEntity[0];
            $sceneData = $sceneEntity->getScene();

            $this->container->get('logger')->err('Updating scenes..', [
                $sceneEntity->getProject()->getId()
            ]);

            $scene = &$this->getSubArray($sceneData, 'scene');
            $sceneObject = &$this->getSubArray($scene, 'object');
            $sceneChildren = &$this->getSubArray($sceneObject, 'children');
            $sceneMaterials = &$this->getSubArray($scene, 'materials');
            $sceneTextures = &$this->getSubArray($scene, 'textures');
            $sceneImages = &$this->getSubArray($scene, 'images');

            $sceneChildren = is_array($sceneChildren) ? $sceneChildren : [];

            foreach ($sceneChildren as &$sceneChild) {
                $product = null;

                if (array_key_exists('productModel', $sceneChild)) {
                    $product = $em->find(Product::class, $sceneChild['productModel']);
                }

                if (
                    (
                        array_key_exists('materialFinishModel', $sceneChild) &&
                        $sceneChild['materialFinishModel'] == $finish->getId()
                    ) ||
                    (
                        $product && $product->getMaterialFinishId() == $finish->getId()
                    )
                ) {
                    if (array_key_exists('material', $sceneChild)) {
                        $materialKey = $this->findKey($sceneMaterials, 'uuid', $sceneChild['material']);

                        if (false !== $materialKey) {
                            $material = &$sceneMaterials[$materialKey];

                            if (array_key_exists('map', $material)) {
                                $sceneTextureKey = $this->findKey($sceneTextures, 'uuid', $material['map']);

                                if (false !== $sceneTextureKey) {
                                    $sceneTexture = &$sceneTextures[$sceneTextureKey];

                                    if (array_key_exists('image', $sceneTexture)) {
                                        $sceneImageKey = $this->findKey(
                                            $sceneImages,
                                            'uuid',
                                            $sceneTexture['image']
                                        );

                                        if (false !== $sceneImageKey) {
                                            $sceneImages[$sceneImageKey]['url'] = $hostUrl . '/' . $finish->getTextureWebPath();
                                        }
                                    }

                                    unset($sceneTexture);
                                }
                            }

                            $material['color'] = $finish->getDecimalColor();

                            unset($material);
                        }
                    }
                }

                gc_collect_cycles();
            }

            $sceneData = json_decode(json_encode($sceneData, 0, 2024), true, 2024);
            $sceneEntity->setScene($sceneData);
            $em->flush();

            unset($scene, $sceneObject, $sceneChildren, $sceneMaterials, $sceneTextures, $sceneImages, $sceneChild);

            gc_collect_cycles();
        }
    }

    private function &getSubArray(&$array, $key)
    {
        if (!array_key_exists($key, $array)) {
            $array[$key] = [];
        }

        return $array[$key];
    }

    private function findKey($array, $searchKey, $searchValue)
    {
        $key = array_search($searchValue, array_column($array, $searchKey));

        return $key === false ? false : array_keys($array)[$key];
    }
}
