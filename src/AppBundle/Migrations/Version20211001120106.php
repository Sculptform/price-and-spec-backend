<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20211001120106 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        $this->addSql('UPDATE product SET item_number = "cob50tubeae"  WHERE item_number = "cob5050tae"');
        $this->addSql('UPDATE product SET item_number = "cob50tubeewd" WHERE item_number = "cob5050tewd"');
        $this->addSql('UPDATE product SET item_number = "cob50tubepp"  WHERE item_number = "cob5050tpp"');
        $this->addSql('UPDATE product SET item_number = "cob50tubesp"  WHERE item_number = "cob5050tsp"');
        $this->addSql('UPDATE product SET item_number = "cob50tubev"   WHERE item_number = "cob5050tv"');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20211001120106\LoadFixtures()]);
    }
}
