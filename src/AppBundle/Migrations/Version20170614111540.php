<?php

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614111540 extends AbstractFixtureMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE railings ADD dwg_path VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE material ADD dwg_path VARCHAR(255) DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material DROP dwg_path');
        $this->addSql('ALTER TABLE railings DROP dwg_path');
    }
}
