<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\FintraxInfo;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialShape;
use AppBundle\Service\ProductService;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170808082043 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE fintrax_infos (id INT AUTO_INCREMENT NOT NULL, finish_group_id INT DEFAULT NULL, nosing_id INT DEFAULT NULL, price DOUBLE PRECISION DEFAULT NULL, weight DOUBLE PRECISION DEFAULT NULL, item_number VARCHAR(255) DEFAULT NULL, description VARCHAR(255) DEFAULT NULL, width NUMERIC(10, 0) NOT NULL, depth NUMERIC(10, 0) NOT NULL, INDEX IDX_213A972C471F9A17 (finish_group_id), INDEX IDX_213A972C90EB721F (nosing_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET utf8 COLLATE utf8_unicode_ci ENGINE = InnoDB');
        $this->addSql('ALTER TABLE fintrax_infos ADD CONSTRAINT FK_213A972C471F9A17 FOREIGN KEY (finish_group_id) REFERENCES material_finish_groups (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE fintrax_infos ADD CONSTRAINT FK_213A972C90EB721F FOREIGN KEY (nosing_id) REFERENCES material_shape (id) ON DELETE SET NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('DROP TABLE fintrax_infos');
    }

    public function postUp(Schema $schema)
    {
        /** @var ProductService $productService */
        $productService = $this->container->get('app.service.product');

        $productService->updateABFromCSV(__DIR__ . '/../DataFixtures/Data/Products/Sculptform AB 16.08.csv');
        $productService->updateCCLFromCSV(__DIR__ . '/../DataFixtures/Data/Products/Sculptform CCL 16.08.csv');
        $productService->updateECLFromCSV(__DIR__ . '/../DataFixtures/Data/Products/Sculptform ECL 16.08.csv');
        $productService->updateFTXFromCSV(__DIR__ . '/../DataFixtures/Data/Products/Sculptform FTX 16.08.csv');
    }
}
