<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511112218 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product ADD natural_accent_wholesale_price DOUBLE PRECISION DEFAULT NULL, ADD natural_accent_retail_price DOUBLE PRECISION DEFAULT NULL, ADD cutek_wholesale_price DOUBLE PRECISION DEFAULT NULL, ADD cutek_retail_price DOUBLE PRECISION DEFAULT NULL, ADD enviropro_wholesale_price DOUBLE PRECISION DEFAULT NULL, ADD enviropro_retail_price DOUBLE PRECISION DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP natural_accent_wholesale_price, DROP natural_accent_retail_price, DROP cutek_wholesale_price, DROP cutek_retail_price, DROP enviropro_wholesale_price, DROP enviropro_retail_price');
    }
}
