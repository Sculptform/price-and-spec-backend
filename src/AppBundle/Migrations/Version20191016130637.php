<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20191016130637 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acoustic_backing ADD product_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE acoustic_backing ADD CONSTRAINT FK_EEC338414959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id)');
        $this->addSql('CREATE INDEX IDX_EEC338414959723 ON acoustic_backing (product_type_id)');
        $this->addSql('ALTER TABLE acoustic_backing ADD is_default TINYINT(1) DEFAULT \'0\' NOT NULL');
        $this->addSql("INSERT INTO `acoustic_backing` (`id`, `title`, `color`, `price`, `depth`, `retail_price`, `wholesale_price`, `product_type_id`, `is_default`) VALUES (NULL, 'No Backing', '', '30.7', '0', '0', '0', '1', '1');");
        $this->addSql("INSERT INTO `acoustic_backing` (`id`, `title`, `color`, `price`, `depth`, `retail_price`, `wholesale_price`, `product_type_id`, `is_default`) VALUES (NULL, 'Group 3 Rated - Standard', '', '65.7', '0', '0', '0', '1', '0');");
        $this->addSql("INSERT INTO `acoustic_backing` (`id`, `title`, `color`, `price`, `depth`, `retail_price`, `wholesale_price`, `product_type_id`, `is_default`) VALUES (NULL, 'Group 1 Rated', '', '103.7', '0', '0', '0', '1', '0');");
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE acoustic_backing DROP FOREIGN KEY FK_EEC338414959723');
        $this->addSql('DROP INDEX IDX_EEC338414959723 ON acoustic_backing');
        $this->addSql('ALTER TABLE acoustic_backing DROP product_type_id');
        $this->addSql('ALTER TABLE acoustic_backing DROP is_default');
    }
}
