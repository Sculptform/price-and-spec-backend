<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170412075757 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quotation DROP FOREIGN KEY FK_474A8DB9783E3463');
        $this->addSql('DROP INDEX IDX_474A8DB9783E3463 ON quotation');
        $this->addSql('ALTER TABLE quotation DROP manager_id, DROP status');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE quotation ADD manager_id INT DEFAULT NULL, ADD status INT NOT NULL');
        $this->addSql('ALTER TABLE quotation ADD CONSTRAINT FK_474A8DB9783E3463 FOREIGN KEY (manager_id) REFERENCES user (id)');
        $this->addSql('CREATE INDEX IDX_474A8DB9783E3463 ON quotation (manager_id)');
    }
}
