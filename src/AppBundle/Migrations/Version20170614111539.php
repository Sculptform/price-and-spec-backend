<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170614111539 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_finish_groups ADD product_type_id INT DEFAULT NULL');
        $this->addSql('ALTER TABLE material_finish_groups ADD CONSTRAINT FK_65D5BA1B14959723 FOREIGN KEY (product_type_id) REFERENCES product_type (id)');
        $this->addSql('CREATE INDEX IDX_65D5BA1B14959723 ON material_finish_groups (product_type_id)');

        $this->addSql('UPDATE material_finish_groups SET product_type_id=1');
        $this->addSql("UPDATE material_finish_groups SET application_types='1,2' WHERE title IN ('Anodising', 'Powder Coating', 'Species', 'Bright', 'Evershield Internal', 'Illustro', 'Matt', 'Alphatec', 'Duralloy', 'Duratec', 'Precious')");
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_finish_groups DROP FOREIGN KEY FK_65D5BA1B14959723');
        $this->addSql('DROP INDEX IDX_65D5BA1B14959723 ON material_finish_groups');
        $this->addSql('ALTER TABLE material_finish_groups DROP product_type_id');
    }
}
