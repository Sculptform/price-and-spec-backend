<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20181130101507 extends AbstractMigration
{
    public function up(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE product_type SET description='Completely customisable, adaptable and quick to install - this concealed fix system creates beautiful effects for your walls and ceilings.' WHERE id=1");
        $this->addSql("UPDATE product_type SET description='This complete timber cladding system helps you create stunning linear textures using modern profiles, multi-depth options and a range of finishes.' WHERE id=2");
        $this->addSql("UPDATE product_type SET description='A robust system specifically designed and engineered for large scale external facades.' WHERE id=4");

    }

    public function down(Schema $schema) : void
    {
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE product_type SET description='Completely customisable, easily adaptable and quick to install - this beautiful system creates stunning linear effects for your walls and ceilings.' WHERE id=1");
        $this->addSql("UPDATE product_type SET description='This high-performance, complete timber cladding system helps you create durable, beautiful, long-lasting linear textures. For both the interior and exterior of your building project.' WHERE id=2");
        $this->addSql("UPDATE product_type SET description='A facade blade system specifically designed and engineered for large scale external facades. Allowing large spans between fixings, the system is robust enough to accommodate a range of applications.' WHERE id=4");

    }
}
