<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use Doctrine\DBAL\Schema\Schema;
use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;

/**
 * Class Version20200227090701
 *
 * @package AppBundle\Migrations
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
final class Version20200227090701 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20200227090701\LoadFixtures()]);
    }
}
