<?php declare(strict_types=1);

namespace AppBundle\Migrations;

use AppBundle\DataFixtures\Migration\AbstractFixtureMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20180928094156 extends AbstractFixtureMigration
{
    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $dateOfRelease = '2018-09-26';
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql("UPDATE project SET outdated=FALSE WHERE TRUE");
        $this->addSql("UPDATE project SET outdated=TRUE WHERE created_at <='".$dateOfRelease."' AND product_type_id <> 2");

    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs

    }

    public function postUp(Schema $schema)
    {
        $this->loadFixtures([new Fixtures\Version20180928094156\LoadFixtures()]);
    }
}
