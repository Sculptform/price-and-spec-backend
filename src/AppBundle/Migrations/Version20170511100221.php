<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170511100221 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_coating ADD product_wholesale_price_field_name VARCHAR(255) DEFAULT NULL, ADD product_retail_price_field_name VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD wholesale_price DOUBLE PRECISION DEFAULT NULL, ADD retail_price DOUBLE PRECISION DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE material_coating DROP product_wholesale_price_field_name, DROP product_retail_price_field_name');
        $this->addSql('ALTER TABLE product DROP wholesale_price, DROP retail_price');
    }
}
