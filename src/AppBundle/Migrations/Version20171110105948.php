<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialFinish;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Yaml\Yaml;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171110105948 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE application_type application_type INT DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE project CHANGE application_type application_type INT NOT NULL');
    }

    public function postUp(Schema $schema)
    {
        $em = $this->container->get('doctrine.orm.default_entity_manager');
        $ymlData  = Yaml::parse(
            file_get_contents(__DIR__ . '/../DataFixtures/ORM/materialFinishesCCLAcousticBlades.yml')
        )['AppBundle\Entity\MaterialFinish'];

        $qb = $em->getRepository(MaterialFinish::class)
            ->createQueryBuilder('m')
            ->leftJoin('m.materialType', 'mt')
            ->where('mt.title = :mtitle')
            ->setParameter('mtitle', 'Acoustic Blades');

        /**
         * @var $item MaterialFinish
         */
        foreach ($qb->getQuery()->getResult() as $item) {
            $item->setApplicationTypes($ymlData['material_finish_acoustic_blades_colour_range_'.$item->getTitle()]['applicationTypes']);

            $em->persist($item);
            $em->flush();
        }
    }
}
