<?php

namespace AppBundle\Migrations;

use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\ProductType;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;
use Doctrine\ORM\EntityManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20171124102702 extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
    }

    public function postUp(Schema $schema)
    {
        /** @var EntityManager $em */
        $em = $this->container->get('doctrine.orm.default_entity_manager');

        $ptCCL = $em->find(ProductType::class, 1);
        $ptECL = $em->find(ProductType::class, 2);
        $ptFTX = $em->find(ProductType::class, 3);

        $mfgRepository = $em->getRepository(MaterialFinishGroup::class);
        $mtRepository = $em->getRepository(MaterialType::class);

        $mtAl = $mtRepository->findOneBy(['title' => 'Aluminium']);
        $mtTimber = $mtRepository->findOneBy(['title' => 'Timber']);
        $mtAB = $mtRepository->findOneBy(['title' => 'Acoustic Blades']);

        $cclAlColoursGroup = new MaterialFinishGroup();
        $cclAlColoursGroup
            ->setTitle('CCL AL Colours')
            ->setMaterialType($mtAl)
            ->setProductType($ptCCL)
            ->setApplicationTypes([1, 2]);
        $mfgRepository->persistAsLastChild($cclAlColoursGroup);

        $cclTimberColoursGroup = new MaterialFinishGroup();
        $cclTimberColoursGroup
            ->setTitle('CCL Timber Colours')
            ->setMaterialType($mtTimber)
            ->setProductType($ptCCL)
            ->setApplicationTypes([1, 2]);
        $mfgRepository->persistAsLastChild($cclTimberColoursGroup);

        $cclABColoursGroup = new MaterialFinishGroup();
        $cclABColoursGroup
            ->setTitle('CCL AB Colours')
            ->setMaterialType($mtAB)
            ->setProductType($ptCCL)
            ->setApplicationTypes([1, 2]);
        $mfgRepository->persistAsLastChild($cclABColoursGroup);

        $eclTimberColoursGroup = new MaterialFinishGroup();
        $eclTimberColoursGroup
            ->setTitle('ECL Timber Colours')
            ->setMaterialType($mtTimber)
            ->setProductType($ptECL)
            ->setApplicationTypes([1, 2]);
        $mfgRepository->persistAsLastChild($eclTimberColoursGroup);

        $ftxAlColoursGroup = new MaterialFinishGroup();
        $ftxAlColoursGroup
            ->setTitle('FTX AL Colours')
            ->setMaterialType($mtAl)
            ->setProductType($ptFTX)
            ->setApplicationTypes([1, 2]);
        $mfgRepository->persistAsLastChild($ftxAlColoursGroup);

        $cclAlColoursSubGroups = $mfgRepository->findBy(
            [
                'materialType' => $mtAl,
                'productType' => $ptCCL,
                'parent' => null
            ],
            ['level' => 'ASC', 'left' => 'ASC']
        );

        foreach ($cclAlColoursSubGroups as $cclAlColoursSubGroup) {
            $mfgRepository->persistAsFirstChildOf($cclAlColoursSubGroup, $cclAlColoursGroup);
        }

        $cclTimberColoursSubGroups = $mfgRepository->findBy(
            [
                'materialType' => $mtTimber,
                'productType' => $ptCCL,
                'parent' => null
            ],
            ['level' => 'ASC', 'left' => 'ASC']
        );

        foreach ($cclTimberColoursSubGroups as $cclTimberColoursSubGroup) {
            $mfgRepository->persistAsFirstChildOf($cclTimberColoursSubGroup, $cclTimberColoursGroup);
        }

        $cclABColoursSubGroups = $mfgRepository->findBy(
            [
                'materialType' => $mtAB,
                'productType' => $ptCCL,
                'parent' => null
            ],
            ['level' => 'ASC', 'left' => 'ASC']
        );

        foreach ($cclABColoursSubGroups as $cclABColoursSubGroup) {
            $mfgRepository->persistAsFirstChildOf($cclABColoursSubGroup, $cclABColoursGroup);
        }

        $eclTimberColoursSubGroups = $mfgRepository->findBy(
            [
                'materialType' => $mtTimber,
                'productType' => $ptECL,
                'parent' => null
            ],
            ['level' => 'ASC', 'left' => 'ASC']
        );

        foreach ($eclTimberColoursSubGroups as $eclTimberColoursSubGroup) {
            $mfgRepository->persistAsFirstChildOf($eclTimberColoursSubGroup, $eclTimberColoursGroup);
        }

        $ftxAlColoursSubGroups = $mfgRepository->findBy(
            [
                'materialType' => $mtAl,
                'productType' => $ptFTX,
                'parent' => null
            ],
            ['level' => 'ASC', 'left' => 'ASC']
        );

        foreach ($ftxAlColoursSubGroups as $ftxAlColoursSubGroup) {
            $mfgRepository->persistAsFirstChildOf($ftxAlColoursSubGroup, $ftxAlColoursGroup);
        }

        $em->flush();
    }
}
