<?php

namespace AppBundle\Migrations;

use Doctrine\DBAL\Migrations\AbstractMigration;
use Doctrine\DBAL\Schema\Schema;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
class Version20170325090105 extends AbstractMigration
{
    /**
     * @param Schema $schema
     */
    public function up(Schema $schema)
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE308AC6F');
        $this->addSql('ALTER TABLE product ADD original_price DOUBLE PRECISION DEFAULT NULL, CHANGE material_id material_id INT DEFAULT NULL, CHANGE price price DOUBLE PRECISION DEFAULT NULL, CHANGE weight weight NUMERIC(10, 0) DEFAULT NULL, CHANGE acoustic acoustic NUMERIC(10, 0) DEFAULT NULL, CHANGE item_id item_id VARCHAR(255) DEFAULT NULL');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE308AC6F FOREIGN KEY (material_id) REFERENCES material (id) ON DELETE SET NULL');
        $this->addSql('ALTER TABLE product CHANGE price price DOUBLE PRECISION DEFAULT NULL, CHANGE weight weight NUMERIC(10, 0) DEFAULT NULL, CHANGE acoustic acoustic NUMERIC(10, 0) DEFAULT NULL, CHANGE original_price original_price DOUBLE PRECISION DEFAULT NULL');
    }

    /**
     * @param Schema $schema
     */
    public function down(Schema $schema)
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE product CHANGE price price DOUBLE PRECISION NOT NULL, CHANGE original_price original_price DOUBLE PRECISION NOT NULL, CHANGE weight weight NUMERIC(10, 0) NOT NULL, CHANGE acoustic acoustic NUMERIC(10, 0) NOT NULL');
        $this->addSql('ALTER TABLE product DROP FOREIGN KEY FK_D34A04ADE308AC6F');
        $this->addSql('ALTER TABLE product DROP original_price, CHANGE material_id material_id INT NOT NULL, CHANGE price price DOUBLE PRECISION NOT NULL, CHANGE weight weight NUMERIC(10, 0) NOT NULL, CHANGE acoustic acoustic NUMERIC(10, 0) NOT NULL, CHANGE item_id item_id VARCHAR(255) NOT NULL COLLATE utf8_unicode_ci');
        $this->addSql('ALTER TABLE product ADD CONSTRAINT FK_D34A04ADE308AC6F FOREIGN KEY (material_id) REFERENCES material (id)');
    }
}
