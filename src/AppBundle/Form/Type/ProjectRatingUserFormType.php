<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProjectRatingUser;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectRatingUserFormType
 * @package AppBundle\Form\Type
 */
class ProjectRatingUserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('rating', NumberType::class,[
                'required' => true,
                'description' => 'Rating'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectRatingUser::class,
            'csrf_protection' => false,
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'project_rating_user';
    }
}