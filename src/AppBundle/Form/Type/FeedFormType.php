<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Feed;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Enum\FeedAccessType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FeedFormType
 * @package AppBundle\Form\Type
 */
class FeedFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('content', TextareaType::class, [
                'description' => 'Content'
            ])
            ->add('access_type', ChoiceType::class, [
                'choices' => FeedAccessType::getChoices(),
                'description' => FeedAccessType::getDescription(),
                'required' => true
            ])
            ->add('projects', EntityType::class, [
                'class' => Project::class,
                'required' => false,
                'multiple' => true
            ])
            ->add('liked_user_ids', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'mapped' => false,
                'required' => false,
                'description' => 'Read only'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Feed::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'feed';
    }
}