<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\Project;
use AppBundle\Entity\Image;
use AppBundle\Entity\Sector;
use Doctrine\ORM\EntityManager;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class GalleryProjectFormType
 * @package AppBundle\Form\Type
 */
class GalleryProjectFormType extends AbstractType
{
    /**
     * @var EntityManager $manager
     */
    protected $manager;

    /**
     * GalleryProjectFormType constructor.
     * @param EntityManager $manager
     */
    public function __construct($manager)
    {
        $this->manager = $manager;
    }

    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('name', TextType::class, [
                'label' => 'Project Name'
            ])
            ->add('architect', TextType::class, [
                'label' => 'Architect'
            ])
            ->add('location', TextType::class, [
                'label' => 'Location'
            ])
            ->add('builder', TextType::class, [
                'label' => 'Builder'
            ])
            ->add('applications', TextType::class, [
                'label' => 'Applications'
            ])
            ->add('finish', TextType::class, [
                'label' => 'Finish'
            ])
            ->add('sector', EntityType::class, [
                'class' => Sector::class,
                'required' => false
            ])
            ->add('year', TextType::class, [
                'label' => 'Year'
            ])
            ->add('photographer', TextType::class, [
                'label' => 'Photographer'
            ])
            ->add('videoUrl', TextType::class, [
                'label' => 'Input Video URL',
                'required' => false
            ])
            ->add('galleryProjectImages', EntityType::class, [
                'class' => Image\GalleryProjectImage::class,
                'mapped' => false,
                'required' => false
            ])->add('project', EntityType::class, [
                'class' => Project::class,
                'required' => false
            ])
        ;

    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => GalleryProject::class,
            'csrf_protection' => false,
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'gallery_project';
    }
}