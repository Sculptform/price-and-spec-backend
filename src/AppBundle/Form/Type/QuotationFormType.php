<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 20.07.16
 * Time: 10:30
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Project;
use AppBundle\Entity\Quotation;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\DateTimeType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class QuotationFormType
 */
class QuotationFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'required' => true,
                'description' => 'Project id',
            ])
            ->add('title', TextType::class, [
                'description' => 'Title',
            ])
            ->add('attachment_url', TextType::class, [
                'description' => 'Attachment Url',
            ])
            ->add('company_name', TextType::class, [
                'description' => 'Company Name',
            ])
            ->add('name', TextType::class, [
                'description' => 'Full Name',
            ])
            ->add('position', TextType::class, [
                'description' => 'Position',
            ])
            ->add('email', TextType::class, [
                'description' => 'Email',
            ])
            ->add('phone', TextType::class, [
                'description' => 'Phone',
            ])
            ->add('state', TextType::class, [
                'description' => 'State',
            ])
            ->add('included_in_quantities', CheckboxType::class, [
                'label'    => 'Included In Quantities?',
                'required' => false,
            ])
            ->add('project_area', TextType::class, [
                'description' => 'Project area',
            ])
            ->add('content', TextType::class, [
                'description' => 'Content',
                'required' => false,
            ])
            ->add('back_call_date', DateTimeType::class, [
                'widget' => 'single_text',
                'description' => 'Call back date',
                'required' => false,
            ])
            ->add('attachment_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded File',
                'required' => false,
            ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Quotation::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'quotation';
    }
}
