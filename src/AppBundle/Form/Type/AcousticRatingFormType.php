<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\AcousticBacking;
use AppBundle\Entity\AcousticRating;
use AppBundle\Entity\MaterialCoating;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\ProductType;
use AppBundle\Entity\Railing;
use AppBundle\Entity\User;
use AppBundle\Enum\ApplicationType;
use AppBundle\Entity\Project;
use AppBundle\Enum\ProjectStatus;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AcousticRatingFormType
 * @package AppBundle\Form\Type
 */
class AcousticRatingFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('nrc')
            ->add('backing')
            ->add('cavity')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => AcousticRating::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'acoustic_rating';
    }
}