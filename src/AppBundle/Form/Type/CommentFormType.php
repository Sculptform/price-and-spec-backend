<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Comment\AbstractComment;
use AppBundle\Entity\Project;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class CommentFormType
 * @package AppBundle\Form\Type
 */
class CommentFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('id')
            ->add('parent', EntityType::class, [
                'class' => AbstractComment::class,
                'required' => false,
                'description' => 'Parent Comment id'
            ])
            ->add('content', TextType::class, [
                'description' => 'Content'
            ])
            ->add('projects', EntityType::class, [
                'class' => Project::class,
                'required' => false,
                'multiple' => true
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'comment';
    }
}