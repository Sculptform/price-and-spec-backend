<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormEvent;
use Symfony\Component\Form\FormEvents;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('unit', TextType::class,  [
                'description' => 'Unit'
            ])
            ->add('price', null , [
                'description' => 'Price'
            ])
            ->add('weight', null , [
                'description' => 'Weight'
            ])
            ->add('acoustic', null , [
                'description' => 'Acoustic'
            ])
            ->add('item_id', TextType::class,  [
                'description' => 'Item id'
            ])
            ->add('item_number', TextType::class,  [
                'description' => 'Item number'
            ])
            ->add('item_description', TextType::class,  [
                'description' => 'Item description'
            ])
            ->add('material_id', null, ['mapped' => false])
            ->add('material_finish_id', null, ['mapped' => false])
            ->addEventListener(FormEvents::PRE_SUBMIT, function (FormEvent $event) {
                $data = $event->getData();
                $form = $event->getForm();

                $form
                    ->add('material', EntityType::class, [
                        'class' => Material::class,
                        'required' => true,
                        'description' => 'Material id',
                    ])
                    ->add('materialFinish', EntityType::class, [
                        'class' => MaterialFinish::class,
                        'required' => true,
                        'description' => 'Material Finish id',
                    ]);


                $data['material'] = $data['material_id'];
                $data['materialFinish'] = $data['material_finish_id'];

                $event->setData($data);
            })
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Product::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'product';
    }
}