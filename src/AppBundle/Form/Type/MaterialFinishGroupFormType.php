<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MaterialFinishGroupFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialFinishGroupFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('parent', EntityType::class, [
                'class' => MaterialFinishGroup::class,
                'required' => false,
                'description' => 'Parent id'
            ])
            ->add('material_type', EntityType::class, [
                'class' => MaterialType::class,
                'required' => true,
                'description' => 'Material Type id'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MaterialFinishGroup::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'material_finish_group';
    }
}