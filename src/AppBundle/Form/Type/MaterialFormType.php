<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use AppBundle\Entity\Object3D;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ApplicationType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MaterialFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('material_shape_size', EntityType::class, [
                'class' => MaterialShapeSize::class,
                'required' => true,
                'description' => 'Material Shape Size id',
                'invalid_message' => 'Material Shape Size not found'
            ])
            ->add('material_type', EntityType::class, [
                'class' => MaterialType::class,
                'required' => true,
                'description' => 'Material Type id',
                'invalid_message' => 'Material Type not found'
            ])
            ->add('product_type', EntityType::class, [
                'class' => ProductType::class,
                'required' => true,
                'description' => 'Product Type id',
                'invalid_message' => 'Product Type not found',
            ])
            ->add('object3d', EntityType::class, [
                'class' => Object3D::class,
                'required' => true,
                'description' => 'Object3D id',
                'invalid_message' => 'Object3D not found',
            ])
            ->add('application_types', ChoiceType::class, [
                'choices' => ApplicationType::getChoices(),
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'description' => ApplicationType::getDescription()
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Material::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'material';
    }
}