<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\MaterialType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MaterialTypeFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialTypeFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder->add('title', TextType::class, [
            'description' => 'Title'
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MaterialType::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'material_type';
    }

}