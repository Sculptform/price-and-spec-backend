<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use AppBundle\Entity\Railing;
use AppBundle\Entity\Project;
use AppBundle\Entity\Currency;
use AppBundle\Entity\ProductType;
use AppBundle\Enum\ProjectStatus;
use AppBundle\Enum\ApplicationType;
use AppBundle\Entity\AcousticRating;
use AppBundle\Entity\ProjectFilter;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\AcousticBacking;
use AppBundle\Entity\MaterialCoating;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\Extension\Core\Type\CheckboxType;

/**
 * Class ProjectFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ProjectFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('price', TextType::class, [
                'description' => 'Price (float)'
            ])
            ->add('wholesale_price', TextType::class, [
                'description' => 'Wholesale Price (float)'
            ])
            ->add('retail_price', TextType::class, [
                'description' => 'Retail Price (float)'
            ])
            ->add('sequence_ratio', TextType::class, [
                'description' => 'Sequence Ratio (float)'
            ])
            ->add('application_type', ChoiceType::class, [
                'choices' => ApplicationType::getChoices(),
                'description' => ApplicationType::getDescription(),
                'multiple' => false,
                'required' => false,
            ])
            ->add('product_type', EntityType::class, [
                'class' => ProductType::class,
                'required' => true,
                'description' => 'Product Type id'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Image File',
                'required' => false,
            ])
            ->add('status', ChoiceType::class, [
                'choices' => ProjectStatus::getChoices(),
                'description' => ProjectStatus::getDescription(),
                'required' => false,
            ])
            ->add('material_coatings', EntityType::class, [
                'class' => MaterialCoating::class,
                'multiple' => true,
                'description' => 'Material Coatings: {material_type_id: material_coating_id}'
            ])
            ->add('acoustic_backing', EntityType::class, [
                'class' => AcousticBacking::class,
                'required' => false,
                'description' => 'Acoustic Backing id'
            ])
            ->add('acoustic_rating', AcousticRatingFormType::class)
            ->add('users_ids', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
            ->add('is_template', CheckboxType::class, array(
                'label'    => 'Is it template project?',
                'required' => false
            ))
            ->add('railing_id', EntityType::class, [
                'class' => Railing::class,
                'required' => false,
                'description' => 'Railing id',
                'property_path' => 'railing'
            ])
            ->add('railing_finish', EntityType::class, [
                'class' => MaterialFinish::class,
                'required' => false,
                'description' => 'Railing Material Finish id',
                'property_path' => 'railing_finish_id'
            ])
            ->add('is_snap_to_all', CheckboxType::class, [
                'description' => 'Is Snap Set?',
                'required' => false
            ])
            ->add('snap', IntegerType::class, [
                'description' => 'Snap (integer)'
            ])
            ->add('pricing_category', TextType::class, [
                'description' => 'Pricing category (string)'
            ])
            ->add('total_weight', NumberType::class, [
                'description' => 'Total weight (float)'
            ])
            ->add('currency', EntityType::class, [
                'class' => Currency::class
            ])

            ->add('filters', EntityType::class, [
                'class' => ProjectFilter::class,
                'multiple' => true,
                'expanded' => true,
            ])
//            ->add('filters', CollectionType::class, [
//                'entry_type' => ProjectFilterFormType::class,
//                'allow_add'  => true,
//            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Project::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'project';
    }
}