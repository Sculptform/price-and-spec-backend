<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\ProductSandpit;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProductSandpitFormType
 * @package AppBundle\Form\Type
 */
class ProductSandpitFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('description', TextType::class, [
                'description' => 'Description'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Image File',
                'required' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProductSandpit::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'product_sandpit';
    }
}