<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Discussion;
use AppBundle\Enum\DiscussionStatus;
use AppBundle\Enum\DiscussionType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class DiscussionFormType
 * @package AppBundle\Form\Type
 */
class DiscussionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('content', TextType::class, [
                'description' => 'Content'
            ])
            ->add('type', ChoiceType::class, [
                'choices' => DiscussionType::getChoices(),
                'description' => DiscussionType::getDescription(),
                'required' => true,
            ])
            ->add('status', ChoiceType::class, [
                'choices' => DiscussionStatus::getChoices(),
                'description' => DiscussionStatus::getDescription(),
                'required' => true,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Discussion::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'discussion';
    }
}