<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 20.07.16
 * Time: 10:30
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Project;
use AppBundle\Entity\SampleRequest;
use AppBundle\Enum\SampleRequestProjectSize;
use AppBundle\Enum\SampleType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;

/**
 * Class SampleRequestFormType
 */
class SampleRequestFormType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array                $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('project', EntityType::class, [
                'class' => Project::class,
                'required' => true,
                'description' => 'Project id',
            ])

            ->add('project_name', TextType::class, [
                'description' => 'Project Name',
            ])

            ->add('company_name', TextType::class, [
                'description' => 'Company Name',
            ])

            ->add('name', TextType::class, [
                'description' => "User's full name",
            ])

            ->add('content', TextareaType::class, [
                'description' => "Content",
            ])

            ->add('street_address', TextType::class, [
                'description' => 'Postal Address',
            ])

            ->add('phone', TextType::class, [
                'description' => 'Phone',
            ])

            ->add('sample_type', ChoiceType::class, [
                'choices' => SampleType::getChoices(),
                'description' => SampleType::getDescription(),
                'multiple' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => SampleRequest::class,
            'csrf_protection' => false,
        ]);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return '';
    }

    /**
     * @return string
     */
    public function getBlockPrefix()
    {
        return 'sample_requests';
    }
}
