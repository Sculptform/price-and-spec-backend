<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Scene;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class SceneFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class SceneFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('scene', null, [
                'description' => 'Scene'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Scene::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'scene';
    }
}