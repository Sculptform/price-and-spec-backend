<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\MaterialFinishGroup;
use AppBundle\Entity\MaterialType;
use AppBundle\Enum\ApplicationType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class MaterialFinishFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialFinishFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('group', EntityType::class, [
                'class' => MaterialFinishGroup::class,
                'required' => true,
                'description' => 'Material Finish Group Id'
            ])
            ->add('color', TextType::class, [
                'description' => 'Color'
            ])
            ->add('texture_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Texture Image File',
            ])
            ->add('application_types', ChoiceType::class, [
                'choices' => ApplicationType::getChoices(),
                'multiple' => true,
                'expanded' => true,
                'required' => true,
                'description' => ApplicationType::getDescription()
            ])
            ->add('material_type', EntityType::class, [
                'class' => MaterialType::class,
                'required' => true,
                'description' => 'Material Type id'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => MaterialFinish::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'material_finish';
    }
}