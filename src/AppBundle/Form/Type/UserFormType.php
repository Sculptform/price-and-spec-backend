<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\User;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class UserFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('first_name', TextType::class, [
                'description' => 'User first name'
            ])
            ->add('last_name', TextType::class, [
                'description' => 'User last name'
            ])
            ->add('email', EmailType::class, [
                'description' => 'User email'
            ])
            ->add('phone', TextType::class, [
                'description' => 'User phone number'
            ])
            ->add('position', TextType::class, [
                'description' => 'Position'
            ])
            ->add('company_name', TextType::class, [
                'description' => 'Company name'
            ])
            ->add('company_phone', TextType::class, [
                'description' => 'Company phone'
            ])
            ->add('company_address', TextType::class, [
                'description' => 'Company address'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Image File'
            ])
            ->add('cover_image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Cover Image File'
            ])
            ->add('about', TextareaType::class, [
                'description' => 'About'
            ])
            ->add('tutorial_checked', null, [
                'description' => 'Shows if user has already seen tutorial'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => User::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'user';
    }
}