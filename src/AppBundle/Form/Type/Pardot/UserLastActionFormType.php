<?php

namespace AppBundle\Form\Type\Pardot;

use AppBundle\Enum\PardotUserLastAction;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class UserLastActionFormType
 * @package AppBundle\Form\Type\Pardot
 */
class UserLastActionFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder->add('action', ChoiceType::class, [
            'choices' => PardotUserLastAction::getChoices(),
            'required' => true,
            'description' => PardotUserLastAction::getDescription()
        ]);
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(['csrf_protection' => false]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'pardot_user_last_action';
    }
}