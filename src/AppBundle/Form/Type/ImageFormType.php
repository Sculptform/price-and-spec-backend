<?php

namespace AppBundle\Form\Type;

use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TeamImageFormType
 * @package AppBundle\Form\Type
 */
class ImageFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Image File',
                'required' => true
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'image';
    }
}