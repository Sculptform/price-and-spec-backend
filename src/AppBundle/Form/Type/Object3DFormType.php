<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Object3D;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class Object3DFormType
 * @package AppBundle\Form\Type
 */
class Object3DFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('value', TextType::class, [
                'description' => 'Object3D value'
            ])
            ->add('title', TextType::class, [
                'description' => 'Object3D title'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Image File',
                'required' => false,
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Object3D::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'object3d';
    }
}