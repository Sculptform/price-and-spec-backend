<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Advertisement;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\IntegerType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class AdvertisementFormType
 * @package AppBundle\Form\Type
 *
 * sdf
 */
class AdvertisementFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('ordering', IntegerType::class, [
                'description' => 'Position in list'
            ])
            ->add('link', TextType::class, [
                'description' => 'Link URL of the ad'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Base64 Encoded Image File',
            ])
            ->add('is_show', ChoiceType::class, [
                'choices' => [1,0],
                'multiple' => false,
                'description'=>'1 - Show, 0 - Hide'
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Advertisement::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'advertisement';
    }
}
