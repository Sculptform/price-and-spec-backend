<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\Product;
use AppBundle\Entity\ProjectProduct;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class ProjectProductFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ProjectProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        // Custom fields
        $builder
            ->add('product', EntityType::class, [
                'class' => Product::class,
                'required' => true
            ])
            ->add('uuid')
            ->add('height')
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => ProjectProduct::class,
            'csrf_protection' => false
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'project_product';
    }
}