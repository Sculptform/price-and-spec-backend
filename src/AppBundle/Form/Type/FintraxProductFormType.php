<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 23.03.15
 * Time: 18:37
 */

namespace AppBundle\Form\Type;

use AppBundle\Entity\FintraxProduct;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class FintraxProductFormType
 * @package AppBundle\Form\Type
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class FintraxProductFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('title', TextType::class, [
                'description' => 'Title',
                'required' => false
            ])
            ->add('uuid', TextType::class, [
                'description' => 'Fintrax Product UUID',
                'required' => false
            ])
            ->add('base', EntityType::class, [
                'class' => Product::class,
                'required' => true,
                'description' => 'Base Product ID'
            ])
            ->add('nosing', EntityType::class, [
                'class' => Product::class,
                'required' => false,
                'description' => 'Nosing Product ID'
            ])
            ->add('finish', EntityType::class, [
                'class' => MaterialFinish::class,
                'required' => false,
                'description' => 'Material Finish ID'
            ])
            ->add('stackers', EntityType::class, [
                'class' => Product::class,
                'required' => false,
                'description' => 'Stacker Products IDs',
                'multiple' => true,
                'choice_label' => false,
                'mapped' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => FintraxProduct::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'fintrax_product';
    }
}