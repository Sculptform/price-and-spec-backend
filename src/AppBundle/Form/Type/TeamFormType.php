<?php

namespace AppBundle\Form\Type;

use AppBundle\Entity\Project;
use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use AppBundle\Enum\TeamAccessType;
use Hshn\Base64EncodedFile\Form\Type\Base64EncodedFileType;
use Symfony\Bridge\Doctrine\Form\Type\EntityType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\OptionsResolver\OptionsResolver;

/**
 * Class TeamFormType
 * @package AppBundle\Form\Type
 */
class TeamFormType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('id')
            ->add('title', TextType::class, [
                'description' => 'Title'
            ])
            ->add('description', TextareaType::class, [
                'description' => 'Description'
            ])
            ->add('mission', TextareaType::class, [
                'description' => 'Mission'
            ])
            ->add('image_file', Base64EncodedFileType::class, [
                'description' => 'Image File',
                'required' => false,
            ])
            ->add('cover_image_file', Base64EncodedFileType::class, [
                'description' => 'Cover Image File',
                'required' => false,
            ])
            ->add('access_type', ChoiceType::class, [
                'choices' => TeamAccessType::getChoices(),
                'description' => TeamAccessType::getDescription(),
                'required' => false,
            ])
            ->add('users_ids', EntityType::class, [
                'class' => User::class,
                'multiple' => true,
                'mapped' => false,
                'required' => false
            ])
            ->add('projects', EntityType::class, [
                'class' => Project::class,
                'multiple' => true,
                'required' => false
            ])
        ;
    }

    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Team::class,
            'csrf_protection' => false,
            'allow_extra_fields' => true
        ]);
    }

    public function getName()
    {
        return '';
    }

    public function getBlockPrefix()
    {
        return 'team';
    }
}