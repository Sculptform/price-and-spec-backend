<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\FintraxInfo;
use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialFinish;
use AppBundle\Entity\Product;
use AppBundle\Entity\ProductType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Ddeboer\DataImport\Reader\CsvReader;
use Doctrine\Common\Util\Inflector;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * Class LoadProducts
 *
 * @package AppBundle\DataFixtures\ORM
 * @author  Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadProducts extends AbstractFixture implements OrderedFixtureInterface,
    FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->loadCCLFromCSV($manager);
        $this->loadECLFromCSV($manager);
        //$this->loadABFromCSV($manager);
        $this->loadFTXInfo($manager);
    }

    private function loadCCLFromCSV(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $aluminiumMaterialFinishes = $timberMaterialFinishes = [];

        foreach (array_keys($references) as $key) {
            if (false !== strpos($key, 'material_finish_aluminium')) {
                $aluminiumMaterialFinishes[$key] = $this->getReference($key);
            }
            if (false !== strpos($key, 'material_finish_timber')) {
                $timberMaterialFinishes[$key] = $this->getReference($key);
            }
        }

        $file   = new \SplFileObject(
            __DIR__.'/../Data/Products/Click-on Battens 17.09.18.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description(Long)']);
            $itemNumber  = trim($row['Item Number']);
            $price       = (float)$row['Sculptform Price'];
            $weight      = (float)$row['Net Weight'];
            $unit        = 'L/M';

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                [
                    'Profile: ', 'Click-on Batten', '- Set Lengths -',
                    '(max length we can supply in set length is 3.0m)',
                ],
                ['', '', '', ''],
                $description
            );


            if (false !== strpos($description, 'Aluminium')) {
                foreach (
                    $aluminiumMaterialFinishes as $materialFinishKey =>
                    $materialFinish
                ) {
                    /** @var Material $material */
                    $material = $this->getReference(
                        "material_concept_click_aluminium_tube_50x50"
                    );

                    if (false === strpos($description, 'tube')) {
                        preg_match_all('|\d+|', $normalizedDescription, $sizes);
                        list($wide, $deep) = $sizes[0];

                        $material = $this->getReference(
                            "material_concept_click_aluminium_block_{$wide}x{$deep}"
                        );
                    }
                    $item = new Product();

                    $item
                        ->setItemNumber($itemNumber)
                        ->setItemDescription($description)
                        ->setMaterial($material)
                        ->setMaterialFinish($materialFinish)
                        ->setPrice($price)
                        ->setWeight($weight)
                        ->setUnit($unit);

                    $manager->persist($item);

                    $refName = implode(
                        '_', [
                            'product',
                            $item->getItemNumber(),
                            $this->normalizeName($item->getFinishTitle()),
                        ]
                    );

                    $this->setReference($refName, $item);
                }
            } else {
                $woodType = $this->normalizeName(
                    explode(' - ', $description)[0]
                );

                list($finishTitle, $sizeTitle, $profileTitle) = array_map(
                    'trim', explode(' - ', $normalizedDescription)
                );

                preg_match_all('|\d+|', $sizeTitle, $sizes);
                list($wide, $deep) = $sizes[0];

                $woodType = str_replace(
                    ['american_white_oak'], ['white_oak'], $woodType
                );

                $materialFinishRefName
                    = "material_finish_timber_species_{$woodType}";

                $materialRef
                    = "material_concept_click_timber_{$this->normalizeName($profileTitle)}_{$wide}x{$deep}";

                if ($this->hasReference($materialFinishRefName)) {
                    if ($this->hasReference($materialRef)) {
                        /** @var Material $material */
                        $material = $this->getReference($materialRef);

                        /** @var MaterialFinish $materialFinish */
                        $materialFinish = $this->getReference(
                            $materialFinishRefName
                        );

                        $materialShape = $material->getMaterialShapeSize()
                            ->getMaterialShape()->getTitle();

                        $item = new Product();
                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setNaturalAccentPrice(0)
                            ->setEnviroproPrice(0)
                            ->setCutekPrice(0)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);

                        $refName = implode(
                            '_', [
                                'product',
                                $item->getItemNumber(),
                                $this->normalizeName($item->getFinishTitle()),
                            ]
                        );

                        $this->setReference($refName, $item);
                    }
                }
            }
        }
        $manager->flush();
    }

    private function loadECLFromCSV(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            if (false !== strpos(
                    $key, 'material_finish_expression_cladding_timber'
                )
            ) {
                $materialFinishes[$key] = $this->getReference($key);
            }
        }

        $count = 0;

        $file   = new \SplFileObject(
            __DIR__.'/../Data/Products/Sculptform ECL 18.09.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description    = trim($row['Item Description']);
            $itemNumber     = trim($row['Item: Item Number']);
            $price          = (float)$row['Sculptform Price'];
            $enviroproPrice = (float)$row['Natural Accent or Enviropro'];
            $cutekPrice     = (float)$row['Cutek'];
            $weight         = (float)$row['Weights'];
            $unit           = trim($row['Default Unit of Measure']);

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                ['Profile: ', 'Expression Cladding ', 'Yakisugi'],
                ['', '', 'Charred'],
                $description
            );

            list($finishTitle, $sizeTitle, $profileTitle) = array_map(
                'trim', explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list($wide, $deep) = $sizes[0];

            $count++;

            foreach ($materialFinishes as $materialFinishKey => $materialFinish)
            {
                if (false !== strpos(
                        $materialFinishKey, $this->normalizeName($finishTitle)
                    )
                ) {
                    $materialRefName = sprintf(
                        "material_expression_cladding_timber_%s_%sx%s",
                        $this->normalizeName($profileTitle),
                        $wide,
                        $deep
                    );
                    if ($this->hasReference($materialRefName)) {
                        /** @var Material $material */
                        $material = $this->getReference($materialRefName);

                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setCutekPrice($cutekPrice)
                            ->setNaturalAccentPrice($enviroproPrice)
                            ->setEnviroproPrice($enviroproPrice)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);
                        $manager->flush();

                        $refName = implode(
                            '_', [
                                'product',
                                $item->getItemNumber(),
                                $this->normalizeName($item->getFinishTitle()),
                            ]
                        );

                        $this->setReference($refName, $item);
                    }

                    break;
                }
            }
        }

        $manager->flush();
    }

    private function loadABFromCSV(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        /** @var ProductType $productType */
        $productType = $this->getReference('ptype_concept_click');

        /** @var MaterialFinish[] $materialFinishes */
        $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            /*if (false !== strpos($key, 'material_finish_acoustic_blades')) {
                $materialFinishes[$key] = $this->getReference($key);
            }*/
        }

        $file   = new \SplFileObject(
            __DIR__.'/../Data/Products/Sculptform AB 08.09.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Description']);
            $itemNumber  = trim($row['Number']);
            $price       = (float)$row['Sculptform Price'];
            $weight      = (float)$row['Weight'];
            $unit        = trim($row['Unit']);

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                [
                    'Acoustic Linear Blade',
                    'Acoustic Sculpture Blade',
                ],
                [
                    'straight -',
                    'wave -',
                ],
                $description
            );

            list($shapeTitle, $sizeTitle, $finishTitle) = array_map(
                'trim', explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);
            list(, $deep, $wide) = $sizes[0];

            preg_match_all('|\d+|', $finishTitle, $colors);
            list($color) = $colors[0];

            foreach ($materialFinishes as $materialFinish) {
                if ($this->normalizeName($materialFinish->getTitle())
                    === $this->normalizeName($color)
                ) {
                    /** @var Material $material */
                    /*$material = $this->getReference("material_concept_click_acoustic_blades_{$shapeTitle}_{$wide}x{$deep}");*/

                    if ($material) {
                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setWeight($weight)
                            ->setUnit($unit);

                        $manager->persist($item);
                    } else {
                        $this->container->get('logger')->error(
                            'Material not found!', [
                                $productType->getTitle(),
                                $shapeTitle,
                                $wide,
                                $deep,
                            ]
                        );
                    }

                    break;
                }
            }
        }

        $manager->flush();
    }

    private function loadFTXInfo(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            if (false !== strpos(
                    $key, 'material_finish_facade_blade_aluminium'
                )
            ) {
                $materialFinishes[$key] = $this->getReference($key);
            }
        }

        $file   = new \SplFileObject(
            __DIR__.'/../Data/Products/Facade Blades Data.csv'
        );
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $description = trim($row['Item Description(Long)']);
            $itemNumber  = trim($row['Item Number']);
            $price       = (float)$row['Sculptform Price'];
            $weight      = (float)$row['Net Weight'];
            $unit        = trim($row['Default Unit of Measure']);

            if ( ! $description) {
                return;
            }

            $normalizedDescription = str_replace(
                [
                    'Aluminium – Facade Blade ',
                    'Profile: ',
                    'Anodised 20um (External) - Colour -',
                    'Powdercoated Premium Colour -',
                    'Exterior Foil Wrap - Colour -',
                ],
                [
                    '',
                    '',
                    'Anodising',
                    'Powder Coating',
                    'Exterior Foil Wraps',
                ],
                $description
            );


            list($sizeTitle, $profileTitle, $finishGroupTitle) = array_map(
                'trim', explode(' - ', $normalizedDescription)
            );

            preg_match_all('|\d+|', $sizeTitle, $sizes);

            list($wide, $deep) = $sizes[0];

            $finishGroupTitle = $this->normalizeName($finishGroupTitle);

            foreach ($materialFinishes as $materialFinishKey => $materialFinish)
            {
                if (false !== strpos($materialFinishKey, $finishGroupTitle)) {
                    $materialRefName = sprintf(
                        "material_facade_blade_aluminium_%sx%s",
                        $wide,
                        $deep
                    );
                    if ($this->hasReference($materialRefName)) {
                        /** @var Material $material */
                        $material = $this->getReference($materialRefName);

                        $item = new Product();

                        $item
                            ->setItemNumber($itemNumber)
                            ->setItemDescription($description)
                            ->setMaterial($material)
                            ->setMaterialFinish($materialFinish)
                            ->setPrice($price)
                            ->setCutekPrice(0)
                            ->setNaturalAccentPrice(0)
                            ->setEnviroproPrice(0)
                            ->setUnit($unit)
                            ->setWeight($weight);

                        $manager->persist($item);
                        $manager->flush();

                        $refName = implode(
                            '_', [
                                'product',
                                $item->getItemNumber(),
                                $this->normalizeName($item->getFinishTitle()),
                            ]
                        );

                        $this->setReference($refName, $item);
                    }
                }
            }
        }

        $manager->flush();
    }

    private function normalizeName($name)
    {
        return Inflector::tableize(Inflector::classify($name));
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 3;
    }
}
