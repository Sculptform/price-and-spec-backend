<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Product;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;
use Faker\Generator;

/**
 * Class LoadDummyProducts
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadDummyProducts extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{
    /** @var Generator */
    private $facker;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this->facker = Factory::create();

        // $this->loadDummyConceptClickAluminiumWrappers($manager);

        // $this->loadDummyConceptClickAcousticBlades($manager);

        // $this->loadDummyExpressionCladding($manager);

        //$this->loadDummyFintrax($manager);
        $this->loadDummyFacadeBlade($manager);
    }

    private function loadDummyExpressionCladding(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $timberMaterials = $timberMaterialFinishes = [];

        foreach (array_keys($references) as $key) {
            if(false !== strpos($key, 'material_expression_cladding_timber')) {
                $timberMaterials[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_expression_cladding_timber')) {
                $timberMaterialFinishes[$key] = $this->getReference($key);
            }
        }

        // timber
        $i = 1;
        foreach ((array) $timberMaterials as $material) {
            foreach ((array) $timberMaterialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setAcoustic('10')
                    ->setItemId($this->random_str(15))
                    ->setItemNumber($this->random_str(19))
                    ->setItemDescription($this->facker->text(255))
                    ->setPrice($this->facker->randomFloat(2,5,25))
                    ->setRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setWholesalePrice(0)
                    ->setEnviroproPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproWholesalePrice(0)
                    ->setCutekPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekWholesalePrice(0)
                    ->setNaturalAccentPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentWholesalePrice(0)
                    ->setUnit('L/M')
                    ->setWeight(0)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                ;

                $manager->persist($item);

                $this->setReference('product_expression_cladding_dummy_timber_'.$i, $item);

                $i++;
            }
        }

        $manager->flush();
    }

    private function loadDummyConceptClickAluminiumWrappers(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $aluminiumMaterials = $aluminiumMaterialFinishes = [];

        foreach (array_keys($references) as $key) {
            if(false !== strpos($key, 'material_concept_click_aluminium')) {
                $aluminiumMaterials[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_aluminium_exterior_foil')) {
                $aluminiumMaterialFinishes[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_aluminium_interior_foil')) {
                $aluminiumMaterialFinishes[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_aluminium_interior_veneer')) {
                $aluminiumMaterialFinishes[$key] = $this->getReference($key);
            }
        }

        // aluminium
        $i = 1;
        foreach ((array) $aluminiumMaterials as $material) {
            foreach ((array) $aluminiumMaterialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setAcoustic('10')
                    ->setItemId($this->random_str(15))
                    ->setItemNumber($this->random_str(19))
                    ->setItemDescription($this->facker->text(255))
                    ->setPrice($this->facker->randomFloat(2,5,25))
                    ->setRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setWholesalePrice(0)
                    ->setEnviroproPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproWholesalePrice(0)
                    ->setCutekPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekWholesalePrice(0)
                    ->setNaturalAccentPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentWholesalePrice(0)
                    ->setUnit('lm')
                    ->setWeight($i+1)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                ;

                $manager->persist($item);

                $this->setReference('product_concept_click_dummy_aluminium_wraps_'.$i, $item);

                $i++;
            }
        }

        $manager->flush();
    }

    private function loadDummyConceptClickAcousticBlades(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $materials = $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            /*if(false !== strpos($key, 'material_concept_click_acoustic_blades')) {
                $materials[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_acoustic_blades_colour_range')) {
                $materialFinishes[$key] = $this->getReference($key);
            }*/
        }

        $i = 1;
        foreach ((array) $materials as $material) {
            foreach ((array) $materialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setAcoustic('10')
                    ->setItemId($this->random_str(15))
                    ->setItemNumber($this->random_str(19))
                    ->setItemDescription($this->facker->text(255))
                    ->setPrice($this->facker->randomFloat(2,5,25))
                    ->setRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setWholesalePrice(0)
                    ->setEnviroproPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setEnviroproWholesalePrice(0)
                    ->setCutekPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setCutekWholesalePrice(0)
                    ->setNaturalAccentPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentRetailPrice($this->facker->randomFloat(2,5,25))
                    ->setNaturalAccentWholesalePrice(0)
                    ->setUnit('lm')
                    ->setWeight($i+1)
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                ;

                $manager->persist($item);

                /*$this->setReference('product_concept_click_dummy_acoustic_blades_'.$i, $item);*/

                $i++;
            }
        }

        $manager->flush();
    }

    private function loadDummyFintrax(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $materials = $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            if(false !== strpos($key, 'material_fintrax_aluminium')) {
                $materials[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_fintrax')) {
                $materialFinishes[$key] = $this->getReference($key);
            }
        }

        // timber
        $i = 1;
        foreach ((array) $materials as $material) {
            foreach ((array) $materialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setPrice(0)
                    ->setWeight(0)
                    ->setUnit('L/M')
                ;

                $manager->persist($item);

                $this->setReference('product_fintrax_dummy_'.$i, $item);

                $i++;
            }
        }

        $manager->flush();
    }

    private function loadDummyFacadeBlade(ObjectManager $manager)
    {
        $references = $this->referenceRepository->getReferences();

        $materials = $materialFinishes = [];

        foreach (array_keys($references) as $key) {
            if(false !== strpos($key, 'material_facade_blade_aluminium')) {
                $materials[$key] = $this->getReference($key);
            }
            if(false !== strpos($key, 'material_finish_facade_blade')) {
                $materialFinishes[$key] = $this->getReference($key);
            }
        }

        // timber
        $i = 1;
        $prices = [72.88, 83.96, 89.41, 103.25, 103.91, 107.60, 125.96];
        $weight = [3.76, 4.35, 3.76, 4.35, 5.58, 3.76, 5.58];
        foreach ((array) $materials as $material) {
            foreach ((array) $materialFinishes as $materialFinish) {
                $item = new Product();

                $item
                    ->setMaterial($material)
                    ->setMaterialFinish($materialFinish)
                    ->setPrice($prices[rand(0, 6)])
                    ->setWeight($weight[rand(0, 6)])
                    ->setUnit('L/M')
                ;

                $manager->persist($item);

                $this->setReference('product_facade_blade_dummy_'.$i, $item);

                $i++;
            }
        }

        $manager->flush();
    }

    /**
     * Generate a random string, using a cryptographically secure
     * pseudorandom number generator (random_int)
     *
     * For PHP 7, random_int is a PHP core function
     * For PHP 5.x, depends on https://github.com/paragonie/random_compat
     *
     * @param int $length      How many characters do we want?
     * @param string $keyspace A string of all possible characters
     *                         to select from
     * @return string
     */
    private function random_str($length, $keyspace = '0123456789abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ')
    {
        $str = '';
        $max = mb_strlen($keyspace, '8bit') - 1;
        for ($i = 0; $i < $length; ++$i) {
            $str .= $keyspace[random_int(0, $max)];
        }
        return $str;
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 4;
    }
}
