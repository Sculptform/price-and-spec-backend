<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class AppFixtures2
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class AppFixtures2 extends AbstractAliceFixturesLoader implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return [
            //__DIR__ . '/projects.yml',
            //__DIR__ . '/userProjects.yml',
            //__DIR__ . '/projectProducts.yml',
            //__DIR__ . '/scenes.yml',
            // __DIR__ . '/quotations.yml',
            // __DIR__ . '/sampleRequests.yml',
            //__DIR__ . '/news.yml',
            __DIR__ . '/feeds.yml',
            __DIR__ . '/teams.yml',
            __DIR__ . '/teamUsers.yml',
            //__DIR__ . '/projectRatingUsers.yml',
            __DIR__ . '/galleryProject.yml',
            __DIR__ . '/galleryProjectImages.yml',
            __DIR__ . '/discussions.yml',
            __DIR__ . '/notifications.yml',
            __DIR__ . '/sectors.yml',
            __DIR__ . '/polls.yml'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 5;
    }
}
