<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * Class LoadToolboxThumbnails
 *
 * @package AppBundle\DataFixtures\ORM
 * @author  Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadToolboxThumbnails extends AbstractFixture implements FixtureInterface,
    OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $thumbnailsDir = __DIR__.'/../Data/ToolboxThumbnails';

        $shapeSizes = $manager->getRepository(MaterialShapeSize::class)
            ->findAll();

        /** @var MaterialShapeSize $shapeSize */
        foreach ($shapeSizes as $shapeSize) {
            $shape = $shapeSize->getMaterialShape();

            /** @var Material $material */
            foreach ($shapeSize->getMaterials() as $material) {
                $materialType = $material->getMaterialType();
                $productType  = $material->getProductType();

                $imagePath = '';

                if ('Click-on Battens' === $productType->getTitle()) {
                    $imagePath = implode(
                        '/', [
                        $thumbnailsDir,
                        'ClickOnBattens',
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.svg', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]
                    );
                } elseif ('Tongue & Groove Cladding' === $productType->getTitle(
                    )
                ) {
                    if ('Base' === $shape->getTitle()) {
                        continue;
                    }

                    $imagePath = implode(
                        '/', [
                        $thumbnailsDir,
                        'Expression Cladding',
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.svg', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]
                    );
                } elseif ('Facade Blades' === $productType->getTitle()) {
                    $imagePath = implode(
                        '/', [
                        $thumbnailsDir,
                        $productType->getTitle(),
                        sprintf(
                            '%sx%s.svg', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]
                    );
                } else {
                    $this->container->get('logger')->error(
                        'Toolbox thumbnails loading error', [
                        $productType->getTitle(),
                        $materialType->getTitle(),
                        $shape->getTitle(),
                        sprintf(
                            '%sx%s.svg', $shapeSize->getWidth(),
                            $shapeSize->getDepth()
                        ),
                    ]
                    );
                }

                if ($this->fs->exists($imagePath)) {
                    $imageFilename = md5($imagePath);
                    $imageFileInfo = new \SplFileInfo($imagePath);

                    $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                    $this->fs->copy($imagePath, $tmpFilePath, true);

                    $imageFile = new UploadedFile(
                        $tmpFilePath,
                        $imageFilename,
                        MimeTypeGuesser::getInstance()->guess($imagePath),
                        $imageFileInfo->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $shapeSize->setImageFile($imageFile);
                } else {
                    $this->container->get('logger')->error(
                        'Toolbox thumbnails loading error', [
                        $imagePath,
                    ]
                    );
                }
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 7;
    }
}
