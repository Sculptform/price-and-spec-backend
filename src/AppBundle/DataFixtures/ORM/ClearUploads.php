<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;

/**
 * Class ClearUploads
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ClearUploads extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $objectManager)
    {
        $fs = new Filesystem();

        $uploadsDir = __DIR__.'/../../../../web/uploads';

        $fs->remove(
            Finder::create()->in($uploadsDir)
                ->directories()
                ->ignoreDotFiles(true)
                ->ignoreUnreadableDirs()
        );

        $logsDir = __DIR__.'/../../../../var/logs';

        $fs->remove(
            Finder::create()->in($logsDir)
                ->files()
                ->ignoreDotFiles(true)
        );
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 1;
    }
}
