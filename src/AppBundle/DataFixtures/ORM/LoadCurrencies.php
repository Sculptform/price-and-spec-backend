<?php

namespace AppBundle\DataFixtures\ORM;

use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;

/**
 * Class LoadComments
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadCurrencies extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $this
            ->container
            ->get('currency.updater')
            ->update();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return -6;
    }
}
