<?php
/**
 * Created by PhpStorm.
 * User: vladimir
 * Date: 25/09/18
 * Time: 12:22 AM
 */

namespace AppBundle\DataFixtures\ORM;


use AppBundle\Entity\OAuth\Client;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

class LoadOauthClient extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    public function load(ObjectManager $manager)
    {
        $clientId     = $this->container->getParameter('oauth_client_id');
        $clientSecret = $this->container->getParameter('oauth_client_secret');
        $uris         = $this->container->getParameter('oauth_redirect_uris');
        $grantTypes   = $this->container->getParameter('oauth_grant_types');

        $client = new Client();
        $client->setRandomId($clientId);
        $client->setSecret($clientSecret);
        $client->setAllowedGrantTypes($grantTypes);
        $client->setRedirectUris($uris);

        $manager->persist($client);
        $manager->flush();
    }
    public function getOrder()
    {
        return 5;
    }
}