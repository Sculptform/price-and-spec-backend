<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Material;
use AppBundle\Entity\MaterialShapeSize;
use AppBundle\Entity\MaterialType;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\MimeType\MimeTypeGuesser;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Yaml\Yaml;
/**
 * Class LoadToolboxThumbnails
 * @package AppBundle\DataFixtures\ORM
 */
class LoadToolboxPopupMaterialTypes extends AbstractFixture implements FixtureInterface, OrderedFixtureInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /** @var Filesystem */
    private $fs;

    /**
     * LoadFixtures constructor.
     */
    public function __construct()
    {
        $this->fs = new Filesystem();
    }

    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $imagesDir = __DIR__ . '/../Data/ToolboxPopup/MaterialTypes';

        $materialTypes = ['timber', 'aluminium'];
        foreach ($materialTypes as $materialType){
            $material = $manager->getRepository(MaterialType::class)->findOneBy(['title' => ucwords(str_replace('_', ' ', $materialType))]);
            $path = sprintf('%s/%s.jpg', $imagesDir, $materialType);
            if($material && $this->fs->exists($path)) {
                $imageFilename = md5($path);
                $imageFileInfo = new \SplFileInfo($path);

                $tmpFilePath = $this->fs->tempnam('/tmp', 'uploaded-file-');

                $this->fs->copy($path, $tmpFilePath, true);

                $imageFile = new UploadedFile(
                    $tmpFilePath,
                    $imageFilename,
                    MimeTypeGuesser::getInstance()->guess($path),
                    $imageFileInfo->getSize(),
                    UPLOAD_ERR_OK,
                    true
                );

                $material->setPopupImageFile($imageFile);
            } else {
                $this->container->get('logger')->error('Toolbox popup images for material types loading error', [
                    $path
                ]);
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 8;
    }
}
