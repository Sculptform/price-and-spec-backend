<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\Comment\DiscussionComment;
use AppBundle\Entity\Comment\FeedComment;
use AppBundle\Entity\Comment\GalleryProjectComment;
use AppBundle\Entity\Comment\ProductSandpitComment;
use AppBundle\Entity\Comment\ProjectComment;
use AppBundle\Entity\Comment\TeamComment;
use AppBundle\Entity\Discussion;
use AppBundle\Entity\Feed;
use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\ProductSandpit;
use AppBundle\Entity\Project;
use AppBundle\Entity\Team;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Faker\Factory;

/**
 * Class LoadComments
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class LoadComments extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager){
        $references = $this->referenceRepository->getReferences();

        $faker = Factory::create();

        $feeds = $galleryProjects = $teams = $productSandpitList = $projects = $discussions = $users = [];

        foreach (array_keys($references) as $key) {
            if(preg_match("/^feed_[0-9]+$/", $key)) {
                $feeds[$key] = $this->getReference($key);
            }
            if(preg_match("/^gallery_project_[0-9]+$/", $key)) {
                $galleryProjects[$key] = $this->getReference($key);
            }
            if(preg_match("/^team_[0-9]+$/", $key)) {
                $teams[$key] = $this->getReference($key);
            }
            if(preg_match("/^psandpit/", $key)) {
                $productSandpitList[$key] = $this->getReference($key);
            }
            if(preg_match("/^project_[0-9]+$/", $key)) {
                $projects[$key] = $this->getReference($key);
            }
            if(preg_match("/^discussion_[0-9]+$/", $key)) {
                $discussions[$key] = $this->getReference($key);
            }
            if(preg_match("/^user_user_[0-9]+$/", $key)) {
                $users[$key] = $this->getReference($key);
            }
        }

        /** @var Feed $feed */
        foreach ($feeds as $feed)
        {
            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $comment = new FeedComment();

                $commentTime = $faker->dateTimeThisYear;

                $comment
                    ->setFeed($feed)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childComment = new FeedComment();

                    $childComment
                        ->setFeed($feed)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($commentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();

        /** @var Team $team */
        foreach ($teams as $team)
        {
            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $comment = new TeamComment();

                $commentTime = $faker->dateTimeThisYear;

                $comment
                    ->setTeam($team)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childComment = new TeamComment();

                    $childComment
                        ->setTeam($team)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($commentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();

        /** @var ProductSandpit $productSandpit */
        foreach ($productSandpitList as $productSandpit)
        {
            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $comment = new ProductSandpitComment();

                $commentTime = $faker->dateTimeThisYear;

                $comment
                    ->setProductSandpit($productSandpit)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childComment = new ProductSandpitComment();

                    $childComment
                        ->setProductSandpit($productSandpit)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($commentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();

        /** @var Project $project */
        foreach ($projects as $project)
        {
            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $comment = new ProjectComment();

                $commentTime = $faker->dateTimeThisYear;

                $comment
                    ->setProject($project)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childComment = new ProjectComment();

                    $childComment
                        ->setProject($project)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($commentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();

        /** @var Discussion $discussion */
        foreach ($discussions as $discussion)
        {
            $commentTime = $faker->dateTimeThisYear;

            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $commentTime = $faker->dateTimeBetween($commentTime);

                $comment = new DiscussionComment();

                $comment
                    ->setDiscussion($discussion)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                $childCommentTime = $faker->dateTimeBetween($commentTime);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childCommentTime = $faker->dateTimeBetween($childCommentTime);

                    $childComment = new DiscussionComment();

                    $childComment
                        ->setDiscussion($discussion)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($childCommentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();

        /** @var GalleryProject $galleryProject */
        foreach ($galleryProjects as $galleryProject)
        {
            foreach ($faker->sentences($faker->numberBetween(1, 3)) as $commentContent) {
                $comment = new GalleryProjectComment();

                $commentTime = $faker->dateTimeThisYear;

                $comment
                    ->setGalleryProject($galleryProject)
                    ->setContent($commentContent)
                    ->setCreatedAt($commentTime)
                    ->setUser($faker->randomElement($users))
                ;

                $manager->persist($comment);

                foreach ($faker->sentences($faker->numberBetween(0, 3)) as $childCommentContent) {
                    $childComment = new GalleryProjectComment();

                    $childComment
                        ->setGalleryProject($galleryProject)
                        ->setParent($comment)
                        ->setContent($childCommentContent)
                        ->setCreatedAt($faker->dateTimeBetween($commentTime))
                        ->setUser($faker->randomElement($users))
                    ;

                    $manager->persist($childComment);
                }
            }
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return -6;
    }
}
