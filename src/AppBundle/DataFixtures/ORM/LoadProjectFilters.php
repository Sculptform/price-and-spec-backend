<?php

namespace AppBundle\DataFixtures\ORM;

use AppBundle\Entity\ProjectFilter;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class LoadProjectFilters
 *
 * @package AppBundle\DataFixtures\ORM
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class LoadProjectFilters extends AbstractFixture implements OrderedFixtureInterface, FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $filters = [
            'Most popular',
            'Timber',
            'Aluminium',
            'Affordable'
        ];

        foreach ($filters as $filterName) {
            $filter = new ProjectFilter();
            $filter->setName($filterName);
            $manager->persist($filter);
        }

        $manager->flush();
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return -10;
    }
}
