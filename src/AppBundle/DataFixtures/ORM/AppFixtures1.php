<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\ORM;

use AppBundle\DataFixtures\Loader\AbstractAliceFixturesLoader;
use AppBundle\Entity\ProductType;
use Doctrine\Common\DataFixtures\OrderedFixtureInterface;

/**
 * Class AppFixtures1
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class AppFixtures1 extends AbstractAliceFixturesLoader implements OrderedFixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function setUp()
    {
        parent::setUp();

        $this->allowFixedIdsFor([ProductType::class]);
    }

    /**
     * {@inheritDoc}
     */
    public function getFixtures()
    {
        return [
            __DIR__ . '/users.yml',
            __DIR__ . '/userFollowers.yml',
            __DIR__ . '/advertisements.yml',
            __DIR__ . '/object3ds.yml',
            __DIR__ . '/productTypes.yml',
            __DIR__ . '/productSandpit.yml',
            __DIR__ . '/materialTypes.yml',
            __DIR__ . '/materialShapes.yml',
            __DIR__ . '/materialShapeSizes.yml',
            __DIR__ . '/materials.yml',
            __DIR__ . '/materialFinishGroups.yml',
            __DIR__ . '/materialFinishesCCLTimber.yml',
            __DIR__ . '/materialFinishesCCLAluminium.yml',
            __DIR__ . '/materialFinishesFTXAluminium.yml',
            __DIR__ . '/materialFinishesFBAluminium.yml',
            //__DIR__ . '/materialFinishesCCLAcousticBlades.yml',
            __DIR__ . '/materialFinishesECLTimber.yml',
            __DIR__ . '/defaultMaterialFinish.yml',
            __DIR__ . '/railingFinishesAluminium.yml',
            __DIR__ . '/railingFinishesTimber.yml',
            __DIR__ . '/materialCoatings.yml',
            __DIR__ . '/acousticBackings.yml',
            __DIR__ . '/railings.yml'
        ];
    }

    /**
     * {@inheritDoc}
     */
    public function getOrder()
    {
        return 2;
    }
}
