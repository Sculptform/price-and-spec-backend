<?php

namespace AppBundle\DataFixtures\Processor;

use AppBundle\Entity\User;
use Nelmio\Alice\ProcessorInterface;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class UserProcessor implements ProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function preProcess($object)
    {
        if ($object instanceof User) {
            if ($object->getImagePath()) {
                $file = new \SplFileInfo($object->getImagePath());

                if ($file->isFile() && $file->isReadable()) {
                    $uploadedFile = new UploadedFile(
                        $file->getRealPath(),
                        $file->getFilename(),
                        null,
                        $file->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $object->setImageFile($uploadedFile);
                }
            }

            if ($object->getCoverImagePath()) {
                $file = new \SplFileInfo($object->getCoverImagePath());

                if ($file->isFile() && $file->isReadable()) {
                    $uploadedFile = new UploadedFile(
                        $file->getRealPath(),
                        $file->getFilename(),
                        null,
                        $file->getSize(),
                        UPLOAD_ERR_OK,
                        true
                    );

                    $object->setCoverImageFile($uploadedFile);
                }
            }
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postProcess($object)
    {
    }
}
