<?php

namespace AppBundle\DataFixtures\Processor;

use AppBundle\Entity\ProjectProduct;
use AppBundle\Entity\Scene;
use Nelmio\Alice\ProcessorInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class SceneProcessor implements ProcessorInterface, ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * {@inheritdoc}
     */
    public function preProcess($object)
    {
        if ($object instanceof Scene) {
            $sceneData = $object->getScene();
            $scene = &$sceneData['scene'];

            $sceneImages = &$scene['images'];
            $sceneMaterials = &$scene['materials'];
            $sceneTextures = &$scene['textures'];
            $sceneObjects = &$scene['object']['children'];

            $project = $object->getProject();

            /** @var ProjectProduct[] $projectProducts */
            $projectProducts = $project->getProjectProducts();

            foreach ($sceneObjects as &$sceneObject) {
                if (!isset($sceneObject['woodformShapeType'])) {
                    $sceneObject['woodformShapeType'] = '';
                }
            }

            foreach ($projectProducts as $projectProduct) {
                $product = $projectProduct->getProduct();
                $material = $product->getMaterial();

                $sceneObjectKey = array_search($projectProduct->getUuid(), array_column((array)$sceneObjects, 'uuid'));

                if (null !== $sceneObjectKey) {
                    $sceneObject = &$sceneObjects[$sceneObjectKey];

                    $sceneObject['woodformShapeType'] = $material->getMaterialShapeSize()->getMaterialShape()->getName();

                    if (!isset($sceneObject['materialModel'])) {
                        $sceneObject['materialModel'] = $material->getId();
                    }

                    $sceneMaterialKey = array_search($sceneObject['material'], array_column((array)$sceneMaterials, 'uuid'));

                    if (null !== $sceneMaterialKey) {
                        $sceneMaterial = &$sceneMaterials[$sceneMaterialKey];

                        $sceneTextureKey = array_search($sceneMaterial['map'], array_column((array)$sceneTextures, 'uuid'));

                        if (null !== $sceneTextureKey) {
                            $sceneTexture = &$sceneTextures[$sceneTextureKey];

                            $sceneImageKey = array_search($sceneTexture['image'], array_column((array)$sceneImages, 'uuid'));

                            if (null !== $sceneImageKey) {
                                $sceneImage = &$sceneImages[$sceneImageKey];

                                $sceneImage['url'] = sprintf(
                                    '%s/%s',
                                    $this->container->getParameter('host'),
                                    $projectProduct->getProduct()->getMaterialFinish()->getTextureWebPath()
                                );
                            }
                        }
                    }
                }
            }

            $object->setScene($sceneData);
        }
    }

    /**
     * {@inheritdoc}
     */
    public function postProcess($object)
    {
    }
}
