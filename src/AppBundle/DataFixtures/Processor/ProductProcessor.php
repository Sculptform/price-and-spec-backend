<?php

namespace AppBundle\DataFixtures\Processor;

use AppBundle\Entity\Product;
use Nelmio\Alice\ProcessorInterface;

/**
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
class ProductProcessor implements ProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function preProcess($object)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function postProcess($object)
    {
    }
}
