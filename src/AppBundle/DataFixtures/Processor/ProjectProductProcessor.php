<?php
/**
 * Class ProjectProductProcessor
 * @package AppBundle\DataFixtures\Processor
 * @author Andrey Zaytsev <dreyup96@gmail.com>
 */

namespace AppBundle\DataFixtures\Processor;

use AppBundle\Entity\ProjectProduct;
use Nelmio\Alice\ProcessorInterface;

class ProjectProductProcessor implements ProcessorInterface
{
    /**
     * {@inheritdoc}
     */
    public function preProcess($object)
    {
    }

    /**
     * {@inheritdoc}
     */
    public function postProcess($object)
    {
        if (!$object instanceof ProjectProduct) {
            return;
        }

        $project = $object->getProject();
        $project->setPrice($project->getPrice() + $object->getProduct()->getPrice() * $object->getHeight());
        $project->setWholesalePrice($project->getPrice() - 200);
        $project->setRetailPrice($project->getPrice() + 200);
    }
}