<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 09.06.17
 * Time: 18:56
 */

namespace AppBundle\DataFixtures\Migration;

use Doctrine\Common\DataFixtures\Executor\ORMExecutor;
use Doctrine\Common\DataFixtures\Purger\ORMPurger;
use Doctrine\DBAL\Migrations\AbstractMigration;
use Symfony\Bridge\Doctrine\DataFixtures\ContainerAwareLoader as Loader;
use Symfony\Component\Console\Output\ConsoleOutput;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;

abstract class AbstractFixtureMigration extends AbstractMigration implements ContainerAwareInterface
{
    use ContainerAwareTrait;

    /**
     * @param array $fixtures
     * @param bool $append
     */
    public function loadFixtures(array $fixtures, $append = true)
    {
        $em = $this->container->get('doctrine.orm.entity_manager');

        $loader = new Loader($this->container);

        array_map(array($loader, 'addFixture'), $fixtures);

        $purger = null;

        if ($append === false) {
            $purger = new ORMPurger($em);
            $purger->setPurgeMode(ORMPurger::PURGE_MODE_DELETE);
        }

        $executor = new ORMExecutor($em, $purger);

        $output = new ConsoleOutput;

        $executor->setLogger(function($message) use ($output) {
            $output->writeln(sprintf('  <comment>></comment> <info>%s</info>', $message));
        });

        $executor->execute($loader->getFixtures(), $append);
    }
}