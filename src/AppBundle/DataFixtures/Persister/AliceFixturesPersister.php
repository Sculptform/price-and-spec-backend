<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 06.03.17
 * Time: 15:41
 */

namespace AppBundle\DataFixtures\Persister;

use Doctrine\Common\Persistence\ObjectManager;
use Nelmio\Alice\PersisterInterface;

class AliceFixturesPersister implements PersisterInterface
{
    /**
     * @var ObjectManager
     */
    protected $om;

    /**
     * AliceFixturesPersister constructor.
     * @param ObjectManager $om
     */
    public function __construct(ObjectManager $om)
    {
        $this->om = $om;
    }

    /**
     * Loads a fixture file
     *
     * @param array[object] $objects instance to persist in the DB
     */
    public function persist(array $objects)
    {
        foreach ($objects as $object) {
            $this->om->persist($object);
        }
    }

    /**
     * Finds an object by class and id
     *
     * @param  string $class
     * @param  int    $id
     * @return mixed
     */
    public function find($class, $id)
    {
        return $this->om->find($class, $id);
    }
}