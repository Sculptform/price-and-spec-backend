<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.09.2016
 * Time: 18:43
 */

namespace AppBundle\DataFixtures\Provider;

use Hshn\Base64EncodedFile\HttpFoundation\File\Base64EncodedFile;
use Hshn\Base64EncodedFile\HttpFoundation\File\UploadedBase64EncodedFile;

final class Base64EncodedFileProvider
{
    public function base64EncodedFile($base64EncodedFileString) {
        $base64EncodedFile = new Base64EncodedFile($base64EncodedFileString);

        return new UploadedBase64EncodedFile($base64EncodedFile);
    }
}
