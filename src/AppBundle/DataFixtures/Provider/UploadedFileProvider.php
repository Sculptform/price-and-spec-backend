<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.09.2016
 * Time: 18:43
 */

namespace AppBundle\DataFixtures\Provider;

use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\HttpFoundation\File\File;
use Symfony\Component\HttpFoundation\File\UploadedFile;

final class UploadedFileProvider
{
    /**
     * @return static
     */
    public static function create()
    {
        return new static();
    }

    /**
     * @param string $path
     * @param string $filename
     * @param string $basePath
     *
     * @return UploadedFile|bool
     */
    public function uploadedFile($path, $filename = null, $basePath = null)
    {
        $basePath = $basePath ?: __DIR__ . DIRECTORY_SEPARATOR . '..';
        $origFilePath = $basePath . DIRECTORY_SEPARATOR . $path;

        $origFile = new File($origFilePath);

        $tempDir = implode(DIRECTORY_SEPARATOR, [__DIR__, '..', '..', '..', '..', 'web/temp']);

        $tmpFilename = tempnam($tempDir, 'uploaded');

        $fs = new Filesystem();
        $fs->copy($origFile->getRealPath(), $tmpFilename, true);

        $tmpFile = new File($tmpFilename);

        return new UploadedFile(
            $tmpFile->getPathname(),
            $filename ?: $tmpFile->getFilename(),
            $tmpFile->getMimeType(),
            $tmpFile->getSize(),
            UPLOAD_ERR_OK,
            true
        );
    }
}
