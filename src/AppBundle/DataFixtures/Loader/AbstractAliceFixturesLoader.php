<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 13.09.16
 * Time: 15:21
 */

namespace AppBundle\DataFixtures\Loader;

use AppBundle\DataFixtures\Persister\AliceFixturesPersister;
use Doctrine\Common\DataFixtures\AbstractFixture;
use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Doctrine\ORM\Mapping\ClassMetadata;
use Hautelook\AliceBundle\Doctrine\DataFixtures\LoaderInterface;
use Nelmio\Alice\ProcessorInterface;
use Symfony\Component\DependencyInjection\ContainerAwareInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Class AbstractAliceFixturesLoader
 * @package AppBundle\DataFixtures\ORM
 * @author Dmitry Ramenev <dmitry.ramenev@sibers.com>
 */
abstract class AbstractAliceFixturesLoader extends AbstractFixture implements FixtureInterface, ContainerAwareInterface, LoaderInterface
{
    use ContainerAwareTrait;

    /** @var ObjectManager */
    protected $om;

    /** @var array */
    protected $errorMessages = [];

    /** @var array */
    protected $idGeneratorTypes = [];

    /*** @return ContainerInterface */
    protected function getContainer() {
        return $this->container;
    }

    /**
     * {@inheritDoc}
     */
    public abstract function getFixtures();

    /**
     * @param ObjectManager $om
     * @throws \Exception
     */
    public function load(ObjectManager $om) {
        $this->om = $om;

        $this->setUp();

        $normalizedFixturesFiles = $this->normalizeFixturesFiles($this->getFixtures());

        $references = [];

        foreach (array_keys($this->referenceRepository->getReferences()) as $referenceName) {
            $references[$referenceName] = $this->getReference($referenceName);
        }

        $this->tryToLoadFiles($normalizedFixturesFiles, $references);

        if($this->errorMessages) {
            $this->getContainer()->get('logger')->error('Fixtures loading error', $this->errorMessages);
            throw new \Exception('Fixtures Loading Error');
        }

        $this->flush();

        $this->recoverIdGenerators();
    }

    /**
     * Setup some params before load fixtures
     */
    public function setUp()
    {
        ini_set('pcre.backtrack_limit', 10000000);
    }

    protected function flush(){
        $this->om->flush();
    }

    protected function allowFixedIdsFor(array $entityClasses)
    {
        foreach ($entityClasses as $entityClass) {
            /** @var ClassMetadata $metadata */
            $metadata = $this->om->getClassMetadata($entityClass);
            $this->idGeneratorTypes[$entityClass] = $metadata->generatorType;
            $metadata->setIdGeneratorType(ClassMetadata::GENERATOR_TYPE_NONE);
        }
    }

    private function recoverIdGenerators()
    {
        foreach ($this->idGeneratorTypes as $entityClass => $idGeneratorType) {
            /** @var ClassMetadata $metadata */
            $metadata = $this->om->getClassMetadata($entityClass);
            $metadata->setIdGeneratorType($idGeneratorType);
        }
    }

    /**
     * Returns an array where the key is the fixture file path and the value is the boolean false value.
     *
     * @param string[] $fixturesFiles
     *
     * @return array
     */
    private function normalizeFixturesFiles(array $fixturesFiles)
    {
        $normalizedFixturesFiles = array_flip($fixturesFiles);

        foreach ($normalizedFixturesFiles as $fileRealPath => $index) {
            $normalizedFixturesFiles[$fileRealPath] = false;
        }

        return $normalizedFixturesFiles;
    }

    /**
     * Goes through all fixtures files to try to load them one by one and specify for each if the file could
     * successfuly be loaded or not.
     *
     * @param array         $normalizedFixturesFiles Array with the file real path as key and true as a value if
     *                                                    the files has been loaded.
     * @param array         $references
     */
    private function tryToLoadFiles(array &$normalizedFixturesFiles, array $references)
    {
        foreach ($normalizedFixturesFiles as $fixtureFilePath => $hasBeenLoaded) {
            if (true === $hasBeenLoaded) {
                continue;
            }

            try {
                $loader = $this->getContainer()->get('hautelook_alice.alice.fixtures.loader');

                $loader->setPersister(new AliceFixturesPersister($this->om));

                $objects = $loader->load($fixtureFilePath, $references);

                $normalizedFixturesFiles[$fixtureFilePath] = true;

                $this->persist($objects);

                $references = array_merge($references, $objects);
            } catch (\UnexpectedValueException $exception) {
                $this->registerErrorMessage($fixtureFilePath, $exception->getMessage());
            }
        }
    }

    /**
     * Uses the Fixture persister to persist objects and calling the processors.
     *
     * @param object[] $objects
     */
    private function persist(array $objects)
    {
        /** @var ProcessorInterface[] $processors */
        $processors = $this->getContainer()->get('hautelook_alice.fixtures.loader')->getProcessors();

        foreach ($processors as $processor) {
            foreach ($objects as $object) {
                $processor->preProcess($object);
            }
        }

        foreach ($objects as $object) {
            $this->om->persist($object);
        }

        foreach ($processors as $processor) {
            foreach ($objects as $object) {
                $processor->postProcess($object);
            }
        }

        foreach ($objects as $referenceName => $referenceObject) {
            $this->addReference($referenceName, $referenceObject);
        }
    }

    /**
     * Registers the error message with the related fixture file.
     *
     * @param string $fixtureFilePath
     * @param string $errorMessage
     */
    private function registerErrorMessage($fixtureFilePath, $errorMessage)
    {
        if (true === empty($errorMessage)) {
            return;
        }

        if (!isset($this->errorMessages[$fixtureFilePath])) {
            $this->errorMessages[$fixtureFilePath] = [];
        }

        array_push($this->errorMessages[$fixtureFilePath], $errorMessage);
    }
}
