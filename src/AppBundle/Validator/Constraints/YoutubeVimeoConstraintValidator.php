<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;
use Symfony\Component\Validator\ConstraintValidator;

/**
 * Class YoutubeVimeoConstraintValidator
 * @package AppBundle\Validator\Constraints
 */
class YoutubeVimeoConstraintValidator extends ConstraintValidator
{
    /**
     * @param mixed $value
     * @param Constraint $constraint
     */
    public function validate($value, Constraint $constraint)
    {
        if (
            $constraint instanceof YoutubeVimeoConstraint &&
            !(strpos($value, 'youtube') > 0) &&
            !(strpos($value, 'vimeo') > 0) &&
            strlen($value) > 0
        ) {
            $this->context->buildViolation($constraint->message)
                ->setParameter('%string%', $value)
                ->addViolation();
        }
    }
}