<?php

namespace AppBundle\Validator\Constraints;

use Symfony\Component\Validator\Constraint;

/**
 *
 * @Annotation
 *
 * Checks where the url contains youtube or vimeo urls
 *
 * Class YoutubeVimeoConstraint
 * @package AppBundle\Validator\Constraints
 */
class YoutubeVimeoConstraint  extends Constraint
{
    public $message = 'The url "%string%" is not allowed, only Youtube or Vimeo urls are allowed.';
}