<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 08.08.17
 * Time: 14:21
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Feed
 *
 * @ORM\Table(name="fintrax_infos")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FintraxInfoRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class FintraxInfo
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * Calculated Price material
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * Wight material
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * Width
     *
     * @var float
     * @ORM\Column(type="decimal", nullable=false)
     */
    protected $width;

    /**
     * Depth
     *
     * @var float
     * @ORM\Column(type="decimal", nullable=false)
     */
    protected $depth;

    /**
     * @var MaterialFinishGroup
     *
     * @ORM\ManyToOne(targetEntity="MaterialFinishGroup", cascade={"persist"})
     * @ORM\JoinColumn(name="finish_group_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @JMS\Exclude
     */
    protected $materialFinishGroup;

    /**
     * @var MaterialShape
     *
     * @ORM\ManyToOne(targetEntity="MaterialShape", cascade={"persist"})
     * @ORM\JoinColumn(name="nosing_id", referencedColumnName="id", onDelete="SET NULL", nullable=true)
     * @JMS\Exclude
     */
    protected $materialShape;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return FintraxInfo
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return FintraxInfo
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return FintraxInfo
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return FintraxInfo
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set materialFinishGroup
     *
     * @param MaterialFinishGroup $materialFinishGroup
     *
     * @return FintraxInfo
     */
    public function setMaterialFinishGroup(MaterialFinishGroup $materialFinishGroup = null)
    {
        $this->materialFinishGroup = $materialFinishGroup;

        return $this;
    }

    /**
     * Get materialFinishGroup
     *
     * @return MaterialFinishGroup
     */
    public function getMaterialFinishGroup()
    {
        return $this->materialFinishGroup;
    }

    /**
     * Set width
     *
     * @param string $width
     *
     * @return FintraxInfo
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return string
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set depth
     *
     * @param string $depth
     *
     * @return FintraxInfo
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get depth
     *
     * @return string
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set materialShape
     *
     * @param MaterialShape $materialShape
     *
     * @return FintraxInfo
     */
    public function setMaterialShape(MaterialShape $materialShape = null)
    {
        $this->materialShape = $materialShape;

        return $this;
    }

    /**
     * Get materialShape
     *
     * @return MaterialShape
     */
    public function getMaterialShape()
    {
        return $this->materialShape;
    }

    /**
     * Nosing Id
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @return int
     */
    public function getNosingId()
    {
        return $this->materialShape ? $this->materialShape->getId() : null;
    }
}
