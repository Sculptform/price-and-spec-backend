<?php
/**
 * Created by PhpStorm.
 * User: dreyup
 * Date: 24.04.17
 * Time: 10:41
 */
namespace AppBundle\Entity\NewsFeed;

use AppBundle\Enum\NewsFeedType;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class AbstractNewsFeed
 * @package AppBundle\Entity\NewsFeed
 * @JMS\ExclusionPolicy("NONE")
 * @JMS\Discriminator(field = "type", disabled = false, map = {NewsFeedType::DRUPAL: "AppBundle\Entity\NewsFeed\DrupalNewsFeed"})
 */
abstract class AbstractNewsFeed
{
    use TimestampableEntity;

    /**
     * id
     * @JMS\Type("integer")
     * @JMS\Groups("List")
     * @var integer
     */
    protected $id;

    /**
     * Title
     * @JMS\Type("string")
     * @JMS\Groups("List")
     * @var string
     */
    protected $title;

    /**
     * Content
     * @JMS\Type("string")
     * @JMS\Groups("List")
     * @var string
     */
    protected $content;

    /**
     * Content
     * @JMS\Type("string")
     * @JMS\Groups("List")
     * @var string
     */
    protected $imageUrl;

    /**
     * Content
     * @JMS\Type("string")
     * @JMS\Groups("List")
     * @var string
     */
    protected $url;

    /**
     * @return int
     * @JMS\Type("integer")
     * @see NewsFeedType
     */
    abstract public function getType();

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getImageUrl()
    {
        return $this->imageUrl;
    }

    /**
     * @param string $imageUrl
     */
    public function setImageUrl($imageUrl)
    {
        $this->imageUrl = $imageUrl;
    }

    /**
     * @return string
     */
    public function getUrl()
    {
        return $this->url;
    }

    /**
     * @param string $url
     */
    public function setUrl($url)
    {
        $this->url = $url;
    }

}
