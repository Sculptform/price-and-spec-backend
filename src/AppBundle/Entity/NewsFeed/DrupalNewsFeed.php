<?php
/**
 * Created by PhpStorm.
 * User: dreyup
 * Date: 24.04.17
 * Time: 10:41
 */

namespace AppBundle\Entity\NewsFeed;

use AppBundle\Enum\NewsFeedType;

class DrupalNewsFeed extends AbstractNewsFeed
{
    /**
     * @return int
     */
    public function getType() {
        return NewsFeedType::DRUPAL;
    }
}