<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:14
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductRepository")
 * @ORM\Table
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Product
{
    const BASE_PRICE = 30.7;

    /**
     * Material id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Material
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Material", inversedBy="products", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="material_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $material;

    /**
     * @var MaterialFinish
     *
     * @JMS\Exclude
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MaterialFinish", inversedBy="products", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="material_finish_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     * })
     */
    protected $materialFinish;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\ProjectProduct", mappedBy="product", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $projectProducts;

    /**
     * Unit name
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $unit;

    /**
     * Calculated Price material
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $price;

    /**
     * Natural Accent Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $naturalAccentPrice;

    /**
     * Natural Accent Wholesale Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $naturalAccentWholesalePrice;

    /**
     * Natural Accent Retail Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $naturalAccentRetailPrice;

    /**
     * Cutek Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cutekPrice;

    /**
     * Cutek Wholesale Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cutekWholesalePrice;

    /**
     * Cutek Retail Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $cutekRetailPrice;

    /**
     * Enviropro Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $enviroproPrice;

    /**
     * Enviropro Wholesale Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $enviroproWholesalePrice;

    /**
     * Enviropro Retail Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $enviroproRetailPrice;

    /**
     * Wholesale Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $wholesalePrice;

    /**
     * Retail Price
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $retailPrice;

    /**
     * Wight material
     * 
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $weight;

    /**
     * Acoustic material
     * 
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $acoustic;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $itemId;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $itemNumber;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $itemDescription;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projectProducts = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set unit
     *
     * @param string $unit
     *
     * @return Product
     */
    public function setUnit($unit)
    {
        $this->unit = $unit;

        return $this;
    }

    /**
     * Get unit
     *
     * @return string
     */
    public function getUnit()
    {
        return $this->unit;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Product
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set weight
     *
     * @param float $weight
     *
     * @return Product
     */
    public function setWeight($weight)
    {
        $this->weight = $weight;

        return $this;
    }

    /**
     * Get weight
     *
     * @return float
     */
    public function getWeight()
    {
        return $this->weight;
    }

    /**
     * Set acoustic
     *
     * @param float $acoustic
     *
     * @return Product
     */
    public function setAcoustic($acoustic)
    {
        $this->acoustic = $acoustic;

        return $this;
    }

    /**
     * Get acoustic
     *
     * @return float
     */
    public function getAcoustic()
    {
        return $this->acoustic;
    }

    /**
     * Set itemId
     *
     * @param string $itemId
     *
     * @return Product
     */
    public function setItemId($itemId)
    {
        $this->itemId = $itemId;

        return $this;
    }

    /**
     * Get itemId
     *
     * @return string
     */
    public function getItemId()
    {
        return $this->itemId;
    }

    /**
     * Set itemNumber
     *
     * @param string $itemNumber
     *
     * @return Product
     */
    public function setItemNumber($itemNumber)
    {
        $this->itemNumber = $itemNumber;

        return $this;
    }

    /**
     * Get itemNumber
     *
     * @return string
     */
    public function getItemNumber()
    {
        return $this->itemNumber;
    }

    /**
     * Set itemDescription
     *
     * @param string $itemDescription
     *
     * @return Product
     */
    public function setItemDescription($itemDescription)
    {
        $this->itemDescription = $itemDescription;

        return $this;
    }

    /**
     * Get itemDescription
     *
     * @return string
     */
    public function getItemDescription()
    {
        return $this->itemDescription;
    }

    /**
     * Set material
     *
     * @param Material $material
     *
     * @return Product
     */
    public function setMaterial(Material $material)
    {
        $this->material = $material;

        return $this;
    }

    /**
     * Get material
     *
     * @return Material
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * Set materialFinish
     *
     * @param MaterialFinish $materialFinish
     *
     * @return Product
     */
    public function setMaterialFinish(MaterialFinish $materialFinish)
    {
        $this->materialFinish = $materialFinish;

        return $this;
    }

    /**
     * Get materialFinish
     *
     * @return MaterialFinish
     */
    public function getMaterialFinish()
    {
        return $this->materialFinish;
    }

    /**
     * Add projectProduct
     *
     * @param ProjectProduct $projectProduct
     *
     * @return Product
     */
    public function addProjectProduct(ProjectProduct $projectProduct)
    {
        $this->projectProducts[] = $projectProduct;

        return $this;
    }

    /**
     * Remove projectProduct
     *
     * @param ProjectProduct $projectProduct
     */
    public function removeProjectProduct(ProjectProduct $projectProduct)
    {
        $this->projectProducts->removeElement($projectProduct);
    }

    /**
     * Get projectProducts
     *
     * @return Collection|ProjectProduct[]
     */
    public function getProjectProducts()
    {
        return $this->projectProducts;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Material Finish id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return int|null
     */
    public function getMaterialFinishId()
    {
        return $this->getMaterialFinish() ? $this->getMaterialFinish()->getId() : null;
    }

    /**
     * Material id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return int|null
     */
    public function getMaterialId()
    {
        return $this->getMaterial()->getId();
    }

    /**
     * Material Type id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return int|null
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterial()->getMaterialTypeId();
    }

    /**
     * Material Type id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return int|null
     */
    public function getMaterialShapeId()
    {
        return $this->getMaterial()->getMaterialShapeSize()->getMaterialShapeId();
    }

    /***********************************************/

    /**
     * @return float
     */
    public function getNaturalAccentPrice()
    {
        return $this->naturalAccentPrice;
    }

    /**
     * @param float $naturalAccentPrice
     * @return $this
     */
    public function setNaturalAccentPrice($naturalAccentPrice)
    {
        $this->naturalAccentPrice = $naturalAccentPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getCutekPrice()
    {
        return $this->cutekPrice;
    }

    /**
     * @param float $cutekPrice
     * @return $this
     */
    public function setCutekPrice($cutekPrice)
    {
        $this->cutekPrice = $cutekPrice;
        return $this;
    }

    /**
     * @return float
     */
    public function getEnviroproPrice()
    {
        return $this->enviroproPrice;
    }

    /**
     * @param float $enviroproPrice
     * @return $this
     */
    public function setEnviroproPrice($enviroproPrice)
    {
        $this->enviroproPrice = $enviroproPrice;
        return $this;
    }

    /**
     * @param Product $object
     */
    public function import(self $object)
    {
        foreach (get_object_vars($object) as $key => $value) {
            $this->$key = $value;
        }
    }

    /**
     * Set wholesalePrice
     *
     * @param float $wholesalePrice
     *
     * @return Product
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;

        return $this;
    }

    /**
     * Get wholesalePrice
     *
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * Set retailPrice
     *
     * @param float $retailPrice
     *
     * @return Product
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = $retailPrice;

        return $this;
    }

    /**
     * Get retailPrice
     *
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * Set naturalAccentWholesalePrice
     *
     * @param float $naturalAccentWholesalePrice
     *
     * @return Product
     */
    public function setNaturalAccentWholesalePrice($naturalAccentWholesalePrice)
    {
        $this->naturalAccentWholesalePrice = $naturalAccentWholesalePrice;

        return $this;
    }

    /**
     * Get naturalAccentWholesalePrice
     *
     * @return float
     */
    public function getNaturalAccentWholesalePrice()
    {
        return $this->naturalAccentWholesalePrice;
    }

    /**
     * Set naturalAccentRetailPrice
     *
     * @param float $naturalAccentRetailPrice
     *
     * @return Product
     */
    public function setNaturalAccentRetailPrice($naturalAccentRetailPrice)
    {
        $this->naturalAccentRetailPrice = $naturalAccentRetailPrice;

        return $this;
    }

    /**
     * Get naturalAccentRetailPrice
     *
     * @return float
     */
    public function getNaturalAccentRetailPrice()
    {
        return $this->naturalAccentRetailPrice;
    }

    /**
     * Set cutekWholesalePrice
     *
     * @param float $cutekWholesalePrice
     *
     * @return Product
     */
    public function setCutekWholesalePrice($cutekWholesalePrice)
    {
        $this->cutekWholesalePrice = $cutekWholesalePrice;

        return $this;
    }

    /**
     * Get cutekWholesalePrice
     *
     * @return float
     */
    public function getCutekWholesalePrice()
    {
        return $this->cutekWholesalePrice;
    }

    /**
     * Set cutekRetailPrice
     *
     * @param float $cutekRetailPrice
     *
     * @return Product
     */
    public function setCutekRetailPrice($cutekRetailPrice)
    {
        $this->cutekRetailPrice = $cutekRetailPrice;

        return $this;
    }

    /**
     * Get cutekRetailPrice
     *
     * @return float
     */
    public function getCutekRetailPrice()
    {
        return $this->cutekRetailPrice;
    }

    /**
     * Set enviroproWholesalePrice
     *
     * @param float $enviroproWholesalePrice
     *
     * @return Product
     */
    public function setEnviroproWholesalePrice($enviroproWholesalePrice)
    {
        $this->enviroproWholesalePrice = $enviroproWholesalePrice;

        return $this;
    }

    /**
     * Get enviroproWholesalePrice
     *
     * @return float
     */
    public function getEnviroproWholesalePrice()
    {
        return $this->enviroproWholesalePrice;
    }

    /**
     * Set enviroproRetailPrice
     *
     * @param float $enviroproRetailPrice
     *
     * @return Product
     */
    public function setEnviroproRetailPrice($enviroproRetailPrice)
    {
        $this->enviroproRetailPrice = $enviroproRetailPrice;

        return $this;
    }

    /**
     * Get enviroproRetailPrice
     *
     * @return float
     */
    public function getEnviroproRetailPrice()
    {
        return $this->enviroproRetailPrice;
    }

    /**
     * Shape size
     *
     * @return string
     */
    public function getShapeSize()
    {
        $materialShapeSize = $this->getMaterial()->getMaterialShapeSize();
        return $materialShapeSize->getWidth() . 'x' . $materialShapeSize->getDepth();
    }

    /**
     * Shape Title
     *
     * @return string
     */
    public function getShapeTitle()
    {
        return $this->getMaterial()->getMaterialShapeSize()->getMaterialShape()->getTitle();
    }

    /**
     * @param integer $level
     * @return MaterialShape
     */
    public function getParentShape($level = null)
    {
        $shape = $this->getMaterial()->getMaterialShapeSize()->getMaterialShape();

        return $level !== null ? $shape->getParentByLevel($level) : $shape->getParent();
    }

    /**
     * Material Type Title
     *
     * @return string
     */
    public function getMaterialTypeTitle()
    {
        return $this->getMaterial()->getMaterialType()->getTitle();
    }

    /**
     * Finish Title
     *
     * @return string
     */
    public function getFinishTitle()
    {
        $finishTitle = null;

        $materialFinish = $this->getMaterialFinish();

        if (null !== $materialFinish) {
            $finishTitle = $materialFinish->getTitle();
        }

        return $finishTitle;
    }
}
