<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 31.05.17
 * Time: 12:38
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * ExpressionCladdingProduct
 *
 * @ORM\Table(name="expression_claddings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ExpressionCladdingProductRepository")
 */
class ExpressionCladdingProduct
{
    /**
     * ID
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Expression Cladding Product UUID
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $uuid;

    /**
     * @var Project
     *
     * @JMS\Type("string")
     * @JMS\Accessor(getter="getProjectId")
     * @JMS\SerializedName("project_id")
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="expressionCladdingProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * Title
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * Base Product
     *
     * @var Product
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getBaseId")
     *
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="base_product_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $base;

    /**
     * Stacker Product
     *
     * @var Product
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getStackerId")
     *
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="stacker_product_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $stacker;

    /**
     * Base MaterialFinish
     *
     * @var MaterialFinish
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getBaseFinishId")
     *
     * @ORM\ManyToOne(targetEntity="MaterialFinish", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="base_finish_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $baseFinish;

    /**
     * Stacker MaterialFinish
     *
     * @var MaterialFinish
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getStackerFinishId")
     *
     * @ORM\ManyToOne(targetEntity="MaterialFinish", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="stacker_finish_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $stackerFinish;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return $this
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return $this
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set base
     *
     * @param Product $base
     *
     * @return $this
     */
    public function setBase(Product $base = null)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return Product
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Set stacker
     *
     * @param Product $stacker
     *
     * @return $this
     */
    public function setStacker(Product $stacker = null)
    {
        $this->stacker = $stacker;

        return $this;
    }

    /**
     * Get stacker
     *
     * @return Product
     */
    public function getStacker()
    {
        return $this->stacker;
    }

    /**
     * Set baseFinish
     *
     * @param MaterialFinish $baseFinish
     *
     * @return $this
     */
    public function setBaseFinish(MaterialFinish $baseFinish = null)
    {
        $this->baseFinish = $baseFinish;

        return $this;
    }

    /**
     * Get baseFinish
     *
     * @return MaterialFinish
     */
    public function getBaseFinish()
    {
        return $this->baseFinish;
    }

    /**
     * Set stackerFinish
     *
     * @param MaterialFinish $stackerFinish
     *
     * @return $this
     */
    public function setStackerFinish(MaterialFinish $stackerFinish = null)
    {
        $this->stackerFinish = $stackerFinish;

        return $this;
    }

    /**
     * Get stackerFinish
     *
     * @return MaterialFinish
     */
    public function getStackerFinish()
    {
        return $this->stackerFinish;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return $this
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function getBaseId()
    {
        return $this->base ? $this->base->getId() : null;
    }

    public function getBaseFinishId()
    {
        return $this->baseFinish ? $this->baseFinish->getId() : null;
    }

    public function getStackerId()
    {
        return $this->stacker ? $this->stacker->getId() : null;
    }

    public function getStackerFinishId()
    {
        return $this->stackerFinish ? $this->stackerFinish->getId() : null;
    }

    public function getProjectId()
    {
        return $this->getProject()->getId();
    }
}
