<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:38
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Notification\QuotationNotification;
use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\QuotationRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 *
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Quotation
{
    use TimestampableEntity;

    /**
     * Quotation id
     *
     * @var int
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @var Project
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="quotations")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Notification\QuotationNotification",
     *     mappedBy="quotation", fetch="EXTRA_LAZY"
     * )
     */
    protected $notifications;

    /**
     * @var User
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToOne(targetEntity="User")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     */
    protected $user;


    /**
     * Callback date
     *
     * @var \DateTime
     *
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $backCallDate;

    /**
     * @var UploadedFile
     *
     * @JMS\Exclude
     *
     * @Assert\File(
     *     maxSize = "5M",
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     * )
     */
    protected $attachmentTemp;

    /**
     * @var UploadedFile
     *
     * @JMS\Exclude
     */
    protected $attachmentFile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     *
     * @JMS\Exclude
     */
    protected $attachmentPath;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $attachmentUrl;

    /**
     * Project area
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $projectArea;

    /**
     * Company name
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $companyName;

    /**
     * User first name
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * User last name
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * Position
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $position;

    /**
     * Email
     *
     * @Assert\Email(
     *     strict  = true,
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * Phone
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * State
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $state;

    /**
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $includedInQuantities = false;

    /**
     * Quotation content
     *
     * @var string
     *
     * @ORM\Column(type="string", length=2000, nullable=true)
     */
    protected $content;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Quotation Project Owner Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return null|int
     */
    public function getUserId()
    {
        return $this->project->getOwnerId();
    }

    /**
     * Quotation Project Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    /**
     * Attachment Web path
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("attachment")
     * @JMS\Type("string")
     *
     * @return null|string
     */
    public function getAttachmentWebPath()
    {
        return null === $this->attachmentPath
            ? null
            : $this->getAttachmentUploadDir().'/'.$this->attachmentPath;
    }

    /********************************************************************/

    /**
     * Quotation constructor.
     */
    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set projectArea
     *
     * @param string $projectArea
     *
     * @return Quotation
     */
    public function setProjectArea($projectArea)
    {
        $this->projectArea = $projectArea;

        return $this;
    }

    /**
     * Get projectArea
     *
     * @return string
     */
    public function getProjectArea()
    {
        return $this->projectArea;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return Quotation
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return Quotation
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Quotation
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @return null|string
     */
    public function getAttachmentAbsolutePath()
    {
        return $this->attachmentPath ? $this->getAttachmentUploadRootDir().'/'.$this->attachmentPath : null;
    }

    /**
     * @return string
     */
    protected function getAttachmentUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getAttachmentUploadDir();
    }

    /**
     * @return string
     */
    protected function getAttachmentUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/quotation';
    }

    /**
     * Sets attachment file.
     *
     * @param UploadedFile $file
     */
    public function setAttachmentFile(UploadedFile $file = null)
    {
        $this->attachmentFile = $file;

        // check if we have an old image path
        if (is_file($this->getAttachmentAbsolutePath())) {
            // store the old name to delete after the update
            $this->attachmentTemp = $this->getAttachmentAbsolutePath();
        }

        $this->preUploadAttachment();
    }

    /**
     * Get attachment file.
     *
     * @return UploadedFile
     */
    public function getAttachmentFile()
    {
        return $this->attachmentFile;
    }

    /**
     * @return void
     */
    public function preUploadAttachment()
    {
        if (null !== $this->getAttachmentFile()) {
            $this->attachmentPath = FileUtil::generateFilename($this->getAttachmentFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadAttachment()
    {
        if (null === $this->getAttachmentFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->attachmentTemp)) {
            // delete the old image
            @unlink($this->attachmentTemp);
            // clear the temp image path
            $this->attachmentTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getAttachmentFile()->move(
            $this->getAttachmentUploadRootDir(),
            $this->attachmentPath
        );

        $this->setAttachmentFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeAttachmentFilenameForRemove()
    {
        $this->attachmentTemp = $this->getAttachmentAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeAttachmentUpload()
    {
        if (isset($this->attachmentTemp)) {
            @unlink($this->attachmentTemp);
        }
    }

    /**
     * Set attachmentPath
     *
     * @param string $attachmentPath
     *
     * @return Quotation
     */
    public function setAttachmentPath($attachmentPath)
    {
        $this->attachmentPath = $attachmentPath;

        return $this;
    }

    /**
     * Get attachmentPath
     *
     * @return string
     */
    public function getAttachmentPath()
    {
        return $this->attachmentPath;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Quotation
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set backCallDate
     *
     * @param \DateTime $backCallDate
     *
     * @return Quotation
     */
    public function setBackCallDate($backCallDate)
    {
        $this->backCallDate = $backCallDate;

        return $this;
    }

    /**
     * Get backCallDate
     *
     * @return \DateTime
     */
    public function getBackCallDate()
    {
        return $this->backCallDate;
    }

    /**
     * Add notification
     *
     * @param QuotationNotification $notification
     *
     * @return Quotation
     */
    public function addNotification(QuotationNotification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param QuotationNotification $notification
     */
    public function removeNotification(QuotationNotification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Set attachmentUrl
     *
     * @param string $attachmentUrl
     *
     * @return Quotation
     */
    public function setAttachmentUrl($attachmentUrl)
    {
        $this->attachmentUrl = $attachmentUrl;

        return $this;
    }

    /**
     * Get attachmentUrl
     *
     * @return string
     */
    public function getAttachmentUrl()
    {
        return $this->attachmentUrl;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return Quotation
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return Quotation
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return Quotation
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return Quotation
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return Quotation
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * Set includedInQuantities
     *
     * @param boolean $includedInQuantities
     *
     * @return Quotation
     */
    public function setIncludedInQuantities($includedInQuantities)
    {
        $this->includedInQuantities = $includedInQuantities;

        return $this;
    }

    /**
     * Get includedInQuantities
     *
     * @return boolean
     */
    public function getIncludedInQuantities()
    {
        return $this->includedInQuantities;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Quotation
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return Quotation
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param $name
     *
     * @return $this
     */
    public function setName($name)
    {
        $names = explode(' ', $name);
        if (count($names) > 1) {
            $this->firstName = $names[0];
            $this->lastName  = $names[1];
        } elseif (count($names) > 0) {
            $this->firstName = $names[0];
        }

        return $this;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getName()
    {
        return sprintf('%s %s', $this->firstName, $this->lastName);
    }
}
