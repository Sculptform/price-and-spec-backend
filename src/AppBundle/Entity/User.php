<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 11:15
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Comment\AbstractComment;
use AppBundle\Entity\FollowedContent\FollowedContent;
use AppBundle\Entity\Notification\AbstractNotification;
use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use FOS\UserBundle\Model\User as BaseUser;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserRepository")
 * @ORM\Table(uniqueConstraints={@ORM\UniqueConstraint(columns={"email"})})
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("none")
 * @UniqueEntity("email", groups={"settings"}, message="The email address is already in use. Please try another email address.")
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class User extends BaseUser
{
    use TimestampableEntity;

    const ROLE_ADMIN = 'ROLE_ADMIN';

    const ROLE_MANAGER = 'ROLE_MANAGER';

    const ROLE_TESTER = 'ROLE_TESTER';

    const ROLE_SUBSCRIBER_INDIVIDUAL = 'ROLE_SUBSCRIBER_INDIVIDUAL';

    const ROLE_SUBSCRIBER_ENTERPRISE = 'ROLE_SUBSCRIBER_ENTERPRISE';

    /**
     * User id
     *
     * @var integer
     * @JMS\Groups({"Profile","List"})
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Woodform User id
     *
     * @var integer
     * @JMS\Groups("Profile")
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $uid;

    /**
     * Woodform User uuid
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", nullable=true)
     */
    protected $uuid;

    /**
     * Woodform User Profile id
     *
     * @var integer
     * @JMS\Groups("Profile")
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $pid;

    /**
     * Woodform User Profile Picture (Avatar) UUID
     *
     * @var string
     * @JMS\Groups({"Profile","List"})
     * @ORM\Column(type="string", nullable=true)
     */
    protected $pictureUuid;

    /**
     * @var string
     * @JMS\Groups({"Profile","List"})
     */
    protected $username;

    /**
     * @var string
     * @JMS\Groups({"Profile","List"})
     */
    protected $email;

    /**
     * @var boolean
     * @JMS\Groups("Profile")
     */
    protected $enabled;

    /**
     * @var boolean
     * @JMS\Groups("Profile")
     */
    protected $locked;

    /**
     * @var boolean
     * @JMS\Groups("Profile")
     */
    protected $expired;

    /**
     * @var boolean
     * @JMS\Groups("Profile")
     */
    protected $credentialsExpired;

    /**
     * @var \DateTime
     * @JMS\Groups("Profile")
     */
    protected $lastLogin;

    /**
     * Last login to sculptform
     *
     * @var \DateTime
     * @JMS\Groups("Profile")
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $lastLoginSFM;

    /**
     * @var array
     * @JMS\Accessor(getter="getRoles")
     * @JMS\Groups("Profile")
     */
    protected $roles;

    /**
     * User first name
     *
     * @var string
     * @JMS\Groups({"Profile","List"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * User last name
     *
     * @var string
     * @JMS\Groups({"Profile","List"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * Position
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $position;

    /**
     * User phone number
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $phone;

    /**
     * User state
     *
     * @var string
     *
     * @JMS\Groups("Profile")
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $state;

    /**
     * Company name
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $companyName;

    /**
     * Company phone number
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $companyPhone;

    /**
     * Company address
     *
     * @var string
     * @JMS\Groups("Profile")
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $companyAddress;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToMany(targetEntity="Feed", mappedBy="likedUsers", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $likedFeeds;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Comment\AbstractComment", mappedBy="likedUsers",
     *                                                                          cascade={"persist"},
     *                                                                          fetch="EXTRA_LAZY")
     */
    protected $likedComments;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment\AbstractComment", mappedBy="spammedUser",
     *                                                                         cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $spammedComments;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\GalleryProject", mappedBy="user", cascade={"persist"},
     *                                                                fetch="EXTRA_LAZY")
     */
    protected $galleryProjects;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserProject", mappedBy="user", cascade={"persist"},
     *                                                             fetch="EXTRA_LAZY")
     */
    protected $userProjects;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="PollAnswer", inversedBy="users", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $pollAnswers;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserFollower", mappedBy="user", fetch="EXTRA_LAZY")
     */
    protected $followers;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\UserFollower", mappedBy="follower", fetch="EXTRA_LAZY")
     */
    protected $followings;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\FollowedContent\FollowedContent", mappedBy="users",
     *                                                                                  fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="users_followed_content")
     */
    protected $followedContent;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $coverImageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $coverImageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="cover_image_path")
     * @JMS\Exclude
     */
    protected $coverImagePath;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $email_quotation_notifications = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $email_share_notifications = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $email_news_notifications = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $sms_quotation_notifications = true;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $sms_share_notifications = false;

    /**
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups("notifications")
     */
    protected $sms_news_notifications = false;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="about")'
     * @JMS\Groups("Profile")
     */
    protected $about;

    /**
     * @var Collection
     * @JMS\Exclude
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Notification\AbstractNotification", mappedBy="user",
     *                                                                                   fetch="EXTRA_LAZY")
     */
    protected $notifications;

    /**
     * @ORM\ManyToMany(targetEntity="Award", inversedBy="users")
     * @ORM\JoinTable(name="users_awards")
     * @JMS\Groups("Profile")
     */
    protected $awards;

    /**
     * @var bool
     * @ORM\Column(type="boolean", nullable=false)
     * @JMS\Groups({"Profile","List"})
     */
    protected $tutorialChecked = false;

    /**
     * User constructor.
     */
    public function __construct()
    {
        parent::__construct();

        $this->likedFeeds      = new ArrayCollection();
        $this->likedComments   = new ArrayCollection();
        $this->spammedComments = new ArrayCollection();
        $this->galleryProjects = new ArrayCollection();
        $this->pollAnswers     = new ArrayCollection();
        $this->followers       = new ArrayCollection();
        $this->followings      = new ArrayCollection();
        $this->notifications   = new ArrayCollection();
        $this->userProjects    = new ArrayCollection();
        $this->awards          = new ArrayCollection();
    }

    /**
     * Set firstName
     *
     * @param string $firstName
     *
     * @return User
     */
    public function setFirstName($firstName)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName
     *
     * @return string
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName
     *
     * @param string $lastName
     *
     * @return User
     */
    public function setLastName($lastName)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName
     *
     * @return string
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * @return array
     */
    public function serializeForPardotService()
    {
        return [
            'email'      => $this->getEmail(),
            'first_name' => $this->getFirstName() ?? 'n/a',
            'last_name'  => $this->getLastName() ?? 'n/a',
            'phone'      => $this->getPhone() ?? 'n/a',
            'company'    => $this->getCompanyName() ?? 'n/a',
        ];
    }

    /**
     * Set position
     *
     * @param string $position
     *
     * @return User
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return string
     */
    public function getPosition()
    {
        return $this->position;
    }

    /**
     * Set phone
     *
     * @param string $phone
     *
     * @return User
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;

        return $this;
    }

    /**
     * Get phone
     *
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * Set companyName
     *
     * @param string $companyName
     *
     * @return User
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set companyPhone
     *
     * @param string $companyPhone
     *
     * @return User
     */
    public function setCompanyPhone($companyPhone)
    {
        $this->companyPhone = $companyPhone;

        return $this;
    }

    /**
     * Get companyPhone
     *
     * @return string
     */
    public function getCompanyPhone()
    {
        return $this->companyPhone;
    }

    /**
     * Set companyAddress
     *
     * @param string $companyAddress
     *
     * @return User
     */
    public function setCompanyAddress($companyAddress)
    {
        $this->companyAddress = $companyAddress;

        return $this;
    }

    /**
     * Get companyAddress
     *
     * @return string
     */
    public function getCompanyAddress()
    {
        return $this->companyAddress;
    }

    /**
     * @return string
     */
    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    /**
     * * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Profile","List"})
     *
     * @param bool|integer $resized
     *
     * @return null|string
     */
    public function getImageWebPath($resized = false)
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $resized ? $this->getResizedDir($resized).'/'.$this->imagePath
                : $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    public function getFullName()
    {
        return trim($this->getFirstName().' '.$this->getLastName());
    }

    public function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    public function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/user/avatars';
    }

    public function getResizedDir($size)
    {
        return sprintf('%s/%sx%s', $this->getImageUploadDir(), $size, $size);
    }

    public function getCoverImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getCoverImageUploadDir();
    }

    public function getCoverImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/user/covers';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     *
     * @return $this
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile   = $file;
        $this->pictureUuid = null;

        // check if we have an old image path
        if ($this->imagePath) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        if (null !== $file) {
            $this->imagePath = FileUtil::generateFilename($file);
        }

        return $this;
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        $this->removeImageUpload();

        if (null === $this->getImageFile()) {
            return;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->imageFile = null;
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp)) {
            if (is_file($this->imageTemp)) {
                $fs                = new Filesystem();
                $imageTempFileInfo = new \SplFileInfo($this->imageTemp);
                $files             = Finder::create()->files()
                    ->in($imageTempFileInfo->getPath())
                    ->name($imageTempFileInfo->getFilename());
                $fs->remove($files);
            }

            $this->imageTemp = null;
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Meta field of user
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @JMS\Groups("Profile")
     * @return array
     */
    public function getMeta()
    {
        $userRole         = 'guest';
        $subscriptionPlan = 'free';

        if ($this->hasRole('ROLE_ADMIN')) {
            $userRole = 'admin';
        } elseif ($this->hasRole('ROLE_MANAGER')) {
            $userRole = 'moderator';
        } elseif ($this->hasRole('ROLE_USER')) {
            $userRole = 'user';
        }

        if ($this->hasRole('ROLE_SUBSCRIBER_INDIVIDUAL')) {
            $subscriptionPlan = 'individual';
        } elseif ($this->hasRole('ROLE_SUBSCRIBER_INDIVIDUAL')) {
            $subscriptionPlan = 'enterprise';
        }

        return [
            'role' => $userRole,
            'plan' => $subscriptionPlan,
        ];
    }

    /**
     * Set emailQuotationNotifications
     *
     * @param boolean $emailQuotationNotifications
     *
     * @return User
     */
    public function setEmailQuotationNotifications($emailQuotationNotifications)
    {
        $this->email_quotation_notifications = $emailQuotationNotifications;

        return $this;
    }

    /**
     * Get emailQuotationNotifications
     *
     * @return boolean
     */
    public function getEmailQuotationNotifications()
    {
        return $this->email_quotation_notifications;
    }

    /**
     * Set emailShareNotifications
     *
     * @param boolean $emailShareNotifications
     *
     * @return User
     */
    public function setEmailShareNotifications($emailShareNotifications)
    {
        $this->email_share_notifications = $emailShareNotifications;

        return $this;
    }

    /**
     * Get emailShareNotifications
     *
     * @return boolean
     */
    public function getEmailShareNotifications()
    {
        return $this->email_share_notifications;
    }

    /**
     * Set emailNewsNotifications
     *
     * @param boolean $emailNewsNotifications
     *
     * @return User
     */
    public function setEmailNewsNotifications($emailNewsNotifications)
    {
        $this->email_news_notifications = $emailNewsNotifications;

        return $this;
    }

    /**
     * Get emailNewsNotifications
     *
     * @return boolean
     */
    public function getEmailNewsNotifications()
    {
        return $this->email_news_notifications;
    }

    /**
     * Set smsQuotationNotifications
     *
     * @param boolean $smsQuotationNotifications
     *
     * @return User
     */
    public function setSmsQuotationNotifications($smsQuotationNotifications)
    {
        $this->sms_quotation_notifications = $smsQuotationNotifications;

        return $this;
    }

    /**
     * Get smsQuotationNotifications
     *
     * @return boolean
     */
    public function getSmsQuotationNotifications()
    {
        return $this->sms_quotation_notifications;
    }

    /**
     * Set smsShareNotifications
     *
     * @param boolean $smsShareNotifications
     *
     * @return User
     */
    public function setSmsShareNotifications($smsShareNotifications)
    {
        $this->sms_share_notifications = $smsShareNotifications;

        return $this;
    }

    /**
     * Get smsShareNotifications
     *
     * @return boolean
     */
    public function getSmsShareNotifications()
    {
        return $this->sms_share_notifications;
    }

    /**
     * Set smsNewsNotifications
     *
     * @param boolean $smsNewsNotifications
     *
     * @return User
     */
    public function setSmsNewsNotifications($smsNewsNotifications)
    {
        $this->sms_news_notifications = $smsNewsNotifications;

        return $this;
    }

    /**
     * Get smsNewsNotifications
     *
     * @return boolean
     */
    public function getSmsNewsNotifications()
    {
        return $this->sms_news_notifications;
    }

    /**
     * Set uid
     *
     * @param integer $uid
     *
     * @return User
     */
    public function setUid($uid)
    {
        $this->uid = $uid;

        return $this;
    }

    /**
     * Get uid
     *
     * @return integer
     */
    public function getUid()
    {
        return $this->uid;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return User
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set pid
     *
     * @param integer $pid
     *
     * @return User
     */
    public function setPid($pid)
    {
        $this->pid = $pid;

        return $this;
    }

    /**
     * Get pid
     *
     * @return integer
     */
    public function getPid()
    {
        return $this->pid;
    }

    /**
     * Add follower
     *
     * @param UserFollower $follower
     *
     * @return User
     */
    public function addFollower(UserFollower $follower)
    {
        $this->followers[] = $follower;

        return $this;
    }

    /**
     * Remove follower
     *
     * @param UserFollower $follower
     */
    public function removeFollower(UserFollower $follower)
    {
        $this->followers->removeElement($follower);
    }

    /**
     * Get followers
     *
     * @return Collection
     */
    public function getFollowers()
    {
        return $this->followers;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * get User Followers
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"List", "Profile"})
     * @JMS\Type("array<integer>")
     * @return array
     */
    public function getFollowerIds()
    {
        $followerIds = [];

        /** @var UserFollower $userFollower */
        foreach ($this->getFollowers() as $userFollower) {
            $followerIds[] = $userFollower->getFollower()->getId();
        }

        return $followerIds;
    }

    /**
     * get User Awards
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"List", "Profile"})
     * @JMS\Type("array<integer>")
     * @return array
     */
    public function getAwardIds()
    {
        $awardIds = [];

        /** @var Award $award */
        foreach ($this->getAwards() as $award) {
            $awardIds[] = $award->getId();
        }

        return $awardIds;
    }

    /**
     * get User Followings
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"List", "Profile"})
     * @JMS\Type("array<integer>")
     * @return array
     */
    public function getFollowingIds()
    {
        $followingIds = [];

        /** @var UserFollower $userFollower */
        foreach ($this->getFollowings() as $userFollower) {
            $followingIds[] = $userFollower->getUser()->getId();
        }

        return $followingIds;
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("boolean")
     * @JMS\Groups({"List", "Profile"})
     *
     * @return Bool Whether the user is active or not
     */
    public function isOnline()
    {
        // Delay during wich the user will be considered as still active
        $delay = new \DateTime('2 minutes ago');

        return ($this->getLastLogin() > $delay);
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"List", "Profile"})
     *
     * @return integer
     */
    public function getNotificationsCount()
    {
        return $this->notifications->filter(
            function (AbstractNotification $notification) {
                return null === $notification->getReadAt();
            }
        )->count();
    }

    /***********************************************/

    /**
     * Add likedFeed
     *
     * @param Feed $likedFeed
     *
     * @return User
     */
    public function addLikedFeed(Feed $likedFeed)
    {
        $this->likedFeeds[] = $likedFeed;

        return $this;
    }

    /**
     * Remove likedFeed
     *
     * @param Feed $likedFeed
     */
    public function removeLikedFeed(Feed $likedFeed)
    {
        $this->likedFeeds->removeElement($likedFeed);
    }

    /**
     * Get likedFeeds
     *
     * @return Collection
     */
    public function getLikedFeeds()
    {
        return $this->likedFeeds;
    }

    /**
     * Add galleryProject
     *
     * @param GalleryProject $galleryProject
     *
     * @return User
     */
    public function addGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProjects[] = $galleryProject;

        return $this;
    }

    /**
     * Remove galleryProject
     *
     * @param GalleryProject $galleryProject
     */
    public function removeGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProjects->removeElement($galleryProject);
    }

    /**
     * Get galleryProjects
     *
     * @return Collection
     */
    public function getGalleryProjects()
    {
        return $this->galleryProjects;
    }

    /**
     * Add pollAnswer
     *
     * @param PollAnswer $pollAnswer
     *
     * @return User
     */
    public function addPollAnswer(PollAnswer $pollAnswer)
    {
        $this->pollAnswers[] = $pollAnswer;

        return $this;
    }

    /**
     * Remove pollAnswer
     *
     * @param PollAnswer $pollAnswer
     */
    public function removePollAnswer(PollAnswer $pollAnswer)
    {
        $this->pollAnswers->removeElement($pollAnswer);
    }

    /**
     * Get pollAnswers
     *
     * @return Collection
     */
    public function getPollAnswers()
    {
        return $this->pollAnswers;
    }

    /**
     * Add likedComment
     *
     * @param AbstractComment $likedComment
     *
     * @return User
     */
    public function addLikedComment(AbstractComment $likedComment)
    {
        $this->likedComments[] = $likedComment;

        return $this;
    }

    /**
     * Remove likedComment
     *
     * @param AbstractComment $likedComment
     */
    public function removeLikedComment(AbstractComment $likedComment)
    {
        $this->likedComments->removeElement($likedComment);
    }

    /**
     * Get likedComments
     *
     * @return Collection
     */
    public function getLikedComments()
    {
        return $this->likedComments;
    }

    /**
     * Set about
     *
     * @param string $about
     *
     * @return User
     */
    public function setAbout($about)
    {
        $this->about = $about;

        return $this;
    }

    /**
     * Get about
     *
     * @return string
     */
    public function getAbout()
    {
        return $this->about;
    }

    public function getCoverImageAbsolutePath()
    {
        return $this->coverImagePath ? $this->getCoverImageUploadRootDir().'/'.$this->coverImagePath : null;
    }

    /**
     * Cover Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Create", "Update", "Show", "List", "Profile"})
     * @JMS\Type("string")
     * @return null|string
     */
    public function getCoverImageWebPath()
    {
        if ($this->coverImagePath && is_file($this->getCoverImageAbsolutePath())) {
            return $this->getCoverImageUploadDir().'/'.$this->coverImagePath;
        }

        return null;
    }

    /**
     * Sets cover image file.
     *
     * @param UploadedFile $file
     */
    public function setCoverImageFile(UploadedFile $file = null)
    {
        $this->coverImageFile = $file;

        // check if we have an old image path
        if (is_file($this->getCoverImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->coverImageTemp = $this->getCoverImageAbsolutePath();
        }

        $this->preUploadCoverImage();
    }

    /**
     * Get cover image file.
     *
     * @return UploadedFile
     */
    public function getCoverImageFile()
    {
        return $this->coverImageFile;
    }

    public function preUploadCoverImage()
    {
        if (null !== $this->getCoverImageFile()) {
            $this->coverImagePath = FileUtil::generateFilename($this->getCoverImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadCoverImage()
    {
        $this->removeCoverImageUpload();

        if (null === $this->getCoverImageFile()) {
            return;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getCoverImageFile()->move(
            $this->getCoverImageUploadRootDir(),
            $this->coverImagePath
        );

        $this->coverImageFile = null;
    }

    /**
     * @ORM\PreRemove
     */
    public function storeCoverImageFilenameForRemove()
    {
        $this->coverImageTemp = $this->getCoverImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeCoverImageUpload()
    {
        if (isset($this->coverImageTemp)) {
            if (is_file($this->coverImageTemp)) {
                unlink($this->coverImageTemp);
            }

            $this->coverImageTemp = null;
        }
    }

    /**
     * Set coverImagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setCoverImagePath($imagePath)
    {
        $this->coverImagePath = $imagePath;

        return $this;
    }

    /**
     * Get coverImagePath
     *
     * @return string
     */
    public function getCoverImagePath()
    {
        return $this->coverImagePath;
    }

    /**
     * Add spammedComment
     *
     * @param AbstractComment $spammedComment
     *
     * @return User
     */
    public function addSpammedComment(AbstractComment $spammedComment)
    {
        $this->spammedComments[] = $spammedComment;

        return $this;
    }

    /**
     * Remove spammedComment
     *
     * @param AbstractComment $spammedComment
     */
    public function removeSpammedComment(AbstractComment $spammedComment)
    {
        $this->spammedComments->removeElement($spammedComment);
    }

    /**
     * Get spammedComments
     *
     * @return Collection
     */
    public function getSpammedComments()
    {
        return $this->spammedComments;
    }

    /**
     * @return Collection
     */
    public function getUserProjects()
    {
        return $this->userProjects;
    }

    /**
     * @param Collection $userProjects
     */
    public function setUserProjects($userProjects)
    {
        $this->userProjects = $userProjects;
    }

    /**
     * Add userProject
     *
     * @param UserProject $userProject
     *
     * @return User
     */
    public function addUserProject(UserProject $userProject)
    {
        $this->userProjects[] = $userProject;

        return $this;
    }

    /**
     * Remove userProject
     *
     * @param UserProject $userProject
     */
    public function removeUserProject(UserProject $userProject)
    {
        $this->userProjects->removeElement($userProject);
    }

    /**
     * Add notification
     *
     * @param AbstractNotification $notification
     *
     * @return User
     */
    public function addNotification(AbstractNotification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param AbstractNotification $notification
     */
    public function removeNotification(AbstractNotification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * Add followedContent
     *
     * @param FollowedContent $followedContent
     *
     * @return User
     */
    public function addFollowedContent(FollowedContent $followedContent)
    {
        $this->followedContent[] = $followedContent->addUser($this);

        return $this;
    }

    /**
     * Remove followedContent
     *
     * @param FollowedContent $followedContent
     */
    public function removeFollowedContent(FollowedContent $followedContent)
    {
        $this->followedContent->removeElement($followedContent);
    }

    /**
     * Get followedContent
     *
     * @return Collection
     */
    public function getFollowedContent()
    {
        return $this->followedContent;
    }

    /**
     * @return Collection
     */
    public function getFollowings()
    {
        return $this->followings;
    }

    /**
     * @param Collection $followings
     */
    public function setFollowings($followings)
    {
        $this->followings = $followings;
    }


    /**
     * Add following
     *
     * @param UserFollower $following
     *
     * @return User
     */
    public function addFollowing(UserFollower $following)
    {
        $this->followings[] = $following;

        return $this;
    }

    /**
     * Remove following
     *
     * @param UserFollower $following
     */
    public function removeFollowing(UserFollower $following)
    {
        $this->followings->removeElement($following);
    }

    /**
     * Set pictureUuid
     *
     * @param string $pictureUuid
     *
     * @return User
     */
    public function setPictureUuid($pictureUuid)
    {
        $this->pictureUuid = $pictureUuid;

        return $this;
    }

    /**
     * Get pictureUuid
     *
     * @return string
     */
    public function getPictureUuid()
    {
        return $this->pictureUuid;
    }

    /**
     * Add award
     *
     * @param Award $award
     *
     * @return User
     */
    public function addAward(Award $award)
    {
        $this->awards[] = $award->addUser($this);

        return $this;
    }

    /**
     * Remove award
     *
     * @param Award $award
     */
    public function removeAward(Award $award)
    {
        $this->awards->removeElement($award);
    }

    /**
     * Get awards
     *
     * @return Collection
     */
    public function getAwards()
    {
        return $this->awards;
    }

    /**
     * Set lastLoginSFM
     *
     * @param \DateTime $lastLoginSFM
     *
     * @return User
     */
    public function setLastLoginSFM($lastLoginSFM)
    {
        $this->lastLoginSFM = $lastLoginSFM;

        return $this;
    }

    /**
     * Get lastLoginSFM
     *
     * @return \DateTime
     */
    public function getLastLoginSFM()
    {
        return $this->lastLoginSFM;
    }

    /**
     * Set state
     *
     * @param string $state
     *
     * @return User
     */
    public function setState($state)
    {
        $this->state = $state;

        return $this;
    }

    /**
     * Get state
     *
     * @return string
     */
    public function getState()
    {
        return $this->state;
    }

    /**
     * @return bool
     */
    public function getTutorialChecked(): bool
    {
        return $this->tutorialChecked;
    }

    /**
     * @param bool|NULL $tutorialChecked
     */
    public function setTutorialChecked($tutorialChecked): void
    {
        $this->tutorialChecked = $tutorialChecked == null ? false : $tutorialChecked;
    }
}
