<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 16.02.17
 * Time: 12:24
 */

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Tree\Traits\NestedSetEntity;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Table(name="material_finish_groups")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialFinishGroupRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("ALL")
 */
class MaterialFinishGroup
{
    use NestedSetEntity;

    /**
     * Material finish group id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Expose
     */
    protected $id;

    /**
     * @var MaterialFinishGroup
     *
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="MaterialFinishGroup", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE")
     */
    protected $parent;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="MaterialFinishGroup", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     */
    protected $children;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="MaterialFinish", mappedBy="group")
     * @JMS\Expose
     */
    protected $materialFinishes;

    /**
     * @var MaterialType
     *
     * @ORM\ManyToOne(targetEntity="MaterialType", inversedBy="materialFinishGroups", cascade={"persist"})
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id", onDelete="SET NULL")
     */
    protected $materialType;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @JMS\Expose
     */
    protected $title;

    /**
     * Material finish group application types
     *
     * @var array
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\ApplicationType", "getChoices"}, multiple=true, message="Enter a valid application types.")
     * @ORM\Column(type="simple_array", nullable=true)
     * @see ApplicationType
     */
    protected $applicationTypes;

    /**
     * @var ProductType
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getProductTypeId")
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="ProductType", cascade={"persist"})
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id", nullable=true)
     */
    protected $productType;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->materialFinishes = new ArrayCollection();
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Parent Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getParentId()
    {
        return $this->getParent() ? $this->getParent()->getId() : null;
    }

    /**
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterialType()->getId();
    }

    /**
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @param string $glue
     * @return string
     */
    public function getPath($glue = '_')
    {
        return implode($glue, $this->getPathArray());
    }

    /***********************************************/

    /**
     * @return array
     */
    public function getPathArray()
    {
        $parentIds = [$this->getId()]; // full path (include current group id)

        $this->getAllParentIds($this, $parentIds);

        array_pop($parentIds);

        return array_reverse($parentIds);
    }

    /**
     * Product Type Id
     *
     * @return null|int
     */
    public function getProductTypeId()
    {
        return $this->productType ? $this->productType->getId() : NULL;
    }

    /**
     * @param MaterialFinishGroup $node
     * @param array $parentIds
     */
    protected function getAllParentIds(MaterialFinishGroup $node, array &$parentIds)
    {
        $parent = $node->getParent();

        if ($parent) {
            $parentIds[] = $parent->getId();

            $this->getAllParentIds($parent, $parentIds);
        }
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MaterialFinishGroup
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return MaterialFinishGroup
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return MaterialFinishGroup
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set left
     *
     * @param integer $left
     *
     * @return MaterialFinishGroup
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return integer
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set right
     *
     * @param integer $right
     *
     * @return MaterialFinishGroup
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return integer
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set parent
     *
     * @param MaterialFinishGroup $parent
     *
     * @return MaterialFinishGroup
     */
    public function setParent(MaterialFinishGroup $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return MaterialFinishGroup
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param MaterialFinishGroup $child
     *
     * @return MaterialFinishGroup
     */
    public function addChild(MaterialFinishGroup $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param MaterialFinishGroup $child
     */
    public function removeChild(MaterialFinishGroup $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection|MaterialFinishGroup[]
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add materialFinish
     *
     * @param MaterialFinish $materialFinish
     *
     * @return MaterialFinishGroup
     */
    public function addMaterialFinish(MaterialFinish $materialFinish)
    {
        $this->materialFinishes[] = $materialFinish;

        return $this;
    }

    /**
     * Remove materialFinish
     *
     * @param MaterialFinish $materialFinish
     */
    public function removeMaterialFinish(MaterialFinish $materialFinish)
    {
        $this->materialFinishes->removeElement($materialFinish);
    }

    /**
     * Get materialFinishes
     *
     * @return Collection|MaterialFinish[]
     */
    public function getMaterialFinishes()
    {
        return $this->materialFinishes;
    }

    /**
     * Set materialType
     *
     * @param MaterialType $materialType
     *
     * @return MaterialFinishGroup
     */
    public function setMaterialType(MaterialType $materialType = null)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * Set applicationTypes
     *
     * @param array $applicationTypes
     *
     * @return MaterialFinishGroup
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;

        return $this;
    }

    /**
     * Get applicationTypes
     *
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * Set productType
     *
     * @param ProductType $productType
     *
     * @return MaterialFinishGroup
     */
    public function setProductType(ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir() . '/' . $this->imagePath : NULL;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Show", "List", "Share"})
     * @JMS\Expose
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir() . '/' . $this->imagePath;
        }

        return NULL;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/materialFinishGroups';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     * @return MaterialFinishGroup
     */
    public function setImageFile(UploadedFile $file = NULL)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();

        return $this;
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (NULL !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (NULL === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = NULL;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(NULL);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }
}
