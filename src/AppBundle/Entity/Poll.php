<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Poll
 *
 * @ORM\Table(name="poll")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\PollRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class Poll
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Question
     *
     * @var string
     * @JMS\Groups({"Show", "List"})*
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $question;

    /**
     * Start date
     *
     * @var \DateTime
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $startDate;
    
    /**
     * @var Collection
     * @JMS\Groups({"Show", "List"})
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\PollAnswer", mappedBy="poll",  cascade={"all"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    protected $answers;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     *
     * @return Poll
     */
    public function setQuestion($question)
    {
        $this->question = $question;

        return $this;
    }

    /**
     * Get question
     *
     * @return string
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->answers = new ArrayCollection();
    }

    /**
     * Add answer
     *
     * @param PollAnswer $answer
     *
     * @return Poll
     */
    public function addAnswer(PollAnswer $answer)
    {
        $this->answers[] = $answer->setPoll($this);

        return $this;
    }

    /**
     * Remove answer
     *
     * @param PollAnswer $answer
     */
    public function removeAnswer(PollAnswer $answer)
    {
        $this->answers->removeElement($answer);
    }

    /**
     * Get answers
     *
     * @return Collection
     */
    public function getAnswers()
    {
        return $this->answers;
    }

    /**
     * Set startDate
     *
     * @param \DateTime $startDate
     *
     * @return Poll
     */
    public function setStartDate($startDate)
    {
        $this->startDate = $startDate;

        return $this;
    }

    /**
     * Get startDate
     *
     * @return \DateTime
     */
    public function getStartDate()
    {
        return $this->startDate;
    }
}
