<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\User;
use AppBundle\Entity\UserFollower;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\UserFollowerNotificationRepository")
 * @ORM\Table(name="notification_user_follower")
 *
 */
class UserFollowerNotification extends AbstractNotification
{
    /**
     * @var UserFollower
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\UserFollower", inversedBy="notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_follower_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $userFollower;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * User Following Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("AppBundle\Entity\User")
     * @JMS\Groups({"Notification"})
     * @return User
     */
    public function getUserFollowing()
    {
        return $this->getUserFollower()->getFollower();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::USER_FOLLOWER;
    }

    /**
     * Set user follower
     *
     * @param UserFollower $userFollower
     *
     * @return UserFollowerNotification
     */
    public function setUserFollower(UserFollower $userFollower)
    {
        $this->userFollower = $userFollower;

        return $this;
    }

    /**
     * Get userFollower
     *
     * @return UserFollower
     */
    public function getUserFollower()
    {
        return $this->userFollower;
    }
}
