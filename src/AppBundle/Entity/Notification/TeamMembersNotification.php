<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\Team;
use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\TeamMembersNotificationRepository")
 * @ORM\Table(name="notification_team_members")
 *
 */
class TeamMembersNotification extends AbstractNotification
{
    /**
     * @var Team
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $team;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $eventUser;

    /**
     * @var integer
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=false, name="event_type")
     *
     */
    protected $event;

    public function getKind(){
        return NotificationKind::TEAM_MEMBERS;
    }

    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     */
    public function setTeam($team)
    {
        $this->team = $team;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param User $eventUser
     */
    public function setEventUser($eventUser)
    {
        $this->eventUser = $eventUser;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

}
