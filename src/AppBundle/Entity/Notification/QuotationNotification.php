<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\Quotation;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\QuotationNotificationRepository")
 * @ORM\Table(name="notification_quotation")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class QuotationNotification extends AbstractNotification
{
    /**
     * @var Quotation
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Quotation", inversedBy="notifications")
     * @ORM\JoinColumn(name="quotation_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $quotation;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Quotation Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getQuotationId()
    {
        return $this->getQuotation()->getId();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::QUOTATION;
    }

    /**
     * Set quotation
     *
     * @param Quotation $quotation
     *
     * @return QuotationNotification
     */
    public function setQuotation(Quotation $quotation)
    {
        $this->quotation = $quotation;

        return $this;
    }

    /**
     * Get quotation
     *
     * @return Quotation
     */
    public function getQuotation()
    {
        return $this->quotation;
    }
}
