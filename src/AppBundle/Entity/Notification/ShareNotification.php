<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use AppBundle\Entity\Project;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\ShareNotificationRepository")
 * @ORM\Table(name="notification_share")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ShareNotification extends AbstractNotification
{
    /**
     * @var Project
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="shareNotifications")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Project Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::SHARE;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return ShareNotification
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
