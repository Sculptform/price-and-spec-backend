<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\Team;
use AppBundle\Entity\TeamUser;
use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\TeamUserNotificationRepository")
 * @ORM\Table(name="notification_team_user")
 *
 */
class TeamUserNotification extends AbstractNotification
{
    /**
     * @var TeamUser
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\TeamUser", inversedBy="notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $teamUser;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $eventUser;

    /**
     * @var integer
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=false, name="event_type")
     *
     */
    protected $event;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * User Team
     *
     * @JMS\VirtualProperty
     * @JMS\Type("AppBundle\Entity\Team")
     * @return Team
     */
    public function getTeam()
    {
        return $this->getTeamUser()->getTeam();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::TEAM_USER;
    }

    /**
     * @return TeamUser
     */
    public function getTeamUser()
    {
        return $this->teamUser;
    }

    /**
     * @param TeamUser $teamUser
     */
    public function setTeamUser($teamUser)
    {
        $this->teamUser = $teamUser;
    }

    /**
     * @return int
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param int $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param User $eventUser
     */
    public function setEventUser($eventUser)
    {
        $this->eventUser = $eventUser;
    }
}
