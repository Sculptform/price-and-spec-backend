<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\News;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\NewsNotificationRepository")
 * @ORM\Table(name="notification_news")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class NewsNotification extends AbstractNotification
{
    /**
     * @var News
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\News")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="news_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $news;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * News Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getNewsId()
    {
        return $this->getNews()->getId();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::NEWS;
    }

    /**
     * Set news
     *
     * @param News $news
     *
     * @return NewsNotification
     */
    public function setNews(News $news)
    {
        $this->news = $news;

        return $this;
    }

    /**
     * Get news
     *
     * @return News
     */
    public function getNews()
    {
        return $this->news;
    }
}
