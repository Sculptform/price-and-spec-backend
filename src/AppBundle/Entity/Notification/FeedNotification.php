<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\Feed;
use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\FeedNotificationRepository")
 * @ORM\Table(name="notification_feed")
 *
 */
class FeedNotification extends AbstractNotification
{
    /**
     * @var Feed
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feed", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feed_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $feed;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $eventUser;

    /**
     * @var integer
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=false, name="event_type")
     *
     */
    protected $event;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getFeedId()
    {
        return $this->getFeed()->getId();
    }

    /***********************************************/

    public function getKind(){
        return NotificationKind::FEED;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param User $eventUser
     */
    public function setEventUser($eventUser)
    {
        $this->eventUser = $eventUser;
    }

    /**
     * @return int
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param int $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

}
