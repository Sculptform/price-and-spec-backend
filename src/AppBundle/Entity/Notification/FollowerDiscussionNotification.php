<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\Discussion;
use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\FollowerDiscussionNotificationRepository")
 * @ORM\Table(name="notification_follower_discussion")
 *
 */
class FollowerDiscussionNotification extends AbstractNotification
{
    /**
     * @var Discussion
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discussion", cascade={"all"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discussion_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $discussion;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $eventUser;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getDiscussionId()
    {
        return $this->getDiscussion()->getId();
    }

    /***********************************************/

    public function getKind()
    {
        return NotificationKind::FOLLOWER_DISCUSSION;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @param Discussion $discussion
     * @return $this
     */
    public function setDiscussion(Discussion $discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * @return User
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param User $eventUser
     * @return $this
     */
    public function setEventUser(User $eventUser)
    {
        $this->eventUser = $eventUser;

        return $this;
    }
}
