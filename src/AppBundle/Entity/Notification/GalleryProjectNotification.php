<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\GalleryProject;
use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\GalleryProjectNotificationRepository")
 * @ORM\Table(name="notification_gallery_project")
 *
 */
class GalleryProjectNotification extends AbstractNotification
{
    /**
     * @var GalleryProject
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GalleryProject")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery_project_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $galleryProject;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="event_user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $eventUser;

    /**
     * @var integer
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=false, name="event_type")
     *
     */
    protected $event;

    public function getKind(){
        return NotificationKind::GALLERY_PROJECT;
    }

    /**
     * @return GalleryProject
     */
    public function getGalleryProject()
    {
        return $this->galleryProject;
    }

    /**
     * @param GalleryProject $galleryProject
     */
    public function setGalleryProject($galleryProject)
    {
        $this->galleryProject = $galleryProject;
    }

    /**
     * @return mixed
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @param mixed $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
    }

    /**
     * @return mixed
     */
    public function getEventUser()
    {
        return $this->eventUser;
    }

    /**
     * @param mixed $eventUser
     */
    public function setEventUser($eventUser)
    {
        $this->eventUser = $eventUser;
    }

}
