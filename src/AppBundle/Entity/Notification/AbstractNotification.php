<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:06
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\User;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\AbstractNotificationRepository")
 * @ORM\Table(name="notification")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="kind", type="integer")
 * @ORM\DiscriminatorMap({
 *      NotificationKind::NEWS = "AppBundle\Entity\Notification\NewsNotification",
 *      NotificationKind::SHARE = "AppBundle\Entity\Notification\ShareNotification",
 *      NotificationKind::QUOTATION = "AppBundle\Entity\Notification\QuotationNotification",
 *      NotificationKind::USER_FOLLOWER = "AppBundle\Entity\Notification\UserFollowerNotification",
 *      NotificationKind::TEAM_USER = "AppBundle\Entity\Notification\TeamUserNotification",
 *      NotificationKind::FEED = "AppBundle\Entity\Notification\FeedNotification",
 *      NotificationKind::PROJECT = "AppBundle\Entity\Notification\ProjectNotification",
 *      NotificationKind::GALLERY_PROJECT = "AppBundle\Entity\Notification\GalleryProjectNotification",
 *      NotificationKind::TEAM_MEMBERS = "AppBundle\Entity\Notification\TeamMembersNotification",
 *      NotificationKind::DISCUSSION = "AppBundle\Entity\Notification\DiscussionNotification",
 *      NotificationKind::PRODUCT_SANDPIT = "AppBundle\Entity\Notification\ProductSandpitNotification",
 *      NotificationKind::FOLLOWER_DISCUSSION = "AppBundle\Entity\Notification\FollowerDiscussionNotification"
 *
 * })
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
abstract class AbstractNotification
{
    /**
     * User News id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="notifications")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * Date read
     *
     * @var \DateTime
     *
     * @Assert\Date
     * @ORM\Column(type="datetime", nullable=true)
     */
    protected $readAt;

    /**
     * Date of creation
     *
     * @var \DateTime
     * @JMS\Expose
     * @Assert\Date
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * User Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getUserId()
    {
        return $this->getUser()->getId();
    }

    /**
     * Group
     *
     * @return null|int
     */
    abstract public function getKind();

    /***********************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set readAt
     *
     * @param \DateTime $readAt
     *
     * @return AbstractNotification
     */
    public function setReadAt($readAt)
    {
        $this->readAt = $readAt;

        return $this;
    }

    /**
     * Get readAt
     *
     * @return \DateTime
     */
    public function getReadAt()
    {
        return $this->readAt;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return AbstractNotification
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return $this
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
}
