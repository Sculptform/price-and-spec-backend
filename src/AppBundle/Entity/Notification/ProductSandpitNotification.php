<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Notification;

use AppBundle\Entity\ProductSandpit;
use AppBundle\Enum\NotificationKind;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Notification\ProductSandpitNotificationRepository")
 * @ORM\Table(name="notification_product_sandpit")
 *
 */
class ProductSandpitNotification extends AbstractNotification
{
    /**
     * @var ProductSandpit
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductSandpit")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_sandpit_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $productSandpit;

    public function getKind(){
        return NotificationKind::PRODUCT_SANDPIT;
    }

    /**
     * @return ProductSandpit
     */
    public function getProductSandpit()
    {
        return $this->productSandpit;
    }

    /**
     * @param ProductSandpit $productSandpit
     */
    public function setProductSandpit($productSandpit)
    {
        $this->productSandpit = $productSandpit;
    }

}
