<?php

namespace AppBundle\Entity;

use AppBundle\Enum\DiscussionStatus;
use AppBundle\Entity\Comment\DiscussionComment;
use AppBundle\Enum\DiscussionType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Discussion
 *
 * @ORM\Table(name="discussion")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DiscussionRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class Discussion
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups("List")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @JMS\Groups({"List", "Share"})
     */
    protected $title;

    /**
     * Content
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     * @JMS\Groups({"List", "Share"})
     */
    protected $content;
    
    /**
     * Type
     *
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback = {"AppBundle\Enum\DiscussionType", "getChoices"}, message="Enter a valid Discussion Type.")
     *
     * @ORM\Column(type="integer", nullable=false)
     * @JMS\Groups("List")
     * @see DiscussionType
     */
    protected $type;

    /**
     * Status
     *
     * @var int
     *
     * @Assert\NotBlank()
     * @Assert\Choice(callback = {"AppBundle\Enum\DiscussionStatus", "getChoices"}, message="Enter a valid Discussion Status.")
     *
     * @ORM\Column(type="integer", nullable=false)
     * @JMS\Groups("List")
     * @see DiscussionStatus
     */
    protected $status = DiscussionStatus::TRASH;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     * @JMS\Groups("List")
     *
     */
    protected $user;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Tag", mappedBy="discussions", cascade={"all"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     */
    protected $tags;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment\DiscussionComment", mappedBy="discussion", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @JMS\Exclude
     */
    protected $comments;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\DiscussionView", mappedBy="discussion", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY")
     */
    protected $views;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type)
    {
        $this->type = $type;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser(User $user)
    {
        $this->user = $user;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Discussion
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Add comment
     *
     * @param DiscussionComment $comment
     *
     * @return Discussion
     */
    public function addComment(DiscussionComment $comment)
    {
        $this->comments[] = $comment->setDiscussion($this);

        return $this;
    }

    /**
     * Remove comment
     *
     * @param DiscussionComment $comment
     */
    public function removeComment(DiscussionComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Discussion User id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getUserId()
    {
        return $this->getUser()->getId();
    }

    /**
     * Discussion Comments count
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getCommentsCount()
    {
        return $this->getComments()->count();
    }

    /**
     * Views count
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getViewsCount()
    {
        return $this->getViews()->count();
    }

    /***********************************************/

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Discussion
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->tags     = new ArrayCollection();
        $this->comments = new ArrayCollection();
        $this->views    = new ArrayCollection();
    }

    /**
     * Add tag
     *
     * @param Tag $tag
     *
     * @return Discussion
     */
    public function addTag(Tag $tag)
    {
        $this->tags[] = $tag->addDiscussion($this);

        return $this;
    }

    /**
     * Remove tag
     *
     * @param Tag $tag
     */
    public function removeTag(Tag $tag)
    {
        $this->tags->removeElement($tag);
    }

    /**
     * Get tags
     *
     * @return Collection
     */
    public function getTags()
    {
        return $this->tags;
    }

    /**
     * Add view
     *
     * @param DiscussionView $view
     *
     * @return Discussion
     */
    public function addView(DiscussionView $view)
    {
        $this->views[] = $view;

        return $this;
    }

    /**
     * Remove view
     *
     * @param DiscussionView $view
     */
    public function removeView(DiscussionView $view)
    {
        $this->views->removeElement($view);
    }

    /**
     * Get views
     *
     * @return Collection
     */
    public function getViews()
    {
        return $this->views;
    }
}
