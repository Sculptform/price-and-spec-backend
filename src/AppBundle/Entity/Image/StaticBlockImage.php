<?php

namespace AppBundle\Entity\Image;

use AppBundle\Entity\StaticBlock;
use AppBundle\Enum\ImageType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="static_block_images")
 *
 */
class StaticBlockImage extends AbstractImage
{
    /**
     * @var StaticBlock
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\StaticBlock", inversedBy="staticBlockImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="static_block_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $staticBlock;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Team Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getStaticBlockId()
    {
        return $this->getStaticBlock()->getId();
    }

    /***********************************************/

    public function getType(){
        return ImageType::STATIC_BLOCK;
    }

    /**
     * @return StaticBlock
     */
    public function getStaticBlock()
    {
        return $this->staticBlock;
    }

    /**
     * @param StaticBlock $staticBlock
     */
    public function setStaticBlock($staticBlock)
    {
        $this->staticBlock = $staticBlock;
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/staticBlock';
    }
}
