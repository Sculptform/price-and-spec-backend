<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Image;

use AppBundle\Entity\Feed;
use AppBundle\Enum\ImageType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="feed_images")
 *
 */
class FeedImage extends AbstractImage
{
    /**
     * @var Feed
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feed", inversedBy="images")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feed_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $feed;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getFeedId()
    {
        return $this->getFeed()->getId();
    }

    /***********************************************/

    public function getType(){
        return ImageType::FEED;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     */
    public function setFeed($feed)
    {
        $this->feed = $feed;
    }
}
