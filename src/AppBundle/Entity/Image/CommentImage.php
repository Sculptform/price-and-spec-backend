<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:59
 */

namespace AppBundle\Entity\Image;

use AppBundle\Entity\Comment\AbstractComment;
use AppBundle\Enum\ImageType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="comment_images")
 *
 */
class CommentImage extends AbstractImage
{
    /**
     * @var AbstractComment
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Comment\AbstractComment",  inversedBy="images")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="comment_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $comment;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Comment Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getCommentId()
    {
        return $this->getComment()->getId();
    }

    /***********************************************/

    public function getType(){
        return ImageType::COMMENT;
    }

    /**
     * @return AbstractComment
     */
    public function getComment()
    {
        return $this->comment;
    }

    /**
     * @param AbstractComment $comment
     */
    public function setComment($comment)
    {
        $this->comment = $comment;
    }
}
