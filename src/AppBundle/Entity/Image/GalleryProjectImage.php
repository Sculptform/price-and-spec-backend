<?php

namespace AppBundle\Entity\Image;

use AppBundle\Entity\GalleryProject;
use AppBundle\Enum\ImageType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table(name="gallery_project_images")
 *
 */
class GalleryProjectImage extends AbstractImage
{
    /**
     * @var GalleryProject
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GalleryProject", inversedBy="galleryProjectImages")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery_project_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $galleryProject;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Team Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getGalleryProjectId()
    {
        return $this->getGalleryProject()->getId();
    }

    /***********************************************/

    public function getType(){
        return ImageType::GALLERY_PROJECT;
    }

    /**
     * @return GalleryProject
     */
    public function getGalleryProject()
    {
        return $this->galleryProject;
    }

    /**
     * @param GalleryProject $galleryProject
     */
    public function setGalleryProject($galleryProject)
    {
        $this->galleryProject = $galleryProject;
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/galleryProjects';
    }
}
