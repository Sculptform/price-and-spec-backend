<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 09.06.17
 * Time: 17:26
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * FintraxStacker
 *
 * @ORM\Table(name="fintrax_stackers")
 * @ORM\Entity
 */
class FintraxStacker
{
    /**
     * ID
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var FintraxProduct
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="FintraxProduct", inversedBy="fintraxStackers")
     * @ORM\JoinColumn(name="fintrax_product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $fintraxProduct;

    /**
     * @var Product
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Product")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $product;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set fintraxProduct
     *
     * @param FintraxProduct $fintraxProduct
     *
     * @return FintraxStacker
     */
    public function setFintraxProduct(FintraxProduct $fintraxProduct)
    {
        $this->fintraxProduct = $fintraxProduct;

        return $this;
    }

    /**
     * Get fintraxProduct
     *
     * @return FintraxProduct
     */
    public function getFintraxProduct()
    {
        return $this->fintraxProduct;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return FintraxStacker
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->getProduct()->getPrice();
    }

    /**
     * Shape size
     *
     * @return string
     */
    public function getShapeSize()
    {
        return $this->getProduct()->getShapeSize();
    }

    /**
     * Shape Title
     *
     * @return string
     */
    public function getShapeTitle()
    {
        return $this->getProduct()->getShapeTitle();
    }

    /**
     * Material Type Title
     *
     * @return string
     */
    public function getMaterialTypeTitle()
    {
        return $this->getProduct()->getMaterialTypeTitle();
    }

    /**
     * Finish Title
     *
     * @return string
     */
    public function getFinishTitle()
    {
        $finishTitle = null;

        $product = $this->getProduct();

        if (null !== $product) {
            $materialFinish = $product->getMaterialFinish();

            if (null !== $materialFinish) {
                $finishTitle = $materialFinish->getTitle();
            }
        }

        return $finishTitle;
    }
}
