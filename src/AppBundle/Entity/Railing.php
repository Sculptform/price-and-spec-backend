<?php

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Railing
 *
 * @ORM\Table(name="railings")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RailingRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 * @ORM\HasLifecycleCallbacks
 */
class Railing
{

    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var MaterialType
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToOne(targetEntity="MaterialType")
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id",
     *   nullable=true)
     */
    protected $materialType;

    /**
     * Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $price = 0;

    /**
     * Retail Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $retailPrice = 0;

    /**
     * Wholesale Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $wholesalePrice = 0;

    /**
     * Width
     *
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $width;

    /**
     * Height
     *
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $height;

    /**
     * Title
     *
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $title;

    /**
     * Description
     *
     * @var string
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * 3D Model Data
     *
     * @var array
     * @JMS\Type("array")
     * @ORM\Column(type="json_array", nullable=true)
     */
    protected $modelData;

    /**
     * @var ProductType
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getProductTypeId")
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="railings", cascade={"persist"})
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id", nullable=false)
     */
    protected $productType;

    /**
     * Material finish application types
     *
     * @var array
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\ApplicationType", "getChoices"}, multiple=true, message="Enter a valid application types.")
     * @ORM\Column(type="simple_array", nullable=false)
     * @see ApplicationType
     */
    protected $applicationTypes = [];

    /**
     * @var array
     * @ORM\Column(name="cavity", type="array", nullable=true)
     */
    protected $cavity = [];

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $dwgTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     mimeTypes = {
     *          "application/acad",
     *          "application/x-acad",
     *          "application/autocad_dwg",
     *          "image/x-dwg",
     *          "application/dwg",
     *          "application/x-dwg",
     *          "application/x-autocad",
     *          "image/vnd.dwg",
     *          "drawing/dwg"
     *     },
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the DWG files are allowed."
     * )
     */
    protected $dwgFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="dwg_path")
     * @JMS\Exclude
     */
    protected $dwgPath;

    /**
     * Position for ordering
     *
     * @var integer
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $position;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     *
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return integer
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterialType()->getId();
    }

    /**
     * DWG Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getDwgWebPath()
    {
        if ($this->dwgPath && is_file($this->getDwgAbsolutePath())) {
            return $this->getDwgUploadDir().'/'.$this->dwgPath;
        }

        return null;
    }

    /***********************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set price
     *
     * @param float $price
     *
     * @return Railing
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get price
     *
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * Set retailPrice
     *
     * @param float $retailPrice
     *
     * @return Railing
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = $retailPrice;

        return $this;
    }

    /**
     * Get retailPrice
     *
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * Set wholesalePrice
     *
     * @param float $wholesalePrice
     *
     * @return Railing
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;

        return $this;
    }

    /**
     * Get wholesalePrice
     *
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * Set width
     *
     * @param integer $width
     *
     * @return Railing
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return integer
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set height
     *
     * @param integer $height
     *
     * @return Railing
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return integer
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set modelData
     *
     * @param array $modelData
     *
     * @return Railing
     */
    public function setModelData($modelData)
    {
        $this->modelData = $modelData;

        return $this;
    }

    /**
     * Get modelData
     *
     * @return array
     */
    public function getModelData()
    {
        return $this->modelData;
    }

    /**
     * Set materialType
     *
     * @param MaterialType $materialType
     *
     * @return Railing
     */
    public function setMaterialType(MaterialType $materialType = NULL)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Railing
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/railings';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    public function getDwgAbsolutePath()
    {
        return $this->dwgPath ? $this->getDwgUploadRootDir().'/'.$this->dwgPath : null;
    }

    protected function getDwgUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getDwgUploadDir();
    }

    protected function getDwgUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/dwg';
    }

    /**
     * Sets DWG file.
     *
     * @param UploadedFile $file
     */
    public function setDwgFile(UploadedFile $file = null)
    {
        $this->dwgFile = $file;

        // check if we have an old image path
        if (is_file($this->getDwgAbsolutePath())) {
            // store the old name to delete after the update
            $this->dwgTemp = $this->getDwgAbsolutePath();
        }

        $this->preUploadDwg();
    }

    /**
     * Get DWG file.
     *
     * @return UploadedFile
     */
    public function getDwgFile()
    {
        return $this->dwgFile;
    }

    public function preUploadDwg()
    {
        if (null !== $this->getDwgFile()) {
            $this->dwgPath = FileUtil::generateFilename($this->getDwgFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadDwg()
    {
        if (null === $this->getDwgFile()) {
            return;
        }

        if (isset($this->dwgTemp)) {
            // delete the old image
            unlink($this->dwgTemp);
            // clear the temp image path
            $this->dwgTemp = null;
        }

        $this->getDwgFile()->move(
            $this->getDwgUploadRootDir(),
            $this->dwgPath
        );

        $this->setDwgFile(null);
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }
    /***********************************************
    ***********************************************/


    /**
     * Set description
     *
     * @param string $description
     *
     * @return Railing
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return Railing
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * @ORM\PreRemove
     */
    public function storeDwgFilenameForRemove()
    {
        $this->dwgTemp = $this->getDwgAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeDwgUpload()
    {
        if (isset($this->dwgTemp)) {
            @unlink($this->dwgTemp);
        }
    }

    /**
     * Set DWG path
     *
     * @param string $dwgPath
     * @return $this
     */
    public function setDwgPath($dwgPath)
    {
        $this->dwgPath = $dwgPath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Set productType
     *
     * @param ProductType $productType
     *
     * @return Railing
     */
    public function setProductType(ProductType $productType)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Get DWG path
     *
     * @return string
     */
    public function getDwgPath()
    {
        return $this->dwgPath;
    }

    /**
     * Product Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return integer
     */
    public function getProductTypeId()
    {
        return $this->getProductType()->getId();
    }

    /**
     * Set cavity
     *
     * @param array $cavity
     *
     * @return Railing
     */
    public function setCavity($cavity)
    {
        $this->cavity = $cavity;

        return $this;
    }

    /**
     * Get cavity
     *
     * @return array
     */
    public function getCavity()
    {
        return $this->cavity;
    }

    /**
     * Set applicationTypes
     *
     * @param array $applicationTypes
     *
     * @return Railing
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;

        return $this;
    }

    /**
     * Get applicationTypes
     *
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * Set position
     *
     * @param integer $position
     *
     * @return Railing
     */
    public function setPosition($position)
    {
        $this->position = $position;

        return $this;
    }

    /**
     * Get position
     *
     * @return integer|null
     */
    public function getPosition()
    {
        return $this->position;
    }
}
