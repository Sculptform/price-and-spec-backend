<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:53
 */

namespace AppBundle\Entity;

use Doctrine\Common\Util\Inflector;
use Symfony\Component\PropertyAccess\PropertyAccessor;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectProductRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ProjectProduct
{
    /**
     * Project Product id
     * 
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Project Product UUID
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $uuid;

    /**
     * Height
     *
     * @var float
     * @ORM\Column(type="decimal", nullable=false)
     * @JMS\Type("string")
     */
    protected $height = 1;

    /**
     * @var Project
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="projectProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * @var Product
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Product", inversedBy="projectProducts", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="product_id", referencedColumnName="id", nullable=false)
     */
    protected $product;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
    * Project Id
    *
    * @JMS\VirtualProperty
    * @JMS\Type("string")
    * @return string
    */
    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    /**
     * Product Type
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getProductTypeTitle()
    {
        return $this->getProduct()->getMaterial()->getProductType()->getTitle();
    }

    /**
     * Material Type
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getMaterialTypeTitle()
    {
        return $this->getProduct()->getMaterial()->getMaterialType()->getTitle();
    }

    /**
     * Shape size
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getShapeSize()
    {
        return $this->getProduct()->getShapeSize();
    }

    /**
     * Shape Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getShapeTitle()
    {
        return $this->getProduct()->getShapeTitle();
    }

    /**
     * Finish Title
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getFinishTitle()
    {
        $finishTitle = null;

        $product = $this->getProduct();

        if (null !== $product) {
            $materialFinish = $product->getMaterialFinish();

            if (null !== $materialFinish) {
                $finishTitle = $materialFinish->getTitle();
            }
        }

        return $finishTitle;
    }

    /**
     * Product Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getProductId()
    {
        return $this->getProduct()->getId();
    }

    /**
     * Material Finish Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getFinishId()
    {
        return $this->getProduct()->getMaterialFinishId();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return float
     */
    public function getPrice()
    {
        return $this->getProduct()->getPrice() * $this->getHeight();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return float
     */
    public function getSculptformPrice()
    {
        $product = $this->getProduct();

        $price = $product->getPrice() * $this->getHeight();

        $pa = new PropertyAccessor();

        /** @var MaterialCoating $materialCoating */
        foreach ($this->getProject()->getMaterialCoatings() as $materialCoating) {
            if ($product->getMaterialTypeId() === $materialCoating->getMaterialTypeId()) {
                $price += $pa->getValue($product, Inflector::camelize($materialCoating->getProductPriceFieldName()));
            }
        }

        return $price;
    }

    /***********************************************/

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return ProjectProduct
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    /**
     * Set height
     *
     * @param float $height
     *
     * @return ProjectProduct
     */
    public function setHeight($height)
    {
        $this->height = $height;

        return $this;
    }

    /**
     * Get height
     *
     * @return float
     */
    public function getHeight()
    {
        return $this->height;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return ProjectProduct
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        if (!$project->getProjectProducts()->contains($this)) {
            $project->addProjectProduct($this);
        }

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set product
     *
     * @param Product $product
     *
     * @return ProjectProduct
     */
    public function setProduct(Product $product)
    {
        $this->product = $product;

        return $this;
    }

    /**
     * Get product
     *
     * @return Product
     */
    public function getProduct()
    {
        return $this->product;
    }
}
