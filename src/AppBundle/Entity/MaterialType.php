<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:14
 */

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialTypeRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialType
{
    /**
     * Material Type id
     * 
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     * 
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Popup Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $popupTitle;

    /**
     * Popup Content
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $popupContent;

    /**
     * Popup Url
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $popupUrl;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MaterialFinish", mappedBy="materialType", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $materialFinishes;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\RailingFinish", mappedBy="materialType", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $railingFinishes;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MaterialCoating", mappedBy="materialType", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $materialCoatings;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\MaterialFinishGroup", mappedBy="materialType", cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $materialFinishGroups;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Material", mappedBy="materialType", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $materials;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $popupImageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $popupImageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="popup_image_path")
     * @JMS\Exclude
     */
    protected $popupImagePath;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materialFinishes = new ArrayCollection();
        $this->railingFinishes = new ArrayCollection();
        $this->materialCoatings = new ArrayCollection();
        $this->materials = new ArrayCollection();
        $this->materialFinishGroups = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MaterialType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Add materialFinish
     *
     * @param MaterialFinish $materialFinish
     *
     * @return MaterialType
     */
    public function addMaterialFinish(MaterialFinish $materialFinish)
    {
        $this->materialFinishes[] = $materialFinish;

        return $this;
    }

    /**
     * Remove materialFinish
     *
     * @param materialFinish $materialFinish
     */
    public function removeMaterialFinish(MaterialFinish $materialFinish)
    {
        $this->materialFinishes->removeElement($materialFinish);
    }

    /**
     * Get MaterialFinishes
     *
     * @return Collection
     */
    public function getMaterialFinishes()
    {
        return $this->materialFinishes;
    }

    /**
     * Add material
     *
     * @param Material $material
     *
     * @return MaterialType
     */
    public function addMaterial(Material $material)
    {
        $this->materials[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param Material $material
     */
    public function removeMaterial(Material $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * Add materialFinishGroup
     *
     * @param MaterialFinishGroup $materialFinishGroup
     *
     * @return MaterialType
     */
    public function addMaterialFinishGroup(MaterialFinishGroup $materialFinishGroup)
    {
        $this->materialFinishGroups[] = $materialFinishGroup;

        return $this;
    }

    /**
     * Remove materialFinishGroup
     *
     * @param MaterialFinishGroup $materialFinishGroup
     */
    public function removeMaterialFinishGroup(MaterialFinishGroup $materialFinishGroup)
    {
        $this->materialFinishGroups->removeElement($materialFinishGroup);
    }

    /**
     * Get materialFinishGroups
     *
     * @return Collection
     */
    public function getMaterialFinishGroups()
    {
        return $this->materialFinishGroups;
    }

    /**
     * @return Collection
     */
    public function getMaterialCoatings()
    {
        return $this->materialCoatings;
    }

    /**
     * @param Collection $materialCoatings
     */
    public function setMaterialCoatings($materialCoatings)
    {
        $this->materialCoatings = $materialCoatings;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    public function getPopupImageAbsolutePath()
    {
        return $this->popupImagePath ? $this->getPopupImageUploadRootDir().'/'.$this->popupImagePath : null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getPopupImageUploadRootDir()
    {
        return __DIR__.'/../../../web/'.$this->getPopupImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/materialTypes';
    }

    protected function getPopupImageUploadDir()
    {
        return 'uploads/materialTypes/popup';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Sets popup image file.
     *
     * @param UploadedFile $file
     */
    public function setPopupImageFile(UploadedFile $file = null)
    {
        $this->popupImageFile = $file;

        // check if we have an old image path
        if (is_file($this->getPopupImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->popupImageTemp = $this->getPopupImageAbsolutePath();
        }

        $this->preUploadPopupImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Get popup image file.
     *
     * @return UploadedFile
     */
    public function getPopupImageFile()
    {
        return $this->popupImageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    public function preUploadPopupImage()
    {
        if (null !== $this->getPopupImageFile()) {
            $this->popupImagePath = FileUtil::generateFilename($this->getPopupImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadPopupImage()
    {
        if (null === $this->getPopupImageFile()) {
            return;
        }

        if (isset($this->popupImageTemp)) {
            unlink($this->popupImageTemp);
            $this->popupImageTemp = null;
        }
        $this->getPopupImageFile()->move(
            $this->getPopupImageUploadRootDir(),
            $this->popupImagePath
        );

        $this->setPopupImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp      = $this->getImageAbsolutePath();
        $this->popupImageTemp = $this->getPopupImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp)) {
            unlink($this->imageTemp);
        }

        if (isset($this->popupImageTemp)) {
            unlink($this->popupImageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Set popupImagePath
     *
     * @param string $popupImagePath
     * @return $this
     */
    public function setPopupImagePath($popupImagePath)
    {
        $this->popupImagePath = $popupImagePath;

        return $this;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Has Coating
     *
     * @JMS\VirtualProperty
     * @JMS\Type("boolean")
     * @JMS\SerializedName("has_coating")
     * @return boolean
     */
    public function hasCoating()
    {
        return ($this->getMaterialCoatings()->count() > 0);
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\SerializedName("preview")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    /**
     * Popup Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\SerializedName("popup_image")
     * @return null|string
     */
    public function getPopupImageWebPath()
    {
        if ($this->popupImagePath && is_file($this->getPopupImageAbsolutePath())) {
            return $this->getPopupImageUploadDir().'/'.$this->popupImagePath;
        }

        return null;
    }

    public function __toString()
    {
        return (string)$this->title;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Get popupImagePath
     *
     * @return string
     */
    public function getPopupImagePath()
    {
        return $this->popupImagePath;
    }

    /**
     * Add materialCoating
     *
     * @param MaterialCoating $materialCoating
     *
     * @return MaterialType
     */
    public function addMaterialCoating(MaterialCoating $materialCoating)
    {
        $this->materialCoatings[] = $materialCoating;

        return $this;
    }

    /**
     * Remove materialCoating
     *
     * @param MaterialCoating $materialCoating
     */
    public function removeMaterialCoating(MaterialCoating $materialCoating)
    {
        $this->materialCoatings->removeElement($materialCoating);
    }

    /**
     * Set popupTitle
     *
     * @param string $popupTitle
     *
     * @return MaterialType
     */
    public function setPopupTitle($popupTitle)
    {
        $this->popupTitle = $popupTitle;

        return $this;
    }

    /**
     * Get popupTitle
     *
     * @return string
     */
    public function getPopupTitle()
    {
        return $this->popupTitle;
    }

    /**
     * Set popupContent
     *
     * @param string $popupContent
     *
     * @return MaterialType
     */
    public function setPopupContent($popupContent)
    {
        $this->popupContent = $popupContent;

        return $this;
    }

    /**
     * Get popupContent
     *
     * @return string
     */
    public function getPopupContent()
    {
        return $this->popupContent;
    }

    /**
     * Set popupUrl
     *
     * @param string $popupUrl
     *
     * @return MaterialType
     */
    public function setPopupUrl($popupUrl)
    {
        $this->popupUrl = $popupUrl;

        return $this;
    }

    /**
     * Get popupUrl
     *
     * @return string
     */
    public function getPopupUrl()
    {
        return $this->popupUrl;
    }

    /**
     * Add railingFinish
     *
     * @param \AppBundle\Entity\RailingFinish $railingFinish
     *
     * @return MaterialType
     */
    public function addRailingFinish(\AppBundle\Entity\RailingFinish $railingFinish)
    {
        $this->railingFinishes[] = $railingFinish;

        return $this;
    }

    /**
     * Remove railingFinish
     *
     * @param \AppBundle\Entity\RailingFinish $railingFinish
     */
    public function removeRailingFinish(\AppBundle\Entity\RailingFinish $railingFinish)
    {
        $this->railingFinishes->removeElement($railingFinish);
    }

    /**
     * Get railingFinishes
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRailingFinishes()
    {
        return $this->railingFinishes;
    }
}
