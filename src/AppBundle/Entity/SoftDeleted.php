<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * Trait SoftDeleted
 *
 * @package AppBundle\Entity
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
trait SoftDeleted
{
    /**
     * @var boolean
     *
     * @ORM\Column(type="boolean", name="soft_deleted")
     * @JMS\Expose
     * @JMS\Groups({"Show", "List", "Export"})
     */
    protected $softDeleted;

    /**
     * @param bool $softDeleted
     *
     * @return $this
     */
    public function setSoftDeleted(bool $softDeleted)
    {
        $this->softDeleted = $softDeleted;

        return $this;
    }

    /**
     * @return bool
     */
    public function getSoftDeleted()
    {
        return $this->softDeleted;
    }

    /**
     * @return bool
     */
    public function isSoftDeleted()
    {
        return $this->softDeleted;
    }
}
