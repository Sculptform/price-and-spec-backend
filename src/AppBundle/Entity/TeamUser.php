<?php
/**
 * Created by PhpStorm.
 * User: zaytsev
 * Date: 15.12.16
 * Time: 12:46
 */

namespace AppBundle\Entity;

use AppBundle\Entity\Notification\TeamUserNotification;
use AppBundle\Enum\TeamUserStatus;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamUserRepository")
 * @ORM\Table(name="team_users")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class TeamUser
{
    /**
     * Team User id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Notification\TeamUserNotification", mappedBy="teamUser", cascade={"remove"}, fetch="EXTRA_LAZY")
     */
    protected $notifications;

    /**
     * @var Team
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team", inversedBy="teamUsers", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $team;

    /**
     * User type
     *
     * @var integer
     * @Assert\Choice(callback = {"AppBundle\Enum\TeamUserStatus", "getChoices"}, message="Enter a valid status.")
     * @ORM\Column(type="integer", nullable=false)
     * @see TeamUserStatus
     */
    protected $status = TeamUserStatus::CREATED;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return TeamUser
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return TeamUser
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set team
     *
     * @param Team $team
     *
     * @return TeamUser
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;

        return $this;
    }

    /**
     * Get team
     *
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    /**
     * Add notification
     *
     * @param TeamUserNotification $notification
     *
     * @return TeamUser
     */
    public function addNotification(TeamUserNotification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param TeamUserNotification $notification
     */
    public function removeNotification(TeamUserNotification $notification)
    {
        $this->notifications->removeElement($notification);
    }

    /**
     * Get notifications
     *
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }
}
