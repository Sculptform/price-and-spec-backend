<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\Team;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\TeamCommentRepository")
 * @ORM\Table(name="team_comments")
 *
 */
class TeamComment extends AbstractComment
{
    /**
     * @var Team
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Team", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="team_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $team;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return $this->getTeam()->getAdmin()->getId();
    }

    /**
     * Team Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getTeamId()
    {
        return $this->getTeam()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::TEAM;
    }
    
    /**
     * @return Team
     */
    public function getTeam()
    {
        return $this->team;
    }

    /**
     * @param Team $team
     * @return $this
     */
    public function setTeam(Team $team)
    {
        $this->team = $team;

        return $this;
    }
}
