<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\ProductSandpit;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\ProductSandpitCommentRepository")
 * @ORM\Table(name="product_sandpit_comments")
 *
 */
class ProductSandpitComment extends AbstractComment
{
    /**
     * @var ProductSandpit
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductSandpit", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="product_sandpit_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $productSandpit;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return null;
    }

    /**
     * ProductSandpit Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getProductSandpitId()
    {
        return $this->getProductSandpit()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::PRODUCT_SANDPIT;
    }

    /**
     * Set ProductSandpit
     *
     * @param ProductSandpit $productSandpit
     *
     * @return ProductSandpitComment
     */
    public function setProductSandpit(ProductSandpit $productSandpit)
    {
        $this->productSandpit = $productSandpit;

        return $this;
    }

    /**
     * Get ProductSandpit
     *
     * @return ProductSandpit
     */
    public function getProductSandpit()
    {
        return $this->productSandpit;
    }
}
