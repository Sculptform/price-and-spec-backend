<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\Project;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\ProjectCommentRepository")
 * @ORM\Table(name="project_comments")
 *
 */
class ProjectComment extends AbstractComment
{
    /**
     * @var Project
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="comments")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return $this->getProject()->getOwner()->getId();
    }

    /**
     * Project Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::PROJECT;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return ProjectComment
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
