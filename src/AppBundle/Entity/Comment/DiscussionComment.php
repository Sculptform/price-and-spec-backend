<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\Discussion;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\DiscussionCommentRepository")
 * @ORM\EntityListeners({"AppBundle\EntityListener\DiscussionCommentListener"})
 * @ORM\Table(
 *     name="discussion_comments",
 *     uniqueConstraints={
 *         @ORM\UniqueConstraint(name="discussion_comment_number_idx", columns={"discussion_id", "number"})
 *     }
 * )
 */
class DiscussionComment extends AbstractComment
{
    /**
     * @var Discussion
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discussion", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="discussion_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $discussion;

    /**
     * @var int
     * @ORM\Column(type="integer")
     */
    protected $number;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Discussion Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     * @return null|int
     */
    public function getDiscussionId()
    {
        return $this->getDiscussion()->getId();
    }

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return $this->getDiscussion()->getUser()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::DISCUSSION;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @param Discussion $discussion
     * @return $this
     */
    public function setDiscussion(Discussion $discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }

    /**
     * Set number
     *
     * @param integer $number
     *
     * @return DiscussionComment
     */
    public function setNumber($number)
    {
        $this->number = $number;

        return $this;
    }

    /**
     * Get number
     *
     * @return integer
     */
    public function getNumber()
    {
        return $this->number;
    }
}
