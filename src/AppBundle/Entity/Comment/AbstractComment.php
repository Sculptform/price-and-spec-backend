<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\Image\AbstractImage;
use AppBundle\Entity\Project;
use AppBundle\Entity\User;
use AppBundle\Enum\CommentType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Collections\Criteria;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\AbstractCommentRepository")
 * @ORM\Table(name="comment")
 * @ORM\InheritanceType("JOINED")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *      CommentType::TEAM = "AppBundle\Entity\Comment\TeamComment",
 *      CommentType::FEED = "AppBundle\Entity\Comment\FeedComment",
 *      CommentType::DISCUSSION = "AppBundle\Entity\Comment\DiscussionComment",
 *      CommentType::PROJECT = "AppBundle\Entity\Comment\ProjectComment",
 *      CommentType::PRODUCT_SANDPIT = "AppBundle\Entity\Comment\ProductSandpitComment",
 *      CommentType::GALLERY_PROJECT = "AppBundle\Entity\Comment\GalleryProjectComment",
 * })
 * @JMS\ExclusionPolicy("NONE")
 */
abstract class AbstractComment
{
    use TimestampableEntity;

    /**
     * Comment id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $id;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $user;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="comment_projects")
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $projects;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image\CommentImage", mappedBy="comment", fetch="EXTRA_LAZY")
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $images;

    /**
     * Content
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=false)
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $content;

    /**
     * @var AbstractComment
     *
     * @ORM\ManyToOne(targetEntity="AbstractComment", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", nullable=true, onDelete="CASCADE")
     *
     * @JMS\MaxDepth(1)
     */
    protected $parent;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @ORM\OneToMany(targetEntity="AbstractComment", mappedBy="parent")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @JMS\Groups({"Show", "List"})
     */
    protected $children;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="likedComments", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinTable(name="comment_liked_users")
     */
    protected $likedUsers;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="spammedComments", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="spammed_user_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * })
     * @JMS\MaxDepth(1)
     */
    protected $spammedUser;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->images = new ArrayCollection();
        $this->projects = new ArrayCollection();
        $this->children = new ArrayCollection();
        $this->likedUsers = new ArrayCollection();
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Comments Tree Owner Id (spam moderator)
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    abstract public function getCommentsTreeOwnerId();

    /**
     * @JMS\VirtualProperty
     * @return int
     */
    public function getChildrenCount()
    {
        return $this->children->count();
    }

    /**
     * @JMS\VirtualProperty
     * @JMS\SerializedName("children")
     * @return Collection
     */
    public function getLatestChildren()
    {
        $criteria = new Criteria();

        $criteria->setMaxResults(5);

        return $this->children->matching($criteria);
    }

    /**
     * Project ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @return array
     */
    public function getProjectIds()
    {
        return $this->getProjects()->map(function(Project $project){ return $project->getId(); })->toArray();
    }

    /**
     * Comment Type
     *
     * @see CommentType
     * @return int
     */
    abstract public function getType();

    /**
     * Users ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @JMS\Groups({"Show", "List"})
     * @return array
     */
    public function getLikedUserIds()
    {
        return $this->getLikedUsers()->map(function(User $user){ return $user->getId(); })->toArray();
    }

    /***********************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return AbstractComment
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    
    /**
     * Add image
     *
     * @param AbstractImage $image
     *
     * @return AbstractComment
     */
    public function addImage(AbstractImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param AbstractImage $image
     */
    public function removeImage(AbstractImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add project
     *
     * @param Project $project
     *
     * @return AbstractComment
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return AbstractComment
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set parent
     *
     * @param AbstractComment $parent
     *
     * @return AbstractComment
     */
    public function setParent(AbstractComment $parent)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return AbstractComment
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Add child
     *
     * @param AbstractComment $child
     *
     * @return AbstractComment
     */
    public function addChild(AbstractComment $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param AbstractComment $child
     */
    public function removeChild(AbstractComment $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Add likedUser
     *
     * @param User $likedUser
     *
     * @return AbstractComment
     */
    public function addLikedUser(User $likedUser)
    {
        $this->likedUsers[] = $likedUser;

        return $this;
    }

    /**
     * Remove likedUser
     *
     * @param User $likedUser
     */
    public function removeLikedUser(User $likedUser)
    {
        $this->likedUsers->removeElement($likedUser);
    }

    /**
     * Get likedUsers
     *
     * @return Collection
     */
    public function getLikedUsers()
    {
        return $this->likedUsers;
    }

    /**
     * Set spammedUser
     *
     * @param User $spammedUser
     *
     * @return AbstractComment
     */
    public function setSpammedUser(User $spammedUser = null)
    {
        $this->spammedUser = $spammedUser;

        return $this;
    }

    /**
     * Get spammedUser
     *
     * @return User
     */
    public function getSpammedUser()
    {
        return $this->spammedUser;
    }
}
