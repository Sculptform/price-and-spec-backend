<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\Feed;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\FeedCommentRepository")
 * @ORM\Table(name="feed_comments")
 *
 */
class FeedComment extends AbstractComment
{
    /**
     * @var Feed
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feed", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="feed_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $feed;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return $this->getFeed()->getUser()->getId();
    }

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     *
     * @return int
     */
    public function getFeedId()
    {
        return $this->getFeed()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::FEED;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     * @return FeedComment
     */
    public function setFeed(Feed $feed)
    {
        $this->feed = $feed;

        return $this;
    }
}
