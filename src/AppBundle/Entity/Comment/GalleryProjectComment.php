<?php

namespace AppBundle\Entity\Comment;

use AppBundle\Entity\GalleryProject;
use AppBundle\Enum\CommentType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\Comment\GalleryProjectCommentRepository")
 * @ORM\Table(name="gallery_project_comments")
 *
 */
class GalleryProjectComment extends AbstractComment
{
    /**
     * @var GalleryProject
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GalleryProject", inversedBy="comments")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="gallery_project_id", referencedColumnName="id", nullable=false)
     * })
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $galleryProject;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * {@inheritdoc}
     */
    public function getCommentsTreeOwnerId()
    {
        return $this->getGalleryProject()->getUser()->getId();
    }

    /**
     * GalleryProject Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return null|int
     */
    public function getGalleryProjectId()
    {
        return $this->getGalleryProject()->getId();
    }

    /***********************************************/

    public function getType(){
        return CommentType::GALLERY_PROJECT;
    }

    /**
     * Set GalleryProject
     *
     * @param GalleryProject $galleryProject
     *
     * @return GalleryProjectComment
     */
    public function setGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProject = $galleryProject;

        return $this;
    }

    /**
     * Get GalleryProject
     *
     * @return GalleryProject
     */
    public function getGalleryProject()
    {
        return $this->galleryProject;
    }
}
