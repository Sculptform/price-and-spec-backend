<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AcousticRating
 *
 * @ORM\Table(name="acoustic_rating")
 * @ORM\Entity()
 * @JMS\ExclusionPolicy("NONE")
 */
class AcousticRating
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * NRC
     *
     * @var int
     * @ORM\Column(type="float", nullable=true, options={"default": 0})
     */
    protected $nrc = 0;

    /**
     * Backing
     *
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    protected $backing = 0;

    /**
     * Cavity
     *
     * @var int
     * @ORM\Column(type="integer", nullable=true, options={"default": 0})
     */
    protected $cavity = 0;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set nrc
     *
     * @param float $nrc
     *
     * @return AcousticRating
     */
    public function setNrc($nrc)
    {
        $this->nrc = $nrc;

        return $this;
    }

    /**
     * Get nrc
     *
     * @return float
     */
    public function getNrc()
    {
        return $this->nrc;
    }

    /**
     * Set backing
     *
     * @param integer $backing
     *
     * @return AcousticRating
     */
    public function setBacking($backing)
    {
        $this->backing = $backing;

        return $this;
    }

    /**
     * Get backing
     *
     * @return integer
     */
    public function getBacking()
    {
        return $this->backing;
    }

    /**
     * Set cavity
     *
     * @param integer $cavity
     *
     * @return AcousticRating
     */
    public function setCavity($cavity)
    {
        $this->cavity = $cavity;

        return $this;
    }

    /**
     * Get cavity
     *
     * @return integer
     */
    public function getCavity()
    {
        return $this->cavity;
    }
}
