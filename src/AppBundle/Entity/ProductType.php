<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:10
 */

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProductTypeRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class ProductType
{
    const CONCEPTCLICK = 1;
    const EXPRESSIONCLADDING = 2;
    const FINTRAX = 3;
    const FACADE_BLADES = 'Facade Blades';

    use TimestampableEntity;

    /**
     * Product Type id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $disclaimer;

    /**
     * Popup Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $popupTitle;

    /**
     * Popup Content
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $popupContent;

    /**
     * Popup Url
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $popupUrl;

    /**
     * Title
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $fullTitle;

    /**
     * Description
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;

    /**
     * Price include note
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $priceIncludeNote;

    /**
     * Price exclude note
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $priceExcludeNote;

    /**
     * Price note
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=true)
     */
    protected $priceNote;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $pdfHeaderTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $pdfHeaderFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="pdf_header_path")
     * @JMS\Exclude
     */
    protected $pdfHeaderPath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $popupImageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $popupImageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="popup_image_path")
     * @JMS\Exclude
     */
    protected $popupImagePath;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Material", mappedBy="productType", cascade={"persist"},
     *                                                          fetch="EXTRA_LAZY")
     */
    protected $materials;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Project", mappedBy="productType", cascade={"all"},
     *                                                         fetch="EXTRA_LAZY")
     */
    protected $projects;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Railing", mappedBy="productType", cascade={"all"},
     *                                                         fetch="EXTRA_LAZY")
     */
    protected $railings;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\AcousticBacking", mappedBy="productType", cascade={"all"},
     *                                                                 fetch="EXTRA_LAZY")
     */
    protected $acousticBackings;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materials        = new ArrayCollection();
        $this->railings         = new ArrayCollection();
        $this->projects         = new ArrayCollection();
        $this->acousticBackings = new ArrayCollection();
    }

    /**
     * @return Collection
     */
    public function getAcousticBackings(): Collection
    {
        return $this->acousticBackings;
    }

    /**
     * @param Collection $acousticBackings
     */
    public function setAcousticBackings(Collection $acousticBackings): void
    {
        $this->acousticBackings = $acousticBackings;
    }

    /**
     * Add AcousticBacking
     *
     * @param \AppBundle\Entity\AcousticBacking $acousticBacking
     *
     * @return ProductType
     */
    public function addAcousticBacking(\AppBundle\Entity\AcousticBacking $acousticBacking)
    {
        $this->acousticBackings[] = $acousticBacking;

        return $this;
    }

    /**
     * Remove AcousticBacking
     *
     * @param \AppBundle\Entity\AcousticBacking $acousticBacking
     */
    public function removeAcousticBacking(\AppBundle\Entity\AcousticBacking $acousticBacking)
    {
        $this->acousticBackings->removeElement($acousticBacking);
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return ProductType
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    public function getPopupImageAbsolutePath()
    {
        return $this->popupImagePath ? $this->getPopupImageUploadRootDir().'/'.$this->popupImagePath : null;
    }

    public function getPdfHeaderAbsolutePath()
    {
        return $this->pdfHeaderPath ? $this->getImageUploadRootDir().'/'.$this->pdfHeaderPath : null;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    /**
     * Popup Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("popup_image")
     * @JMS\Type("string")
     * @return null|string
     */
    public function getPopupImageWebPath()
    {
        if ($this->popupImagePath && is_file($this->getPopupImageAbsolutePath())) {
            return $this->getPopupImageUploadDir().'/'.$this->popupImagePath;
        }

        return null;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getPdfHeaderWebPath()
    {
        if ($this->pdfHeaderPath && is_file($this->getPdfHeaderAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->pdfHeaderPath;
        }

        return null;
    }

    /***********************************************/

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getPopupImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getPopupImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/productTypes';
    }

    protected function getPopupImageUploadDir()
    {
        return 'uploads/productTypes/popup';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Sets popup image file.
     *
     * @param UploadedFile $file
     */
    public function setPopupImageFile(UploadedFile $file = null)
    {
        $this->popupImageFile = $file;

        if (is_file($this->getPopupImageAbsolutePath())) {
            $this->popupImageTemp = $this->getPopupImageAbsolutePath();
        }

        $this->preUploadPopupImage();
    }

    /**
     * Sets pdfHeaderFile file.
     *
     * @param UploadedFile $file
     */
    public function setPdfHeaderFile(UploadedFile $file = null)
    {
        $this->pdfHeaderFile = $file;

        // check if we have an old image path
        if (is_file($this->getPdfHeaderAbsolutePath())) {
            // store the old name to delete after the update
            $this->pdfHeaderTemp = $this->getPdfHeaderAbsolutePath();
        }

        $this->preUploadPdfHeader();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    /**
     * Get popup image file.
     *
     * @return UploadedFile
     */
    public function getPopupImageFile()
    {
        return $this->popupImageFile;
    }

    /**
     * @return UploadedFile
     */
    public function getPdfHeaderFile()
    {
        return $this->pdfHeaderFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    public function preUploadPopupImage()
    {
        if (null !== $this->getPopupImageFile()) {
            $this->popupImagePath = FileUtil::generateFilename($this->getPopupImageFile());
        }
    }

    public function preUploadPdfHeader()
    {
        if (null !== $this->getPdfHeaderFile()) {
            $this->pdfHeaderPath = FileUtil::generateFilename($this->getPdfHeaderFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadPopupImage()
    {
        if (null === $this->getPopupImageFile()) {
            return;
        }

        if (isset($this->popupImageTemp)) {
            unlink($this->popupImageTemp);
            $this->popupImageTemp = null;
        }

        $this->getPopupImageFile()->move(
            $this->getPopupImageUploadRootDir(),
            $this->popupImagePath
        );

        $this->setPopupImageFile(null);
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadPdfHeader()
    {
        if (null === $this->getPdfHeaderFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->pdfHeaderTemp)) {
            // delete the old image
            unlink($this->pdfHeaderTemp);
            // clear the temp image path
            $this->pdfHeaderTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getPdfHeaderFile()->move(
            $this->getImageUploadRootDir(),
            $this->pdfHeaderPath
        );

        $this->setPdfHeaderFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp      = $this->getImageAbsolutePath();
        $this->popupImageTemp = $this->getPopupImageAbsolutePath();
        $this->pdfHeaderTemp  = $this->getPdfHeaderAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp)) {
            unlink($this->imageTemp);
        }

        if (isset($this->popupImageTemp)) {
            unlink($this->popupImageTemp);
        }

        if (isset($this->pdfHeaderTemp)) {
            unlink($this->pdfHeaderTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Set popup imagePath
     *
     * @param string $popupImagePath
     *
     * @return $this
     */
    public function setPopupImagePath($popupImagePath)
    {
        $this->popupImagePath = $popupImagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Get popup imagePath
     *
     * @return string
     */
    public function getPopupImagePath()
    {
        return $this->popupImagePath;
    }

    /**
     * @return string
     */
    public function getPdfHeaderPath()
    {
        return $this->pdfHeaderPath;
    }

    /**
     * @param string $pdfHeaderPath
     */
    public function setPdfHeaderPath($pdfHeaderPath)
    {
        $this->pdfHeaderPath = $pdfHeaderPath;
    }


    /**
     * Add material
     *
     * @param Material $material
     *
     * @return ProductType
     */
    public function addMaterial(Material $material)
    {
        $this->materials[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param Material $material
     */
    public function removeMaterial(Material $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * @return Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * @param Collection $projects
     */
    public function setProjects($projects)
    {
        $this->projects = $projects;
    }

    /**
     * Add project
     *
     * @param Project $project
     *
     * @return ProductType
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return ProductType
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->title;
    }

    /**
     * Add railing
     *
     * @param \AppBundle\Entity\Railing $railing
     *
     * @return ProductType
     */
    public function addRailing(\AppBundle\Entity\Railing $railing)
    {
        $this->railings[] = $railing;

        return $this;
    }

    /**
     * Remove railing
     *
     * @param \AppBundle\Entity\Railing $railing
     */
    public function removeRailing(\AppBundle\Entity\Railing $railing)
    {
        $this->railings->removeElement($railing);
    }

    /**
     * Get railings
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getRailings()
    {
        return $this->railings;
    }

    /**
     * Set popupTitle
     *
     * @param string $popupTitle
     *
     * @return ProductType
     */
    public function setPopupTitle($popupTitle)
    {
        $this->popupTitle = $popupTitle;
    }

    /**
     * Set fullTitle
     *
     * @param string $fullTitle
     *
     * @return ProductType
     */
    public function setFullTitle($fullTitle)
    {
        $this->fullTitle = $fullTitle;

        return $this;
    }

    /**
     * Get popupTitle
     *
     * @return string
     */
    public function getPopupTitle()
    {
        return $this->popupTitle;
    }

    /**
     * Set popupContent
     *
     * @param string $popupContent
     *
     * @return ProductType
     */
    public function setPopupContent($popupContent)
    {
        $this->popupContent = $popupContent;

        return $this;
    }

    /**
     * Get popupContent
     *
     * @return string
     */
    public function getPopupContent()
    {
        return $this->popupContent;
    }

    /**
     * Set popupUrl
     *
     * @param string $popupUrl
     *
     * @return ProductType
     */
    public function setPopupUrl($popupUrl)
    {
        $this->popupUrl = $popupUrl;

        return $this;
    }

    /**
     * Get popupUrl
     *
     * @return string
     */
    public function getPopupUrl()
    {
        return $this->popupUrl;
    }

    /**
     * Get fullTitle
     *
     * @return string
     */
    public function getFullTitle()
    {
        return $this->fullTitle;
    }

    /**
     * Set priceIncludeNote.
     *
     * @param string|null $priceIncludeNote
     *
     * @return ProductType
     */
    public function setPriceIncludeNote($priceIncludeNote = null)
    {
        $this->priceIncludeNote = $priceIncludeNote;

        return $this;
    }

    /**
     * Get priceIncludeNote.
     *
     * @return string|null
     */
    public function getPriceIncludeNote()
    {
        return $this->priceIncludeNote;
    }

    /**
     * Set priceExcludeNote.
     *
     * @param string|null $priceExcludeNote
     *
     * @return ProductType
     */
    public function setPriceExcludeNote($priceExcludeNote = null)
    {
        $this->priceExcludeNote = $priceExcludeNote;

        return $this;
    }

    /**
     * Get priceExcludeNote.
     *
     * @return string|null
     */
    public function getPriceExcludeNote()
    {
        return $this->priceExcludeNote;
    }

    /**
     * Set priceNote.
     *
     * @param string|null $priceNote
     *
     * @return ProductType
     */
    public function setPriceNote($priceNote = null)
    {
        $this->priceNote = $priceNote;

        return $this;
    }

    /**
     * Get priceNote.
     *
     * @return string|null
     */
    public function getPriceNote()
    {
        return $this->priceNote;
    }

    /**
     * Set disclaimer.
     *
     * @param string|null $disclaimer
     *
     * @return ProductType
     */
    public function setDisclaimer($disclaimer = null)
    {
        $this->disclaimer = $disclaimer;

        return $this;
    }

    /**
     * Get disclaimer.
     *
     * @return string|null
     */
    public function getDisclaimer()
    {
        return $this->disclaimer;
    }
}
