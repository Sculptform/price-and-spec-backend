<?php

namespace AppBundle\Entity;

use AppBundle\Enum\ApplicationType;
use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Ldap\Adapter\ExtLdap\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Material
{
    use SoftDeleted;

    /**
     * Material id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Object3D
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Object3D", inversedBy="materials", cascade={"persist"})
     * @ORM\JoinColumn(name="object3d_id", referencedColumnName="id")
     */
    protected $object3d;

    /**
     * @var array
     *
     * @ORM\Column(type="simple_array", nullable=true)
     * @see ApplicationType
     */
    protected $applicationTypes;

    /**
     * @var MaterialType
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MaterialType", inversedBy="materials", cascade={"persist"})
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id")
     */
    protected $materialType;

    /**
     * @var ProductType
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\ProductType", inversedBy="materials", cascade={"persist"})
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;

    /**
     * @var MaterialShapeSize
     *
     * @ORM\ManyToOne(
     *     targetEntity="AppBundle\Entity\MaterialShapeSize",
     *     inversedBy="materials",
     *     cascade={"persist"},
     *     fetch="EAGER"
     * )
     * @ORM\JoinColumn(name="material_shape_size_id", referencedColumnName="id")
     */
    protected $materialShapeSize;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Product",
     *     mappedBy="material",
     *     cascade={"all"},
     *     fetch="EXTRA_LAZY"
     * )
     */
    protected $products;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $dwgTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {
     *          "application/acad",
     *          "application/x-acad",
     *          "application/autocad_dwg",
     *          "image/x-dwg",
     *          "application/dwg",
     *          "application/x-dwg",
     *          "application/x-autocad",
     *          "image/vnd.dwg",
     *          "drawing/dwg"
     *     },
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the DWG files are allowed."
     * )
     */
    protected $dwgFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="dwg_path")
     * @JMS\Exclude
     */
    protected $dwgPath;

    /**
     * @var int
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $orderWeight;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * DWG Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getDwgWebPath()
    {
        if ($this->dwgPath && is_file($this->getDwgAbsolutePath())) {
            return $this->getDwgUploadDir().'/'.$this->dwgPath;
        }

        return null;
    }


    /**
     * Set applicationTypes
     *
     * @param array $applicationTypes
     *
     * @return Material
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;

        return $this;
    }

    /**
     * Get applicationTypes
     *
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * Set object3d
     *
     * @param Object3D $object3d
     *
     * @return Material
     */
    public function setObject3d(Object3D $object3d = NULL)
    {
        $this->object3d = $object3d;

        return $this;
    }

    /**
     * Get object3d
     *
     * @return Object3D
     */
    public function getObject3d()
    {
        return $this->object3d;
    }

    /**
     * Set materialType
     *
     * @param MaterialType $materialType
     *
     * @return Material
     */
    public function setMaterialType(MaterialType $materialType = NULL)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    /**
     * Set materialShapeSize
     *
     * @param MaterialShapeSize $materialShapeSize
     *
     * @return Material
     */
    public function setMaterialShapeSize(MaterialShapeSize $materialShapeSize = NULL)
    {
        $this->materialShapeSize = $materialShapeSize;

        return $this;
    }

    /**
     * Get materialShapeSize
     *
     * @return MaterialShapeSize
     */
    public function getMaterialShapeSize()
    {
        return $this->materialShapeSize;
    }

    /**
     * Set productType
     *
     * @param ProductType $productType
     *
     * @return Material
     */
    public function setProductType(ProductType $productType = NULL)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * @return ArrayCollection
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * @param Collection $products
     */
    public function setProducts($products)
    {
        $this->products = $products;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products         = new ArrayCollection();
        $this->softDeleted      = false;
        $this->applicationTypes = [];
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return Material
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     *
     * Material Shape Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return integer
     */
    public function getMaterialShapeId()
    {
        return $this->getMaterialShapeSize()->getMaterialShape()->getId();
    }

    /**
     *
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return integer
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterialType()->getId();
    }

    /**
     * Product Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     *
     * @return integer
     */
    public function getProductTypeId()
    {
        return $this->getProductType()->getId();
    }

    /***********************************************/

    /**
     * @return string
     */
    public function getCompositeTitle()
    {
        if ($this->getProductType() && $this->getMaterialType() && $this->getMaterialShapeSize()
                ->getMaterialShape()
        ) {
            return sprintf('%s - %s - %s %d (w) x %d (d)',
                $this->getProductType()->getTitle(),
                $this->getMaterialType()->getTitle(),
                $this->getMaterialShapeSize()->getMaterialShape()->getTitle(),
                $this->getMaterialShapeSize()->getWidth(),
                $this->getMaterialShapeSize()->getDepth()
            );
        }

        return '';
    }

    public function getDwgAbsolutePath()
    {
        return $this->dwgPath ? $this->getDwgUploadRootDir().'/'.$this->dwgPath : null;
    }

    protected function getDwgUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getDwgUploadDir();
    }

    protected function getDwgUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/dwg';
    }

    /**
     * Sets DWG file.
     *
     * @param UploadedFile $file
     */
    public function setDwgFile(UploadedFile $file = null)
    {
        $this->dwgFile = $file;

        // check if we have an old image path
        if (is_file($this->getDwgAbsolutePath())) {
            // store the old name to delete after the update
            $this->dwgTemp = $this->getDwgAbsolutePath();
        }

        $this->preUploadDwg();
    }

    /**
     * Get DWG file.
     *
     * @return UploadedFile
     */
    public function getDwgFile()
    {
        return $this->dwgFile;
    }

    public function preUploadDwg()
    {
        if (null !== $this->getDwgFile()) {
            $this->dwgPath = FileUtil::generateFilename($this->getDwgFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadDwg()
    {
        if (null === $this->getDwgFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->dwgTemp)) {
            // delete the old image
            unlink($this->dwgTemp);
            // clear the temp image path
            $this->dwgTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getDwgFile()->move(
            $this->getDwgUploadRootDir(),
            $this->dwgPath
        );

        $this->setDwgFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeDwgFilenameForRemove()
    {
        $this->dwgTemp = $this->getDwgAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeDwgUpload()
    {
        if (isset($this->dwgTemp)) {
            @unlink($this->dwgTemp);
        }
    }

    /**
     * Set DWG path
     *
     * @param string $dwgPath
     * @return $this
     */
    public function setDwgPath($dwgPath)
    {
        $this->dwgPath = $dwgPath;

        return $this;
    }

    /**
     * Get DWG path
     *
     * @return string
     */
    public function getDwgPath()
    {
        return $this->dwgPath;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getCompositeTitle();
    }

    /**
     * Set orderWeight
     *
     * @param int $orderWeight
     *
     * @return Material
     */
    public function setOrderWeight($orderWeight)
    {
        $this->orderWeight = $orderWeight;

        return $this;
    }

    /**
     * Get orderWeight
     *
     * @return int
     */
    public function getOrderWeight()
    {
        return $this->orderWeight;
    }
}
