<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:38
 */

namespace AppBundle\Entity;

use AppBundle\Enum\SampleRequestProjectSize;
use AppBundle\Enum\SampleType;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class SampleRequest
{
    use TimestampableEntity;

    /**
     * SampleRequest id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Sample type
     *
     * @var integer
     * @JMS\Expose
     * @JMS\Groups({"Show", "List"})
     * @Assert\NotBlank()
     * @Assert\Choice(
     *     callback={"AppBundle\Enum\SampleType", "getChoices"},
     *     message="Enter a valid sample type."
     * )
     * @ORM\Column(type="integer", nullable=false, options={"default"=1})
     * @see SampleType
     */
    protected $sampleType;

    /**
     * @return integer
     */
    public function getSampleType()
    {
        return $this->sampleType;
    }

    /**
     * @param integer $sampleType
     */
    public function setSampleType($sampleType)
    {
        $this->sampleType = $sampleType;
    }

    /**
     * Project Name
     *
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $projectName;

    /**
     * Project Name
     *
     * @var string
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $companyName;

    /**
     * Street Address
     *
     * @Assert\NotBlank()
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $streetAddress;

    /**
     * Post Code
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $postCode;

    /**
     * Suburb/Town
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $town;

    /**
     * Email address
     *
     * @var string
     * @Assert\Email(
     *     strict  = true,
     *     message = "The email '{{ value }}' is not a valid email.",
     *     checkMX = true
     * )
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * User's full name
     *
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * Content
     *
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="text", nullable=false)
     */
    protected $content;

    /**
     * Phone number
     * @Assert\NotBlank()
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $phone;

    /**
     * @var int
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\SampleRequestProjectSize", "getChoices"}, message="Enter a valid project size.")
     * @ORM\Column(type="integer", nullable=true)
     * @see SampleRequestProjectSize
     */
    protected $projectSize;

    /**
     * @var Project
     *
     * @JMS\Exclude
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="sampleRequests")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * @var User
     *
     * @Assert\NotBlank()
     *
     * @JMS\Exclude
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * Quotation constructor.
     */
    public function __construct() {}

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }
    /**
     * Set user
     *
     * @param User $user
     *
     * @return SampleRequest
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * @return string
     */
    public function getProjectName()
    {
        return $this->projectName;
    }

    /**
     * @param string $projectName
     */
    public function setProjectName($projectName)
    {
        $this->projectName = $projectName;
    }

    /**
     * @return mixed
     */
    public function getStreetAddress()
    {
        return $this->streetAddress;
    }

    /**
     * @param mixed $streetAddress
     */
    public function setStreetAddress($streetAddress)
    {
        $this->streetAddress = $streetAddress;
    }

    /**
     * @return mixed
     */
    public function getPostCode()
    {
        return $this->postCode;
    }

    /**
     * @param mixed $postCode
     */
    public function setPostCode($postCode)
    {
        $this->postCode = $postCode;
    }

    /**
     * @return mixed
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param mixed $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return int
     */
    public function getProjectSize()
    {
        return $this->projectSize;
    }

    /**
     * @param int $projectSize
     */
    public function setProjectSize($projectSize)
    {
        $this->projectSize = $projectSize;
    }



    /**
     * Set town
     *
     * @param string $town
     *
     * @return SampleRequest
     */
    public function setTown($town)
    {
        $this->town = $town;

        return $this;
    }

    /**
     * Get town
     *
     * @return string
     */
    public function getTown()
    {
        return $this->town;
    }

    /**
     * Set project
     *
     * @param \AppBundle\Entity\Project $project
     *
     * @return SampleRequest
     */
    public function setProject(\AppBundle\Entity\Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return \AppBundle\Entity\Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set companyName.
     *
     * @param string $companyName
     *
     * @return SampleRequest
     */
    public function setCompanyName($companyName)
    {
        $this->companyName = $companyName;

        return $this;
    }

    /**
     * Get companyName.
     *
     * @return string
     */
    public function getCompanyName()
    {
        return $this->companyName;
    }

    /**
     * Set name.
     *
     * @param string $name
     *
     * @return SampleRequest
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name.
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set content.
     *
     * @param string $content
     *
     * @return SampleRequest
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content.
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }
}
