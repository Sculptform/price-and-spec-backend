<?php

namespace AppBundle\Entity\SharedContent;

use AppBundle\Entity\Feed;
use AppBundle\Enum\SharedContentType;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SharedFeed extends SharedContent
{
    /**
     * @var Feed
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Feed")
     * @ORM\JoinColumn(name="feed_id", referencedColumnName="id", nullable=true)
     * @JMS\Groups({"Show", "List"})
     */
    protected $feed;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     *
     * @return int
     */
    public function getFeedId()
    {
        return $this->getFeed()->getId();
    }

    /***********************************************/

    /**
     * @return int
     */
    public function getType(){
        return SharedContentType::FEED;
    }

    /**
     * @return Feed
     */
    public function getFeed()
    {
        return $this->feed;
    }

    /**
     * @param Feed $feed
     * @return SharedFeed
     */
    public function setFeed(Feed $feed)
    {
        $this->feed = $feed;

        return $this;
    }
}
