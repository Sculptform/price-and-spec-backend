<?php

namespace AppBundle\Entity\SharedContent;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Entity\Project;
use AppBundle\Enum\SharedContentType;

/**
 * @ORM\Entity
 */
class SharedProject extends SharedContent
{
    /**
     * @var Project
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=true)
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $project;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Show", "List"})
     *
     * @return string
     */
    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    /***********************************************/

    /**
     * @return int
     */
    public function getType(){
        return SharedContentType::PROJECT;
    }

    /**
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return SharedProject
     */
    public function setProject(Project $project = null)
    {
        $this->project = $project;

        return $this;
    }
}
