<?php

namespace AppBundle\Entity\SharedContent;

use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Enum\SharedContentType;

/**
 * @ORM\Entity
 * @ORM\Table(name="shared_content")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *      SharedContentType::FEED            = "AppBundle\Entity\SharedContent\SharedFeed",
 *      SharedContentType::GALLERY_PROJECT = "AppBundle\Entity\SharedContent\SharedGalleryProject",
 *      SharedContentType::PROJECT         = "AppBundle\Entity\SharedContent\SharedProject",
 *      SharedContentType::DISCUSSION      = "AppBundle\Entity\SharedContent\SharedDiscussion"
 * })
 * @JMS\ExclusionPolicy("NONE")
 */
abstract class SharedContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * @return int
     * @see SharedContentType
     */
    abstract public function getType();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
}
