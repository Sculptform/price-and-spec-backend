<?php

namespace AppBundle\Entity\SharedContent;

use AppBundle\Entity\GalleryProject;
use AppBundle\Enum\SharedContentType;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SharedGalleryProject extends SharedContent
{
    /**
     * @var GalleryProject
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\GalleryProject")
     * @ORM\JoinColumn(name="gallery_project_id", referencedColumnName="id", nullable=true)
     *
     * @JMS\Groups({"Show", "List"})
     */
    protected $galleryProject;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * GalleryProject Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     * @return null|int
     */
    public function getGalleryProjectId()
    {
        return $this->getGalleryProject()->getId();
    }

    /***********************************************/

    /**
     * @return int
     */
    public function getType(){
        return SharedContentType::GALLERY_PROJECT;
    }

    /**
     * Set GalleryProject
     *
     * @param GalleryProject $galleryProject
     *
     * @return SharedGalleryProject
     */
    public function setGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProject = $galleryProject;

        return $this;
    }

    /**
     * Get GalleryProject
     *
     * @return GalleryProject
     */
    public function getGalleryProject()
    {
        return $this->galleryProject;
    }
}
