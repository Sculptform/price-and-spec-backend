<?php

namespace AppBundle\Entity\SharedContent;

use AppBundle\Entity\Discussion;
use AppBundle\Enum\SharedContentType;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class SharedDiscussion extends SharedContent
{
    /**
     * @var Discussion
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discussion")
     * @ORM\JoinColumn(name="discussion_id", referencedColumnName="id", nullable=true)
     * @JMS\Groups({"Show", "List"})
     */
    protected $discussion;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Feed Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     *
     * @return int
     */
    public function getDiscussionId()
    {
        return $this->getDiscussion()->getId();
    }

    /***********************************************/

    /**
     * @return int
     */
    public function getType(){
        return SharedContentType::DISCUSSION;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @param Discussion $discussion
     * @return SharedDiscussion
     */
    public function setDiscussion(Discussion $discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }
}
