<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Comment\GalleryProjectComment;
use AppBundle\Entity\Image\GalleryProjectImage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use AppBundle\Validator\Constraints as CustomAssert;
use Gedmo\Timestampable\Traits\TimestampableEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\GalleryProjectRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 *
 * @author Vladimir Eliseev <vladimir.eliseev@sibers.com>
 */
class GalleryProject
{
    /**
     * Hook timestampable behavior
     * updates createdAt, updatedAt fields
     */
    use TimestampableEntity;

    /**
     * Project id
     *
     * @var integer
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update", "Share"})
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $name;

    /**
     * Architect
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $architect;

    /**
     * Location
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $location;

    /**
     * Builder
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $builder;

    /**
     * Applications
     *
     * @var integer
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $applications;

    /**
     * Finish
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $finish;

    /**
     * Sector
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\ManyToOne(targetEntity="Sector")
     * @ORM\JoinColumn(name="sector_id", referencedColumnName="id")
     *
     */
    protected $sector;

    /**
     * Year
     *
     * @var integer
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @Assert\Range(
     *     min = "1900",
     *     max = "3000"
     * )
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $year;

    /**
     * Photographer
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $photographer;

    /**
     * Video URL
     *
     * @var string
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update"})
     * @JMS\SerializedName("videoUrl")
     * @CustomAssert\YoutubeVimeoConstraint
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $videoUrl;

    /**
     * @var Collection
     *
     * @JMS\Expose
     * @JMS\Groups({"List", "show", "update", "Share"})
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Image\GalleryProjectImage",
     *     mappedBy="galleryProject", cascade={"persist", "remove"},
     *     orphanRemoval=true, fetch="EXTRA_LAZY"
     * )
     * @JMS\Type("array<AppBundle\Entity\Image\GalleryProjectImage>")
     */
    protected $galleryProjectImages;

    /**
     * @var Project
     *
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="galleryProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $project;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="User", inversedBy="galleryProjects")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * @JMS\Groups("List")
     */
    protected $user;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Comment\GalleryProjectComment",
     *     mappedBy="galleryProject", cascade={"persist"}, fetch="EXTRA_LAZY"
     * )
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @JMS\Groups({"Show", "List"})
     * @JMS\Exclude
     */
    protected $comments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->galleryProjectImages = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Comments count
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @return int
     */
    public function getCommentsCount()
    {
        return $this->comments->count();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return GalleryProject
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set architect
     *
     * @param string $architect
     *
     * @return GalleryProject
     */
    public function setArchitect($architect)
    {
        $this->architect = $architect;

        return $this;
    }

    /**
     * Get architect
     *
     * @return string
     */
    public function getArchitect()
    {
        return $this->architect;
    }

    /**
     * Set location
     *
     * @param string $location
     *
     * @return GalleryProject
     */
    public function setLocation($location)
    {
        $this->location = $location;

        return $this;
    }

    /**
     * Get location
     *
     * @return string
     */
    public function getLocation()
    {
        return $this->location;
    }

    /**
     * Set builder
     *
     * @param string $builder
     *
     * @return GalleryProject
     */
    public function setBuilder($builder)
    {
        $this->builder = $builder;

        return $this;
    }

    /**
     * Get builder
     *
     * @return string
     */
    public function getBuilder()
    {
        return $this->builder;
    }

    /**
     * Set applications
     *
     * @param string $applications
     *
     * @return GalleryProject
     */
    public function setApplications($applications)
    {
        $this->applications = $applications;

        return $this;
    }

    /**
     * Get applications
     *
     * @return string
     */
    public function getApplications()
    {
        return $this->applications;
    }

    /**
     * Set finish
     *
     * @param string $finish
     *
     * @return GalleryProject
     */
    public function setFinish($finish)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return string
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set sector
     *
     * @param string $sector
     *
     * @return GalleryProject
     */
    public function setSector($sector)
    {
        $this->sector = $sector;

        return $this;
    }

    /**
     * Get sector
     *
     * @return string
     */
    public function getSector()
    {
        return $this->sector;
    }

    /**
     * Set year
     *
     * @param integer $year
     *
     * @return GalleryProject
     */
    public function setYear($year)
    {
        $this->year = $year;

        return $this;
    }

    /**
     * Get year
     *
     * @return integer
     */
    public function getYear()
    {
        return $this->year;
    }

    /**
     * Set photographer
     *
     * @param string $photographer
     *
     * @return GalleryProject
     */
    public function setPhotographer($photographer)
    {
        $this->photographer = $photographer;

        return $this;
    }

    /**
     * Get photographer
     *
     * @return string
     */
    public function getPhotographer()
    {
        return $this->photographer;
    }

    /**
     * Set videoUrl
     *
     * @param string $videoUrl
     *
     * @return GalleryProject
     */
    public function setVideoUrl($videoUrl)
    {
        $this->videoUrl = $videoUrl;

        return $this;
    }

    /**
     * Get videoUrl
     *
     * @return string
     */
    public function getVideoUrl()
    {
        return $this->videoUrl;
    }

    /**
     * Add galleryProjectImage
     *
     * @param GalleryProjectImage $galleryProjectImage
     *
     * @return GalleryProject
     */
    public function addGalleryProjectImage(GalleryProjectImage $galleryProjectImage)
    {
        $this->galleryProjectImages[] = $galleryProjectImage;

        return $this;
    }

    /**
     * Remove galleryProjectImage
     *
     * @param GalleryProjectImage $galleryProjectImage
     */
    public function removeGalleryProjectImage(GalleryProjectImage $galleryProjectImage)
    {
        $this->galleryProjectImages->removeElement($galleryProjectImage);
    }

    /**
     * Get galleryProjectImages
     *
     * @return Collection
     */
    public function getGalleryProjectImages()
    {
        return $this->galleryProjectImages;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return GalleryProject
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return GalleryProject
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Add comment
     *
     * @param GalleryProjectComment $comment
     *
     * @return GalleryProject
     */
    public function addComment(GalleryProjectComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param GalleryProjectComment $comment
     */
    public function removeComment(GalleryProjectComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
