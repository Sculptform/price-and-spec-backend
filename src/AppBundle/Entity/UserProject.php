<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:19
 */

namespace AppBundle\Entity;

use AppBundle\Enum\UserProjectUserType;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserProjectRepository")
 * @ORM\Table(uniqueConstraints={
 *    @ORM\UniqueConstraint(name="unique_idx", columns={"project_id", "user_id"})
 * }))
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 * @UniqueEntity(
 *     fields={"project", "user"},
 *     errorPath="user",
 *     message="This user is already in use on that project."
 * )
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class UserProject
{
    /**
     * User Project id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="userProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $user;

    /**
     * @var Project
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Project", inversedBy="userProjects", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * User type
     *
     * @var integer
     * @Assert\Choice(callback = {"AppBundle\Enum\UserProjectUserType", "getChoices"}, message="Enter a valid project user type.")
     * @ORM\Column(type="integer", nullable=false)
     * @see UserProjectUserType
     */
    protected $userType = UserProjectUserType::OWNER;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set userType
     *
     * @param integer $userType
     *
     * @return UserProject
     */
    public function setUserType($userType)
    {
        $this->userType = $userType;

        return $this;
    }

    /**
     * Get userType
     *
     * @return integer
     */
    public function getUserType()
    {
        return $this->userType;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserProject
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return UserProject
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        $project->addUserProject($this);

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
