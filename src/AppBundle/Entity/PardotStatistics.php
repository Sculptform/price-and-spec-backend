<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Entity;

use Gedmo\Timestampable\Traits\TimestampableEntity;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Class PardotStatistics
 *
 * @ORM\Entity()
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Entity
 */
class PardotStatistics
{
    use TimestampableEntity;

    /**
     * PardotStatistics constructor.
     *
     * @param User|null    $user
     * @param Project|null $project
     * @param string|null  $actionType
     */
    public function __construct(
        User $user = null, Project $project = null, string $actionType = null
    ) {
        if ($user) {
            $this->email     = $user->getEmail();
            $this->firstName = $user->getFirstName();
            $this->lastName  = $user->getLastName();
            $this->username  = $user->getUsername();
        }

        if ($project) {
            $this->projectId = $project->getId();
        }

        if ($actionType) {
            $this->actionType = $actionType;
        }

        $this->updatedAt  = new \DateTime();
        $this->createdAt  = new \DateTime();
        $this->actionDate = new \DateTime();
    }

    /**
     * ID
     *
     * @var string
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     * @Assert\Uuid
     */
    protected $id;

    /**
     * @var string
     * @ORM\Column(type="pardot_last_action")
     *
     */
    protected $actionType;

    /**
     * User first name
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $firstName;

    /**
     * User last name
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $lastName;

    /**
     * Username
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $username;

    /**
     * Email
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $email;

    /**
     * Project ID
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $projectId;

    /**
     * @var \DateTime
     * @ORM\Column(type="datetime")
     */
    protected $actionDate;

    /**
     * Get id.
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set actionType.
     *
     * @param string $actionType
     *
     * @return PardotStatistics
     */
    public function setActionType($actionType)
    {
        $this->actionType = $actionType;

        return $this;
    }

    /**
     * Get actionType.
     *
     * @return string
     */
    public function getActionType()
    {
        return $this->actionType;
    }

    /**
     * Set firstName.
     *
     * @param string|null $firstName
     *
     * @return PardotStatistics
     */
    public function setFirstName($firstName = null)
    {
        $this->firstName = $firstName;

        return $this;
    }

    /**
     * Get firstName.
     *
     * @return string|null
     */
    public function getFirstName()
    {
        return $this->firstName;
    }

    /**
     * Set lastName.
     *
     * @param string|null $lastName
     *
     * @return PardotStatistics
     */
    public function setLastName($lastName = null)
    {
        $this->lastName = $lastName;

        return $this;
    }

    /**
     * Get lastName.
     *
     * @return string|null
     */
    public function getLastName()
    {
        return $this->lastName;
    }

    /**
     * Set username.
     *
     * @param string|null $username
     *
     * @return PardotStatistics
     */
    public function setUsername($username = null)
    {
        $this->username = $username;

        return $this;
    }

    /**
     * Get username.
     *
     * @return string|null
     */
    public function getUsername()
    {
        return $this->username;
    }

    /**
     * Set email.
     *
     * @param string|null $email
     *
     * @return PardotStatistics
     */
    public function setEmail($email = null)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email.
     *
     * @return string|null
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set projectId.
     *
     * @param string|null $projectId
     *
     * @return PardotStatistics
     */
    public function setProjectId($projectId = null)
    {
        $this->projectId = $projectId;

        return $this;
    }

    /**
     * Get projectId.
     *
     * @return string|null
     */
    public function getProjectId()
    {
        return $this->projectId;
    }

    /**
     * Set actionDate.
     *
     * @param \DateTime $actionDate
     *
     * @return PardotStatistics
     */
    public function setActionDate($actionDate)
    {
        $this->actionDate = $actionDate;

        return $this;
    }

    /**
     * Get actionDate.
     *
     * @return \DateTime
     */
    public function getActionDate()
    {
        return $this->actionDate;
    }
}
