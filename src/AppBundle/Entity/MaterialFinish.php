<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:51
 */

namespace AppBundle\Entity;

use AppBundle\Enum\ApplicationType;
use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialFinishRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialFinish
{
    use ApplicationTypeTrait;
    /**
     * Material finish id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Color
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $color;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $textureTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $textureFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $texturePath;

    /**
     * @var MaterialType
     *
     * @ORM\ManyToOne(targetEntity="MaterialType", inversedBy="materialFinishes", cascade={"persist"})
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id",  onDelete="SET NULL")
     * @JMS\Exclude
     */
    protected $materialType;

    /**
     * @var MaterialFinishGroup
     *
     * @ORM\ManyToOne(targetEntity="MaterialFinishGroup", inversedBy="materialFinishes", cascade={"persist"})
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id",  onDelete="SET NULL")
     * @JMS\Exclude
     */
    protected $group;

    /**
     * Material finish application types
     *
     * @var array
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\ApplicationType", "getChoices"}, multiple=true, message="Enter a valid application types.")
     * @ORM\Column(type="simple_array", nullable=false)
     * @see ApplicationType
     */
    protected $applicationTypes;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Product", mappedBy="materialFinish", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $products;

    /**
     * Sort Order
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $orderWeight = -1;

    /**
     * @return string
     *
     * @JMS\VirtualProperty
     * @JMS\SerializedName("group_title")
     */
    public function getGroupTitle()
    {
        return ($this->group)
            ? $this->group->getTitle()
            : '';
    }
    
    /**
     * Returns sort order
     *
     * @return integer
     */
    public function getOrderWeight(): int
    {
        return $this->orderWeight;
    }

    /**
     * Sets sort order
     *
     * @param int $orderWeight
     *
     * @return MaterialFinish
     */
    public function setOrderWeight(int $orderWeight): MaterialFinish
    {
        $this->orderWeight = $orderWeight;

        return $this;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Application Type Label
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getApplicationLabel(){
        return $this->getLabel($this->applicationTypes);
    }

    /**
     * Texture Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getTextureWebPath()
    {
        if ($this->texturePath && is_file($this->getTextureAbsolutePath())) {
            return $this->getTextureUploadDir().'/'.$this->texturePath;
        }

        return null;
    }

    /**
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterialType()->getId();
    }

    /***********************************************/

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->products = new ArrayCollection();
        $this->applicationTypes = [];
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MaterialFinish
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getTextureAbsolutePath()
    {
        return $this->texturePath ? $this->getTextureUploadRootDir().'/'.$this->texturePath : null;
    }

    protected function getTextureUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getTextureUploadDir();
    }

    protected function getTextureUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/materialShapeFinishes';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     * @return MaterialFinish
     */
    public function setTextureFile(UploadedFile $file = null)
    {
        $this->textureFile = $file;

        // check if we have an old image path
        if (is_file($this->getTextureAbsolutePath())) {
            // store the old name to delete after the update
            $this->textureTemp = $this->getTextureAbsolutePath();
        }

        $this->preUploadTexture();

        return $this;
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getTextureFile()
    {
        return $this->textureFile;
    }

    public function preUploadTexture()
    {
        if (null !== $this->getTextureFile()) {
            $this->texturePath = FileUtil::generateFilename($this->getTextureFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadTexture()
    {
        if (null === $this->getTextureFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->textureTemp)) {
            // delete the old image
            unlink($this->textureTemp);
            // clear the temp image path
            $this->textureTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getTextureFile()->move(
            $this->getTextureUploadRootDir(),
            $this->texturePath
        );

        $this->setTextureFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeTextureFilenameForRemove()
    {
        $this->textureTemp = $this->getTextureAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeTextureUpload()
    {
        if (isset($this->textureTemp)) {
            @unlink($this->textureTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $texturePath
     * @return $this
     */
    public function setTexturePath($texturePath)
    {
        $this->texturePath = $texturePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getTexturePath()
    {
        return $this->texturePath;
    }

    /**
     * Set applicationTypes
     *
     * @param array $applicationTypes
     *
     * @return MaterialFinish
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;

        return $this;
    }

    /**
     * Get applicationTypes
     *
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * Set materialType
     *
     * @param MaterialType $materialType
     *
     * @return MaterialFinish
     */
    public function setMaterialType(MaterialType $materialType = null)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return MaterialFinish
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * Add product
     *
     * @param Product $product
     *
     * @return MaterialFinish
     */
    public function addProduct(Product $product)
    {
        $this->products[] = $product;

        return $this;
    }

    /**
     * Remove product
     *
     * @param Product $product
     */
    public function removeProduct(Product $product)
    {
        $this->products->removeElement($product);
    }

    /**
     * Get products
     *
     * @return Collection|Product[]
     */
    public function getProducts()
    {
        return $this->products;
    }

    /**
     * Set group
     *
     * @param MaterialFinishGroup $group
     *
     * @return MaterialFinish
     */
    public function setGroup(MaterialFinishGroup $group = null)
    {
        $this->group = $group;

        return $this;
    }

    /**
     * Get group
     *
     * @return MaterialFinishGroup
     */
    public function getGroup()
    {
        return $this->group;
    }

    public function __toString()
    {
        return (string)$this->getTitle();
    }

    /**
     * @param bool $default
     * @return bool|int|number
     */
    public function getDecimalColor($default = false)
    {
        $origColor = $default ? 'default' : strtolower($this->getColor());

        if (!$origColor) {
            return 16777215;
        }

        $color = $this->getColorByKeyword($origColor);

        return false === $color ? hexdec(str_replace('#', '', $origColor)) : $color;
    }

    /**
     * @param $colorKeyword
     * @return bool|int
     */
    private function getColorByKeyword($colorKeyword) {
        $colors = [
            'aliceblue' => 15792383,
            'antiquewhite' => 16444375,
            'aqua' => 65535,
            'aquamarine' => 8388564,
            'azure' => 15794175,
            'beige' => 16119260,
            'bisque' => 16770244,
            'black' => 0,
            'blanchedalmond' => 16772045,
            'blue' => 255,
            'blueviolet' => 9055202,
            'brown' => 10824234,
            'burlywood' => 14596231,
            'cadetblue' => 6266528,
            'chartreuse' => 8388352,
            'chocolate' => 13789470,
            'coral' => 16744272,
            'cornflowerblue' => 6591981,
            'cornsilk' => 16775388,
            'crimson' => 14423100,
            'cyan' => 65535,
            'darkblue' => 139,
            'darkcyan' => 35723,
            'darkgoldenrod' => 12092939,
            'darkgray' => 11119017,
            'darkgreen' => 25600,
            'darkgrey' => 11119017,
            'darkkhaki' => 12433259,
            'darkmagenta' => 9109643,
            'darkolivegreen' => 5597999,
            'darkorange' => 16747520,
            'darkorchid' => 10040012,
            'darkred' => 9109504,
            'darksalmon' => 15308410,
            'darkseagreen' => 9419919,
            'darkslateblue' => 4734347,
            'darkslategray' => 3100495,
            'darkslategrey' => 3100495,
            'darkturquoise' => 52945,
            'darkviolet' => 9699539,
            'deeppink' => 16716947,
            'deepskyblue' => 49151,
            'dimgray' => 6908265,
            'dimgrey' => 6908265,
            'dodgerblue' => 2003199,
            'firebrick' => 11674146,
            'floralwhite' => 16775920,
            'forestgreen' => 2263842,
            'fuchsia' => 16711935,
            'gainsboro' => 14474460,
            'ghostwhite' => 16316671,
            'gold' => 16766720,
            'goldenrod' => 14329120,
            'gray' => 8421504,
            'green' => 32768,
            'greenyellow' => 11403055,
            'grey' => 8421504,
            'honeydew' => 15794160,
            'hotpink' => 16738740,
            'indianred' => 13458524,
            'indigo' => 4915330,
            'ivory' => 16777200,
            'khaki' => 15787660,
            'lavender' => 15132410,
            'lavenderblush' => 16773365,
            'lawngreen' => 8190976,
            'lemonchiffon' => 16775885,
            'lightblue' => 11393254,
            'lightcoral' => 15761536,
            'lightcyan' => 14745599,
            'lightgoldenrodyellow' => 16448210,
            'lightgray' => 13882323,
            'lightgreen' => 9498256,
            'lightgrey' => 13882323,
            'lightpink' => 16758465,
            'lightsalmon' => 16752762,
            'lightseagreen' => 2142890,
            'lightskyblue' => 8900346,
            'lightslategray' => 7833753,
            'lightslategrey' => 7833753,
            'lightsteelblue' => 11584734,
            'lightyellow' => 16777184,
            'lime' => 65280,
            'limegreen' => 3329330,
            'linen' => 16445670,
            'magenta' => 16711935,
            'maroon' => 8388608,
            'mediumaquamarine' => 6737322,
            'mediumblue' => 205,
            'mediumorchid' => 12211667,
            'mediumpurple' => 9662683,
            'mediumseagreen' => 3978097,
            'mediumslateblue' => 8087790,
            'mediumspringgreen' => 64154,
            'mediumturquoise' => 4772300,
            'mediumvioletred' => 13047173,
            'midnightblue' => 1644912,
            'mintcream' => 16121850,
            'mistyrose' => 16770273,
            'moccasin' => 16770229,
            'navajowhite' => 16768685,
            'navy' => 128,
            'oldlace' => 16643558,
            'olive' => 8421376,
            'olivedrab' => 7048739,
            'orange' => 16753920,
            'orangered' => 16729344,
            'orchid' => 14315734,
            'palegoldenrod' => 15657130,
            'palegreen' => 10025880,
            'paleturquoise' => 11529966,
            'palevioletred' => 14381203,
            'papayawhip' => 16773077,
            'peachpuff' => 16767673,
            'peru' => 13468991,
            'pink' => 16761035,
            'plum' => 14524637,
            'powderblue' => 11591910,
            'purple' => 8388736,
            'red' => 16711680,
            'rosybrown' => 12357519,
            'royalblue' => 4286945,
            'saddlebrown' => 9127187,
            'salmon' => 16416882,
            'sandybrown' => 16032864,
            'seagreen' => 3050327,
            'seashell' => 16774638,
            'sienna' => 10506797,
            'silver' => 12632256,
            'skyblue' => 8900331,
            'slateblue' => 6970061,
            'slategray' => 7372944,
            'slategrey' => 7372944,
            'snow' => 16775930,
            'springgreen' => 65407,
            'steelblue' => 4620980,
            'tan' => 13808780,
            'teal' => 32896,
            'thistle' => 14204888,
            'tomato' => 16737095,
            'turquoise' => 4251856,
            'violet' => 15631086,
            'wheat' => 16113331,
            'white' => 16777215,
            'whitesmoke' => 16119285,
            'yellow' => 16776960,
            'yellowgreen' => 10145074,
            'default' => 14606046
        ];

        return array_key_exists($colorKeyword, $colors) ? $colors[$colorKeyword] : false;
    }
}
