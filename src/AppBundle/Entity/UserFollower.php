<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Notification\UserFollowerNotification;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity as UniqueEntity;

/**
 * UserFollower
 *
 * @ORM\Table(name="user_follower")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\UserFollowerRepository")
 * @UniqueEntity(
 *     fields={"follower", "user"},
 *     errorPath="follower",
 *     message="This follower is already in use."
 * )
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 */
class UserFollower
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", inversedBy="followers")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $user;

    /**
     * @var User
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User", cascade={"persist"}, inversedBy="followings")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="follower_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $follower;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Notification\UserFollowerNotification", mappedBy="userFollower", cascade={"remove"}, fetch="EXTRA_LAZY")
     */
    protected $notifications;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set user
     *
     * @param User $user
     *
     * @return UserFollower
     */
    public function setUser(User $user)
    {
        $this->user = $user;

        return $this;
    }

    /**
     * Get user
     *
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * Set follower
     *
     * @param User $follower
     *
     * @return UserFollower
     */
    public function setFollower(User $follower)
    {
        $this->follower = $follower;

        return $this;
    }

    /**
     * Get follower
     *
     * @return User
     */
    public function getFollower()
    {
        return $this->follower;
    }

    /**
     * @return Collection
     */
    public function getNotifications()
    {
        return $this->notifications;
    }

    /**
     * @param Collection $notifications
     */
    public function setNotifications($notifications)
    {
        $this->notifications = $notifications;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->notifications = new ArrayCollection();
    }

    /**
     * Add notification
     *
     * @param UserFollowerNotification $notification
     *
     * @return UserFollower
     */
    public function addNotification(UserFollowerNotification $notification)
    {
        $this->notifications[] = $notification;

        return $this;
    }

    /**
     * Remove notification
     *
     * @param UserFollowerNotification $notification
     */
    public function removeNotification(UserFollowerNotification $notification)
    {
        $this->notifications->removeElement($notification);
    }
}
