<?php

namespace AppBundle\Entity;

use AppBundle\Validator\Constraints\YoutubeVimeoConstraint;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\VideoTutorialRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 *
 */
class VideoTutorial
{
    use TimestampableEntity;

    /**
     * Tutorial id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Description
     *
     * @var string
     *
     * @Assert\NotBlank()
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $description;


    /**
     * @ORM\OneToOne(targetEntity="Application\Sonata\MediaBundle\Entity\Media", cascade={"persist"})
     * @ORM\JoinColumn(name="video_id", referencedColumnName="id")
     */
    protected $video;

    /**
     * YouTube Url
     *
     * @YoutubeVimeoConstraint()
     *
     * @var string
     *
     * @ORM\Column(type="string", nullable=true)
     */
    protected $youTubeUrl;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return VideoTutorial
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }


    public function setVideo($video)
    {
        $this->video = $video;

        return $this;
    }

    public function getVideo()
    {
        return $this->video;
    }

    /**
     * Set description
     *
     * @param string $description
     *
     * @return VideoTutorial
     */
    public function setDescription($description)
    {
        $this->description = $description;

        return $this;
    }

    /**
     * Get description
     *
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * Get YouTube Url
     *
     * @return string
     */
    public function getYouTubeUrl()
    {
        return $this->youTubeUrl;
    }

    /**
     * Set YouTube Url
     *
     * @param string $youTubeUrl
     *
     * @return VideoTutorial
     */
    public function setYouTubeUrl( $youTubeUrl )
    {
        $this->youTubeUrl = $youTubeUrl;

        return $this;
    }


}
