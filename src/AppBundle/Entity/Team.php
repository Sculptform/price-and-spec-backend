<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Comment\TeamComment;
use AppBundle\Enum\TeamAccessType;
use AppBundle\Enum\TeamUserStatus;
use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * Team
 *
 * @ORM\Table(name="team")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TeamRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class Team
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Mission
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $mission;

    /**
     * Description
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $description;

    /**
     * @var Collection
     *
     * @JMS\Groups({"Show", "Create", "Update"})
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\TeamUser", mappedBy="team", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $teamUsers;
    
    /**
     * @var User
     *
     * @JMS\Groups({"Show", "List"})
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="admin_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $admin;

    /**
     * @var Collection
     *
     * @JMS\Groups({"Show", "Update"})
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", inversedBy="teams", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="team_projects")
     */
    protected $projects;

    /**
     * @var Collection
     *
     * @JMS\Groups({"Show", "Create", "Update"})
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image\TeamImage", mappedBy="team", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $images;

    /**
     * Access Type
     *
     * @var int
     * @Assert\Choice(callback = {"AppBundle\Enum\TeamAccessType", "getChoices"}, message="Enter a valid access type.")
     * @JMS\Groups({"create", "Update", "Show", "List"})
     * @ORM\Column(type="integer", nullable=false)
     * @see TeamAccessType
     */
    protected $accessType = TeamAccessType::PRIVATE_ACCESS;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $coverImageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $coverImageFile;

    /**
     * @ORM\Column(type="string", length=255, nullable=true, name="cover_image_path")
     * @JMS\Exclude
     */
    protected $coverImagePath;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment\TeamComment", mappedBy="team", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @JMS\Groups({"Show", "List"})
     * @JMS\Exclude
     */
    protected $comments;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->teamUsers  = new ArrayCollection();
        $this->projects   = new ArrayCollection();
        $this->images = new ArrayCollection();
        $this->comments = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Update", "Show", "List", "Create"})
     * @JMS\Type("string")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/teams';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    public function getCoverImageAbsolutePath()
    {
        return $this->coverImagePath ? $this->getImageUploadRootDir().'/'.$this->coverImagePath : null;
    }

    /**
     * Cover Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @JMS\Type("string")
     * @return null|string
     */
    public function getCoverImageWebPath()
    {
        if ($this->coverImagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->coverImagePath;
        }

        return null;
    }

    /**
     * Sets cover image file.
     *
     * @param UploadedFile $file
     */
    public function setCoverImageFile(UploadedFile $file = null)
    {
        $this->coverImageFile = $file;

        // check if we have an old image path
        if (is_file($this->getCoverImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->coverImageTemp = $this->getCoverImageAbsolutePath();
        }

        $this->preUploadCoverImage();
    }

    /**
     * Get cover image file.
     *
     * @return UploadedFile
     */
    public function getCoverImageFile()
    {
        return $this->coverImageFile;
    }

    public function preUploadCoverImage()
    {
        if (null !== $this->getCoverImageFile()) {
            $this->coverImagePath = FileUtil::generateFilename($this->getCoverImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadCoverImage()
    {
        if (null === $this->getCoverImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->coverImageTemp)) {
            // delete the old image
            unlink($this->coverImageTemp);
            // clear the temp image path
            $this->coverImageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getCoverImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->coverImagePath
        );

        $this->setCoverImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeCoverImageFilenameForRemove()
    {
        $this->coverImageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeCoverImageUpload()
    {
        if (isset($this->coverImageTemp)) {
            unlink($this->coverImageTemp);
        }
    }

    /**
     * Set coverImagePath
     *
     * @param string $imagePath
     * @return $this
     */
    public function setCoverImagePath($imagePath)
    {
        $this->coverImagePath = $imagePath;

        return $this;
    }

    /**
     * Get coverImagePath
     *
     * @return string
     */
    public function getCoverImagePath()
    {
        return $this->coverImagePath;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return int
     */
    public function getAccessType()
    {
        return $this->accessType;
    }

    /**
     * @param int $accessType
     */
    public function setAccessType($accessType)
    {
        $this->accessType = $accessType;
    }

    /**
     * Add project
     *
     * @param Project $project
     *
     * @return Team
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /**
     * Set admin
     *
     * @param User $admin
     *
     * @return Team
     */
    public function setAdmin(User $admin)
    {
        $this->admin = $admin;

        return $this;
    }

    /**
     * Get admin
     *
     * @return User
     */
    public function getAdmin()
    {
        return $this->admin;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Comments count
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @return int
     */
    public function getCommentsCount()
    {
        return $this->comments->count();
    }

    /**
     * Team Users ids
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show"})
     * @return array
     */
    public function getUsersIds()
    {
        $ids   = [];

        /** @var TeamUser $teamUser */
        foreach ($this->getTeamUsers() as $teamUser) {
            if ($teamUser->getStatus() == TeamUserStatus::CONFIRMED)
                $ids[] = $teamUser->getUser()->getId();
        }

        return $ids;
    }

    /**
     * Team Confirm Users
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show"})
     * @return array
     */
    public function getConfirmUsers()
    {
        $users = [];

        /** @var TeamUser $teamUser */
        foreach ($this->getTeamUsers() as $teamUser) {
            if ($teamUser->getStatus() == TeamUserStatus::CONFIRMED) {
                $users[] = $teamUser->getUser();
            }
        }

        return $users;
    }

    /**
     * Team Projects ids
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @JMS\Type("array")
     * @return array
     */
    public function getProjectsIds()
    {
        return $this->getProjects()->map(function(Project $project){ return $project->getId(); })->toArray();
    }

    /**
     * Images ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @return array
     */
    public function getImagesIds()
    {
        return $this->getImages()->map(function(Image\TeamImage $teamImage){ return $teamImage->getId(); })->toArray();
    }

    /**
     * Admin id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getAdminId()
    {
        return $this->getAdmin()->getId();
    }

    /***********************************************/

    /**
     * Add teamUser
     *
     * @param TeamUser $teamUser
     *
     * @return Team
     */
    public function addTeamUser(TeamUser $teamUser)
    {
        $this->teamUsers[] = $teamUser;

        return $this;
    }

    /**
     * Remove teamUser
     *
     * @param TeamUser $teamUser
     */
    public function removeTeamUser(TeamUser $teamUser)
    {
        $this->teamUsers->removeElement($teamUser);
    }

    /**
     * Get teamUsers
     *
     * @return Collection
     */
    public function getTeamUsers()
    {
        return $this->teamUsers;
    }

    /**
     * @return string
     */
    public function getMission()
    {
        return $this->mission;
    }

    /**
     * @param string $mission
     */
    public function setMission($mission)
    {
        $this->mission = $mission;
    }

    /**
     * Add Image
     *
     * @param Image\TeamImage $image
     *
     * @return Team
     */
    public function addImage(Image\TeamImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param Image\TeamImage $image
     */
    public function removeImage(Image\TeamImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add comment
     *
     * @param TeamComment $comment
     *
     * @return Team
     */
    public function addComment(TeamComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param TeamComment $comment
     */
    public function removeComment(TeamComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }
}
