<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:43
 */

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialShapeSizeRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialShapeSize
{
    /**
     * Material Shape Size id
     * 
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var MaterialShape
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\MaterialShape", inversedBy="materialShapeSizes", cascade={"persist"})
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="material_shape_id", referencedColumnName="id", nullable=false)
     * })
     */
    protected $materialShape;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * Width
     * 
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    protected $width;

    /**
     * Depth
     * 
     * @var float
     * @ORM\Column(type="float", nullable=false)
     */
    protected $depth;

    /**
     * Cover Width
     *
     * @var float
     * @ORM\Column(type="float", nullable=true)
     */
    protected $coverWidth;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Material", mappedBy="materialShapeSize", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $materials;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Material Shape id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getMaterialShapeId()
    {
        return $this->getMaterialShape()->getId();
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    /***********************************************/

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->materials = new ArrayCollection();
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/materialShapeSizes';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            if (is_file($this->imageTemp)) {
                unlink($this->imageTemp);
            }

            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set width
     *
     * @param float $width
     *
     * @return MaterialShapeSize
     */
    public function setWidth($width)
    {
        $this->width = $width;

        return $this;
    }

    /**
     * Get width
     *
     * @return float
     */
    public function getWidth()
    {
        return $this->width;
    }

    /**
     * Set depth
     *
     * @param float $depth
     *
     * @return MaterialShapeSize
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;

        return $this;
    }

    /**
     * Get depth
     *
     * @return float
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * Set materialShape
     *
     * @param MaterialShape $materialShape
     *
     * @return MaterialShapeSize
     */
    public function setMaterialShape(MaterialShape $materialShape)
    {
        $this->materialShape = $materialShape;

        return $this;
    }

    /**
     * Get materialShape
     *
     * @return MaterialShape
     */
    public function getMaterialShape()
    {
        return $this->materialShape;
    }

    /**
     * Add material
     *
     * @param Material $material
     *
     * @return MaterialShapeSize
     */
    public function addMaterial(Material $material)
    {
        $this->materials[] = $material;

        return $this;
    }

    /**
     * Remove material
     *
     * @param Material $material
     */
    public function removeMaterial(Material $material)
    {
        $this->materials->removeElement($material);
    }

    /**
     * Get materials
     *
     * @return Collection
     */
    public function getMaterials()
    {
        return $this->materials;
    }

    /**
     * Set coverWidth
     *
     * @param string $coverWidth
     *
     * @return MaterialShapeSize
     */
    public function setCoverWidth($coverWidth)
    {
        $this->coverWidth = $coverWidth;

        return $this;
    }

    /**
     * Get coverWidth
     *
     * @return string
     */
    public function getCoverWidth()
    {
        return $this->coverWidth;
    }

    public function __toString()
    {
        return sprintf('%d (w) x %d (d)', $this->width, $this->depth);
    }
}
