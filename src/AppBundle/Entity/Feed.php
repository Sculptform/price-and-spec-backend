<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Comment\FeedComment;
use AppBundle\Entity\SharedContent\SharedContent;
use AppBundle\Enum\FeedAccessType;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * Feed
 *
 * @ORM\Table(name="feed")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FeedRepository")
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 */
class Feed
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * Content
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show", "List", "Share"})
     * @ORM\Column(type="string", nullable=false)
     */
    protected $content;

    /**
     * Access Type
     *
     * @var int
     * @Assert\Choice(callback = {"AppBundle\Enum\FeedAccessType", "getChoices"}, message="Enter a valid access type.")
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="integer", nullable=false)
     * @see FeedAccessType
     */
    protected $accessType = FeedAccessType::PUBLIC_ACCESS;

    /**
     * @var User
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\User")
     * @ORM\JoinColumns({
     *   @ORM\JoinColumn(name="user_id", referencedColumnName="id", nullable=false)
     * })
     * @JMS\Groups({"Create", "Show", "List"})
     */
    protected $user;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="likedFeeds", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="feed_liked_users")
     */
    protected $likedUsers;

    /**
     * @var Collection
     *
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\Project", inversedBy="feeds", fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="feed_projects")
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     */
    protected $projects;

    /**
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\SharedContent\SharedContent", cascade={"all"})
     * @ORM\JoinColumn(name="shared_content_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @JMS\Type("AppBundle\Entity\SharedContent\SharedContent")
     */
    protected $sharedContent;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image\FeedImage", mappedBy="feed", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @JMS\Groups({"Show", "List", "Share"})
     */
    protected $images;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment\FeedComment", mappedBy="feed", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @JMS\Groups({"Show", "List"})
     */
    protected $comments;

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * @param string $content
     */
    public function setContent($content)
    {
        $this->content = $content;
    }

    /**
     * @return User
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param User $user
     */
    public function setUser($user)
    {
        $this->user = $user;
    }
    
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->projects   = new ArrayCollection();
        $this->likedUsers = new ArrayCollection();
        $this->images     = new ArrayCollection();
        $this->comments   = new ArrayCollection();
    }

    /**
     * Add project
     *
     * @param Project $project
     *
     * @return Feed
     */
    public function addProject(Project $project)
    {
        $this->projects[] = $project;

        return $this;
    }

    /**
     * Remove project
     *
     * @param Project $project
     */
    public function removeProject(Project $project)
    {
        $this->projects->removeElement($project);
    }

    /**
     * Get projects
     *
     * @return Collection
     */
    public function getProjects()
    {
        return $this->projects;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Users ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @JMS\Groups("List")
     * @return array
     */
    public function getLikedUserIds()
    {
        return $this->getLikedUsers()->map(function(User $user){ return $user->getId(); })->toArray();
    }

    /***********************************************/

    /**
     * Add feedImage
     *
     * @param Image\FeedImage $image
     *
     * @return Feed
     */
    public function addImage(Image\FeedImage $image)
    {
        $this->images[] = $image;

        return $this;
    }

    /**
     * Remove image
     *
     * @param Image\FeedImage $image
     */
    public function removeImage(Image\FeedImage $image)
    {
        $this->images->removeElement($image);
    }

    /**
     * Get images
     *
     * @return Collection
     */
    public function getImages()
    {
        return $this->images;
    }

    /**
     * Add likedUser
     *
     * @param User $likedUser
     *
     * @return Feed
     */
    public function addLikedUser(User $likedUser)
    {
        if (!$this->likedUsers->contains($likedUser)) {
            $this->likedUsers[] = $likedUser;
        }

        return $this;
    }

    /**
     * Remove likedUser
     *
     * @param User $likedUser
     */
    public function removeLikedUser(User $likedUser)
    {
        $this->likedUsers->removeElement($likedUser);
    }

    /**
     * Get likedUsers
     *
     * @return Collection
     */
    public function getLikedUsers()
    {
        return $this->likedUsers;
    }

    /**
     * Set accessType
     *
     * @param integer $accessType
     *
     * @return Feed
     */
    public function setAccessType($accessType)
    {
        $this->accessType = $accessType;

        return $this;
    }

    /**
     * Get accessType
     *
     * @return integer
     */
    public function getAccessType()
    {
        return $this->accessType;
    }

    /**
     * Add comment
     *
     * @param FeedComment $comment
     *
     * @return Feed
     */
    public function addComment(FeedComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param FeedComment $comment
     */
    public function removeComment(FeedComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * @param SharedContent|null $sharedContent
     * @return $this
     */
    public function setSharedContent($sharedContent = null)
    {
        $reflect = new \ReflectionClass($sharedContent);
        $class   = $reflect->getNamespaceName().'\SharedContent\Shared'.$reflect->getShortName();
        $setter  = 'set'.$reflect->getShortName();

        if(class_exists($class)){
            $this->sharedContent = new $class;
            if(method_exists($this->sharedContent, $setter)){
                $this->sharedContent->$setter($sharedContent);
            }else{
                $this->sharedContent = null;
            }
        }

        return $this;
    }

    /**
     * Get sharedContent
     *
     * @return SharedContent
     */
    public function getSharedContent()
    {
        return $this->sharedContent;
    }
}
