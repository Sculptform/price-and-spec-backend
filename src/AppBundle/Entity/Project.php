<?php

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\ORM\Mapping as ORM;
use AppBundle\Enum\ProjectStatus;
use AppBundle\Enum\ApplicationType;
use JMS\Serializer\Annotation as JMS;
use AppBundle\Enum\UserProjectUserType;
use Doctrine\Common\Collections\Collection;
use AppBundle\Entity\Comment\ProjectComment;
use Doctrine\Common\Collections\ArrayCollection;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use AppBundle\Entity\Notification\ShareNotification;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\ProjectRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Project
{
    use TimestampableEntity;
    use ApplicationTypeTrait;

    const SQUARE_METRE = 10.7639;
    const NZD_ADDITION_PERCENTAGE = 10;

    public static $sqFtPricingCategory
        = [
            '0-100'   => '0-1100',
            '100-300' => '1100-3200',
            '300+'    => '3200+',
        ];

    /**
     * Project id
     *
     * @var string
     * @JMS\Expose
     * @JMS\Groups({"Show", "List", "Export"})
     * @ORM\Id
     * @ORM\Column(type="guid")
     * @ORM\GeneratedValue(strategy="UUID")
     * @Assert\Uuid
     */
    protected $id;

    /**
     * Is template project?
     *
     * @var string
     * @JMS\Expose
     * @JMS\Groups({"Show", "List", "Share"})
     * @ORM\Column(type="boolean", nullable=false, options={"default": false})
     */
    protected $isTemplate = false;

    /**
     * Title
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Expose
     * @JMS\Groups({"Show", "List", "Share"})
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Application type
     *
     * @var integer
     * @JMS\Expose
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="integer", nullable=true)
     * @see ApplicationType
     */
    protected $applicationType = ApplicationType::INTERIOR;

    /**
     * @var ProductType
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getProductTypeId")
     * @Assert\NotBlank()
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="projects", cascade={"persist"})
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id", nullable=false)
     */
    protected $productType;

    /**
     * Status
     *
     * @JMS\Expose
     * @JMS\Groups({"Show", "List"})
     * @var integer
     * @Assert\Choice(
     *     callback = {"AppBundle\Enum\ProjectStatus", "getChoices"},
     *     message="Enter a valid status."
     * )
     * @ORM\Column(type="integer", nullable=false)
     * @see ProjectStatus
     */
    protected $status = ProjectStatus::ACTIVE;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="UserProject", mappedBy="project", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $userProjects;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\Notification\ShareNotification",
     *     mappedBy="project", fetch="EXTRA_LAZY"
     * )
     */
    protected $shareNotifications;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="Quotation", mappedBy="project", fetch="EXTRA_LAZY")
     */
    protected $quotations;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="SampleRequest", mappedBy="project", fetch="EXTRA_LAZY")
     */
    protected $sampleRequests;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToMany(targetEntity="Team", mappedBy="projects", fetch="EXTRA_LAZY")
     */
    protected $teams;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToMany(targetEntity="ProjectFilter")
     * @ORM\JoinTable(name="project_filters",
     *     joinColumns={@ORM\JoinColumn(name="project_id", referencedColumnName="id")},
     *     inverseJoinColumns={@ORM\JoinColumn(name="filter_id", referencedColumnName="id")}
     * )
     */
    protected $filters;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\ManyToMany(targetEntity="Feed", mappedBy="projects", fetch="EXTRA_LAZY")
     */
    protected $feeds;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Comment\ProjectComment", mappedBy="project", fetch="EXTRA_LAZY")
     * @ORM\OrderBy({"createdAt" = "DESC"})
     *
     * @JMS\Groups({"Show", "List"})
     * @JMS\Exclude
     */
    protected $comments;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @ORM\OneToMany(targetEntity="ProjectRatingUser", mappedBy="project", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $projectRatingUsers;

    /**
     * @var Collection
     *
     * @JMS\Expose
     * @JMS\Groups("Show")
     *
     * @ORM\OneToMany(targetEntity="ProjectProduct", mappedBy="project", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $projectProducts;

    /**
     * @var Collection
     *
     * @JMS\Expose
     * @JMS\Groups("Show")
     *
     * @ORM\OneToMany(targetEntity="FintraxProduct", mappedBy="project", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $fintraxProducts;

    /**
     * @var Collection
     *
     * @JMS\Expose
     * @JMS\Groups("Show")
     *
     * @ORM\OneToMany(
     *     targetEntity="ExpressionCladdingProduct", mappedBy="project",
     *     cascade={"persist"}, fetch="EXTRA_LAZY"
     * )
     */
    protected $expressionCladdingProducts;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OrderBy({"createdAt" = "DESC"})
     * @ORM\OneToMany(targetEntity="Scene", mappedBy="project", cascade={"persist"}, fetch="EXTRA_LAZY")
     */
    protected $scenes;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(targetEntity="GalleryProject", mappedBy="project",cascade={"all"}, fetch="EXTRA_LAZY")
     */
    protected $galleryProjects;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * @var integer
     * @ORM\Column(type="integer", nullable=true, name="views",
     *   options={"default" : 0})
     * @JMS\Groups("List")
     */
    protected $views;

    /**
     * Project is public?
     *
     * @var boolean
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="boolean", length=255, nullable=false)
     */
    protected $public = false;

    /**
     * Project is outdated?
     *
     * @var boolean
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $outdated = false;

    /**
     * @var float
     * @Assert\NotBlank()
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="float", nullable=false)
     */
    protected $price;

    /**
     * @var float
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="float", nullable=true)
     */
    protected $wholesalePrice;

    /**
     * @var float
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="float", nullable=true)
     */
    protected $retailPrice;

    /**
     * @var float
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="float", nullable=true)
     */
    protected $sequenceRatio;

    /**
     * Is snap value locked?
     *
     * @var boolean
     * @JMS\Expose
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="boolean", nullable=false, options={"default": true})
     */
    protected $isSnapToAll = true;

    /**
     * Snap value
     *
     * @var integer
     * @JMS\Groups({"Show", "List"})
     * @ORM\Column(type="integer", nullable=true)
     */
    protected $snap;

    /**
     * @var Collection
     *
     * @JMS\Groups({"Show", "List"})
     * @ORM\ManyToMany(targetEntity="MaterialCoating", cascade={"persist"},
     *   fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="project_material_coatings")
     * @JMS\Exclude
     */
    protected $materialCoatings;

    /**
     * @var AcousticBacking
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getAcousticBackingId")
     * @ORM\ManyToOne(targetEntity="AcousticBacking", cascade={"persist"})
     * @ORM\JoinColumn(name="acoustic_backing_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $acousticBacking;

    /**
     * @var AcousticRating
     *
     * @JMS\Type("AppBundle\Entity\AcousticRating")
     * @JMS\SerializedName("acoustic_rating")
     * @ORM\OneToOne(targetEntity="AcousticRating", cascade={"persist"})
     * @ORM\JoinColumn(name="acoustic_rating_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     */
    protected $acousticRating;

    /**
     * @var Railing
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getRailingId")
     * @JMS\SerializedName("railing_id")
     *
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="Railing")
     * @ORM\JoinColumn(name="railing_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Groups({"Show"})
     */
    protected $railing;

    /**
     * @var RailingFinish
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getRailingFinishId")
     * @JMS\SerializedName("railing_finish_id")
     *
     * @Assert\Valid()
     * @ORM\ManyToOne(targetEntity="RailingFinish", fetch="EAGER")
     * @ORM\JoinColumn(name="railing_finish_id", referencedColumnName="id", nullable=true, onDelete="SET NULL")
     * @JMS\Groups({"Show"})
     */
    protected $railingFinish;

    /**
     * @var string
     * @JMS\Expose
     * @ORM\Column(type="string", nullable=true)
     */
    protected $pricingCategory;

    /**
     * @var float|int
     * @JMS\Expose
     * @JMS\SerializedName("total_weight")
     * @ORM\Column(type="float", nullable=true)
     */
    protected $totalWeight;

    /**
     * @var Currency
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Currency")
     * @ORM\JoinColumn(name="currency_id", referencedColumnName="id")
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getCurrencyCode")
     */
    protected $currency;

    /***********************************************
     *                Virtual fields
     ***********************************************/


    /**
     * Application Type Label
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return string
     */
    public function getApplicationLabel()
    {
        $label = $this->getLabel($this->applicationType);

        return $label;
    }

    public function getCurrencyCode()
    {
        return $this->currency->getId();
    }

    /**
     * Material coatings grouped by material type
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array<string, string>")
     * @JMS\SerializedName("material_coatings")
     * @JMS\Groups({"Show", "List"})
     * @return array
     */
    public function getMaterialCoatingsIdsGroupedByMaterialTypeId()
    {
        $coatings = [];

        /** @var MaterialCoating $materialCoating */
        foreach ($this->materialCoatings as $materialCoating) {
            $coatings[$materialCoating->getMaterialTypeId()] = $materialCoating->getId(
            ); // overwrite possible duplicates of materialTypes
        }

        return $coatings;
    }

    /**
     * Comments count
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @return int
     */
    public function getCommentsCount()
    {
        return $this->comments->count();
    }

    /**
     * Project count comments
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getCountLikeComments()
    {
        $count = 0;

        /** @var ProjectComment $comment */
        foreach ($this->getComments() as $comment) {
            $count += $comment->getLikedUsers()->count();
        }

        return $count;
    }

    /**
     * Project rating
     *
     * @JMS\VirtualProperty
     * @JMS\Type("float")
     * @return float
     */
    public function getProjectRating()
    {
        $rating    = 0;
        $userCount = 0;

        /** @var ProjectRatingUser $projectRatingUser */
        foreach ($this->getProjectRatingUsers() as $projectRatingUser) {
            $rating += $projectRatingUser->getRating();
            $userCount++;
        }

        return ! $userCount ? $rating : $rating / $userCount;
    }

    /**
     * Liked materials ids array,
     * todo: hardcoded for now
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array")
     * @return array
     */
    public function getLikedMaterials()
    {
        $likedMaterialsIds = [];

        return $likedMaterialsIds;
    }

    /**
     * Get filters.
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFilters()
    {
        return $this->filters;
    }

    /**
     * @JMS\Type("array")
     * @JMS\VirtualProperty()
     * @JMS\SerializedName("filters")
     */
    public function getProjectFilters()
    {
        $filters = [];
        foreach ($this->filters->toArray() as $filter) {
            /** @var ProjectFilter $filter */
            $filters[] = $filter->getId();
        }

        return $filters;
    }

    /**
     * ProjectProducts ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Groups("List")
     * @JMS\Type("array")
     * @return array
     */
    public function getProjectProductsIds()
    {
        $ids = [];

        foreach ($this->getProjectProducts() as $projectProduct) {
            $ids[] = $projectProduct->getId();
        }

        return $ids;
    }

    /**
     * FintraxProducts ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Groups("List")
     * @JMS\Type("array")
     * @return array
     */
    public function getFintraxProductsIds()
    {
        $ids = [];

        foreach ($this->getFintraxProducts() as $fintraxProduct) {
            $ids[] = $fintraxProduct->getId();
        }

        return $ids;
    }

    /**
     * Expression Cladding Products ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Groups("List")
     * @JMS\Type("array")
     * @return array
     */
    public function getExpressionCladdingProductsIds()
    {
        $ids = [];

        foreach ($this->getExpressionCladdingProducts() as $ecProduct) {
            $ids[] = $ecProduct->getId();
        }

        return $ids;
    }

    /**
     * Shared users ids array
     *
     * @JMS\VirtualProperty
     * @JMS\Type("array<integer>")
     * @return integer[]
     */
    public function getUsersIds()
    {
        $usersIds = [];

        foreach ($this->getSharedUsers() as $user) {
            $usersIds[] = $user->getId();
        }

        return $usersIds;
    }

    /**
     * Last scene Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return int|null
     */
    public function getSceneId()
    {
        /*** @var Scene $scene */
        $scene = $this->getScenes()->first();

        return $scene ? $scene->getId() : null;
    }

    /**
     * Owner user Id for API
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\SerializedName("owner_id")
     * @return null|int
     */
    public function getOwnerIdForAPI()
    {
        $owner = $this->getOwnerForAPI();

        return $owner ? $owner->getId() : null;
    }

    /***********************************************/

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->userProjects               = new ArrayCollection();
        $this->shareNotifications         = new ArrayCollection();
        $this->quotations                 = new ArrayCollection();
        $this->sampleRequests             = new ArrayCollection();
        $this->teams                      = new ArrayCollection();
        $this->feeds                      = new ArrayCollection();
        $this->comments                   = new ArrayCollection();
        $this->projectRatingUsers         = new ArrayCollection();
        $this->projectProducts            = new ArrayCollection();
        $this->scenes                     = new ArrayCollection();
        $this->galleryProjects            = new ArrayCollection();
        $this->materialCoatings           = new ArrayCollection();
        $this->fintraxProducts            = new ArrayCollection();
        $this->expressionCladdingProducts = new ArrayCollection();
        $this->price                      = 0;
        $this->filters                    = new ArrayCollection();
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * @JMS\VirtualProperty
     * @JMS\Type("AppBundle\Entity\User")
     * @JMS\SerializedName("owner")
     * @return User|null
     */
    public function getOwnerForAPI()
    {
        if ($this->isTemplate) {
            return null;
        }

        return $this->getOwner();
    }

    /**
     * Shared users array
     *
     * @return User[]
     */
    public function getSharedUsers()
    {
        $users = [];

        /** @var UserProject $userProject */
        foreach ($this->getUserProjects() as $userProject) {
            if ($userProject->getUserType() === UserProjectUserType::SHARED) {
                $users[] = $userProject->getUser();
            }
        }

        return $users;
    }

    /**
     * Acoustic backing id
     *
     * @return null|int
     */
    public function getAcousticBackingId()
    {
        return $this->acousticBacking ? $this->acousticBacking->getId() : null;
    }

    /**
     * Acoustic rating id
     *
     * @return null|int
     */
    public function getAcousticRatingId()
    {
        return $this->acousticRating ? $this->acousticRating->getId() : null;
    }

    /**
     * Railing id
     *
     * @return null|int
     */
    public function getRailingId()
    {
        return $this->railing ? $this->railing->getId() : null;
    }

    /**
     * Railing finish id
     *
     * @return null|int
     */
    public function getRailingFinishId()
    {
        return $this->railingFinish ? $this->railingFinish->getId() : null;
    }

    /**
     * Product Type Id
     *
     * @return null|int
     */
    public function getProductTypeId()
    {
        return $this->productType ? $this->productType->getId() : null;
    }

    /***********************************************/

    /**
     * @return User|null
     */
    public function getOwner()
    {
        /** @var UserProject $userProject */
        foreach ($this->getUserProjects() as $userProject) {
            if ($userProject->getUserType() === UserProjectUserType::OWNER) {
                return $userProject->getUser();
            }
        }

        return null;
    }

    /**
     * Owner user Id
     *
     * @return null|int
     */
    public function getOwnerId()
    {
        $owner = $this->getOwner();

        return $owner ? $owner->getId() : null;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Project
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set applicationType
     *
     * @param integer $applicationType
     *
     * @return Project
     */
    public function setApplicationType($applicationType)
    {
        $this->applicationType = $applicationType;

        return $this;
    }

    /**
     * Get applicationType
     *
     * @return integer
     */
    public function getApplicationType()
    {
        return $this->applicationType;
    }

    /**
     * Set status
     *
     * @param integer $status
     *
     * @return Project
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set productType
     *
     * @param ProductType $productType
     *
     * @return Project
     */
    public function setProductType(ProductType $productType)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Add userProject
     *
     * @param UserProject $userProject
     *
     * @return Project
     */
    public function addUserProject(UserProject $userProject)
    {
        $this->userProjects[] = $userProject;

        return $this;
    }

    /**
     * Remove userProject
     *
     * @param UserProject $userProject
     */
    public function removeUserProject(UserProject $userProject)
    {
        $this->userProjects->removeElement($userProject);
    }

    /**
     * Get userProjects
     *
     * @return Collection
     */
    public function getUserProjects()
    {
        return $this->userProjects;
    }

    /**
     * Add scene
     *
     * @param Scene $scene
     *
     * @return Project
     */
    public function addScene(Scene $scene)
    {
        $this->scenes[] = $scene;

        return $this;
    }

    /**
     * Remove scene
     *
     * @param Scene $scene
     */
    public function removeScene(Scene $scene)
    {
        $this->scenes->removeElement($scene);
    }

    /**
     * Get scenes
     *
     * @return Collection
     */
    public function getScenes()
    {
        return $this->scenes;
    }

    /**
     * Add projectProduct
     *
     * @param ProjectProduct $projectProduct
     *
     * @return Project
     */
    public function addProjectProduct(ProjectProduct $projectProduct)
    {
        $this->projectProducts[] = $projectProduct;

        return $this;
    }

    /**
     * Remove projectProduct
     *
     * @param ProjectProduct $projectProduct
     */
    public function removeProjectProduct(ProjectProduct $projectProduct)
    {
        $this->projectProducts->removeElement($projectProduct);
    }

    /**
     * Get projectProducts
     *
     * @return Collection
     */
    public function getProjectProducts()
    {
        return $this->projectProducts;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Show", "List", "Share"})
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/projects';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * Add team
     *
     * @param Team $team
     *
     * @return Project
     */
    public function addTeam(Team $team)
    {
        $this->teams[] = $team;

        return $this;
    }

    /**
     * Remove team
     *
     * @param Team $team
     */
    public function removeTeam(Team $team)
    {
        $this->teams->removeElement($team);
    }

    /**
     * Get teams
     *
     * @return Collection
     */
    public function getTeams()
    {
        return $this->teams;
    }

    /**
     * Add feed
     *
     * @param Feed $feed
     *
     * @return Project
     */
    public function addFeed(Feed $feed)
    {
        $this->feeds[] = $feed;

        return $this;
    }

    /**
     * Remove feed
     *
     * @param Feed $feed
     */
    public function removeFeed(Feed $feed)
    {
        $this->feeds->removeElement($feed);
    }

    /**
     * Get feeds
     *
     * @return Collection
     */
    public function getFeeds()
    {
        return $this->feeds;
    }

    /**
     * Add comment
     *
     * @param ProjectComment $comment
     *
     * @return Project
     */
    public function addComment(ProjectComment $comment)
    {
        $this->comments[] = $comment;

        return $this;
    }

    /**
     * Remove comment
     *
     * @param ProjectComment $comment
     */
    public function removeComment(ProjectComment $comment)
    {
        $this->comments->removeElement($comment);
    }

    /**
     * Get comments
     *
     * @return Collection
     */
    public function getComments()
    {
        return $this->comments;
    }

    /**
     * Add shareNotification
     *
     * @param ShareNotification $shareNotification
     *
     * @return Project
     */
    public function addShareNotification(ShareNotification $shareNotification)
    {
        $this->shareNotifications[] = $shareNotification;

        return $this;
    }

    /**
     * Remove shareNotification
     *
     * @param ShareNotification $shareNotification
     */
    public function removeShareNotification(ShareNotification $shareNotification)
    {
        $this->shareNotifications->removeElement($shareNotification);
    }

    /**
     * Get shareNotifications
     *
     * @return Collection
     */
    public function getShareNotifications()
    {
        return $this->shareNotifications;
    }

    /**
     * Add quotation
     *
     * @param Quotation $quotation
     *
     * @return Project
     */
    public function addQuotation(Quotation $quotation)
    {
        $this->quotations[] = $quotation;

        return $this;
    }

    /**
     * Remove quotation
     *
     * @param Quotation $quotation
     */
    public function removeQuotation(Quotation $quotation)
    {
        $this->quotations->removeElement($quotation);
    }

    /**
     * Get quotations
     *
     * @return Collection
     */
    public function getQuotations()
    {
        return $this->quotations;
    }

    /**
     * Add galleryProject
     *
     * @param GalleryProject $galleryProject
     *
     * @return Project
     */
    public function addGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProjects[] = $galleryProject;

        return $this;
    }

    /**
     * Remove galleryProject
     *
     * @param GalleryProject $galleryProject
     */
    public function removeGalleryProject(GalleryProject $galleryProject)
    {
        $this->galleryProjects->removeElement($galleryProject);
    }

    /**
     * Get galleryProjects
     *
     * @return Collection
     */
    public function getGalleryProjects()
    {
        return $this->galleryProjects;
    }

    /**
     * @return Collection
     */
    public function getProjectRatingUsers()
    {
        return $this->projectRatingUsers;
    }

    /**
     * @param Collection $projectRatingUsers
     */
    public function setProjectRatingUsers($projectRatingUsers)
    {
        $this->projectRatingUsers = $projectRatingUsers;
    }

    /**
     * @return int
     */
    public function getViews()
    {
        return $this->views;
    }

    /**
     * @param int $views
     */
    public function setViews($views)
    {
        $this->views = $views;
    }

    /**
     * Add projectRatingUser
     *
     * @param ProjectRatingUser $projectRatingUser
     *
     * @return Project
     */
    public function addProjectRatingUser(ProjectRatingUser $projectRatingUser)
    {
        $this->projectRatingUsers[] = $projectRatingUser;

        return $this;
    }

    /**
     * Remove projectRatingUser
     *
     * @param ProjectRatingUser $projectRatingUser
     */
    public function removeProjectRatingUser(ProjectRatingUser $projectRatingUser)
    {
        $this->projectRatingUsers->removeElement($projectRatingUser);
    }

    /**
     * @return bool
     */
    public function isPublic()
    {
        return $this->public;
    }

    /**
     * @param bool $public
     */
    public function setPublic($public)
    {
        $this->public = $public;
    }

    /**
     * @param $outdated
     */
    public function setOutdated($outdated)
    {
        $this->outdated = $outdated;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     *
     * @return $this
     */
    public function setPrice($price)
    {
        $this->price = $price;

        return $this;
    }

    /**
     * Get public
     *
     * @return boolean
     */
    public function getPublic()
    {
        return $this->public;
    }

    /**
     * Get outdated
     *
     * @return boolean
     */
    public function getOutdated()
    {
        return $this->outdated;
    }

    /**
     * Add materialCoating
     *
     * @param MaterialCoating $materialCoating
     *
     * @return Project
     */
    public function addMaterialCoating(MaterialCoating $materialCoating)
    {
        $this->materialCoatings[] = $materialCoating;

        return $this;
    }

    /**
     * Remove materialCoating
     *
     * @param MaterialCoating $materialCoating
     */
    public function removeMaterialCoating(MaterialCoating $materialCoating)
    {
        $this->materialCoatings->removeElement($materialCoating);
    }

    /**
     * Get materialCoatings
     *
     * @return Collection
     */
    public function getMaterialCoatings()
    {
        return $this->materialCoatings;
    }

    /**
     * Set materialCoatings
     *
     * @param array $materialCoatings
     *
     * @return $this
     */
    public function setMaterialCoatings(array $materialCoatings)
    {
        $this->materialCoatings = new ArrayCollection($materialCoatings);

        return $this;
    }

    /**
     * @return AcousticBacking
     */
    public function getAcousticBacking()
    {
        return $this->acousticBacking;
    }

    /**
     * @param AcousticBacking $acousticBacking
     *
     * @return $this
     */
    public function setAcousticBacking($acousticBacking)
    {
        $this->acousticBacking = $acousticBacking;

        return $this;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return (string) $this->title;
    }

    /**
     * Add sampleRequest
     *
     * @param SampleRequest $sampleRequest
     *
     * @return Project
     */
    public function addSampleRequest(SampleRequest $sampleRequest)
    {
        $this->sampleRequests[] = $sampleRequest;

        return $this;
    }

    /**
     * Remove sampleRequest
     *
     * @param SampleRequest $sampleRequest
     */
    public function removeSampleRequest(SampleRequest $sampleRequest)
    {
        $this->sampleRequests->removeElement($sampleRequest);
    }

    /**
     * Get sampleRequests
     *
     * @return Collection
     */
    public function getSampleRequests()
    {
        return $this->sampleRequests;
    }

    /**
     * Set wholesalePrice
     *
     * @param float $wholesalePrice
     *
     * @return Project
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;

        return $this;
    }

    /**
     * Get wholesalePrice
     *
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }

    /**
     * Set retailPrice
     *
     * @param float $retailPrice
     *
     * @return Project
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = $retailPrice;

        return $this;
    }

    /**
     * Get retailPrice
     *
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * Set isTemplate
     *
     * @param boolean $isTemplate
     *
     * @return Project
     */
    public function setIsTemplate($isTemplate)
    {
        $this->isTemplate = $isTemplate;

        return $this;
    }

    /**
     * Get isTemplate
     *
     * @return boolean
     */
    public function getIsTemplate()
    {
        return $this->isTemplate;
    }

    /**
     * Set railing
     *
     * @param Railing $railing
     *
     * @return Project
     */
    public function setRailing(Railing $railing = null)
    {
        $this->railing = $railing;

        return $this;
    }

    /**
     * Get railing
     *
     * @return Railing
     */
    public function getRailing()
    {
        return $this->railing;
    }

    /**
     * Set railingFinish
     *
     * @param RailingFinish $railingFinish
     *
     * @return Project
     */
    public function setRailingFinish(RailingFinish $railingFinish = null)
    {
        $this->railingFinish = $railingFinish;

        return $this;
    }

    /**
     * Get railingFinish
     *
     * @return RailingFinish
     */
    public function getRailingFinish()
    {
        return $this->railingFinish;
    }

    /**
     * Add fintraxProduct
     *
     * @param FintraxProduct $fintraxProduct
     *
     * @return Project
     */
    public function addFintraxProduct(FintraxProduct $fintraxProduct)
    {
        $this->fintraxProducts[] = $fintraxProduct;

        return $this;
    }

    /**
     * Remove fintraxProduct
     *
     * @param FintraxProduct $fintraxProduct
     */
    public function removeFintraxProduct(FintraxProduct $fintraxProduct)
    {
        $this->fintraxProducts->removeElement($fintraxProduct);
    }

    /**
     * Get fintraxProducts
     *
     * @return Collection|FintraxProduct[]
     */
    public function getFintraxProducts()
    {
        return $this->fintraxProducts;
    }

    /**
     * Add expressionCladdingProduct
     *
     * @param ExpressionCladdingProduct $expressionCladdingProduct
     *
     * @return Project
     */
    public function addExpressionCladdingProduct(ExpressionCladdingProduct $expressionCladdingProduct)
    {
        $this->expressionCladdingProducts[] = $expressionCladdingProduct;

        return $this;
    }

    /**
     * Remove expressionCladdingProduct
     *
     * @param ExpressionCladdingProduct $expressionCladdingProduct
     */
    public function removeExpressionCladdingProduct(ExpressionCladdingProduct $expressionCladdingProduct)
    {
        $this->expressionCladdingProducts->removeElement($expressionCladdingProduct);
    }

    /**
     * Get expressionCladdingProducts
     *
     * @return Collection
     */
    public function getExpressionCladdingProducts()
    {
        return $this->expressionCladdingProducts;
    }

    /**
     * @return float
     */
    public function getSequenceRatio()
    {
        return $this->sequenceRatio;
    }

    /**
     * @param float $sequenceRatio
     *
     * @return Project
     */
    public function setSequenceRatio($sequenceRatio)
    {
        $this->sequenceRatio = $sequenceRatio;

        return $this;
    }

    /**
     * Get is snap value locked?
     *
     * @return boolean
     */
    public function getIsSnapToAll()
    {
        return $this->isSnapToAll;
    }

    /**
     * Set Is snap value locked?
     *
     * @param boolean $isSnapToAll
     *
     * @return Project
     */
    public function setIsSnapToAll($isSnapToAll)
    {
        $this->isSnapToAll = $isSnapToAll;

        return $this;
    }


    /**
     * Get snap value
     *
     * @return integer
     */
    public function getSnap()
    {
        return $this->snap;
    }

    /**
     * Set snap value
     *
     * @param integer $snap
     *
     * @return Project
     */
    public function setSnap($snap)
    {
        $this->snap = $snap;

        return $this;
    }

    /**
     * Set acousticRating
     *
     * @param AcousticRating $acousticRating
     *
     * @return Project
     */
    public function setAcousticRating($acousticRating = null)
    {
        $this->acousticRating = $acousticRating;

        return $this;
    }

    /**
     * Get acousticRating
     *
     * @return AcousticRating
     */
    public function getAcousticRating()
    {
        return $this->acousticRating;
    }

    /**
     * Set pricingCategory
     *
     * @param string $pricingCategory
     *
     * @return Project
     */
    public function setPricingCategory($pricingCategory = null)
    {
        $this->pricingCategory = $pricingCategory;

        return $this;
    }

    /**
     * Get pricingCategory
     *
     * @return string
     */
    public function getPricingCategory()
    {
        return $this->pricingCategory;
    }

    /**
     * Set totalWeight.
     *
     * @param float|null $totalWeight
     *
     * @return Project
     */
    public function setTotalWeight($totalWeight = null)
    {
        $this->totalWeight = $totalWeight;

        return $this;
    }

    /**
     * Get totalWeight.
     *
     * @return float|null
     */
    public function getTotalWeight()
    {
        return $this->totalWeight;
    }

    /**
     * Add filter.
     *
     * @param \AppBundle\Entity\ProjectFilter $filter
     *
     * @return Project
     */
    public function addFilter(\AppBundle\Entity\ProjectFilter $filter)
    {
        $this->filters[] = $filter;

        return $this;
    }

    /**
     * Remove filter.
     *
     * @param \AppBundle\Entity\ProjectFilter $filter
     *
     * @return boolean TRUE if this collection contained the specified element, FALSE otherwise.
     */
    public function removeFilter(\AppBundle\Entity\ProjectFilter $filter)
    {
        return $this->filters->removeElement($filter);
    }

    /**
     * Set currency.
     *
     * @param \AppBundle\Entity\Currency|null $currency
     *
     * @return Project
     */
    public function setCurrency(\AppBundle\Entity\Currency $currency = null)
    {
        $this->currency = $currency;

        return $this;
    }

    /**
     * Get currency.
     *
     * @return \AppBundle\Entity\Currency|null
     */
    public function getCurrency()
    {
        return $this->currency;
    }

    public function getNZDPrice()
    {
        $price = (float) ($this->price * $this->currency->getRate());

        return (($price / 100) * self::NZD_ADDITION_PERCENTAGE) + $price;
    }
}
