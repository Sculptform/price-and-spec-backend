<?php

namespace AppBundle\Entity;

use AppBundle\Enum\ApplicationType;

trait ApplicationTypeTrait {
    /**
     * @param $applicationTypes|mixed
     * @return string
     */
    public function getLabel($applicationTypes)
    {
        $applicationTypes = is_array($applicationTypes) ? $applicationTypes : [$applicationTypes];

        if (count($applicationTypes) == 1) {
            if(in_array(ApplicationType::INTERIOR ,$applicationTypes)){
                return ApplicationType::INTERIOR_LABEL;
            }

            if(in_array(ApplicationType::EXTERIOR ,$applicationTypes)){
                return ApplicationType::EXTERIOR_LABEL;
            }
        }

        return ApplicationType::BOTH_LABEL;
    }
}