<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 15:43
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\SceneRepository")
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Scene
{
    /**
     * Scene id
     * 
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var Project
     *
     * @JMS\Exclude
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="scenes")
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * Scene body
     * 
     * @var array
     * @JMS\Type("array")
     * @ORM\Column(type="json_array", nullable=false)
     */
    protected $scene;

    /**
     * Date of creation
     * 
     * @var \DateTime
     *
     * @Assert\Date
     * @ORM\Column(type="datetime", nullable=false)
     * @Gedmo\Timestampable(on="create")
     */
    protected $createdAt;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set scene
     *
     * @param array $scene
     *
     * @return Scene
     */
    public function setScene(array $scene)
    {
        $this->scene = $scene;

        return $this;
    }

    /**
     * Get scene
     *
     * @return array
     */
    public function getScene()
    {
        return $this->scene;
    }

    /**
     * @return array
     */
    public function getSceneSpecification()
    {
        $defaultSpec = ['trackDetails' => '', 'sequence' => '', ''];

        $scene = $this->getScene();

        return array_merge($defaultSpec, isset($scene['spec']) ? $scene['spec'] : []);
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Scene
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return Scene
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }
}
