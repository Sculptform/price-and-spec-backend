<?php

namespace AppBundle\Entity;

use AppBundle\Entity\Image\StaticBlockImage;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * Class represents static customizable blocks in the main layout
 *
 * StaticBlock
 *
 * @ORM\Table(name="static_block")
 * @ORM\Entity()
 * @DoctrineAssert\UniqueEntity(
 *     fields="name",
 *     message="Block with same name already exists",
 * )
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 */
class StaticBlock
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * Name
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    protected $name;

    /**
     * Title
     *
     * @var string
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $title;

    /**
     * Content
     *
     * @var string
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="text", nullable=true)
     */
    protected $content;

    /**
     * isVisible
     *
     * @var boolean
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="boolean")
     */
    protected $isVisible = true;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     * @JMS\Groups({"List", "show", "update"})
     * @ORM\OneToMany(targetEntity="AppBundle\Entity\Image\StaticBlockImage", mappedBy="staticBlock", cascade={"persist", "remove"}, orphanRemoval=true, fetch="EXTRA_LAZY" )
     * @JMS\Type("array<AppBundle\Entity\Image\StaticBlockImage>")
     */
    protected $staticBlockImages;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return StaticBlock
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return StaticBlock
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return StaticBlock
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Set isVisible
     *
     * @param boolean $isVisible
     *
     * @return StaticBlock
     */
    public function setIsVisible($isVisible)
    {
        $this->isVisible = $isVisible;

        return $this;
    }

    /**
     * Get isVisible
     *
     * @return boolean
     */
    public function getIsVisible()
    {
        return $this->isVisible;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->staticBlockImages = new ArrayCollection();
    }

    /**
     * Add staticBlockImage
     *
     * @param StaticBlockImage $staticBlockImage
     *
     * @return StaticBlock
     */
    public function addStaticBlockImage(StaticBlockImage $staticBlockImage)
    {
        $this->staticBlockImages[] = $staticBlockImage;

        return $this;
    }

    /**
     * Remove staticBlockImage
     *
     * @param StaticBlockImage $staticBlockImage
     */
    public function removeStaticBlockImage(StaticBlockImage $staticBlockImage)
    {
        $this->staticBlockImages->removeElement($staticBlockImage);
    }

    /**
     * Get staticBlockImages
     *
     * @return Collection
     */
    public function getStaticBlockImages()
    {
        return $this->staticBlockImages;
    }
}
