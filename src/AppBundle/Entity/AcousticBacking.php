<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * AcousticBacking
 *
 * @ORM\Table(name="acoustic_backing")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AcousticBackingRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class AcousticBacking
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Color
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $color;

    /**
     * Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $price = 0;

    /**
     * Retail Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $retailPrice = 0;

    /**
     * Wholesale Price
     *
     * @var int
     * @ORM\Column(type="float", nullable=false, options={"default": 0})
     */
    protected $wholesalePrice = 0;

    /**
     * Depth
     *
     * @var int
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $depth;

    /**
     * Defines if it's default backing
     *
     * @var bool
     * @ORM\Column(type="boolean", options={"default" : 0})
     */
    protected $isDefault = false;

    /**
     * @return bool
     */
    public function isDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool $isDefault
     */
    public function setIsDefault(bool $isDefault): void
    {
        $this->isDefault = $isDefault;
    }


    /**
     * @var ProductType
     * @JMS\Exclude
     * @ORM\ManyToOne(targetEntity="ProductType", inversedBy="acousticBackings", cascade={"persist"})
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id", nullable=true)
     */
    protected $productType;

    /**
     * @return ProductType|null
     */
    public function getProductType(): ?ProductType
    {
        return $this->productType;
    }

    /**
     * @param ProductType $productType
     */
    public function setProductType(ProductType $productType): void
    {
        $this->productType = $productType;
    }

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param string $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return float
     */
    public function getPrice()
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice($price)
    {
        $this->price = $price;
    }

    /**
     * @return int
     */
    public function getDepth()
    {
        return $this->depth;
    }

    /**
     * @param int $depth
     */
    public function setDepth($depth)
    {
        $this->depth = $depth;
    }

    /**
     * Set retailPrice
     *
     * @param float $retailPrice
     *
     * @return AcousticBacking
     */
    public function setRetailPrice($retailPrice)
    {
        $this->retailPrice = $retailPrice;

        return $this;
    }

    /**
     * Get retailPrice
     *
     * @return float
     */
    public function getRetailPrice()
    {
        return $this->retailPrice;
    }

    /**
     * Set wholesalePrice
     *
     * @param float $wholesalePrice
     *
     * @return AcousticBacking
     */
    public function setWholesalePrice($wholesalePrice)
    {
        $this->wholesalePrice = $wholesalePrice;

        return $this;
    }

    /**
     * Get wholesalePrice
     *
     * @return float
     */
    public function getWholesalePrice()
    {
        return $this->wholesalePrice;
    }
}
