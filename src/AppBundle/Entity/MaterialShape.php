<?php
/**
 * Created by PhpStorm.
 * User: eXPert
 * Date: 22.06.2016
 * Time: 16:22
 */

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\Common\Inflector\Inflector;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Tree\Traits\NestedSetEntity;
use Gedmo\Mapping\Annotation as Gedmo;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * @Gedmo\Tree(type="nested")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialShapeRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class MaterialShape
{
    use NestedSetEntity;

    /**
     * Material Shape id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @var MaterialShape
     *
     * @Gedmo\TreeParent
     * @ORM\ManyToOne(targetEntity="MaterialShape", inversedBy="children")
     * @ORM\JoinColumn(name="parent_id", referencedColumnName="id", onDelete="CASCADE", nullable=true)
     *
     * @JMS\Exclude
     */
    protected $parent;

    /**
     * @var Collection
     *
     * @ORM\OneToMany(targetEntity="MaterialShape", mappedBy="parent")
     * @ORM\OrderBy({"left" = "ASC"})
     *
     * @JMS\Exclude
     */
    protected $children;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Unique name
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true, unique=true)
     */
    protected $name;

    /**
     * Is Show SubShapes ?
     *
     * @var bool
     *
     * @ORM\Column(type="boolean", nullable=false, options={"default": 1})
     */
    protected $isShowSubShapes = true;

    /**
     * @var Collection
     *
     * @JMS\Exclude
     *
     * @ORM\OneToMany(
     *     targetEntity="AppBundle\Entity\MaterialShapeSize",
     *     mappedBy="materialShape",
     *     cascade={"all"},
     *     fetch="EXTRA_LAZY"
     * )
     *
     * @ORM\OrderBy({"width" = "ASC", "depth" = "ASC"})
     */
    protected $materialShapeSizes;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $imageTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->children = new ArrayCollection();
        $this->materialShapeSizes = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return MaterialShape
     */
    public function setTitle($title)
    {
        $this->title = $title;

        if (!$this->name) {
            $this->name = Inflector::tableize($title);
        }

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return MaterialShape
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Add materialShapeSize
     *
     * @param MaterialShapeSize $materialShapeSize
     *
     * @return MaterialShape
     */
    public function addMaterialShapeSize(MaterialShapeSize $materialShapeSize)
    {
        $this->materialShapeSizes[] = $materialShapeSize;

        return $this;
    }

    /**
     * Remove materialShapeSize
     *
     * @param MaterialShapeSize $materialShapeSize
     */
    public function removeMaterialShapeSize(MaterialShapeSize $materialShapeSize)
    {
        $this->materialShapeSizes->removeElement($materialShapeSize);
    }

    /**
     * Get materialShapeSizes
     *
     * @return Collection
     */
    public function getMaterialShapeSizes()
    {
        return $this->materialShapeSizes;
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Get path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     *
     * @param bool $includeSelf
     *
     * @return string
     */
    public function getPath($includeSelf = true)
    {
        $parents = $includeSelf ? [$this] : [];

        $this->getAllParents($this, $parents);

        $parents = array_reverse($parents);

        $parents = array_map(function (MaterialShape $parent) {
            return $parent->getId();
        }, $parents);

        return implode('_', $parents);
    }

    /**
     * @param int  $level
     * @param bool $includeSelf
     *
     * @return mixed
     */
    public function getParentByLevel($level, $includeSelf = true)
    {
        $parents = $includeSelf ? [$this] : [];

        $this->getAllParents($this, $parents);

        $parents = array_filter($parents, function (MaterialShape $parent) use ($level) {
            return $parent->level === $level;
        });

        return array_pop($parents);
    }

    /**
     * Parent Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     *
     * @return string
     */
    public function getParentId()
    {
        return $this->getParent() ? $this->getParent()->getId() : null;
    }

    /***********************************************/

    /**
     * @param MaterialShape   $node
     * @param MaterialShape[] $parents
     */
    public function getAllParents(MaterialShape $node, array &$parents)
    {
        $parent = $node->getParent();

        if ($parent) {
            $parents[] = $parent;

            $this->getAllParents($parent, $parents);
        }
    }

    /**
     * Set root
     *
     * @param integer $root
     *
     * @return MaterialShape
     */
    public function setRoot($root)
    {
        $this->root = $root;

        return $this;
    }

    /**
     * Get root
     *
     * @return integer
     */
    public function getRoot()
    {
        return $this->root;
    }

    /**
     * Set level
     *
     * @param integer $level
     *
     * @return MaterialShape
     */
    public function setLevel($level)
    {
        $this->level = $level;

        return $this;
    }

    /**
     * Get level
     *
     * @return integer
     */
    public function getLevel()
    {
        return $this->level;
    }

    /**
     * Set left
     *
     * @param integer $left
     *
     * @return MaterialShape
     */
    public function setLeft($left)
    {
        $this->left = $left;

        return $this;
    }

    /**
     * Get left
     *
     * @return integer
     */
    public function getLeft()
    {
        return $this->left;
    }

    /**
     * Set right
     *
     * @param integer $right
     *
     * @return MaterialShape
     */
    public function setRight($right)
    {
        $this->right = $right;

        return $this;
    }

    /**
     * Get right
     *
     * @return integer
     */
    public function getRight()
    {
        return $this->right;
    }

    /**
     * Set parent
     *
     * @param MaterialShape $parent
     *
     * @return MaterialShape
     */
    public function setParent(MaterialShape $parent = null)
    {
        $this->parent = $parent;

        return $this;
    }

    /**
     * Get parent
     *
     * @return MaterialShape
     */
    public function getParent()
    {
        return $this->parent;
    }

    /**
     * Add child
     *
     * @param MaterialShape $child
     *
     * @return MaterialShape
     */
    public function addChild(MaterialShape $child)
    {
        $this->children[] = $child;

        return $this;
    }

    /**
     * Remove child
     *
     * @param MaterialShape $child
     */
    public function removeChild(MaterialShape $child)
    {
        $this->children->removeElement($child);
    }

    /**
     * Get children
     *
     * @return Collection
     */
    public function getChildren()
    {
        return $this->children;
    }

    /**
     * Set isShowSubShapes
     *
     * @param boolean $isShowSubShapes
     *
     * @return MaterialShape
     */
    public function setIsShowSubShapes($isShowSubShapes)
    {
        $this->isShowSubShapes = $isShowSubShapes;

        return $this;
    }

    /**
     * Get isShowSubShapes
     *
     * @return boolean
     */
    public function getIsShowSubShapes()
    {
        return $this->isShowSubShapes;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir() . '/' . $this->imagePath : NULL;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @JMS\Groups({"Show", "List", "Share"})
     * @JMS\Expose
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir() . '/' . $this->imagePath;
        }

        return NULL;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/materialShapes';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = NULL)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (NULL !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (NULL === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = NULL;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(NULL);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp) && is_file($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     *
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    public function __toString()
    {
        return $this->getTitle();
    }
}
