<?php

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\RailingFinishRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class RailingFinish
{
    use ColorTrait;

    /**
     * Material finish id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Color
     *
     * @var string
     *
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $color;

    /**
     * Railing finish application types
     *
     * @var array
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\ApplicationType", "getChoices"}, multiple=true, message="Enter a valid application types.")
     * @ORM\Column(type="simple_array", nullable=false)
     * @see ApplicationType
     */
    protected $applicationTypes;

    /**
     * @var MaterialType
     *
     * @ORM\ManyToOne(targetEntity="MaterialType", inversedBy="railingFinishes", cascade={"persist"})
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id",  onDelete="SET NULL")
     * @JMS\Exclude
     */
    protected $materialType;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     */
    protected $textureTemp;

    /**
     * @var UploadedFile
     * @JMS\Exclude
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $textureFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $texturePath;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Texture Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getTextureWebPath()
    {
        if ($this->texturePath && is_file($this->getTextureAbsolutePath())) {
            return $this->getTextureUploadDir().'/'.$this->texturePath;
        }

        return null;
    }

    /***********************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return RailingFinish
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    public function getTextureAbsolutePath()
    {
        return $this->texturePath ? $this->getTextureUploadRootDir().'/'.$this->texturePath : null;
    }

    protected function getTextureUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../web/' . $this->getTextureUploadDir();
    }

    protected function getTextureUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/railingFinishes';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     * @return RailingFinish
     */
    public function setTextureFile(UploadedFile $file = null)
    {
        $this->textureFile = $file;

        // check if we have an old image path
        if (is_file($this->getTextureAbsolutePath())) {
            // store the old name to delete after the update
            $this->textureTemp = $this->getTextureAbsolutePath();
        }

        $this->preUploadTexture();

        return $this;
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getTextureFile()
    {
        return $this->textureFile;
    }

    public function preUploadTexture()
    {
        if (null !== $this->getTextureFile()) {
            $this->texturePath = FileUtil::generateFilename($this->getTextureFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadTexture()
    {
        if (null === $this->getTextureFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->textureTemp)) {
            // delete the old image
            unlink($this->textureTemp);
            // clear the temp image path
            $this->textureTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getTextureFile()->move(
            $this->getTextureUploadRootDir(),
            $this->texturePath
        );

        $this->setTextureFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeTextureFilenameForRemove()
    {
        $this->textureTemp = $this->getTextureAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeTextureUpload()
    {
        if (isset($this->textureTemp)) {
            @unlink($this->textureTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $texturePath
     * @return $this
     */
    public function setTexturePath($texturePath)
    {
        $this->texturePath = $texturePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getTexturePath()
    {
        return $this->texturePath;
    }

    /**
     * Set color
     *
     * @param string $color
     *
     * @return RailingFinish
     */
    public function setColor($color)
    {
        $this->color = $color;

        return $this;
    }

    /**
     * Get color
     *
     * @return string
     */
    public function getColor()
    {
        return $this->color;
    }

    public function __toString()
    {
        return (string)$this->getTitle();
    }

    /**
     * Set applicationTypes
     *
     * @param array $applicationTypes
     *
     * @return RailingFinish
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;

        return $this;
    }

    /**
     * Get applicationTypes
     *
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * Set materialType
     *
     * @param \AppBundle\Entity\MaterialType $materialType
     *
     * @return RailingFinish
     */
    public function setMaterialType(\AppBundle\Entity\MaterialType $materialType = null)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return \AppBundle\Entity\MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }
}
