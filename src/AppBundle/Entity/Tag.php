<?php

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Gedmo\Timestampable\Traits\TimestampableEntity;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;
use Gedmo\Mapping\Annotation as Gedmo;
use Symfony\Bridge\Doctrine\Validator\Constraints as DoctrineAssert;

/**
 * Tag
 *
 * @ORM\Table(name="tag")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\TagRepository")
 * @DoctrineAssert\UniqueEntity(
 *     fields="content",
 *     message="This tag already exists",
 *     groups={"discussion"}
 * )
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 */
class Tag
{
    use TimestampableEntity;

    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * Content
     *
     * @var string
     * @Assert\NotBlank()
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     * @ORM\Column(type="string", nullable=false, unique=true)
     */
    protected $content;

    /**
     * @var Collection
     * @ORM\ManyToMany(targetEntity="Discussion", inversedBy="tags", cascade={"persist"}, fetch="EXTRA_LAZY")
     * @ORM\JoinTable(name="discussion_tags")
     * @JMS\Groups({"Create", "Update", "Show", "List"})
     */
    private $discussions;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set content
     *
     * @param string $content
     *
     * @return Tag
     */
    public function setContent($content)
    {
        $this->content = $content;

        return $this;
    }

    /**
     * Get content
     *
     * @return string
     */
    public function getContent()
    {
        return $this->content;
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->discussions = new ArrayCollection();
    }

    /**
     * Add discussion
     *
     * @param Discussion $discussion
     *
     * @return Tag
     */
    public function addDiscussion(Discussion $discussion)
    {
        $this->discussions[] = $discussion;

        return $this;
    }

    /**
     * Remove discussion
     *
     * @param Discussion $discussion
     */
    public function removeDiscussion(Discussion $discussion)
    {
        $this->discussions->removeElement($discussion);
    }

    /**
     * Get discussions
     *
     * @return Collection
     */
    public function getDiscussions()
    {
        return $this->discussions;
    }
}
