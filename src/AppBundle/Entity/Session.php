<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 27.02.17
 * Time: 9:50
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="sessions")
 *
 * @author Dmitriy Ramenev <diman4k@gmail.com>
 */
class Session
{
    /**
     * Session id
     *
     * @var string
     *
     * @ORM\Id
     * @ORM\Column(name="sess_id", type="string", length=128, nullable=false)
     */
    protected $id;

    /**
     * Session data
     *
     * @var string
     *
     * @ORM\Column(name="sess_data", type="blob", nullable=false)
     */
    protected $data;

    /**
     * Session time
     *
     * @var integer
     *
     * @ORM\Column(name="sess_time", type="integer", nullable=false)
     */
    protected $time;

    /**
     * Session lifetime
     *
     * @var integer
     *
     * @ORM\Column(name="sess_lifetime", type="integer", nullable=false)
     */
    protected $lifetime;

    /**
     * Set id
     *
     * @param string $id
     *
     * @return Session
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return string
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set data
     *
     * @param string $data
     *
     * @return Session
     */
    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    /**
     * Get data
     *
     * @return string
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * Set time
     *
     * @param integer $time
     *
     * @return Session
     */
    public function setTime($time)
    {
        $this->time = $time;

        return $this;
    }

    /**
     * Get time
     *
     * @return integer
     */
    public function getTime()
    {
        return $this->time;
    }

    /**
     * Set lifetime
     *
     * @param integer $lifetime
     *
     * @return Session
     */
    public function setLifetime($lifetime)
    {
        $this->lifetime = $lifetime;

        return $this;
    }

    /**
     * Get lifetime
     *
     * @return integer
     */
    public function getLifetime()
    {
        return $this->lifetime;
    }
}
