<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 31.05.17
 * Time: 12:38
 */

namespace AppBundle\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Doctrine\Common\Collections\Collection;
use Doctrine\ORM\Mapping as ORM;
use Symfony\Component\Validator\Constraints as Assert;
use JMS\Serializer\Annotation as JMS;

/**
 * FintraxProduct
 *
 * @ORM\Table(name="fintrax_products")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\FintraxProductRepository")
 */
class FintraxProduct
{
    /**
     * ID
     *
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Fintrax Product UUID
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $uuid;

    /**
     * @var Project
     *
     * @JMS\Type("string")
     * @JMS\Accessor(getter="getProjectId")
     * @JMS\SerializedName("project_id")
     *
     * @Assert\NotBlank()
     * @ORM\ManyToOne(targetEntity="Project", inversedBy="fintraxProducts", cascade={"persist"})
     * @ORM\JoinColumn(name="project_id", referencedColumnName="id", nullable=false, onDelete="CASCADE")
     */
    protected $project;

    /**
     * Title
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $title;

    /**
     * Base Product
     *
     * @var Product
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getBaseId")
     *
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="base_product_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $base;

    /**
     * Stacker Products Array
     *
     * @var Collection|FintraxStacker[]
     *
     * @JMS\Exclude
     * @ORM\OneToMany(targetEntity="FintraxStacker", cascade={"all"}, mappedBy="fintraxProduct")
     */
    protected $fintraxStackers;

    /**
     * Nosing Product
     *
     * @var Product
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getNosingId")
     *
     * @ORM\ManyToOne(targetEntity="Product", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="nosing_product_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $nosing;

    /**
     * MaterialFinish
     *
     * @var MaterialFinish
     *
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getFinishId")
     *
     * @ORM\ManyToOne(targetEntity="MaterialFinish", cascade={"persist"}, fetch="EAGER")
     * @ORM\JoinColumn(name="material_finish_id", referencedColumnName="id", nullable=true,  onDelete="SET NULL")
     */
    protected $finish;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->fintraxStackers = new ArrayCollection();
    }

    /**
     * Stacker Products IDs
     * @JMS\Type("array<integer>")
     * @JMS\VirtualProperty
     * @JMS\SerializedName("stackers")
     */
    public function getStackerProductsIds()
    {
        return $this->fintraxStackers->map(function (FintraxStacker $stacker){
            return $stacker->getProduct()->getId();
        });
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return FintraxProduct
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set project
     *
     * @param Project $project
     *
     * @return FintraxProduct
     */
    public function setProject(Project $project)
    {
        $this->project = $project;

        return $this;
    }

    /**
     * Get project
     *
     * @return Project
     */
    public function getProject()
    {
        return $this->project;
    }

    /**
     * Set base
     *
     * @param Product $base
     *
     * @return FintraxProduct
     */
    public function setBase(Product $base = null)
    {
        $this->base = $base;

        return $this;
    }

    /**
     * Get base
     *
     * @return Product
     */
    public function getBase()
    {
        return $this->base;
    }

    /**
     * Add stacker
     *
     * @param FintraxStacker $stacker
     *
     * @return FintraxProduct
     */
    public function addFintraxStacker(FintraxStacker $stacker)
    {
        $this->fintraxStackers[] = $stacker;

        return $this;
    }

    /**
     * Remove stacker
     *
     * @param FintraxStacker $stacker
     */
    public function removeFintraxStacker(FintraxStacker $stacker)
    {
        $this->fintraxStackers->removeElement($stacker);
    }

    /**
     * Get stackers
     *
     * @return Collection|FintraxStacker[]
     */
    public function getFintraxStackers()
    {
        return $this->fintraxStackers;
    }

    /**
     * Set nosing
     *
     * @param Product $nosing
     *
     * @return FintraxProduct
     */
    public function setNosing(Product $nosing = null)
    {
        $this->nosing = $nosing;

        return $this;
    }

    /**
     * Get nosing
     *
     * @return Product
     */
    public function getNosing()
    {
        return $this->nosing;
    }

    /**
     * Set finish
     *
     * @param MaterialFinish $finish
     *
     * @return FintraxProduct
     */
    public function setFinish(MaterialFinish $finish = null)
    {
        $this->finish = $finish;

        return $this;
    }

    /**
     * Get finish
     *
     * @return MaterialFinish
     */
    public function getFinish()
    {
        return $this->finish;
    }

    /**
     * Set uuid
     *
     * @param string $uuid
     *
     * @return FintraxProduct
     */
    public function setUuid($uuid)
    {
        $this->uuid = $uuid;

        return $this;
    }

    /**
     * Get uuid
     *
     * @return string
     */
    public function getUuid()
    {
        return $this->uuid;
    }

    public function getBaseId()
    {
        return $this->base ? $this->base->getId() : null;
    }

    public function getProjectId()
    {
        return $this->getProject()->getId();
    }

    public function getNosingId()
    {
        return $this->nosing ? $this->nosing->getId() : null;
    }

    public function getFinishId()
    {
        return $this->finish ? $this->finish->getId() : null;
    }
}
