<?php

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\DefaultMaterialFinishRepository")
 * @ORM\Table
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class DefaultMaterialFinish
{
    /**
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getMaterialFinishId")
     * @ORM\ManyToOne(targetEntity="MaterialFinish")
     * @ORM\JoinColumn(name="material_finish_id", referencedColumnName="id")
     */
    protected $materialFinish;

    /**
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getProductTypeId")
     * @ORM\ManyToOne(targetEntity="ProductType")
     * @ORM\JoinColumn(name="product_type_id", referencedColumnName="id")
     */
    protected $productType;

    /**
     * @JMS\Type("integer")
     * @JMS\Accessor(getter="getMaterialTypeId")
     * @ORM\ManyToOne(targetEntity="MaterialType")
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id")
     */
    protected $materialType;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Material Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getMaterialTypeId()
    {
        return $this->getMaterialType()->getId();
    }

    /**
     * Product Type Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getProductTypeId()
    {
        return $this->getProductType()->getId();
    }

    /**
     * Material Finish Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @return integer
     */
    public function getMaterialFinishId()
    {
        return $this->getMaterialFinish()->getId();
    }

    /***********************************************/

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set materialFinish
     *
     * @param \AppBundle\Entity\MaterialFinish $materialFinish
     *
     * @return DefaultMaterialFinish
     */
    public function setMaterialFinish(\AppBundle\Entity\MaterialFinish $materialFinish = null)
    {
        $this->materialFinish = $materialFinish;

        return $this;
    }

    /**
     * Get materialFinish
     *
     * @return \AppBundle\Entity\MaterialFinish
     */
    public function getMaterialFinish()
    {
        return $this->materialFinish;
    }

    /**
     * Set productType
     *
     * @param \AppBundle\Entity\ProductType $productType
     *
     * @return DefaultMaterialFinish
     */
    public function setProductType(\AppBundle\Entity\ProductType $productType = null)
    {
        $this->productType = $productType;

        return $this;
    }

    /**
     * Get productType
     *
     * @return \AppBundle\Entity\ProductType
     */
    public function getProductType()
    {
        return $this->productType;
    }

    /**
     * Set materialType
     *
     * @param \AppBundle\Entity\MaterialType $materialType
     *
     * @return DefaultMaterialFinish
     */
    public function setMaterialType(\AppBundle\Entity\MaterialType $materialType = null)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return \AppBundle\Entity\MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }
}
