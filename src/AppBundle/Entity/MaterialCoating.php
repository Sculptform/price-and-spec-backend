<?php
/**
 * Class MaterialCoating
 * @package AppBundle\Entity
 * @author Andrey Zaytsev <dreyup96@gmail.com>
 */

namespace AppBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * MaterialCoating
 *
 * @ORM\Table(name="material_coating")
 * @ORM\Entity(repositoryClass="AppBundle\Repository\MaterialCoatingRepository")
 * @JMS\ExclusionPolicy("NONE")
 */
class MaterialCoating
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;

    /**
     * Description
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $description;

    /**
     * Product Price field name (Natural Accent, Cutek, Enviropro prices)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $productPriceFieldName;

    /**
     * Product Wholesale Price field name (Wholesale)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $productWholesalePriceFieldName;

    /**
     * Product Retail Price field name (Retail)
     *
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $productRetailPriceFieldName;

    /**
     * @var array
     *
     * @Assert\Choice(callback = {"AppBundle\Enum\ApplicationType", "getChoices"}, multiple=true, message="Enter a valid application types.")
     * @ORM\Column(type="array", nullable=false)
     * @see ApplicationType
     */
    protected $applicationTypes;

    /**
     * @var MaterialType
     *
     * @ORM\ManyToOne(targetEntity="MaterialType", inversedBy="materialCoatings", cascade={"persist"})
     * @ORM\JoinColumn(name="material_type_id", referencedColumnName="id",  onDelete="SET NULL")
     * @JMS\Exclude
     */
    protected $materialType;

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->applicationTypes = [];
    }

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Comments count
     *
     * @JMS\VirtualProperty
     * @JMS\Groups({"Show", "List"})
     * @return int
     */
    public function getMaterialTypeId()
    {
        return $this->materialType ? $this->materialType->getId() : null;
    }

    /************************************************/

    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    /**
     * @return mixed
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param mixed $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return array
     */
    public function getApplicationTypes()
    {
        return $this->applicationTypes;
    }

    /**
     * @param array $applicationTypes
     */
    public function setApplicationTypes($applicationTypes)
    {
        $this->applicationTypes = $applicationTypes;
    }

    /**
     * @return string
     */
    public function getProductPriceFieldName()
    {
        return $this->productPriceFieldName;
    }

    /**
     * @param string $productPriceFieldName
     */
    public function setProductPriceFieldName($productPriceFieldName)
    {
        $this->productPriceFieldName = $productPriceFieldName;
    }

    /**
     * Set materialType
     *
     * @param MaterialType $materialType
     *
     * @return MaterialCoating
     */
    public function setMaterialType(MaterialType $materialType = null)
    {
        $this->materialType = $materialType;

        return $this;
    }

    /**
     * Get materialType
     *
     * @return MaterialType
     */
    public function getMaterialType()
    {
        return $this->materialType;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getTitle();
    }

    /**
     * Set productWholesalePriceFieldName
     *
     * @param string $productWholesalePriceFieldName
     *
     * @return MaterialCoating
     */
    public function setProductWholesalePriceFieldName($productWholesalePriceFieldName)
    {
        $this->productWholesalePriceFieldName = $productWholesalePriceFieldName;

        return $this;
    }

    /**
     * Get productWholesalePriceFieldName
     *
     * @return string
     */
    public function getProductWholesalePriceFieldName()
    {
        return $this->productWholesalePriceFieldName;
    }

    /**
     * Set productRetailPriceFieldName
     *
     * @param string $productRetailPriceFieldName
     *
     * @return MaterialCoating
     */
    public function setProductRetailPriceFieldName($productRetailPriceFieldName)
    {
        $this->productRetailPriceFieldName = $productRetailPriceFieldName;

        return $this;
    }

    /**
     * Get productRetailPriceFieldName
     *
     * @return string
     */
    public function getProductRetailPriceFieldName()
    {
        return $this->productRetailPriceFieldName;
    }
}
