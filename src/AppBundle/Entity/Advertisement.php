<?php

namespace AppBundle\Entity;

use AppBundle\Util\FileUtil;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity(repositoryClass="AppBundle\Repository\AdvertisementRepository")
 * @ORM\Table
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 */
class Advertisement
{
    /**
     * Advertisement id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * Title
     *
     * @var string
     * @Assert\NotBlank()
     * @ORM\Column(type="string", length=255, nullable=false)
     */
    protected $title;


    /**
     * Ad position in list
     *
     * @var integer
     * @Assert\Type(type="integer")
     * @ORM\Column(type="integer", nullable=false)
     */
    protected $ordering;

    /**
     * Link URL of the ad
     *
     * @var string
     * @ORM\Column(type="text", nullable=true)
     */
    protected $link;
    

    /**
     * @JMS\Exclude
     * @var UploadedFile
     */
    protected $imageTemp;

    /**
     * @JMS\Exclude
     * @var UploadedFile
     * @Assert\File(
     *     maxSize = "5M",
     *     mimeTypes = {"image/jpeg", "image/gif", "image/png", "image/tiff"},
     *     maxSizeMessage = "The maxmimum allowed file size is 5MB.",
     *     mimeTypesMessage = "Only the filetypes image are allowed."
     * )
     */
    protected $imageFile;

    /**
     * @var string
     * @ORM\Column(type="string", length=255, nullable=true, name="image_path")
     * @JMS\Exclude
     */
    protected $imagePath;

    /**
     * Show this ad?
     *
     * @var boolean
     * @ORM\Column(type="boolean", nullable=false)
     */
    protected $isShow;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    
    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * @param string $title
     */
    public function setTitle($title)
    {
        $this->title = $title;
    }

    public function getImageAbsolutePath()
    {
        return $this->imagePath ? $this->getImageUploadRootDir().'/'.$this->imagePath : null;
    }

    /**
     * Image Web path
     *
     * @JMS\VirtualProperty
     * @JMS\Type("string")
     * @return null|string
     */
    public function getImageWebPath()
    {
        if ($this->imagePath && is_file($this->getImageAbsolutePath())) {
            return $this->getImageUploadDir().'/'.$this->imagePath;
        }

        return null;
    }

    protected function getImageUploadRootDir()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__.'/../../../web/'.$this->getImageUploadDir();
    }

    protected function getImageUploadDir()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/advertisements';
    }

    /**
     * Sets image file.
     *
     * @param UploadedFile $file
     */
    public function setImageFile(UploadedFile $file = null)
    {
        $this->imageFile = $file;

        // check if we have an old image path
        if (is_file($this->getImageAbsolutePath())) {
            // store the old name to delete after the update
            $this->imageTemp = $this->getImageAbsolutePath();
        }

        $this->preUploadImage();
    }

    /**
     * Get image file.
     *
     * @return UploadedFile
     */
    public function getImageFile()
    {
        return $this->imageFile;
    }

    public function preUploadImage()
    {
        if (null !== $this->getImageFile()) {
            $this->imagePath = FileUtil::generateFilename($this->getImageFile());
        }
    }

    /**
     * @ORM\PostPersist
     * @ORM\PostUpdate
     */
    public function uploadImage()
    {
        if (null === $this->getImageFile()) {
            return;
        }

        // check if we have an old image
        if (isset($this->imageTemp)) {
            // delete the old image
            unlink($this->imageTemp);
            // clear the temp image path
            $this->imageTemp = null;
        }

        // you must throw an exception here if the file cannot be moved
        // so that the entity is not persisted to the database
        // which the UploadedFile move() method does
        $this->getImageFile()->move(
            $this->getImageUploadRootDir(),
            $this->imagePath
        );

        $this->setImageFile(null);
    }

    /**
     * @ORM\PreRemove
     */
    public function storeImageFilenameForRemove()
    {
        $this->imageTemp = $this->getImageAbsolutePath();
    }

    /**
     * @ORM\PostRemove
     */
    public function removeImageUpload()
    {
        if (isset($this->imageTemp)) {
            unlink($this->imageTemp);
        }
    }

    /**
     * Set imagePath
     *
     * @param string $imagePath
     * @return $this
     */
    public function setImagePath($imagePath)
    {
        $this->imagePath = $imagePath;

        return $this;
    }

    /**
     * Get imagePath
     *
     * @return string
     */
    public function getImagePath()
    {
        return $this->imagePath;
    }

    /**
     * @return boolean
     */
    public function getIsShow()
    {
        return $this->isShow;
    }

    /**
     * @param boolean $isShow
     */
    public function setIsShow($isShow)
    {
        $this->isShow = $isShow;
    }

    /**
     * @return string
     */
    public function getLink()
    {
        return $this->link;
    }

    /**
     * @param string $link
     */
    public function setLink($link)
    {
        $this->link = $link;
    }

    /**
     * @return integer
     */
    public function getOrdering()
    {
        return $this->ordering;
    }

    /**
     * @param integer $ordering
     */
    public function setOrdering($ordering)
    {
        $this->ordering = $ordering;
    }
}
