<?php
/**
 * Created by PhpStorm.
 * User: skyguide
 * Date: 19.01.17
 * Time: 19:45
 */

namespace AppBundle\Entity;

use Symfony\Component\Validator\Constraints as Assert;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as JMS;

/**
 * @ORM\Entity()
 * @ORM\Table()
 * @ORM\HasLifecycleCallbacks
 * @JMS\ExclusionPolicy("NONE")
 *
 */
class Sector
{
    /**
     * Sector id
     *
     * @var integer
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Type("integer")
     * @JMS\Groups({"List", "show", "update"})
     */
    protected $id;

    /**
     * Id from the Woodformarch.com site
     *
     * @var integer
     *
     * @ORM\Column(type="integer", nullable=true)
     * @JMS\Type("integer")
     * @JMS\Groups({"List", "show", "update"})
     */
    protected $woodformId;

    /**
     * Title
     *
     * @var string
     *
     * @ORM\Column(type="text", nullable=false)
     * @JMS\Type("string")
     * @JMS\Groups({"List", "show", "update"})
     */
    protected $title;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set woodformId
     *
     * @param integer $woodformId
     *
     * @return Sector
     */
    public function setWoodformId($woodformId)
    {
        $this->woodformId = $woodformId;

        return $this;
    }

    /**
     * Get woodformId
     *
     * @return integer
     */
    public function getWoodformId()
    {
        return $this->woodformId;
    }

    /**
     * Set title
     *
     * @param string $title
     *
     * @return Sector
     */
    public function setTitle($title)
    {
        $this->title = $title;

        return $this;
    }

    /**
     * Get title
     *
     * @return string
     */
    public function getTitle()
    {
        return $this->title;
    }
}
