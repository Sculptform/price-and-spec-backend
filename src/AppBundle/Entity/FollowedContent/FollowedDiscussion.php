<?php

namespace AppBundle\Entity\FollowedContent;

use AppBundle\Entity\Discussion;
use AppBundle\Enum\FollowedContentType;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class FollowedDiscussion extends FollowedContent
{
    /**
     * @var Discussion
     *
     * @ORM\ManyToOne(targetEntity="AppBundle\Entity\Discussion")
     * @ORM\JoinColumn(name="discussion_id", referencedColumnName="id", nullable=true)
     */
    protected $discussion;

    /***********************************************
     *                Virtual fields
     ***********************************************/

    /**
     * Discussion Id
     *
     * @JMS\VirtualProperty
     * @JMS\Type("integer")
     * @JMS\Groups({"Show", "List"})
     *
     * @return int
     */
    public function getDiscussionId()
    {
        return $this->getDiscussion()->getId();
    }

    /***********************************************/

    /**
     * @return int
     */
    public function getType(){
        return FollowedContentType::DISCUSSION;
    }

    /**
     * @return Discussion
     */
    public function getDiscussion()
    {
        return $this->discussion;
    }

    /**
     * @param Discussion $discussion
     * @return FollowedDiscussion
     */
    public function setDiscussion(Discussion $discussion)
    {
        $this->discussion = $discussion;

        return $this;
    }
}
