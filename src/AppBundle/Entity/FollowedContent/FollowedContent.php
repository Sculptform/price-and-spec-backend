<?php

namespace AppBundle\Entity\FollowedContent;

use AppBundle\Enum\FollowedContentType;
use Doctrine\Common\Collections\Collection;
use JMS\Serializer\Annotation as JMS;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="followed_content")
 * @ORM\HasLifecycleCallbacks
 * @ORM\InheritanceType("SINGLE_TABLE")
 * @ORM\DiscriminatorColumn(name="type", type="integer")
 * @ORM\DiscriminatorMap({
 *      FollowedContentType::DISCUSSION      = "AppBundle\Entity\FollowedContent\FollowedDiscussion"
 * })
 * @JMS\ExclusionPolicy("NONE")
 * @JMS\Discriminator(field = "type", disabled = false, map = {FollowedContentType::DISCUSSION: "AppBundle\Entity\FollowedContent\FollowedDiscussion"})
 */
abstract class FollowedContent
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     * @JMS\Groups({"Update", "Show", "List"})
     */
    protected $id;

    /**
     * @var Collection
     * @JMS\Exclude
     * @ORM\ManyToMany(targetEntity="AppBundle\Entity\User", inversedBy="followedContent")
     */
    protected $users;

    /**
     * @return int
     * @see FollowedContentType
     */
    abstract public function getType();

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }
    /**
     * Constructor
     */
    public function __construct()
    {
        $this->users = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add user
     *
     * @param \AppBundle\Entity\User $user
     *
     * @return FollowedContent
     */
    public function addUser(\AppBundle\Entity\User $user)
    {
        $this->users[] = $user;

        return $this;
    }

    /**
     * Remove user
     *
     * @param \AppBundle\Entity\User $user
     */
    public function removeUser(\AppBundle\Entity\User $user)
    {
        $this->users->removeElement($user);
    }

    /**
     * Get users
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getUsers()
    {
        return $this->users;
    }
}
