<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 25.07.16
 * Time: 16:50
 */

namespace AppBundle\Model;

use JMS\Serializer\Annotation as JMS;

class APIBadRequestException extends APIEXception
{
    /**
     * Errors array
     *
     * @var array
     * @JMS\Type("array")
     */
    protected $errors;
}