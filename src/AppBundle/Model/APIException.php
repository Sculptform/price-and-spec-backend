<?php
/**
 * Created by PhpStorm.
 * User: ramenev
 * Date: 25.07.16
 * Time: 16:50
 */

namespace AppBundle\Model;

use JMS\Serializer\Annotation as JMS;

class APIException
{
    /**
     * Error code
     *
     * @var integer
     * @JMS\Type("integer")
     */
    protected $code;

    /**
     * Error message
     *
     * @var string
     * @JMS\Type("string")
     */
    protected $message;
}