<?php

namespace AppBundle\Command;

use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class CurrencyUpdaterCommand
 *
 * @package AppBundle\Command
 * @author  Faruh Narzullaev <faruh.narzullaev@sibers.com>
 */
class CurrencyUpdaterCommand extends ContainerAwareCommand
{
    const BASE_CURRENCY = 'AUD';

    protected function configure()
    {
        $this
            ->setName('currency:updater:update')
            ->setDescription('Convert and update currency')
            ->addArgument('symbol', InputArgument::OPTIONAL, 'Currency to be updated.', 'nzd')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $this
            ->getContainer()
            ->get('currency.updater')
            ->update();
    }
}
