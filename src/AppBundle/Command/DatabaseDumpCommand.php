<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 28.06.17
 * Time: 11:57
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class DatabaseDumpCommand extends ContainerAwareCommand
{
    const NAME = 'app:database:dump';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Dump database.')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'Absolute path for the file you need to dump database to.'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs        = new Filesystem();
        $container = $this->getContainer();

        $database = $container->getParameter('database_name');
        $username = $container->getParameter('database_user');
        $password = $container->getParameter('database_password');
        $host     = $container->getParameter('database_host');

        $path = $input->getArgument('file');

        $output->writeln(
            sprintf(
                '<comment>Dumping DB <fg=green>"%s"</fg=green> to <fg=green>"%s"</fg=green> </comment>',
                $database,
                $path
            )
        );

        if (! $fs->exists($path)) {
            $fs->mkdir(dirname($path));
        }

        $cmdStructure = sprintf(
            'mysqldump  --set-gtid-purged=OFF --no-data -u%s -p%s -h"%s" %s > %s',
            $username,
            $password,
            $host,
            $database,
            $path
        );
        $cmdData      = sprintf(
            'mysqldump --set-gtid-purged=OFF --no-create-info --ignore-table=%4$s.scene -u%s -p%s -h"%s" %s >> %s',
            $username,
            $password,
            $host,
            $database,
            $path
        );

        $cmdCompress = sprintf('gzip %s', $path);

        $commandStack = [
            $cmdStructure,
            $cmdData,
            $cmdCompress,
        ];

        foreach ($commandStack as $cmd) {
            $process = new Process($cmd, null, null, null, null);
            $process->run();

            if ($process->getExitCode() > 0) {
                throw new \Exception("Could not dump database ($cmd), Due to ".$process->getExitCodeText());
            }
        }

        $output->writeln('<comment>All done.</comment>');
    }
}
