<?php

namespace AppBundle\Command;

use AppBundle\Entity\Project;
use AppBundle\Entity\SampleRequest;
use AppBundle\Entity\User;
use AppBundle\Enum\PardotUserLastAction;
use AppBundle\Service\PardotService;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class PardotCommand
 */
class PardotCommand extends ContainerAwareCommand
{
    public function configure()
    {
        $this
            ->setName('pardot:send')
            ->setDescription('Dump database.')
            ->addArgument('name', InputArgument::OPTIONAL, 'Name of the command', 'download-drawings')
            ->addArgument('email', InputArgument::OPTIONAL, 'Email address of the user', 'ekaterina.tarasova@sibers.com')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $name  = $input->getArgument('name');
        $email = $input->getArgument('email');

        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        /** @var PardotService $pardotService */
        $pardotService = $this->getContainer()->get('app.service.pardot');

        /** @var User $user */
        $user = $em->getRepository(User::class)->findOneBy(['email' => $email]);

        $additionalData = $this->getAdditionalData();
        $pardotResponse = $pardotService->sendUserLastAction($user, $name, $additionalData);

        dump($pardotResponse);
        die;
    }

    protected function getAdditionalData($name = null, User $user = null)
    {
        return [
            'phone' => '+61 491 570 156',
            'company' => 'WoodMood Company',
            'notes'   => 'Lorem Ipsum',
            'project_url' => 'https://google.com'
        ];

//        if ($name === 'request-sample') {
//            $sampleRequest = $this->getSampleRequestPhone($user, $em);
//            if ($sampleRequest) {
//                $additionalData['phone'] = ($sampleRequest->getPhone()) ? $sampleRequest->getPhone() : '';
//            }
//        }
//
//        if ($name === 'download-drawings') {
//            /** @var Project $project */
//            $project = $user->getUserProjects()->first();
//            $additionalData['project_url'] =
//                "{$this->getContainer()->getParameter('sculptform_url')}/guest/designer/{$project->getId()}";
//        }
    }

    /**
     * @param User $user
     * @param $em
     *
     * @return SampleRequest
     */
    public function getSampleRequestPhone(User $user, $em)
    {
        /** @var SampleRequest $sampleRequest */
        $sampleRequest = $em
            ->getRepository(SampleRequest::class)
            ->findOneBy(['user' => $user])
        ;

        return $sampleRequest;
    }
}
