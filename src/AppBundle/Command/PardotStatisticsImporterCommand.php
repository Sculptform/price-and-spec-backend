<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Command;

use AppBundle\Entity\PardotStatistics;
use AppBundle\Enum\PardotTrackActionsEnum;
use Ddeboer\DataImport\Reader\CsvReader;
use DoctrineExtensions\Query\Mysql\Date;
use JMS\Serializer\Serializer;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Exception\InvalidArgumentException;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Exception\FileNotFoundException;
use Symfony\Component\Filesystem\Filesystem;

/**
 * Class PardotStatisticsImporterCommand
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Command
 */
class PardotStatisticsImporterCommand extends ContainerAwareCommand
{
    // the name of the command (the part after "bin/console")
    const NAME = 'pardot:form-handler:import';

    protected function configure()
    {
        $this->setName(self::NAME)->setDescription(
            'Imports Pardot form hadlers data from exported CSV files.'
        );

        $this
            ->addArgument(
                'filename', InputArgument::REQUIRED,
                'Absolute path to the CSV file'
            )
            ->addOption(
                'type',
                't',
                InputOption::VALUE_REQUIRED,
                'Type of the data, should be one of the following '
                .PardotTrackActionsEnum::toString(),
                1
            );
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs = new Filesystem();
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');

        $filename = $input->getArgument('filename');
        $type     = $input->getOption('type');

        if ( ! $fs->exists($filename)) {
            throw new FileNotFoundException('File not found.');
        }

        if ( ! $type || ! in_array($type, PardotTrackActionsEnum::toArray())) {
            throw new InvalidArgumentException(
                'Type is required and should be one of the following '
                .PardotTrackActionsEnum::toString()
            );
        }

        $file   = new \SplFileObject($filename);
        $reader = new CsvReader($file);
        $reader->setHeaderRowNumber(0);

        foreach ($reader as $row) {
            $firstName  = trim($row['First Name']);
            $lastName   = trim($row['Last Name']);
            $username   = $email = trim($row['Email']);
            $actionDate = new \DateTime(trim($row['Last Activity At']));

            $statistics = new PardotStatistics();
            $statistics->setActionType($type);
            $statistics->setEmail($email);
            $statistics->setUsername($username);
            $statistics->setFirstName($firstName);
            $statistics->setLastName($lastName);
            $statistics->setActionDate($actionDate);

            $em->persist($statistics);
        }

        $em->flush();
    }
}
