<?php
/**
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 */

namespace AppBundle\Command;


use AppBundle\Service\AWS\RDSManagerService;
use Aws\Exception\AwsException;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\DependencyInjection\ContainerAwareTrait;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\Response;

/**
 * Class AwsDBCloner
 *
 * @author        Vladimir Eliseev <vladimir.eliseev@sibers.com>
 * @copyright (c) 2019, Sibers
 * @package       AppBundle\Command
 */
class AwsDBClonerCommand extends ContainerAwareCommand
{
    use ContainerAwareTrait;

    const NAME = 'app:aws:clone_db';

    /**
     * Configuration of the command
     *
     * @return void
     */
    protected function configure()
    {
        $this->setName(self::NAME)->setDescription(
            'Restores a DB cluster to an arbitrary point in time.'
        );
    }

    /**
     * @param InputInterface $input
     * @param OutputInterface $output
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var ContainerInterface $container
         */
        $container = $this->getContainer();

        /**
         * @var RDSManagerService $manager
         */
        $manager = $container->get('app.aws.rds.manager');

        $output->writeln(
            'Starting the process of re-creation cluster with instance... 
                    Be patient...'
        );

        if ($manager->clusterExists() && $manager->clusterAvailable()) {
            $output->writeln(
                sprintf(
                    'Cluster %s exists and available.',
                    $manager->getStagingClusterName()
                )
            );

            if ($manager->instanceExists() && $manager->instanceAvailable()) {
                $output->writeln(
                    sprintf(
                        'Instance %s exists and available.',
                        $manager->getStagingInstanceName()
                    )
                );

                $manager->removeDbInstance();
                $manager->createDbCluster();
            }
        }elseif(!$manager->clusterExists()){
            $manager->createDbCluster();
        }

        $output->writeln('Finished.');
    }
}
