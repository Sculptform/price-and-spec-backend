<?php

namespace AppBundle\Command;

use AppBundle\Entity\{
    News, Quotation, UserProject, SampleRequest
};

use AppBundle\Event\{
    Events, NewsEvent, QuotationEvent, UserProjectEvent, SampleRequestEvent
};

use Symfony\Component\Console\{
    Input\InputOption, Input\InputArgument, Input\InputInterface, Output\OutputInterface
};

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class EmailNotificationCommand
 *
 * @package AppBundle\Command
 * @author  Farukh Narzullaev <faruh.narzullaev@sibers.com>
 */
class EmailNotificationCommand extends ContainerAwareCommand
{
    private static $commands = [
        'news'  => [
            'entity'     => News::class,
            'eventName'  => Events::NEWS_CREATED,
            'eventClass' => NewsEvent::class,
        ],
        'project_shared' => [
            'entity'     => UserProject::class,
            'eventName'  => Events::PROJECT_SHARED,
            'eventClass' => UserProjectEvent::class
        ],
        'quote' => [
            'entity'     => Quotation::class,
            'eventName'  => Events::QUOTATION_RECEIVED,
            'eventClass' => QuotationEvent::class
        ],
        'sample_request' => [
            'entity'     => SampleRequest::class,
            'eventName'  => Events::SAMPLE_REQUEST_RECEIVED,
            'eventClass' => SampleRequestEvent::class,
        ]
    ];

    protected function configure()
    {
        $this
            ->setName('email:notify')
            ->setDescription('Email notification.')
            ->addArgument('type', InputArgument::REQUIRED, 'Command name you are trying to execute.')
            ->addArgument('id',   InputArgument::REQUIRED, 'ID of the news OR shared project OR quotation OR sample request.')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $em = $this->getContainer()->get('doctrine.orm.entity_manager');
        $dispatcher = $this->getContainer()->get('event_dispatcher');

        $type = $input->getArgument('type');
        $id   = $input->getArgument('id');

        $entityName = static::$commands[$type]['entity'];
        $eventName  = static::$commands[$type]['eventName'];
        $eventClass = static::$commands[$type]['eventClass'];

        $entity = $em->getRepository($entityName)->find($id);
        if ($entity) {
            $dispatcher->dispatch(
                $eventName, new $eventClass($entity)
            );

            $output->writeln('<comment>Success</comment>');
        }
    }
}
