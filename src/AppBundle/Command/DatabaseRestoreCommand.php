<?php
/**
 * Created by PhpStorm.
 * User: expert
 * Date: 28.06.17
 * Time: 11:57
 */

namespace AppBundle\Command;

use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Process\Process;

class DatabaseRestoreCommand extends ContainerAwareCommand
{
    const NAME = 'app:database:restore';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Restore database.')
            ->addArgument(
                'file',
                InputArgument::REQUIRED,
                'Absolute path for the file you need to restore database from.'
            );
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $fs        = new Filesystem();
        $container = $this->getContainer();

        $database = $container->getParameter('database_name');
        $username = $container->getParameter('database_user');
        $password = $container->getParameter('database_password');
        $host     = $container->getParameter('database_host');

        $path = $input->getArgument('file');

        $output->writeln(
            sprintf(
                '<comment>Restore DB <fg=green>"%s"</fg=green> from <fg=green>"%s"</fg=green> </comment>',
                $database,
                $path
            )
        );

        if (! $fs->exists($path)) {
            throw new \Exception(sprintf('Could not find file: %s', $path));
        }

        $cmd = sprintf('(zcat %s | mysql -u%s -h"%s" -p%s %s)', $path, $username, $host,$password, $database);

        $process = new Process($cmd, null, null, null, null);

        $process->run();

        if ($process->getExitCode() > 0) {
            throw new \Exception('Could not restore database');
        }

        $output->writeln('<comment>All done.</comment>');
    }
}
