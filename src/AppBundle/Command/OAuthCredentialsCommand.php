<?php

namespace AppBundle\Command;

use AppBundle\Entity\OAuth\Client;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;

/**
 * Class OAuthCredentialsCommand
 *
 * @package AppBundle\Command
 */
class OAuthCredentialsCommand extends ContainerAwareCommand
{
    const NAME = 'app:oauth:config';

    protected function configure()
    {
        $this
            ->setName(self::NAME)
            ->setDescription('Set oauth client id and client secret.');
    }

    /**
     * @param InputInterface  $input
     * @param OutputInterface $output
     *
     * @return void
     * @throws \Exception
     */
    protected function execute(InputInterface $input, OutputInterface $output)
    {
        $container = $this->getContainer();

        $clientId     = $container->getParameter('oauth_client_id');
        $clientSecret = $container->getParameter('oauth_client_secret');
        $uris         = $container->getParameter('oauth_redirect_uris');
        $grantTypes   = $container->getParameter('oauth_grant_types');

        /**
         * @var $em EntityManagerInterface
         */
        $em      = $container->get('doctrine.orm.entity_manager');
        $client = $em->getRepository(Client::class)->findOneBy(['randomId' => $clientId, 'secret' => $clientSecret]);

        if(!$client){
            $em->persist($this->insertClient($clientId, $clientSecret, $uris, $grantTypes));
            $em->flush();
            $output->writeln('<comment>Key was inserted.</comment>');
        }else{
            $output->writeln('<comment>Key already exists.</comment>');
        }

        $output->writeln('<comment>All done.</comment>');
    }

    /**
     * @param string $clientId
     * @param string $clientSecret
     * @param array  $uris
     * @param array  $grantTypes
     *
     * @return Client
     */
    private function insertClient(string $clientId, string $clientSecret, array $uris, array $grantTypes)
    {
        $client = new Client();
        $client->setRandomId($clientId);
        $client->setSecret($clientSecret);
        $client->setAllowedGrantTypes($grantTypes);
        $client->setRedirectUris($uris);

        return $client;
    }
}